#!/bin/bash 

# compile_jiggle.sh
# This script compiles Jiggle .java source files into .class files from the command line
# This script lives in the src/ directory

if [ "$#" -ne 1 ]
then
   echo "Usage: ./compile_jiggle.sh <java_version>"
   echo "Possible values of <java_version>: 6, 7, 8"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   6)
      echo "6"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.6.0_65`
      ;;
   7 )
      echo "7"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.7.0_80`
      ;;
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   * )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac


# Input directory for .java source files
SRC_DIR="./"

# Input directory for .jar external libraries
LIB_DIR="../lib/"

# Output directory for .class files
CLASS_DIR="./out_classes_java"${JAVA_VER}
echo "CLASS_DIR: "${CLASS_DIR}

# Oracle database driver jar name
JDBC_FILE="ojdbc"${JAVA_VER}".jar"
echo "JDBC_FILE: "${JDBC_FILE}

if [ -d ${CLASS_DIR} ] 
then
   rm -r ${CLASS_DIR}
fi
mkdir ${CLASS_DIR}

# Compile Jiggle
CLASS_PATH="${SRC_DIR}*:${LIB_DIR}postgresql-42.2.5.jar:${LIB_DIR}${JDBC_FILE}:${LIB_DIR}acme.jar:${LIB_DIR}openmap.jar:${LIB_DIR}QWClient.jar:${LIB_DIR}seed-pdcc.jar:${LIB_DIR}looks-2.0.4.jar:${LIB_DIR}forms-1.0.7.jar:${LIB_DIR}colt.jar:${LIB_DIR}swarm.jar:${LIB_DIR}usgs.jar:${LIB_DIR}jregex1.2_01.jar" 
javac -version
javac -d ${CLASS_DIR} $(find ${SRC_DIR} -name "*.java") -cp ${CLASS_PATH} -encoding ISO8859-1

package gov.usgs.swarm.wave;

import java.awt.event.MouseEvent;
import java.util.EventListener;

// Derivative of WaveViewPanelListener by Dan Cervelli in AVO Swarm distribution
public interface JiggleSwarmWaveViewPanelListener extends EventListener {
    public void waveZoomed(JiggleSwarmWaveViewPanel src, double oldST, double oldET, double newST, double newET);
    public void mousePressed(JiggleSwarmWaveViewPanel src, MouseEvent e, boolean dragging);
    public void waveClosed(JiggleSwarmWaveViewPanel src);
    public void waveTimePressed(JiggleSwarmWaveViewPanel src, MouseEvent e, double j2k);
}

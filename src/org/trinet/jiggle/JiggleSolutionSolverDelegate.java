package org.trinet.jiggle;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.*;
import org.trinet.util.locationengines.*;


// Should all engine solving invocations spawn a worker thread
// or only those with time-intensive waveform scans?

// Generalized api to have ability to add engines 
// stored in hashmap with "type" as key for lookup.
//
// TODO:
// Refactor the subclassed Jiggle/Mung locate/calcMag methods
// perhaps by implementing listeners to delegate in Jiggle/Mung 
// for the the engine property changes then they can do their 
// own GUI updates rather than invoking parent's GUI changes 
// here (e.g. statusPanel updates).
// Also the Mung related subclass generates a button/menu action 
// to configure its menu toolbar based upon state but an
// analog is not implemented here.
//

/**
* Generic engine adapter helper class to support Jiggle
* interface with LOCATION, MAGNITUDE engines etc. 
*/
public final class JiggleSolutionSolverDelegate extends AbstractHypoMagEngineDelegate {
// NOTE if need to subclass remove "final" in declaration above

    private static final String THREAD_SOLVING_MAG = "Calculating mag...";
    private static final String THREAD_SOLVING_LOC = "Locating...";
    private static final String NULL_LOC_ENGINE = "null location engine";

    private boolean locEngineFixedZNotify = true;

    private Jiggle jiggle;

    // Special constructor used by DPMagMethod panel to preserve magMethod and magEngine status stored in input delegate maps
    protected JiggleSolutionSolverDelegate(JiggleSolutionSolverDelegate delegate, JiggleProperties newProps) { // added -aww 2008/11/07 
        super();
        setDelegateProperties(newProps);
        if (delegate != null) {
          this.jiggle = delegate.jiggle;
          magMethodMap = new HashMap(delegate.magMethodMap);
          magTypeEngPropMap = new HashMap(delegate.magTypeEngPropMap);
        }
        else {
          this.jiggle = Jiggle.jiggle;
        }

        //create new magMethod instances and set their properties to those the original input maps,
        //likewise clone properties for the engine for the method magtypes
        Set keys = magMethodMap.keySet();
        Iterator iter = keys.iterator();
        String magType = null;
        MagnitudeMethodIF oldMagMethod = null;
        MagnitudeMethodIF newMagMethod = null;
        GenericPropertyList gpl = null;
        while (iter.hasNext()) {
          magType = (String) iter.next();
          oldMagMethod = (MagnitudeMethodIF) magMethodMap.get(magType);
          try {
            // create new instance of magmethod
            newMagMethod = (MagnitudeMethodIF) oldMagMethod.getClass().newInstance();
            // set props of this new instance to old values
            newMagMethod.setProperties(oldMagMethod.getProperties());
            // save this updated new instance back into this delegate instance's new method map
            magMethodMap.put(magType, newMagMethod);
            // clone input property lists for this delegate instance's engine map
            gpl = (GenericPropertyList) magTypeEngPropMap.get(magType);
            if (gpl != null) magTypeEngPropMap.put(magType, (GenericPropertyList) gpl.clone());
          }
          catch (Exception ex) {
            ex.printStackTrace();
          }
        }
    }

    protected JiggleSolutionSolverDelegate(Jiggle jiggle) {
        super(jiggle.getProperties());
        this.jiggle = jiggle;
    }

    public boolean setDelegateProperties(GenericPropertyList props) {
        boolean status = super.setDelegateProperties(props);
        locEngineFixedZNotify = props.getBoolean("locEngineFixedZNotify", true);
        return status;
    }

    // Must run in event thread effect parent GUI: 
    public void reportMessage(String titleType, String message) {
        jiggle.statusPanel.setText(message);
        System.err.println(message);
        jiggle.popInfoFrame(titleType, message);
    }

/////////// BEGIN LOCATION ENGINE METHODS /////////////////

    public String getPrtFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getPrtFileContents(); // return text contents of Hypoinverse PRT file
    }

    // |GetInstFileList| - return list of available instruction files, returns one filename per line terminated by |EOT|
    public String getInstFileList() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getInstFileList();
    }

    // |GetInstName| - returns the name of the current session instruction file
    public String getInstName() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getInstName();
    }

    // |GetInstFile|  - return contents of current session instruction file
    public String getInstFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getInstFileContents();
    }

    // |GetInstFile| <filename> - return contents of specified instruction file
    public String getInstFileContents(String filename) {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getInstFileContents(filename);
    }

    // |PutInstFile| <filename> - write text lines to server "filename" instruction file, sends |EOT| 
    public boolean putInstFile(String instrTextLines, String filename) {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return false;
        return locEng.putInstFile(instrTextLines, filename);
    }

    // |SetInstFile| - set instruction file back to default hypinst.
    public boolean setInstFile() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return false;
        return locEng.setInstFile();
    }

    // |SetInstFile| <filename> - set use of named instruction file for the duration of the connection
    public boolean setInstFile(String filename) {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return false;
        return locEng.setInstFile(filename);
    }

    public String getStaFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getStaFileContents();  // return text contents of Hypoinverse station file
    }

    public String getCrhFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getCrhFileContents(); // return text contents of Hypoinverse crustal model file
    }

    public String getArcInFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getArcInFileContents(); // return text contents of Hypoinverse crustal model file
    }

    public String getArcOutFileContents() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getArcOutFileContents(); // return text contents of Hypoinverse crustal model file
    }

    public String getStaFile() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getStaFile(); // return text contents of Hypoinverse crustal model file
    }

    public String getCrhFile() {
        LocationEngineHypoInverse locEng = (LocationEngineHypoInverse) getLocationEngine();
        if (locEng == null) return NULL_LOC_ENGINE;
        return locEng.getCrhFile(); // return text contents of Hypoinverse crustal model file
    }

    /**
     * Locate the current event.
     * Runs in separate thread
     * Returns false since the thread run result is not know upon its return.
     */
    public boolean locate(Solution sol) {

        if (! okToLocate(sol) ) {
           reportStatusMessage("Locate Failure"); 
           return false;
        }

        solveSuccess = false;

        LocationEngineIF locEng = getLocationEngine();
        /*
        if (locEng == null) {
          reportMessage("Location Engine Null, Can't locate(solution)",
                       "Check property file for LocationEngine property definition");
        */

        // delegate instance trial property overrides that set by engine property
        locEng.setUseTrialLocation(myProps.getBoolean("useTrialLocation"));
        locEng.setUseTrialOriginTime(myProps.getBoolean("useTrialOriginTime"));

        /* Below locEng config done by setDelegateProperties init of loc engine after property changed in prefs -aww 2008/09/09
        if (locEng instanceof LocationEngineServiceIF) {
          LocationEngineServiceIF service = (LocationEngineServiceIF) locEng;
          String address = myProps.getProperty("locationEngineAddress");
          int port       = myProps.getInt("locationEnginePort");
          // This allows for the fact that the address and port may have been changed
          // since the last time. If it hasn't changed this is a nooop.
          service.setServer(address, port);
        }
        */

        SwingWorker worker = new LocEngSwingWorker(locEng, sol);
        // setup the statusPanel the duration of the run, a progress object would be better
        jiggle.initStatusPanelForWork(THREAD_SOLVING_LOC);
        worker.start(); // does the location
        return false; // don't know result
    }

    private class LocEngSwingWorker extends org.trinet.util.SwingWorker {

        final LocationEngineIF locEng;
        final Solution sol;
        double oldOriginTime = Double.NaN;
        WorkerStatusFrame workStatus = new WorkerStatusFrame();
        boolean status = false; // save location status here -aww 2010/08/24
        boolean depthFixed = false;

        public LocEngSwingWorker(LocationEngineIF locEng, Solution sol) {
            super();
            this.locEng = locEng;
            this.sol = sol;
            this.oldOriginTime = sol.getTime();
            this.depthFixed = sol.depthFixed.booleanValue();

        }

        // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS using SwingUtilites.invokeLater(new Runnable(...))!
        public Object construct() {
            //locEng.reset();
            workStatus.setBeep(jiggle.beep);
            // Create a popup for the duration of the run, a progress object would be better
            StringBuffer sb = new StringBuffer(132);
            sb.append("<html><center><b>Relocating ").append(sol.getId()).append("</b><p>");
            sb.append("LocationEngine = ").append(locEng.getLocatorName()).append("</center></html>");
            workStatus.pop("Locate", sb.toString(), true); // "Locating event "+sol.getId().longValue()
            status = locEng.solve(sol); // added return status for insurance on engine failure state -aww 2010/08/24
            return null;
        }

        //Runs on the event-dispatching thread so graphics calls are OK here.
        public void finished() {
            if (!locEng.success() || !status) { // check for failure, include status result too -aww 2010/08/24
              solveSuccess = false;
              workStatus.unpop();
              // get message from LocationEngine for display in text area
              reportMessage("Location Server Failure: Check data or Retry", locEng.getStatusString());
            } else {

              jiggle.resetVelocityModel(sol); // added 2011/07/28 -aww
              jiggle.resetAuthority(sol);

              solveSuccess = true;
              // saved state effected by solCommitResetsRflag property which sets currentSol.commitResetsRflag -aww 2015/04/03
              if ( sol.commitResetsRflag && !EnvironmentInfo.isAutomatic()) {
                  sol.processingState.setValue(JasiProcessingConstants.STATE_HUMAN_TAG);
              }
              boolean typeChange = false;
              // make event etype "eq" if not defined by a property override -aww
              if ( sol.isEventDbType(EventTypeMap3.UNKNOWN) || sol.isEventDbType(EventTypeMap3.TRIGGER) ) {
                 sol.setEventType(myProps.getProperty("defaultTriggerEventType", EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE)));
                 typeChange = true;
              }
              // make origin gtype "local" if not defined by a property override -aww
              if ( !(sol.isOriginGType(GTypeMap.LOCAL) || sol.isOriginGType(GTypeMap.REGIONAL)) ) {
                sol.setOriginGType(myProps.getProperty("defaultTriggerGType", GTypeMap.LOCAL));
                typeChange = true;
              }

              if (typeChange) {
                  jiggle.toolBar.solPanel.resetChooserTypes();
              }

              jiggle.statusPanel.clearOnlyOnMatch(THREAD_SOLVING_LOC);

              // resort views distance order after locating, resets the GUI layout in Event thread
              jiggle.resortWFViews();

              jiggle.selectTab(Jiggle.TAB_LOCATION);

              workStatus.unpop();

              // now recalculate the preferred mag using the new location results
              autoRecalcPrefmag(sol);

              if (! Double.isNaN(oldOriginTime) && Math.abs(sol.getTime()-oldOriginTime) > 180.) {
                 reportMessage("Large Origin Time Change Warning", "Possibly summary parse error, verify event origin time.");
              }
              if (this.depthFixed != sol.depthFixed.booleanValue() && locEngineFixedZNotify ) {
                 reportMessage("Origin Fixed Depth Warning", "Event origin fixed depth state changed by solserver.");
              }
          }
          // update the text the tabs & frame title
          jiggle.updateTextViews(); // could invoke via a SOLVED PropertyChangeEvent listener in Jiggle
        }

        /** Invoked after a relocation to recalculate the preferred magnitude.  */
        private boolean autoRecalcPrefmag(Solution sol) {
            //Magnitude mag = sol.getPreferredMagnitude();
            //if (mag == null || mag.hasNullValue()) return false; //aww
            //TODO: MagEngineDelegateIF method like getMagMethodTypes() in lieu of JiggleProperties? -aww
            String [] magTypes = jiggle.getProperties().getMagMethodTypes();
            Magnitude mag = null;
            boolean status = false;
            for (int i = 0; i<magTypes.length; i++) { 
              mag = sol.getPrefMagOfType(magTypes[i].substring(1).toLowerCase()); // test here
              if (mag == null || mag.hasNullValue()) {
                  statusString = "EngineDelegate: Can't recalculate mag, undefined prefmag type";
                  continue; //skip, don't create new magnitudes ? - aww
              }

              String magType = mag.getTypeString().toUpperCase();
              if (mag.getReadingsCount() > 0) { // data to support mag
                if (myProps.getBoolean("autoRecalc"+magType)) {
                    statusString = "EngineDelegate: doing autocalc for: "+magType;
                    status = calcMag(sol, magType, false);
                } else {
                    statusString = "EngineDelegate: auto-recalc off (false) for "+magType;
                }
              } else { // no data to support mag !
                statusString = "EngineDelegate: Can't recalculate mag, no readings found for "+magType;
                // Timers complained about this. Happens on all Mh's
                // String mmsg = "<html><b>WARNING: </b>Magnitude has no supporting readings<br>"+
                // " and can not be recalculated.<br>"+
                // "The previous magnitude will be retained but...<p>"+
                // "<b>MAGNITUDE MAY BE INVALID FOR NEW LOCATION</b></html>";
                // jiggle.popInfoFrame("Old Magnitude Used", mmsg);
              }
            }
            jiggle.statusPanel.setText(statusString);
            System.out.println(statusString);
            return status;
        }

    } // end of LocEngSwingWorker class

//
//////////// END LOCATION ENGINE METHODS ///////////////////
//

//
/////////// BEGIN MAGNITUDE ENGINE METHODS ////////////////
//

    /** Calculate the preferred magnitude of input Solution.
    Does NOT re-examine the waveforms, uses existing readings. */
    public boolean calcMag(Solution sol) {
        Magnitude mag = sol.getPreferredMagnitude();
        if (mag == null) return false;  // no mag
        return calcMag(sol, mag.getTypeString(), true);
    }
    /**
     * Updates an existing preferred Magnitude of input type <i>magType</i>
     * (string of form  "Msubtype" e.g. Ml)
     * else create a new Magnitude and sets it to be the preferred for that type.
     * Does not set the preferred magnitude for the event.
     **/
    // Must run in event thread: 
    public boolean calcMag(Solution sol, String magType) {
        if ( magType.endsWith("d") && jiggle.getProperties().getBoolean("mdTauOptionEnabled")) {
            int answer = JOptionPane.showConfirmDialog(jiggle,
               "Reset every tau to values extrapolated from Q,A fit parameters or not?\n(yes, overrides any remaining normal termination cutoff values)",
               "Md Method Tau Option",
               JOptionPane.YES_NO_OPTION
            );
            boolean oldtf = CodaMagnitudeMethod.SET_TAU_FROM_FIT;;
            CodaMagnitudeMethod.SET_TAU_FROM_FIT = ( answer == JOptionPane.YES_OPTION);
            boolean tf = calcMag(sol, magType, true);
            CodaMagnitudeMethod.SET_TAU_FROM_FIT = oldtf;
            return tf;
        }

        return calcMag(sol, magType, true);
    }

    public boolean calcMag(Solution sol, String magType, boolean selectMagTab) {

        solveSuccess = false;
        // Check for mag method/engine instantiation
        MagnitudeEngineIF magEng = initMagEngineForType(sol, magType); 
        if (magEng == null || ! magEng.isEngineValid()) {
          reportStatusMessage("Magnitude Engine Initialization Failed: Check data or properties");
          return false;
        }

        if (debug) {
          System.out.println(" Jiggle DEBUG Calculating mag " +
                             magEng.getMagMethod().getMethodName() +
                             " for: " +
                             sol.toString()
                            );
        }
        boolean hasPrefMag = true; // assume it already has some magnitude
        Magnitude prefMag = sol.getPreferredMagnitude();
        // Should we make a new mag or reuse preexisting mag objects? 
        // Does a preferred magnitude exist of the same type as requested?
        // if not, need to create a new mag then copy the appropiate solution
        // associated reading list into this new mag of requested type. -aww 
        // NOTE: mag.isSameType(type) is case sensitive (uses no "M" prefix only the lowercase subscript)

        if (prefMag != null && prefMag.getTypeString().equalsIgnoreCase(magType)) { // event preferred
          // By default use existing readings, a new prefMag may not have been committed to database
          if (prefMag.getReadingList().size() < 1) prefMag.loadReadingList(); // load only if readings don't exist 
        }
        else { // preferred of type, but not the event preferred
          // If there an existing mapped prefmag of type use it rather than creating a "new" mag  - aww 10/19/2004 
          int startAt = (magType.startsWith("M")) ? 1 : 0; // need to ignore any leading "M"
          prefMag = sol.getPrefMagOfType(magType.substring(startAt).toLowerCase());
          if (prefMag == null) {  // if no prefmag of type exists, THEN make a new mag
            prefMag = magEng.getNewMag(sol); // engine initialized to prefmag magMethod type above
            hasPrefMag = false;
          }
          // If it's NOT a newly created prefmag and has an empty reading list 
          // then try loading prefmag assoc reading list data from datasource - aww 02/14/2007
          if (prefMag.getReadingsCount() < 1 && hasPrefMag) {
              prefMag.loadReadingList();
              prefMag.associateLists(sol); // put newly loaded datasource readings into masterview sol reading list
          }

          if (prefMag.getReadingsCount() < 1) { // prefmag is either new or has no datasource reading associations 
              prefMag.setReadingListFromAssocSolution(); // copy reading list of assoc sol - aww 07/19/2004
              if (prefMag.getReadingsCount() < 1) { // assoc solution has an empty reading list 
                  // Load prefmag reading list with datasource data associated with it's solution
                  //prefMag.fastAssociateAll((java.util.List)prefMag.getReadingTypeInstance().getBySolution(sol));
                  prefMag.loadReadingListBySolution(); // aww 07/19/2004
              }
          }
        }

        boolean makePref = magEng.isMakePreferredMagEnabled(); // save engine state
        // Below line overrides magEngine method default property settings
        magEng.setMakePreferredMag(false); // do not allow engine to set event preferred magnitude 10/19/2004 aww
        prefMag = (magEng.solve(prefMag)) ? magEng.getSummaryMagnitude() : prefMag ;
        magEng.setMakePreferredMag(makePref); // reset engine state

        // engine should only return success if it's a valid magnitude

        if (magEng.success()) {
          solveSuccess = true;
          Magnitude eprefmag = sol.getPreferredMagnitude();
          // Check for ml type
          Magnitude mlrMag = null;
          if (jiggle.getProperties().getBoolean("mlrMagOptionEnabled")) {
            if ( prefMag != null && magType.endsWith("l") ) {
              Object mm = magEng.getMagMethod();

              if ( mm instanceof MlMagMethod ) {
                  mlrMag = ((MlMagMethod)mm).createMlrMag(prefMag);
                  if (mlrMag != null) sol.setPrefMagOfType(mlrMag);
              }
            }
          }
          // set the calculated prefMag to the event preferred if no event preferred already
          if (eprefmag == null) {
            if (prefMag != null) {
              // set the mlrMag to the event preferred if it exists
              if (mlrMag != null) {
                sol.setPreferredMagnitude( (prefMag.getPriority() >= mlrMag.getPriority()) ? prefMag : mlrMag );
              }
          else {
                sol.setPreferredMagnitude(prefMag);
              }
            }
          }
          // set non-null mlrMag to the event preferred, if current preferred is of type ML
          else if (eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) && mlrMag != null) {
              if (mlrMag.getPriority() >= eprefmag.getPriority()) sol.setPreferredMagnitude(mlrMag);
          }
          else if ( magType.endsWith("l") && eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) && mlrMag == null ) {
              // override old event preferred Mlr with new Ml - aww 10/18/2016
              eprefmag.setDeleteFlag(true);
              sol.setPreferredMagnitude(prefMag);
          }
          // REPLACED BY ABOVE
          //if (sol.getPreferredMagnitude() == null) sol.setPreferredMagnitude(prefMag); // set preferred of "event"
          //
          //else {
            if (! hasPrefMag) System.out.println("INFO: No existing prefmag for magtype, setting prefmag for : " + magType); 
            sol.setPrefMagOfType(prefMag); // set "preferred" of magtype
          //}
          //
        }
        else {
          // get message from engine for display in text area
          reportMessage("ERROR", "Magnitude Engine Failed:"+magEng.getStatusString());
        }
        jiggle.updateTextViews(); // could invoke via a SOLVED PropertyChangeEvent listener in Jiggle
        //if (prefMag != null) jiggle.setMagTabByType(prefMag.getTypeString()); // -aww 2008/03/03
        if (prefMag != null ) jiggle.updateMagTab(prefMag.getTypeString());
        if (selectMagTab) jiggle.selectTab(Jiggle.TAB_MAGNITUDE);
        return solveSuccess;
    }

    /**
     * Method below is required by IF, the input mag maybe the input Solution's 
     * preferred mag or not.
     * Method relocates any stale Solution before solving for magnitude.
     * Upon success, the input magnitude becomes the preferred magnitude.
     */
    public boolean calcMag(Magnitude aMag, Solution aSol) {

        solveSuccess = false;

        // Check inputs for not null and association
        if (! checkInputAssocAreValid(aMag, aSol) ) return false;

        // Make sure solution location is ok
        if (! resolveLocation(aSol) ) return false;

        MagnitudeEngineIF magEng = initMagEngineForType(aSol, aMag.getTypeString());
        if (magEng == null || ! magEng.isEngineValid()) {
          reportStatusMessage("Magnitude Engine Initialization Failed: Check data or properties");
          return false;
        }

        // Resolve input magnitude's data, if it's an empty list,
        // load it from data source by desired assocSol or assocMag
        // depending on delegates configuration properties 
        if (! resolveMagReadingList(aMag, aSol) ) return false;

        // intialize an engine to solve for this magnitude type
        solveSuccess = magEng.solve(aMag); // could set prefmag in event
        reportEngineCalculationStatus(magEng);
        return solveSuccess;
    }
/*
// Below are overloaded method signatures not used in Jiggle
    public boolean calcMag() {
        return calcMag(jiggle.mv.getSelectedSolution());
    }
    public boolean calcMag(String type) {
        return calcMag(jiggle.mv.getSelectedSolution(), type);
    }
    public boolean calcMagFromWaveforms() {
        return calcMagFromWaveforms(jiggle.mv.getSelectedSolution());
    }
    public boolean calcMagFromWaveforms(String type) {
        return calcMagFromWaveforms(jiggle.mv.getSelectedSolution(), type);
    }
*/
    /** Calculates a new preferred magnitude for input Solution of same type
     * as its current preferred magnitude by doing scans of its Waveforms
     * to calculate new Channel magnitude data.
     * Runs in a separate thread.
     * Returns false since the thread run result is not know upon return.
    * */
    public boolean calcMagFromWaveforms(Solution sol) {
        return calcMagFromWaveforms(sol, sol.getPreferredMagnitude().getTypeString());
    }

    String activeWfMagCalcType = "";

    /** Calculates a new preferred magnitude of the specified input type
     * for the input Solution by doing scans of its Waveforms to calculate
     * new Channel magnitude data.
     * Runs in a separate thread.
     * Returns false since the thread run result is not know upon return.
    * */
    public boolean calcMagFromWaveforms(Solution sol, String magType) {
      solveSuccess = false;
      // Check for a valid solution
      if (! jiggle.hasGoodLocation(sol) ) return false;


      /* Check for magnitude method/engine instantiation
      MagnitudeEngineIF magEng = initMagEngineForType(sol, magType); 
      if (magEng == null || ! magEng.isEngineValid()) {
        reportStatusMessage("Magnitude Engine Initialization Failed: Check data or properties");
        return false;
      }
      */
 
          // Check for magnitude method/engine instantiation

      //SwingWorker worker = new MagEngSwingWorker(magEng, sol);
      SwingWorker worker = new MagEngSwingWorker(magType, sol);
      // setup the statusPanel the duration of the run, a progress object would be better
      //jiggle.initStatusPanelForWork(THREAD_SOLVING_MAG);  doing call below instead -aww 06/19/2006
      jiggle.initStatusGraphicsForThread("Solving from Waveforms", THREAD_SOLVING_MAG);

      worker.start();  //required for SwingWorker 3

      return false; // don't know the result
    }

    private class MagEngSwingWorker extends org.trinet.util.SwingWorker {

        MagnitudeEngineIF magEng;
        final Solution sol;
        final WorkerStatusFrame workStatus = new WorkerStatusFrame();
        Magnitude newMag = null;
        String magType = null;

        //public MagEngSwingWorker(MagnitudeEngineIF magEng, Solution sol) {
        public MagEngSwingWorker(String magType, Solution sol) {
          super(magType+"MagEngSwingWorkerWFLoad");
          //this.magEng = magEng;
          this.magType = magType;
          this.sol = sol;
        }

        // NO GRAPHICS CALLS, HERE UNLESS by SwingUtilities.invokeLater(...)
        public Object construct() {
          Thread[] tarray = new Thread[25];
          int tcnt = 0;
          String otherMagType = ""; 
          Thread myThread = Thread.currentThread();
          //System.out.println("Current thread name: " + myThread.getName());
          Outer:
          while (true) {
              tcnt = Thread.enumerate(tarray);
              for (int ii=0; ii<tcnt; ii++) {
                otherMagType = "";
                //System.out.println("MagEngSwingWorker Thread enumerate#" + ii + " name: " + tarray[ii].getName());
                if (tarray[ii].getName().endsWith("MagEngSwingWorkerWFLoad")) {
                    if (!tarray[ii].getName().startsWith(magType))  {
                        otherMagType = tarray[ii].getName().substring(0,2);
                        if (!tarray[ii].isAlive() && activeWfMagCalcType.equals(otherMagType)) { 
                            break Outer; // old active one dead
                        }
                        //System.out.println("JSSD MagEngineSwingWorker set otherMagType to: " + otherMagType);
                        break; // 1 or more mag types active, wait and try again
                    }
                    else if (myThread != tarray[ii]) {
                        System.out.println("INFO: MagEngSwingWorker Interrupting executing thread named: " + tarray[ii].getName());
                        tarray[ii].interrupt();
                        break Outer; // same type restart new thread
                    }
                }
              }
              if (otherMagType == "") break; // Outer, no other mag types

              // Wait
              try {
                if (debug)
                  System.out.println("DEBUG: Still doing wf mag calc " +otherMagType+ ": " +magType+ " thread is waiting for other thread to end....");
                myThread.sleep(200l);
              }
              catch (InterruptedException ex) {
                  return false;
              }
          }

          activeWfMagCalcType = magType;

          System.out.println("INFO: MagEngSwingWorker initMagEngineForType: " + magType);
          magEng = initMagEngineForType(sol, magType); 
          if (magEng == null || ! magEng.isEngineValid()) {
            SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        reportStatusMessage("Magnitude Engine Initialization Failed: Check data or properties");
                    }
                }
            );
            return null;
          }

          //
          //Thread[] tarray = new Thread[25];
          //int tcnt = Thread.enumerate(tarray);
          //for (int ii=0;ii<tcnt;ii++) {
          //    System.out.println("MagEngThread " + ii + " name: " + tarray[ii].getName());
          //}
          //

          workStatus.setBeep(jiggle.beep);
          // Create a popup for the duration of the run, a progress object would be better
          StringBuffer sb = new StringBuffer(132);
          sb.append("Calculation of ").append("M").append(magEng.getMagMethod().getMagnitudeTypeString());
          workStatus.pop("Magnitude", sb.toString(), true);
          sb.append(" for event ").append(sol.getId());
          sb.append(" STARTED at ").append(new DateTime().toString()); // UTC time -aww 2008/02/10
          System.out.println(sb.toString());
          //
          // Blow away the existing reading data associated with the solution,
          // otherwise repeated calls will accumulate copies of the same data!
          // Could instead try to do list additions by invoking addOrReplace - aww
          //
          //sol.getListFor(magEng.getMagMethod().getReadingClass()).clear(); // let engine clear it ? 
          //
          if (debug) {
            System.out.println("Jiggle DEBUG Mag calc type: "+ magEng.getMagMethod().getMethodName()
                                 +" from scratch for: "+sol.toString());
          }
          //xxx THE MagMethod accesses WaveClient, conflicts with scroller load, do we need a separate client connection ?
          jiggle.mv.wfvList.stopCacheManager(); // test to see if fewer "bad" waveserver loads -aww 2010/08/06
          //xxx

          List wfList = null;
          if ( jiggle.getProperties().getBoolean("addEHGFilterButton") ) { //  -aww 2014/04/26
              wfList = (java.util.List) jiggle.getWFScroller().getWFGroupPanel().getWFPanelList().getWfList(); //aww -2014/04/24
          }
          else {
              wfList = (java.util.List) jiggle.mv.getWaveformList();
          }

          Magnitude oldPrefMag = sol.getPrefMagOfType(magEng.getMagMethod().getMagnitudeTypeString().substring(1).toLowerCase()); // for undo ?
          JasiReadingList oldMagReadings = null;
          if (oldPrefMag != null)
              oldMagReadings = (JasiReadingList) oldPrefMag.getListFor(magEng.getMagMethod().getReadingClass()); // undo ? 

          newMag = (magEng.solveFromWaveforms(sol, wfList)) ? magEng.getSummaryMagnitude() : null;
          // How do we know if returned mag it one's any good, should
          // ask operator before setting as preferred with popup dialog??
          // See setting setMakePreferredMag(true) in engine configuration method
          // or via setting it via engine config properties
          // below code forces prefMag thus overriding makePreferredMag = false in engine props
          // should use engine settings to set preferred, if overriden here, how do we know it valid?
          //if (newMag != null) sol.magnitude = newMag; // NO! doesn't update mag list set below -aww
          //if (newMag != null) sol.setPreferredMagnitude(newMag); // should we do this here see text how...
          if (newMag != null) sol.setPrefMagOfType(newMag); // set "preferred" of magtype 04/28/2005
          // NOTE: Only enable block below for troubleshooting failure to see generated readings,
          // but do not commit event, since the failed prefmag of thistype will be stale (corrupted) 
          /* removed 2008/01/30 -aww
          else {
            Magnitude prefMag = sol.getPrefMagOfType(magEng.getMagMethod().getMagnitudeTypeString());
            if (prefMag != null) {
              prefMag.getReadingList().clear();
              //prefMag.fastAssociateAll(magEng.getLastNewMag().getReadingList()); // assign causes exception 
              prefMag.associateAll(magEng.getLastNewMag().getReadingList()); // replace above -aww 2008/01/30
            }
          }
          */

          Magnitude eprefmag = sol.getPreferredMagnitude();
          if (debug) {
            System.out.println("Jiggle DEBUG selected solution mag calc: "+ sol.toNeatString());
            System.out.println( (eprefmag == null) ?
              " Mag Calculation failed?  Solution has no preferred magnitude" : "Event Preferred Magnitude:");
            if (eprefmag != null) System.out.println(eprefmag.neatDump());
          }
          // 
          Magnitude mlrMag = null;
          if (jiggle.getProperties().getBoolean("mlrMagOptionEnabled")) {
            if ( newMag != null && magType.endsWith("l") ) {
              Object mm = magEng.getMagMethod();
              if ( mm instanceof MlMagMethod ) {
                  mlrMag = ((MlMagMethod)mm).createMlrMag(newMag);
                  if (mlrMag != null) sol.setPrefMagOfType(mlrMag);
              }
            }
          }
          // set the newMag to the event preferred if no event preferred
          if (eprefmag == null) {
            if (newMag != null) {
              // set the mlrMag to the event preferred if it exists
              if (mlrMag != null) {
                sol.setPreferredMagnitude( (newMag.getPriority() >= mlrMag.getPriority()) ? newMag : mlrMag );
              }
              else {
                sol.setPreferredMagnitude(newMag);
              }
            }
          }
          // set non-null mlrMag to the event preferred, if current preferred is of type ML
          else if (eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) && mlrMag != null) {
              if (mlrMag.getPriority() >= eprefmag.getPriority()) sol.setPreferredMagnitude(mlrMag);
          }
          else if ( magType.endsWith("l") && eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) && mlrMag == null) {
              // override old event preferred Mlr with new Ml - aww 10/18/2016
              eprefmag.setDeleteFlag(true);
              sol.setPreferredMagnitude(newMag);
          }

 // Removed below logic, magEng method should do the association since newMag may be invalid. aww
          // add new data to MasterView's list ?
          // if (newMag != null) {
          //   JasiReadingListIF jrl = newMag.getReadingList();
          //   sol.getListFor(jrl).addAll(jrl);
          // }

          Class aClass = magEng.getMagMethod().getReadingClass();
          if ( aClass == org.trinet.jasi.Coda.class || aClass == org.trinet.jasi.coda.CodaList.class) {
              if (oldMagReadings != null && oldMagReadings.size() > 0) jiggle.mv.addToCodaUndoList(oldMagReadings, true); // undo?
              //else jiggle.mv.coUndoList.clear(false); // reset undo here ? or save 
          }
          else if ( aClass == org.trinet.jasi.Amplitude.class || aClass == org.trinet.jasi.AmpList.class) {
              if (oldMagReadings != null && oldMagReadings.size() > 0) jiggle.mv.addToAmpUndoList(oldMagReadings, true); // undo?
              //else jiggle.mv.amUndoList.clear(false); // reset undo here ? or save 
          }

          //WFView lists updates done via a list listener to solutions reading list with prefmag assoc
          // refresh to purge readings not associated with new mag -aww 2008/02/26
          jiggle.mv.updateLocalLists(aClass);

          return null;
        }

        //Runs on the event-dispatching thread, so graphics calls OK.
        public void finished() {
          jiggle.statusPanel.clearOnlyOnMatch(THREAD_SOLVING_MAG);
          workStatus.unpop();
          if ( magEng.success()) {
            solveSuccess = true;
          }
          else {
            solveSuccess = false;
            // get status message from Magnitude engine for display in text area
            reportMessage("Magnitude Engine Failed: Check data", magEng.getStatusString());
          }
          StringBuffer sb = new StringBuffer(132);
          sb.append("Calculation of ").append("M").append(magEng.getMagMethod().getMagnitudeTypeString());
          sb.append(" for event ").append(sol.getId());
          sb.append(" ENDED at ").append(new DateTime().toString()).append("\n"); // UTC time -aww 2008/02/10
          System.out.println(sb.toString());
          jiggle.updateTextViews();  // could invoke via a SOLVED PropertyChangeEvent listener in Jiggle
          if (newMag != null ) jiggle.updateMagTab(newMag.getTypeString());
          //if (newMag != null) jiggle.setMagTabByType(newMag.getTypeString()); // -aww 2008/03/03
          jiggle.selectTab(Jiggle.TAB_MAGNITUDE);
          jiggle.resetStatusGraphicsForThread();  // Added 06/19/2006 -aww
          SwingUtilities.invokeLater(new Runnable() {
              public void run() {
                  activeWfMagCalcType = "";
              }
          });
        }
    } //end of MagEngSwingWorker class

} // end of JiggleSolutionSoverDelegate class

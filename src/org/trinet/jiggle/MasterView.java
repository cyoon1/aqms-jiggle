package org.trinet.jiggle;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeEvent;
import java.util.*;

import javax.swing.*;            // JFC "swing" library
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.locationengines.*;

/**
 * This class is the Big Data View.
 * It is primarily a list of WFViews that define a
 * "view" of network data. Note that this is a REQUESTED view;
 * some requested channels or time windows may contain no data.
 * This also contains a list of Solutions that are associated with phases
 * that appear in the view.
 * The MasterView timeSpan will be expanded to include any added objects.<p>
 *
 * The WFViews that make up the MasterView are determined by the ChannelTimeWindowModel.
 * The default model is DataSourceChannelTimeModel which gets its list of channels
 * and time windows from those associated with the event at the DataSource.
 *
 *  @see: ChannelTimeWindowModel()
 *  @see: DataSourceChannelTimeModel()
 *
 */
/*
    Removed all notions of what's "selected" from this class to keep it pure. What
    Solution, WFView, Phase, etc. is currently selected is meaningless for many
    uses of MasterView and, when needed, is maintained by SelectionModel classes.
 */

public class MasterView implements ChangeListener {

    private Object myOwner = null;

    // For viewing spectral amps on waveform - for troubleshooting -  enable via jiggle property "loadSpectralAmps=true"
    public boolean loadSpectralAmps = false;
    public boolean loadPeakGroundAmps = false;

    // flag for addSolution to load eventprefmag by default
    public boolean loadPrefMags = true;

    protected PhaseList phUndoList = new PhaseList(128);
    protected AmpList amUndoList = new AmpList(128);
    protected CodaList coUndoList = new CodaList(128);

    //boolean wfCacheAllWf2Disk = false; // MACBETH asked for auto wf file caching

    /** Property to indicate how phases of same type (P, S) are replaced by Sta or Channel.
     * Default is by Channel.
     * */
    public boolean replacePhasesByStaType = false;

    protected String badChannelListName = null;

    /** The model for which WFPanel and :. which channel is selected. */
    public SelectedWFViewModel  masterWFViewModel = new SelectedWFViewModel();
    /** The model for the zoomed waveform window that is selected. */
    public SelectedWFWindowModel masterWFWindowModel = new SelectedWFWindowModel();
    /** The model for the scrolling window that is selected. */
//    public SelectedWFWindowModel masterWFScrollModel = new SelectedWFWindowModel();

    /** The total time span bounding this view */
    public TimeSpan timeSpan = new TimeSpan();

    /** MVC model for list and selected solution */
//    public ActiveSolutionList solList = new ActiveSolutionList();
    public SolutionList solList = new SolutionList();

    /** Set true if you want to retreive all phases */
    public boolean phaseRetrieveFlag = true;

    /** Default value for stripping residuals. */
    double residualStripValue = 1.0;

    /** Default value for stripping amp residuals. */
    double residualAmpStripValue = 1.0;

    /** Default value for stripping coda residuals. */
    double residualCodaStripValue = 1.0;

    /** Paint WFPanel time scale red if clock quality is worse than this value,
     *  0.0 -> 1.0 */
    double clockQualityThreshold = 0.5;

    /** Set true if you want to retreive all amplitudes */
    public boolean ampRetrieveFlag = true;

    /** Set true if you want to retreive all coda */
    public boolean codaRetrieveFlag = true;

    /** List of WFViews */
    public WFViewList wfvList = new WFViewList();

    protected ChannelTimeWindowModel defaultChannelTimeWindowModel =
                                          new DataSourceChannelTimeModel();

    /** Keep individual WFView lists in synch with the master lists. */
    JasiReadingListModel jasiReadingListModel = new JasiReadingListModel(wfvList);

/** Set true if you want to retreive all waveforms */
    boolean wfRetrieveFlag = true;

/** If true, load Channel data (lat/lon) for each WFView */
    boolean loadChannelData = true;

    public static final String NEW_SLIST_PROPNAME = "newSolList";
    public static final String NEW_WFVIEW_PROPNAME = "newWFView";

    /* Enumerate load mode options */
    public static final int LoadAllInForeground = 0;
    public static final int LoadAllInBackground = 1;
    public static final int LoadNone            = 2;
    public static final int Cache               = 3;
    public static final int LimitNumber         = 4;
    public static final int ModeCount           = 4;


    /** Set waveform default load mode */
    int waveformLoadMode = LoadAllInBackground;

/** Set true if you want to line up the waveforms by time. If true the viewBox of
 *  all the WFView's will be set to the max/min times of the whole data set */
//    boolean timeAlign = true;

    int AlignmentModeCount = 4;
    public static final int AlignOnTime = 0;
    public static final int AlignOnP    = 1;
    public static final int AlignOnS    = 2;
    public static final int AlignOnV    = 3;
    int alignmentMode = AlignOnTime;
    double alignmentVelocity = 6.0;

    /** Include only WFViews that have phase picks */
    boolean includeOnlyWithPhases = false;

/** */
    // 1.3 StatusPanel statusPanel = null;

    /** Set max. waveforms to load */
    int waveformLoadLimit = Integer.MAX_VALUE;

    // output verbosity flag
    static boolean verbose = true;

    // debug flag
    static boolean debug = false;


    // access runtime information
    //    Runtime runtime = Runtime.getRuntime();
    //

    private PropertyChangeSupport propChangeSupport = null;
  
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(listener);
    }
    public void addPropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(propName, listener);
    }
    public void firePropertyChange(PropertyChangeEvent evt) {
        propChangeSupport.firePropertyChange(evt);
    }
    public void firePropertyChange(String propName, boolean oldValue, boolean newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, int oldValue, int newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, Object oldValue, Object newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
     /* jdk 1.4 only
    public PropertyChangeListener[] getPropertyChangeListeners() {
        return propChangeSupport.getPropertyChangeListeners();
    }
    public PropertyChangeListener[] getPropertyChangeListeners(String propName) {
        return propChangeSupport.getPropertyChangeListeners(propName);
    }
    */
    public boolean hasListeners(String propName) {
        return propChangeSupport.hasListeners(propName);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(propName, listener);
    }
  
    public void setOwner(Object owner) {
        myOwner = owner;
    } 
    public Object getOwner() {
        return myOwner;
    } 

    public void setDebug(boolean tf) {
        debug = tf;
        if (debug) verbose = true;
        if ( defaultChannelTimeWindowModel != null) defaultChannelTimeWindowModel.setDebug(debug); 
    }

/**
 * Default constructor
 */
  public MasterView() {

      propChangeSupport = new PropertyChangeSupport(this);
      // give all WFViews a reference to the MasterView via a static member
      WFView.setMasterView(this);
      masterWFViewModel.addChangeListener(this);
  }

 /**
 * Default constructor with statusPanel reference.
 *
 * @param ts - a StatusPanel where status updates will go
 * @see StatusPanel()
 */
// 1.3  public MasterView(StatusPanel sp) {
// 1.3       this();
// 1.3       setStatusPanel(sp);
// 1.3 }

/**
 * Constructor sets initial size of timeSpan
 *
 * @param ts - a TimeSpan object
 * @see TimeSpan
 */
  public MasterView(TimeSpan ts ) {

      this();

      timeSpan = ts;

  }

/**
 * Build a master view for this 'evid'. In other words, the view is based on one
 * event as found in the dbase.  The view's timeSpan will be defined to include
 * all the waveforms and phases associated with this evid.  Other phases in the
 * time span associated with another evid (or no evid) will not be included.  */
  public MasterView(long id) {

      this();

      defineByDataSource(id);
  }

/**
 * Build a master view for this Solution based on this event as found in the
 * DataSource.  The view's timeSpan will be defined to include all the waveforms
 * and phases associated with this evid.  Other phases in the time span
 * associated with another evid (or no evid) will not be included.  */
  public MasterView(Solution sol) {

      this();

      defineByDataSource(sol);
  }

    /**
     * Build a MasterView based on this solution containing stations within the
     * parameter bounds and with waveforms with the given timespan. The view is
     * NOT based on waveforms saved in the DataSource. Rather Waveform objects
     * are created based on the TimeSpan given. These are effectively 'requests'
     * that may or may not be successfully fulfilled when a call to
     * loadWaveforms() is made. The DataSource will be searced for the TimeSpan
     * specified to find phases, amps, etc.  <p>
     *
     * The list will end at either the  distance cutoff or the station count,
     * whichever comes first. For example, assume there are 50 channels within
     * 100 km of the epicenter; if distCutoff=100 & maxCount=20, only the
     * nearest 20 channels will be included. If distCutoff=100 & maxCount=200,
     * only the 50 stations within 100 km will be include. The station list
     * comes from the channel list in * the Channel class.
     *
     * @see: defineByParameters()*/

    public MasterView(Solution sol, double distCutoff, int maxCount, TimeSpan ts) {

      this();

      defineByParameters(sol, distCutoff,maxCount, ts);
    }

    /**
     * Build a MasterView based on this solution ID containing stations within the
     * parameter bounds and with waveforms with the given timespan. The view is
     * NOT based on waveforms saved in the DataSource. Rather Waveform objects
     * are created based on the TimeSpan given. These are effectively 'requests'
     * that may or may not be successfully fulfilled when a call to
     * loadWaveforms() is made. The DataSource will be searced for the TimeSpan
     * specified to find phases, amps, etc.  <p>
     *
     * The list will end at either the  distance cutoff or the station count,
     * whichever comes first. For example, assume there are 50 channels within
     * 100 km of the epicenter; if distCutoff=100 & maxCount=20, only the
     * nearest 20 channels will be included. If distCutoff=100 & maxCount=200,
     * only the 50 stations within 100 km will be include. The station list
     * comes from the channel list in * the Channel class.
     *
     * @see: defineByParameters()*/

    public MasterView(long id, double distCutoff, int maxCount, TimeSpan ts) {

        this();

        defineByParameters(id, distCutoff, maxCount, ts);

    }

    /** Create MasterView defining time window as 'preOT' seconds before origin time
     * and 'postOT' seconds after origin time. The Total window size is preOT+postOT. */
    public MasterView(Solution sol, double distCutoff, int maxCount,
                       double preOT, double postOT) {

        this();

        defineByParameters(sol, distCutoff, maxCount, preOT, postOT);

    }

    /** Create MasterView defining time window as 'preOT' seconds before origin time
     * and 'postOT' seconds after origin time. The Total window size is preOT+postOT. */
    public MasterView(long evid, double distCutoff, int maxCount,
                       double preOT, double postOT) {

        this();

        defineByParameters(evid, distCutoff, maxCount, new TimeSpan(preOT, postOT));

    }

/** Set the statusPanel where we should repor progress. */
//1.3  public void setStatusPanel(StatusPanel sp) {
//1.3       statusPanel = sp;
//1.3  }

/** Report to statusPanel */
  void report(String msg, int val) {
       //1.3 if (statusPanel == null) return;
       //doesn't work: statusPanel.setProgressBarValue(val, msg);
       System.out.println("MV: " + msg + " at " + new DateTime().toString()); // UTC time now - aww 2008/02/10
  }
  void report(String msg) {
       report(msg, 0);   // sets status bar to min value
  }
  void report(int val) {
       //1.3 if (statusPanel == null) return;
       //doesn't work: statusPanel.setProgressBarValue(val);
       //1.3 statusPanel.validate();  // redundant???
  }

  /** Make a Solution from scratch and add it to the solList. Returns the
  * new solution. */
  public Solution createSolution() {
     Solution sol = Solution.create();
     addSolution(sol);
     return sol;
  }

  /** Add solution to the master view. Will not add again if it's already in the list.
   * Fires solList change event. Adds reading list change listeners. Returns true
   * if Solution was added, false if not. Also, loads the event's prefmag. 
   * @see: addSolution(Solution sol, boolean loadPrefMags)*/
    public boolean addSolution(Solution sol) {
        return addSolution(sol, loadPrefMags, true);
    }
    public boolean addSolution(Solution sol, boolean loadPrefMags) {
        return addSolution(sol, loadPrefMags, true);
    }

  /** Add solution to the master view. Will not add again if it's already in the list.
  * Fires solList change event. Adds reading list change listeners. Returns true
  * if Solution was added, false if not.<p>
  * If loadPrefMags is true also, loads the event's prefmag. */
    public boolean addSolution(Solution sol, boolean loadPrefMags, boolean notify) {
      if (sol == null) return false; // short-circuit no-op here -aww
      // Note if input sol has same id as an exising instance of a Solution
      // in list but is itself a different instance you have duplication.
      // To implement "replacement" using instead addOrReplacedById(Sol) 
      // the reading list's ListDataStateListener should be removed from
      // the replaced instance if not identical to the input sol and if it's 
      // the selected Solution, its replacement should be set as list selection.
      //  - aww 11/01/05
      if ( solList.add(sol, notify) ) {
        // added in proper time order and notified SolutionList observers
        //
        //NOTE: not sure where loading solution prefmags belongs! -aww
        if (loadPrefMags) loadPrefMagsFor(sol); // 11/11/04 aww temp test here
        //
        // add listeners to keep individual WFView lists in synch -aww
        sol.phaseList.addListDataStateListener(jasiReadingListModel);
        sol.ampList.addListDataStateListener(jasiReadingListModel);
        sol.codaList.addListDataStateListener(jasiReadingListModel);
        return true;
      }
      return false;
    }

    // Other objects (e.g. map panels) may need to be aware of list changes
    public void addSolutionListListener(ListDataStateListener ll) {
        if (solList != null  && ll != null) {
            solList.removeListDataStateListener(ll);
            solList.addListDataStateListener(ll);
        }
    }
    // Other objects may no longer need to be aware of list changes
    public void removeSolutionListListener(ListDataStateListener ll) {
        if (solList != null && ll != null) solList.removeListDataStateListener(ll);
    }
    /* Also need to add logic to invoke WFPanel update for realignment if not "time" alignment mode and selected solution is relocated !!!
    ListDataStateAdapter myLDSL = new ListDataStateAdapter() {
        public void stateChanged(ListDataStateEvent e) {
            if (e.getSource() == getSolutionList()) {
                if (e.getStateChange().getStateName().equals("selected") ) {
                    if (getAlignmentMode() != AlignOnTime)
                }
            }
        }
    };
    */

    private void loadPrefMagsFor(Solution sol) {
        if (debug) report("DEBUG: loadPrefMagsFor now loading prefmags...");
        sol.loadPrefMags(); // test added here to get prefMags - aww 10/19/2004
        // TEMPORARY fix - stuff "preferred" mag into map; SQL exception when
        // creating Solution when missing mag calibration or prefmag tables.
        Iterator iter = sol.getPrefMags().iterator();
        Magnitude prefMag = null;
        while (iter.hasNext()) {
            prefMag = (Magnitude) iter.next();
            if (prefMag.getReadingsCount() == 0) {
                boolean stale = prefMag.isStale();
                boolean needsCommit = prefMag.getNeedsCommit();
                if ((prefMag.isAmpMag() && ampRetrieveFlag) || (prefMag.isCodaMag() && codaRetrieveFlag)) {
                    prefMag.loadReadingList();
                    //prefMag.getReadingList().insureLatLonZInfo(sol.getDateTime()); // wrapper that just does below call...
                    prefMag.getReadingList().matchChannelsWithDataSource(sol.getDateTime()); // aww test 2014/08/14
                }
                prefMag.setStale(stale); // assume no changed readings in db, 01/24/2005 test aww
                prefMag.setNeedsCommit(needsCommit); // assumed to be false for those from db
            }
        }
        if (debug) report("DEBUG: loadPrefMagsFor done ...");
    }

/**
 * Remove this solution and all associated data from this MasterView. Does NOT
 * delete it from the data source. Does NOT set delete flags in the solution.
 */
public void removeSolution(Solution sol) {

    if (sol == null) return;

    //Stop wf cache manager if this sol was selected. */
    if (sol == getSelectedSolution()) {
       wfvList.stopCacheManager();
    }

    // remove listeners else you have dangling references
    // used to be ChangeListener aww
    sol.phaseList.removeListDataStateListener(jasiReadingListModel); // aww
    sol.ampList.removeListDataStateListener(jasiReadingListModel); // aww
    sol.codaList.removeListDataStateListener(jasiReadingListModel); // aww

    // remove solution
    solList.remove(sol);

    // Re-make a local phase and amp lists for each WFView to reflect change.
    makeLocalLists();
}
/** Remove all solutions for the MasterView's solutionList. */
public void clearSolutionList() {

    // Stop wf cache manager if there is one running
    wfvList.stopCacheManager();

    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
           // remove listeners else you have dangling references
           // used to be ChangeListener aww
          sol[i].phaseList.removeListDataStateListener(jasiReadingListModel); // aww
          sol[i].ampList.removeListDataStateListener(jasiReadingListModel); // aww
          sol[i].codaList.removeListDataStateListener(jasiReadingListModel); // aww
     }

     SolutionList oldSL = solList;
     solList = new SolutionList();
     firePropertyChange(NEW_SLIST_PROPNAME, oldSL, solList);

     // Re-make a local phase and amp lists for each WFView to reflect change.
     makeLocalLists();
}
  /** Return the selected solution. */
  public Solution getSelectedSolution() {
    return (Solution) solList.getSelected();
  }

  /** Return the selected solution. */
  public boolean setSelectedSolution(Solution solution) {
      // Solution first must be added to solution list as a separate action 
      // using method addSolution(sol) (not solList.add(sol))  - aww 11/01/05
      return solList.setSelected(solution);
  }
    /**
     * Build a MasterView based on this solution containing stations within the
     * parameter bounds and with waveforms with the given timespan. The view is
     * NOT based on waveforms saved in the DataSource. Rather Waveform objects
     * are created based on the TimeSpan given. These are effectively 'requests'
     * that may or may not be successfully fulfilled when a call to
     * loadWaveforms() is made. The DataSource will be searced for the TimeSpan
     * specified to find phases, amps, etc.  <p>
     *
     * The list will end at either the  distance cutoff or the station count,
     * whichever comes first. For example, assume there are 50 channels within
     * 100 km of the epicenter; if distCutoff=100 & maxCount=20, only the
     * nearest 20 channels will be included. If distCutoff=100 & maxCount=200,
     * only the 50 stations within 100 km will be include. The station list
     * comes from the c
     */
    public void defineByParameters(Solution sol, double distCutoff,
                                               int maxCount, TimeSpan ts) {

        if (sol == null) return;

        // adds sol to list in proper time order, notifies observers
        addSolution(sol);
        setSelectedSolution(sol);

        makeWFViewList(distCutoff, maxCount, ts);

        // load Phases, Amps, * Codas
        loadReadings();

        // copy lat/lon info from Channel class to WFViews
        loadChannelData();

        // calc distances and sort by dist from selected Solution
        distanceSort(sol);

        // Time align the waveforms by setting all their viewSpans equal to the
        // master View's timeSpan. Obviously must do AFTER the timeSpan is set
        // by all data in loop above.
        alignViews();

        // Get timeseries if flag is set
        loadWaveforms();

     }

     /**
     * Load all readings; Phases, Amps & Codas IF the loader flag of each is set
     */
    protected void loadReadings() {
        if (debug) report("DEBUG loading readings...");

        loadPhases();

       // load amps for magnitude of the sols
        loadAmplitudes();

       // load amps for magnitude of the sols
        loadCodas();

       // Make a local phase, amp and coda lists for each WFView.
        makeLocalLists();

    }



    /**
     * Build a MasterView based on this solution containing stations within the
     * parameter bounds and with waveforms with timespan defined as a window
     * 'preOT' seconds before and 'postOT' seconds after the origin time of the
     * solution. The view is NOT based on waveforms saved in the
     * DataSource. Rather Waveform objects are created based on the TimeSpan
     * given. These are effectively 'requests' that may or may not be
     * successfully fulfilled when a call to loadWaveforms() is made. The
     * DataSource will be searced for the TimeSpan specified to find phases,
     * amps, etc.  <p>
     *
     * The list will end at either the  distance cutoff or the station count,
     * whichever comes first. For example, assume there are 50 channels within
     * 100 km of the epicenter; if distCutoff=100 & maxCount=20, only the
     * nearest 20 channels will be included. If distCutoff=100 & maxCount=200,
     * only the 50 stations within 100 km will be include. The station list
     * comes from the Channel class. */
    public void defineByParameters(Solution sol, double distCutoff, int maxCount,
                                   double preOT, double postOT) {

        if (sol != null ) {

            double ot   = sol.datetime.doubleValue();
            TimeSpan ts = new TimeSpan (ot - preOT, ot + postOT);

            defineByParameters(sol, distCutoff, maxCount, ts);
        }
    }
    /**
     * Build a MasterView based on this solution ID containing stations within
     * the parameter bounds and with waveforms with this timespan. The view is
     * NOT based on waveforms saved in the DataSource. Rather Waveform objects
     * are created based on the TimeSpan given. These are effectively 'requests'
     * that may or may not be successfully fulfilled when a call to
     * loadWaveforms() is made. The DataSource will be searced for the TimeSpan
     * specified to find phases, amps, etc.  <p>
     *
     * The list will end at either the  distance cutoff or the station count,
     * whichever comes first. For example, assume there are 50 channels within
     * 100 km of the epicenter; if distCutoff=100 & maxCount=20, only the
     * nearest 20 channels will be included. If distCutoff=100 & maxCount=200,
     * only the 50 stations within 100 km will be include. The station list
     * comes from the Channel class. */

    public void defineByParameters(long evid, double distCutoff, int maxCount,
                                   TimeSpan ts) {

        Solution sol = Solution.create().getById(evid);

        if (sol != null ) defineByParameters(sol, distCutoff, maxCount, ts);
    }


    /** Define MasterView to include all channels with readings; phases, amps or
     * codas.
     * This is used to construct a MasterView that does
     * not depend on the waveforms windows that were assigned to this event by the
     * data source. It does NOT load time series. */

    public void defineByReadings(Solution sol,
                                   double distCutoff, int maxCount,
                                   TimeSpan ts) {

        if (sol == null) return;

        // adds sol to list in proper time order, notifies observers
        addSolution(sol);
        setSelectedSolution(sol);

        // Set ALL views + master view to the time window passed in the arg
        setAllViewSpans(ts);

        loadReadings();

        // create a WFView for each channel with a reading
        insureReadingsHaveViews(true);

        // match channels info
        loadChannelData();

        // calc distances and sort by dist from selected Solution
        distanceSort();

        // toss out-of-bounds views
        wfvList.trim(maxCount);
        wfvList.trim(distCutoff);

        // create waveform time windows from scratch NOT from the data source
     // (time-series are NOT loaded yet)
        WFView wfv[] = wfvList.getArray();

        for ( int i=0; i < wfv.length; i++) {

            Waveform wf = AbstractWaveform.createDefaultWaveform();
            wf.setChannelObj(wfv[i].chan);
            wf.setTimeSpan(ts);

            wfv[i].setWaveform( wf );

        }

    }


    /** Define MasterView to include all channels with phases.
     * This is used to construct a MasterView that does
     * not depend on the waveforms windows that were assigned to this event by the
     * data source. It does NOT load time series. */

    public void defineByPhaseList(Solution sol,
                                   double distCutoff, int maxCount,
                                   TimeSpan ts) {

        if (sol == null) return;

        // adds sol to list in proper time order, notifies observers
        addSolution(sol, false);  // false = don't load the prefmag and amps
        setSelectedSolution(sol);

        // Set ALL views + master view to the time window passed in the arg
        setAllViewSpans(ts);

        loadPhases();

        // create a WFView for each channel with a reading
        //insureReadingsHaveViews(true);
        insureReadingsHaveViews(sol, sol.phaseList, true);
        // match channels info
        loadChannelData();

        // calc distances and sort by dist from selected Solution
        distanceSort();

        // toss out-of-bounds views
        wfvList.trim(maxCount);
        //wfvList.trim(distCutoff);

        // create waveform time windows from scratch NOT from the data source
     // (time-series are NOT loaded yet)
        WFView wfv[] = wfvList.getArray();

        for ( int i=0; i < wfv.length; i++) {

            if (wfv[i].getDistance() > distCutoff) break;  // dist cutoff exceeded
         
            Waveform wf = AbstractWaveform.createDefaultWaveform();
            wf.setChannelObj(wfv[i].chan);
            wf.setTimeSpan(ts);
    
            wfv[i].setWaveform( wf );

        }

    }


/**
 * Create a MasterView based on the Waveforms associated with this Solution in
 * the DataSource. Load the phase, amp, etc.  */
/* public boolean defineByChannelTimeWindowModel(ChannelTimeWindowModel model,
                                              double distCutoff, int maxCount) {
    if (defineByChannelTimeWindowModel(model)) {

        // toss out-of-bounds views
        wfvList.trim(maxCount);
        wfvList.trim(distCutoff);
     return true;
    }
    return false;
}
*/

/**
 * Set MasterView's default ChannelTimeWindowModel.
 * */
//public void setChannelTimeWindowModel(ChannelTimeWindowModel model, GenericPropertyList props) //
public void setChannelTimeWindowModel(ChannelTimeWindowModel model) {
    defaultChannelTimeWindowModel = model;
}
/**
 * Get MasterView's default ChannelTimeWindowModel.
 * */
public ChannelTimeWindowModel getChannelTimeWindowModel() {
    return defaultChannelTimeWindowModel;
}
/**
 * Create a MasterView based the default ChannelTimeWindowModel and the currently
 * selected Solution. Returns true on success. */
public boolean defineByChannelTimeWindowModel() {
    if (getSelectedSolution() == null) return false;
    return defineByChannelTimeWindowModel(defaultChannelTimeWindowModel,
                                          getSelectedSolution());
}
/**
 * Create a MasterView based this model and event ID number. Loads the phases, amps, etc.
 * The Solution created for this 'evid' will be added to the MasterView and set
 * as the selected Solution.
 * Returns true on success.  */
public boolean defineByChannelTimeWindowModel(ChannelTimeWindowModel model, long evid) {
    Solution sol = Solution.create();
    sol.getById(evid);       // lookup solution in datasource
    if (sol == null) return false;
    addSolution(sol);
    setSelectedSolution(sol);
    return defineByChannelTimeWindowModel(model, sol);
}
/**
 * Create a MasterView based this model and Solution. Loads the phases, amps, etc.
 * Returns true on success. */
public boolean defineByChannelTimeWindowModel(ChannelTimeWindowModel model, Solution sol) {
    model.setSolution(sol);
    return defineByChannelTimeWindowModel(model);
}

/**
 * Create a MasterView based this model. Loads the phases, amps, etc.  Note that
 * the solution must be set for the model.
 * The currently selected solution of the MasterView will be used as the model's
 * Solution.
 * Returns true on success. */
public boolean defineByChannelTimeWindowModel(ChannelTimeWindowModel model) {

    Solution sol = getSelectedSolution();   // MasterView sol is 1st choice
    // Gotta' have a Solution
    if ( sol == null ) {
        // Should you do addSolution(sol) setSelectedSolution(sol) here? -aww 11/01/05 
        sol = model.getSolution();
        addSolution(sol);
        setSelectedSolution(sol);
    }
    if ( sol == null ) return false;
    if (debug) report("DEBUG defineByChannelTimeWindowModel model: " + model.getClass().getName());
    
    report("Getting model waveform list.", 20);
    // Get Waveforms Header Info (time series is NOT loaded yet)
    ArrayList wfa = (ArrayList) model.getWaveformList(sol);

    if (verbose)
      report("Number of available waveforms = "+wfa.size());
    if (wfa != null && wfa.size() > 0) {

    // Create a WFView for each waveform found for this event

        report("Making Waveform views by CTWM.", 0);

     // Clean and reuse WFViewList. Can't just abandon the old reference and
     // instatiate a new one because the Waveform listeners would still have
     // a reference and never garbage collect.
        wfvList.clear();

        // Add these WFViews to this MasterView (would getting an array be faster here?)
        int inc = 100/wfa.size();
        int done = 0;

        for (int i = 0; i<wfa.size(); i++)  {
           this.addWFView(new WFView( (Waveform) wfa.get(i)));

            report(done);
            done += inc;
        }
    } else {
        report("No Waveform views defined.", 0);
    }
    // loads phases, amps and codas
    loadReadings();

    // NOTE:
    // Default ChannelTimeModel includes property to include both
    // waveforms and readings as done by the "insure" method. 
    //
    // PicksOnlyChannelTimeModel creates timespan window using 
    // the predicted TT and not actual loaded reading time, so
    // some picks might lie outside predicted window bounds.
    // This model creates/updates windows for channels with
    // associated waveforms if its "includeDataSourceWf" property
    // is set true.
    //
    // But the DataSourceChannelTimeModel overrides default method
    // behavior, "readings" from database for a Channel without 
    // a Waveform or outside Waveform time window are not even
    // "included" in the masterview, e.g. associator picks w/o wf.
    //
    // Note that deleting "all" readings via GUI clears list of the
    // non-viewable readings, but any "hidden" readings that were
    // loaded from db are still passed to the location/mag methods.
    //
    // Could make below "insure" call an user settable "static" option
    // (e.g. by using a property setting in the Jiggle initialization): 
    //     public static boolean insureReadingsHaveViews = false;  -aww

    // create a WFView for each channel with a reading
    //insureReadingsHaveViews(true); // removed 01/20/2006 by DDG

    // copy lat/lon info from Channel class to WFViews
    loadChannelData();

    // calc distances and sort by dist from selected Solution
    //if (!sol.isEventDbType(EventTypeMap3.TRIGGER)) distanceSort(sol);
    if (!sol.isEventDbType(EventTypeMap3.TRIGGER) || model.getTriggerSortOrder().equalsIgnoreCase("DIST") ) distanceSort(sol); // -aww 2012/02/06

    alignViews();
    
    // Get timeseries if flag is set
    loadWaveforms();

    return true;
}

/**
 * Create a MasterView based this model.
 * The currently selected solution of the MasterView will be used as the model's
 * Solution.
 * Returns true on success. */
public boolean refreshWFViewListByCurrentModel() {
    ChannelTimeWindowModel model = getChannelTimeWindowModel();
    Solution sol = getSelectedSolution();   // MasterView sol is 1st choice
    // Gotta' have a Solution
    if ( sol == null ) {
        // Should you do addSolution(sol) setSelectedSolution(sol) here? -aww 11/01/05 
        sol = model.getSolution();
        addSolution(sol);
        setSelectedSolution(sol);
    }
    if ( sol == null ) return false;
    if (debug) report("DEBUG refreshWFListByModel model: " + model.getClass().getName());
    
    report("Getting model waveform list (refresh).", 20);
    // Get Waveforms Header Info (time series is NOT loaded yet)
    ArrayList wfa = (ArrayList) model.getWaveformList(sol);

    if (verbose)
      report("Number of available waveforms (refresh) = "+wfa.size());
    if (wfa != null && wfa.size() > 0) {

    // Create a WFView for each waveform found for this event

        report("Making Waveform views by CTWM (refresh).", 0);

     // Clean and reuse WFViewList. Can't just abandon the old reference and
     // instatiate a new one because the Waveform listeners would still have
     // a reference and never garbage collect.
        wfvList.clear();

        // Add these WFViews to this MasterView (would getting an array be faster here?)
        int inc = 100/wfa.size();
        int done = 0;

        for (int i = 0; i<wfa.size(); i++)  {
           this.addWFView(new WFView( (Waveform) wfa.get(i)));

            report(done);
            done += inc;
        }
    } else {
        report("No Waveform views defined (refresh).", 0);
    }

    // Re-make a local phase and amp lists for each WFView to reflect change.
    makeLocalLists();

    // copy lat/lon info from Channel class to WFViews
    loadChannelData();
    
    // calc distances and sort by dist from selected Solution
    if (!sol.isEventDbType(EventTypeMap3.TRIGGER) && !model.getTriggerSortOrder().equalsIgnoreCase("PICK") ) distanceSort(sol); // -aww 2012/02/06

    alignViews();

    // Get timeseries if flag is set
    loadWaveforms();

    return true;
}

private void loadChannelData() {
    if (debug) report("DEBUG loadChannelData() ...");
    wfvList.loadChannelData();

    if ( badChannelListName == null || badChannelListName.length() == 0 ) return;

    // Load by channels and selected WFViews - aww 2015/03/20
    Solution sol = getSelectedSolution();
    DateTime dt = ( sol == null ) ? new DateTime() : sol.getDateTime();
    ChannelableList cl = Channel.create().getNamedChannelList(badChannelListName, dt);
    int cnt = cl.size();
    int badCnt = 0;
    if (cnt > 0) {
        WFView wfvBad = null;
        for ( int idx = 0; idx < cnt; idx++) {
            wfvBad = wfvList.get((Channelable)cl.get(idx));
            if (wfvBad != null) {
                wfvBad.setSelected(true);
                badCnt++;
            }
        }
        report("Views flagged with bad channels: " + badCnt);
    }
    else {
        report("No bad channel list");
    }
}

/**
 * Create a MasterView based on a ChannelTimeWindow List. */
public boolean defineByChannelTimeWindowList(Collection ctwList) {

   // Create a WFView for each waveform found for this event
      if (ctwList.size() > 0) {

        // Get Waveforms Header Info (time series is NOT loaded yet)
        ChannelTimeWindow ctw[] = (ChannelTimeWindow[]) ctwList.toArray(new ChannelTimeWindow[0]);

        report("Making Waveform views by CTWL.", 0);

        // start with a clean WFViewList. Used to just make a new make a new wfvList
        // but that killed the Waveform listeners that inform when they are loaded
        // to repaint WFPanel.
        wfvList.clear();

        // Add these WFViews to this MasterView (would getting an array be faster here?)
        int counter = 0;
        int inc = 100/ctw.length;
        int done = 0;

        WFView wfv;
        TimeSpan ts;

        for (int i = 0; i<ctw.length; i++)  {

          ts = new TimeSpan(ctw[i].getStart(), ctw[i].getEnd());
          wfv = new WFView(ctw[i].getChannelObj(), ts);

//           wfv.setWaveform(new Waveform(ctw[i].chan, ts));

           Waveform wf = AbstractWaveform.createDefaultWaveform();
           wf.setChannelObj(ctw[i].getChannelObj());
           wf.setTimeSpan(ts); 

           wfv.setWaveform( wf );

           addWFView(wfv);

           report(done);
           done += inc;
        }
      }

      loadChannelData();

      // Make a local phase and amp lists for each WFView.
      makeLocalLists();

      alignViews();

      // Get timeseries if flag is set
      loadWaveforms();

      return true;
}

/**
 * Create a MasterView based on TriggerChannelTimeWindows list.
 * Trigger times will create pseudo-phases of type ".P4". */
public boolean defineByTriggerChannelTimeWindowList(Collection ctwList) {

      // Create a WFView for each waveform found for this event
      if (ctwList.size() > 0) {

        // Get Waveforms Header Info (time series is NOT loaded yet)
        TriggerChannelTimeWindow ctw[] =
           (TriggerChannelTimeWindow[]) ctwList.toArray(new TriggerChannelTimeWindow[0]);

        report("Making Waveform views by TCTWL.", 0);

        // start with a clean WFViewList. Used to just make a new make a new wfvList
        // but that killed the Waveform listeners that inform when they are loaded
        // to repaint WFPanels.
        wfvList.clear();

        // Add these WFViews to this MasterView (would getting an array be faster here?)
        int counter = 0;
        int inc = 100/ctw.length;
        int done = 0;

        WFView wfv;
        TimeSpan ts;

        for (int i = 0; i<ctw.length; i++)  {

          ts = new TimeSpan(ctw[i].getStart(), ctw[i].getEnd());
          wfv = new WFView(ctw[i].getChannelObj(), ts);

//           wfv.setWaveform(new Waveform(ctw[i].chan, ts));
          Waveform wf = AbstractWaveform.createDefaultWaveform();
          wf.setChannelObj(ctw[i].getChannelObj());
          wf.setTimeSpan(ts);

          wfv.setWaveform( wf );

          addWFView(wfv);

//  Interpret the trigger time as a P-phase so a pick marker will be plotted

          if (ctw[i].triggerTime != 0.0) {
            Phase ph = Phase.create();
            ph.setChannelObj(ctw[i].getChannelObj());
            ph.setTime(ctw[i].triggerTime);
            ph.description = new PhaseDescription("P", "w", ".", 4);
            // undo clear ? addReadingToCurrentSolution(ph, (i==0));
            addReadingToCurrentSolution(ph);
          }

          report(done);
          done += inc;

        }
      }

      loadChannelData();

      // Make a local phase and amp lists for each WFView.
      makeLocalLists();

      alignViews();

      // Get timeseries if flag is set
      loadWaveforms();

      return true;
}



    /**
     * Build a WFViewList based containing channels within the parameters with
     * waveforms with the given timespan. The list will end at either the
     * distance cutoff or the station count, whichever comes first. For example,
     * assume there are 50 channels within 100 km of the epicenter; if
     * distCutoff=100 & maxCount=20, only the nearest 20 channels will be
     * included. If distCutoff=100 & maxCount=200, only the 50 stations within
     * 100 km will be include.  Returns the number of WFViews created.<p>
     *
     * The station list comes from the static channel
     * list in the Channel class. If it is not already loaded it will be loaded with
     * all currently active channels.
     *
@param channelList - a collection of Channel objects that is the set from which
the WFViewList will be made.
@see org.trinet.jasi.Channel()

@param distCutoff - (km) the maximum distance allowed to a station
@param maxCount - the maximum number of channels to return in the list
@param ts - the time-span from which define the WFViews and Waveforms
 */

  public int makeWFViewList(double distCutoff, int maxCount, TimeSpan ts) {
    if (MasterChannelList.isEmpty()) { // load if you must
      MasterChannelList.smartLoad();
    }

    // sort the list by distance from the hypocenter
     MasterChannelList.get().distanceSort(getSelectedSolution());

//    return makeWFViewList(Channel.getList(), distCutoff, maxCount, ts);
    return makeWFViewList(MasterChannelList.get(), distCutoff, maxCount, ts);
  }

    /**
     * Build a WFViewList based containing channels within the parameters with
     * waveforms with the given timespan. The list will end at either the
     * distance cutoff or the station count, whichever comes first. For example,
     * assume there are 50 channels within 100 km of the epicenter; if
     * distCutoff=100 & maxCount=20, only the nearest 20 channels will be
     * included. If distCutoff=100 & maxCount=200, only the 50 stations within
     * 100 km will be include.  Returns the number of
     * WFViews created.<p>
     *
     * You pass in a channelList from which the WFViewList will be selected.

@param channelist - a collection of Channel objects that is the set from which
the WFViewList will be made.
@see org.trinet.jasi.Channel()

@param distCutoff - (km) the maximum distance allowed to a station
@param maxCount - the maximum number of channels to return in the list
@param ts - the time-span from which define the WFViews and Waveforms

    */

public int makeWFViewList(Collection channelList, double distCutoff,
                          int maxCount, TimeSpan ts) {

    // get channel array
    Channel chan [] = new Channel[channelList.size()];
    channelList.toArray(chan);

    // create WFViews for channels within cutoff and maxCount
    WFView wfv;
    int count = 0;
    for ( int i=0; i < chan.length; i++) {
        //if (chan[i].getDistance() > distCutoff) break;  // don't use slant aww 06/11/2004
        if (chan[i].getHorizontalDistance() > distCutoff) break;  // too far, bail // aww 06/11/2004

        wfv = new WFView( chan[i], ts );

//      wfv.setWaveform(new Waveform(chan[i], ts));
        Waveform wf = AbstractWaveform.createDefaultWaveform();
        wf.setChannelObj(chan[i]);
        wf.setTimeSpan(ts);

        wfv.setWaveform( wf );

        addWFView(wfv);

        if (++count >= maxCount) break; // got enough changed to prefix ++ -aww
    }

    return count;
}

/**
 * Create a MasterView and load all the date for a single evid in the dbase.
 * Returns 'true' if successful.
 */
    //TODO: test "error" conditions: no WF's, no phases, etc.
public boolean defineByDataSource(long evid) {
    // Get Solution

    Solution sol = Solution.create().getById(evid);

    if (sol == null) {
      if (verbose)
        report("No Solution matches id# :" +evid);
        return false;
    }
    this.addSolution(sol);
    this.setSelectedSolution(sol);

    return defineByChannelTimeWindowModel(getChannelTimeWindowModel());
    //TEST//return defineByDataSource(sol);

}
/**
 * Create a MasterView and load all the date for a single evid using the current model.
 * Returns 'true' if successful.
 * @see: setChannelTimeWindowModel()
 */
    //TODO: test "error" conditions: no WF's, no phases, etc.
public boolean defineByCurrentModel(long evid) {
    // Get Solution

    Solution sol = Solution.create().getById(evid);

    if (sol == null) {
      if (verbose)
        report("No Solution matches id# :" +evid);
        return false;
    }

    // add to MV and set selected
    this.addSolution(sol);
    this.setSelectedSolution(sol);

    return defineByChannelTimeWindowModel(getChannelTimeWindowModel());
    //TEST//return defineByDataSource(sol);

}
/**
 * Create a MasterView based on the Waveforms associated with this Solution in
 * the DataSource. Load the phase, amp, etc.  */
public boolean defineByDataSource(Solution sol) {

    // TODO: read in other event that fall in the window

    if (sol == null) return false;

    // adds sol to list in proper time order, notifies observers
    addSolution(sol);
    solList.setSelected(sol);
    return defineByChannelTimeWindowModel(getChannelTimeWindowModel());

    /*
    //REMOVED BELOW CODE (see method in above return call)  - aww 12/05/2005
    //below removed to make behavior consistent with defineByDataSource(long)
    report("Getting waveform headers.", 20);

    // [WAVEFORMS] Get Waveforms Header Info (time series is NOT loaded yet)
    ArrayList wfa = (ArrayList) AbstractWaveform.createDefaultWaveform().getBySolution(sol);

    if (verbose)
      report("Number of available waveforms = "+wfa.size());

    // Create a WFView for each waveform found for this event
    if (wfa.size() > 0) {

        report("Making Waveform views.", 0);

        // start with a clean WFViewList. Used to just make a new make a new wfvList
        // but that killed the Waveform listeners that inform when they are loaded
        // to repaint WFPanels.
        wfvList.clear();

        // Add these WFViews to this MasterView (would getting an array be faster here?)
        int counter = 0;
        int inc = 100/wfa.size();
        int done = 0;

        for (int i = 0; i<wfa.size(); i++)  {

            this.addWFView(new WFView( (Waveform) wfa.get(i)));

            report(done);
            done += inc;
        }
    }

    // load amps, phases, codas
    report("Loading parameters...", 0);
    loadReadings();

    // Make sure TimeSpan of WFViews are big enough to show phases.
    // The 'true' means create WFViews for any phases (channels) that don't have them.
    // There can be phases with no waveforms.
    /////////// no latlonz at this point
    if (debug) {
      System.out.println("DEBUG MasterView phaseList b4 insure...Views ---- 0 ----");
      System.out.println(phasesToString()); /// DEBUG!!!!!!!!!!!!!!
    }

    insureReadingsHaveViews(true);

    // copy lat/lon info from Channel class to WFViews
    report("Loading channel data.", 0);

    if (debug) {
      System.out.println("DEBUG MasterView phaseList b4 loadChannelData ---- 1 ----");
      System.out.println(phasesToString()); /// DEBUG!!!!!!!!!!!!!!
    }

    loadChannelData();

    if (debug) {
      System.out.println("DEBUG MasterView phaseList b4 distanceSort ---- 2 ----");
      System.out.println(phasesToString()); /// DEBUG!!!!!!!!!!!!!!
    }

    // calc distances and sort by dist from selected Solution
    distanceSort(sol);

    if (debug) {
      System.out.println("DEBUG MasterView phaseList b4 loadWaveforms ---- 3 ----");
      System.out.println(phasesToString()); /// DEBUG!!!!!!!!!!!!!!
    }

    // [TIMESERIES] Get timeseries if flag is set, needs wfviews and sort
    //  else wf's won't load in sorted order
    loadWaveforms();

    alignViews();

    return true;
    */
  }

    /**
     * Make a local phase and amp lists for each WFView. This speeds some functions.
     */
    public void makeLocalLists() {
        int size = wfvList.size();
        WFView wfv = null;
        for (int i = 0; i<size; i++)  {
            wfv = (WFView) wfvList.get(i);
            wfv.updatePhaseList();
            wfv.updateAmpList();
            wfv.updateCodaList();
        }
    }

    protected final void updateLocalLists(Class aClass) {
        if ( aClass == org.trinet.jasi.Amplitude.class || aClass == org.trinet.jasi.AmpList.class) {
            updateAmpLists();
        }
        else if ( aClass == org.trinet.jasi.Coda.class || aClass == org.trinet.jasi.coda.CodaList.class) {
            updateCodaLists();
        }
        else if ( aClass == org.trinet.jasi.Phase.class || aClass == org.trinet.jasi.PhaseList.class) {
            updatePhaseLists();
        }
    }

    protected final void updatePhaseLists() {
        int size = wfvList.size();
        for (int i = 0; i<size; i++)  {
            ((WFView) wfvList.get(i)).updatePhaseList();
        }
    }
    protected final void updateAmpLists() {
        int size = wfvList.size();
        for (int i = 0; i<size; i++)  {
            ((WFView) wfvList.get(i)).updateAmpList();
        }
    }

    protected final void updateCodaLists() {
        int size = wfvList.size();
        for (int i = 0; i<size; i++)  {
            ((WFView) wfvList.get(i)).updateCodaList();
        }
    }

/** Scan the reading lists for each solution and make sure there is a WFView
 for each reading and that it has a TimeSpan big enough to
include the reading. This insures that each WFView's viewSpan is
big enough to display its readings. Otherwise, they could land before or after available
waveform data. If 'createNewWFViews' is true, create a new WFView for any reading
with no matching WFView.  Thus, there can be phases, amps, etc with no waveforms.<p>
*
* If the masterView's timeSpan is not 'null' the Channel WFView's TimeSpan is
* set to that value. */

private void insureReadingsHaveViews(boolean createNewWFViews) {
    if (debug) report("DEBUG insuring readings have views: " + createNewWFViews); 

    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       insureReadingsHaveViews(sol[i], sol[i].phaseList, createNewWFViews);
       insureReadingsHaveViews(sol[i], sol[i].ampList,   createNewWFViews);
       insureReadingsHaveViews(sol[i], sol[i].codaList,  createNewWFViews);
    }
    // Re-make a local phase and amp lists for each WFView to reflect change.
    makeLocalLists();
}
/** Scan this reading list and make sure there is a WFView
 for each reading and that it has a TimeSpan big enough to
include the readin. This insures that each WFView's viewSpan is
big enough to display its readings. Otherwise, they could land before or after available
waveform data. If 'createNewWFViews' is true, create a new WFView for any reading
with no matching WFView.  Thus, there can be phases, amps, etc with no waveforms.<p>
*
* If the masterView's timeSpan is not 'null' the Channel WFView's TimeSpan is
* set to that value. */
void insureReadingsHaveViews(Solution solution, JasiReadingList list, boolean createNewWFViews) {

     JasiReading[] jr = (JasiReading[]) list.toArray(new JasiReading[0]);
     WFView wfv;
     boolean match = false;
     final double minWindowSize = 10.0; // seconds
     final double halfWindow = minWindowSize/2.0;

     for (int i = 1; i < jr.length; i++) {

        wfv = wfvList.get(jr[i].getChannelObj());

        if (wfv != null) {    // is there already a matching view?
                wfv.viewSpanInclude(jr[i].getTime()); // make sure it includes reading time
        } else {
 //         if (false) {
         if (createNewWFViews) {
                      // show at least minWindowSize
                TimeSpan ts = new TimeSpan(jr[i].getTime() - halfWindow,
                                           jr[i].getTime() + halfWindow);
//               this.addWFView(new WFView(jr[i], getViewSpan() ));  // set viewspan to the masterview's
               wfv = new WFView(jr[i], ts );
               wfv.viewSpan.set(getViewSpan().getStart(), getViewSpan().getEnd());
               wfv.dataSpan.set(jr[i].getTime(), jr[i].getTime());
               this.addWFView(new WFView(jr[i], ts ));

          }
        }
     }
     // Re-make a local phase and amp lists for each WFView to reflect change.
     makeLocalLists();
}

    /**
    * Load the amplitudes for this MasterView. Loads all amps associated with
    * the preferred magnitude of all solutions in view.
    */
    /* This should be done for a time window but they've screwed up the dbase by
    * writing multiple RT system's data into it with no indication of what is primary.
    * I am ignoring "network amplitudes" for now.
    */
// NOTE THAT THESE THE AMP,CODA LOADING  METHODS ARE NOW IDENTICAL, THUS CREATE ONE CALLED loadMagReadings() ?
    public void loadAmplitudes() {
        if (! ampRetrieveFlag) return;
        report("Getting Amp data.", 20);
        Solution sol[] = solList.getArray();

          for (int i = 0; i<sol.length; i++) {
            //sol[i].addAmps (Amplitude.create().getByMagnitude(sol[i].magnitude) ); // aww replaced with below

            //Magnitude prefMag = sol[i].getPreferredMagnitude(); // aww 05/06/2005 removed
            // Do "prefMag" of type to get observations -aww
            Magnitude prefMag = sol[i].getPrefMagOfType(MagTypeIdIF.ML); // aww 05/06/2005 added
            //if (prefMag == null) prefMag = sol[i].getPrefMagOfType(MagTypeIdIF.M?); // what other types to test for?
            if (prefMag == null || ! prefMag.isAmpMag()) return;  // wrong kind of Magnitude

            //NOTE: Associating reading with a magnitude should also update the solution ampList associations
            // Use fastAssociateAll only if you know the ampList is EMPTY, or you first CLEAR(tf) the list,
            // it doesn't check for preexisting matching instances and does a single interval notification:
            // prefMag.fastAssociateAll( (List) Amplitude.create().getByMagnitude(prefMag, null, false) );
            // But if you want to PRESERVE the PRIOR data, then do each, one at a time, for check and notify :
            //Amplitude.create().getByMagnitude(prefMag); // association listener by default should update sol too.
            //prefMag.associate((List) Amplitude.create().getByMagnitude(prefMag, null, false) ); // doesn't clear list
            boolean stale = prefMag.isStale();
            if (prefMag.getReadingList().isEmpty()) { // only load if empty -aww 2010/07/06
                prefMag.loadReadingList(true); // does the associations true, true clears the list first
            }
            if (! prefMag.isPreferred()) { // must then add to solution's list (since no listener update)
              //sol[i].associate(prefMag.getReadingList());  // aww 05/06/2005
              sol[i].addOrReplaceAll(prefMag.getReadingList());  // replaced above to avoid dups, model may have loaded amps - aww 2008/05/14
            }
            prefMag.setStale(stale); // assume no changed readings in db, 01/24/2005 test aww
            prefMag.getReadingList().matchChannelsWithDataSource(sol[i].getDateTime()); // aww test 11/17
          }

        // Can we move this to end of loadAmplitudes ?
        // To view spectral amps on wfpanel for testing:
        if (loadSpectralAmps && getSelectedSolution() != null) {
            //ArrayList aList = (ArrayList) Amplitude.create().getBySolution(getSelectedSolution(), new String[] { "SP.3"}, false);
            ArrayList aList = (ArrayList) Amplitude.create().getBySolution(getSelectedSolution(), new String[] {"SP.3","SP1.0","SP3.0"}, false);
            System.out.println("MV: Loaded spectral amp list size: " + aList.size());
            if (aList != null) {
                AmpList al = getSelectedSolution().getAmpList();
                al.assignAllTo(getSelectedSolution());
                al.fastAddAll(aList);
                MasterChannelList.matchChannelsWithDataSource(al, getSelectedSolution().getDateTime()); 
                al.calcDistances(getSelectedSolution());
                //al.dump();
            }
        }
        if (loadPeakGroundAmps && getSelectedSolution() != null) {
            //ArrayList aList = (ArrayList) Amplitude.create().getBySolution(getSelectedSolution(), new String[] { "SP.3"}, false);
            ArrayList aList = (ArrayList) Amplitude.create().getBySolution(getSelectedSolution(), new String[] {"PGA","PGV","PGD"}, false);
            System.out.println("MV: Loaded peak ground amp list size: " + aList.size());
            if (aList != null) {
                AmpList al = getSelectedSolution().getAmpList();
                al.assignAllTo(getSelectedSolution());
                al.fastAddAll(aList);
                MasterChannelList.matchChannelsWithDataSource(al, getSelectedSolution().getDateTime()); 
                al.calcDistances(getSelectedSolution());
                //al.dump();
            }
        }
 
    }

    /**
     * Load the codas for this MasterView. Loads all codas associated with
     * the preferred magnitude of all solutions in view.
     */
// NOTE THAT THESE THE AMP,CODA LOADING  METHODS ARE NOW IDENTICAL, THUS CREATE ONE CALLED loadMagReadings() ?
    public void loadCodas() {
        if (! codaRetrieveFlag) return;
        report("Getting Coda data.", 30);
        Solution sol[] = solList.getArray();
        for (int i = 0; i<sol.length; i++) {
          //sol[i].addCodas( Coda.create().getByMagnitude(sol[i].magnitude) ); // aww replaced with below

          //Magnitude prefMag = sol[i].getPreferredMagnitude(); // aww 05/06/2005 removed
          // Do "prefMag" of type to get observations -aww
          Magnitude prefMag = sol[i].getPrefMagOfType(MagTypeIdIF.MD); // aww 05/06/2005 added
          if (prefMag == null) prefMag = sol[i].getPrefMagOfType(MagTypeIdIF.MC);
          if (prefMag == null || ! prefMag.isCodaMag()) return;  // wrong kind of Magnitude

          //NOTE: Associating reading with a magnitude should also update the solution codaList associations
          // Use fastAssociateAll only if you know the codaList is EMPTY, or you first CLEAR(tf) the list,
          // it doesn't check for preexisting matching instances and does a single interval notification:
          // prefMag.fastAssociateAll( (List) Coda.create().getByMagnitude(prefMag) );
          // But if you want to PRESERVE the PRIOR data, then do each, one at a time, for check and notify :
          //Coda.create().getByMagnitude(prefMag); // association listener by default should update sol too.
          //prefMag.associate((List) Coda.create().getByMagnitude(prefMag, null, false) ); // doesn't clear list
          boolean stale = prefMag.isStale();
          if (prefMag.getReadingList().isEmpty()) { // only load if empty -aww 2010/07/06
              prefMag.loadReadingList(true); // does the associations true, true clears the list first
          }
          if (! prefMag.isPreferred()) // must then add to solution's list (since no listener update)
              sol[i].associate(prefMag.getReadingList());  // aww 05/06/2005
          prefMag.setStale(stale); // assume no changed readings in db, 01/24/2005 test aww
          prefMag.getReadingList().matchChannelsWithDataSource(sol[i].getDateTime()); // aww test 11/17
        }
    }

    /**
     * Load the phases for this MasterView. Connect a list to each solution
     * and add all phases to the MasterView's list.
     */
    public void loadPhases() {
        if (! phaseRetrieveFlag) return;
        report("Getting Phase data.", 10);
        Solution sol[] = solList.getArray();
        for (int i = 0; i<sol.length; i++) {
          //sol[i].addPhases( Phase.create().getBySolution(sol[i]) ); // aww replace with below

          // Use fastAssociateAll only if you know the phaseList is EMPTY, or you first CLEAR(tf) the list,
          // it doesn't check for preexisting matching instances and does a single interval notification:
          //sol[i].fastAssociateAll(Phase.create().getBySolution(sol[i], null, false) );
          // But if you want to PRESERVE the PRIOR data, then do each, one at a time, for check and notify :
          //Phase.create().getBySolution(sol[i]); // does the sol association
          //sol[i].associate( (List) Phase.create().getBySolution(sol[i], null, false) );
          sol[i].listenToPhaseListChange(false); // otherwise origin dependent magnitudes become stale -aww
          //boolean stale = sol[i].getPreferredMagnitude().isStale(); // not needed if listener disabled -aw
          if (sol[i].getPhaseList().isEmpty()) { // only load when empty -aww 2010/07/06
              sol[i].loadPhaseList(true); // does the association
          }
          //sol[i].getPreferredMagnitude().setStale(stale); // assume no changed readings in db, 01/24/2005 test aww
          sol[i].getPhaseList().matchChannelsWithDataSource(sol[i].getDateTime()); // aww test 11/17
          sol[i].listenToPhaseListChange(true); // re-enable phaseList listener after load -aww
        }
    }

    /**
     * Load the waveforms for this MasterView using the mode defined by
     * MasterView.waveformLoadMode.  */
    public void loadWaveforms() {
        // Get timeseries if flag is set
        BenchMark bm = new BenchMark();
        System.out.print("MasterView: loadWaveforms mode = "+waveformLoadMode + " at " + new DateTime());

        if (waveformLoadMode == LoadAllInForeground) {
            System.out.println(" =>foreground.");
            wfvList.loadWaveformsNow();

        } else if (waveformLoadMode == LoadAllInBackground) {
            System.out.println(" =>background.");
            wfvList.loadWaveformsInBackground();

        } else if (waveformLoadMode == Cache) {
            // start the CacheMgr
            System.out.println(" =>cache.");
            //for MACBETH: if (wfCacheAllWf2Disk) toolBar.writeWfCache(1);  // write all to local disk before or after loading them by cache manager?
            wfvList.loadWaveformsInCache(); 
        } else if (waveformLoadMode == LoadNone) {
            System.out.println(" =>none.");
        }
        System.out.println("MasterView: loadWaveforms finished at "+ new DateTime());
        bm.print("Masterview: loadWaveforms load time");
    }

/**
 * Return ArrayList of waveforms in this masterView. Not all
 * WFViews will necessarily have a waveform so you can't assume the returned
 * collection will have the same size or order as the WFView list.
 * Returns empty list if there are no waveforms. */
public List getWaveformList() {

    WFView wfv[] = wfvList.getArray();

    ArrayList wfa = new ArrayList();

    for (int i = 0; i<wfv.length; i++)  {
        if (wfv[i].wf != null) wfa.add(wfv[i].wf);
    }

    return wfa;

}
// Some wfViews may not have a waveform (readings only type) -aww
public int getWaveformCount() {
    int wfvCnt = wfvList.size();
    if (wfvCnt == 0) return 0;

    int wfCount = 0;
    for (int i = 0; i<wfvCnt; i++)  {
      if ( ((WFView) wfvList.get(i)).wf != null ) wfCount++;
    }
    return wfCount;
}

public int getSelectedWFSegmentCount() {
    WFView wfvSel = masterWFViewModel.get();
    if (wfvSel == null || wfvSel.wf == null) return 0 ;
    else return wfvSel.wf.getSegmentCount();
}

// Is method below really needed? -aww
public int getWFSegmentCount() {
    WFView wfv[] = wfvList.getArray();
    int knt = 0;
    for (int i = 0; i<wfv.length; i++)  {
      if (wfv[i].wf != null) knt += wfv[i].wf.getSegmentCount(); // aww fix
    }
    return knt;
}
/**
 * Return array of waveforms in this masterView. Not all
 * WFViews will necessarily have a waveform. Returns empty array if there are no
 * waveforms. */
public Waveform[] getWaveformArray() {
    ArrayList list = new ArrayList(getWaveformList());
    return (Waveform []) list.toArray(new Waveform [list.size()]);
}

/**
 * Add a WFView to the masterView
 */
public void addWFView(WFView wfv) {

    wfvList.add(wfv);
    this.timeSpan.include(wfv.viewSpan);
    //if (wfv.getChannelObj().getDistance() != Channel.NULL_DIST) this.timeSpan.include(wfv.viewSpan);

}

// Below implementation generalized to include Phase, Coda, and Amp 
public void addReadingToCurrentSolution(JasiReading jr) {
    addReadingToCurrentSolution(jr, true);
}
public void addReadingToCurrentSolution(JasiReading jr, boolean clearUndo) {

         Solution sol = getSelectedSolution();
         jr.assign(sol); // associate with current solution

         JasiReadingList solList = (JasiReadingList)sol.getListFor(jr);

         String msg1 = null;
         String msg2 = null;
         boolean doitFlag = true;
         Phase ph = null;
 
         if (jr instanceof Phase) {
             Phase phase = (Phase) jr;
             ph = (Phase) solList.getNearestToTime(phase); 
             if (ph != null) {
               // Different instance in same list with same channel name and datetime, did type change P to S or S to P ?
               if (ph != phase && phase.isLikeTime(ph) && !phase.isSameType(ph)) {
                   msg1 = "Replace OLD phase of type " + ((String) ph.getTypeQualifier())  +
                   " with NEW phase of type" + ((String) phase.getTypeQualifier())  + " ?";
                   msg2 = "DIFFERENT phase type EXISTS at SAME time for " + ph.getChannelObj().toDelimitedNameString();
                   doitFlag = (JOptionPane.YES_OPTION ==
                         JOptionPane.showConfirmDialog((java.awt.Component)myOwner, msg1, msg2, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
                   );
                   if (doitFlag) {
                       //undo?
                       addToPhaseUndoList(ph, clearUndo);
                       solList.remove(ph); //delete old phase and continue tests below
                   }
                   else return; //no-op bail
               }
             }
         }

         if (jr instanceof Phase && replacePhasesByStaType)  {  // aww added flag test 06/22/2006
             Phase phase = (Phase) jr;
             PhaseList pList2 = (PhaseList) solList.getAllOfSameStaType(jr);  // P or S
             if (pList2.size() > 0) {
               ph = null;
               String type = ((String) phase.getTypeQualifier()).trim();
               for (int ii=0; ii < pList2.size(); ii++) {

                 ph = pList2.getPhase(ii);
                 if (ph.isDeleted()) continue; // skip over existing deleted, assume they are replaceable -aww 2010/10/06

                 if (phase.getChannelObj().isVertical()) {
                   if (type.equals("S")) { 
                      if (ph.getChannelObj().isHorizontal()) {
                          msg1 = "Replace HORIZONTAL S pick " +
                                 ph.getChannelObj().getSeedchan() + " " + ph.description.toString() +
                                 " with VERTICAL " +
                                 phase.getChannelObj().getSeedchan() + " " + phase.description.toString() + " ?";
                          msg2 = "HORIZONTAL S pick already EXISTS for " + ph.getChannelObj().toDelimitedNameString();
                          doitFlag = false;
                          break;
                      }
                   }
                 }
                 else if (phase.getChannelObj().isHorizontal()) {
                  if (type.equals("P")) {
                      if (ph.getChannelObj().isVertical()) {
                          msg1 = "Replace VERTICAL P pick " +
                                 ph.getChannelObj().getSeedchan() + " " + ph.description.toString() +
                                 " with HORIZONTAL " +
                                 phase.getChannelObj().getSeedchan() + " " + phase.description.toString() + " ?";
                          msg2 = "VERTICAL P pick already EXISTS for " + ph.getChannelObj().toDelimitedNameString();
                          doitFlag = false;
                          break;
                      }
                   }
                 }

                 // Check for AUTO versus NON-auto pick on same station
                 if (phase.sameStationAs(ph) && (phase.isAuto() && !ph.isAuto())) {
                   String prcstate = ph.getProcessingStateString();
                   msg1 = "Replace " +
                       ph.getChannelObj().getSeedchan() + " " + ph.description.toString() +
                       " " + prcstate + " pick with AUTO pick " +
                       phase.getChannelObj().getSeedchan() + " " + phase.description.toString() + " ?";
                   msg2 = prcstate + " pick EXISTS on " + ph.getChannelObj().toDelimitedNameString();
                   doitFlag = false;
                   break;
                 }

                 if (! phase.sameSeedChanAs(ph) && (phase.getQuality() < ph.getQuality())) {
                   msg1 = "Replace " +
                       ph.getChannelObj().getSeedchan() + " " + ph.description.toString() +
                       " with the LOWER QUALITY pick " +
                       phase.getChannelObj().getSeedchan() + " " + phase.description.toString() + " ?";
                   msg2 = "HIGHER quality pick EXISTS on " + ph.getChannelObj().toDelimitedNameString();
                   doitFlag = false;
                   break;
                 }
               }

               if (! doitFlag) {
                 doitFlag = (JOptionPane.YES_OPTION ==
                             JOptionPane.showConfirmDialog((java.awt.Component)myOwner,
                                 msg1,
                                 msg2,
                                 JOptionPane.YES_NO_OPTION,
                                 JOptionPane.QUESTION_MESSAGE)
                             );
               }
             }
             if (doitFlag) {
                 //undo
                 addToPhaseUndoList(pList2, clearUndo);
                 solList.addOrReplaceAllOfSameStaType(jr, false);  // modified -aww 2010/09/30
             }
         }
         else {
             //undo
             JasiReadingIF jr2 = solList.addOrReplace(jr);  // jr is a phase, amp or coda here 
             if (jr2 instanceof Phase) {
                 addToPhaseUndoList((Phase)jr2, clearUndo);
             }
             else if (jr2 instanceof Amplitude) {
                 addToAmpUndoList((Amplitude)jr2, clearUndo);
             }
             else if (jr2 instanceof Coda) {
                 addToCodaUndoList((Coda)jr2, clearUndo);
             }

         }
         if (jr instanceof Phase) return;

         Magnitude mag = null;
         if (jr instanceof Amplitude) {
             mag = sol.getPrefMagOfType("l");
             amUndoList.clear(false);
         }
         else if (jr instanceof Coda) {
             mag = sol.getPrefMagOfType("d");
             coUndoList.clear(false);
         }
         if (mag != null)  {
             //undo
             mag.getListFor(jr).addOrReplace(jr);
         }
}

/**
 * Return the number of WFView in this MasterView
 */
public int getWFViewCount() {
    return wfvList.size();
}
public TimeSpan getViewSpan() {
    return timeSpan;
}

/**
 * Return list of phases in all solutions in the MasterView.
 * @return PhaseList
 */
public PhaseList getAllPhases() {
    PhaseList bigList = PhaseList.create();
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       bigList.fastAddAll(sol[i].phaseList);
    }
    return bigList;
}
/** Return the count of phases in ALL solutions. */
public int getPhaseCount() {
    int knt = 0;
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       knt += sol[i].phaseList.size();
    }
    return knt;
}
/**
 * Return list of amps in all solutions in the MasterView.
 * @return AmpList
 */
public AmpList getAllAmps() {
    AmpList bigList = AmpList.create();
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       bigList.fastAddAll(sol[i].ampList);
    }
    return bigList;
}
/** Return the count of amps in ALL solutions. */
public int getAmpCount() {
    int knt = 0;
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       knt += sol[i].ampList.size();
    }
    return knt;
}
/**
 * Return list of codas in all solutions in the MasterView.
 * @return CodaList
 */
public CodaList getAllCodas() {
    CodaList bigList = CodaList.create();
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       bigList.fastAddAll(sol[i].codaList);
    }
    return bigList;
}
/** Return the count of codas in ALL solutions. */
public int getCodaCount() {
    int knt = 0;
    Solution sol[] = solList.getArray();
    for (int i = 0; i<sol.length; i++) {
       knt += sol[i].codaList.size();
    }
    return knt;
}
/** If arg is true, only WFViews with phases will be included in the master view. */
public void includeOnlyWithPhases(boolean tf) {

    includeOnlyWithPhases = tf;

}
/** Returns true if only WFViews with phases will be included in the master view. */
public boolean getIncludeOnlyWithPhases() {
    return includeOnlyWithPhases;
}

/** Set the alignment mode. Currently only time alignment is supported. Returns
* false if an invalid mode is specified. */
    public boolean setAlignmentMode(int mode) {
      if (mode > -1 && mode < AlignmentModeCount) {
         alignmentMode = mode;
         return true;
      } else {
         return false;
      }
    }

    /** Return the alignment mode. */
    public int getAlignmentMode() {
      return  alignmentMode;
    }

    public double getAlignmentVelocity() {
        return alignmentVelocity;
    }
    public void setAlignmentVelocity(double vel) {
        alignmentVelocity = vel;
    }

    /** Align views on whatever is defined by getAlignmentMode() */
    public void alignViews() {
       if (getAlignmentMode() == AlignOnTime) {
         timeAlignViews();
       } else if (getAlignmentMode() == AlignOnP) {
         pAlignViews();
       } else if (getAlignmentMode() == AlignOnS) {
         sAlignViews();
       } else if (getAlignmentMode() == AlignOnV) {
         vAlignViews();
       } 
    }
/**
 * Time align the WFViews by setting all the viewSpans equal to the
 * MasterView's viewSpan
 */
    public void timeAlignViews() {
        WFView wfv[] = wfvList.getArray();
        if (wfv.length < 1) return;
        System.out.println("MV: Align views by master timespan " + this.timeSpan);
        for (int i = 0; i<wfv.length; i++)  {
            wfv[i].setViewSpan(this.timeSpan);
        }
    }

    public void pAlignViews() {
        WFView wfv[] = wfvList.getArray();
        if (wfv.length < 1) return;
        TravelTime tt = TravelTime.getInstance();
        double z = 0.;
        if (getSelectedSolution() != null) z = getSelectedSolution().getModelDepth(); // mdepth -aww 2015/10/10
        //double baseTime = getSelectedSolution().datetime.doubleValue() - 10.0; // OT - 10 sec
        //System.out.println("MV: Align views by P-traveltime from (OriginTime - 10s) = " +LeapSeconds.trueToString(baseTime));
        double baseTime = wfvList.getEarliestSampleTime();
        double duration = timeSpan.getDuration(); // wfvList.getLatestSampleTime()-baseTime; 
        System.out.println("MV: Align views by P-traveltime added to earliest start time of " +
                LeapSeconds.trueToString(baseTime) + " duration: " + Math.round(duration*1000.)/1000.);
        double start = 0.; 
        for (int i = 0; i<wfv.length; i++)  {
           start = baseTime + tt.getTTp(wfv[i].getChannelObj().getHorizontalDistance(), z); // add P-wave traveltime to each
           wfv[i].getViewSpan().set(start,start+duration); // keep duration the same so pixels/sec the same
        }
    }

    public void sAlignViews() {
        WFView wfv[] = wfvList.getArray();
        if (wfv.length < 1) return;
        TravelTime tt = TravelTime.getInstance();
        double z = 0.;
        if (getSelectedSolution() != null) z = getSelectedSolution().getModelDepth(); // mdepth -aww 2015/10/10
        //double baseTime = getSelectedSolution().datetime.doubleValue() - 10.0; // OT - 10 sec
        //System.out.println("MV: Align views by S-traveltime from (OriginTime - 10s) = " +LeapSeconds.trueToString(baseTime));
        double baseTime = wfvList.getEarliestSampleTime();
        double duration = timeSpan.getDuration(); // wfvList.getLatestSampleTime()-baseTime; 
        System.out.println("MV: Align views by S-traveltime added to earliest start time of " +
                LeapSeconds.trueToString(baseTime) + " duration: " + Math.round(duration*1000.)/1000.);
        double start = 0.; 
        for (int i = 0; i<wfv.length; i++)  {
           start = baseTime + tt.getTTs(wfv[i].getChannelObj().getHorizontalDistance(), z); // add S-wave traveltime to each
           wfv[i].getViewSpan().set(start,start+duration); // keep duration the same so pixels/sec the same
        }
    }

    public void vAlignViews() {
        WFView wfv[] = wfvList.getArray();
        if (wfv.length < 1) return;
        //double baseTime = getSelectedSolution().datetime.doubleValue() - 10.0; // OT - 10 sec
        //System.out.println("MV: Align views by reduction v=" +alignmentVelocity+ " from (OriginTime - 10s) = " +LeapSeconds.trueToString(baseTime));
        double baseTime = wfvList.getEarliestSampleTime();
        double duration = timeSpan.getDuration(); // wfvList.getLatestSampleTime()-baseTime; 
        System.out.println("MV: Align views by v=" +alignmentVelocity+
                " traveltime added to earliest start time of " + LeapSeconds.trueToString(baseTime) + " duration: " + Math.round(duration*1000.)/1000.);
        double start = 0.; 
        for (int i = 0; i<wfv.length; i++)  {
           start = baseTime + wfv[i].getChannelObj().getHorizontalDistance()/alignmentVelocity; // add vel traveltime to each
           wfv[i].getViewSpan().set(start,start+duration); // keep duration the same so pixels/sec the same
        }
    }

/**
 * Set the viewSpan of the MasterView and ALL WFViews to this timeSpan.
 */
    public void setAllViewSpans(TimeSpan ts) {
        setAllViewSpans(ts, false);
    }

    public void setAllViewSpans(TimeSpan ts, boolean doWfs) {
        this.timeSpan = ts;
        alignViews();
        if (!doWfs) return;
        //System.out.println("MasterView resetting all waveform spans... " + ts);
        WFView[] wfv = wfvList.getArray();
        Waveform wf = null;
        TimeSpan ts2 = null;
        for (int idx = 0; idx<wfv.length; idx++) {
            wf = wfv[idx].wf;
            if (wf != null) {
                ts2 = wfv[idx].getViewSpan();
                if (! wf.getTimeSpan().equals(ts2)) {
                    wf.setTimeSpan(ts2);
                    wfv[idx].autoSetDataSpan();
                    if (wfv[idx].hasTimeSeries()) { 
                        ((AbstractWaveform) wf).unloadTimeSeries(); // need to do this first, why?
                        //((AbstractWaveform) wf).loadTimeSeries(false);
                    }
                }
            }
        }
        if (debug) System.out.println("MV: DEBUG setAllViewSpans, setCacheIndex(0) doWfs: " + doWfs);
        wfvList.setCacheIndex(0);
    }

/**
 * Set flag to time align the WFViews by setting all the viewSpans equal to
 * the MasterView's viewSpan
 */
/*    public void setTimeAlign(boolean tf) {
        timeAlign = tf;
    }
*/
/**
 * Set max number of waveforms to load. Used to avoid memory overflow
 */
    public void setWaveformLoadLimit(int limit) {
        waveformLoadLimit = limit;
    }

/**
 * Return max number of waveforms to load. Used to avoid memory overflow
 */
    public int getWaveformLoadLimit() {
        return waveformLoadLimit;
    }

/**
 * Sort/resort the WFView list by distance from the current selected Solution.
 */
    public void distanceSort() {
        distanceSort(getSelectedSolution());
    }

/**
 * Sort/resort the WFView list by distance from the given LatLonZ.
*/
    public void distanceSort(GeoidalLatLonZ latlonz) {
      if (debug) report("DEBUG distanceSort(latlonZ) ...");
      if (latlonz == null) return;
      wfvList.distanceSort(latlonz);
      if (getSelectedSolution() != null)
        getSelectedSolution().sortReadingLists(latlonz);
    }

  public void setResidualStripValue(double value) {
    residualStripValue = value;
  }

  public double getResidualStripValue() {
    return residualStripValue;
  }

  public void setResidualAmpStripValue(double value) {
    residualAmpStripValue = value;
  }

  public double getResidualAmpStripValue() {
    return residualAmpStripValue;
  }

  public void setResidualCodaStripValue(double value) {
    residualCodaStripValue = value;
  }

  public double getResidualCodaStripValue() {
    return residualCodaStripValue;
  }

  public void setClockQualityThreshold(double value) {
    clockQualityThreshold = value;
  }

  public double getClockQualityThreshold() {
    return clockQualityThreshold;
  }

/**
* Delete all Amps associated with this solution that are greater
* than or equal to this dist.  Returns the number of deleted items.
*/
  public int rejectAmpsByDistance(double cutDist, Solution sol) {
      return rejectAmpsByDistance(cutDist, sol, true);
  }
  public int unrejectAmpsByDistance(double cutDist, Solution sol) {
      return rejectAmpsByDistance(cutDist, sol, false);
  }

  public final int rejectAmpsByDistance(double cutDist, Solution sol, boolean tf) {

     int knt = 0;

     Amplitude amp[] = sol.ampList.getArray();

     for (int i = 0;i<amp.length; i++)  {
        if (amp[i].isFinal() && tf) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView ampLists
        //if (amp[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (amp[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           amp[i].setReject(tf);
           knt++;
        }
     }
     updateAmpLists(); // -aww 2008/02/26

     return knt;
  }
/**
* Delete all Codas associated with this solution that are greater
* than or equal to this dist.  Returns the number of deleted items.
*/
  public int rejectCodasByDistance(double cutDist, Solution sol) {
      return rejectCodasByDistance(cutDist, sol, true);
  } 
  public int unrejectCodasByDistance(double cutDist, Solution sol) {
      return rejectCodasByDistance(cutDist, sol, false);
  } 
  public final int rejectCodasByDistance(double cutDist, Solution sol, boolean tf) {

     int knt = 0;

     Coda coda[] = sol.codaList.getArray();

     for (int i = 0;i<coda.length; i++)  {
        if (coda[i].isFinal() && tf) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView codaLists
        // horizontalDistance() ?
        //if (coda[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (coda[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           coda[i].setReject(tf);
           knt++;
        }
     }
     updateCodaLists(); // -aww 2008/02/26

     return knt;
  }
/**
* Delete all phases associated with this solution that are in WFViews greater
* than or equal to this dist. (This function is here rather than in PhaseList or
* Solution because the WFViews are the only components garenteed to have the
* distance set) . Returns the number of deleted phases.
*/
  public int rejectPhasesByDistance(double cutDist, Solution sol) {
      return rejectPhasesByDistance(cutDist, sol, true);
  }
  public int unrejectPhasesByDistance(double cutDist, Solution sol) {
      return rejectPhasesByDistance(cutDist, sol, false);
  }
  public final int rejectPhasesByDistance(double cutDist, Solution sol, boolean tf) {

     int knt = 0;

     Phase ph[] = sol.phaseList.getArray();

     for (int i = 0;i<ph.length; i++)  {
        if (ph[i].isFinal() && tf) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView.phaseLists
        //if (ph[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (ph[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           ph[i].setReject(tf);
           knt++;
        }
     }
     updatePhaseLists(); // -aww 2008/02/26

     return knt;
  }

/**
* Delete all phases associated with the selected solution that have a residual greater
* than or equal to the value set by setResidualStripValue().
* Returns the number of deleted phases.
*/
  public int rejectPhasesByResidual() {
     return  rejectPhasesByResidual(getSelectedSolution());
  }
  public int rejectAmpsByResidual() {
     return  rejectAmpsByResidual(getSelectedSolution());
  }
  public int rejectCodasByResidual() {
     return  rejectCodasByResidual(getSelectedSolution());
  }
/**
* Delete all phases associated with this solution that have a residual greater
* than or equal to the value set by setResidualStripValue().
* Returns the number of deleted phases.
*/
  public int rejectPhasesByResidual(Solution sol) {
     return  rejectPhasesByResidual(getResidualStripValue(), sol);
  }
  public int rejectAmpsByResidual(Solution sol) {
     return  rejectAmpsByResidual(getResidualAmpStripValue(), sol);
  }
  public int rejectCodasByResidual(Solution sol) {
     return  rejectCodasByResidual(getResidualCodaStripValue(), sol);
  }
/**
* Delete all phases associated with this solution that have a residual greater
* than or equal to this value. Returns the number of deleted phases.
*/
  public int rejectPhasesByResidual(double val, Solution sol) {
     int cnt = sol.phaseList.rejectByResidual(val, sol);
     updatePhaseLists();
     return cnt;
  }
  public int rejectAmpsByResidual(double val, Solution sol) {
     int cnt = sol.ampList.rejectByMagResidual(val, sol);
     updateAmpLists();
     return cnt;
  }
  public int rejectCodasByResidual(double val, Solution sol) {
     int cnt = sol.codaList.rejectByMagResidual(val, sol);
     updateCodaLists();
     return cnt;
  }

/**
* Delete all Amps associated with this solution that are greater
* than or equal to this dist.  Returns the number of deleted items.
*/
  public int stripAmpsByDistance(double cutDist, Solution sol) {
      return stripAmpsByDistance(cutDist, sol, false);
  }
  public int stripAmpsByDistance(double cutDist, Solution sol, boolean autoOnly) {

     int knt = 0;

     Amplitude amp[] = sol.ampList.getArray();

     amUndoList.clear(false);

     for (int i = 0;i<amp.length; i++)  {
        if (amp[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView ampLists
        //if (amp[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (amp[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           //sol.deleteAmplitude(amp[i]);
           // Note differences between delete,erase, and remove
           // invoked on AbstractCommitableList data in a solution. - aww
           // Is the one you want here?
           if (!autoOnly || amp[i].isAuto()) {
               amUndoList.add(amp[i], false);
               sol.erase(amp[i]); // aww deleteFlag == true, removes from list and notifies listeners
           }
           knt++;
        }
     }
     updateAmpLists(); // -aww 2008/02/26

     return knt;
  }
/**
* Delete all Codas associated with this solution that are greater
* than or equal to this dist.  Returns the number of deleted items.
*/
  public int stripCodasByDistance(double cutDist, Solution sol) {
      return stripCodasByDistance(cutDist, sol, false);
  }
  public int stripCodasByDistance(double cutDist, Solution sol, boolean autoOnly) {
     int knt = 0;

     Coda coda[] = sol.codaList.getArray();

     coUndoList.clear(false);

     for (int i = 0;i<coda.length; i++)  {
        if (coda[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView codaLists
        // horizontalDistance() ?
        //if (coda[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (coda[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           //sol.deleteCoda(coda[i]);
           // Note differences between delete,erase, and remove
           // invoked on AbstractCommitableList data in a solution. - aww
           // Is the one you want here?
           if (!autoOnly || coda[i].isAuto()) {
               coUndoList.add(coda[i], false);
               sol.erase(coda[i]); // aww deleteFlag == true, removes from list and notifies listeners
           }
           knt++;
        }
     }
     updateCodaLists(); // -aww 2008/02/26

     return knt;
  }
/**
* Delete all phases associated with this solution that are in WFViews greater
* than or equal to this dist. (This function is here rather than in PhaseList or
* Solution because the WFViews are the only components garenteed to have the
* distance set) . Returns the number of deleted phases.
*/
  public int stripPhasesByDistance(double cutDist, Solution sol) {
      return stripPhasesByDistance(cutDist, sol, false, false);
  }
  public int stripPhasesByDistance(double cutDist, Solution sol, boolean autoOnly, boolean sOnly) {

     int knt = 0;

     Phase ph[] = sol.phaseList.getArray();

     phUndoList.clear(false);

     for (int i = 0;i<ph.length; i++)  {
        if (ph[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
        // must delete from Solutions's list *NOT* WFView's list because
        // the MV's list is Active and will notify observers of change
        // This, in turn, will update the WFView.phaseLists
        //if (ph[i].getDistance() > cutDist) { // don't use slant aww 06/11/2004
        if (ph[i].getHorizontalDistance() > cutDist) { // aww 06/11/2004
           //sol.deletePhase(ph[i]);
           // Note differences between delete,erase, and remove
           // invoked on AbstractCommitableList data in a solution. - aww
           // Is the one you want here?
           if (!autoOnly || ph[i].isAuto()) {
               if (!sOnly || ph[i].isS()) {
                   phUndoList.add(ph[i], false);
                   sol.erase(ph[i]); // aww deleteFlag == true, removes from list and notifies listeners
               }
           }
           knt++;
        }
     }

     updatePhaseLists(); // -aww 2008/02/26

     return knt;
  }

/**
* Delete all phases associated with the selected solution that have a residual greater
* than or equal to the value set by setResidualStripValue().
* Returns the number of deleted phases.
*/
  public int stripPhasesByResidual() {
     return  stripPhasesByResidual(getSelectedSolution());
  }
  public int stripAmpsByResidual() {
     return  stripAmpsByResidual(getSelectedSolution());
  }
  public int stripCodasByResidual() {
     return  stripCodasByResidual(getSelectedSolution());
  }
/**
* Delete all phases associated with this solution that have a residual greater
* than or equal to the value set by setResidualStripValue().
* Returns the number of deleted phases.
*/
  public int stripPhasesByResidual(Solution sol) {
     return  stripPhasesByResidual(getResidualStripValue(), sol);
  }
  public int stripAmpsByResidual(Solution sol) {
     return  stripAmpsByResidual(getResidualAmpStripValue(), sol);
  }
  public int stripCodasByResidual(Solution sol) {
     return  stripCodasByResidual(getResidualCodaStripValue(), sol);
  }
/**
* Delete all phases associated with this solution that have a residual greater
* than or equal to this value. Returns the number of deleted phases.
*/
  public int stripPhasesByResidual(double val, Solution sol) {

      // Replaced with code below for "undo" list inclusion
      //int cnt = sol.phaseList.stripByResidual(val, sol);
      //updatePhaseLists(); // -aww 2008/02/26
      //return cnt;

      int cnt = 0;
      Phase ph[] = sol.phaseList.getArray();
      phUndoList.clear(false);
      for (int i = 0;i<ph.length; i++)  {
          if (ph[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
          // must delete from Solutions's list *NOT* WFView's list because
          // the MV's list is Active and will notify observers of change
          // This, in turn, will update the WFView.phaseLists
          if ( Math.abs( ph[i].getResidual() ) > val ) {
              phUndoList.add(ph[i], false);
              sol.erase(ph[i]);
              cnt++;
          }
      }
      updatePhaseLists();
      return cnt;
  }

  public int stripAmpsByResidual(double val, Solution sol) {
      // Replaced with code below for "undo" list inclusion
      //int cnt = sol.ampList.stripByMagResidual(val, sol);
      int cnt = 0;
      Amplitude amp[] = sol.ampList.getArray();
      amUndoList.clear(false);
      for (int i = 0;i<amp.length; i++)  {
          if (amp[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
          // must delete from Solutions's list *NOT* WFView's list because
          // the MV's list is Active and will notify observers of change
          // This, in turn, will update the WFView.ampLists
          if ( Math.abs( amp[i].getMagResidual() ) > val ) {
              amUndoList.add(amp[i], false);
              sol.erase(amp[i]);
              cnt++;
          }
      }
      updateAmpLists(); // -aww 2008/02/26
      return cnt;
  }
  public int stripCodasByResidual(double val, Solution sol) {
      // Replaced with code below for "undo" list inclusion
      //int cnt = sol.codaList.stripByMagResidual(val, sol);
      int cnt = 0;
      Coda coda[] = sol.codaList.getArray();
      coUndoList.clear(false);
      for (int i = 0;i<coda.length; i++)  {
          if (coda[i].isFinal()) continue; // don't remove required? -aww 2013/02/15
          // must delete from Solutions's list *NOT* WFView's list because
          // the MV's list is Active and will notify observers of change
          // This, in turn, will update the WFView.codaLists
          if ( Math.abs( coda[i].getMagResidual() ) > val ) {
              coUndoList.add(coda[i], false);
              sol.erase(coda[i]);
              cnt++;
          }
      }
      updateCodaLists(); // -aww 2008/02/26
      return cnt;
  }

    public void clearUndoLists() {
        clearPhaseUndoList();
        clearAmpUndoList();
        clearCodaUndoList();
    }

    public void clearPhaseUndoList() {
        phUndoList.clear(false);
    }
    public void addToPhaseUndoList(Phase ph, boolean clear) {
        if (clear) {
            phUndoList.clear(false);
            phUndoList.add(ph, false);
        }
        //else phUndoList.addOrReplaceAllOfSameStaType(ph, false);
        else phUndoList.addOrReplaceAllOfSameChanType(ph, false);
    }
    public void addToPhaseUndoList(List aList, boolean clear) {
        if (clear) {
            phUndoList.clear(false);
            phUndoList.addAll(aList, false);
        }
        //phUndoList.addOrReplaceAllOfSameStaType(aList, false);
        phUndoList.addOrReplaceAllOfSameChanType(aList, false);
    }
    public int undoPhases(boolean doAll) {

        int cnt = phUndoList.size();
        if (cnt == 0) return 0;

        Solution sol = getSelectedSolution();
        if (sol == null) return 0;

        Phase ph = null;

        if (!doAll) { // restore last element in list
            ph = (Phase) phUndoList.get(cnt-1);
            // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
            ph.setDeleteFlag(false);
            ph.assign(sol);
            //sol.getPhaseList().addOrReplaceAllOfSameStaType(ph, false); // autoOnly=false
            sol.getPhaseList().addOrReplaceAllOfSameChanType(ph, false); // autoOnly=false
            phUndoList.remove(ph, false);
        }
        else { // restore all list elements
            Phase[] pArray = phUndoList.getArray();
            for (int idx=0; idx < pArray.length; idx++) {
                ph = pArray[idx];
                // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
                ph.setDeleteFlag(false);
                ph.assign(sol);
                //sol.getPhaseList().addOrReplaceAllOfSameStaType(ph, false); // autoOnly=false
                sol.getPhaseList().addOrReplaceAllOfSameChanType(ph, false); // autoOnly=false
            }
            phUndoList.clear(false);
        }
        return cnt;
    }

    public void clearAmpUndoList() {
        amUndoList.clear(false);
    }
    public void addToAmpUndoList(Amplitude am, boolean clear) {
        if (clear) {
            amUndoList.clear(false);
            amUndoList.add(am, false);
        }
        //else amUndoList.addOrReplaceAllOfSameStaType(am, false);
        else amUndoList.addOrReplaceAllOfSameChanType(am, false);
    }
    public void addToAmpUndoList(List aList, boolean clear) {
        if (clear) {
            amUndoList.clear(false);
            amUndoList.addAll(aList, false);
        }
        //amUndoList.addOrReplaceAllOfSameStaType(aList, false);
        amUndoList.addOrReplaceAllOfSameChanType(aList, false);
    }
    public int undoAmps(boolean doAll) {

        Solution sol = getSelectedSolution();
        if (sol == null) return 0;

        Magnitude mag = sol.getPrefMagOfType("l");
        if (mag == null) return 0;

        AmpList aList = (AmpList) amUndoList.getAssociatedWith(mag, false, true);
        int cnt = aList.size();
        if (cnt == 0) return 0;

        Amplitude am = null;

        if (!doAll) { // restore last element in list
            am = (Amplitude) aList.get(cnt-1);
            // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
            am.setDeleteFlag(false);
            am.assign(sol);
            am.assign(mag);
            //mag.getReadingList().addOrReplaceAllOfSameStaType(am, false); // autoOnly=false
            mag.getReadingList().addOrReplaceAllOfSameChanType(am, false); // autoOnly=false
            amUndoList.remove(am, false);
        }
        else { // restore all list elements
            for (int idx=0; idx < cnt; idx++) {
                am = (Amplitude) aList.get(idx);
                // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
                am.setDeleteFlag(false);
                am.assign(sol);
                am.assign(mag);
                //mag.getReadingList().addOrReplaceAllOfSameStaType(am, false); // autoOnly=false
                mag.getReadingList().addOrReplaceAllOfSameChanType(am, false); // autoOnly=false
                //amUndoList.remove(am, false);
            }
            amUndoList.clear(false);
        }
        return cnt;
    }

    public void clearCodaUndoList() {
        coUndoList.clear(false);
    }
    public void addToCodaUndoList(Coda co, boolean clear) {
        if (clear) {
            coUndoList.clear(false);
            coUndoList.add(co, false);
        }
        //else coUndoList.addOrReplaceAllOfSameStaType(co, false);
        else coUndoList.addOrReplaceAllOfSameChanType(co, false);
    }
    public void addToCodaUndoList(List aList, boolean clear) {
        if (clear) {
            coUndoList.clear(false);
            coUndoList.addAll(aList, false);
        }
        //coUndoList.addOrReplaceAllOfSameStaType(aList, false);
        coUndoList.addOrReplaceAllOfSameChanType(aList, false);
    }
    public int undoCodas(boolean doAll) {

        Solution sol = getSelectedSolution();
        if (sol == null) return 0;

        Magnitude mag = sol.getPrefMagOfType("d");
        if (mag == null) return 0;

        CodaList cList = (CodaList) coUndoList.getAssociatedWith(mag, false, true);
        int cnt = cList.size();
        if (cnt == 0) return 0;

        Coda co = null;

        if (!doAll) { // restore last element in list
            co = (Coda) cList.get(cnt-1);
            // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
            co.setDeleteFlag(false);
            co.assign(sol);
            co.assign(mag);
            //mag.getReadingList().addOrReplaceAllOfSameStaType(co, false); // autoOnly=false
            mag.getReadingList().addOrReplaceAllOfSameChanType(co, false); // autoOnly=false
            coUndoList.remove(co, false);
        }
        else { // restore all list elements
            for (int idx=0; idx < cnt; idx++) {
                co = (Coda) cList.get(idx);
                // have to do below if readings are aliased (erase method effect) otherwise need to clone list data before erase
                co.setDeleteFlag(false);
                co.assign(sol);
                co.assign(mag);
                //mag.getReadingList().addOrReplaceAllOfSameStaType(co, false); // autoOnly=false
                mag.getReadingList().addOrReplaceAllOfSameChanType(co, false); // autoOnly=false
                //coUndoList.remove(co, false);
            }
            coUndoList.clear(false);
        }

        return cnt;
    }

/**
 * Create a new masterView containing only the first 'maxViews' components in 'mv'.
 * This will generally be used to create a more managable MasterView for events
 * with lots of components. If the original MasterView has 'maxViews' WFViews or
 * fewer it is returned without modification.
 */
    public static MasterView subset(MasterView mv, int maxViews) {

        if (mv.getWFViewCount() <= maxViews) return mv;

        MasterView mvNew = new MasterView();

        // copy the solution list
        mvNew.solList = new SolutionList(mv.solList);

        WFView wfv[] = mv.wfvList.getArray();

        // select the proper WFView's
        for ( int i=0; i < wfv.length; i++)
        {
            mvNew.addWFView(wfv[i]);
            if (i == maxViews-1) break;                // got enough
        }

        mvNew.alignViews();

        return mvNew;
    }

/**
 * Create a new masterView containing only vertical components. It will contain no more
 * then 'maxViews' WFViews.
 */
// Is there a more general way to do this sort of thing?

    public static MasterView vertOnly(MasterView mv, int maxViews) {

        MasterView mvNew = new MasterView();

        // copy the solution list
    //    mvNew.solList = new ActiveSolutionList(mv.solList);
        mvNew.solList = new SolutionList(mv.solList);

        WFView wfv[] = mv.wfvList.getArray();

        // select the proper WFView's
        for ( int i=0; i < wfv.length; i++)
        {
            if (wfv[i].chan.isVertical()) mvNew.addWFView(wfv[i]);
            if (i == maxViews) break;                // got enough
        }

        mvNew.alignViews();

        return mvNew;
    }
/**
 * Create a new masterView containing only components that have phase picks.
 */
public static MasterView withPhasesOnly(MasterView mv) {

    return withPhasesOnly(mv, Integer.MAX_VALUE);
}

/**
 * Create a new masterView containing only components that have phase picks.
 * It will contain no more then 'maxViews' WFViews.
 */
// Is there a more general way to do this sort of thing?

public static MasterView withPhasesOnly(MasterView mv, int maxViews) {

    MasterView mvNew = new MasterView();

    // copy the solution list
    mvNew.solList = new SolutionList(mv.solList);

    WFView wfv[] = mv.wfvList.getArray();

    // select the proper WFView's
    for ( int i=0; i < wfv.length; i++)        {
            if (wfv[i].hasPhases()) mvNew.addWFView(wfv[i]);
            if (i == maxViews) break;                // got enough
        }

        mvNew.alignViews();

        return mvNew;
}

/**
 * Calculate the true and horizontal distance from location in the argument for
 * each site in the list. Requires that loadChannelData be called first.
 */

public void calcDistances(GeoidalLatLonZ loc) { // aww 06/11/2004
    wfvList.calcDistances(loc);
}


/**
 * Calculate the distance from the given location
 * Requires that loadChannelData be called first.
 */

public void calcDistances(float lat, float lon, float z)
{
    calcDistances(new LatLonZ(lat, lon, z));
}

/**
 * Calculate the distance from the location of the selected Solution.
 * Requires that loadChannelData be called first.
 */

public void calcDistances()
{
    calcDistances(getSelectedSolution()); // aww 6/11/2004
}

/**
 * If 'true' waveforms will be retreived.
 */
 /*
public void setWfRetrieveFlag(boolean tf)
{
    if (tf) {
        waveformLoadMode = LoadAllInForeground;
    } else {
        waveformLoadMode = LoadNone;
    }
}
*/
/**
 * Depricated
 */
 /*
public boolean getWfRetrieveFlag()
{
    return wfRetrieveFlag;
}
*/
//* Set the waveform loading method. Returns 'false' if the mode is invalid.
public boolean setWaveformLoadMode(int mode) {

    // check for valid mode type
    if (mode > ModeCount) return false;

    waveformLoadMode = mode;

    wfvList.setCachingEnabled( (waveformLoadMode == Cache) );

    return true;
}

public int getWaveformLoadMode() {
    return waveformLoadMode;
}

public void setCacheSize(int above, int below) {
    wfvList.setCacheSize(above, below); // changed to size new field members -aww 2010/08/06
}

public void setLoadChannelData(boolean tf) {
    loadChannelData = tf;
}

public boolean getLoadChannelData() {
    return loadChannelData;
}


/** Cleanup stuff before this mv is destroyed. Otherwise, dangling references
* cause a memory leak. Stops the cache manager. */
public void destroy() {
       try {
         if (waveformLoadMode == LoadAllInBackground) {
           wfvList.stopLoadWaveformsInBackground();
           Thread.sleep(10l);
           if (wfvList.wfLoaderThread != null) {
              if (wfvList.wfLoaderThread.thread.isAlive()) wfvList.wfLoaderThread.thread.interrupt(); 
           }
         }
         else {
           wfvList.stopCacheManager();
           if (wfvList.cacheMgr != null) {
             if (wfvList.cacheMgr.cacheMgrThread.thread.isAlive()) wfvList.cacheMgr.cacheMgrThread.thread.interrupt();
           }
         }
       } catch (Exception ex) {
           System.err.println(ex.getMessage());
       }
       finally {
         if (wfvList.wfLoaderThread != null) {
           wfvList.wfLoaderThread.thread = null;
           wfvList.wfLoaderThread.wfvList = null;
           wfvList.wfLoaderThread = null;
         }
         if (wfvList.cacheMgr != null) {
           wfvList.cacheMgr.cacheMgrThread.thread = null;
           wfvList.cacheMgr.cacheMgrThread = null;
           wfvList.cacheMgr.wfvList = null;
           wfvList.cacheMgr = null;
         }
       }

       // solList replaced changeListener with ListDataStateListener -aww
       solList.clearListDataStateListeners(); // aww

       masterWFViewModel.clearChangeListeners();
       masterWFWindowModel.clearChangeListeners();

       wfvList.clearListDataStateListeners(); // like the others ? aww

       for (int i=0; i<wfvList.size(); i++) {
         ((WFView)wfvList.get(i)).clear(); // added for GC insurance  -aww 2009/03/11
       }
       wfvList.clear(); //  added for GC insurance  -aww 2009/03/11

       //jasiReadingListModel.setWFViewList(null); // causes NullPointerException, wfvList is re-used for each event load
       jasiReadingListModel.clearWFViewList();

       clearSolutionList(); // remove solutions and readinglist listeners, remakes wfv readinglists so do after wfv clear

       PropertyChangeListener [] pcl = propChangeSupport.getPropertyChangeListeners();
       for (int i=0; i< pcl.length; i++) {
           propChangeSupport.removePropertyChangeListener((PropertyChangeListener)pcl[i]);
       }

       // WFView.setMasterView(null);
}

// SelectedWFViewModel listener
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == masterWFViewModel) {
            WFView wfv = masterWFViewModel.get();
            if (wfvList != null) wfvList.selectedWFView = wfv;
            firePropertyChange(NEW_WFVIEW_PROPNAME, null, wfv);
        }
    }

/**
 * Dump some info about the views for debugging
 */
    public void dump() {
        System.out.println(dumpToString());
    }

/** Returns a string containing a line describing each solution followed
* by a list of amplitudes associated with that solution and preferred magnitudes and their associated coda. */
    public String ampsToString() {
        String str="";
        Solution sol[] = solList.getArray();
        Magnitude prefMag = null;
        Iterator iter = null;
        if (sol.length == 0) str += "none";
        for (int i = 0; i<sol.length; i++) {
          str += sol[i].getNeatStringHeader()+"\n";
          str += sol[i].toNeatString()+"\n";
          //str += sol[i].ampList.toNeatString()+"\n\n"; // just list count, for Jiggle origin those associated with Ml, RT origin has Me, SM amps
          str += "Prefor associated amp count: " + sol[i].ampList.size()+"\n\n";
          iter = sol[i].getPrefMags().iterator();
          str +="Prefmag associated amps:\n";
          while (iter.hasNext()) {
            prefMag = (Magnitude) iter.next();
            if (prefMag.isAmpMag()) {
                str += prefMag.getNeatStringHeader()+"\n";
                str += prefMag.toNeatString()+"\n";
                str += prefMag.ampList.toNeatString(true) + "\n\n";
            }
          }
        }
        return str;
     }

/** Returns a string containing a line describing each solution followed
* by a list of codas associated with that solution and preferred magnitudes and their associated coda. */
    public String codasToString() {
        String str="";
        Solution sol[] = solList.getArray();
        Magnitude prefMag = null;
        Iterator iter = null;
        if (sol.length == 0) str += "none";
        for (int i = 0; i<sol.length; i++) {
          str += sol[i].getNeatStringHeader()+"\n";
          str += sol[i].toNeatString()+"\n";
          //str += sol[i].codaList.toNeatString()+"\n\n";
          str += "Prefor associated coda count: " + sol[i].codaList.size()+"\n\n";
          iter = sol[i].getPrefMags().iterator();
          str +="Prefmag associated codas:\n";
          while (iter.hasNext()) {
            prefMag = (Magnitude) iter.next();
            if (prefMag.isCodaMag()) {
              str += prefMag.getNeatStringHeader()+"\n";
              str += prefMag.toNeatString()+"\n";
              str += prefMag.codaList.toNeatString(true) + "\n\n";
            }
          }
        }
        return str;
     }

/** Returns a string containing a line describing each solution followed
* by a list of phases associated with that solution. */
    public String phasesToString() {

       Solution sol[] = solList.getArray();
       StringBuffer [] sbArray = new StringBuffer[sol.length];
       StringBuffer sb = null;
       PhaseList phList = null;
       int totalSize = 0;

       if (sol.length == 0) return "Solution associated phases:\nnone";

       for (int i = 0; i < sol.length; i++) {
          phList = sol[i].getPhaseList();
          sb = new StringBuffer(512+( HypoFormat.ARC_STRING_SIZE*phList.size() ));
          //sb.append( sol[i].getNeatStringHeader() );
          //sb.append("\n");
          //sb.append( sol[i].toNeatString() );
          sb.append(HypoFormat.toSummaryString(sol[i]));
          sb.append("\n");
          // This is not just the raw phaseList weeded out of virtually deleted events
          sb.append( ((PhaseList) phList.getAssociatedWith(sol[i])).dumpToArcString() );
          sb.append(HypoFormat.toTerminatorString(sol[i]));
          sb.append("\n");
          sbArray[i] = sb;
          totalSize += sb.length();
       }
       sb = new StringBuffer(totalSize);
       for (int i = 0; i<sbArray.length; i++) {
          sb.append(sbArray[i]);
       }
       return sb.toString();
    }

    public String dumpToString() {

      String str = solList.size() + " origins\n";
      str += solList.dumpToString() + "\n";

      str += wfvList.size() + " waveforms\n";

      // show info about WFViews
      WFView wfv[] = wfvList.getArray();

      for ( int i=0; i < wfv.length; i++) {
        str += "\n";
        str += wfv[i].dumpToString()+"\n";
        if (wfv[i].wf == null) {
          str += " (no waveform)";
        } else {
          str += wfv[i].wf.toString() +"\n";
        }
      }
      return str;
    }

/** Dump the ChannelList to a mongo string . Each line has channel name and distance.
If a waveform exists a "~" is in the first column.<p>

Example:

<pre>
  CI ALP  HLN   9999.9
  CI ALP  HLZ   9999.9
~ CI ALV  EHZ     45.1
  CI ARV  EHZ   9999.9
</pre>
* */
public String getChannelChecklistString(ChannelList channelList)
{
  Channel[] ch = channelList.getArray();
  StringBuffer sb = new StringBuffer();
  WFView wfv;
  final Format fmt = new Format("%8.1f");

  if (ch.length == 0) {
    return "No channels in list.";
  } else {
    for (int i = 0; i < ch.length; i++)
    {
      //sb.append(ch[i].getChannelName().toNeatString());
      wfv = this.wfvList.get(ch[i]);
      if (wfv == null || !wfv.hasWaveform()) {  // no waveform
        sb.append("  ");
      } else {
        sb.append("+ ");
      }
      //sb.append(ch[i].getChannelName().toNeatString()+" "+fmt.form(ch[i].getDistance())+"\n"); // don't use slant - aww 06/11/2004
      sb.append(ch[i].getChannelName().toNeatString()+" "+fmt.form(ch[i].getHorizontalDistance())+"\n"); // aww 06/11/2004
    }
  }
  return sb.toString();
}
/**
 * Dump the MasterView to the given JTextArea
*/
public void dumpToJTextArea(JTextArea jtext)
{
    jtext.setText(dumpToString());
}

/*
  public static final class Tester {
    public static void main(String args[])
    {
        int evid;

        if (args.length <= 0)        // no args
        {
          System.out.println("Usage: java MasterView [evid])");

//          evid =  13946516 ;
          evid =  9166744;
          System.out.println("Using evid "+ evid+" as a test...");

        } else {

          Integer val = Integer.valueOf(args[0]);    // convert arg String to 'double'
          evid = (int) val.intValue();
        }

        System.out.println("Making connection...");
        DataSource db = TestDataSource.create();    // make connection
        System.out.println("DataSource = "+db.toString());

        System.out.println("Reading in current channel info...");
        String compList[] = {"EH_", "HH_", "HL_", "AS_"};
            ChannelList chanList = ChannelList.getByComponent(compList);
        MasterChannelList.set(chanList);

///        DataSourceChannelTimeModel model = new DataSourceChannelTimeModel();

        Solution sol    = Solution.create().getById(evid);
//        model.setSolution(sol);
        System.out.println(sol.toSummaryString());
        System.out.println("Making MasterView for evid = "+evid);

              MasterView mv = new MasterView();

        mv.addSolution(sol);
        mv.setSelectedSolution(sol);
        mv.setWaveformLoadMode(MasterView.LoadNone);

        mv.defineByDataSource(sol);
        //mv.defineByChannelTimeWindowModel();


        /// TEST
        System.out.println("Check list...");
        System.out.println(mv.getChannelChecklistString(chanList));

    }
  } // end of Tester
*/
}   // end of class


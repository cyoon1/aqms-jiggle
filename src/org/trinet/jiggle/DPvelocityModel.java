package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.TravelTime;
import org.trinet.jasi.VelocityModelList;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModel;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModelIF;

/** A Dialog panel for choosing the default velocity model.*/
public class DPvelocityModel extends JPanel {

    JiggleProperties props = null;

    String selectedModelName = null;

    JTextArea textArea = null;
    JComboBox modelBox = null;

    public DPvelocityModel(JiggleProperties properties) {
        props = properties;
        try  {
            init();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private void init() throws Exception {
        setLayout(new BorderLayout());

        JPanel modelPanel = new JPanel(new BorderLayout());
        Box vbox = Box.createVerticalBox();
        vbox.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),"Current Velocity Model"));

        textArea = new JTextArea();
        new JTextClipboardPopupMenu(textArea);
        textArea.setEditable(false); // until we come up with property parser for text -aww 09/20/2007
        textArea.setBackground(this.getBackground()); // used to be Color.white
        JScrollPane jsp = new JScrollPane(textArea);
        jsp.setMaximumSize(new Dimension(220,160));

        JLabel jlbl = new JLabel("Model Name");
        jlbl.setMaximumSize(new Dimension(100,20));
        jlbl.setToolTipText("Choose a velocity model to use for defining time windows");
        Box hbox = Box.createHorizontalBox();
        hbox.add(Box.createHorizontalGlue());
        hbox.add(jlbl);
        hbox.add(makeModelBox());  // must do box after text area created to get text properties displayed
        hbox.add(Box.createHorizontalGlue());

        vbox.add(hbox);
        vbox.add(jsp);

        JButton jb = new JButton("Edit");
        jb.setToolTipText("Edit/create  velocity model via input dialog");
        jb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                createModelFromInputDialog();
            }
        });

        jb.setMaximumSize(new Dimension(100,20));
        hbox = Box.createHorizontalBox();
        hbox.add(Box.createHorizontalGlue());
        hbox.add(jb);
        hbox.add(Box.createHorizontalGlue());

        vbox.add(hbox);

        this.add(vbox, BorderLayout.CENTER);
    }

    private void createModelFromInputDialog() {

        JPanel panel = new JPanel(new GridLayout(0,2));
        JLabel jlbl = new JLabel("Velocity model name ");
        panel.add(jlbl);
        JTextField modelNameField = new JTextField();
        modelNameField.setText(selectedModelName);
        panel.add(modelNameField);

        String modelPrefix = "velocityModel." + selectedModelName;

        jlbl = new JLabel("P/S velocity ratio ");
        panel.add(jlbl);
        JTextField psRatioField = new JTextField();
        psRatioField.setText(props.getProperty(modelPrefix + ".psRatio",""));
        panel.add(psRatioField);

        jlbl = new JLabel("Top of layer kms depth ");
        panel.add(jlbl);
        JTextField depthsField = new JTextField();
        depthsField.setText(props.getProperty(modelPrefix + ".depths",""));
        panel.add(depthsField);

        jlbl = new JLabel("Top of layer velocity ");
        panel.add(jlbl);
        JTextField velocitiesField = new JTextField();
        velocitiesField.setText(props.getProperty(modelPrefix + ".velocities",""));
        panel.add(velocitiesField);

        panel.setMaximumSize(new Dimension(350,80));

        int answer = JOptionPane.showConfirmDialog(
                DPvelocityModel.this.getParent(),
                panel,
                "Enter new velocity model data",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE
        );

        if (answer == JOptionPane.OK_OPTION) {
            String modelName = modelNameField.getText();
            String ratioStr = null;
            String depthStr = null;
            String velStr = null;

            boolean status = false;
            if (modelName != null && modelName != "") {
              status = true;
              for (;;) {
                ratioStr = psRatioField.getText();
                if (ratioStr == null || ratioStr == "") {
                    status = false;
                    break; 
                }

                depthStr = depthsField.getText();
                if (depthStr == null || depthStr == "") {
                    status = false;
                    break; 
                }

                velStr = velocitiesField.getText();
                if (velStr == null || velStr == "") {
                    status = false;
                    break; 
                }
                break;
              }
            }
            if (status) {
              String modelListStr = props.getProperty("velocityModelList");
              if (modelListStr == null) modelListStr = modelName;
              else if (modelListStr.indexOf(modelName) < 0) {
                  modelListStr += " " + modelName;
              }
              props.setProperty("velocityModelList", modelListStr); 

              modelPrefix = "velocityModel." + modelName;
              GenericPropertyList gpl = new GenericPropertyList();
              gpl.setProperty(modelPrefix + ".psRatio", ratioStr);
              gpl.setProperty(modelPrefix + ".depths", depthStr);
              gpl.setProperty(modelPrefix + ".velocities", velStr);
              UniformFlatLayerVelocityModel model = UniformFlatLayerVelocityModel.getNamedModelFromProps(modelName, gpl);
              if (model == null) status = false;
              else {
                  props.addVelocityModel(model);
                  DefaultComboBoxModel dcm = (DefaultComboBoxModel) modelBox.getModel();
                  int idx = dcm.getIndexOf(modelName);
                  if (idx < 0) modelBox.addItem(modelName);
                  else {
                    modelBox.removeItemAt(idx);
                    modelBox.insertItemAt(modelName, idx);
                  }

                  modelBox.setSelectedItem(modelName);
                  setSelectedModelName(modelName);
              }
            }

            if (! status) {
                JOptionPane.showMessageDialog(DPvelocityModel.this.getParent(),"Missing data needed to define a new model, try again.");
            }
        }

    }


    /** Fill the JComboBox with the items in the wsgList which is the list of available models. */
    private JComboBox makeModelBox() {

        modelBox = new JComboBox();
        modelBox.setMaximumSize(new Dimension(100,20));

        VelocityModelList modelList = props.getVelocityModelList();
        if (modelList.size() == 0) { // no defined model so make a default
            UniformFlatLayerVelocityModelIF defaultModel = TravelTime.createDefaultModel();
            if (defaultModel != null) {
               modelList.addObject(defaultModel); 
               props.setDefaultVelocityModelName(defaultModel.getName());
            }
        }

        UniformFlatLayerVelocityModel model = null;
        for (int idx = 0; idx < modelList.size(); idx++ ) {
          model = (UniformFlatLayerVelocityModel) modelList.get(idx);
          if (model != null) {
            modelBox.addItem(model.getName());
            if (model == modelList.getSelected()) {
              modelBox.setSelectedItem(model.getName());
              setSelectedModelName(model.getName());
            }
          }
        }

        // add AFTER box's model is populated else adding items fires events
        modelBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setSelectedModelName((String) ((JComboBox) e.getSource()).getSelectedItem());
            }
        });

        return modelBox;
    }

    /** Set selected model to this one.*/
    protected void setSelectedModelName(String modelName) {
        if (modelName == null) return;
        selectedModelName = modelName;
        props.setDefaultVelocityModelName(selectedModelName); // sets selected model in list
        setModelText(selectedModelName);
    }

    /** Return the selected model (should be the default). */
    public UniformFlatLayerVelocityModel getSelectedModel() {
        return (UniformFlatLayerVelocityModel) props.getVelocityModelList().getByName(selectedModelName);
    }

    /** Set text area with model properties.*/
    protected void setModelText(String modelName) {
        if (textArea != null) {
            String str = props.getVelocityModelList().getByName(modelName).toString();
            textArea.setText(str);
        }
    }

}

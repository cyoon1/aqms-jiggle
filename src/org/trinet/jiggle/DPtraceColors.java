package org.trinet.jiggle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.ActiveList;

/**
 * Panel for insertion into dialog box to edit waveform seedchan trace colors.
 */
public class DPtraceColors extends JPanel {

    private JiggleProperties newProps = null;

    private ActiveList jbList = new ActiveList();

    private Box seedBox = null;

    private MyColorButton lastButton = null;

    protected boolean colorChanged = false;
    protected boolean sightColorChanged = false;
    protected boolean deltimColorChanged = false;
    protected boolean unusedPickColorChanged = false;

    public DPtraceColors(JiggleProperties props) {

      newProps = props;
      ComponentColor.fromProperties(newProps); // get user defs
      ComponentColor.toProperties(newProps); // set user defs

      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {

      Box hbox = Box.createHorizontalBox();
      Box vbox = Box.createVerticalBox();
      vbox.setBorder(BorderFactory.createTitledBorder("Add"));
      JButton jb = new JButton("Seedchan");
      jb.setToolTipText("Press to add a new seedchan color mappings");
      jb.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              addNewSeedchanButton();
          }
      });
      vbox.add(jb);

      vbox = Box.createVerticalBox();
      vbox.setBorder(BorderFactory.createTitledBorder("Seedchan"));
      addColorButtons(vbox);
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      hbox.add(vbox);
      seedBox=vbox;

      vbox = Box.createVerticalBox();
      vbox.setBorder(BorderFactory.createTitledBorder("Defaults"));
      addDefaultColorButtons(vbox);
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);

      hbox.add(vbox);
      hbox.setAlignmentY(Component.TOP_ALIGNMENT);

      this.add(hbox);
    }

    private class MyColorButton extends JButton {
        public boolean equals(Object obj) {
            if (obj instanceof MyColorButton) {
               return getActionCommand().equals(((MyColorButton)obj).getActionCommand());
            }
            return false;
        }
    }

    private void addNewSeedchanButton() {
        String input =
            JOptionPane.showInputDialog(getTopLevelAncestor(),
                "Enter first 2 letters of seedchan code",
                "New Seedchan Code Color",
                JOptionPane.PLAIN_MESSAGE);

        if (input != null && input.length() > 1) {

            String label = "color.seedchan."+ input.substring(0,2)+".panel"; 
            MyColorButton jb = null;
            boolean inList = false;
            for (int idx = 0; idx < jbList.size(); idx++) {
              jb = (MyColorButton) jbList.get(idx);
              if (jb.getActionCommand().equals(label)) {
                  inList = true;
                  break;
              }
            }
            if (inList) jb.doClick();
            else {
                seedBox.add(makeColorPropertyComponent(label, Color.white));
                seedBox.revalidate();
                if (lastButton != null) lastButton.doClick();
            }

            label = "color.seedchan."+ input.substring(0,2)+".wave"; 
            jb = null;
            inList = false;
            for (int idx = 0; idx < jbList.size(); idx++) {
              jb = (MyColorButton) jbList.get(idx);
              if (jb.getActionCommand().equals(label)) {
                  inList = true;
                  break;
              }
            }
            if (inList) jb.doClick();
            else {
                seedBox.add(makeColorPropertyComponent(label, Color.white));
                seedBox.revalidate();
                if (lastButton != null) lastButton.doClick();
            }
        }
    }
    
    private JComponent makeColorPropertyComponent(final String label, final Color color) {
        Box comp = Box.createHorizontalBox(); 
        comp.add(makeColorButtonLabel(label));
        comp.add(makeColorButton(label, color));
        comp.setAlignmentX(Component.LEFT_ALIGNMENT);
        return comp;
    }

    private JLabel makeColorButtonLabel(String label) {
        //int idx = label.lastIndexOf('.');
        int idx = 14; // "color.seedchan."
        JLabel jlbl = new JLabel(label.substring(idx+1));
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("First 2 letters of seedchan, press color to change its color");
        return jlbl;
    }
        
    private void addDefaultColorButtons(Box vbox) {

        Box hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel jlbl = new JLabel("Panel");
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for waveform panel background, press color to change color");
        hbox.add(jlbl);
        String key = "color.wfpanel.default.bg";
        Color bg = newProps.getColor(key); 
        if (bg == null) bg = ComponentColor.DEFAULT_BG;
        hbox.add(makeDefaultColorButton(key, bg, newProps.getColor("color.wfpanel.default.fg"), bg)); 
        vbox.add(hbox);
 
        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Wave "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default trace color for unknown seedchan type, press color to change its color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.fg";
        Color fg = newProps.getColor(key); 
        if (fg == null) fg = ComponentColor.DEFAULT_FG;
        hbox.add(makeDefaultColorButton(key, fg, fg, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Selected "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for selected waveform background, press color to change color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.selected";
        Color cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_SELECTED;
        hbox.add(makeDefaultColorButton(key, cc, fg, cc));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Highlight "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for selected time box, press color to change color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.highlight";
        cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_HIGHLIGHT;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Drag "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for drag background, press color to change color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.drag";
        cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_DRAG;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Segment "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for segment gap lines, press color to change color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.segment";
        cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_SEGMENT;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Cue "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for phase cues, press color to change color");
        hbox.add(jlbl);
        key = "color.wfpanel.default.cue";
        cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_CUE;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Sight line "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for bias and vertical timing sight lines in picking panel, press color to change color");
        hbox.add(jlbl);
        key = "color.pick.sightline";
        cc = newProps.getColor(key); 
        if (cc == null) cc = ComponentColor.DEFAULT_SIGHTLINE;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


        //
        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Deltim "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for pick deltim/residual bars straddling picks, press color to change color");
        hbox.add(jlbl);
        key = "color.pick.deltim";
        cc = newProps.getColor(key); 
        if (cc == null) cc = PickFlag.DEFAULT_DELTIM_COLOR;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        //
        //
        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("Unused "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color for non-contributing picks, press color to change color");
        hbox.add(jlbl);
        key = "color.pick.unused";
        cc = newProps.getColor(key); 
        if (cc == null) cc = PickFlag.DEFAULT_UNUSED_PICK_COLOR;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        //
        // Color of text for panel channel labels
        //wfpanel.channelLabel.font.fg=ff000000
        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("SNCL "); 
        jlbl.setPreferredSize(new Dimension(64,14));
        jlbl.setMaximumSize(new Dimension(64,14));
        jlbl.setToolTipText("Default color of waveform panel channel labels, press color to change color");
        hbox.add(jlbl);
        key = "wfpanel.channelLabel.font.fg";
        //key = "color.wfpanel.channelLabel.fg";
        cc = newProps.getColor(key); 
        if (cc == null) cc = Color.black;
        hbox.add(makeDefaultColorButton(key, cc, cc, bg));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        //

    }

    private MyColorButton makeColorButton(final String label, final Color inColor) {

        final boolean isPanel = label.endsWith(".panel");
        final boolean isWave = label.endsWith(".wave");

        final MyColorButton jb = new MyColorButton();
        jb.setActionCommand(label);
        jb.setBackground(inColor);
        jb.setToolTipText(Integer.toHexString(inColor.getRGB()));
        jb.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
                  final JColorChooser jcc = new JColorChooser(inColor);
                  final class MyJLabel extends JLabel implements ChangeListener {

                      public MyJLabel() { super(); }
                      public MyJLabel(String str) { super(str); }

                      public void stateChanged(ChangeEvent evt) {
                          if (isPanel) MyJLabel.this.setBackground(jcc.getSelectionModel().getSelectedColor());
                          else MyJLabel.this.setForeground(jcc.getSelectionModel().getSelectedColor());
                          MyJLabel.this.repaint();
                      }
                  };
                  final MyJLabel jlbl2 = new MyJLabel("==========||||||||||||||||||||||||||==========");
                  JPanel jp = new JPanel();
                  jp.setBorder( BorderFactory.createTitledBorder(jb.getActionCommand()) );

                  if (isWave) {
                      jlbl2.setForeground(jcc.getColor());
                  }
                  else if (isPanel) {
                      jlbl2.setBackground(jcc.getColor());
                  }
                  else {
                      jlbl2.setForeground(jcc.getColor());
                  }

                  int keyIdx = label.lastIndexOf(".");
                  String key =  label.substring(0, keyIdx);
                  if (isWave) {
                      key += ".panel";
                  }
                  else if (isPanel) {
                      key += ".wave";
                  }
                  //System.out.println("DEBUG DPtraceColors key is : " + key);

                  Color ccolor = newProps.getColor(key);
                  if (label.endsWith(".wave")) {
                      jlbl2.setBackground(((ccolor == null) ? Color.white : ccolor));
                  }
                  else if (label.endsWith(".panel")) {
                      jlbl2.setForeground(((ccolor == null) ? Color.white : ccolor));
                  }
                  else {
                      jlbl2.setBackground(((ccolor == null) ? Color.white : ccolor));
                  }

                  jlbl2.setOpaque(true);
                  jlbl2.setPreferredSize(new Dimension(275,20));
                  jp.add(jlbl2);
                  jcc.getSelectionModel().addChangeListener(jlbl2);
                  jcc.setPreviewPanel(jp);
                  JDialog jd = jcc.createDialog(getTopLevelAncestor(), "Select color for "+label, true, jcc,
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) {
                                  Color newColor = jcc.getColor();
                                  if (! newColor.equals(inColor)) {
                                      colorChanged = true;
                                      newProps.setProperty(label, newColor);
                                      jb.setBackground(newColor);
                                      jb.setToolTipText(Integer.toHexString(newColor.getRGB()));
                                  }
                              }
                          },
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) { }
                          }
                  );
                  jd.pack();
                  jd.setSize(500,600);
                  jd.setVisible(true);
                }
                catch(HeadlessException ex) {}
            }
        });

        lastButton = jb;
        jbList.addOrReplace(jb);
        jb.setPreferredSize(new Dimension(36,14));
        jb.setMaximumSize(new Dimension(36,14));

        return jb;
    }

    private MyColorButton makeDefaultColorButton(final String label, final Color inColor, final Color colorFG, final Color colorBG) {

        final MyColorButton jb = new MyColorButton();
        jb.setActionCommand(label);
        jb.setBackground(inColor);
        jb.setToolTipText(Integer.toHexString(inColor.getRGB()));
        jb.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
                  final JColorChooser jcc = new JColorChooser(inColor);
                  final class MyJLabel extends JLabel implements ChangeListener {

                      public MyJLabel() { super(); }
                      public MyJLabel(String str) { super(str); }

                      public void stateChanged(ChangeEvent evt) {
                          Color newColor = jcc.getSelectionModel().getSelectedColor();
                          if (inColor.equals(colorBG)) MyJLabel.this.setBackground(newColor);
                          else MyJLabel.this.setForeground(newColor);
                          MyJLabel.this.repaint();
                      }
                  };
                  final MyJLabel jlbl2 = new MyJLabel("==========||||||||||||||||||||||||||==========");
                  JPanel jp = new JPanel();
                  jp.setBorder( BorderFactory.createTitledBorder(jb.getActionCommand()) );

                  jlbl2.setForeground(colorFG);
                  jlbl2.setBackground(colorBG);
                  jlbl2.setOpaque(true);
                  jlbl2.setPreferredSize(new Dimension(275,20));
                  jp.add(jlbl2);
                  jcc.getSelectionModel().addChangeListener(jlbl2);
                  jcc.setPreviewPanel(jp);
                  JDialog jd = jcc.createDialog(getTopLevelAncestor(), "Select color for "+label, true, jcc,
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) {
                                  Color newColor = jcc.getColor();
                                  if (! newColor.equals(inColor)) {
                                      if (jb.getActionCommand().equals("color.pick.sightline")) {
                                          sightColorChanged = true;
                                      }
                                      else if (jb.getActionCommand().equals("color.pick.deltim")) {
                                          deltimColorChanged = true;
                                          // Until Java1.7 ColorChooser has alpha selection, use fixed value alpha=255 xFF for opaque -aww
                                          newColor = new Color(newColor.getRed(),newColor.getGreen(),newColor.getBlue(), 255);
                                      }
                                      else if (jb.getActionCommand().equals("color.pick.unused")) {
                                          unusedPickColorChanged = true;
                                          // Until Java1.7 ColorChooser has alpha selection, use fixed value alpha=100 (hex=64) for transparency -aww
                                          newColor = new Color(newColor.getRed(),newColor.getGreen(),newColor.getBlue(), 100);
                                      }
                                      else colorChanged = true;

                                      newProps.setProperty(label, newColor);
                                      jb.setBackground(newColor);
                                      jb.setToolTipText(Integer.toHexString(newColor.getRGB()));
                                  }
                              }
                          },
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) { }
                          }
                  );
                  jd.pack();
                  jd.setSize(500,600);
                  jd.setVisible(true);
                }
                catch(HeadlessException ex) {}
            }
        });

        lastButton = jb;
        jb.setPreferredSize(new Dimension(36,14));
        jb.setMaximumSize(new Dimension(36,14));
        jbList.addOrReplace(jb);

        return jb;
    }

    private void addColorButtons(Box box) {
        int idx = -1;
        String key = null;
        jbList.clear();

        boolean newStyle = false;
        for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
            key = (String) e.nextElement();
            if (key.indexOf(".panel") > 0 || key.indexOf(".wave") > 0) {
                newStyle = true;
                break;
            }
        }

        for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
            key = (String) e.nextElement();
            if (newStyle && key.length() <= 18) continue; // skip old style entry
            idx = key.indexOf("color.seedchan");
            if (idx >= 0) {
                // getColor() returns null if unparseable
                box.add(makeColorPropertyComponent(key, newProps.getColor(key)));
            }
        }
    }

    protected void setColorProperties() {
        JButton jb = null;
        for (int idx=0; idx < jbList.size(); idx++) {
            jb = (JButton) jbList.get(idx);
            newProps.setProperty(jb.getActionCommand(), jb.getBackground());
            //ComponentColor.put(jb.getActionCommand().substring(15).intern(), jb.getBackground());
        }
    }

    protected void resetComponentColorMap() {
        newProps.loadComponentColors();
        //System.out.println(ComponentColor.toPropertyString());
    }

}

package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import org.trinet.util.locationengines.*;

public class DPLocationServer extends JPanel {

    JiggleProperties props = null;

    JTextField locEngService = null;
    JTextField locEngType = null;
    JTextField locEngTimeout = null;
    JTextField locEngUrl = null;
    JTextField locEngPort = null;

    JCheckBox usePRTCk = null;
    JCheckBox trialLocCk = null;
    JCheckBox trialTimeCk = null;
    JCheckBox fixQuarryCk = null;
    JCheckBox cmdFileEdtCk = null;

    JTextField quarryFixDepthValueField; // added - aww 2008/06/16

    //JLabel serviceLabel = new JLabel();

    JList locServiceDescList = null;

    LocationServerGroup lsg = null;

    protected boolean hasChanged = false;

    public DPLocationServer(JiggleProperties properties) {

        props = properties;

        try {
          initGraphics();
        }
        catch(Exception ex) {
          ex.printStackTrace();
        }
    }




    private JPanel createEditPanel(boolean newService) {

        LocationServiceDescIF lsd = (LocationServiceDescIF) locServiceDescList.getSelectedValue();

        JPanel jp = new JPanel();
        String str = null;
        JLabel     label0 = new JLabel("Service: ", JLabel.RIGHT);
        str = (lsd == null) ? "" : lsd.getServiceName();
        locEngService = new JTextField(str);
        locEngService.setEditable(newService);

        JLabel     label1 = new JLabel("Type: ", JLabel.RIGHT);
        str = (lsd == null) ? "" : lsd.getLocatorName();
        locEngType = new JTextField(str);

        JLabel     label2 = new JLabel("IP-address: ", JLabel.RIGHT);
        str = (lsd == null) ? "" : lsd.getIPAddress();
        locEngUrl = new JTextField(str);

        JLabel     label3 = new JLabel("Port #: ", JLabel.RIGHT);
        str = (lsd == null) ? "" : String.valueOf(lsd.getPort());
        locEngPort = new JTextField(str); 

        JLabel     label4 = new JLabel("Timeout ms: ", JLabel.RIGHT);
        str = (lsd == null) ? "" : String.valueOf(lsd.getServerTimeoutMillis());
        locEngTimeout = new JTextField(str);

        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        setBorder( new TitledBorder("Location Engine Setup") );

        JPanel fieldPanel = new JPanel();
        fieldPanel.setLayout(new GridLayout(0, 2));
        fieldPanel.add(label0);
        fieldPanel.add(locEngService);
        fieldPanel.add(label1);
        fieldPanel.add(locEngType);
        fieldPanel.add(label2);
        fieldPanel.add(locEngUrl);
        fieldPanel.add(label3);
        fieldPanel.add(locEngPort);
        fieldPanel.add(label4);
        fieldPanel.add(locEngTimeout);

        jp.add(fieldPanel);

        return jp;
    }
/*
// Solution server instructions:
// Need action buttons for get/set, init configuration through property settings (locationServerXXX)
// allow user to edit properties them in GUI, then use buttons to change:
|GetInstName| - get name of the current instruction file (send command by button action return to field?)
|GetInstFile| - get contents of currently set instruction file (to JTextArea)
|GetInstFileList| - return list of available instruction files, one per line (to JList or JTextArea)
|GetInstFile| <filename> - return contents of this instruction file (to JTextArea)
|PutInstFile| <filename> - write text until |EOT| or |EOF to "filename" instruction file (text from JTextArea) << -- Dont' allow user to do this ?
|SetInstFile| <filename> - set use of named instruction file for the duration of the connection (from JList or field)
|SetInstFile| - set instruction file back to default (send command by button action)
*/

    private void initGraphics() throws Exception {

        Box checkPanel = Box.createVerticalBox();
        checkPanel.setBorder(BorderFactory.createEtchedBorder());

        trialLocCk = new JCheckBox("Use Trial Location", props.getBoolean("useTrialLocation"));
        trialLocCk.setAlignmentX(Component.LEFT_ALIGNMENT);

        trialTimeCk = new JCheckBox("Use Trial OriginTime", props.getBoolean("useTrialOriginTime"));
        trialTimeCk.setAlignmentX(Component.LEFT_ALIGNMENT);

        fixQuarryCk = new JCheckBox("Fix Quarry Depth", props.getBoolean("fixQuarryDepth"));
        fixQuarryCk.setAlignmentX(Component.LEFT_ALIGNMENT);

        usePRTCk = new JCheckBox("Auto dump HYP2000 server PRT output to text tabpane", props.getBoolean("locationEnginePRT"));
        usePRTCk.setAlignmentX(Component.LEFT_ALIGNMENT);

        cmdFileEdtCk = new JCheckBox("Enable HYP2000 alternate command file selection", props.getBoolean("hypoinvCmdFileEditing"));
        cmdFileEdtCk.setAlignmentX(Component.LEFT_ALIGNMENT);

        CheckBoxListener checkBoxListener = new CheckBoxListener();
        usePRTCk.addItemListener(checkBoxListener);
        trialLocCk.addItemListener(checkBoxListener);
        trialTimeCk.addItemListener(checkBoxListener);
        fixQuarryCk.addItemListener(checkBoxListener);
        cmdFileEdtCk.addItemListener(checkBoxListener);

        checkPanel.add(usePRTCk);
        checkPanel.add(trialLocCk);
        checkPanel.add(trialTimeCk);
        checkPanel.add(cmdFileEdtCk);

        Box hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(fixQuarryCk);

// quarry depth value // added - aww 2008/06/16
        JLabel jlbl = new JLabel("  fix depth km");
        jlbl.setToolTipText("Default fix depth for quarry event type");
        hbox.add(jlbl);
        quarryFixDepthValueField = new JTextField(6);
        quarryFixDepthValueField.setText( props.getProperty("quarryFixDepth", "0.") );
        quarryFixDepthValueField.setMaximumSize(new Dimension(30,20));
        quarryFixDepthValueField.getDocument().addDocumentListener( new DocumentListener() {
          public void insertUpdate(DocumentEvent e) {
            props.setProperty("quarryFixDepth", quarryFixDepthValueField.getText().trim());
          }
          public void removeUpdate(DocumentEvent e) {
            props.setProperty("quarryFixDepth", quarryFixDepthValueField.getText().trim());
          }
          public void changedUpdate(DocumentEvent e) {
          }
        });
        hbox.add(quarryFixDepthValueField);
        hbox.add(Box.createHorizontalGlue());
        checkPanel.add(hbox);
//

        Box vbox = Box.createVerticalBox();
        checkPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(checkPanel);

        lsg = props.getLocationServerGroup();
        lsg.updateProperties(props);

        //serviceLabel.setText(lsg.getSelectedService().toString());
        //serviceLabel.setOpaque(true);
        //serviceLabel.setBackground(Color.cyan);
        //vbox.add(serviceLabel);

        DefaultListModel model = new DefaultListModel();
        Object [] services = lsg.getServices().toArray();
        java.util.Arrays.sort(services);
        for (int idx = 0; idx < services.length; idx++) {
            model.addElement(services[idx]);
        }
        locServiceDescList = new JList(model);
        locServiceDescList.setCellRenderer(new MyCellRenderer());
        locServiceDescList.setSelectedValue(lsg.getSelectedService(), true);
        locServiceDescList.addListSelectionListener( new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) return;
                JList serviceDescList = (JList) e.getSource();
                if (serviceDescList.getSelectedIndex() == -1) return;
                lsg.setSelected( ((LocationServiceDescIF) serviceDescList.getSelectedValue()).getServiceName() );
                lsg.updateProperties(props);
                //serviceLabel.setText(lsg.getSelectedService().toString());
            }
        });
        locServiceDescList.setVisibleRowCount(3);
        locServiceDescList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        locServiceDescList.setAlignmentX(Component.LEFT_ALIGNMENT);
        JScrollPane jsp = new JScrollPane(locServiceDescList);
        jsp.setAlignmentX(Component.LEFT_ALIGNMENT);
        //jsp.setPreferredSize(new Dimension(100,25));
        vbox.add(jsp);

        JButton editButton = new JButton("Edit");
        editButton.setToolTipText("Popup dialog to edit the currently selected solution server properties.");
        editButton.addActionListener(
            new  ActionListener() {
                public void actionPerformed(ActionEvent e) {
                   int yn = JOptionPane.showConfirmDialog(getTopLevelAncestor(),createEditPanel(false),
                       "Edit Location Service", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                   if (yn == JOptionPane.YES_OPTION) {
                       String name = locEngService.getText();
                       String type = locEngType.getText();
                       int timeout = Integer.parseInt(locEngTimeout.getText());
                       String url = locEngUrl.getText();
                       int port = Integer.parseInt(locEngPort.getText());
                       LocationServiceDescIF svc = lsg.getService(name);
                       if (svc == null) {
                           JOptionPane.showMessageDialog(getTopLevelAncestor(),"No such location service: " + name);
                           return;
                       }
                       lsg.changeServiceDesc(name, type, timeout, url, port);
                       lsg.updateProperties(props);
                       locServiceDescList.repaint();
                       //serviceLabel.setText(svc.toString());
                   }

                }
            }
        );

        JButton addButton = new JButton("Add");
        addButton.setToolTipText("Popup dialog to create a new solution server to add the property list");
        addButton.addActionListener(
            new  ActionListener() {
                public void actionPerformed(ActionEvent e) {
                   int yn = JOptionPane.showConfirmDialog(getTopLevelAncestor(),createEditPanel(true),
                       "Add Location Service", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                   if (yn == JOptionPane.YES_OPTION) {
                       String name = locEngService.getText();
                       String type = locEngType.getText();
                       int timeout = Integer.parseInt(locEngTimeout.getText());
                       String url = locEngUrl.getText();
                       int port = Integer.parseInt(locEngPort.getText());
                       LocationServiceDescIF svc = lsg.getService(name);
                       if (svc != null) {
                           JOptionPane.showMessageDialog(getTopLevelAncestor(),"Use Edit button, service already exists: " + name);
                           return;
                       }
                       lsg.addService(name,type,timeout,url,port);
                       svc = lsg.getService(name);
                       DefaultListModel dlm =  (DefaultListModel) locServiceDescList.getModel();
                       Object [] services = dlm.toArray();
                       boolean added = false;
                       for (int idx = 0; idx < services.length; idx++) {
                           if (services[idx].toString().compareTo(svc.toString()) > 0) {
                               //dlm.add(locServiceDescList.getSelectedIndex(), svc); // used to be where user selected
                               dlm.add(idx, svc);
                               added = true;
                               break;
                           }
                       }
                       if (! added) dlm.addElement(svc);
                       lsg.updateProperties(props);
                       //serviceLabel.setText(svc.toString());
                   }

                }
            }
        );

        JButton deleteButton = new JButton("Delete");
        deleteButton.setToolTipText("Remove the selected solution server from property list.");
        deleteButton.addActionListener(
            new  ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    DefaultListModel model = (DefaultListModel) locServiceDescList.getModel();
                    int idx = locServiceDescList.getSelectedIndex();
                    LocationServiceDescIF lsd = (LocationServiceDescIF) model.get(idx); 
                    model.remove(idx);
                    lsg.remove(lsd);
                }
            }
        );

        Box  buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(editButton);
        buttonBox.add(addButton);
        buttonBox.add(deleteButton);
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(buttonBox);
        add(vbox);

    }

    class CheckBoxListener implements ItemListener {
    
        public void itemStateChanged(ItemEvent e) {
    
            Object source = e.getItemSelectable();
    
            if (source == usePRTCk) {
                 props.setProperty("locationEnginePRT", usePRTCk.isSelected());
            }
            else if (source == trialLocCk) {
                 props.setProperty("useTrialLocation", trialLocCk.isSelected());
            } else if (source == trialTimeCk) {
                 props.setProperty("useTrialOriginTime", trialTimeCk.isSelected());
            } else if (source == fixQuarryCk) {
                 props.setProperty("fixQuarryDepth", fixQuarryCk.isSelected());
            } else if (source == cmdFileEdtCk) {
                 props.setProperty("hypoinvCmdFileEditing", cmdFileEdtCk.isSelected());
            }
            hasChanged = true;
    
        }
    }

    class MyCellRenderer extends JLabel implements ListCellRenderer {
    
        public Component getListCellRendererComponent(JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {
            String str = null;
            if (value instanceof String) str = (String) value; 
            else str = ((LocationServiceDescIF) value).toString();
            if (str == null) str = "NULL";

            setText(str);

            if (isSelected) {
                 setBackground(list.getSelectionBackground());
                 setForeground(list.getSelectionForeground());
            }
            else {
                 setBackground(list.getBackground());
                 setForeground(list.getForeground());
            }
            setEnabled(list.isEnabled());
            setFont(list.getFont());
            setOpaque(true);
            return this;
        }
    }
}

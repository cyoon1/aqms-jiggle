package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.PhaseDescription;
import org.trinet.jasi.magmethods.AmpMagnitudeMethod;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public class DPguiLayout extends AbstractPreferencesPanel {

    public DPguiLayout(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        DecimalFormat df1 = new DecimalFormat ("###0");
        DecimalFormat df2 = new DecimalFormat ( "#0.00" );
        DecimalFormat df4 = new DecimalFormat ( "#0.0000" );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;
        ButtonGroup bg = null;
        JRadioButton jrb = null;
        ActionListener al = null;

//GUI Layout
        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Waveform Display Settings"));


//Waveform peak amp scanNoiseType =0  AVG sum ABS peaks, =1 RMS of samples, =2 AVG sum ABS samples (CODA)
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Waveform noise level derivation ");
        hbox.add(jlbl);
        bg = new ButtonGroup();
        al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String str = evt.getActionCommand();
                if (str.equals("AVG")) newProps.setProperty("scanNoiseType", "0");
                else if (str.equals("RMS")) newProps.setProperty("scanNoiseType", "1");
                else if (str.equals("AAA")) newProps.setProperty("scanNoiseType", "2");
            }
        };

        int ntype = newProps.getInt("scanNoiseType", 0);
        jrb = new JRadioButton("AVG");
        jrb.setToolTipText("Average of abs(peaks) in window");
        jrb.setSelected(ntype == 0);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("RMS");
        jrb.setToolTipText("Rms of samples in window");
        jrb.addActionListener(al);
        jrb.setSelected(ntype == 1);
        bg.add(jrb);
        hbox.add(jrb);
        jrb = new JRadioButton("AAA");
        jrb.setToolTipText("Average of abs(samples) in window");
        jrb.addActionListener(al);
        jrb.setSelected(ntype == 2);
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

//defaultZoomFilterType
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Default zoom panel filter ");
        jlbl.setToolTipText("Default filter for upper picking panel");
        hbox.add(jlbl);
        JComboBox combo = new JComboBox(new String[]{"HIGHPASS","BANDPASS","LOWPASS"});
        combo.setMaximumSize(new Dimension(100,20));
        combo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty("defaultZoomFilterType", (String)((JComboBox)evt.getSource()).getSelectedItem());
            }
        });
        combo.setToolTipText("Default filter for upper picking panel");
        combo.setSelectedItem(newProps.getProperty("defaultZoomFilterType", "HIGHPASS"));
        hbox.add(combo);
        jcb = makeUpdateCheckBox("Always use default filter", "defaultZoomFilterAlways");
        jcb.setToolTipText("True, forces use of configured default filter, not last custom filter setting");
        hbox.add(Box.createHorizontalStrut(5));
        hbox.add(jcb);

        this.add(hbox);

//clippingAmpScalar.analog
//clippingAmpScalar.digital.vel
//clippingAmpScalar.digital.acc
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Clipping amp scalar analog ");
        jlbl.setToolTipText("Suspect waveform is clipped if max amp > scalar*MaxDigitizerCounts");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("clippingAmpScalar.analog", AmpMagnitudeMethod.DEFAULT_A_CLIP )));
        jtf.getDocument().addDocumentListener(new MiscDocListener("clippingAmpScalar.analog", jtf));
        jtf.setToolTipText("Suspect waveform as clipped if max amp > scalar*maxDigitizerCounts");
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Clipping amp scalar digital vel");
        jlbl.setToolTipText("Suspect waveform is clipped if max amp > scalar*MaxDigitizerCounts");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("clippingAmpScalar.digital.vel", AmpMagnitudeMethod.DEFAULT_DV_CLIP )));
        jtf.getDocument().addDocumentListener(new MiscDocListener("clippingAmpScalar.digital.vel", jtf));
        jtf.setToolTipText("Suspect waveform as clipped if max amp > scalar*maxDigitizerCounts");
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Clipping amp scalar digital acc");
        jlbl.setToolTipText("Suspect waveform is clipped if max amp > scalar*MaxDigitizerCounts");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("clippingAmpScalar.digital.acc", AmpMagnitudeMethod.DEFAULT_DA_CLIP )));
        jtf.getDocument().addDocumentListener(new MiscDocListener("clippingAmpScalar.digital.acc", jtf));
        jtf.setToolTipText("Suspect waveform as clipped if max amp > scalar*maxDigitizerCounts");
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

// wfpanel.ampScaleFactor=1. default
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Group panel amplitude scalar ");
        jlbl.setToolTipText("Scale all panel's amplitudes up (>1) or down (<1 and >0) by this factor");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getFloat("wfpanel.ampScaleFactor", 1.f)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("wfpanel.ampScaleFactor", jtf));
        jtf.setToolTipText("Scale all panel's amplitudes up (>1) or down (<1 and >0) by this factor");
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

//wfpanel.scaleBy=gain
// min,max = noiseScalar*noiseLevel at start of trace averged over noiseScanSecs
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Set panels min,max amplitude range by");
        jlbl.setToolTipText("Method for setting the min,max amplitude bound of panels");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String value = evt.getActionCommand();
                newProps.setProperty("wfpanel.scaleBy", value);
                updateGUI = true;
            }
        };

        bg = new ButtonGroup();
        String value = newProps.getProperty("wfpanel.scaleBy", "data");
        jrb = new JRadioButton("data");
        jrb.setSelected(value.equals(jrb.getActionCommand()));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("noise");
        jrb.setSelected(value.equals(jrb.getActionCommand()));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("gain");
        jrb.setSelected(value.equals(jrb.getActionCommand()));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("units");
        jrb.setSelected(value.equals(jrb.getActionCommand()));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

//wfpanel.noiseScalar=10.
//wfpanel.noiseScanSecs=6.
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Scale by noise level ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df1.format(newProps.getFloat("wfpanel.noiseScalar", 10.f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range)");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.noiseScalar",jtf));
        jtf.setMaximumSize(new Dimension(45,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);

        jlbl = new JLabel("* noise in ");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getFloat("wfpanel.noiseScanSecs", 6.f)));
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.noiseScanSecs",jtf));
        jtf.setToolTipText("Window seconds from start of waveform timeseries for noise calculation");
        jtf.setMaximumSize(new Dimension(45,20));
        hbox.add(jtf);
        jlbl = new JLabel(" secs measured from start");
        hbox.add(jlbl);
        hbox.add(Box.createHorizontalGlue());

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

//wfpanel.maxVelGainScale=0.001
//wfpanel.maxAccGainScale=0.01
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Scale by gain for vel (cnts/cm/s)*");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxVelGainScale", .001f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range)");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxVelGainScale",jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);

        jlbl = new JLabel("  for acc (cnts/cm/s^2)*");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxAccGainScale", .01f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range).");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxAccGainScale",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


//wfpanel.maxCntUnitsScale=2048.
//wfpanel.maxAccUnitsScale=0.1
//wfpanel.maxVelUnitsScale=0.001
//wfpanel.maxDisUnitsScale=0.01
//wfpanel.maxUnkUnitsScale=1.
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Scale by units max vel ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxVelUnitsScale", .001f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range)");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxVelUnitsScale",jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);

        jlbl = new JLabel("  acc ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxAccUnitsScale", .1f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range).");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxAccUnitsScale",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("  dis ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxDisUnitsScale", .01f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range).");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxDisUnitsScale",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("  cnt ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df1.format(newProps.getInt("wfpanel.maxCntUnitsScale", 2048)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range).");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxCntUnitsScale",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("  unk ");
        jlbl.setToolTipText("Increase value to zoom out (increase min,max range)");
        hbox.add(jlbl);
        jtf = new JTextField(df4.format(newProps.getFloat("wfpanel.maxUnkUnitsScale", 1.f)));
        jtf.setToolTipText("Increase value to zoom out (increase min,max range).");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.maxUnkUnitsScale",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);

        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

// trace count and seconds
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Page traces ");
        hbox.add(jlbl);
        jtf = new JTextField(df1.format(newProps.getInt("tracesPerPage", 10)));
        jtf.setToolTipText("Number of panels in group scroller panel");
        jtf.getDocument().addDocumentListener(new ResetDocListener("tracesPerPage",jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        jlbl = new JLabel("  secs ");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getFloat("secsPerPage", 100.f)));
        jtf.setToolTipText("Seconds width of view in group scroller panel");
        jtf.getDocument().addDocumentListener(new ResetDocListener("secsPerPage",jtf));
        jtf.setMaximumSize(new Dimension(50,20));
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

//zoomScaleList=1.00 2.00 5.00 10.00 20.00 30.00 60.00 90.00
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Picking Panel Zoom Scale Time Windows (s) :"); 
        hbox.add(jlbl);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        hbox = Box.createHorizontalBox();
        jtf = new JTextField((newProps.getStringList("zoomScaleList")).toString(" "));
        jtf.setToolTipText("Combobox list of viewport width seconds"); 
        jtf.getDocument().addDocumentListener(new UpdateDocListener("zoomScaleList",jtf));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

 // minDragTime 
        jlbl = new JLabel ("Min drag time allowed in waveform panel ");
        jlbl.setToolTipText("Smallest time axis drag in seconds for zoom rescaling.");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getFloat("minDragTime", .01f)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("minDragTime",jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

// clock quality value
        jlbl = new JLabel ("Waveform clock quality threshold ");
        jlbl.setToolTipText("Red panel frame if qual is below this value.");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getFloat("clockQualityThreshold", .5f)));
        jtf.getDocument().addDocumentListener(new UpdateDocListener("clockQualityThreshold",jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


//codaModeFlag int value
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Coda fit type plotted on waveform panels ");
        jlbl.setToolTipText("Plot curve derived for parameters of the selected type");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String value = null;
                if (evt.getActionCommand().equals("Free")) {
                    value = "1";
                }
                else if (evt.getActionCommand().equals("Fixed")) {
                    value = "2";
                }
                else if (evt.getActionCommand().equals("Line")) {
                    value = "0";
                }
                newProps.setProperty("codaFlagMode", value);
                updateGUI = true;
            }
        };

        bg = new ButtonGroup();
        int mode = newProps.getInt("codaFlagMode", 1);
        jrb = new JRadioButton("Free");
        jrb.setSelected(mode == 1);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Fixed");
        jrb.setSelected(mode == 2);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Line");
        jrb.setSelected(mode == 0);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


        jcb = makeUpdateCheckBox("Waveform panel popupmenu flat", "wfpPopupMenuFlat");
        jcb.setToolTipText("Single menu, uncheck for nesting submenus");
        vbox.add(jcb);

        this.add(vbox);
    }
}


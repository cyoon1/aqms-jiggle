package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDbReader;
import org.trinet.jasi.EnvironmentInfo;

/**
 * Panel for insertion into dialog box for setting miscellaneous properties.
 */
public class DPdbgOutput extends AbstractPreferencesPanel {

    public DPdbgOutput(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        DecimalFormat df1 = new DecimalFormat ("###0");
        DecimalFormat df2 = new DecimalFormat ( "#0.00" );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Output Options"));
        hbox = Box.createHorizontalBox();
        jcb = makeCheckBox("Output to log file","Text output written to file not command window", "fileMessageLogging");
        hbox.add(jcb);
        jcb = makeCheckBox("Verbose", "Print extra log info messages", "verbose");
        hbox.add(jcb);
        jcb = makeCheckBox("Auto delete old logs", "Delete logs files older than max age days without confirmation dialog", "autoDeleteOldLogs");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);
        hbox = Box.createHorizontalBox();
        JLabel jbl = new JLabel("Max log age days ");
        jbl.setToolTipText("Delete log files older than max days age");
        hbox.add(jbl);
        jtf = new JTextField(5);
        jtf.setText(newProps.getProperty("maxLogAgeDays"));
        jtf.setToolTipText("Delete log files older than max days age");
        jtf.getDocument().addDocumentListener(new MiscDocListener("maxLogAgeDays", jtf));
        jtf.setMaximumSize(new Dimension(20,64));
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);
        this.add(vbox);

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Debug Options"));
        hbox = Box.createHorizontalBox();
        jcb = makeCheckBox("Debug", "Print general debugging messages and property settings", "debug");
        hbox.add(jcb);
        jcb = makeCheckBox("Loc/Mag engines", "Print debug messages from location engine and magnitude method processing", "delegateDebug");
        hbox.add(jcb);
        jcb = makeCheckBox("SQL query text", "Print database query text", "debugSQL");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jcb = makeCheckBox("TN/Commit","Print TN debug messages and database commit details", "debugCommit");
        hbox.add(jcb);
        jcb = makeCheckBox("Waveserver","Extra messages logged from waveserver connection attempts", "waveServerDebug");
        hbox.add(jcb);
        jcb = makeCheckBox("SeedReader", "List channels missing waveform timeseries", "seedReaderVerbose");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);
        this.add(vbox);

        jcb = makeCheckBox("Save properties back to disk file when program exits",
           "Overwrites user's properties file (i.e. text comments are not preserved)",
           "savePropsOnExit");
        this.add(jcb);

    }

}


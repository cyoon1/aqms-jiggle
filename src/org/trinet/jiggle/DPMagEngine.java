package org.trinet.jiggle;

import java.awt.*;
import java.io.File;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;

import org.trinet.jasi.engines.AbstractHypoMagEngineDelegate;
import org.trinet.jasi.engines.MagnitudeEngineIF;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
import org.trinet.jasi.SolutionWfEditorPropertyList;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/** A Dialog panel for reviewing/editing the magnitude method properties.
 * The magnitude method properties filenames are contained withing the JiggleProperties class.*/

public class DPMagEngine extends JPanel implements ActionListener {

    protected JiggleSolutionSolverDelegate delegate;
    private JiggleProperties newProps;

    /** The currently selected magnitude method */
    private String selectedMagMethodType = null;
    private MagnitudeEngineIF selectedMagEngine = null;
    private JTextArea engineTextArea = null;
    private JComboBox magMethodBox = null;
    private JCheckBox autoRecalcCheckBox = null;
    private JTextField mmField = null;
    /*
     private JTextField methodClassNameField = new JTextField(48);
        methodClassNameField.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty(((String)magMethodBox.getSelectedItem())+"MagMethod", methodClassNameField.getText()); 
            }
     });
    */

    private boolean hasChanged = false;

    /** Allow selection of waveform reading mode. */
    public DPMagEngine(Jiggle jiggle, JiggleProperties newProps, JiggleSolutionSolverDelegate delegate) {

        this.newProps = newProps;
        this.delegate = delegate;
           // new JiggleSolutionSolverDelegate(jiggle.solSolverDelegate, newProps);

        try {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Build the GUI. */
    private void initGraphics() throws Exception {

        setLayout(new BorderLayout());

        String [] types = new String [] { "un" };
        if (delegate != null)  {
          types = delegate.getMagMethodTypes();
          if (types == null || types.length == 0) types = new String [] { "un" };
        }
        magMethodBox = new JComboBox(types);

        int idx = ((DefaultComboBoxModel) magMethodBox.getModel()).getIndexOf(
                newProps.getProperty("defaultPreferencesMagMethodType", "ml").toLowerCase()
        );
        if (idx >= 0) magMethodBox.setSelectedIndex(idx);
        else magMethodBox.setSelectedIndex(0);
        selectedMagMethodType = (String) magMethodBox.getSelectedItem();

        JPanel selectPanel = new JPanel(new BorderLayout());
        Box hbox = Box.createHorizontalBox();
        JLabel jlbl = new JLabel("Magnitude Method ");
        hbox.add(jlbl);
        magMethodBox.setPreferredSize(new Dimension(60,30));
        magMethodBox.setMaximumSize(new Dimension(60,30));
        hbox.add(magMethodBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(Box.createHorizontalGlue());
        selectPanel.add(hbox,BorderLayout.NORTH);

        //autoRecalc Mag 
        autoRecalcCheckBox = new JCheckBox("Auto recalculate magnitude after relocation");
        autoRecalcCheckBox.setSelected(newProps.getBoolean("autoRecalc"+selectedMagMethodType.toUpperCase()));
        autoRecalcCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        autoRecalcCheckBox.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent evt) {
            newProps.setProperty("autoRecalc"+selectedMagMethodType.toUpperCase(), (evt.getStateChange() == ItemEvent.SELECTED));
          }
        });
        selectPanel.add(autoRecalcCheckBox, BorderLayout.SOUTH);

        engineTextArea = new JTextArea();
        engineTextArea.setToolTipText("Edit text to add a property or change its value, then press \"Set\"");
        engineTextArea.setEditable(true); // until we come up with property parser for text -aww 09/20/2007
        engineTextArea.setBackground(Color.white);
        engineTextArea.setRows(12);
        engineTextArea.setColumns(60);
        new JTextClipboardPopupMenu(engineTextArea);
        //engineTextArea.setPreferredSize(new Dimension(600,240));
        setEngineTextArea(selectedMagMethodType);

        // engine properties action buttons
        ActionListener actionListener =  new SetSaveLoadEngineActionHandler();
        JButton setPropsButton = new JButton("Set");
        setPropsButton.addActionListener(actionListener);
        setPropsButton.setToolTipText("Set the engine's properties from current text area values.");

        JButton loadPropsButton = new JButton("Load");
        loadPropsButton.addActionListener(actionListener); 
        loadPropsButton.setToolTipText("Read and set the engine's properties from a specified file.");

        JButton savePropsButton = new JButton("Save");
        savePropsButton.addActionListener(actionListener); 
        savePropsButton.setToolTipText("Set, then write the engine's properties as shown in text area to a specified file.");

        JButton removePropsButton = new JButton("Remove");
        removePropsButton.addActionListener(actionListener); 
        removePropsButton.setToolTipText("Remove selected/input property key (e.g. misspelled)");

        Box engineButtonBox = Box.createHorizontalBox();
        engineButtonBox.add(Box.createHorizontalGlue());
        engineButtonBox.add(setPropsButton);
        engineButtonBox.add(savePropsButton);
        engineButtonBox.add(loadPropsButton);
        engineButtonBox.add(removePropsButton);
        engineButtonBox.add(Box.createHorizontalGlue());


        TitledBorder titledBorder =
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, 
                        new Color(148, 145, 140)),"Magnitude Engine Properties");
        this.setBorder(titledBorder);
        this.setMaximumSize(new Dimension(800,350));
        this.add(selectPanel, BorderLayout.NORTH);
        JScrollPane jsp = new JScrollPane(engineTextArea);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(jsp, BorderLayout.CENTER);
        engineButtonBox.setMaximumSize(new Dimension(800,60));
        this.add(engineButtonBox, BorderLayout.SOUTH);

        // add AFTER box's magMethod is populated else adding/selected items fires events
        magMethodBox.addActionListener(DPMagEngine.this);

    }

    protected void addMagTypeChangeListener(ActionListener l) {
        magMethodBox.addActionListener(l);
    }

    public void actionPerformed(ActionEvent e) {
        JComboBox src = (JComboBox) e.getSource();
        String magType = (String) src.getSelectedItem();

        if (magType != null && !magType.equals(selectedMagMethodType) )  {

            if (src != magMethodBox) {
                magMethodBox.setSelectedItem(magType);
                return;
            }

            selectedMagMethodType = magType;
            if (autoRecalcCheckBox != null) {
                autoRecalcCheckBox.setSelected(newProps.getBoolean("autoRecalc"+selectedMagMethodType.toUpperCase()));
            }

            setEngineTextArea(selectedMagMethodType);

            if (mmField != null) {
                mmField.setText(newProps.getProperty(magType+"MagMethod"));
            }
        }
    }

    private void setEngineTextArea(String magType) {
        if (delegate != null) {
            selectedMagEngine = delegate.initMagEngineForType(selectedMagMethodType);
        }
        if (selectedMagEngine != null) {
            setMagEngineTextArea(selectedMagEngine);
        }
        else {
            String msg = (delegate == null) ?
                       "Magnitude Engine delegate null, not yet initialized by Jiggle startup." :
                       "No magnitude method is defined for type : " + selectedMagMethodType;
            engineTextArea.setText(msg);
            engineTextArea.selectAll();
        }
    }

    private boolean doSetProperties() {
        return doSetEngineProperties();
    }

    private boolean doSetEngineProperties() {
        boolean status = true;
        if (selectedMagEngine != null) {
            SolutionWfEditorPropertyList gpl = new SolutionWfEditorPropertyList(); // changed list type -aww 2008/03/26
            System.out.println("INFO: Properties dialog setting engine properties for: " + selectedMagMethodType);
            status = gpl.setProperties( engineTextArea.getText() ); // parse properties from text area string
            selectedMagEngine.setProperties(gpl);
            delegate.mapMagEnginePropertiesForType(selectedMagMethodType, gpl); // added -aww 2008/11/07 
            hasChanged = true;
        }
        return status;
    }

    protected boolean hasChanged() {
        return hasChanged;
    }

    /** Return the type of the selected magnitude method. */
    public void setSelectedMagMethodType(String type) {
        magMethodBox.setSelectedItem(type);
    }

    /** Return the type of the selected magnitude method. */
    public String getSelectedMagMethodType() {
        return selectedMagMethodType;
    }

    /** Populate the text area with magnitude engine properties.  */
    protected void setMagEngineTextArea(MagnitudeEngineIF magEngine) {
        if (engineTextArea == null) return;
        StringBuffer sb = new StringBuffer(1024);
        if (magEngine != null) {
          sb.append("# -- MagEngine Class: ").append(magEngine.getClass().getName()).append("\n");
          sb.append("#").append(magEngine.getEngineName()).append(" Properties for ").append(selectedMagMethodType).append(" MagMethod --\n");
          sb.append(magEngine.getProperties().listToString()).append("\n");
        }
        engineTextArea.setText(sb.toString());
        engineTextArea.setCaretPosition(0);
    }

    private class SetSaveLoadEngineActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

        if (selectedMagEngine == null) return;

        String cmd = evt.getActionCommand();

        try {
          boolean success = true;

          if (cmd == "Set") {
            success = doSetEngineProperties();
          }
          else {

            JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            //jfc.setFileFilter(new MyFileFilter());
            //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
            String filename = newProps.getUserFileNameFromProperty(selectedMagMethodType+"MagEngineProps");
            String oldName = filename;
            File file = null;
            if (filename != null) {
                jfc.setSelectedFile(new File(filename));
                oldName = jfc.getSelectedFile().getAbsolutePath();
            }

            if (cmd == "Save") {
              if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  doSetEngineProperties(); // assume user wants to use those saved as the current properties
                  System.out.println("INFO: Saving user edited properties for: " +
                      selectedMagMethodType + " engine to file: " + filename);
                  /*
                  if (file.exists()) {
                      file.renameTo(file.getAbsolutePath()+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));
                  }
                  */
                  selectedMagEngine.getProperties().setUserPropertiesFileName(filename);
                  success = selectedMagEngine.getProperties().saveProperties("Saved by Jiggle GUI"); 
              }
            }
            else if (cmd == "Load") {
              if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  if (! file.exists()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           filename + " file D.N.E. !", 
                           "Load MagEngine Properties", JOptionPane.ERROR_MESSAGE);
                     return;
                  }

                  System.out.println("INFO: Loading properties for: " + selectedMagMethodType +
                      " engine from file: " + filename);
                  success = selectedMagEngine.setProperties(filename);

                  delegate.mapMagEnginePropertiesForType(selectedMagMethodType, selectedMagEngine.getProperties()); // added -aww 2008/11/07 
                  hasChanged = true;
              }
            }
            else if (cmd == "Remove") {
                StringTokenizer toke =  new StringTokenizer(engineTextArea.getSelectedText(), "=:");
                String key = null;
                if (toke.countTokens() > 0) {
                    key = toke.nextToken().trim();
                }
                key = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter a property name (selected text key) to remove from list", key);
                if (key != null && key.length() > 0) {
                    selectedMagEngine.getProperties().remove(key);
                    setMagEngineTextArea(selectedMagEngine);
                }
                else return;  //no-op
            }

            // Change delegate property for property filename of this magmethod type ? 
            if (success) {
              selectedMagEngine.reset();
              if (oldName == null || ! oldName.equals(filename)) { // prompt to change jiggle.props delegate property
                if ( JOptionPane.showConfirmDialog(null,
                    "REQUIRED properties not specified in input file were set to DEFAULT values\n" +
                    "Set new filename as new default for " + selectedMagMethodType + " MagEngineProps in jiggle.props?",
                    "Update Jiggle properties",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                  //System.out.println(jfc.getCurrentDirectory().getAbsolutePath() + " : " + newProps.getUserFilePath());
                  if (jfc.getCurrentDirectory().getAbsolutePath().equals(newProps.getUserFilePath()) ) {
                      filename = jfc.getSelectedFile().getName(); // relative to jiggle USER dir
                  } // else its the full absolute directory path not in jiggle USER dir 
                  newProps.setProperty(selectedMagMethodType + "MagEngineProps",  filename);
                  hasChanged = true;
                }
              }
              else {
                // Popup below reminds user that the currently engine class sets its required properties
                // to default values when they are not defined in the loaded input file
                JOptionPane.showMessageDialog(getTopLevelAncestor(),
                    "REQUIRED properties not specified in input file were set to DEFAULT values",
                    "Load MagEngine Properties", JOptionPane.PLAIN_MESSAGE);
              }
            }
          }
          if (! success) notifyFailure(cmd, "Engine");

          // setSelectedItem fires actionEvent and this forces listening magMethodBox to refresh everything 
          // magMethodBox.setSelectedItem(selectedMagMethodType);
          setMagEngineTextArea(selectedMagEngine);

        }
        catch(Exception ex) {
          ex.printStackTrace();
          notifyFailure(cmd, "Engine");
        }
      }

    }

    private void notifyFailure(String cmd, String type) {
        JOptionPane.showMessageDialog(null,
            cmd + " properties failed , check text and messages!",
            "Magnitude " + type + " Properties", JOptionPane.PLAIN_MESSAGE);
    }

    private class MyFileFilter extends javax.swing.filechooser.FileFilter {

        public String getDescription() {
            return ".props";
        }

        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            String str = f.getName();
            int idx = str.lastIndexOf('.');
            String ext = null;
            if (idx > 0 &&  idx < str.length() - 1) {
                ext = str.substring(idx+1).toLowerCase();
            }
            if (ext != null) {
                if (ext.equalsIgnoreCase("props")) return true;
            }
            return false;
        }
    }

}

package org.trinet.jiggle;

import java.util.Observable;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Dimension;
import javax.swing.JViewport;
import javax.swing.JScrollPane;

//import java.applet.*;
import java.text.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.util.TimeSpan;
//import org.trinet.util.LeapSeconds; // for debug output

/**
 * An active panel that can change scale dynamically. It must be contained in a
 * JScrollPane which manages a JViewport through which part of this component is
 * visible at any given time.  "Zooming" this WFPanel involves resizing it so
 * that the part of it that can be seen in the JViewport is at the desired
 * scale. It rescales on every repaint to accomodate resizes of the containing
 * component.<br>
 *
 * Scaling and centering are done relative to the MasterWFVModel WFSelectionBox.
 *
 * @See: javax.swing.JScrollPane */

public class ZoomableWFPanel extends ActiveWFPanel {
    // ActiveWFPanel extends WFPanel & implements Scrollable
    // Could over-ride the Scrollable methods here to customize behavior.

    /** JScrollPane that manages a JViewport that represents the visible * part of this WFPanel. */
    private JScrollPane scrollPane;

    private boolean adjustingViewport = false;

    // for centering on viewport 
    private int oldX = 0;
    private int oldY = 0;
    //private double oldStartTime = 0.;

    protected static double horzBlockScrollFactor = .05;  // added - aww 2009/10/06
    protected static double vertBlockScrollFactor = .10;  // added - aww 2009/10/06

    protected static boolean paintBiasLine = false;
    protected static boolean paintZeroLine = false;
    protected static float biasLineWidth = 2.f; 
    protected Color biasLineColor = Color.gray; // or try cue.color.darker() ?

    // listen for changes to selected channel or time/amp window
    // Overrides ActiveWFPanel's listeners.
    private WFWindowListener wfwChangeListener = new WFWindowListener();
    private WFViewListener   wfvChangeListener = new WFViewListener();

    /**
     * Constructor requires a JScrollPane so that the WFSelectionBox of the
     * ZoomableWFPanel can be scaled
     * to fit in the JViewport of the JScrollPane.
     */
    public ZoomableWFPanel(JScrollPane scrollPane, MasterView mv) {

        super();   // super() adds the superclass's mouse listeners
        // this is different from ActiveWFPanel which only adds listeners when it's selected. We are ALWAYS active.
        addListeners(mv);

        setJScrollPane(scrollPane);

        // some behavior flags specific to Zoomable panels
        showTimeScale = false;
        showChannelLabel = false;

    }

    /** Sets the JScrollPane that will control this panel. And adds this
     * panel to the JScrollPane's JViewport. */
    public void setJScrollPane(JScrollPane scrollPane) {
         this.scrollPane = scrollPane;
         if (scrollPane != null) {
           scrollPane.getViewport().setView(this);

         }
    }
    public void setViewport(JViewport viewport) {
      scrollPane.setViewport(viewport);
    }
    public JViewport getViewport() {
      return scrollPane.getViewport();
    }

    /**
     * Tell Layout managers etal what size we want to be. This panel's size is based on
     * scaling the MasterWFVModel's WFSelectionBox to the current JViewport's size.
     */
    // This solved several problems that resulted in missing panels because they
    // dimensions of (0, 0)

    public Dimension getPreferredSize() {
        WFSelectionBox selBox =
              wfv.mv.masterWFWindowModel.getSelectionBox(getWf());

        // This is a simple proportion calc.  Wp/Wv = Tp/Tv :. Wp = Wv*(Tp/Tv)
        double sSize = selBox.getTimeSize();
        // Avoid divide by zero exceptions - aww 10/16/2006
        double ratio = (sSize == 0.) ?  1. : panelBox.getTimeSize()/sSize;
        int newWidth = (int) (getViewport().getWidth() * ratio);

        sSize = selBox.getAmpSize();
        ratio = (sSize == 0.) ?  1. : panelBox.getAmpSize()/sSize;
        int newHeight = (int) (getViewport().getHeight() * ratio);

        // make sure we don't get too small
        newWidth  = (int) Math.max (newWidth,  getMinimumSize().getWidth());
        newHeight = (int) Math.max (newHeight, getMinimumSize().getHeight());
        //System.out.println("ZWFP ratio: " +ratio+ " w: " +newWidth+ " h: " +newHeight);

        return new Dimension(newWidth, newHeight);
    }

    /** Returns the minimum size for this component in pixels. It can't shink smaller then
     * the the viewport, :. the min height = the viewport height. However, the
     * width must be TWICE the viewport width to allow the first and last point
     * in the seismogram to be centered on the bombsight. <p>
     *
     <tt>
     *<-----JViewport------>
     *+--------------------+********************
     *|         |          |          .        *
     *|         |<----seismogram----->.        *
     *|         |/\/\/\/\/\/\/\/\/\/\/.        *
     *|         |          |          .        *
     *|         |          |          .        *
     *+--------------------+********************
     *<-------------ZoomableWFPanel------------>
     </tt> */
    public Dimension getMinimumSize() {
        return new Dimension(getViewport().getWidth() * 2, getViewport().getHeight());
    }

    /**
    * Set the size of the panel box size have "insets" or white space around the
    * edge so you can center the first and last samples.
    */
    public void setPanelBoxSize() {
        if (wfv == null) return;
        TimeSpan ts =  wfv.viewSpan;
        double pad = ts.getDuration()/2.0;
        TimeSpan tsNew = new TimeSpan(ts.getStart() - pad, ts.getEnd() + pad);

        Waveform wf = getWf();
        if (wf != null)  {
          panelBox.set(tsNew, wf.getMaxAmp(), wf.getMinAmp());
        }
        else {
          // box must have non-zero dimension else /0 error happen elsewhere
          panelBox.set(tsNew, 1, 0);
        }
        //System.out.println("ZWP setPanelBoxSize() scalePanel");
        scalePanel();
        repaint();
    }
    /** Override so the background is NOT painted in the high color. */
    public void paintBackground(Graphics g) {
      //System.out.println("Painting background of ZoomableWFPanel.");
      setSelected(false);
      super.paintBackground(g);
    }

    public void paintTimeSeries(Graphics g) {
        super.paintTimeSeries(g);
        paintBiasZeroLines(g);
    }

    private void paintBiasZeroLines(Graphics g) {
        Rectangle vr = getViewport().getViewRect();
        if (vr == null) return;
        Graphics2D  g2d = (Graphics2D) g;
        Color oldColor = g2d.getColor();  // remember current color to reset to later
        Stroke oldStroke = g2d.getStroke();  // remember current stroke to reset to later

        BasicStroke bsBias = new BasicStroke(biasLineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.f, new float [] {5.f, 5.f}, 5);
        BasicStroke bsZero = new BasicStroke(biasLineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.f, new float [] {5.f, 5.f}, 5);
        BasicStroke bsOff = new BasicStroke(biasLineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.f, new float [] {10.f, 10.f}, 5);
        
        if (paintZeroLine) {
            // x,y        x+width,y
            // x,y+height x+width,y+height
          g2d.setColor(biasLineColor);
          int myPixel = pixelOfAmp(0.);
          int yPixel = Math.max(vr.y+2, myPixel); 
          yPixel = Math.min(vr.y+vr.height-2, yPixel);
          g2d.setStroke((myPixel == yPixel) ? bsZero : bsOff);
          g2d.drawLine(vr.x, yPixel, vr.x+vr.width, yPixel); // amp value zero
        }
        if (paintBiasLine) {
          g2d.setColor(biasLineColor);
          int myPixel = pixelOfAmp( (getWf() != null) ? getWf().getBias() : 0.);
          int yPixel = Math.max(vr.y+2, myPixel); 
          yPixel = Math.min(vr.y+vr.height-2, yPixel);
          g2d.setStroke((myPixel == yPixel) ? bsBias : bsOff);
          g2d.drawLine(vr.x, yPixel, vr.x+vr.width, yPixel); // bias shifted
        }
        g2d.setColor(oldColor);
        g2d.setStroke(oldStroke);
    }


    /** Return a WFSelectionBox representing the part of this WFPanel that is
    * visible in the ViewPort. */
    public WFSelectionBox getVisibleBox() {

        Rectangle vrect = getViewport().getViewRect();

        double maxAmp = ampOfPixel((int)Math.round(vrect.getMinY()));
        double minAmp = ampOfPixel((int)Math.round(vrect.getMaxY()));

        double startTime = dtOfPixel((int)Math.round(vrect.getMinX()));
        double endTime   = dtOfPixel((int)Math.round(vrect.getMaxX()));

        return new WFSelectionBox(startTime, endTime,
                                  maxAmp, minAmp);
    }

    /**
     * Change the size of the MasterWFVModel WFSelectionBox by a scaling factor
     * which forces the panel to rescale. A magnification of "2" plots twice as big
     * while 0.5 plots half as big. This really just changes the panel's size and
     * recalculates scaling factors.  */
    public void setScale(double magX, double magY) {
        if (wfv == null) return;

        // resize the selectionBox, it expands equally in both directions so that the
        // center in time and amp are the same. The panel will be automatically resized
        // Note that when you EXPAND the scale by 2x you are actually SHRINKING the
        // selection area by 1/2, :. 1.0/magX

        WFSelectionBox selBox = wfv.mv.masterWFWindowModel.getSelectionBox(getWf());
        // do x (time) scale
        if (magX != 1.0 ) {
            double oldCenterTime = selBox.getCenterTime();
             // min disallows making view too small
            double newTimeSpan = Math.min(dataBox.getTimeSize(), selBox.getTimeSize()/magX);
            double halfTime = newTimeSpan/2.0;
            selBox.setTimeSpan(oldCenterTime-halfTime, oldCenterTime+halfTime);
        }

        // change y (amp) scale
        if (magY != 1.0) {
            // force it to be centered around the bias -aww 2009/03/27
            double oldCenterAmp = (paintBiasLine) ? getWf().getBias() : selBox.getCenterAmp();

            //double newAmpSize = Math.min(dataBox.getAmpSize(), selBox.getAmpSize()/magY);
            // Don't squeeze the amp scale more than the default scaling
            double newAmpSize = selBox.getAmpSize()/magY;
            double halfAmp = newAmpSize/2.0;
            selBox.setAmpSpan(oldCenterAmp+halfAmp, oldCenterAmp-halfAmp);
            //System.out.println( "ZWP centerAmp,+/-half:" +oldCenterAmp+" "+(oldCenterAmp+halfAmp) + " "+ (oldCenterAmp-halfAmp) );
        }

        //System.out.println( "ZWP setScale selBox: " + selBox);
        wfv.mv.masterWFWindowModel.set(getWf(), selBox); // notify all listeners of the change

    }

    /**
     * Set the WFPanel's size and scaling factors so that the WFSelectionBox fits in
     * the JViewport.  This overides WFPanel.scalePanel() which is called on every
     * WFPanel.repaint to accomodate window resizes, etc.  This may change the
     * aspect ratio.
     * */
    public void scalePanel() {
        if (wfv == null) return;
        synchronized (dragBox) { // this synch is most likely not needed, everything seems to be done in event thread
          // set flag so used by WindowModel listener, so scroll events don't cause an infinite event loop
          adjustingViewport = true;
          setScaleFactors(panelBox); // Set scaling factors used for painting
          centerViewportOnBox(wfv.mv.masterWFWindowModel.getSelectionBox(getWf()));
          adjustingViewport = false;
        }
    }

    /**
     * Center viewport on the given WFSelectionBox.
     */
    void centerViewportOnBox(WFSelectionBox box) {

      // This method calls vport.setViewPosition(newPos) which fires a scroll event which calls the
      // ZoomPanel.ScrollListener() which adjusts the centertime of masterWFWindowModel which notifies 
      // this panel's listener method which calls scalePanel() which calls centerViewport() AGAIN.

      // get coordinates of upper left corner of the box
      int x0 = pixelOfTime(box.getStartTime());
      int y0 = pixelOfAmp(box.getMaxAmp());

      //System.out.println("\nDEBUG ZWP centerViewportOnBox called :" + new Date());
      //System.out.println("          cVPoB oldT="+LeapSeconds.trueToString(oldStartTime)+" newT="+LeapSeconds.trueToString(box.getStartTime()));
      //System.out.println("          cVPoB pixel x old:new ="+oldX+":"+x0+" y old:new ="+oldY+":"+y0+" pixelsPerSec: "+Math.round(pixelsPerSecond));
      //System.out.println("          cVPoB size: "+getSize()+" box="+box.toString());

      //The below code is intended to stop the recursion if no real change has occured HOWEVER somehow the position is actually getting changed!
      //Perhaps "precision jitter" when the ZoomPanel.ScrollListener() recalculates the WFSelectionBox from viewport dimensions and scale factors.
      //The viewport x position always seems to get incremented by 1, thus the Math.abs(oldX-x0) <= 1 test is a test for -no change-.
      if ( Math.abs(oldX-x0) <= 1 && oldY == y0) return;    // crawls, amp moves
      // A "resize" of frame container doesn't reset viewport view position correctly with code line below enabled ! -aww
      //else if (Math.abs(oldStartTime - box.getStartTime())*pixelsPerSecond <= 1. && oldY == y0) return;

      //oldStartTime = box.getStartTime();
      oldX = x0;
      oldY = y0;

      Point newPos = new Point(x0, y0);

      //System.out.println("DEBUG ZWP          cVPoB ==>> setViewPosition(newPos) ==>>"+newPos+" startTime: "+LeapSeconds.trueToString(dtOfPixel(newPos.x)));
      getViewport().setViewPosition(newPos);   // this fires a scroll event

    }

    // Override ActiveWFPanel.removeListeners and ActiveWFPanel.addListeners
    // because panel must listen for masterWFWindowModel change events (e.g. scroll resulting)
    // and the masterWFViewModel selected WFView change events (e.g. mouse panel selection)
    /** Remove mouse listeners and MVC model */
    protected void removeListeners() {
        if (wfv == null) return;
        removeListeners(wfv.mv);
    }

    protected void removeListeners(MasterView mv) {
        if (mv == null) return;
        if (mv.masterWFViewModel != null)
            mv.masterWFViewModel.removeChangeListener(wfvChangeListener);

        if (mv.masterWFWindowModel != null)
            mv.masterWFWindowModel.removeChangeListener(wfwChangeListener);

        removeWFViewListeners();

        // phase, amp and coda listeners
        removeReadingListeners();
    }

    /** Add ReadingList listeners and MasterView model listeners */
    protected void addListeners() {
        addListeners(wfv.mv);
    }

    /** Add model listeners to this MasterView's models. */
    protected void addListeners(MasterView mv) {
        if (mv == null) return;
        // listener for selected panel changes
        if (mv.masterWFViewModel != null) {
            mv.masterWFViewModel.removeChangeListener(wfvChangeListener);
            mv.masterWFViewModel.addChangeListener(wfvChangeListener);
        }
        // use same listener for selected viewport changes
        if (mv.masterWFWindowModel != null) {;
            mv.masterWFWindowModel.removeChangeListener(wfwChangeListener);
            mv.masterWFWindowModel.addChangeListener(wfwChangeListener);
        }
        // phase, amp and coda listeners
        addReadingListeners();
    }

/** INNER CLASS: Handle changes to the selected channel. */
      private class WFViewListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            final WFView newWFV = ((SelectedWFViewModel) changeEvent.getSource()).get();
            if (SwingUtilities.isEventDispatchThread()) {
                setWFView(newWFV); // replace the WFView
                // adjust WFPanel size only if it has changed ATTEMPT TO STOP CREEP
                if (!getSize().equals(getPreferredSize())) {
                  setSize(getPreferredSize());
                }
            }
            else {
              SwingUtilities.invokeLater(
                  new Runnable() {
                      public void run() {
                          setWFView(newWFV); // replace the WFView
                          // adjust WFPanel size only if it has changed ATTEMPT TO STOP CREEP
                          if (!getSize().equals(getPreferredSize())) {
                              setSize(getPreferredSize());
                          }

                      }
                  }
              );
            }
        }
      }

/** INNER CLASS: Handle changes to the selected time/amp window. */
      private class WFWindowListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            if (SwingUtilities.isEventDispatchThread()) {
                    if (! adjustingViewport) {
                      // do "sizing" check for case of drag box change
                      if (!getSize().equals(getPreferredSize())) {
                        setSize(getPreferredSize()); // component resize listener invokes setPanelBoxSize() which should rescale but
                        //return;  // don't return because drag zoom isn't always updated correctly when selection box is made larger
                                   // for some reason it does a validation of BombsightViewport tree vs. processing mouseReleased event
                      }
                      //System.out.println(">>> ZWP window listener scalePanel adjustingViewport: " + adjustingViewport);
                      scalePanel();
                      repaint();
                    }
                    //else System.out.println(">>> ZWP noop adjustingViewport: " + adjustingViewport);
            }
            else {
              SwingUtilities.invokeLater( new Runnable() {
                public void run() {
                    if (! adjustingViewport) {
                      // do "sizing" check for case of drag box change
                      if (!getSize().equals(getPreferredSize())) {
                        setSize(getPreferredSize()); // component resize listener invokes setPanelBoxSize() which should rescale but
                        //return;  // don't return because drag zoom isn't always updated correctly when selection box is made larger
                                   // for some reason it does a validation of BombsightViewport tree vs. processing mouseReleased event
                      }
                      //System.out.println(">>> ZWP later window listener scalePanel adjustingViewport: " + adjustingViewport);
                      scalePanel();
                      repaint();
                    }
                    //else System.out.println(">>> ZWP later noop adjusting Viewport: " + adjustingViewport);
                }
              });
            }
        }
      }  // end of WFWindowListener

/** **************************************************************************************
 * Scrollable interface methods
 * These methods determine the behavior of this component if it is placed in a JScrollPane.
 * @See: Scrollable
 */

    boolean trackViewportX = false;
    boolean trackViewportY = false;

    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public boolean getScrollableTracksViewportWidth() {
        return trackViewportX;
    }

    public boolean getScrollableTracksViewportHeight() {
        return trackViewportY;
    }
    public void setScrollableTracksViewportWidth(boolean tf) {
        trackViewportX = tf;;
    }
    public void setScrollableTracksViewportHeight(boolean tf) {
        trackViewportY = tf;
    }

/**
 * Determines scrolling increment when "track" is clicked. (Coarse scrolling)
 * For WFPanel the default is 1/10 of the WFPanel dimension. This override 
 * changes the default HORIZONTAL scroll to 1/20 of the viewport width.
 * The dimension fraction to be scrolled is settable using the Jiggle properties
 * "zoom.vert.blockScrollFactor" and zoom.horz.blockScrollFactor"
 */
    public int getScrollableBlockIncrement(Rectangle visRec, int orient, int dir) {
        int move = 1;
        if (orient == SwingConstants.VERTICAL) {
          move = (int) Math.round(getViewport().getHeight() * vertBlockScrollFactor);  // settable static scalar added - aww 2009/10/06
          //System.out.println ("Vblock move = "+move);
        }
        else { // horizontal
          move = (int) Math.round(getViewport().getWidth() * horzBlockScrollFactor);  // settable static scalar added - aww 2009/10/06
          //System.out.println ("Hblock move = "+move);
        }
        if (move < 1) move = 1;
        return move;
    }

/**
 * Determines scrolling increment when arrow widgit is clicked. (Fine scrolling)
 * Default in horizontal is 1 sample or 1 pixel, whichever is larger. Default in vertical
 * is 1 pixel.
 */
    public int getScrollableUnitIncrement(Rectangle visRec, int orient, int dir) {
        //TODO: because of rounding, repeated scroll jumps cause cumulative error in whole should calc. jump amt. to be explicit to next sample.
        int jump = 1;
        if (orient == SwingConstants.VERTICAL) {
          //int jump = Math.max(1, this.getHeight()/100); // 0.01 still causes large jumps if amp scale is zoom magnified -aww
          jump = 1; // changed to 1 pixel -aww 2009/10/06
          //System.out.println("Vunit move = "+jump);
        }
        else { // horizontal
          jump = (int) Math.round(getWf().getSampleInterval() * getPixelsPerSecond());
          if (jump < 1) jump = 1;
          //System.out.println("Hunit move = "+jump);
        }
        return jump;
    }

/*
 * End of Scrollable interface methods
 ************************************************************* */
} // ZoomableWFPanel

package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.metal.*;

/**
  * This dialog can be centered. Also has a ColumnLayout
  */
public class CenteredDialog extends JDialog {
     /** Status for dialog when closed. */
    protected int returnValue = JOptionPane.CLOSED_OPTION;

    public CenteredDialog() {
        super();
    }

    public CenteredDialog(Frame frame) {
        super(frame);
    }

    public CenteredDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        centerDialog();
    }

     /** Returns status of the button that closed the dialog. */
    public int getButtonStatus() {
        return returnValue;
    }
/**
 * Center the dialog on the screen
 */
    public void centerDialog() {
        Dimension fullScreenSize = this.getToolkit().getScreenSize();
        Dimension size = this.getSize();

        boolean changedSize = false;
        if (size.height > fullScreenSize.height) {
            changedSize = true;
            size.height= fullScreenSize.height-5;
        }
        if (size.width > fullScreenSize.width) {
            changedSize = true;
            size.width= fullScreenSize.width-5;
        }
        if (changedSize) this.setSize(size);
        this.setMaximumSize(fullScreenSize);

        size.height = size.height/2;
        size.width = size.width/2;
        int y = fullScreenSize.height/2 - size.height;
        int x = fullScreenSize.width/2 - size.width;
        this.setLocation(x,y);
    }
}


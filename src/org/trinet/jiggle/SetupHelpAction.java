package org.trinet.jiggle;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

public class SetupHelpAction extends AbstractAction {

    private JDialog jd = null;

    public SetupHelpAction() {
        super("Initial setup help");
    }

    public void actionPerformed(ActionEvent e) {
        makeHelpDialog();
    }

    protected void makeHelpDialog() {
  
        if (jd != null && jd.isVisible()) {
            return;
        }
  
        JTextArea jta = new JTextArea(); 
        jta.setEditable(false);
        jta.append("For the initial setup there are a minimal set of Jiggle properties that needs to be configured.\n");
        jta.append("These properties can set either by hand-editing the user's properties file in the user's directory\n");
        jta.append("or more easily by using the Jiggle application's menubar Properties dialog.\n\n");
        jta.append("Using the 'QC Debug' preferences tab set the value of your network code, the default is unknown, '??'. \n");
        jta.append("localNetCode=??\n\n");
        jta.append("Also set your preferred the channel cache filename, the default is 'channelList.cache'\n");
        jta.append("channelCacheFilename=channelList.cache\n\n");
        jta.append("If the specifed channel cache file does not already exist a database query will build it; however,\n");
        jta.append("this query does take quite a while to complete.\n\n"); 
        jta.append("Set the request card generator channel list group name, the default is 'RCG-TRINET'\n");
        jta.append("channelGroupName=RCG-TRINET\n");
        jta.append("\n");
        jta.append("\n");
        jta.append("Using the 'Event Datasource' preferences tab configure your parametric database connection data.\n");
        jta.append("These connection parameters are defined in the properties file as the values of the properties:\n");
        jta.append("dbaseDomain=\n");
        jta.append("dbaseHost=\n");
        jta.append("dbaseName=\n");
        jta.append("dbasePort=\n");
        jta.append("dbaseUser=\n");
        jta.append("dbasePasswd=\n");
        jta.append("\n");
        jta.append("\n");
        jta.append("Using the 'Location Server' preferences tab configure the HYP2000 location server setup,\n");
        jta.append("the name the selected server to use:\n");
        jta.append("locationServerSelected=ls2\n");
        jta.append("and in this configure a list of HYP2000 location servers, specified by the abstraction:\n");
        jta.append("      'servername+type+timeoutmillis+ip_address:port'\n");
        jta.append("A properties file example specifying 3 services is shown below:\n");
        jta.append("locationServerGroupList=/ls1+HYP2000+40000+locserver.gps.caltech.edu\\:6501\\ \n");
        jta.append("                        /ls2+HYP2000+40000+locserver.gps.caltech.edu\\:6502\\ \n");
        jta.append("                        /ls3+HYP2000+40000+locserver.gps.caltech.edu\\:6503\n");
        jta.append("\n");
        jta.append("\n");
        jta.append("Using the 'Velocity Model' preferences tab configure a flat layer velocity model used to predict phase times.\n");
        jta.append("An example of the velocity model properties syntax is shown below:\n");
        jta.append("velocityModel.DEFAULT.modelName=my_model\n");
        jta.append("velocityModel.my_model.depths=0.0 5.5 16. 32.\n");
        jta.append("velocityModel.my_model.psRatio=1.73\n");
        jta.append("velocityModel.my_model.velocities=5.5 6.3 6.7 7.8\n");
        jta.append("velocityModelList=my_model\n");
        jta.append("\n");
        jta.append("\n");
        jta.append("Using the 'Wave Datasource' preferences tab configure your selected realtime waveserver group name:\n");
        jta.append("currentWaveServerGroup=rts1\n");
        jta.append("Then define the list of servers specified by :\n");
        jta.append("   groupname+maxRetries+maxTimeoutMilliSecs+verifyWaveforms+truncateAtTimeGap\n");
        jta.append("followed by a list of services of the form 'ip_address:port'\n");
        jta.append("A properties file example is shown below for two groups with two services:\n");
        jta.append("# rapid waveserver on port 6500 and a disk waveserver on port 6501\n");
        jta.append("waveServerGroupList=/rts1+1+0+false+false+rt1.gps.caltech.edu\\:6500+rt1.gps.caltech.edu\\:6501\\ \n");
        jta.append("                    /rts2+1+0+false+false+rt2.gps.caltech.edu\\:6500+rt2.gps.caltech.edu\\:6501\n");


        jta.append("\n");

        jta.setBackground(Jiggle.jiggle.getBackground());
  
        new JTextClipboardPopupMenu(jta);
  
        JOptionPane jop = new JOptionPane(new JScrollPane(jta), JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        jd = jop.createDialog(Jiggle.jiggle, "Initial setup help");
        jd.setModal(false);
        jd.pack();
        Dimension size = jd.getSize();
        Dimension screenSize = Jiggle.jiggle.getToolkit().getScreenSize();
        jd.setLocation((int)(screenSize.width - size.width)/2, (int)(screenSize.height - size.height)/2);
        jd.show();
    }
}

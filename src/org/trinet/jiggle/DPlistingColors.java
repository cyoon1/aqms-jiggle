package org.trinet.jiggle;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;

import org.trinet.jasi.ActiveList;
import org.trinet.util.Format;

/**
 * Panel for insertion into dialog box to edit waveform seedchan trace colors.
 */
public class DPlistingColors extends JPanel {


    private JiggleProperties newProps = null;

    private ActiveList jbList = new ActiveList();

    private MyColorButton lastButton = null;

    protected boolean tabColorChanged = false;
    protected boolean listColorChanged = false;

    public DPlistingColors(JiggleProperties props) {

      newProps = props;
      SelectableReadingList.fromProperties(newProps);
      SelectableReadingList.toProperties(newProps);

      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {

      Box hbox = Box.createHorizontalBox();

      Box vbox = Box.createVerticalBox();
      vbox.setBorder(BorderFactory.createTitledBorder("Data List Tab"));
      addListColorButtons(vbox);
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      hbox.add(vbox);

      vbox = Box.createVerticalBox();
      addMsgTabPaneColorButtons(vbox);
      addThresholdFields(vbox);
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      hbox.add(vbox);

      this.add(hbox);

    }

    private void addThresholdFields(JComponent jc) {

        DecimalFormat df2 = new DecimalFormat ( "#0.00" );
        Dimension d = new Dimension(30,20);
        Dimension d2 = new Dimension(120,20);

        Box vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Residual Color Thresholds"));

        //JPanel hbox = new JPanel(new GridLayout(1,2));
        Box hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        JTextField jtf = new JTextField(df2.format(newProps.getFloat("threshold.warn.ttResidual")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("threshold.warn.ttResidual", jtf));
        jtf.setPreferredSize(d);
        jtf.setMaximumSize(d);
        jtf.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel jlbl = new JLabel ("Traveltime warning");
        jlbl.setToolTipText("Warning color text if residual > this value and < outlier value");
        jlbl.setPreferredSize(d2);
        jlbl.setMaximumSize(d2);
        jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);

        hbox.add(jlbl);
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jtf = new JTextField(df2.format(newProps.getFloat("threshold.outlier.ttResidual")));
        jtf.setPreferredSize(d);
        jtf.setMaximumSize(d);
        jtf.getDocument().addDocumentListener(new MiscDocListener("threshold.outlier.ttResidual", jtf));
        jtf.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel ("Traveltime outlier");
        jlbl.setToolTipText("Outlier color text if residual > this value");
        jlbl.setPreferredSize(d2);
        jlbl.setMaximumSize(d2);
        jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);

        hbox.add(jlbl);
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jtf = new JTextField(df2.format(newProps.getFloat("threshold.warn.magResidual")));
        jtf.setPreferredSize(d);
        jtf.setMaximumSize(d);
        jtf.getDocument().addDocumentListener(new MiscDocListener("threshold.warn.magResidual", jtf));
        jtf.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel ("Magnitude warning");
        jlbl.setToolTipText("Warning color text if residual > this value and < outlier value");
        jlbl.setPreferredSize(d2);
        jlbl.setMaximumSize(d2);
        jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);

        hbox.add(jlbl);
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jtf = new JTextField(df2.format(newProps.getFloat("threshold.outlier.magResidual")));
        jtf.setPreferredSize(d);
        jtf.setMaximumSize(d);
        jtf.getDocument().addDocumentListener(new MiscDocListener("threshold.outlier.magResidual", jtf));
        jtf.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel ("Magnitude outlier");
        jlbl.setToolTipText("Outlier color text if residual > this value");
        jlbl.setPreferredSize(d2);
        jlbl.setMaximumSize(d2);
        jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);

        hbox.add(jlbl);
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());

        vbox.add(hbox);
        //vbox.setAlignmentY(Component.TOP_ALIGNMENT);

        jc.add(vbox);

    }

    private class MiscDocListener implements DocumentListener { // inner class
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) { }
          private void setValues(DocumentEvent e) {
              newProps.setProperty(prop, jtf.getText().trim());
              listColorChanged = true;
          }
    }

    private class MyColorButton extends JButton {
        public boolean equals(Object obj) {
            if (obj instanceof MyColorButton) {
               return getActionCommand().equals(((MyColorButton)obj).getActionCommand());
            }
            return false;
        }
    }

    private JComponent makeColorPropertyComponent(String label, Color inColor, Color fg, Color bg) {
        Box comp = Box.createHorizontalBox(); 
        comp.add(makeColorButtonLabel(label));
        comp.add(makeDefaultColorButton(label, inColor, fg, bg));
        comp.setAlignmentX(Component.LEFT_ALIGNMENT);
        return comp;
    }

    private JLabel makeColorButtonLabel(String label) {
        int idx = 15; // "color.readings.";
        JLabel jlbl = new JLabel(label.substring(idx));
        jlbl.setPreferredSize(new Dimension(100,16));
        jlbl.setMaximumSize(new Dimension(100,16));
        jlbl.setToolTipText("Press color to change color");
        return jlbl;
    }
        
 
    private void addMsgTabPaneColorButtons(Box panelBox) {

        Box vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Message Tab"));

        Box hboxBG = Box.createHorizontalBox();
        hboxBG.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel jlbl = new JLabel("text.bg");
        jlbl.setPreferredSize(new Dimension(150,16));
        jlbl.setMaximumSize(new Dimension(150,16));
        jlbl.setToolTipText("Background color in message tab pane, press color to change color");
        hboxBG.add(jlbl);
        String keyBG = "color.tab.text.bg";
        Color bg = newProps.getColor(keyBG); 
        if (bg == null) bg = Color.white;
 
        Box hboxFG = Box.createHorizontalBox();
        hboxFG.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("text.fg"); 
        jlbl.setPreferredSize(new Dimension(150,16));
        jlbl.setMaximumSize(new Dimension(150,16));
        jlbl.setToolTipText("Text color in message tab pane, press color to change its color");
        hboxFG.add(jlbl);
        String keyFG = "color.tab.text.fg";
        Color fg = newProps.getColor(keyFG); 
        if (fg == null) fg = Color.black;

        hboxBG.add(makeDefaultColorButton(keyBG, bg, fg, bg)); 
        vbox.add(hboxBG);

        hboxFG.add(makeDefaultColorButton(keyFG, fg, fg, bg));
        vbox.add(hboxFG);

        hboxBG = Box.createHorizontalBox();
        hboxBG.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("selection.bg ");
        jlbl.setPreferredSize(new Dimension(150,16));
        jlbl.setMaximumSize(new Dimension(150,16));
        jlbl.setToolTipText("Selection background color in message tab pane, press color to change color");
        hboxBG.add(jlbl);
        keyBG = "color.tab.text.selection.bg";
        bg = newProps.getColor(keyBG); 
        if (bg == null) bg = Color.white;
 
        hboxFG = Box.createHorizontalBox();
        hboxFG.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl = new JLabel("selection.fg "); 
        jlbl.setPreferredSize(new Dimension(150,16));
        jlbl.setMaximumSize(new Dimension(150,16));
        jlbl.setToolTipText("Selection text color in message tab pane, press color to change its color");
        hboxFG.add(jlbl);
        keyFG = "color.tab.text.selection.fg";
        fg = newProps.getColor(keyFG); 
        if (fg == null) fg = Color.black;

        hboxBG.add(makeDefaultColorButton(keyBG, bg, fg, bg)); 
        vbox.add(hboxBG);

        hboxFG.add(makeDefaultColorButton(keyFG, fg, fg, bg));
        vbox.add(hboxFG);
        
        panelBox.add(vbox);

    }

    private MyColorButton makeDefaultColorButton(final String label, final Color inColor, final Color colorFG, final Color colorBG) {

        final MyColorButton jb = new MyColorButton();
        jb.setActionCommand(label);
        jb.setBackground(inColor);
        jb.setToolTipText(Integer.toHexString(inColor.getRGB()));
        jb.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
                  final JColorChooser jcc = new JColorChooser(inColor);
                  final class MyJLabel extends JLabel implements ChangeListener {

                      public MyJLabel() { super(); }
                      public MyJLabel(String str) { super(str); }

                      public void stateChanged(ChangeEvent evt) {
                          Color newColor = jcc.getSelectionModel().getSelectedColor();
                          if (inColor.equals(colorBG)) MyJLabel.this.setBackground(newColor);
                          else MyJLabel.this.setForeground(newColor);
                          MyJLabel.this.repaint();
                      }
                  };
                  final MyJLabel jlbl2 = new MyJLabel("==========||||||||||||||||||||||||||==========");
                  JPanel jp = new JPanel();
                  jp.setBorder( BorderFactory.createTitledBorder(jb.getActionCommand()) );

                  jlbl2.setForeground(colorFG);
                  jlbl2.setBackground(colorBG);
                  jlbl2.setOpaque(true);
                  jlbl2.setPreferredSize(new Dimension(275,20));
                  jp.add(jlbl2);
                  jcc.getSelectionModel().addChangeListener(jlbl2);
                  jcc.setPreviewPanel(jp);
                  JDialog jd = jcc.createDialog(DPlistingColors.this.getParent(), "Select color for "+label, true, jcc,
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) {
                                  Color selectedColor = jcc.getColor();
                                  if (! selectedColor.equals(inColor)) {
                                      if (label.startsWith("color.tab")) tabColorChanged = true;
                                      else listColorChanged = true;
                                      //System.out.println("DEBUG DPlistColors newProps.setProperty: "+label +"="+ selectedColor.toString());
                                      newProps.setProperty(label, selectedColor);
                                      jb.setBackground(selectedColor);
                                      jb.setToolTipText(Integer.toHexString(selectedColor.getRGB()));
                                  }
                              }
                          },
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) { }
                          }
                  );
                  jd.pack();
                  jd.setSize(500,600);
                  jd.setVisible(true);
                }
                catch(HeadlessException ex) {}
            }
        });

        lastButton = jb;
        jbList.addOrReplace(jb);

        return jb;
    }

    private void addListColorButtons(JComponent jc) {
        int idx = -1;
        jbList.clear();

        Box vbox = Box.createVerticalBox();
        //vbox.setBorder(BorderFactory.createTitledBorder("Colors"));

        vbox.setAlignmentX(Component.LEFT_ALIGNMENT);

        String keyBG = "color.readings.summary.bg";
        Color bg = newProps.getColor(keyBG);
        if (bg == null) bg = SelectableReadingList.summaryBgColor;

        String keyFG = "color.readings.summary.fg";
        Color fg = newProps.getColor(keyFG);
        if (fg == null) fg = SelectableReadingList.summaryFgColor;

        vbox.add(makeColorPropertyComponent(keyBG, bg, fg, bg));
        vbox.add(makeColorPropertyComponent(keyFG, fg, fg, bg));
        vbox.add(Box.createVerticalStrut(10));

        Box hbox = Box.createHorizontalBox(); 
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        JLabel jlbl = new JLabel("list.bg ");
        jlbl.setPreferredSize(new Dimension(100,16));
        jlbl.setMaximumSize(new Dimension(100,16));
        jlbl.setToolTipText("Background of pane below cell listings, press color to change color");
        hbox.add(jlbl);
        keyBG = "color.readings.list.bg";
        bg = newProps.getColor(keyBG); 
        if (bg == null) bg = SelectableReadingList.listBgColor;
        hbox.add(makeDefaultColorButton(keyBG, bg, fg, bg));  //use summary fg color from above
        vbox.add(hbox);

        vbox.add(Box.createVerticalStrut(10));

        bg = newProps.getColor("color.readings.cell.bg");
        if (bg == null) bg = SelectableReadingList.cellBgColor;
        Color cellColor = null;
        String key = null;
        for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
            key = (String) e.nextElement();
            idx = key.indexOf("color.readings.cell.");
            if (idx >= 0) {
                cellColor = newProps.getColor(key);  // getColor() returns null if unparseable
                vbox.add(makeColorPropertyComponent(key, cellColor, cellColor, bg));
            }
        }

        //vbox.setAlignmentY(Component.TOP_ALIGNMENT);

       jc.add(vbox);
    }

    protected void setColorProperties() {
        JButton jb = null;
        for (int idx=0; idx < jbList.size(); idx++) {
            jb = (JButton) jbList.get(idx);
            newProps.setProperty(jb.getActionCommand(), jb.getBackground());
        }
    }

    protected void resetReadingListColors() {
        newProps.loadReadingListColors();
    }

}

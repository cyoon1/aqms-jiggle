package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;

import org.trinet.jasi.*;
import org.trinet.util.Format;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.GazetteerType;

public class SolutionPanel extends JToolBar implements PropertyChangeListener {

    // must keep references to these so we can remove them as observers
    SolutionListComboBox solCombo = null;
    EventTypeChooserMenu typeChooser = null;
    GTypeChooserMenu gtypeChooser = null;
    FixDepthButton fixDepthButton = null;
    JButton jbEventType = null;
    JButton jbGType = null;

    //MasterView mv;
    static Jiggle jiggle;

    /** Default to a horizontal box*/
    public SolutionPanel(Jiggle jig) {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        jiggle = jig;
        setMasterView();  // default to HorizontalBox
    }

    public void setMasterView() {
        //this.mv = mv;
        buildComponent();
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JToolBar) {
            JToolBar jtb = (JToolBar) evt.getSource();
            if (evt.getPropertyName().equals("orientation")) {
              if ( ((Integer)evt.getNewValue()).intValue() == JToolBar.VERTICAL)
                setLayout(new BoxLayout(SolutionPanel.this, BoxLayout.Y_AXIS));
              else {
                setLayout(new BoxLayout(SolutionPanel.this, BoxLayout.X_AXIS));
              }
            }
        }
    }

    /** Make/remake the SolutionPanel graphic. It is a Box with comboboxes and buttons.  */
    private void buildComponent() {

        //setBorder( new TitledBorder("Selected Solution") );

        int ypix = (jiggle.getProperties().getBoolean("miniToolBarButtons")) ? 20 : 24;
        // can't do the rest without a MasterView
        if (jiggle.mv == null || jiggle.mv.solList == null || jiggle.mv.solList.size() == 0) {
            setEnabled(false);
            if (!jiggle.getProperties().getBoolean("miniToolBarButtons"))
                add(Box.createRigidArea(new Dimension(100,ypix)));
            return;
        }

        jiggle.mv.solList.removeListDataStateListener(solCombo);  //aww
        jiggle.mv.solList.removeListDataStateListener(typeChooser);  //aww
        jiggle.mv.solList.removeListDataStateListener(fixDepthButton);  //aww
        jiggle.mv.solList.removeListDataStateListener(gtypeChooser);  //aww -2015/05/06

        this.removeAll();
   
        // [EVENT]
        solCombo = new SolutionListComboBox(jiggle.mv.solList);
        solCombo.setToolTipText("Selected event");
        solCombo.setMaximumSize(new Dimension(120,ypix));
        solCombo.setPreferredSize(new Dimension(120,ypix));
   
        // [TYPE] set selected 'type' correctly
        String etyp =  (jiggle.mv != null && jiggle.mv.getSelectedSolution() != null) ?
            EventTypeMap3.toDbType(jiggle.mv.getSelectedSolution().getEventTypeString()) : EventTypeMap3.UNKNOWN;
        String []  eventTypeChoices = jiggle.getProperties().getStringArray("eventTypeChoices");
        typeChooser = new EventTypeChooserMenu(this, eventTypeChoices, etyp);
        typeChooser.setToolTipText("Selected event type");
   
        // [GTYPE] set selected 'gtype' correctly
        String gtyp =  (jiggle.mv != null && jiggle.mv.getSelectedSolution() != null) ?
            GTypeMap.toDbType(jiggle.mv.getSelectedSolution().getOriginGTypeString()) : "";
        gtypeChooser = new GTypeChooserMenu(this, gtyp);
        gtypeChooser.setToolTipText("Selected gtype");
   
        // [FIX]
        fixDepthButton = new FixDepthButton(jiggle); // aww 06/15/2006
        fixDepthButton.addActionListener(new FixButtonHandler());
   
        jiggle.mv.solList.addListDataStateListener(solCombo); //aww
        jiggle.mv.solList.addListDataStateListener(typeChooser); //aww
        jiggle.mv.solList.addListDataStateListener(fixDepthButton); //aww
        jiggle.mv.solList.addListDataStateListener(gtypeChooser); //aww -2015/05/06
   
        this.add(solCombo);

        Solution sol = jiggle.mv.getSelectedSolution();

        jbEventType = (sol == null) ? new JButton("??") : new JButton(sol.getEventTypeString());
        jbEventType.setMaximumSize(new Dimension(100,ypix));
        jbEventType.setPreferredSize(new Dimension(100,ypix));
        jbEventType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                typeChooser.show(jbEventType, 0, 0);
            }
        });
        this.add(jbEventType);

        if (sol == null) {
          jbGType = new JButton("?");
          jbGType.setToolTipText("unknown gtype");
        }
        else {
          jbGType = new JButton(gtyp.toUpperCase());
          jbGType.setToolTipText(GTypeMap.fromDbType(gtyp) + " gtype");
        }
        jbGType.setMaximumSize(new Dimension(24,ypix));
        jbGType.setPreferredSize(new Dimension(24,ypix));
        //jbGType.setMargin(new Insets(0,0,0,0));
        jbGType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                gtypeChooser.show(jbGType, 0, 0);
            }
        });
        this.add(jbGType);
        this.add(Box.createHorizontalStrut(10));
        this.add(fixDepthButton);

    }

    protected void resetChooserTypes() {
        if (typeChooser == null) return; // on startup the masterview is not yet loaded with solution
        Solution sol = jiggle.mv.getSelectedSolution();

        String etyp = (sol == null) ? EventTypeMap3.UNKNOWN : EventTypeMap3.toDbType(sol.getEventTypeString());
        String []  eventTypeChoices = jiggle.getProperties().getStringArray("eventTypeChoices");
        typeChooser.initMenu(eventTypeChoices, etyp);
        jbEventType.setText(typeChooser.selectedEventType.name);

        String gtyp = (sol == null) ? "" : GTypeMap.toDbType(sol.getOriginGTypeString());
        gtypeChooser.initMenu(gtyp);
        jbGType.setText(gtypeChooser.selectedGType.name);
    }

    /** Enable/disable components that only make sense when there's selected event.*/
    public void setEnabled(boolean tf) {
        if (solCombo != null) solCombo.setEnabled(tf);
        if (typeChooser != null) typeChooser.setEnabled(tf);
        if (fixDepthButton != null) fixDepthButton.setEnabled(tf);
    }

    protected void setEventType() {
        Solution sol = (Solution) jiggle.mv.solList.getSelected(); //aww
        if (sol == null) {
            System.out.println("INFO: Unable to change event type, no selected solution in masterview");
            return; // no solution
        }

        if ( typeChooser.selectedEventType == null ) {
            System.out.println("INFO: Unable to change event type, the selected type is NULL");
            return;  // internal setSelected event
        }

        EventType eventType = typeChooser.selectedEventType;
        if (eventType.name.equalsIgnoreCase(sol.getEventTypeString())) {
           if (jbEventType != null && !eventType.name.equals(jbEventType.getText())) jbEventType.setText(eventType.name);
           System.out.println ("INFO: Event type not changed from : "+ eventType.name);
            return; // no change = no action
        }

        if (eventType.isDepthFixed()) {

          if (! jiggle.getProperties().getBoolean("disableQuarryCheck")) { // 01/07/2008 aww added property to override for Kate
            // Check for know quarry criteria and warn user
            if (eventType.code.equals(EventTypeMap3.QUARRY) && !sol.isQuarry()) {
              JOptionPane.showMessageDialog(jiggle,
                "Solution fails known quarry criteria test.",
                "EventTypeCheck",
                JOptionPane.PLAIN_MESSAGE);
            }
          }

          // New block for fixed depth - aww 06/15/2006
          double fixZ = eventType.fixDepth;
          if (eventType.code.equals(EventTypeMap3.QUARRY) && jiggle.getProperties().getBoolean("fixQuarryDepth")) {
              fixZ = jiggle.getProperties().getDouble("quarryFixDepth");
          }
          if (! (sol.mdepth.doubleValue() == fixZ)) { // set model depth, it's used for terminator -aww 2015/06/11
                  sol.mdepth.setValue(fixZ);
                  // NOTE: static Solution.calcOriginDatum should be enabled only for CRH CRT models (non-geoidal type)
                  double datum = sol.calcOriginDatumKm(); // returns 0. when Solution.calcOriginDatum is false (default)
                  // subtract datum correction to the fixZ mdepth here - 2015/06/12 -aww
                  sol.depth.setValue((Double.isNaN(datum)) ? fixZ : fixZ - datum);
                  sol.setStale(true);
          }
          if (sol.depthFixed.booleanValue() == false) {
                  sol.depthFixed.setValue(true);
                  sol.setStale(true);
          }
          fixDepthButton.setSolution(sol); // aww 06/15/2006

          if (eventType.code.equals(EventTypeMap3.QUARRY)) { // if its a QUARRY, set comment text to a default string
            if ( jiggle.getProperties().getBoolean("disableQuarryCheck") ) jiggle.addComment();
            else {
              WhereIsEngine whereEngine = jiggle.whereEngine;
              LatLonZ latlonz = sol.getLatLonZ();
              whereEngine.setReference(latlonz.getLat(), latlonz.getLon(), 0.); // latlonz.getZ());
              WhereSummary ws = (WhereSummary)whereEngine.whereSummaryType(GazetteerType.QUARRY);
              if (ws == null) {
                JOptionPane.showMessageDialog(jiggle,
                        "No matching row found in database GazetteerQuarry table", "Quarry Lookup", JOptionPane.PLAIN_MESSAGE);
                jiggle.addComment();
              }
              else {
                Format f = new Format("%7.1f");
                String placeString = ws.fromPlaceString(false)
                  + " " + f.form(ws.getDistanceAzimuthElevation().getDistanceKm()).trim() + " km " // added trim() -aww 2008/10/22
                  + GeoidalConvert.directionString(ws.getDistanceAzimuthElevation().getAzimuth()).trim(); // az from quarry to the event

                // must strip of " from " which the Where class always tacks on the place name
                //if (placeString.startsWith(" from")) placeString = placeString.substring(6);
                jiggle.addComment(placeString);
              }
            }
          }
          else { // else if (sol.hasComment()) { // for other types might want to change the comment
            jiggle.addComment();
          }
        }
        // for other types might want to change the existing comment
        else if (sol.hasComment()) {
          jiggle.addComment();
        }
        // for types non-local w/o comment might want to ADD a new comment
        //else if ( ! (sol.isClone() && eventType.code.equals(EventTypeMap2.LOCAL)) ) {
        else if (!eventType.code.equals(EventTypeMap2.LOCAL)) {
           jiggle.addComment();
        }

        if (! eventType.isDepthFixed() && sol.depthFixed.booleanValue() == true) {
            if (JOptionPane.showConfirmDialog(jiggle, "Unfix event depth?", "Unfix Depth",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) fixDepthButton.toggle();
        }

        sol.setEventType(eventType.name);
        String name = sol.getEventTypeString();
        System.out.println ("INFO: set event type: " + name +
                ((sol.depthFixed.booleanValue()) ? " fixed" : "") +
                " depth km: " + new Format("%4.1f").form(sol.depth.doubleValue())+
                " mdepth km:" + new Format("%4.1f").form(sol.mdepth.doubleValue())+ // added 2015/10/09 -aww
                " [" +sol.getComment() +"]");
        jbEventType.setText(name);

        if (jiggle.srlPhases != null) jiggle.srlPhases.resetSummaryTextArea(); // aww 01/07/2008

        // do this to force a change event to notify listners
        jiggle.mv.solList.setSelected(sol);
        jiggle.updateFrameTitle();
        jiggle.toFront();  // test 2008/01/10 -aww Kate's map popup to forground problem?

    }

    protected void setOriginGType() {
        Solution sol = (Solution) jiggle.mv.solList.getSelected(); //aww
        if (sol == null) {
            System.out.println("INFO: Unable to change origin gtype, no selected solution in masterview");
            return; // no solution
        }

        if ( gtypeChooser.selectedGType == null ) {
            System.out.println("INFO: Unable to change origin gtype, the selected type is NULL");
            return;  // internal setSelected event
        }

        GType gtype = gtypeChooser.selectedGType;
        if (gtype.name.equalsIgnoreCase(sol.getOriginGTypeString())) {
           if (jbGType != null && !gtype.name.equals(jbGType.getText().toLowerCase())) {
               jbGType.setText(gtype.code.toUpperCase());
               jbGType.setToolTipText(gtype.name + " gtype");
           }
           System.out.println ("INFO: Event origin gtype not changed from : "+ gtype.toString());
            return; // no change = no action
        }

        if (sol.hasComment()) { // for other types might want to change the comment
          if ( sol.hasLatLonZ() || ! (sol.isClone() && sol.isOriginGType(GTypeMap.LOCAL)) ) jiggle.addComment();
        }

        sol.setOriginGType(gtype.name);

        String name = sol.getOriginGTypeString();
        System.out.println ("INFO: set event origin gtype: " + gtype.toString() + " [" +sol.getComment() +"]");
        jbGType.setText(GTypeMap.toDbType(name).toUpperCase());
        jbGType.setToolTipText(name + " gtype");

        if ( sol.isOriginGType(GTypeMap.TELESEISM)) typeChooser.setSelectedItemByDbType(EventTypeMap3.EARTHQUAKE);
        //if (sol.isOriginGType(GTypeMap.TELESEISM)) resetChooserTypes();

        if (jiggle.srlPhases != null) jiggle.srlPhases.resetSummaryTextArea(); // aww 01/07/2008

        // do this to force a change event to notify listners
        jiggle.mv.solList.setSelected(sol);
        jiggle.updateFrameTitle();
        jiggle.toFront();

    }

    class FixButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            fixDepthButton.toggle();
            jiggle.toFront();  // test 2008/01/10 -aww Kate's map popup to forground problem?
        }
    }

} // end of SolutionPanel

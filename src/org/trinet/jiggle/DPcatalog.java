package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.ActiveList;
import org.trinet.util.StringList;
import org.trinet.util.graphics.IconImage;
import org.trinet.util.graphics.table.CatalogTable;

/**
 * Panel for insertion into dialog box for selecting catalog column property.
 */
public class DPcatalog extends JPanel {

    private JiggleProperties newProps;    // new properties as changed by this dialog
    private CatalogColumnChooserPanel columnChooserPanel = null;

    protected boolean colorChanged = false;

    public DPcatalog(JiggleProperties props) {
      newProps = props;
      columnChooserPanel = new CatalogColumnChooserPanel();
      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
        columnChooserPanel.initGraphics();
        this.add(columnChooserPanel);
    }

    public void setCatalogColumnListProperty() {
      ArrayList aList = columnChooserPanel.getChoosen();
      StringBuffer sb = new StringBuffer(512);

      for (int idx = 0; idx < aList.size(); idx++) sb.append((String)aList.get(idx)).append(" ");

      String str = sb.toString();
      newProps.setProperty("catalogColumnList", str);

    }
    
    private static final String [] SORTED_CAT_COL_NAMES = {
          "AUTH","B","CM","COMMENT","DATETIME","DIST","DM","ERR_H","ERR_T","ERR_Z",
          "ESRC","ETYPE","EXT_ID","FM","GAP","GT","GZ","HF","LAT","LDDATE","LON",
          "MAG","MERR","METHOD","MMETH","MOBS","MOD","MSTA","MTYP","MWHO","Md",
          "Me","Ml","Ml-Md","Mlr","Mw","MZ", "OAUTH","OBS","OT","OWHO","PM",
          "PR","Q","RMS","S","SRC","ST","TF", "USED","V","VER",
          "VM","WRECS","Z","ZF"
    };

    private static final String [] CAT_COL_NAME_TIPS = {
        "Event authority","Bogus origin","Velocity model domain","Event comment","Origin time UTC)",
        "Closest sta (km)","Crustal model type","Horizontal error (km)","Time error (secs)","Depth error (km)",
        "Event subsource","Event type","External id","First motion count","Gap",
        "Geographic type","Geoid depth","Fixed location","Latitude","Origin lddate","Longitude",
        "Event magnitude (event.prefmag)","Event prefmag error","Origin algorithm","Event prefmag algorithm","Event prefmag channels",
        "Crustal model name","Event prefmag stations","Event prefmag type","Event prefmag who (attribution)","Md",
        "Me","Ml","Ml-Md","Mlr","Mw","Model depth (mdepth)",
        "Origin authority","Total phases","Origin Type","Origin who (attribution)", "Prefmec flag",
        "Event priority","Origin Quality","Origin RMS traveltime residual (secs)","S phase count", "Origin subsource",
        "Event processing state","Fixed Time","Phases used","Event valid (primary)","Event update version",
        "Velocity model id","Event waveform count","Origin depth","Fixed Depth"
    };

    class CatalogColumnChooserPanel extends JPanel {

        private final HashMap toolTipMap = new HashMap(53);

        private java.util.List allItems = null;
        private java.util.List rejectItems = null;;
        private java.util.List chooseItems = null;
        private javax.swing.JList chooseList = null; 

        public CatalogColumnChooserPanel() { 
            setBorder(BorderFactory.createTitledBorder("Catalog columns"));

            for (int idx = 0; idx<SORTED_CAT_COL_NAMES.length; idx++) { 
              toolTipMap.put(SORTED_CAT_COL_NAMES [idx],CAT_COL_NAME_TIPS [idx]);
            }

            setAll(new StringList(SORTED_CAT_COL_NAMES).getList());

            java.util.List list = newProps.getStringList("catalogColumnList").getList();
            if (list == null || list.isEmpty()) list = new StringList(SORTED_CAT_COL_NAMES).getList();
            setChoosen(list);
        }

        public void setAll(java.util.List all) {
            allItems = all;
        }

        public ArrayList getChoosen() {
          Object [] names = ((DefaultListModel)chooseList.getModel()).toArray();
          ArrayList aList = new ArrayList(names.length);
          for (int idx = 0; idx < names.length; idx++) {
            aList.add(names[idx]);
          }
          return aList;
        }

        public void setChoosen(java.util.List choose) {
            ArrayList aList = new ArrayList(allItems);
            aList.removeAll(choose);
            rejectItems = aList;
            aList = new ArrayList(choose);
            aList.removeAll(rejectItems);
            chooseItems = aList;
        }

        public void setRejected(java.util.List reject) {
            ArrayList aList = new ArrayList(allItems);
            aList.removeAll(reject);
            chooseItems = aList;
            aList = new ArrayList(allItems);
            aList.removeAll(chooseItems);
            rejectItems = aList;
        }

        public void initGraphics() {
            Box hbox = Box.createHorizontalBox();
            hbox.setBorder(BorderFactory.createTitledBorder("Catalog Table Column Arrangement"));
            DefaultListModel dlm  = new DefaultListModel();
            for (int idx = 0; idx < rejectItems.size(); idx++) dlm.addElement(rejectItems.get(idx));
            final JList rejectList = new JList(dlm);
            MyToolTipListCellRenderer ttr = new MyToolTipListCellRenderer();
            rejectList.setCellRenderer(ttr);
            rejectList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            rejectList.setPreferredSize(new Dimension(80,640));
            JScrollPane jsp = new JScrollPane(rejectList);
            //jsp.getViewport().setViewSize(new Dimension(60,250));
            jsp.setBorder(BorderFactory.createTitledBorder("Available to add"));
            hbox.add(jsp);

            Box vbox = Box.createVerticalBox();
            JButton jb = makeButton(">","Add selected available column(s) to catalog table","e.gif");
            final ActionListener al = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    String cmd = evt.getActionCommand();
                    if (cmd.equals(">")) {
                        Object [] selected = rejectList.getSelectedValues();
                        DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                        DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                        int selectIdx = chooseList.getSelectedIndex();
                        if (selectIdx < 0) selectIdx = cmodel.size()-1;
                        for (int idx = 0; idx < selected.length; idx++) {
                            cmodel.add(++selectIdx, selected[idx]);
                            rmodel.removeElement(selected[idx]);
                        } 
                    }
                    else if (cmd.equals("<")) {
                        Object [] selected = chooseList.getSelectedValues();
                        DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                        DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                        ArrayList rlist = new ArrayList(Arrays.asList(rmodel.toArray()));
                        rlist.addAll(Arrays.asList(selected));
                        Collections.sort(rlist);
                        rmodel = new DefaultListModel();
                        for (int idx = 0; idx < rlist.size(); idx++) {
                            rmodel.addElement(rlist.get(idx));
                        }
                        for (int idx = 0; idx < selected.length; idx++) {
                            cmodel.removeElement(selected[idx]);
                        }
                        rejectList.setModel(rmodel);
                    }
                }
            };
            jb.addActionListener(al);
            vbox.add(jb);
            jb = makeButton("<","Remove selected catalog table column(s)", "w.gif");
            jb.addActionListener(al);
            vbox.add(jb);
            hbox.add(vbox);
            
            dlm  = new DefaultListModel();
            for (int idx = 0; idx < chooseItems.size(); idx++) dlm.addElement(chooseItems.get(idx));
            chooseList = new JList(dlm);
            chooseList.setCellRenderer(ttr);
            chooseList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            chooseList.setPreferredSize(new Dimension(80,640));
            jsp = new JScrollPane(chooseList);
            //jsp.getViewport().setViewSize(new Dimension(60,250));
            jsp.setBorder(BorderFactory.createTitledBorder("Catalog table"));
            hbox.add(jsp);

            vbox = Box.createVerticalBox();
            jb = makeButton("-","Move selected catalog table column to left","n.gif");
            final ActionListener al2 = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    String cmd = evt.getActionCommand();
                    int selectIdx = chooseList.getSelectedIndex();
                    Object selected = (selectIdx >= 0) ?  chooseList.getModel().getElementAt(selectIdx) : null;
                    if (selected == null) return;

                    DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();

                    if (cmd.equals("+")) {
                      if (selectIdx+1 < cmodel.size()) {
                        cmodel.removeElementAt(selectIdx);
                        cmodel.insertElementAt(selected, selectIdx+1);
                        chooseList.setSelectedIndex(selectIdx+1);
                      }
                    }
                    else if (cmd.equals("-")) {
                      if (selectIdx-1 >= 0) {
                        cmodel.removeElementAt(selectIdx);
                        cmodel.insertElementAt(selected, selectIdx-1);
                        chooseList.setSelectedIndex(selectIdx-1);
                      }
                    }
                }
            };
            jb.addActionListener(al2);
            vbox.add(jb);
            jb = makeButton("+","Move selected catalog table column to right","s.gif");
            jb.addActionListener(al2);
            vbox.add(jb);
            hbox.add(vbox);
            this.add(hbox);
        }
     
        private JButton makeButton(String labelStr, String tooltip, String imageStr) {
            Image image = IconImage.getImage(imageStr);
            JButton btn = null;
            if (image == null) { // handle situation when .gif file can't be found
                btn = new JButton(labelStr);
            } else {
                btn = new JButton(new ImageIcon(image));
            }
            btn.setActionCommand(labelStr);
            btn.setPreferredSize(new Dimension(20,20));
            btn.setMaximumSize(new Dimension(20,20));
            btn.setToolTipText(tooltip);
            return btn;
        }

        class MyToolTipListCellRenderer extends JLabel implements ListCellRenderer {
           public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
             String s = value.toString();
             setText(s);
             if (isSelected) {
                 setBackground(list.getSelectionBackground());
                 setForeground(list.getSelectionForeground());
             }
             else {
                 setBackground(list.getBackground());
                 setForeground(list.getForeground());
             }
             setToolTipText((String)toolTipMap.get(value));
             setEnabled(list.isEnabled());
             setFont(list.getFont());
             setOpaque(true);
             return this;
         }
       }

    } // end of class CatalogColumnChooserPanel 

} // DPcatalog

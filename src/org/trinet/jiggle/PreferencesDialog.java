package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.trinet.util.GenericPropertyList;

/**
  * This dialog allows user to choose Jiggle preferences
  */
public class PreferencesDialog extends CenteredDialog {

    Jiggle jiggle = null; // holds current properties and allows access to other members needing mods
    JiggleProperties newProps = null; // new properties as changed by this dialog
  
    JPanel container = new JPanel( new BorderLayout());            // the main dialog panel
    JScrollPane scroller = null;

    // make the tabbed panels
    JTabbedPane tabs = new JTabbedPane();

    DPdatabase dbDP = null;
    DPLocationServer locEngDP = null;
    DPwaveSource wfSourceDP = null;
    DPChannelTimeWindowModel ctwmDP = null;
    DPvelocityModel velModelDP = null;
    DPchannel channelDP = null;

    DPMagMethod magMethodDP = null;
    DPMagEngine magEngineDP = null;

    DPpicker pickPropsDP = null;
    DPpickerParms pickParmsDP = null;

    DPguiLayout wfControlsDP = null;
    DPwfPanel wfPanelDP = null;
    DPtraceColors wfColorsDP = null;
    DPsolColors solColorsDP = null;

    DPpickLayout pickLayoutDP = null;
    DPwfFrameLayout wfFrameDP = null;
    DPmapFrameLayout mapFrameDP = null;
    DPeventType eventTypeDP = null;
    DPmagType magTypeDP = null;

    DPcatalog catalogDP = null;
    DPcatColors catColorsDP = null;

    DPlistingColors listColorsDP = null;

    DPdbgOutput dbgOutputDP = null;
    DPcheckQC qcCheckDP = null;
    DPdbSave dbSaveDP = null;
    DPMiscProps miscDP = null;

    public static final int TAB_CATCOLORS     = 0;
    public static final int TAB_CATCOLS       = 1;
    public static final int TAB_CHANNELLIST   = 2;
    public static final int TAB_LISTCOLORS    = 3;
    public static final int TAB_DBSAVE        = 4;
    public static final int TAB_DATACONNECTION= 5;
    public static final int TAB_ETYPE         = 6;
    public static final int TAB_MAGTYPE       = 7;
    public static final int TAB_LOCSERVER     = 8;
    public static final int TAB_MAGENGINE     = 9;
    public static final int TAB_MAGMETHOD     = 10;
    public static final int TAB_MAPFRAMELAYOUT= 11;
    public static final int TAB_MISC          = 12;
    public static final int TAB_OUTPUT        = 13;
    public static final int TAB_PICKLAYOUT    = 14;
    public static final int TAB_SOLCOLORS     = 15;
    public static final int TAB_PICKERPARMS   = 16;
    public static final int TAB_PICKERPROPS   = 17;
    public static final int TAB_QC            = 18;
    public static final int TAB_CHANWINMODEL  = 19;
    public static final int TAB_VELMODEL      = 20;
    public static final int TAB_WAVEDATASOURCE= 21;
    public static final int TAB_WAVECOLORS    = 22;
    public static final int TAB_WFFRAMELAYOUT = 23;
    public static final int TAB_WF_CONTROLS   = 24;
    public static final int TAB_WFPANEL       = 25;

    int topTab = 0;

    /** Constructor */
    public PreferencesDialog(Jiggle jiggle) {
        this(jiggle, 0);
    }
    /** Constructor */
    public PreferencesDialog(Jiggle jiggle, int tabNumber) {

        super(null, "Preferences", true);

        this.jiggle = jiggle; // this class will NOT affect the original copy
        //newProps = (JiggleProperties) jiggle.getProperties().deepClone(); // can't Serialize Oracle Connection
        newProps = (JiggleProperties) jiggle.getProperties().clone(); // shallow - aww 06/21/2005

        topTab = tabNumber;

        try  {
             initGraphics();
        }
        catch(Exception ex) {
             ex.printStackTrace();
        }
    }

     /** Build the GUI */
     private void initGraphics() throws Exception {

        // remove contents so we can call this again without repeating components
        container.removeAll();
        tabs.removeAll();

        dbDP = new DPdatabase(newProps);

        locEngDP = new DPLocationServer(newProps);

        wfSourceDP = new DPwaveSource(newProps, jiggle.mv);

        ctwmDP = new DPChannelTimeWindowModel(newProps);

        velModelDP = new DPvelocityModel(newProps);

        channelDP = new DPchannel(newProps); // added new panel here for testing - aww 2010/07/01

        // Share delegate clone with 2 mag panels for updates 
        JiggleSolutionSolverDelegate delegate =  new JiggleSolutionSolverDelegate(jiggle.solSolverDelegate, newProps);
        magMethodDP = new DPMagMethod(jiggle, newProps, delegate);
        magEngineDP = new DPMagEngine(jiggle, newProps, delegate);
        // Listen for magtype combo box selection change 
        magMethodDP.addMagTypeChangeListener(magEngineDP); 
        magEngineDP.addMagTypeChangeListener(magMethodDP); 

        pickPropsDP = new DPpicker(jiggle, newProps);

        pickParmsDP = new DPpickerParms(jiggle, newProps);
        pickPropsDP.addPickerChangeListener(pickParmsDP); 
        pickParmsDP.addPickerChangeListener(pickPropsDP); 

        wfControlsDP = new DPguiLayout(newProps); // added new panel here for testing - aww 2008/06/10

        wfPanelDP = new DPwfPanel(newProps); // added new panel here for testing - aww 2008/06/10

        wfColorsDP = new DPtraceColors(newProps);

        solColorsDP = new DPsolColors(newProps);

        pickLayoutDP = new DPpickLayout(newProps);

        wfFrameDP = new DPwfFrameLayout(newProps);

        mapFrameDP = new DPmapFrameLayout(newProps);

        eventTypeDP = new DPeventType(newProps);
        magTypeDP = new DPmagType(newProps);

        catalogDP = new DPcatalog(newProps); // added new panel here for testing - aww 2008/06/10

        catColorsDP = new DPcatColors(newProps); // added new panel here for testing - aww 2011/08/09

        listColorsDP = new DPlistingColors(newProps);

        dbgOutputDP = new DPdbgOutput(newProps);

        qcCheckDP = new DPcheckQC(newProps);

        dbSaveDP = new DPdbSave(newProps);

        miscDP = new DPMiscProps(newProps); // moved channel cache config to DPchannel -aww 2010/07/01

        tabs.addTab( "Catalog Colors", null, catColorsDP );
        tabs.addTab( "Catalog Columns", null, catalogDP );
        tabs.addTab( "Channel List", null, channelDP );
        tabs.addTab( "Data List Colors", null, listColorsDP );
        tabs.addTab( "DB Save", null, dbSaveDP );
        tabs.addTab( "Event DataSource", null, dbDP);
        tabs.addTab( "Event Type Menu", null, eventTypeDP );
        tabs.addTab( "Mag Type Menu", null, magTypeDP );
        tabs.addTab( "Location Server", null, locEngDP );
        tabs.addTab( "Mag Engine", null, magEngineDP );
        tabs.addTab( "Mag Method", null, magMethodDP );
        tabs.addTab( "Map Frame", null, mapFrameDP );
        tabs.addTab( "Misc", null, miscDP );
        tabs.addTab( "Output Options", null, dbgOutputDP );
        tabs.addTab( "Pick Display", null, pickLayoutDP );
        tabs.addTab( "Pick Flag Colors", null, solColorsDP );
        tabs.addTab( "Picker Parms", null, pickParmsDP );
        tabs.addTab( "Picker Props", null, pickPropsDP );
        tabs.addTab( "QC Checks", null, qcCheckDP );
        tabs.addTab( "TimeWindow Model", null, ctwmDP );
        tabs.addTab( "Velocity Model", null, velModelDP );
        tabs.addTab( "Wave DataSource", null, wfSourceDP );
        tabs.addTab( "Waveform Colors", null, wfColorsDP );
        tabs.addTab( "Waveform Frame", null, wfFrameDP );
        tabs.addTab( "Waveform Layout ", null, wfControlsDP );
        tabs.addTab( "Waveform Paint ", null, wfPanelDP );

        // define the buttons: [OK] [Reset] [Cancel]
        JPanel buttonPanel = new JPanel();

        JButton ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {
                               public void actionPerformed(ActionEvent e) {
                                   OKPressed();
                               }});
        ok.setToolTipText("Use changes");
        buttonPanel.add( ok );

        JButton reset = new JButton("RESET");
        reset.addActionListener(new ActionListener() {
                               public void actionPerformed(ActionEvent e) {
                                   resetPressed();
                               }});
        reset.setToolTipText("Reset properties in ALL tabs to original values.");
        buttonPanel.add( reset );

        JButton cancel = new JButton("CANCEL");
        cancel.addActionListener(new ActionListener() {
                               public void actionPerformed(ActionEvent e) {
                                   cancelPressed();
                               }});
        cancel.setToolTipText("Cancel changes");
        buttonPanel.add( cancel );

        getRootPane().setDefaultButton(ok); // default button
        //JScrollPane scroller = new JScrollPane(tabs);
        //scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        //container.add(scroller, BorderLayout.CENTER);
        container.add(tabs, BorderLayout.CENTER);
        container.add(buttonPanel, BorderLayout.SOUTH);
        getContentPane().add(container);

        // To stop user's pressing of return/enter while editing panels from activating the dialog's "ok" button, closing dialog -aww
        JRootPane rootPane = getRootPane();
        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put( KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "doNothing" );
        rootPane.getActionMap().put("doNothing", new AbstractAction("doNothing") { public void actionPerformed(ActionEvent evt) {} });

        pack();
        centerDialog();

        /*
        tabs.getModel().addChangeListener(
                new ChangeListener() {
                    public void stateChanged(ChangeEvent e) {
                        Rectangle r = scroller.getViewport().getViewRect();
                        r.y = scroller.getViewport().getView().getHeight() - r.height;
                        if (r.y <= 0) return; 
                        Point p = new Point(r.x,r.y);
                        scroller.getViewport().setViewPosition(p);
                        scroller.getViewport().repaint();
                    }
                }
        );
        */

        // bring the chosen tab to the front
        tabs.setSelectedIndex(topTab);

        //show(); // call moved to Jiggle 2008/10/30 -aww
     }

     /** Return the JiggleProperties */
     public JiggleProperties getJiggleProperties() {
            // reset certain values after any changes
            // but don't read auxFile properties, they overwrite changes!
            newProps.setup(false); // Jiggle must still configure engines, TravelTime, DataSource connection, Waveform.WaveDataSource -aww
            return newProps;
     }

// //////////////////////////////////////////////////////////////////
/**
 *  Action to take when [Cancel] is pressed
 *  Close dialog, make no changes
 */
    public void cancelPressed() {
        returnValue = JOptionPane.CANCEL_OPTION;
        dispose();
    }

/**
 *  Action to take when [OK] is pressed
 *  Close dialog, make changes
 */
    public void OKPressed() {
        returnValue = JOptionPane.OK_OPTION;
        catalogDP.setCatalogColumnListProperty();
        wfColorsDP.resetComponentColorMap();
        solColorsDP.resetColorList();
        magTypeDP.updateMagTypes();
        listColorsDP.resetReadingListColors();
        dispose();
    }
/**
 *  Action to take when [Reset] is pressed
 *  Reset all preferences to original values that are in the files.
 */
    public void resetPressed() {

        // return to the same tab after reset
        int index = tabs.getSelectedIndex();

        //newProps = (JiggleProperties) jiggle.getProperties().deepClone(); // can't Serialize Oracle Connection
        newProps = (JiggleProperties) jiggle.getProperties().clone(); // shallow - aww 06/21/2005

        try  {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

       tabs.setSelectedIndex(index);

    }

    /** Returns true if gui layout panel has a change. */
    public boolean updateGUI() { // added new panel here for testing - aww 2008/06/10
        return wfControlsDP.updateGUI ||
               wfPanelDP.updateGUI ||
               wfFrameDP.updateGUI ||
               mapFrameDP.updateGUI ||
               pickLayoutDP.updateGUI ||
               qcCheckDP.updateGUI ||
               dbgOutputDP.updateGUI ||
               miscDP.updateGUI;

    }

    /** Returns true if the reset of entire GUI is needed. */
    public boolean resetGUI() {
      return wfControlsDP.resetGUI ||
             wfPanelDP.resetGUI ||
             wfFrameDP.resetGUI ||
             mapFrameDP.resetGUI ||
             pickLayoutDP.resetGUI ||
             qcCheckDP.resetGUI ||
             dbgOutputDP.resetGUI ||
             miscDP.resetGUI ||
             dataSourceChanged();
    }

    /** Returns true if any of the catalog color properties were changed. */
    public boolean catalogColorChanged() {
        return catColorsDP.colorChanged;
    }

    /** Returns true if any of the waveform color properties were changed. */
    public boolean wfColorChanged() {
        return wfColorsDP.colorChanged;
    }

    /** Returns true if any sight line color property was changed. */
    public boolean sightColorChanged() {
        return wfColorsDP.sightColorChanged;
    }

    public boolean deltimColorChanged() {
        return wfColorsDP.deltimColorChanged;
    }

    public boolean unusedPickColorChanged() {
        return wfColorsDP.unusedPickColorChanged;
    }

    /** Returns true if any of the tab text color properties were changed. */
    public boolean tabTextColorChanged() {
        return listColorsDP.tabColorChanged;
    }
    /** Returns true if any of the reading list color properties were changed. */
    public boolean readingListColorChanged() {
        return listColorsDP.listColorChanged;
    }

    /** Returns true if any of the event type properties were changed. */
    public boolean eventTypeMenuChanged() {
        return eventTypeDP.typeChanged;
    }

    /** Returns true if any of the mag type properties were changed. */
    public boolean magTypeMenuChanged() {
        return magTypeDP.typeChanged;
    }

    /** Returns true if any of the sol color properties were changed. */
    public boolean solColorChanged() {
        return solColorsDP.colorChanged;
    }

    /** Returns true if map properties were changed or map should be reset. */
    public boolean mapPropsChanged() {
        return mapFrameDP.mapPropsChanged;
        //return !jiggle.getProperties().getProperty("mapPropFilename", "").equals(newProps.getProperty("mapPropFilename", ""));
    }

    /** Phase picker channel parms or properties changed */
    public boolean pickerChanged() {
        return pickPropsDP.hasChangedPicker();
    }
    public boolean pickerPropsChanged() {
        return pickPropsDP.hasChangedProps();
    }
    public boolean pickerParmsChanged() {
        return pickParmsDP.hasChangedParms();
    }
    public java.util.List getPickerChannelParms() {
        return pickParmsDP.getPickerChannelParms();
    }

    protected GenericPropertyList getPickerPropertyList() {
        return pickPropsDP.getSelectedPickerProps();
    }

    /** Returns true if channel cache changed. */
    public boolean channelCacheFilenameChanged() {
        //return miscDP.channelCacheChanged;
        return !jiggle.getProperties().getProperty("channelCacheFilename", "").equals(newProps.getProperty("channelCacheFilename", ""));
    }

    /** Returns true if catalog column list changed. */
    public boolean catalogColsChanged() { // added new panel here for testing - aww 2008/06/10
        return !jiggle.getProperties().getProperty("catalogColumnList", "").equals(newProps.getProperty("catalogColumnList", ""));
    }

    /** Returns true if any of the DataSource properties were changed. */
    public boolean dataSourceChanged() {
        return !jiggle.getProperties().dataSourceEquals(newProps);
    }
    /** Returns true if any of the default velocity model changed. */
    public boolean velocityModelChanged() { // added -aww 2008/03/25
        return !jiggle.getProperties().velocityModelEquals(newProps);
    }
    /** Returns true if any of the wavesource properties were changed. */
    public boolean waveSourceChanged() {
        return !jiggle.getProperties().waveSourceEquals(newProps);
    }

    /** Returns true if any of the wavesource properties were changed. */
    public boolean waverootsCopyChanged() {
        return !jiggle.getProperties().getProperty("waverootsCopy", "").equals(newProps.getProperty("waverootsCopy", ""));
    }

    /** Returns true if the ChannelTimeWindowModel changed. */
    /** Returns true if the ChannelTimeWindowModel changed. */
    public boolean channelTimeWindowModelChanged() {
        return !jiggle.getProperties().channelTimeWindowModelEquals(newProps);
    }
    /** Returns true if the locationEngine changed. */
    public boolean locationEngineChanged() {
      return !jiggle.getProperties().locationEngineEquals(newProps);
    }
    /** Returns true if the map layout in splitpane vs. standalone window changed. */
    public boolean mapInSplitChanged() {
      return !jiggle.getProperties().mapInSplitEquals(newProps);
    }

    /** Returns true if the map split frame layout changed. */
    public boolean swarmInWaveformTabChanged() {
      return ! (jiggle.getProperties().getBoolean("swarmInWaveformTabPane") == newProps.getBoolean("swarmInWaveformTabPane"));
    }

    protected JiggleSolutionSolverDelegate getSolutionSolverDelegate() {
        return magMethodDP.delegate;
    }

    protected boolean magConfigChanged() {
        return magMethodDP.hasChanged() || magEngineDP.hasChanged();
    }

    /** Pop dialog asking if current preferences should be saved. */
    public void savePropertiesDialog() {
           int answer = JOptionPane.showConfirmDialog(
                   null, "Save current dialog properties to startup file?",
                   "Save Properties?",
                   JOptionPane.YES_NO_OPTION);

            if (answer  == JOptionPane.YES_OPTION) newProps.saveProperties();
    }

} // end of PrefsDialog class


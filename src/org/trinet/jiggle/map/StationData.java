package org.trinet.jiggle.map;

import java.util.HashSet;

import org.trinet.jasi.Channelable;
import org.trinet.jasi.ChannelableList;
import org.trinet.jasi.ChannelIdentifiable;
import org.trinet.jasi.ChannelName;

import org.trinet.util.DateRange;
import org.trinet.util.gazetteer.LatLonZ;

public class StationData {

    private String  net = null; // site net
    private String  sta = null; // site name
    private LatLonZ llz = null; // site location
    private DateRange dr = null;   // earliest, latest Channel operational dates

    private HashSet chanSet = new HashSet(11);

    // add methods to return seedchan sorted array/list from HashSet
    // or description string about min,max on/off date and seedchan

        public ChannelIdentifiable getChannelIdentifiable() {
            return new ChannelName(net, sta, null, null);
        }

        public String getNet() {
            return net;
        }

        public String getSta() {
            return sta;
        }

        public float getLat() {
            return (float) llz.getLat();
        }

        public float getLon() {
            return (float) llz.getLon();
        }

        public DateRange getDateRange() {
            return dr;
        }

        public Object getMapKey() {
            return net+sta;  // NN.SSS vs NNSSS ambiguous only if network 1-char
        }

        public static Object getMapKey(Channelable c) {
            return c.getChannelObj().getNet()+c.getChannelObj().getSta();
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (! (o instanceof StationData)) return false;
            StationData sid = (StationData) o;
            return 
                net.equals(sid.net) && sta.equals(sid.sta); // && llz.equals(sid.llz);
        }

        public int hashCode() {
            return (net+sta).hashCode();
        }

        public boolean add(Channelable c) {
            if (net == null && sta == null) {
                // Kludge assume all channels have the same lat,lon which for
                // most map scales plotted should not make difference to user.
                net = c.getChannelObj().getNet();
                sta = c.getChannelObj().getSta();
                dr  = (DateRange) c.getChannelObj().getDateRange().clone();
            }
            if (llz == null || llz.isNull()) llz = c.getChannelObj().getLatLonZ();

            if (sameStationAs(c)) {
                // Re Station_Data vs. Channel_Data differences for ondate/offdate 
                // at a given station its components may have different on/off dates. 
                dr.include(c.getChannelObj().getDateRange());
                if (! chanSet.add(c) ) {
                  // should only be adds since MasterList grows and nothing should be removing elements.
                  chanSet.remove(c); // We DO NOT change expanded date range
                  chanSet.add(c);
                }
                return true;
            }
            return false;
        }

        // For completeness, should only be adds nothing should be removing elements from MasterList 
        public boolean remove(Channelable c) {
            return chanSet.remove(c);
        }


        public boolean contains(Channelable c) {
            return chanSet.contains(c);
        }

        public boolean sameStationAs(Channelable c) {
            return ( net.equals(c.getChannelObj().getNet()) &&
                      sta.equals(c.getChannelObj().getSta())
                   );
        }

        public ChannelableList getChannelables() {
            ChannelableList ch = new ChannelableList(chanSet);
            ch.sortByStation();
            return ch;
        }

        public String toString() {
            return net + "." + sta;
        }

        public String toInfoString() {
            return toString() + " on/off: " + dr.toString() + " channels: " + chanSet.size();
        }

        public String toHtmlString() {
          ChannelableList ch = getChannelables();
          int count = ch.size();
          StringBuffer sb = new StringBuffer((count+1)*256);
          if (count == 0) {
            sb.append("No channelables found in list.");
          } else {
            for (int i = 0; i < count; i++) {
              sb.append(ch.get(i).toString()).append("<br>");
            }
          }
          return sb.toString();
        }

}

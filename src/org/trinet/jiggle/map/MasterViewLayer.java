package org.trinet.jiggle.map;

import java.awt.event.MouseEvent;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JOptionPane;

import com.bbn.openmap.LatLonPoint;
import com.bbn.openmap.MapHandler;
import com.bbn.openmap.event.MapMouseEvent;

import org.trinet.jasi.Solution;
import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleMapBean;
import org.trinet.jiggle.MasterView;

/**
* MasterViewLayer displays data from  a Jiggle MasterView SolutionList.
* Listens for a property change from JiggleMapBean for new MasterView SolutionList.
 */
public class MasterViewLayer extends SolutionListLayer {

    /**
     * Construct an MasterViewLayer for current Solution reading highlights.
     */
    public MasterViewLayer() {
        super();
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JiggleMapBean) {
          String propName = evt.getPropertyName();
          if (propName.equals(Jiggle.NEW_MV_PROPNAME)) {
            setSolutionList( ((MasterView)evt.getNewValue()).solList );
          }
        }
        super.propertyChange(evt);
    }

    public void findAndInit(Object obj) {
        if (obj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) obj;
            mapBean.addPropertyChangeListener(this);
            setSolutionList(mapBean.getMvSolutionList());
            this.jiggle = mapBean.jiggle;
            mapBean.masterViewLayer = this;
        }
        super.findAndInit(obj);
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    public boolean mousePressed(MouseEvent e) {
        //if (e.isPopupTrigger())                
        //    System.out.println(getClass().getName() + " DEBUG MasterViewLayer MOUSE PRESSED EVENT popup trigger");
        if (
             (e.getModifiers() == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK)) &&
             (e instanceof MapMouseEvent)
           )
        {
        //System.out.println("DEBUG MasterViewLayer  MapMouseEvent  left-button-shift mask to set trial location");
            MapHandler mh = (MapHandler) getBeanContext();
            if (mh != null) {
               JiggleMapBean mapBean = (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
               if (mapBean != null) {
                 Solution sol = ((Solution) mapBean.getMvSolutionList().getSelected());
                 LatLonPoint p = ((MapMouseEvent) e).getLatLon();
                 if (sol != null) {
                   int yn = JOptionPane.showConfirmDialog( this, 
                                "Change Event lat,lon to the location of selected map point? "+
                                sol.getId() + "?", "Map Trial Location Option",
                                JOptionPane.YES_NO_OPTION);
                   if (yn == JOptionPane.YES_OPTION) {
                       String str = JOptionPane.showInputDialog( this, "Event Trial depth = ", "5.");
                       double z = 5.;
                       try {
                           z = Double.parseDouble(str);
                       }
                       catch (NumberFormatException ex) {
                       }
                       double dt = sol.getTime(); // save existing time -aww 2012/05/09
                       if (Double.isNaN(dt) || (dt == 0.)) { // if not set, set to MasterView starting time
                           dt = jiggle.getMasterView().getViewSpan().getStart();
                       }
                       sol.clearLocationAttributes(); // reset, since changing lat,lon,z above makes them bogus -aww 2010/09/17
                       // restore states for some attributes:
                       sol.dummyFlag.setValue(0l); // flip to dummy state bogus origin -aww 2012/05/09
                       sol.algorithm.setValue("HAND"); // -aww 2010/09/17
                       sol.source.setValue("Jiggle"); // -aww 2010/09/17
                       sol.setTime(dt); // reset back to time -aww 2012/05/09
                       sol.setLatLonZ((double) p.getLatitude(), (double) p.getLongitude(), z);
                       resetGraphicLocationFor(sol);
                   }
                 }
                 //jiggle.updateTextViews(); // ?? force repaint of title ??
                 jiggle.updateFrameTitle();
                 //jiggle.updateWhereTab(); // removed 12/12/2006 -aww deprecated for a button replacement
               }
            }
            return true;
        }
        else return super.mousePressed(e);
    }

}

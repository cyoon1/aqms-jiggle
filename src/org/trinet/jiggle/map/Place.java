package org.trinet.jiggle.map;
import java.awt.Paint;
/*  OpenMap  */
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.layer.location.*;

/**
 * A Place the standard Location - dot for the marker, text to
 * the right of the dot. Other graphics can be substituted for the
 * dot, but you should modify the setGraphicLocations methods
 * accordingly.
 */
public class Place extends com.bbn.openmap.layer.location.BasicLocation {

    public Place() {}

    /**
     * Create a location at a latitude/longitude. If the
     * locationMarker is null, a small rectangle (dot) will be created
     * to mark the location.
     * 
     * @param latitude the latitude, in decimal degrees, of the
     *        location.
     * @param longitude the longitude, in decimal degrees, of the
     *        location.
     * @param name the name of the location, also used in the label.
     * @param locationMarker the OMGraphic to use for the location
     *        mark.
     */
    public Place(float latitude, float longitude, String name,
            OMGraphic locationMarker) {
        super(latitude, longitude, name, locationMarker);
    }

    /**
     * Create a location at a map location. If the locationMarker is
     * null, a small rectangle (dot) will be created to mark the
     * location.
     * 
     * @param x the pixel location of the object from the let of the
     *        map.
     * @param y the pixel location of the object from the top of the
     *        map
     * @param name the name of the location, also used in the label.
     * @param locationMarker the OMGraphic to use for the location
     *        mark.
     */
    public Place(int x, int y, String name, OMGraphic locationMarker) {
        super(x, y, name, locationMarker);
    }

    /**
     * Create a location at a pixel offset from a latitude/longitude.
     * If the locationMarker is null, a small rectangle (dot) will be
     * created to mark the location.
     * 
     * @param latitude the latitude, in decimal degrees, of the
     *        location.
     * @param longitude the longitude, in decimal degrees, of the
     *        location.
     * @param xOffset the pixel location of the object from the
     *        longitude.
     * @param yOffset the pixel location of the object from the
     *        latitude.
     * @param name the name of the location, also used in the label.
     * @param locationMarker the OMGraphic to use for the location
     *        mark.
     */
    public Place(float latitude, float longitude, int xOffset,
            int yOffset, String name, OMGraphic locationMarker) {
        super(latitude, longitude, xOffset, yOffset, name, locationMarker);
    }

    /**
     * Set the select java.awt.Paint for the marker graphic.
     */
    public void setSelectPaint(Paint paint) {
        if (location != null) {
            location.setSelectPaint(paint);
        }
    }
    public void select() {
        super.select();
        if (location != null) location.select();
    }
    public void deselect() {
        super.deselect();
        if (location != null) location.deselect();
    }
    public void setSelected(boolean tf) {
        super.setSelected(tf);
        if (location != null) location.setSelected(tf);
    }
    public boolean isSelected() {
        boolean tf = super.isSelected();
        if (location != null) tf = ( tf || location.isSelected());
        return tf;
    }

    /**
     * Set the fill java.awt.Paint for the marker graphic.
     */
    public void setFillPaint(Paint paint) {
        if (location != null) {
            location.setFillPaint(paint);
        }
    }

    /**
     * Set the fill java.awt.Paint for the marker graphic.
     */
    public void setLinePaint(Paint paint) {
        if (location != null) {
            location.setLinePaint(paint);
            label.setLinePaint(paint);
        }
    }
}

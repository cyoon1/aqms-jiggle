package org.trinet.jiggle.map;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import com.bbn.openmap.Environment;
import com.bbn.openmap.I18n;
import com.bbn.openmap.gui.OMToolComponent;
import com.bbn.openmap.gui.WindowSupport;
import com.bbn.openmap.InformationDelegator;
import com.bbn.openmap.layer.DrawingToolLayer;
import com.bbn.openmap.MapHandler;
import com.bbn.openmap.omGraphics.GraphicAttributes;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.tools.drawing.DrawingTool;
import com.bbn.openmap.tools.drawing.OMDrawingTool;
import com.bbn.openmap.tools.drawing.EditToolLoader;
import com.bbn.openmap.tools.drawing.OMDrawingToolLauncher; // for gif resource?
import com.bbn.openmap.util.Debug;
import com.bbn.openmap.util.PaletteHelper;
import com.bbn.openmap.util.PropUtils;

import org.trinet.jasi.EventSelectionProperties;

public class CatalogFilterLauncher extends OMToolComponent implements
        ActionListener, PropertyChangeListener {

    protected I18n i18n = Environment.getI18n();
    protected DrawingTool drawingTool = null;
    protected boolean useTextEditToolTitles = false;
    protected GraphicAttributes defaultGraphicAttributes = new GraphicAttributes();
    public int maxHorNumLoaderButtons = -1;

    // Places buttons in alphabetical order
    //     protected TreeMap loaders = new TreeMap();
    protected Vector loaders = new Vector();

    protected CatalogFilterLayer catalogFilterLayer = null;
    private JTextField jtextName = new JTextField(); // added -aww 2008/04/24 
    protected String currentCreation = null;

    /**
     * Property for setting the maximum number of loader buttons to
     * allow in the horizontal direction in the GUI
     * (horizNumLoaderButtons). -1 means to just lay them out in one
     * row.
     */
    public final static String HorizontalNumberOfLoaderButtonsProperty = "horizNumLoaderButtons";
    public final static String UseLoaderTextProperty = "useTextLabels";

    String[] rtc = {
            i18n.get(CatalogFilterLauncher.class,
                    "renderingType.LatLon",
                    "Lat/Lon"),
            i18n.get(CatalogFilterLauncher.class, "renderingType.XY", "X/Y"),
            i18n.get(CatalogFilterLauncher.class,
                    "renderingType.XYOffset",
                    "X/Y Offset") };

    public final static String CreateCmd = "CREATE";
    public final static String SetCmd = "SET";
    public final static String FilterCmd = "FILTER";

    /** Default key for the DrawingToolLauncher Tool. */
    public static final String defaultKey = "CatalogFilterLauncher";

    protected ButtonGroup bg = null;

    public CatalogFilterLauncher() {
        super();

        setWindowSupport(new WindowSupport(this, i18n.get(CatalogFilterLauncher.class,
                "CatalogFilterLauncher",
                "Catalog Filter Launcher")));
        setKey(defaultKey);
        defaultGraphicAttributes.setRenderType(OMGraphic.RENDERTYPE_LATLON);
        defaultGraphicAttributes.setLineType(OMGraphic.LINETYPE_GREATCIRCLE);
        //defaultGraphicAttributes.setLineType(OMGraphic.LINETYPE_STRAIGHT);
        resetGUI();
    }

    /**
     * Set the DrawingTool for this launcher.
     */
    public void setDrawingTool(DrawingTool dt) {
        if (drawingTool != null && drawingTool instanceof OMDrawingTool) {
            ((OMDrawingTool) drawingTool).removePropertyChangeListener(this);
        }

        drawingTool = dt;

        if (drawingTool != null && drawingTool instanceof OMDrawingTool) {
            ((OMDrawingTool) drawingTool).addPropertyChangeListener(this);
        }
    }

    public DrawingTool getDrawingTool() {
        return drawingTool;
    }

    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand().intern();

        Debug.message("drawingtool", "DrawingToolLauncher.actionPerformed(): " + command);

        DrawingTool dt = getDrawingTool();

        if (dt instanceof OMDrawingTool) {
            OMDrawingTool omdt = (OMDrawingTool) dt;
            if (omdt.isActivated()) omdt.deactivate();
        }

        if (command == SetCmd && catalogFilterLayer != null) {
            catalogFilterLayer.setPropertiesFromGraphics();
            return;
        }        
        else if (command == FilterCmd && catalogFilterLayer != null) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    catalogFilterLayer.resetCatalog();
                }
            });
            return;
        }        
        else if (command == CreateCmd && dt != null && currentCreation != null ) {
            GraphicAttributes ga = (GraphicAttributes) defaultGraphicAttributes.clone();
            dt.setBehaviorMask(OMDrawingTool.DEFAULT_BEHAVIOR_MASK);
            OMGraphic omg = dt.create(currentCreation, ga, catalogFilterLayer);

            if (omg != null) { // added this block 2008/04/24 -aww
                String name = jtextName.getText();
                if (name == null || name.length() == 0) {
                    name = String.valueOf(catalogFilterLayer.getList().size());
                }
                omg.setAppObject(name);
            }

            return;
        }

        StringBuffer sb = new StringBuffer();
        if (dt == null) {
                sb.append("   No drawing tool is available!\n");
        } else {
                sb.append("   Drawing tool OK.\n");
        }

        if (currentCreation == null) {
                sb.append("   No valid choice of graphic to create.\n");
        } else {
                sb.append("   Graphic choice OK.\n");
        }

        if (catalogFilterLayer == null) {
                sb.append("   No valid CatalogFilterLayer for the created graphic.\n");
        } else {
                sb.append("   Graphic receiver OK.\n");
        }

        Debug.output("CatalogFilterLauncher: Something is not set:\n" + sb.toString());

        MapHandler mapHandler = (MapHandler) getBeanContext();
        if (mapHandler != null) {
              InformationDelegator id =
                  (InformationDelegator) mapHandler.get("com.bbn.openmap.InformationDelegator");
              if (id != null) id.displayMessage("Error", "CatalogFilter problem: "+sb.toString());
        }
    }

    private boolean isPolyListEventFilter() {
        if (catalogFilterLayer != null && catalogFilterLayer.jiggle != null) {
            EventSelectionProperties esp = catalogFilterLayer.jiggle.getEventProperties();
            if (esp != null && EventSelectionProperties.REGION_POLYLIST.equals(esp.getRegionType()) ) return true;
        }
        return false;
    }

    /**
     * Build the stuff that goes in the launcher.
     */
    public void resetGUI() {
        removeAll();

        JPanel palette = new JPanel();

        palette.setLayout(new BoxLayout(palette, BoxLayout.Y_AXIS));
        palette.setAlignmentX(Component.CENTER_ALIGNMENT); // LEFT
        palette.setAlignmentY(Component.CENTER_ALIGNMENT); // BOTTOM

        JPanel panel = PaletteHelper.createPaletteJPanel(i18n.get(CatalogFilterLauncher.class,
                "panelSendTo",
                "Send To:"));
        panel.add(new JLabel("CatalogFilterLayer"));
        palette.add(panel);

        if (Debug.debugging("omdtl")) {
            Debug.output("Figuring out tools, using names");
        }

        panel = PaletteHelper.createPaletteJPanel(i18n.get(CatalogFilterLauncher.class,
                "panelGraphicType",
                "Graphic Type:"));
        panel.add(getToolWidgets(useTextEditToolTitles));
        palette.add(panel);

        panel = PaletteHelper.createVerticalPanel(i18n.get(CatalogFilterLauncher.class,
                "panelGraphicAttributes",
                "Graphic Attributes:"));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel panel3 = PaletteHelper.createVerticalPanel(i18n.get(CatalogFilterLauncher.class,
                "panelLineColorTypes",
                "Line Types and Colors:"));
        panel3.add(defaultGraphicAttributes.getGUI());
        panel.add(panel3);
        palette.add(panel);

        JButton createButton = new JButton(i18n.get(CatalogFilterLauncher.class,
                "createButton",
                "Create Graphic"));
        createButton.setActionCommand(CreateCmd);
        createButton.setToolTipText("Create a region of selected graphic type");
        createButton.addActionListener(this);

        JButton setButton = new JButton(i18n.get(CatalogFilterLauncher.class,
                "setButton",
                "Set"));
        setButton.setActionCommand(SetCmd);
        setButton.setToolTipText("Set event selection property to selected region.");
        setButton.addActionListener(this);

        JButton filterButton = new JButton(i18n.get(CatalogFilterLauncher.class,
                "filterButton",
                "Filter"));
        filterButton.setActionCommand(FilterCmd);
        filterButton.setToolTipText("Set selection property and regenerate catalog.");
        filterButton.addActionListener(this);

        JButton dismissButton = new JButton(i18n.get(CatalogFilterLauncher.class,
                "dismissButton",
                "Close"));
        dismissButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getWindowSupport().killWindow();
            }
        });

        JPanel buttonBox = new JPanel();
        buttonBox.setLayout(new BoxLayout(buttonBox, BoxLayout.X_AXIS));
        buttonBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonBox.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        buttonBox.add(createButton);
        buttonBox.add(setButton);
        buttonBox.add(filterButton);
        buttonBox.add(dismissButton);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setAlignmentX(Component.CENTER_ALIGNMENT);
        setAlignmentY(Component.BOTTOM_ALIGNMENT);

        // below box added -aww 2008/04/24 
        Box nameBox = Box.createHorizontalBox();
        JLabel jtextLabel = new JLabel("Region name");
        jtextLabel.setMaximumSize(new Dimension(10,50));
        nameBox.add(jtextLabel);
        if (jtextName == null) {
          jtextName = new JTextField();
          jtextName.setMaximumSize(new Dimension(20,50));
        }
        else jtextName.setText("");
        nameBox.add(jtextName);
        nameBox.add(Box.createHorizontalGlue());

        add(palette);
        add(nameBox); // added -aww 2008/04/24 
        add(buttonBox);

    }
    
    protected void resetRegionGraphicButtons() {

        Enumeration en = bg.getElements(); 
        JToggleButton jb = null;
        boolean activeSet = false;
        boolean polyListFilter = isPolyListEventFilter();
        String pName = null;
        while (en.hasMoreElements()) {
            jb = (JToggleButton) en.nextElement();
            pName = jb.getActionCommand();
            if ( polyListFilter ) {
                jb.setEnabled( (pName.indexOf("Poly") >= 0) );
            }
            // Just set one as active, the first one.
            if (!activeSet && jb.isEnabled()) {
                setCurrentCreation(pName);
                activeSet = true;
                jb.setSelected(true); // added 2011/08/03 -aww
            }
            else jb.setSelected(false); // added 2011/08/03 -aww
        }
    }

    protected JComponent getToolWidgets() {
        JPanel iconBar = new JPanel();
        // this parameters should be read from properties!
        iconBar.setLayout(new GridLayout(2, 4));
        bg = new ButtonGroup();
        JToggleButton btn = null;
        boolean setFirstButtonSelected = true;
        boolean polyListFilter = isPolyListEventFilter();

        for (Iterator it = getLoaders(); it.hasNext();) {
            LoaderHolder lh = (LoaderHolder) it.next();
            String pName = lh.prettyName;
            EditToolLoader etl = lh.loader;
            ImageIcon icon = etl.getIcon(getEditableClassName(pName));
            btn = new JToggleButton(icon);
            btn.setMargin(new Insets(0, 0, 0, 0));
            btn.setToolTipText(pName);
            btn.setActionCommand(pName);

            if ( polyListFilter ) {
                btn.setEnabled( (pName.indexOf("Poly") >= 0) );
                //System.out.println(pName + " enabled: " + btn.isEnabled()); 
            }

            btn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    setCurrentCreation(ae.getActionCommand());
                }
            });

            if (setFirstButtonSelected && btn.isEnabled()) {
                btn.setSelected(true);
                setCurrentCreation(pName);
                setFirstButtonSelected = false;
            }
            else btn.setSelected(false); // added 2011/08/03 -aww
            //System.out.println(pName + " selected: " + btn.isSelected()); 

            bg.add(btn);
            iconBar.add(btn);
        }
        return iconBar;
    }

    protected JComponent getToolWidgets(boolean useText) {

        if (useText) {
            // Set editables with all the pretty names.
            Vector editables = new Vector();

            for (Iterator it = getLoaders(); it.hasNext();) {
                LoaderHolder lh = (LoaderHolder) it.next();
                editables.add(lh.prettyName);
            }

            return createToolOptionMenu(editables);
        } else {
            return createToolButtonPanel();
        }
    }

    private JComboBox createToolOptionMenu(Vector editables) {
        String[] toolNames = new String[editables.size()];
        for (int i = 0; i < toolNames.length; i++) {
            toolNames[i] = (String) editables.elementAt(i);
            if (Debug.debugging("omdtl")) {
                Debug.output("Adding TOOL " + toolNames[i] + " to menu");
            }
        }

        JComboBox tools = new JComboBox(toolNames);
        tools.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox jcb = (JComboBox) e.getSource();
                String currentChoice = (String) jcb.getSelectedItem();
                setCurrentCreation(currentChoice);
            }
        });

        if (toolNames.length > 0) {
            tools.setSelectedIndex(0);
        }

        return tools;
    }

    private JPanel createToolButtonPanel() {
        // Otherwise, create a set of buttons.
        JPanel panel = new JPanel();
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        panel.setLayout(gridbag);

        bg = new ButtonGroup();

        int toolbarCount = 0;
        boolean limitWidth = false;
        if (maxHorNumLoaderButtons >= 0) {
            limitWidth = true;
        }

        JToggleButton btn;
        JToolBar iconBar = null;
        boolean activeSet = false;
        boolean polyListFilter = isPolyListEventFilter();

        for (Iterator it = getLoaders(); it.hasNext();) {

            if (toolbarCount == 0) {
                iconBar = new JToolBar();
                iconBar.setFloatable(false);
                gridbag.setConstraints(iconBar, c);
                panel.add(iconBar);
            }

            LoaderHolder lh = (LoaderHolder) it.next();
            String pName = lh.prettyName;
            EditToolLoader etl = lh.loader;
            ImageIcon icon = etl.getIcon(getEditableClassName(pName));

            btn = new JToggleButton(icon, !activeSet);
            btn.setToolTipText(pName);
            btn.setActionCommand(pName);


            if ( polyListFilter ) {
                btn.setEnabled( (pName.indexOf("Poly") >= 0) );
            }

            btn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    setCurrentCreation(ae.getActionCommand());
                }
            });
            bg.add(btn);

            if (iconBar != null) {
                iconBar.add(btn);
            }

            toolbarCount++;

            // Just set one as active, the first one.
            if (!activeSet && btn.isEnabled()) {
                setCurrentCreation(pName);
                activeSet = true;
                btn.setSelected(true); // added 2011/08/03 -aww
            }
            else btn.setSelected(false); // added 2011/08/03 -aww

            if (limitWidth && toolbarCount >= maxHorNumLoaderButtons) {
                toolbarCount = 0;
            }
        }

        return panel;
    }

    /**
     * Set the next thing to be created to be whatever the pretty name
     * represents. Sets currentCreation.
     * 
     * @param name GUI pretty name of thing to be created, from one of
     *        the EditToolLoaders.
     */
    public void setCurrentCreation(String name) {
        currentCreation = getEditableClassName(name);
    }

    /**
     * Given a pretty name, look through the EditToolLoaders and find
     * out the classname that goes with editing it.
     * 
     * @param prettyName GUI pretty name of tool, or thing to be
     *        created, from one of the EditToolLoaders.
     */
    public String getEditableClassName(String prettyName) {
        for (Iterator it = getLoaders(); it.hasNext();) {
            LoaderHolder lh = (LoaderHolder) it.next();
            EditToolLoader etl = lh.loader;
            String[] ec = etl.getEditableClasses();

            for (int i = 0; i < ec.length; i++) {
                if (prettyName.equals(etl.getPrettyName(ec[i]))) {
                    return ec[i];
                }
            }
        }
        return null;
    }

    /**
     * This is the method that your object can use to find other
     * objects within the MapHandler (BeanContext). This method gets
     * called when the object gets added to the MapHandler, or when
     * another object gets added to the MapHandler after the object is
     * a member.
     * 
     * @param someObj the object that was added to the BeanContext
     *        (MapHandler) that is being searched for. Find the ones
     *        you need, and hook yourself up.
     */
    public void findAndInit(Object someObj) {
        if (someObj instanceof OMDrawingTool) {
            Debug.message("omdtl", "CatalogFilterLauncher found a DrawingTool.");
            setDrawingTool((DrawingTool) someObj);
        }
        if (someObj instanceof CatalogFilterLayer) {
            if (Debug.debugging("omdtl")) {
                Debug.output("CatalogFilterLauncher found a CatalogFilterLayer - "
                        + ((CatalogFilterLayer) someObj).getName());
            }
            catalogFilterLayer = (CatalogFilterLayer) someObj;
            catalogFilterLayer.launcher = this;

            resetGUI();
        }
    }

    /**
     * BeanContextMembershipListener method. Called when a new object
     * is removed from the BeanContext of this object. For the Layer,
     * this method doesn't do anything. If your layer does something
     * with the childrenAdded method, or findAndInit, you should take
     * steps in this method to unhook the layer from the object used
     * in those methods.
     */
    public void findAndUndo(Object someObj) {
        if (someObj instanceof OMDrawingTool) {
            Debug.message("omdtl", "CatalogFilterLauncher found a DrawingTool.");
            OMDrawingTool dt = (OMDrawingTool) someObj;
            if (dt == getDrawingTool()) {
                setDrawingTool(null);
                dt.removePropertyChangeListener(this);
            }
        }
        if (someObj instanceof CatalogFilterLayer) {
            if (Debug.debugging("omdtl")) {
                Debug.output("CatalogFilterLauncher removing a CatalogFilterLayer - "
                        + ((CatalogFilterLayer) someObj).getName());
            }
            if (this == catalogFilterLayer.launcher) {
               catalogFilterLayer.launcher = null;
            }
            catalogFilterLayer = null;

            resetGUI();
        }
    }

    /**
     * Tool interface method. The retrieval tool's interface. This
     * method creates a button that will bring up the LauncherPanel.
     * 
     * @return String The key for this tool.
     */
    public Container getFace() {
        JToolBar jtb = null;
        if (getUseAsTool()) {
            jtb = new com.bbn.openmap.gui.GridBagToolBar();
            //"Catalog Filter Launcher";
            JButton drawingToolButton = new JButton(new ImageIcon(OMDrawingToolLauncher.class.getResource("Drawing.gif"), i18n.get(CatalogFilterLauncher.class,
                    "drawingToolButton",
                    I18n.TOOLTIP,
                    "Catalog Filter Launcher")));
            drawingToolButton.setToolTipText(i18n.get(CatalogFilterLauncher.class,
                    "drawingToolButton",
                    I18n.TOOLTIP,
                    "Catalog Filter Launcher"));
            drawingToolButton.addActionListener(getActionListener());
            jtb.add(drawingToolButton);
        }
        return jtb;
    }

    /**
     * Get the ActionListener that triggers the LauncherPanel. Useful
     * to have to provide an alternative way to bring up the
     * LauncherPanel.
     * 
     * @return ActionListener
     */
    public ActionListener getActionListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                // -1 will get size from pack();
                int w = -1;
                int h = -1;
                int x = 10;
                int y = 10;

                Point loc = getWindowSupport().getComponentLocation();
                if (loc != null) {
                    x = (int) loc.getX();
                    y = (int) loc.getY();
                }

                MapHandler mh = (MapHandler) getBeanContext();
                Frame frame = null;
                if (mh != null) {
                    frame = (Frame) mh.get(java.awt.Frame.class);
                }

                getWindowSupport().displayInWindow(frame, x, y, w, h);
            }
        };
    }

    /**
     * Get the attributes that initalize the graphic.
     */
    public GraphicAttributes getDefaultGraphicAttributes() {
        return defaultGraphicAttributes;
    }

    /**
     * Set the attributes that initalize the graphic.
     */
    public void setDefaultGraphicAttributes(GraphicAttributes ga) {
        defaultGraphicAttributes = ga;
    }

    /**
     * Set the loaders with an Iterator containing EditToolLoaders.
     */
    public void setLoaders(Iterator iterator) {
        loaders.clear();
        while (iterator.hasNext()) {
            addLoader((EditToolLoader) iterator.next());
        }
    }

    /**
     * Returns an iterator of LoaderHolders.
     */
    public Iterator getLoaders() {
        return loaders.iterator();
    }

    public void addLoader(EditToolLoader etl) {
        if (etl != null) {
            String[] classNames = etl.getEditableClasses();
            for (int i = 0; i < classNames.length; i++) {
                loaders.add(new LoaderHolder(etl.getPrettyName(classNames[i]), etl));
            }
        }
    }

    public void removeLoader(EditToolLoader etl) {
        if (etl != null) {
            for (Iterator it = getLoaders(); it.hasNext();) {
                LoaderHolder lh = (LoaderHolder) it.next();
                if (lh.loader == etl) {
                    loaders.remove(lh);
                }
            }
        }
    }

    /**
     * PropertyChangeListener method, to listen for the
     * OMDrawingTool's list of loaders that may or may not change.
     */
    public void propertyChange(PropertyChangeEvent pce) {
        if (pce.getPropertyName() == OMDrawingTool.LoadersProperty) {
            setLoaders(((Vector) pce.getNewValue()).iterator());
            resetGUI();
        }
    }

    public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);

        defaultGraphicAttributes.setProperties(prefix, props); // GraphicAttributes input prefix can't end in "."

        prefix = PropUtils.getScopedPropertyPrefix(prefix);
        maxHorNumLoaderButtons = PropUtils.intFromProperties(props,
                prefix + HorizontalNumberOfLoaderButtonsProperty,
                maxHorNumLoaderButtons);
        useTextEditToolTitles = PropUtils.booleanFromProperties(props, prefix
                + UseLoaderTextProperty, useTextEditToolTitles);
    }

    public Properties getProperties(Properties props) {
        props = super.getProperties(props);
        String prefix = PropUtils.getScopedPropertyPrefix(this);
        props.put(prefix + HorizontalNumberOfLoaderButtonsProperty,
                Integer.toString(maxHorNumLoaderButtons));
        props.put(prefix + UseLoaderTextProperty,
                new Boolean(useTextEditToolTitles).toString());
        return props;
    }

    public Properties getPropertyInfo(Properties props) {
        props = super.getPropertyInfo(props);
        String internString = i18n.get(CatalogFilterLauncher.class,
                HorizontalNumberOfLoaderButtonsProperty,
                I18n.TOOLTIP,
                "Number of loader buttons to place horizontally");
        props.put(HorizontalNumberOfLoaderButtonsProperty, internString);

        internString = i18n.get(CatalogFilterLauncher.class,
                HorizontalNumberOfLoaderButtonsProperty,
                "# Horizontal Buttons");
        props.put(HorizontalNumberOfLoaderButtonsProperty + LabelEditorProperty,
                internString);

        internString = i18n.get(CatalogFilterLauncher.class,
                UseLoaderTextProperty,
                I18n.TOOLTIP,
                "Use text popup for loader selection.");
        props.put(UseLoaderTextProperty, internString);

        internString = i18n.get(CatalogFilterLauncher.class,
                UseLoaderTextProperty,
                "Use Text For Selection");
        props.put(UseLoaderTextProperty + LabelEditorProperty, internString);

        return props;
    }

    public static class LoaderHolder {
        public String prettyName;
        public EditToolLoader loader;

        public LoaderHolder(String pn, EditToolLoader etl) {
            prettyName = pn;
            loader = etl;
        }
    }
}

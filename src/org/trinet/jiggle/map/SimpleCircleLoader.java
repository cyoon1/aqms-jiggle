package org.trinet.jiggle.map;

import com.bbn.openmap.tools.drawing.AbstractToolLoader;
import com.bbn.openmap.tools.drawing.EditClassWrapper;
import com.bbn.openmap.tools.drawing.EditToolLoader;
import com.bbn.openmap.tools.drawing.OMCircleLoader;

import com.bbn.openmap.omGraphics.EditableOMCircle;
import com.bbn.openmap.omGraphics.EditableOMGraphic;
import com.bbn.openmap.omGraphics.GraphicAttributes;
import com.bbn.openmap.omGraphics.OMCircle;
import com.bbn.openmap.omGraphics.OMGraphic;

/**
 * Loader that knows how to create/edit an OMCircle 
 */
public class SimpleCircleLoader extends AbstractToolLoader implements
        EditToolLoader {

    protected String circleClassName = "com.bbn.openmap.omGraphics.OMCircle";

    public SimpleCircleLoader() {
        init();
    }

    public void init() {
        EditClassWrapper ecw = new EditClassWrapper(circleClassName,
                "com.bbn.openmap.omGraphics.EditableOMCircle", "editablecircle.gif",
                i18n.get(OMCircleLoader.class, "omcircle.circle", "Circle"));
        addEditClassWrapper(ecw);
    }

    /**
     * Give the classname of a graphic to create, returning an
     * EditableOMGraphic for that graphic. The GraphicAttributes
     * object lets you set some of the initial parameters of the
     * circle, like circle type and rendertype.
     */
    public EditableOMGraphic getEditableGraphic(String classname,
                                                GraphicAttributes ga) {
        String name = classname.intern();
        if (name == circleClassName) {
            return new EditableOMCircle(ga);
        }
        return null;
    }

    /**
     * Give an OMGraphic to the EditToolLoader, which will create an
     * EditableOMGraphic for it.
     */
    public EditableOMGraphic getEditableGraphic(OMGraphic graphic) {
        if (graphic instanceof OMCircle) {
            return new EditableOMCircle((OMCircle) graphic);
        }
        return null;
    }
}

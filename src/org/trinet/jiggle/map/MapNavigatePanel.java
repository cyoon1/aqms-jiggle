//package com.bbn.openmap.gui;
package org.trinet.jiggle.map;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.bbn.openmap.gui.OMToolComponent;
import com.bbn.openmap.Environment;
import com.bbn.openmap.event.CenterListener;
import com.bbn.openmap.event.CenterSupport;
import com.bbn.openmap.event.PanListener;
import com.bbn.openmap.event.PanSupport;
import com.bbn.openmap.event.ZoomListener;
import com.bbn.openmap.event.ZoomEvent;
import com.bbn.openmap.event.ZoomSupport;
import com.bbn.openmap.util.Debug;

/**
 * A Navigation Rosette Bean. This bean is a source for PanEvents and
 * CenterEvents.
 */
public class MapNavigatePanel extends OMToolComponent implements Serializable,
        ActionListener {

    public final static String panNWCmd = "panNW";
    public final static String panNCmd = "panN";
    public final static String panNECmd = "panNE";
    public final static String panECmd = "panE";
    public final static String panSECmd = "panSE";
    public final static String panSCmd = "panS";
    public final static String panSWCmd = "panSW";
    public final static String panWCmd = "panW";
    public final static String centerCmd = "center";

    protected transient JButton nwButton = null;
    protected transient JButton nButton = null;
    protected transient JButton neButton = null;
    protected transient JButton eButton = null;
    protected transient JButton seButton = null;
    protected transient JButton sButton = null;
    protected transient JButton swButton = null;
    protected transient JButton wButton = null;
    protected transient JButton cButton = null;

    // default icon names
    protected static String nwName = "nw.gif";
    protected static String nName = "n.gif";
    protected static String neName = "ne.gif";
    protected static String eName = "e.gif";
    protected static String seName = "se.gif";
    protected static String sName = "s.gif";
    protected static String swName = "sw.gif";
    protected static String wName = "w.gif";
    protected static String cName = "center.gif";

    protected transient ZoomSupport zoomDelegate = null;
    protected PanSupport panDelegate;
    protected CenterSupport centerDelegate;
    protected boolean useTips = false;
    protected float panFactor = 1f;

    // protected int height = 0; // calculated
    // protected int width = 0; // calculated

    protected boolean useDefaultCenter = false;
    protected float defaultCenterLat = 0;
    protected float defaultCenterLon = 0;

    public final static String defaultKey = "mapnavigatepanel";

    /**
     * Construct the NavigationPanel.
     */
    public MapNavigatePanel() {
        super();
        setKey(defaultKey);
        panDelegate = new PanSupport(this);
        centerDelegate = new CenterSupport(this);
        zoomDelegate = new ZoomSupport(this);

        JPanel panel = new JPanel();
        GridBagLayout internalGridbag = new GridBagLayout();
        GridBagConstraints c2 = new GridBagConstraints();
        panel.setLayout(internalGridbag);

        // begin top row
        String info = i18n.get(MapNavigatePanel.class,
                "panel.northwest",
                "Pan Northwest");
        nwButton = getButton(nwName, info, panNWCmd);
        c2.gridx = 0;
        c2.gridy = 0;
        internalGridbag.setConstraints(nwButton, c2);
        panel.add(nwButton);

        info = i18n.get(MapNavigatePanel.class, "panel.north", "Pan North");
        nButton = getButton(nName, info, panNCmd);
        c2.gridx = 1;
        c2.gridy = 0;
        internalGridbag.setConstraints(nButton, c2);
        panel.add(nButton);

        info = i18n.get(MapNavigatePanel.class, "panel.northeast", "Pan Northeast");
        neButton = getButton(neName, info, panNECmd);
        c2.gridx = 2;
        c2.gridy = 0;
        internalGridbag.setConstraints(neButton, c2);
        panel.add(neButton);

        // begin middle row
        info = i18n.get(MapNavigatePanel.class, "panel.west", "Pan West");
        wButton = getButton(wName, info, panWCmd);
        c2.gridx = 0;
        c2.gridy = 1;
        internalGridbag.setConstraints(wButton, c2);
        panel.add(wButton);

        info = i18n.get(MapNavigatePanel.class,
                "panel.centerAtStart",
                "Center Map at Starting Coords");
        cButton = getButton(cName, info, centerCmd);
        c2.gridx = 1;
        c2.gridy = 1;
        internalGridbag.setConstraints(cButton, c2);
        panel.add(cButton);

        info = i18n.get(MapNavigatePanel.class, "panel.east", "Pan East");
        eButton = getButton(eName, info, panECmd);
        c2.gridx = 2;
        c2.gridy = 1;
        internalGridbag.setConstraints(eButton, c2);
        panel.add(eButton);

        // begin bottom row
        info = i18n.get(MapNavigatePanel.class, "panel.southwest", "Pan Southwest");
        swButton = getButton(swName, info, panSWCmd);
        c2.gridx = 0;
        c2.gridy = 2;
        internalGridbag.setConstraints(swButton, c2);
        panel.add(swButton);

        info = i18n.get(MapNavigatePanel.class, "panel.south", "Pan South");
        sButton = getButton(sName, info, panSCmd);
        c2.gridx = 1;
        c2.gridy = 2;
        internalGridbag.setConstraints(sButton, c2);
        panel.add(sButton);

        info = i18n.get(MapNavigatePanel.class, "panel.southeast", "Pan Southeast");
        seButton = getButton(seName, info, panSECmd);
        c2.gridx = 2;
        c2.gridy = 2;
        internalGridbag.setConstraints(seButton, c2);
        panel.add(seButton);

        add(panel);
    }

    /**
     * Add the named button to the panel.
     * 
     * @param name GIF image name
     * @param info ToolTip text
     * @param command String command name
     *  
     */
    protected JButton getButton(String name, String info, String command) {
        URL url = MapNavigatePanel.class.getResource("/images/"+name);
        ImageIcon icon = new ImageIcon(url, info);
        JButton b = new JButton(icon);
        b.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        b.setToolTipText(info);
        b.setMargin(new Insets(0, 0, 0, 0));
        b.setActionCommand(command);
        b.addActionListener(this);
        b.setBorderPainted(Debug.debugging("layout"));
        b.setOpaque(false);
        return b;
    }

    public synchronized void addZoomListener(ZoomListener listener) {
        zoomDelegate.addZoomListener(listener);
    }

    /**
     * Remove a ZoomListener from the listener list.
     * 
     * @param listener The ZoomListener to be removed
     */
    public synchronized void removeZoomListener(ZoomListener listener) {
        zoomDelegate.removeZoomListener(listener);
    }


    /**
     * Add a CenterListener.
     * 
     * @param listener CenterListener
     */
    public synchronized void addCenterListener(CenterListener listener) {
        centerDelegate.addCenterListener(listener);
    }

    /**
     * Remove a CenterListener
     * 
     * @param listener CenterListener
     */
    public synchronized void removeCenterListener(CenterListener listener) {
        centerDelegate.removeCenterListener(listener);
    }

    /**
     * Add a PanListener.
     * 
     * @param listener PanListener
     */
    public synchronized void addPanListener(PanListener listener) {
        panDelegate.addPanListener(listener);
    }

    /**
     * Remove a PanListener
     * 
     * @param listener PanListener
     */
    public synchronized void removePanListener(PanListener listener) {
        panDelegate.removePanListener(listener);
    }

    protected synchronized void fireZoomEvent(float scale) {
        if (Debug.debugging("navpanel")) {
            Debug.output("MapNavigatePanel setting scale to " + scale);
        }
        zoomDelegate.fireZoom(ZoomEvent.ABSOLUTE, scale);
    }

    /**
     * Fire a CenterEvent.
     */
    protected synchronized void fireCenterEvent(float lat, float lon) {
        centerDelegate.fireCenter(lat, lon);
    }

    /**
     * Fire a PanEvent.
     * 
     * @param az azimuth east of north
     */
    protected synchronized void firePanEvent(float az) {
        panDelegate.firePan(az);
    }

    /**
     * Get the pan factor.
     * <p>
     * The panFactor is the amount of screen to shift when panning in
     * a certain direction: 0=none, 1=half-screen shift.
     * 
     * @return float panFactor (0.0 &lt;= panFactor &lt;= 1.0)
     */
    public float getPanFactor() {
        return panFactor;
    }

    /**
     * Set the pan factor.
     * <p>
     * This defaults to 1.0. The panFactor is the amount of screen to
     * shift when panning in a certain direction: 0=none,
     * 1=half-screen shift.
     * 
     * @param panFactor (0.0 &lt;= panFactor &lt;= 1.0)
     */
    public void setPanFactor(float panFactor) {
        if ((panFactor < 0f) || (panFactor > 1f)) {
            throw new IllegalArgumentException("should be: (0.0 <= panFactor <= 1.0)");
        }
        this.panFactor = panFactor;
    }

    /**
     * Use this function to set where you want the map projection to
     * pan to when the user clicks on "center" button on the
     * navigation panel. The scale does not change. When you call this
     * function, the projection does not change.
     * 
     * @param passedLat float the center latitude (in degrees)
     * @param passedLon float the center longitude (in degrees)
     */
    public void setDefaultCenter(float passedLat, float passedLon) {
        useDefaultCenter = true;
        defaultCenterLat = passedLat;
        defaultCenterLon = passedLon;
    }

    /**
     * ActionListener Interface.
     * 
     * @param e ActionEvent
     */
    public void actionPerformed(java.awt.event.ActionEvent e) {

        String command = e.getActionCommand();

        Debug.message("navpanel", "MapNavigatePanel.actionPerformed(): " + command);
        if (command.equals(panNWCmd)) {
            firePanEvent(-45f);
        } else if (command.equals(panNCmd)) {
            firePanEvent(0f);
        } else if (command.equals(panNECmd)) {
            firePanEvent(45f);
        } else if (command.equals(panECmd)) {
            firePanEvent(90f);
        } else if (command.equals(panSECmd)) {
            firePanEvent(135f);
        } else if (command.equals(panSCmd)) {
            firePanEvent(180f);
        } else if (command.equals(panSWCmd)) {
            firePanEvent(-135f);
        } else if (command.equals(panWCmd)) {
            firePanEvent(-90f);
        } else if (command.equals(centerCmd)) {
            // go back to the center point

            float lat;
            float lon;
            if (useDefaultCenter) {
                lat = defaultCenterLat;
                lon = defaultCenterLon;
            } else {
                lat = Environment.getFloat(Environment.Latitude, 0f);
                lon = Environment.getFloat(Environment.Longitude, 0f);
            }
            fireCenterEvent(lat, lon);
            fireZoomEvent(Environment.getFloat(Environment.Scale, 5000000f));
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    //// OMComponentPanel methods to make the tool work with
    //// the MapHandler to find objects it needs.
    ///////////////////////////////////////////////////////////////////////////

    public void findAndInit(Object obj) {
        if (obj instanceof PanListener) {
            addPanListener((PanListener) obj);
        }
        if (obj instanceof CenterListener) {
            addCenterListener((CenterListener) obj);
        }
        if (obj instanceof ZoomListener) {
            addZoomListener((ZoomListener) obj);
        }
    }

    public void findAndUndo(Object obj) {
        if (obj instanceof PanListener) {
            removePanListener((PanListener) obj);
        }
        if (obj instanceof CenterListener) {
            removeCenterListener((CenterListener) obj);
        }
        if (obj instanceof ZoomListener) {
            removeZoomListener((ZoomListener) obj);
        }
    }

}


package org.trinet.jiggle.map;

import java.awt.Font;

import com.bbn.openmap.omGraphics.OMGraphic;
import java.util.Properties;
import com.bbn.openmap.layer.util.LayerUtils;
import com.bbn.openmap.omGraphics.OMPoly;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.omGraphics.OMCircle;
import com.bbn.openmap.PropertyConsumer;
//import com.isti.util.propertyeditor.ValueArrayPropertyEditor;

/**
 * The location marker.
 */
public class JiggleLocationMarker implements Cloneable
{
  /** Rectangle marker type (default). */
  public final static String rectangleMarkerType = "Rectangle";

  /** Triangle marker type. */
  public final static String triangleMarkerType = "Triangle";

  /** Circle marker type. */
  public final static String circleMarkerType = "Circle";

  /** Star marker type. */
  public final static String starMarkerType = "Star";

  /** Hex marker type. */
  public final static String hexMarkerType = "Hex";

  /** Diamond marker type. */
  public final static String diamondMarkerType = "Diamond";

  /** Character text marker type. */
  public final static String  textMarkerType = "Text";

  /** Default marker type (rectangle). */
  public final static String defaultMarkerType = rectangleMarkerType;

  /** Default marker size. */
  public final static int defaultMarkerSize = 2;

  /** Default marker text. */
  public final static String defaultMarkerText = "O";

  /** Default marker font. */
  public final static String defaultMarkerFont = "SansSerif";

  private int markerSize = 0;  //marker size
  private String markerType = null;  //marker type
  private String markerText = null;
  private String markerFont = null;
  private Font textFont = null;

  /** Marker size property. */
  public final static String markerSizeProperty = "markerSize";

  /** Marker type property. */
  public final static String markerTypeProperty = "markerType";

  /** Marker text property. */
  public final static String markerTextProperty = "markerText";

  /** Marker text font property. */
  public final static String markerFontProperty = "markerFont";

  /**
   * Creates the marker properties from the default values.
   */
  public JiggleLocationMarker()
  {
    this(defaultMarkerSize, defaultMarkerType, defaultMarkerText, defaultMarkerFont);
  }

  /**
   * Creates the marker properties from the specified values.
   * @param markerSize the marker size.
   * @param markerType the marker type.
   */
  private JiggleLocationMarker(int markerSize, String markerType, String markerText, String markerFont)
  {
    this.markerSize = markerSize;
    this.markerType = markerType;
    this.markerText = markerText;
    this.markerFont = markerFont;
  }

  // AWW: This needs properties for : setFillPaint(Paint), setSelectPaint(Paint) setLinePaint(Paint)
  // use color input properties to change default color characteristics of OMGraphic for marker
  //
  /**
   * Creates the marker properties from the properties list.
   * @param prefix string prefix used in the properties list for this object.
   * @param properties the properties set in the properties list.
   * @param defaultProperties the properties to use if not specified in the
   * properties list.
   */
  public JiggleLocationMarker(String prefix, Properties properties, JiggleLocationMarker defaultProperties) {
      this(
        getMarkerSizeFromProperties(prefix, properties, defaultProperties),
        getMarkerTypeFromProperties(prefix, properties, defaultProperties),
        getMarkerTextFromProperties(prefix, properties, defaultProperties),
        getMarkerFontFromProperties(prefix, properties, defaultProperties)
      );
  }

  /**
   * Create a clone of this object.
   * @return the clone of this object.
   */
  public Object clone() {
    //new JiggleLocationMarker(markerSize, markerType, markerText, markerFont);
    Object obj = null;
    try {
      obj = super.clone();
    }
    catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
    }
    return obj;
  }

  /**
   * Create the marker graphic.
   * @param lat the marker latitude.
   * @param lon the marker longitude.
   * @return the marker graphic.
   */
  public OMGraphic createMarkerGraphic(float lat, float lon)
  {
    final OMGraphic graphicObj;
    if (circleMarkerType.equals(markerType))
    {
      final int circleSize = markerSize * 2;
      graphicObj = new OMCircle(lat, lon, circleSize, circleSize);
    }
    else if (triangleMarkerType.equals(markerType))
    {
      final int[] xypoints = new int[]
      {
        0, -markerSize,
        markerSize, markerSize,
        -markerSize, markerSize
      };
      graphicObj = new OMPoly(lat, lon, xypoints, OMPoly.COORDMODE_ORIGIN);
    }
    else if (starMarkerType.equals(markerType))
    {
      final int[] xypoints  = new int[]
      {
        0, -markerSize,
        Math.round((float).25*markerSize), Math.round((float)-.25*markerSize),
        Math.round((float)1.25*markerSize), Math.round((float)-.25*markerSize),
        Math.round((float).5*markerSize), Math.round((float).25*markerSize),
        Math.round((float).75*markerSize), markerSize,
        0, Math.round((float).5*markerSize),
        Math.round((float)-.75*markerSize), markerSize,
        Math.round((float)-.5*markerSize), Math.round((float).25*markerSize),
        Math.round((float)-1.25*markerSize), Math.round((float)-.25*markerSize),
        Math.round((float)-.25*markerSize), Math.round((float)-.25*markerSize),
        0, -markerSize
      };  // star
      graphicObj = new OMPoly(lat, lon, xypoints, OMPoly.COORDMODE_ORIGIN);
    }
    else if (diamondMarkerType.equals(markerType))
    {
      final int[] xypoints  = new int[]
      {
        0, -markerSize,
        Math.round((float).5*markerSize), 0,
        0, markerSize,
        Math.round((float)-.5*markerSize), 0,
        0, -markerSize 
      }; // diamond
      graphicObj = new OMPoly(lat, lon, xypoints, OMPoly.COORDMODE_ORIGIN);
    }
    else if (hexMarkerType.equals(markerType))
    {
      final int[] xypoints  = new int[]
      {
        Math.round((float)-1.25*markerSize), 0,
        Math.round((float)-.5*markerSize), markerSize,
        Math.round((float).5*markerSize), markerSize,
        Math.round((float)1.25*markerSize), 0,
        Math.round((float).5*markerSize), -markerSize,
        Math.round((float)-.5*markerSize), -markerSize,
        Math.round((float)-1.25*markerSize), 0
      }; // hexagon
      graphicObj = new OMPoly(lat, lon, xypoints, OMPoly.COORDMODE_ORIGIN);
    }
    else if (textMarkerType.equals(markerType))
    {
       if (textFont == null) {
           textFont = new Font(markerFont, Font.BOLD, markerSize);
       } 
       //OMText omt = new OMText(lat, lon, 0, 0, markerText, textFont, OMText.JUSTIFY_CENTER);
       JiggleSymbolText omt = new JiggleSymbolText(lat, lon, 0, 0, markerText, textFont, OMText.JUSTIFY_CENTER);
       graphicObj = omt;
    }
    else
    {
      graphicObj = new OMRect(lat, lon, -markerSize, -markerSize, markerSize, markerSize);
    }
    return graphicObj;
  }

  /**
   * Gets the marker size.
   * @return the marker size.
   */
  public int getMarkerSize()
  {
    return markerSize;
  }

  /**
   * Gets the marker type.
   * @return the marker type.
   */
  public String getMarkerType()
  {
    return markerType;
  }

  /**
   * Gets the marker text.
   * @return the marker text.
   */
  public String getMarkerText()
  {
    return markerText;
  }

  /**
   * Gets the marker font name.
   * @return the marker font name.
   */
  public String getMarkerFont()
  {
    return markerFont;
  }

  /**
   * Gets the marker properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param props a Properties object to load the marker properties into or
   * null for a new Properties list.
   * object should be created.
   * @return the properties.
   */
  public Properties getProperties(String prefix, Properties props)
  {
    if (props == null)
      props = new Properties();
    props.put(prefix+markerSizeProperty, Integer.toString(markerSize));
    props.put(prefix+markerTypeProperty, markerType);
    props.put(prefix+markerTextProperty, markerText);
    props.put(prefix+markerFontProperty, markerFont);
    return props;
  }

  /**
   * Overridden version that handles the "ViewRatio" properties.
   * @param list properties object to use.
   * @return The properties object.
   */
  public Properties getPropertyInfo(Properties list)
  {
    if (list == null)
      list = new Properties();

    final String editorClass = MarkerTypePropertyEditor.class.getName();
    list.setProperty(
        markerTypeProperty + "." + PropertyConsumer.EditorProperty,
        editorClass);

    return list;
  }

  /**
   * Sets the marker size.
   * @param markerSize the marker size.
   */
  public void setMarkerSize(int markerSize)
  {
    this.markerSize = markerSize;
  }

  /**
   * Sets the marker type.
   * @param markerType the marker type.
   */
  public void setMarkerType(String markerType)
  {
    this.markerType = markerType;
  }

  /**
   * Sets the marker text.
   * @param markerText the marker text.
   */
  public void setMarkerText(String markerText)
  {
    this.markerText = markerText;
  }

  /**
   * Sets the marker font name.
   * @param markerFont the marker font name.
   */
  public void setMarkerFont(String markerFont)
  {
    this.markerFont = markerFont;
  }

  /**
   * Sets the marker properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param props a Properties object to load the marker properties from.
   */
  public void setProperties(String prefix, Properties props)
  {
    setMarkerSize(getMarkerSizeFromProperties(prefix, props, this));
    setMarkerType(getMarkerTypeFromProperties(prefix, props, this));
    setMarkerText(getMarkerTextFromProperties(prefix, props, this));
    setMarkerFont(getMarkerFontFromProperties(prefix, props, this));
  }

  /**
   * Gets the marker size from the properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param properties the properties set in the properties list.
   * @param defaultProperties the default properties.
   * @return the marker size.
   */
  private static int getMarkerSizeFromProperties(String prefix,
      Properties properties, JiggleLocationMarker defaultProperties)
  {
    return LayerUtils.intFromProperties(
        properties, prefix + markerSizeProperty, defaultProperties.markerSize);
  }

  /**
   * Gets the marker type from the properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param properties the properties set in the properties list.
   * @param defaultProperties the default properties.
   * @return the marker type.
   */
  private static String getMarkerTypeFromProperties(String prefix,
      Properties properties, JiggleLocationMarker defaultProperties)
  {
    String markerType = properties.getProperty(prefix + markerTypeProperty);
    if (markerType == null)
      markerType = defaultProperties.markerType;
    return markerType;
  }

  /**
   * Gets the marker text from the properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param properties the properties set in the properties list.
   * @param defaultProperties the default properties.
   * @return the marker text.
   */
  private static String getMarkerTextFromProperties(String prefix,
      Properties properties, JiggleLocationMarker defaultProperties)
  {
    String markerText = properties.getProperty(prefix + markerTextProperty);
    if (markerText == null)
      markerText = defaultProperties.markerText;
    return markerText;
  }

  /**
   * Gets the marker font name from the properties.
   *
   * @param prefix string prefix used in the properties list for this object.
   * @param properties the properties set in the properties list.
   * @param defaultProperties the default properties.
   * @return the marker font name.
   */
  private static String getMarkerFontFromProperties(String prefix,
      Properties properties, JiggleLocationMarker defaultProperties)
  {
    String markerFont = properties.getProperty(prefix + markerFontProperty);
    if (markerFont == null)
      markerFont = defaultProperties.markerFont;
    return markerFont;
  }

  /**
   * Marker type property editor.
   */
  public static class MarkerTypePropertyEditor extends ValueArrayMapPropertyEditor
  {
    private final static Object [] validValuesArr =
    {
      rectangleMarkerType,
      triangleMarkerType,
      circleMarkerType,
      starMarkerType,
      hexMarkerType,
      diamondMarkerType,
      textMarkerType
    };

    public MarkerTypePropertyEditor()
    {
      super(validValuesArr);
    }
  }
}

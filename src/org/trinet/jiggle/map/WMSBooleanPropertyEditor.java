package org.trinet.jiggle.map;
import javax.swing.JRadioButton;
import com.bbn.openmap.util.propertyEditor.TrueFalsePropertyEditor;

public class WMSBooleanPropertyEditor extends TrueFalsePropertyEditor {

    public final static String TRUE = "TRUE";
    public final static String FALSE = "FALSE";

    public WMSBooleanPropertyEditor() {
        setUseAltCommandStrings(true);
        trueButton = new JRadioButton("True");
        trueButton.setActionCommand(WMSBooleanPropertyEditor.TRUE);
        falseButton = new JRadioButton("False");
        falseButton.setActionCommand(WMSBooleanPropertyEditor.FALSE);
    }
}

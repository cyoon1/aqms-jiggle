package org.trinet.jiggle.map;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import com.bbn.openmap.gui.*;
import com.bbn.openmap.Environment;
import com.bbn.openmap.PropertyHandler;
import com.bbn.openmap.util.Debug;

public class JiggleMapFrame extends OpenMapFrame {
    /**
     * Create the frame with "OpenMap <version>" in the title.
     */
    public JiggleMapFrame() {
        this(Environment.get(Environment.Title));
    }

    /**
     * @param useAsInternalFrameRootPaneIfNecessary will tell the
     *        to set its root pane as the Environment's
     *        desktop if the Environment has been told to use internal
     *        frames, and if a root pane hasn't been set.
     */
    public JiggleMapFrame(boolean useAsInternalFrameRootPaneIfNecessary) {
        this(Environment.get(Environment.Title), useAsInternalFrameRootPaneIfNecessary);
    }

    /**
     * Create a OpenMap frame with a title.
     * 
     * @param title The Frame title.
     */
    public JiggleMapFrame(String title) {
        this(title, true);
    }

    /**
     * Create a OpenMap frame with a title, with a WindowListner that
     * says what to do when the frame is closed.
     * 
     * @param title The Frame title.
     * @param useAsInternalFrameRootPaneIfNecessary will tell the
     *        frame to set its root pane as the Environment's
     *        desktop if the Environment has been told to use internal
     *        frames, and if a root pane hasn't been set.
     */
    public JiggleMapFrame(String title, boolean useAsInternalFrameRootPaneIfNecessary) {
        super(title, useAsInternalFrameRootPaneIfNecessary);
    }

    public void findAndInit(Object someObj) {
        if (someObj instanceof MapPanel && someObj instanceof Container) {
            Debug.message("basic", "JiggleMapFrame: Found a MapPanel");
            getContentPane().add((Container) someObj);

            JMenuBar jmb = ((MapPanel) someObj).getMapMenuBar();
            if (jmb != null) {
                Debug.message("basic", "JiggleMapFrame: Got MenuBar from MapPanel");
                JMenu jm = jmb.getMenu(0);
                if (jm != null)  {
                   Component [] me = jm.getComponents();
                   for (int i=0; i < me.length; i++) {
                     if (me[i] instanceof org.trinet.jiggle.map.QuitMenuItem) {
                         jm.remove(me[i]);
                         break;
                     }
                   }
                   jm.add( new org.trinet.jiggle.map.QuitMenuItem(this) );
                }
                getRootPane().setJMenuBar(jmb);
            }

            setPosition();
            invalidate();
            show();
        }

        // We shouldn't find this if we've already defined one
        // in the MapPanel, but we have this for backward
        // compatibility.
        if (someObj instanceof JMenuBar) {
            Debug.message("basic", "JiggleMapFrame: Found a MenuBar");
            getRootPane().setJMenuBar((JMenuBar) someObj);
            invalidate();
        }

        if (someObj instanceof PropertyHandler) {
            // Might get called twice if someone uses the
            // ComponentFactory to create the frame, but the
            // default OpenMap application doesn't use the
            // ComponentFactory to create the frame, so
            // setProperties isn't called by default.
            setProperties(((PropertyHandler) someObj).getProperties());
        }
    }

}

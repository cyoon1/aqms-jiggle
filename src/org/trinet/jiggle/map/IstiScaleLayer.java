package org.trinet.jiggle.map;

import java.util.*;

import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.event.ProjectionEvent;
import com.bbn.openmap.proj.Projection;
import java.awt.Graphics;
import com.bbn.openmap.I18n;
import com.bbn.openmap.PropertyConsumer;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.util.PaletteHelper;
//import com.bbn.openmap.util.propertyEditor.Inspector;
import java.awt.FontMetrics;
//import java.awt.GridLayout;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

public class IstiScaleLayer extends org.trinet.jiggle.map.IstiLabelLayer
{
  public final static transient String UseMetricProperty = "useMetricUnits";

  public static final int SCALE_KILOM = 0;
  public static final int SCALE_METER = 1;
  public static final int SCALE_MILES = 2;
  public static final int SCALE_FEET = 3;
  public static final String[] aUnitName =
  {
    "km",
    "meters",
    "miles",
    "feet"
  };
  /**
   * Maximum projection-zoom-scale value for visible scale display
   * (4.01E7 = just over 500 mi).
   */
  public static final float MAX_PROJSCALE_VAL = 4.01E7f;

  private boolean useMetricFlag = true;  //true to use metric system

  private double dMapTop,         //world coordinates of map window
    dMapLeft,
    dMapBottom,
    dMapRight,
    dMapScaleX,
    dMapXRange;

  private int ibarlength;
  private final static int ibarHeight = 2;
  private final static int scaleHeight = 10;
  private final int textHeight;


  /**
   * Construct the IstiScalelLayer.
   */
  public IstiScaleLayer()
  {
    super();
    //get the font metrics to find the size
    FontMetrics fm = getFontMetrics(text.getFont());
    textHeight = fm.getHeight();
//    logObj.debug("Constructed IstiScaleLayer");
  }

  /**
   * Gets the use metric flag.
   * @return true if metric system is used.
   */
  public boolean getUseMetric()
  {
    return useMetricFlag;
  }

  /**
   * Sets the use metric flag.
   * @param b true to use metric system for scale.
   */
  public synchronized void setUseMetric(boolean b)
  {
    if (useMetricFlag != b)  //if change
    {
      useMetricFlag = b;     //save new value
//      logObj.debug("IstiScaleLayer:  setUseMetric(useMetricFlag=" + b + ")");
      setProjection(getProjection()); //update the projection
    }
  }

  /**
   * Set map projection parameters.
   * @param proj projection.
   */
  public synchronized void setProjection(Projection proj)
  {
//    logObj.debug("IstiScaleLayer:  entered setProjection()");
    super.setProjection(proj);
    if (proj == null)
      return;

    double toplat = proj.getUpperLeft().getLatitude();
    double bottomlat = proj.getLowerRight().getLatitude();
    double centermeridian = proj.getCenter().getLongitude();
    final double minrange = 1.0e-07;

    /*-----------------------------------------------------
    * find average horizontal scale
    * (integral of cos(lat) from top to bottom)/(delta lat)
    * weighted by (pi/4 - lat squared)
    *
    *	itegral of:
    *	((pisquared/4)-thetasquared))*cos(theta)*d(theta)
    *   from lat1 to lat2
    */

    double lat1 = toplat;
    double lat2 = bottomlat;

    if( lat1 > 90.0 )
      lat1 = 90.0;
    else if( lat1 < -90.0 )
      lat1 = -90.0;

    if( lat2 > 90.0 )
      lat2 = 90.0;
    else if( lat2 < -90.0 )
      lat2 = -90.0;

    if( lat1 - lat2 < minrange )
      return;

    lat1 = Math.toRadians(lat1);
    lat2 = Math.toRadians(lat2);

    /* geodaspect = ( Math.sin(lat1) - Math.sin(lat2) ) / (lat1-lat2); */

    double numerator =  (4.4674011-lat1*lat1)*Math.sin(lat1) - 2.0*lat1*Math.cos(lat1)
      -(4.4674011-lat2*lat2)*Math.sin(lat2) + 2.0*lat2*Math.cos(lat2);

    double denominator =  (2.4674011 - lat1*lat1/3.0)*lat1
      -(2.4674011 - lat2*lat2/3.0)*lat2;

    if( denominator < minrange )
      return;

    double geodaspect	= numerator / denominator;

    if( geodaspect < minrange )
      return;

    if( proj.getWidth() <= 0  || proj.getHeight() <= 0 )
      return;           //if width or height zero then abort

    double yrange = toplat - bottomlat;
    double dPixelAspect = (double)(proj.getWidth()) / proj.getHeight();

    if( dPixelAspect < 1e-20)
      return;           //if value is very small or negative then abort

    double xrange =  (yrange * (double)(proj.getWidth())) /
                           (geodaspect * dPixelAspect * (proj.getHeight()));

    while( centermeridian < -180.0 )
      centermeridian += 360.0;

    while( centermeridian > 180.0 )
      centermeridian -= 360.0;

    dMapTop    = toplat;
    dMapBottom = bottomlat;
    dMapLeft   = centermeridian - xrange / 2.0;
    dMapRight  = centermeridian + xrange / 2.0;
    dMapXRange = xrange;

    dMapScaleX = (double)(proj.getWidth()) / dMapXRange;

//    logObj.debug("IstiScaleLayer:  xrange=" + xrange + ", proj.getWidth()=" +
//        proj.getWidth() + ", geodaspect=" + geodaspect + ", dPixelAspect=" +
//                   dPixelAspect + ", proj.getHeight()=" + proj.getHeight() +
//                                              ", dMapScaleX=" + dMapScaleX);
//    logObj.debug("IstiScaleLayer:  toplat=" + toplat + ", bottomlat=" +
//                          bottomlat + ", centermeridian=" + centermeridian);

    initScaleBar();
//    logObj.debug("IstiScaleLayer:  exitted setProjection()");
  }

  /**
   * Initialize scale bar
   */
  protected void initScaleBar()
  {
//    logObj.debug("IstiScaleLayer:  entered initScaleBar()");
    double centerlat, dscale, dbarsize;
    long ibarsize = 1;
//    int ibarleft, ibarbottom, nchar;
//    short labeltextrow, labeltextcol;
    int unit;

    if (useMetricFlag)
      unit = SCALE_KILOM;
    else
      unit = SCALE_MILES;

    /*-----------------------------------------------------------
     * find optimum size of scale bar
     */

    centerlat = (dMapTop + dMapBottom) / 2.0;

//    logObj.debug("IstiScaleLayer:  initial centerlat=" + centerlat);

    if( centerlat > 89.0 )
      centerlat = 89.0;

    else if( centerlat < -89.0 )
      centerlat = -89.0;

//    logObj.debug("IstiScaleLayer:  adjusted centerlat=" + centerlat);

    dscale = 111.1950032 * Math.cos( Math.toRadians( centerlat ) ); /* scale: km/ deg.lon */

    if (!getUseMetric())
      dscale *= 0.621371192;                       /* scale: mi/ deg.lon */

//    logObj.debug("IstiScaleLayer:  dscale=" + dscale);

    dbarsize = dscale*dMapXRange/4.5;  /* length of scale bar, world units */

//    logObj.debug("IstiScaleLayer:  initial dbarsize=" + dbarsize);

    /*---------------------------------------------------------------------
    * find scale length that fits optimum size to nearest integral units
     */

    if( dbarsize < 1.0 )
    {
      dbarsize *= 1000.0;
      dscale   *= 1000.0;
      if (!getUseMetric())
      {
        dbarsize *= 5.2800;
        dscale   *= 5.2800;
      }
      unit++;
    }

//    logObj.debug("IstiScaleLayer:  after <1.0 dbarsize=" + dbarsize);

    while( dbarsize > 10.0 )
    {
      dbarsize = dbarsize/ 10.0;
//      logObj.debug("IstiScaleLayer:  in dbarsize/10.0 loop:  " + dbarsize);
      ibarsize = ibarsize * 10;
    }

//    logObj.debug("IstiScaleLayer:  after >10.0 dbarsize=" + dbarsize);

    if( dbarsize > 5.0 )
    {
      dbarsize = dbarsize/ 5.0;
      ibarsize = ibarsize * 5;
    }

//    logObj.debug("IstiScaleLayer:  after >5.0 dbarsize=" + dbarsize);

    while( dbarsize > 2.0 )
    {
      dbarsize = dbarsize/ 2.0;
//      logObj.debug("IstiScaleLayer:  in dbarsize/2.0 loop:  " + dbarsize);
      ibarsize = ibarsize * 2;
    }

//    logObj.debug("IstiScaleLayer:  after >2.0 dbarsize=" + dbarsize);

    dscale = dMapScaleX / dscale;                  /* scale: pixels per mi */

//    logObj.debug("IstiScaleLayer:  adjusted dscale=" + dscale);

    ibarlength = (int)(0.5 + ibarsize * dscale); /* pixel size of scalebar */

//    logObj.debug("IstiScaleLayer:  ibarlength=" + ibarlength);

    /*-----------------------------------------------------------
    * label scale bar
     */

    labelText = ibarsize + " " + aUnitName[unit];

//    logObj.debug("IstiScaleLayer:  labelText = \"" + labelText + "\"");
//    logObj.debug("IstiScaleLayer:  exitted initScaleBar()");
  }

  /**
   * Invoked when the projection has changed or this Layer has been
   * added to the MapBean.  This method needs to be overridden to get the
   * subclass version of 'setProjection()' to be used.
   * @param e ProjectionEvent
   */
  public synchronized void projectionChanged(ProjectionEvent e)
  {
    setProjection(e);
    repaint();
  }

  /**
   * Implementing the ProjectionPainter interface.  This method needs to be
   * overridden to get the subclass version of 'setProjection()' to be used.
   * @param proj Projection
   * @param g Graphics
   */
  public synchronized void renderDataForProjection(Projection proj, Graphics g)
  {
    if (proj != null)
    {
      setProjection(proj.makeClone());
      paint(g);
    }
  }

  /**
   * Paints the layer.
   * @param g the Graphics context for painting
   */
  public void paint(Graphics g)
  {
    Projection projObj;
    if((projObj=getProjection()) == null ||
                                    projObj.getScale() <= MAX_PROJSCALE_VAL)
    {    //projection-zoom-scale is not too large; paint label and scale bars
      super.paint(g);

      /*-----------------------------------------------------------
      * draw scale bar
       */
      final int x = text.getX();
      final int y = text.getY() - textHeight;
      final int dx = ibarlength / 2;
      final int left = x - dx;
      final int right = left + ibarlength - 1;
      final int bottom = y - scaleHeight;
      final int top = bottom - ibarHeight;
      g.setColor(fgColor);
      g.drawRect(left, bottom, dx, ibarHeight);
      g.fillRect(x, bottom, dx+1, ibarHeight+1);
      g.drawLine(left, bottom, left, top - ibarHeight);
      g.drawLine(right, bottom, right, top - ibarHeight);
    }
  }

  /**
   * Position the text graphic.
   * @param w width
   * @param h height
   */
  protected synchronized void positionText(int w, int h)
  {
    int xoff = xpos + ibarlength / 2;
    int yoff = ypos;
    int justify = OMText.JUSTIFY_CENTER;
    if (xgrav.equals("-"))
    {
      xoff = w - xoff;
    }
    if (ygrav.equals("-"))
    {
      yoff = h - yoff;
    }
    else
    {
      yoff += textHeight + scaleHeight + ibarHeight*2;
    }
    text.setX(xoff);
    text.setY(yoff);
    text.setJustify(justify);
  }

  /**
   * Sets the properties for the <code>Layer</code>.
   * @param prefix the token to prefix the property names
   * @param props the <code>Properties</code> object
   */
  public void setProperties(String prefix, Properties props)
  {
    props.remove(prefix+labelProperty);
    super.setProperties(prefix, props);
    prefix = PropUtils.getScopedPropertyPrefix(prefix);
    setUseMetric(Boolean.valueOf(props.getProperty(prefix+UseMetricProperty, "true")).booleanValue());
  }

  /**
   * PropertyConsumer method, to fill in a Properties object,
   * reflecting the current values of the layer.  If the
   * layer has a propertyPrefix set, the property keys should
   * have that prefix plus a separating '.' prepended to each
   * propery key it uses for configuration.
   *
   * @param props a Properties object to load the PropertyConsumer
   * properties into.  If props equals null, then a new Properties
   * object should be created.
   * @return Properties object containing PropertyConsumer property
   * values.  If getList was not null, this should equal getList.
   * Otherwise, it should be the Properties object created by the
   * PropertyConsumer.
   */
  public Properties getProperties(Properties props)
  {
    props = super.getProperties(props);
    String prefix = PropUtils.getScopedPropertyPrefix(propertyPrefix);
    props.remove(prefix+labelProperty);
    props.setProperty(prefix+UseMetricProperty, String.valueOf(getUseMetric()));
    return props;

  }
  public Properties getPropertyInfo(Properties list)
  {
    list = super.getPropertyInfo(list);
    list.setProperty(UseMetricProperty, "Use metric units");
    String internString = i18n.get(IstiScaleLayer.class, UseMetricProperty, I18n.TOOLTIP, "Use kilometers units for scale.");
    list.put(UseMetricProperty, internString);
    internString = i18n.get(IstiScaleLayer.class, UseMetricProperty, "Kilometer units");
    list.put(UseMetricProperty + LabelEditorProperty, internString);
    list.put(UseMetricProperty + ScopedEditorProperty, "com.bbn.openmap.util.propertyEditor.YesNoPropertyEditor");
    list.put(PropertyConsumer.initPropertiesProperty, UseMetricProperty + " " + geometryProperty + " " + fgColorProperty);

    return list;
  }

    public java.awt.Component getGUI() {
        if (palette == null) {
            if (com.bbn.openmap.util.Debug.debugging("scale"))
                System.out.println(getClass().getName() + " : creating GUI Palette.");

            palette = Box.createVerticalBox();

            JPanel layerPanel = PaletteHelper.createPaletteJPanel(i18n.get(IstiScaleLayer.class, "scalePanel", "Click to Display"));
            JButton propsButton = new JButton(i18n.get(IstiScaleLayer.class, "setProperties", "Properties"));
            propsButton.setActionCommand(DisplayPropertiesCmd);
            propsButton.addActionListener(this);
            layerPanel.add(propsButton);
            palette.add(layerPanel);
        }

        return palette;
    }

}

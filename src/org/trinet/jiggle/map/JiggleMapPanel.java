package org.trinet.jiggle.map;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import com.bbn.openmap.*; 
import com.bbn.openmap.event.*; 
import com.bbn.openmap.gui.*;
import com.bbn.openmap.gui.menu.*;
import com.bbn.openmap.proj.*;
import com.bbn.openmap.util.Debug;


//import com.bbn.openmap.event.*; 

import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleMapBean;

public class JiggleMapPanel extends OMComponentPanel implements MapPanel {

    private MapHandler mapHandler;
    private MapBean mapBean;
    private PropertyHandler propertyHandler;
    private MenuList menuList;
    private Jiggle jiggle = null;

    /** Navigation mouse mode. */
    //public static final String regionMouseModeID = QWRegionMouseMode.modeID; // some user defined mode 
    public static final String navMouseModeID = NavMouseMode.modeID;
    public static final String defaultMouseModeID = navMouseModeID;

    public JiggleMapPanel(Jiggle jiggle, PropertyHandler propertyHandler) {
        this(jiggle, propertyHandler, false);
    }

    public JiggleMapPanel(Jiggle jiggle, PropertyHandler propertyHandler, boolean delayCreation) {
        this.jiggle = jiggle;
        setPropertyHandler(propertyHandler);
        if (! delayCreation)  create();
    }

    public MapBean getMapBean() {
        if (mapBean == null) {
          //setMapBean(BasicMapPanel.createMapBean()); // Test debug case
          setMapBean(createMapBean(jiggle));
        }
        return mapBean;
    }

    //MapBean Methods:
    //////////////////

    public MapBean createMapBean(Jiggle jiggle) {
        int envWidth = Environment.getInteger(Environment.Width, MapBean.DEFAULT_WIDTH);
        int envHeight = Environment.getInteger(Environment.Height, MapBean.DEFAULT_HEIGHT);

        if (envWidth <= 0 || envHeight <= 0) {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            if (envWidth <= 0) {
                envWidth = (int) d.getWidth();
            }
            if (envHeight <= 0) {
                envHeight = (int) d.getHeight();
            }
        }

        Projection proj = ProjectionFactory.getDefaultProjectionFromEnvironment();

        if (Debug.debugging("mappanel")) {
            Debug.output("MapPanel: creating MapBean with initial projection "
                    + proj);
        }

        return createMapBean(jiggle, proj, new BevelBorder(BevelBorder.LOWERED));
    }

    public MapBean createMapBean(Jiggle jiggle, Projection proj, Border border) {

        JiggleMapBean mapBean = JiggleMapBean.createMapBean(jiggle);
        if (mapBean != null) {
          mapBean.setBorder(border);
          mapBean.setProjection(proj);
          mapBean.setPreferredSize(new Dimension(proj.getWidth(), proj.getHeight()));
          mapBean.setLayers(getLayerHandler());
        }
        return mapBean;
    }

    public LayerHandler getLayerHandler() {
        //return (LayerHandler) getMapHandler().get(LayerHandler.class);
        Iterator i = getMapHandler().getAll(LayerHandler.class).iterator();
        return ( i.hasNext() ) ? (LayerHandler) i.next() : (LayerHandler) null;
        
    }

    public InformationDelegator getInfoDelegator() {
        return (InformationDelegator) getMapHandler().get(InformationDelegator.class);
    }

    public MouseDelegator getMouseDelegator() {
        return (MouseDelegator) getMapHandler().get(MouseDelegator.class);
    }

    public void setInformationDelegator(InformationDelegator id) {
        getMapHandler().add(id);
    }

    public void setLayerHandler(LayerHandler lh) {
        // SoloMapComponent
        MapHandler mph = getMapHandler();
        Object oldHandler = mph.get(LayerHandler.class);
        if (oldHandler != null) mph.remove(oldHandler);
        mph.add(lh);
    }

    public void setMouseDelegator(MouseDelegator md) {
        // SoloMapComponent
        MapHandler mph = getMapHandler();
        Object oldHandler = mph.get(MouseDelegator.class);
        if (oldHandler != null) mph.remove(oldHandler);
        mph.add(md);
    }

    public void setActiveMouseModeWithID(String modeID) {
        getMouseDelegator().setActiveMouseModeWithID( (modeID == null) ? defaultMouseModeID : modeID );
    }

    /**
     * The method that triggers setLayout() and createComponents() to
     * be called. If you've told the BasicMapPanel to delay creation,
     * you should call this method to trigger the PropertyHandler to
     * create components based on the contents of its properties.
     */
    public void create() {
        setLayout(createLayoutManager());
        createComponents();
    }

    /**
     * The constructor calls this method that sets the LayoutManager
     * for this MapPanel. It returns a BorderLayout by default, but
     * this method can be overridden to change how the MapPanel places
     * components. If you change what this method returns, you should
     * also change how components are added in the findAndInit()
     * method.
     */
    protected LayoutManager createLayoutManager() {
        return new BorderLayout();
    }

    /**
     * Position the map bean in this panel according to the layout
     * manger. Defaults to BorderLayout.CENTER.
     */
    protected void addMapBeanToPanel(MapBean map) {
        add(map, BorderLayout.CENTER);
    }

    /**
     * The constructor calls this method that creates the MapHandler
     * and MapBean, and then tells the PropertyHandler to create the
     * components described in its properties. This method calls
     * getMapHandler() and getMapBean(). If the PropertyHandler is not
     * null, it will be called to created components based on its
     * properties, and those components will be added to the
     * MapHandler in this MapPanel.
     */
    protected void createComponents() {
        PropertyHandler ph = getPropertyHandler();
        // MapBean do we set before or after creating components above?
        MapBean mb = getMapBean();

        MapHandler mh = getMapHandler();
        mh.add(this);
        ph.createComponents(mh);

        // setLayerHandler( new LayerHandler("openmap", ph.getProperties()) );
        // below delegators can instead be created from components list above
        //setInformationDelegator( new InformationDelegator() ); // ?? not sure we need this object
        //MouseDelegator md = new MouseDelegator();
        //md.setDefaultMouseModes();
        //setMouseDelegator(md);
        //final MapMouseMode[] modes = new MapMouseMode[] { new NavMouseMode(true) };
        //md.setMouseModes(modes);
        //setActiveMouseModeWithID(null);  //set the default mouse mode

        // Check MapHandler to see if a ProjectionFactory has been added.
        // If it hasn't, create one with the default ProjectionLoaders.
        // We might want to remove this at some point, but not having it here 
        // will catch some people by suprise when 4.6.1 comes out.
        Object obj = mh.get(com.bbn.openmap.proj.ProjectionFactory.class);
        if (obj == null) {
            Debug.message("mappanel",
                    "BasicMapPanel adding ProjectionFactory and projections to MapHandler since there are none to be found.");
            mh.add(ProjectionFactory.loadDefaultProjections());
        }

        // MapBean do we set before or after creating components above?
        //MapBean mb = getMapBean();
        // default Environment loaded after property handler file read.
        mb.setProjection(ProjectionFactory.getDefaultProjectionFromEnvironment());
        mb.setBckgrnd(Environment.getCustomBackgroundColor());
    }

    /**
     * Set the map bean used in this map panel, replace the map bean
     * in the MapHandler if there isn't already one, or if the policy
     * allows replacement. The MapHandler will be created if it
     * doesn't exist via a getMapHandler() method call.
     * 
     * @throws MultipleSoloMapComponentException if there is already a
     *         map bean in the map handler and the policy is to reject
     *         duplicates (since the MapBean is a SoloMapComponent).
     */
    public void setMapBean(MapBean bean) {
        if (bean == null && mapBean != null) {
            // remove the current MapBean from the application...
            getMapHandler().remove(mapBean);
        }

        mapBean = bean;

        if (mapBean != null) {
            getMapHandler().add(mapBean);
            addMapBeanToPanel(mapBean);
        }
    }

    /**
     * Get the PropertyHandler containing properties used to configure
     * the panel, creating it if it doesn't exist.
     */
    public PropertyHandler getPropertyHandler() {
        if (propertyHandler == null) {
            try {
               setPropertyHandler(new JiggleMapPropertyHandler(this.jiggle));
            }
            catch(MalformedURLException ex) {
                System.err.println(ex.getMessage());
            }
            catch(IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return propertyHandler;
    }

    /**
     * Set the PropertyHandler containing the properties used to
     * configure this panel. Adds the PropertyHandler to the
     * MapHandler. If the MapHandler isn't set at this point, it will
     * be created via a getMapHandler() call.
     */
    public void setPropertyHandler(PropertyHandler handler) {
        propertyHandler = handler;
        if (handler != null) {
            getMapHandler().add(handler);
        }
    }

    /**
     * MapPanel method. Get the MapHandler used for the MapPanel.
     * Creates a standard MapHandler if it hasn't been created yet.
     */
    public MapHandler getMapHandler() {
        if (mapHandler == null) {
            mapHandler = new MapHandler();
        }
        return mapHandler;
    }

    /**
     * MapPanel method. Get a JMenuBar containing menus created from
     * properties.
     */
    public JMenuBar getMapMenuBar() {
        if (menuList != null) {
            return menuList.getMenuBar();
        } else {
            return null;
        }
    }

    /**
     * MapPanel method. Get a JMenu containing sub-menus created from
     * properties.
     */
    public JMenu getMapMenu() {
        if (menuList != null) {
            return menuList.getMenu();
        } else {
            return null;
        }
    }

    //Map Component Methods:
    ////////////////////////

    /**
     * Adds a component to the map bean context. This makes the
     * <code>mapComponent</code> available to the map layers and
     * other components.
     * 
     * @param mapComponent a component to be added to the map bean
     *        context
     * @throws MultipleSoloMapComponentException if mapComponent is a
     *         SoloMapComponent and another instance already exists
     *         and the policy is a reject policy.
     */
    public void addMapComponent(Object mapComponent) {
        if (mapComponent != null) {
            getMapHandler().add(mapComponent);
        }
    }

    /**
     * Remove a component from the map bean context.
     * 
     * @param mapComponent a component to be removed to the map bean
     *        context
     * @return true if the mapComponent was removed.
     */
    public boolean removeMapComponent(Object mapComponent) {
        if (mapComponent != null) {
            return getMapHandler().remove(mapComponent);
        }
        return true;
    }

    /**
     * Given a Class, find the object in the MapHandler. If the class
     * is not a SoloMapComponent and there are more than one of them
     * in the MapHandler, you will get the first one found.
     */
    public Object getMapComponentByType(Class c) {
        return getMapHandler().get(c);
    }

    /**
     * Get all of the mapComponents that are of the given class type.
     */
    public Collection getMapComponentsByType(Class c) {
        return getMapHandler().getAll(c);
    }

    /**
     * Find the object with the given prefix by looking it up in the
     * prefix librarian in the MapHandler.
     */
    public Object getMapComponent(String prefix) {
        return getPropertyHandler().get(prefix);
    }

    /**
     * The BasicMapPanel looks for MapPanelChild components, finds out
     * from them where they prefer to be placed, and adds them.
     */
    public void findAndInit(Object someObj) {
        if (someObj instanceof MapPanelChild && someObj instanceof Component) {
            if (Debug.debugging("mappanel")) {
                Debug.output("MapPanel: adding " + someObj.getClass().getName());
            }
            /*
            if (someObj instanceof ToolPanel) {
                ToolPanel tp = (ToolPanel) someObj;
                JiggleButton jb = new JiggleButton();
                jb.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            if (jiggle != null) jiggle.toFront();
                        }
                    }
                );
                tp.add((Tool)jb);
            }
            */
            MapPanelChild mpc = (MapPanelChild) someObj;
            addMapPanelChild(mpc);
            invalidate();
        }

        if (someObj instanceof MenuList) {
            menuList = (MenuList) someObj;
        }
    }

    /**
     * Add a child to the MapPanel.
     */
    protected void addMapPanelChild(MapPanelChild mpc) {
        add((Component) mpc, mpc.getPreferredLocation());
    }

    /**
     * The MapPanel looks for MapPanelChild components and removes
     * them from iteself.
     */
    public void findAndUndo(Object someObj) {
        if (someObj instanceof MapPanelChild && someObj instanceof Component) {
            if (Debug.debugging("mappanel")) {
                Debug.output("MapPanel: removing "
                        + someObj.getClass().getName());
            }
            remove((Component) someObj);
            invalidate();
        }

        if (someObj instanceof MenuList && menuList == someObj) {
            menuList = null;
        }
    }

    //Property Functions:
    /////////////////////

    /**
     * Get the current properties.
     */
    public Properties getProperties() {
        return getPropertyHandler().getProperties();
    }

    /**
     * Remove an existing property if it exists.
     * 
     * @return true if a property was actually removed.
     */
    public boolean removeProperty(String property) {
        return getPropertyHandler().removeProperty(property);
    }

    /**
     * Add (or overwrite) a property to the current properties
     */
    public void addProperty(String property, String value) {
        getPropertyHandler().addProperty(property, value);
    }

    /**
     * Add in the properties from the given URL. Any existing
     * properties will be overwritten except for openmap.components,
     * openmap.layers and openmap.startUpLayers which will be
     * appended.
     */
    public void addProperties(URL urlToProperties) {
        getPropertyHandler().addProperties(urlToProperties);
    }

    /**
     * Add in the properties from the given source, which can be a
     * resorce, file or URL. Any existing properties will be
     * overwritten except for openmap.components, openmap.layers and
     * openmap.startUpLayers which will be appended.
     * 
     * @throws MalformedURLException if propFile doesn't resolve
     *         properly.
     */
    public void addProperties(String propFile)
            throws java.net.MalformedURLException {
        getPropertyHandler().addProperties(propFile);
    }

    /**
     * remove a marker from a space delimated set of properties.
     */
    public void removeMarker(String property, String marker) {
        getPropertyHandler().removeMarker(property, marker);
    }

    /**
     * Add in the properties from the given Properties object. Any
     * existing properties will be overwritten except for
     * openmap.components, openmap.layers and openmap.startUpLayers
     * which will be appended.
     */
    public void addProperties(Properties p) {
        getPropertyHandler().addProperties(p);
    }

    /**
     * Append the given property into the current properties
     */
    public void appendProperty(String property, Properties src) {
        getPropertyHandler().appendProperty(property, src);
    }

    /**
     * Append the given property into the current properties
     */
    public void appendProperty(String property, String value) {
        getPropertyHandler().appendProperty(property, value);
    }

    /**
     * Prepend the given property into the current properties
     */
    public void prependProperty(String property, Properties src) {
        getPropertyHandler().prependProperty(property, src);
    }

    /**
     * Prepend the given property into the current properties
     */
    public void prependProperty(String property, String value) {
        getPropertyHandler().prependProperty(property, value);
    }
}

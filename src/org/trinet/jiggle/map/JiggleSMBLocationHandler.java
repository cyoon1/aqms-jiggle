// Adapted from Isti SMBLocationHandler source extends class code to accomodate Jiggle needs
// Changed scale values from int to float so as to allow user to specify float "." in input properties
package org.trinet.jiggle.map;

import java.awt.Font;
import java.io.*;
import java.util.Vector;
import java.util.Iterator;
import java.util.Properties;
import java.net.URL;
import java.awt.Point;

import com.bbn.openmap.omGraphics.FontSizer;
import com.bbn.openmap.omGraphics.OMCircle;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.util.Debug;
import com.bbn.openmap.util.CSVTokenizer;
import com.bbn.openmap.util.quadtree.QuadTree;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.layer.util.LayerUtils;
import com.bbn.openmap.layer.location.*;
import com.bbn.openmap.layer.location.csv.CSVLocationHandler;
import com.bbn.openmap.layer.location.LocationLayer;
import com.bbn.openmap.proj.Projection;

/**
 * LocationHandler designed to let you put data on the map layer 
 * based on information from a QPager/CUBE symbol (".SMB")
 * file.  It's assumed that the each row in the file refers
 * to a certain location, and that location contains a name label, a
 * latitude and a longitude (both in decimal degrees).
 *
 * This file is an extension of the OpenMap "CSVLocationHandler.java" file.
 * It has been modified to allow leading and trailing spaces around the
 * numeric fields.  Each field must be separated by a comma.
 *
 * <P>The JiggleSMBLocationHandler gives you some basic functionality.  The
 * properties file lets you set defaults on whether to draw the
 * locations and the names by default.  For crowded layers, having all
 * the names displayed might cause a cluttering problem.  In gesture
 * mode, OpenMap will display the name of each location as the mouse
 * is passed over it.  Pressing the left mouse button over a location
 * brings up a popup menu that lets you show/hide the name label, and
 * also to display the entire row contents of the location SMB file in
 * a Browser window that OpenMap launches.
 *
 * <P>The locationFile property should contain a URL referring to the file.
 * This can take the form of file:/myfile.smb for a local file or
 * http://somehost.org/myfile.smb for a remote file.
 *
 * <P>In the openmap.properties file (for instance):<BR>
 * <pre>
 * # In the section for the LocationLayer:
 * locationLayer.locationHandlers=smblocationhandler
 *
 * smblocationhandler.class=com.isti.quakewatch.SMBLocationHandler
 * smblocationhandler.locationFile=ca_town_qw.smb
 * smblocationhandler.locationColor=FF0000
 * smblocationhandler.nameColor=008C54
 * smblocationhandler.showNames=false
 * smblocationhandler.showLocations=true
 * smblocationhandler.nameIndex=0
 * smblocationhandler.latIndex=8
 * smblocationhandler.lonIndex=10
 * # Optional property, if the eastern hemisphere longitudes are negative.
 * #  False by default.
 * smblocationhandler.eastIsNeg=false
 * </pre>
 * *
 * Note that, in this class, the values for 'nameIndex', 'latIndex' and
 * 'lonIndex' will default to the default values for QPager/CUBE symbol
 * (".SMB") files if not explictedly set among the properties.
 */
public class JiggleSMBLocationHandler extends CSVLocationHandler 
{
    /** Property name of value for zoom scale above which
     *   a "filtered" view of locations is presented. */
  public static final String viewFilteredScaleProperty = "viewFilteredScale";

    /** Property name of value for zoom scale above which
     *   no locations are presented. */
  public static final String viewNoneScaleProperty = "viewNoneScale";

    /** Property name of flag enabling filtering by all-upper-case-names. */
  public static final String viewUpperCaseFilterProperty = "viewUpperCaseFilter";

    /** Property name of max scale to label locations with their names. */
  public final static String viewNameScaleProperty = "viewNameScale";

    /** Property name of label font name string. */
  public static final String fontNameProperty = "fontName";

    /** Property name of label font name string. */
  public static final String fontStyleProperty = "fontStyle";

  public static final String SymbolNumberIndexProperty = "symbolNumber";
  public static final String SymbolSizeIndexProperty = "symbolSize";
  public static final String TextAlignIndexProperty = "textAlign";
  public static final String HorizOffsetIndexProperty = "horizOffset";
  public static final String VertOffsetIndexProperty = "vertOffset";

    //default field index values for symbol files:
  private static final int SMB_NAME_INDEX = 7;
  private static final int SMB_LAT_INDEX = 1;
  private static final int SMB_LON_INDEX = 0;

  /** Maximum symbol-size value allowed from properties. */
  public static final int MAX_SYMBOL_SIZE = 20;
  /** Default zoom scale above which a "filtered" view is presented. */
  public static final float DEFAULT_VIEW_FILTERED_SCALE = 10000000.f;
  /** Default zoom scale above which no locations are presented. */
  public static final float DEFAULT_VIEW_NONE_SCALE = 40000000.f;
  /** Default max scale above which locations are not labeled by default. */
  public static final float DEFAULT_VIEW_NAME_SCALE =  1000000.f;
  /** Default for filter-by-upper-case-names-enabled flag. */
  public static final boolean DEFAULT_FILTERBY_UCNAMES_FLAG = true;

  private static float staticViewFilteredScale = DEFAULT_VIEW_FILTERED_SCALE;
  private static float staticViewNoneScale = DEFAULT_VIEW_NONE_SCALE;
  private static float staticViewNameScale = DEFAULT_VIEW_NAME_SCALE;
  private static boolean staticFilterByUCNamesFlag = DEFAULT_FILTERBY_UCNAMES_FLAG;

  private float viewFilteredScale = DEFAULT_VIEW_FILTERED_SCALE;
  private float viewNoneScale = DEFAULT_VIEW_NONE_SCALE;
  private boolean filterByUCNamesFlag = DEFAULT_FILTERBY_UCNAMES_FLAG;
  private float viewNameScale = DEFAULT_VIEW_NAME_SCALE;

  private boolean dataLoadedFlag = false;   //true after data loaded
  private int symbolNumberIndex = 2;   //symbol-number field index
  private int symbolSizeIndex = 3;     //symbol-size field index
  private int textAlignIndex = 4;      //text-alignment field index
  private int horizOffsetIndex = 5;    //horizontal-offset field index
  private int vertOffsetIndex = 6;     //vertical-offset field index

  private SMBMinMaxInfo mmi = new SMBMinMaxInfo();

  private JiggleLocationMarker locationMarkerObj = new JiggleLocationMarker();

  public static final String DEFAULT_FONT_NAME = "Arial";
  public static final String DEFAULT_FONT_STYLE = "PLAIN";

  private String fontName = DEFAULT_FONT_NAME;
  private String fontStyle = DEFAULT_FONT_STYLE;
  private int iStyle = Font.PLAIN;
  private static int defaultFontSize = 10;

  private int minScaleFontSize = defaultFontSize;
  private int maxScaleFontSize = minScaleFontSize;

  /**
   * Overidden version that enters default field index values for symbol
   * files if none given in the properties and that handles the "ViewScale"
   * properties.
   * @param prefix string prefix used in the properties file for this layer.
   * @param properties the properties set in the properties file.
   */
  public void setProperties(String prefix,java.util.Properties properties) {
    setShowNames(true);                //change show-names default to true
    super.setProperties(prefix,properties);    //call parent function
    if(nameIndex < 0)                  //enter defaults for index values
      nameIndex = SMB_NAME_INDEX;      // as needed
    if(latIndex < 0)
      latIndex = SMB_LAT_INDEX;
    if(lonIndex < 0)
      lonIndex = SMB_LON_INDEX;

    prefix = PropUtils.getScopedPropertyPrefix(prefix);

    fontName = properties.getProperty(prefix+fontNameProperty, DEFAULT_FONT_NAME);
    fontStyle = properties.getProperty(prefix+fontStyleProperty, DEFAULT_FONT_STYLE);
    if (fontStyle.equalsIgnoreCase("PLAIN")) iStyle = Font.PLAIN;
    else if (fontStyle.equalsIgnoreCase("BOLD")) iStyle = Font.BOLD;
    else if (fontStyle.equalsIgnoreCase("ITALIC")) iStyle = Font.ITALIC;

    minScaleFontSize =
        PropUtils.intFromProperties(properties, prefix+"minFontSize", defaultFontSize);
    maxScaleFontSize =
        PropUtils.intFromProperties(properties, prefix+"maxFontSize", defaultFontSize);

    viewFilteredScale =
        PropUtils.floatFromProperties(properties, prefix+viewFilteredScaleProperty, staticViewFilteredScale);

    viewNoneScale = 
        PropUtils.floatFromProperties(properties, prefix+viewNoneScaleProperty, staticViewNoneScale);

    viewNameScale =
        PropUtils.floatFromProperties(properties, prefix+viewNameScaleProperty, staticViewNameScale);

    filterByUCNamesFlag =
        PropUtils.booleanFromProperties(properties, prefix+viewUpperCaseFilterProperty, staticFilterByUCNamesFlag);

    symbolNumberIndex =
        PropUtils.intFromProperties(properties, prefix + SymbolNumberIndexProperty, 2);  //symbol-number field index
    symbolSizeIndex =
        PropUtils.intFromProperties(properties, prefix + SymbolSizeIndexProperty, 3);  //symbol-size field index
    textAlignIndex =
        PropUtils.intFromProperties(properties, prefix + TextAlignIndexProperty, 4);  //text-alignment field index
    horizOffsetIndex =
        PropUtils.intFromProperties(properties, prefix + HorizOffsetIndexProperty, 5);  //horizontal-offset field index
    vertOffsetIndex =
        PropUtils.intFromProperties(properties, prefix + VertOffsetIndexProperty, 6);  //vertical-offset field index

    //Now set the location marker config from properties
    locationMarkerObj.setProperties(prefix, properties);
  }

  /**
   * Overridded version that handles the "ViewScale" properties.
   * @param props properties object to use.
   * @return The properties object.
   */
  public Properties getProperties(Properties props) {
    props = super.getProperties(props);
    String prefix = PropUtils.getScopedPropertyPrefix(this);

    props.put(prefix+fontNameProperty, fontName);
    props.put(prefix+fontStyleProperty, fontStyle);

    props.put(prefix+viewUpperCaseFilterProperty, (new Boolean(filterByUCNamesFlag)).toString());

    if(viewFilteredScale >= 0.f) 
      props.put(prefix+viewFilteredScaleProperty, Float.toString(viewFilteredScale));

    if(viewNoneScale >= 0.f) 
      props.put(prefix+viewNoneScaleProperty, Float.toString(viewNoneScale));

    if(viewNameScale >= 0.f) 
      props.put(prefix+viewNameScaleProperty, Float.toString(viewNameScale));

    props.put(prefix + SymbolNumberIndexProperty, symbolNumberIndex);
    props.put(prefix + SymbolSizeIndexProperty, symbolSizeIndex);
    props.put(prefix + TextAlignIndexProperty, textAlignIndex);
    props.put(prefix + HorizOffsetIndexProperty, horizOffsetIndex);
    props.put(prefix + VertOffsetIndexProperty, vertOffsetIndex);

    //Add the location marker properties
    locationMarkerObj.getProperties(prefix, props);

    return props;
  }

  /**
   * Overridden version that handles the "ViewScale" properties.
   * @param list properties object to use.
   * @return The properties object.
   */
  public Properties getPropertyInfo(Properties list) {
    list = super.getPropertyInfo(list);

    list.put(fontNameProperty, "Location label font name");
    list.put(fontStyleProperty, "Location label font style (PLAIN, BOLD, ITALIC)");

    list.put(viewUpperCaseFilterProperty,"Enable filter to show only locations with uppercase names.");
    list.put(viewFilteredScaleProperty,"Max zoom scale showing a 'filtered' view of locations.");
    list.put(viewNoneScaleProperty, "Max zoom scale showing any location.");
    list.put(viewNameScaleProperty, "Max zoom scale showing all location labels by default.");

    locationMarkerObj.getPropertyInfo(list);

    return list;
  }

  /**
   * Enters default view-scale values to be used if they are not specified
   * in the properties.  These values are used to reduce screen clutter
   * when the view is zoomed out.  This method (if used) must be called
   * before the location layers are loaded.
   * @param viewFilteredVal max zoom scale to show "unfiltered" locations.
   * @param viewNoneVal max zoom scale to show any location.
   * @param viewNameVal max zoom scale to show location labels by default.
   */
  public static void setDefaultViewScaleValues(float viewFilteredVal, float viewNoneVal,  float viewNameVal) {
    staticViewFilteredScale = viewFilteredVal;
    staticViewNoneScale = viewNoneVal;
    staticViewNameScale = viewNameVal;
  }

  /**
   * Enters a default value for the flag that enables a filtered
   * view where only all-upper-case names are displayed.  This method
   * (if used) must be called before the location layers are loaded.
   * @param flgVal true to enable a filtered view where only
   * all-upper-case names are displayed.
   */
  public static void setDefaultFilterByUCNamesFlag(boolean flgVal) {
    staticFilterByUCNamesFlag = flgVal;
  }

  /**
   * Prepares the graphics for the layer.  This is where the
   * getRectangle() method call is made on the location.  This
   * overridden version is modified to avoid always displaying a
   * debug message.  <p>
   * Occasionally it is necessary to abort a prepare call.  When
   * this happens, the map will set the cancel bit in the
   * LayerThread, (the thread that is running the prepare).  If this
   * Layer needs to do any cleanups during the abort, it should do
   * so, but return out of the prepare asap.
   * @param nwLat north-west latitude
   * @param nwLon north-west longitude
   * @param seLat south-east latitude
   * @param seLon south-east longitude
   * @param graphicList vector of graphics objects
   * @return updated vector of graphics objects
   * @throws RuntimeException if error creating data.
   */
  public Vector get(float nwLat, float nwLon, float seLat, float seLon, Vector graphicList) {
    if(!dataLoadedFlag)
    {    //quadtree not yet setup; do it now
      if(Debug.debugging("csvlocation"))
      {        //only show this if debugging enabled
        Debug.output("SMBLocationHandler: Figuring out the locations " +
                     "and names (This is a one-time operation)");
      }
      quadtree = createData();
      dataLoadedFlag = true;      //indicate data loaded (even if failed)
    }
    if(quadtree != null)
    {
      if(Debug.debugging("csvlocation"))
      {
        Debug.output("SMBLocationHandler|CSVLocationHandler.get() ul.lon = "
                     + nwLon + " lr.lon = " + seLon +
                     " delta = " + (seLon - nwLon));
      }
         //figure out if "all", "filtered" (by symbol # or upper-case names)
         // or no view of locations should be presented:
      final LocationLayer layerObj;
      final Projection projObj;
      if((layerObj=getLayer()) != null &&
                               (projObj=layerObj.getProjection()) != null)
      {  //location layer and projection object fetched OK
        final float projScaleVal = projObj.getScale();
        if(projScaleVal >= viewFilteredScale)
        {   //zoom scale is large enough to be filtered
          if(projScaleVal < viewNoneScale)
          { //zoom scale is small enough for "filtered" view; get locations
            Object obj;
            if(mmi.maxSymbolNumberVal > mmi.minSymbolNumberVal)
            {      //symbol-number values for layer not all same
              quadtree.get(nwLat, nwLon, seLat, seLon, graphicList);
                        //get iteration of location objects (need to
                        // use clone to prevent threading exception):
              final Iterator iter = ((Vector)graphicList.clone()).iterator();
                   //remove all locations with symbol # less than max value:
              while(iter.hasNext())
              {    //for each location
                if((obj=iter.next()) instanceof GeneralMapLocation &&
                                  ((GeneralMapLocation)obj).getSymbolNumber() <
                                                     mmi.maxSymbolNumberVal)
                {       //object type OK and not max symbol number
                  graphicList.remove(obj);     //remove location from list
                }
              }
            }
            else if(filterByUCNamesFlag && mmi.upperCaseFlag)
            {      //filter-by-UC enabled and some names are all-upper-case
              quadtree.get(nwLat, nwLon, seLat, seLon, graphicList);
                        //get iteration of location objects (need to
                        // use clone to prevent threading exception):
              final Iterator iter = ((Vector)graphicList.clone()).iterator();
              String nameStr;
                   //remove locations with names that are not all uppercase:
              while(iter.hasNext())
              {    //for each location
                if((obj=iter.next()) instanceof GeneralMapLocation)
                {       //object type OK
                  nameStr = ((GeneralMapLocation)obj).getName();
                  if(nameStr == null || !isAllUpperCase(nameStr))
                    graphicList.remove(obj);     //remove location from list
                }
              }
            }
          }
              //(if zoom scale large enough for no-location view or
              // if unable to filter locations then return empty Vector)
          return graphicList;          //return Vector of locations
        }
      }

         //if no filtering then generate full locations list:
      quadtree.get(nwLat, nwLon, seLat, seLon, graphicList);
    }
    return graphicList;
  }

  /**
   * Parses the symbol file and creates the QuadTree holding all the
   * Locations.  Modified to allow leading and trailing spaces around
   * numeric fields.
   * @return location data
   * @throws RuntimeException if error creating data.
   */
  protected QuadTree createData() {
    SMBLocationInfo li = new SMBLocationInfo(
        locationFile,eastIsNeg,
        lonIndex,latIndex,symbolNumberIndex,symbolSizeIndex,textAlignIndex,
        horizOffsetIndex,vertOffsetIndex,nameIndex);
    return loadData(li, mmi, this);
  }

  /**
   * Parses the symbol file and creates the QuadTree holding all the
   * Locations.  Modified to allow leading and trailing spaces around
   * numeric fields.
   * @param li location info
   * @param mmi min max info
   * @param lhan location handler
   * @return location data
   * @throws RuntimeException if error loading data.
   */
  protected QuadTree loadData(SMBLocationInfo li, SMBMinMaxInfo mmi, JiggleSMBLocationHandler lhan) {
    final QuadTree qt =
        new QuadTree(90.0f, -180.0f, -90.0f, 180.0f, 100, 50f);

    if(li.latIndex == -1 || li.lonIndex == -1)
    {
      throw new RuntimeException(
          "SMBLocationHandler: loadData(): Index properties for " +
          "Lat/Lon/Name are not set properly, lat index:" + li.latIndex +
          ", lon index:" + li.lonIndex);
    }
    BufferedReader streamReader = null;
    Object token = null;

    if (Debug.debugging("csvlocation"))
       System.out.println("DEBUG JiggleSMBLocationHandler opening SMB location file \"" + li.locationFile + "\"");

    // Redid logic, if not absolute, look for resource file in directory where openmap/jiggle jars are located -aww 2010/08/27
    URL csvURL = null;
    try {
        csvURL = PropUtils.getResourceOrFileOrURL(null, li.locationFile);
        streamReader = new BufferedReader(new InputStreamReader(csvURL.openStream()));
    }
    catch (IOException ex) {
        System.err.println("Error JiggleSMBLocationHandler exception opening SMB location file: " + ex.getMessage());
        System.out.println("DEBUG JiggleSMBLocationHandler opening SMB location file \"" + csvURL + "\"");
        return null;
    }
    final CSVTokenizer tokenizerObj = new CSVTokenizer(streamReader, true);

    mmi.minLatVal = Float.MAX_VALUE;     //init var to track minimum latitude
    mmi.maxLatVal = -Float.MAX_VALUE;    //init var to track maximum latitude
    mmi.minLonVal = Float.MAX_VALUE;     //init var to track minimum longitude
    mmi.maxLonVal = -Float.MAX_VALUE;    //init var to track maximum longitude
    mmi.minSymbolNumberVal = Integer.MAX_VALUE;    //var to track min symbol #
    mmi.maxSymbolNumberVal = Integer.MIN_VALUE;    //var to track max symbol #
                      //flag set true if some all-upper-case names found:
    mmi.upperCaseFlag = false;

    String name = "", prevName = "(unknown)";
    float lat = 0;
    float lon = 0;
    int symbolNumberVal = 1;              //symbol number from file
    boolean symbolNumberFlag = false;     //true if symbol number found
    //location marker
    final JiggleLocationMarker markerObj;
    int horizOffsetVal = 0, vertOffsetVal = 0;
    int val,len,justVal,vAlignVal;
    Integer intObj;
    String str;
    OMText textObj;
    GeneralMapLocation locObj = null;
            //flag set true if any all-upper-case names found:
    boolean upperCaseNameFlag = false;
            //flag set true if any not-all-upper-case names found:
    boolean notUpperCaseNameFlag = false;

    if (lhan != null)  //if handler was specified
    {
      //use handler marker as default
      markerObj = (JiggleLocationMarker)lhan.locationMarkerObj.clone();
    }
    else
    {
      //use default marker
      markerObj = new JiggleLocationMarker();
    }

    token = tokenizerObj.token();

    Debug.message("csvlocation", "SMBLocationHandler: Reading File:"
                  + li.locationFile
                  + " NameIndex: " + li.nameIndex
                  + " latIndex: " + li.latIndex
                  + " lonIndex: " + li.lonIndex
                  + " eastIsNeg: " + li.eastIsNeg);

    while (!tokenizerObj.isEOF(token))
    {
      int i = 0;

      Debug.message("csvlocation", "SMBLocationHandler| Starting a line");

      name = "";                          //initialize name string
      lat = lon = 0.0f;                   //initialize values
      symbolNumberFlag = false;           //initialize "found" flag
      horizOffsetVal = vertOffsetVal = 0; //initialize label offsets
      justVal = OMText.JUSTIFY_LEFT;      //reset to default alignment
      vAlignVal = GeneralMapLocation.VALIGN_MIDDLE;
      while (!tokenizerObj.isNewline(token) && !tokenizerObj.isEOF(token))
      {
        try
        {
          if(token instanceof String && (len=(str=(String)token).length()) > 0)
          {   //token is a String that contains data
            if(i == li.nameIndex)
              name = (String)token;      //set label text
            else if(i == li.latIndex)       //set latitude for symbol
              lat = strToDouble(token).floatValue();
            else if(i == li.lonIndex)
            {   //set latitude for symbol
              lon = strToDouble(token).floatValue();
              if(li.eastIsNeg)         //if flag then
                lon *= -1;          //invert sign
            }
            else if(i == li.symbolNumberIndex)
            {      //set symbol number
              symbolNumberVal = strToInteger(token).intValue();
              symbolNumberFlag = true;      //indicate value found
            }
            else if(i == li.symbolSizeIndex)   //set symbol size on map
            {
              if((intObj=strToInteger(token)) != null &&
                      (val=intObj.intValue()) > 0 && val <= MAX_SYMBOL_SIZE)
              {    //value parsed OK and size is OK
                markerObj.setMarkerSize(val);
              }
            }
            else if(i == li.textAlignIndex)
            {   //setup text alignment for symbol vs label text
              str = (String)token;          //set handle to string
              if((len=str.length()) > 2)    //get length
                len = 2;       //don't process more then 2 characters
              parseAlignLoop:
              for(int cIdx=0; cIdx<len; ++cIdx)
              {      //for each character in alignment code
                switch(str.charAt(cIdx))
                {
                  case 'L':
                    justVal = OMText.JUSTIFY_LEFT;
                    break;
                  case 'R':
                    justVal = OMText.JUSTIFY_RIGHT;
                    break;
                  case 'C':
                    justVal = OMText.JUSTIFY_CENTER;
                    break;
                  case 'T':
                    vAlignVal = GeneralMapLocation.VALIGN_TOP;
                    break;
                  case 'M':
                    vAlignVal = GeneralMapLocation.VALIGN_MIDDLE;
                    break;
                  case 'B':
                    vAlignVal = GeneralMapLocation.VALIGN_BOTTOM;
                    break;
                  case 'U':
                    vAlignVal = GeneralMapLocation.VALIGN_UNDERLINE;
                    break;
                  default:
                    Debug.error("Unrecognized text-alignment code (\"" +
                                             str + "\") in symbol file \"" +
                                                    li.locationFile + "\"");
                    break parseAlignLoop;
                }
              }
            }
            else if(i == li.horizOffsetIndex)    //set horizontal offset value
              horizOffsetVal = strToInteger(token).intValue();
            else if(i == li.vertOffsetIndex)     //set vertical offset value
              vertOffsetVal = strToInteger(token).intValue();
          }
          ++i;          //increment token count
          token = tokenizerObj.token();
          // For some reason, the check above doesn't always
          // work
//            if(tokenizerObj.isEOF(token)) break;
        }
        catch(Exception ex)
        {     //exception error during parsing; throw error message
          throw new RuntimeException("Error parsing SMB location file \"" +
                  li.locationFile + "\" after name \"" + prevName + "\"");
        }
      }
      prevName = name;          //save name for next time

      if(tokenizerObj.isEOF(token))
        break;        //if end-of-file then exit outer loop
      token = tokenizerObj.token();       //skip past newline token

      if(i > 0 && (!symbolNumberFlag || symbolNumberVal != 0) &&
                                            markerObj.getMarkerSize() > 0)
      {     //tokens found and neither symbol number or symbol size is 0
        locObj = new GeneralMapLocation(
            lat, lon, name.replace('_',' '),
            markerObj.createMarkerGraphic(lat, lon),
            justVal, vAlignVal);
        //if any non-zero label text offsets then enter them:
        if(horizOffsetVal != 0 || vertOffsetVal != 0)
          locObj.setLabelOffsets(horizOffsetVal,vertOffsetVal);
        if(lat < mmi.minLatVal)     //track min/max lat/lon
          mmi.minLatVal = lat;
        if(lat > mmi.maxLatVal)
          mmi.maxLatVal = lat;
        if(lon < mmi.minLonVal)
          mmi.minLonVal = lon;
        if(lon > mmi.maxLonVal)
          mmi.maxLonVal = lon;
        if(symbolNumberFlag)
        {   //value for symbol number was found
          locObj.setSymbolNumber(symbolNumberVal);  //enter value
          if(symbolNumberVal < mmi.minSymbolNumberVal)  //if val < min then
            mmi.minSymbolNumberVal = symbolNumberVal;   //set new min value
          if(symbolNumberVal > mmi.maxSymbolNumberVal)  //if val > max then
            mmi.maxSymbolNumberVal = symbolNumberVal;   //set new max value
        }

        // let the layer default handle these initially...
        locObj.setShowName(isShowNames());
        locObj.setShowLocation(isShowLocations());

        if (lhan != null)
        {
          locObj.setLocationHandler(lhan);
          locObj.setLocationPaint(lhan.getLocationColor());
          if (! locationMarkerObj.getMarkerType().equals(JiggleLocationMarker.textMarkerType))
              locObj.setLocationFillPaint(lhan.getLocationColor());
          locObj.getLabel().setLinePaint(lhan.getNameColor());
          // Test here - aww
          //locObj.getLabel().setFont(new Font("Arial",Font.PLAIN, 8)); 
          if (minScaleFontSize == maxScaleFontSize)
              locObj.getLabel().setFont(new Font(fontName, iStyle, minScaleFontSize)); 
          else {
            locObj.getLabel().setFontSizer(
              new FontSizer(
                  new Font(fontName, iStyle, defaultFontSize),
                  1000000.f,
                  1,
                  minScaleFontSize,
                  maxScaleFontSize
              )
            );
          }

          if (getLayer() != null) {
             Projection projObj = getLayer().getProjection(); 
             if (projObj != null) locObj.setShowName( (projObj.getScale() <= viewNameScale) );
          }
          else locObj.setShowName(false);
        }
        locObj.setDetails(name + " is at lat: " + lat + ", lon: " + lon);

            //set 'mmi.upperCaseFlag' after not-all-upper-case
            // and all-upper-case names have been found:
        if(!mmi.upperCaseFlag)
        {   //flag not yet set
          if(!notUpperCaseNameFlag && !isAllUpperCase(name))
          {      //first not-all-upper-case name found
            notUpperCaseNameFlag = true;
            if(upperCaseNameFlag)         //if all-upper-case previously
              mmi.upperCaseFlag = true;   // then set flag
          }
          else if(!upperCaseNameFlag && isAllUpperCase(name))
          {      //first all-upper-case name found
            upperCaseNameFlag = true;
            if(notUpperCaseNameFlag)      //if not-all-upper-case
              mmi.upperCaseFlag = true;   // previously then set flag
          }
        }

        qt.put(lat, lon, locObj);
      }
    }

    Debug.message("csvlocation",
                  "SMBLocationHandler | Finished File:" + li.locationFile);
    try
    {
      if(streamReader != null)
        streamReader.close();
    }
    catch(java.io.IOException ioe)
    {
      new com.bbn.openmap.util.HandleError(ioe);
    }
    //put in zero for any min/max lat/lon values not entered:
    if(mmi.minLatVal == Float.MAX_VALUE)
      mmi.minLatVal = 0.0f;
    if(mmi.maxLatVal == -Float.MAX_VALUE)
      mmi.maxLatVal = 0.0f;
    if(mmi.minLonVal == Float.MAX_VALUE)
      mmi.minLonVal = 0.0f;
    if(mmi.maxLonVal == -Float.MAX_VALUE)
      mmi.maxLonVal = 0.0f;
    if(mmi.minSymbolNumberVal == Integer.MAX_VALUE)  //if no min entered then
      mmi.minSymbolNumberVal = 0;                    //set to zero
    if(mmi.maxSymbolNumberVal == Integer.MIN_VALUE)  //if no max entered then
      mmi.maxSymbolNumberVal = 0;                    //set to zero
    return qt;
  }

  //Convert Object (actually a String) to Double, return null if error.
  private static Double strToDouble(Object obj) {
    try
    {
      return new Double(((String)obj).trim());
    }
    catch(Exception ex)
    {
      return null;
    }
  }

  //Convert Object (actually a String) to Integer, return null if error.
  private static Integer strToInteger(Object obj) {
    try
    {
      return new Integer(((String)obj).trim());
    }
    catch(Exception ex)
    {
      return null;
    }
  }

  //Returns true if alphabetic characters in string are upper case.
  private static boolean isAllUpperCase(String str) {
    if(str == null)
      return false;
    final int len = str.length();
    char ch;
    for(int p=0; p<len; ++p)
    {    //for each character in string
      if(Character.isLetter(ch=str.charAt(p)) && !Character.isUpperCase(ch))
      {  //is letter and is not upper-case
        return false;
      }
    }
    return true;
  }


  /**
   * SMB Location information.
   */
  public static class SMBLocationInfo
  {
    protected final String locationFile;     //location file
    protected final boolean eastIsNeg;       //true if east is negative
    protected final int lonIndex;            //longitude field index
    protected final int latIndex;            //latitude field index
    protected final int symbolNumberIndex;   //symbol-number field index
    protected final int symbolSizeIndex;     //symbol-size field index
    protected final int textAlignIndex;      //text-alignment field index
    protected final int horizOffsetIndex;    //horizontal-offset field index
    protected final int vertOffsetIndex;     //vertical-offset field index
    protected final int nameIndex;           //name field index

    /**
     * Creates SMB location info.
     * @param locationFile location file
     * @param eastIsNeg true if east is negative
     * @param lonIndex longitude field index
     * @param latIndex latitude field index
     * @param symbolNumberIndex symbol-number field index
     * @param symbolSizeIndex symbol-size field index
     * @param textAlignIndex text-alignment field index
     * @param horizOffsetIndex horizontal-offset field index
     * @param vertOffsetIndex vertical-offset field index
     * @param nameIndex name field index
     */
    public SMBLocationInfo(String locationFile,boolean eastIsNeg,
        int lonIndex,int latIndex,int symbolNumberIndex,int symbolSizeIndex,
        int textAlignIndex,int horizOffsetIndex,int vertOffsetIndex,int nameIndex)
    {
      this.locationFile = locationFile;
      this.eastIsNeg = eastIsNeg;
      this.lonIndex = lonIndex;
      this.latIndex = latIndex;
      this.symbolNumberIndex = symbolNumberIndex;
      this.symbolSizeIndex = symbolSizeIndex;
      this.textAlignIndex = textAlignIndex;
      this.horizOffsetIndex = horizOffsetIndex;
      this.vertOffsetIndex = vertOffsetIndex;
      this.nameIndex = nameIndex;
    }

    /**
     * Creates SMB location info, assumes default input data column index mapping. 
     * Default mapping is: lon,lat,symbolId,symbolSize,alignmentCode,labelHorizOff,labelVertOffset,name.
     * Default is W longitude is negative.
     * @param locationFile location file
     */
    public SMBLocationInfo(String locationFile)
    {
      this.locationFile = locationFile;
      this.eastIsNeg = false;
      this.lonIndex = 0;
      this.latIndex = 1;
      this.symbolNumberIndex = 2;
      this.symbolSizeIndex = 3;
      this.textAlignIndex = 4;
      this.horizOffsetIndex = 5;
      this.vertOffsetIndex = 6;
      this.nameIndex = 7;
    }
  }


  /**
   * Class SMBMinMaxInfo holds input data min,max bounds for the location layer.
   */
  public static class SMBMinMaxInfo
  {
    protected float minLatVal = 0.0f;      //variable to track minimum latitude
    protected float maxLatVal = 0.0f;      //variable to track maximum latitude
    protected float minLonVal = 0.0f;      //variable to track minimum longitude
    protected float maxLonVal = 0.0f;      //variable to track maximum longitude
    protected int minSymbolNumberVal = 0;  //var to track min symbol number
    protected int maxSymbolNumberVal = 0;  //var to track max symbol number
                        //flag set true if some all-upper-case names found:
    protected boolean upperCaseFlag = false;
  }

}

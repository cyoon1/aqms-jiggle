package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.AbstractWaveform;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDbReader;
import org.trinet.jasi.EnvironmentInfo;

/**
 * Panel for insertion into dialog box for setting miscellaneous properties.
 */
public class DPMiscProps extends AbstractPreferencesPanel {

    public DPMiscProps(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        DecimalFormat df1 = new DecimalFormat ("###0");
        DecimalFormat df2 = new DecimalFormat ( "#0.00" );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;

//Region auth ????
        jcb = makeUpdateCheckBox("Set trigger/clone authority by region", "Event auth is set to name of containing region polygon", "authRegionEnabled");
        this.add(jcb);

        // May not need this for existing db events, see above:
        //jcb = makeUpdateCheckBox("Set all events authority by region", "Event auth is set to name of containing region polygon", "resetAllEtypeAuth");
        //this.add(jcb);

// network code
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Local database seismic network (default auth)");
        jlbl.setToolTipText("Set default FDSN 2-char network code.");
        hbox.add(jlbl);

        jtf = new JTextField(newProps.getProperty("localNetCode",""));
        jtf.getDocument().addDocumentListener(new MiscDocListener("localNetCode", jtf));
        jtf.setToolTipText("Set default FDSN 2-char network code.");
        hbox.add(jtf);
        this.add(hbox);

// auth net codes for region/local event type test
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Auth networks codes ");
        jlbl.setToolTipText("Set list of 2-char network region codes to test for event type local vs. regional on commit.");
        hbox.add(jlbl);

        jtf = new JTextField(6);
        jtf.setText(newProps.getProperty("authNetCodes",""));
        jtf.getDocument().addDocumentListener(new MiscDocListener("authNetCodes", jtf));
        jtf.setToolTipText("Set list of 2-char network region codes to test for event type local vs. regional on commit.");
        hbox.add(jtf);
        this.add(hbox);

//solNextProcStateMask = "AHIFC"
        jlbl = new JLabel("Next event processing state mask ");
        jlbl.setToolTipText("Any combo of the letters A,H,I,F, or C, usually AHIFC (for all) or just A");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);

        String mask = newProps.getProperty("solNextProcStateMask","AHIFC");
        final JCheckBox jcbA = new JCheckBox("A");
        jcbA.setToolTipText("Automatic");
        jcbA.setSelected(mask.indexOf("A") >= 0);
        hbox.add(jcbA);
        final JCheckBox jcbH = new JCheckBox("H");
        jcbH.setToolTipText("Human Reviewed");
        jcbH.setSelected(mask.indexOf("H") >= 0);
        hbox.add(jcbH);
        final JCheckBox jcbI = new JCheckBox("I");
        jcbI.setToolTipText("Intermediate or incomplete");
        jcbI.setSelected(mask.indexOf("I") >= 0);
        hbox.add(jcbI);
        final JCheckBox jcbF = new JCheckBox("F");
        jcbF.setSelected(mask.indexOf("F") >= 0);
        jcbF.setToolTipText("Finalized");
        hbox.add(jcbF);
        final JCheckBox jcbC = new JCheckBox("C");
        jcbC.setSelected(mask.indexOf("C") >= 0);
        jcbC.setToolTipText("Cancelled");
        hbox.add(jcbC);

        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                StringBuffer sb = new StringBuffer(4);
                if (jcbA.isSelected()) sb.append("A"); 
                if (jcbH.isSelected()) sb.append("H"); 
                if (jcbI.isSelected()) sb.append("I"); 
                if (jcbF.isSelected()) sb.append("F"); 
                if (jcbC.isSelected()) sb.append("C"); 
                if (sb.length() == 0) {
                    jcbA.setSelected(true);
                    jcbH.setSelected(true);
                    jcbI.setSelected(true);
                    jcbF.setSelected(true);
                    jcbC.setSelected(true);
                    sb.append("AHIFC");
                }
                newProps.setProperty("solNextProcStateMask", sb.toString()); 
            }
        };

        jcbA.addActionListener(al);
        jcbH.addActionListener(al);
        jcbI.addActionListener(al);
        jcbF.addActionListener(al);
        jcbC.addActionListener(al);

        this.add(hbox);

// Environment jasiUserName 
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Credit/alias id ");
        jlbl.setToolTipText("User id (or alias in db table CREDIT_ALIAS) to be associated with any new preferred origin or magnitude.");
        hbox.add(jlbl);

        String user = EnvironmentInfo.getUsername();
        if (user == null || user.equals("")) user = newProps.getProperty("jasiUserName", "");
        jtf = new JTextField(user);
        jtf.setPreferredSize(new Dimension(80, 16));
        jtf.getDocument().addDocumentListener(new MiscDocListener("jasiUserName", jtf));
        jtf.setToolTipText("User id (or alias in db table CREDIT_ALIAS) to be associated with any new preferred origin or magnitude.");
        hbox.add(jtf);
        this.add(hbox);

//Default mag type to load for preferences dialog DPmagMethod DPmagEngine properties panels
        String [] magTypes = newProps.getMagMethodTypes();
        if (magTypes.length > 0) {
          hbox = Box.createHorizontalBox();
          jlbl = new JLabel("Magnitude type by default to select in preferences dialog ");
          jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);
          hbox.add(jlbl);
          JComboBox defaultPrefMagTypeList = new JComboBox(magTypes);
          defaultPrefMagTypeList.setEditable(false);
          defaultPrefMagTypeList.setMaximumRowCount(1);
          defaultPrefMagTypeList.setSelectedItem(newProps.getProperty("defaultPreferencesMagMethodType", "ml"));
          defaultPrefMagTypeList.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent evt) {
                 newProps.setProperty("defaultPreferencesMagMethodType", (String)((JComboBox)evt.getSource()).getSelectedItem());
             }
          });
          hbox.add(defaultPrefMagTypeList);
          this.add(hbox);
        }

//PCS state postings ????
        jcb = makeUpdateCheckBox("PCS posting enabled", "Enables Event menubar item for posting loaded evid", "pcsPostingEnabled");
        this.add(jcb);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Posting State ");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("pcsPostingState", ""));
        jtf.getDocument().addDocumentListener(new UpdateDocListener("pcsPostingState",jtf));
        jtf.setMaximumSize(new Dimension(250,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

        this.add(Box.createVerticalStrut(10));

//Catalog load from file????
        jcb = makeUpdateCheckBox("Load event catalog from file", "Reads evid for catalog query from an input file (default catalog_evids.txt)",
                "catalog.loadFromFile");
        this.add(jcb);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Catalog evid file ");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("catalog.evidfile", "catalog_evids.txt"));
        jtf.getDocument().addDocumentListener(new UpdateDocListener("catalog.evidfile",jtf));
        jtf.setMaximumSize(new Dimension(250,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Catalog STATE to load ");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("catalog.pcsState", "Jiggle"));
        jtf.getDocument().addDocumentListener(new UpdateDocListener("catalog.pcsState",jtf));
        jtf.setMaximumSize(new Dimension(250,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(hbox);

        this.add(Box.createVerticalStrut(10));

// strip value panel
        JPanel stripPanel = new JPanel();
        stripPanel.setBorder(BorderFactory.createTitledBorder("Data Reading Strip Values"));
        vbox = Box.createVerticalBox();

// arrival strip value        
        jtf = new JTextField(df2.format(newProps.getFloat("pickStripValue")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("pickStripValue", jtf));
        jtf.setToolTipText("Delete if residual > this value");
        jlbl = new JLabel ("Strip pick traveltime residual (s) > ");
        jlbl.setToolTipText("Delete if residual > this value");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        hbox.add(jtf);

        
        jtf = new JTextField(df2.format(newProps.getFloat("pickStripDistance")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("pickStripDistance", jtf));
        jtf.setToolTipText("Delete if range > this value");
        jlbl = new JLabel (" or strip distance (km) > ");
        jlbl.setToolTipText("Delete if range > this value");
        hbox.add(jlbl);
        hbox.add(jtf);
        vbox.add(hbox);

// coda strip value
        jtf = new JTextField(df2.format(newProps.getFloat("codaStripValue")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("codaStripValue", jtf));
        jtf.setToolTipText("Delete if residual > this value");
        jlbl = new JLabel ("Strip coda magnitude residual (s) > ");
        jlbl.setToolTipText("Delete if residual > this value");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        hbox.add(jtf);
        
        jtf = new JTextField(df2.format(newProps.getFloat("codaStripDistance")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("codaStripDistance", jtf));
        jtf.setToolTipText("Delete if range > this value");
        jlbl = new JLabel (" or strip distance (km) > ");
        jlbl.setToolTipText("Delete if range > this value");
        hbox.add(jlbl);
        hbox.add(jtf);
        vbox.add(hbox);

// amp strip value
        jtf = new JTextField(df2.format(newProps.getFloat("ampStripValue")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("ampStripValue", jtf));
        jtf.setToolTipText("Delete if residual > value");
        jlbl = new JLabel ("Strip  amp magnitude residual (s) > ");
        jlbl.setToolTipText("Delete if residual > value");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        hbox.add(jtf);

        
        jtf = new JTextField(df2.format(newProps.getFloat("ampStripDistance")));
        jtf.getDocument().addDocumentListener(new MiscDocListener("ampStripDistance", jtf));
        jtf.setToolTipText("Delete if range > this value");
        jlbl = new JLabel (" or strip distance (km) > ");
        jlbl.setToolTipText("Delete if range > this value");
        hbox.add(jlbl);
        hbox.add(jtf);
        vbox.add(hbox);

        stripPanel.add(vbox);
        this.add(stripPanel);

// SmP multiplier for wf timeseries energy scan 
        jtf = new JTextField( df2.format(newProps.getDouble("wfSmPWindowMultiplier", AbstractWaveform.smpWindowMultiplier)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("wfSmPWindowMultiplier", jtf));
        jtf.setToolTipText("SmP multiplier scales length of window scanned for peak amp");
        jlbl = new JLabel ("SmP multiplier for peak amp window scans ");
        jlbl.setToolTipText("SmP multiplier scales length of window scanned for peak amp");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        hbox.add(jtf);

        this.add(hbox);

    }
}


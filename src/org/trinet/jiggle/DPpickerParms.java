package org.trinet.jiggle;

import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;

import org.trinet.jasi.picker.AutoPickGeneratorIF;
import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.GenericPropertyList;

/**
 * A Dialog panel for reviewing/editing phase picker properties.
 * Properties defined within the input GenericPropertiesList define picker classname(s)
 * and/or associated properties filesname(s), if any. These properties have the
 * prefixes "picker.class." and "picker.props." before the picker name, which 
 * is typically the String returned by PhasePickerIF .xetName().
 * The Picker to be used for autopicking is selected by setting the property 
 * picker.selected.name to the picker name String.
 */
public class DPpickerParms extends JPanel implements ActionListener {

    private GenericPropertyList newProps = null;
    private List chanParmList = null;
    private String prefix = AbstractPicker.getPropertyPrefix();

    private GenericPropertyList selectedPickerProps = null;

    /** The name of the currently selected picker */
    private String selectedPickerName = null;
    private String pickerPropsFilename = null;

    private JComboBox pickerNameBox = null;
    //private JLabel pickerClassnameField = null;
    //private JLabel pickerPropFileLabel = null;
    private JLabel pickerParmFileLabel = null;

    private JTextArea channelParmTextArea = null;

    private boolean hasChangedParms = false;

    private AutoPickGeneratorIF jiggle = null;

    JButton setParmsButton = null;
    JButton saveParmsButton = null;

    public DPpickerParms(GenericPropertyList newProps) {
        this(null, newProps);
    }

    /** Allow selection of waveform reading mode. */
    public DPpickerParms(AutoPickGeneratorIF jiggle, GenericPropertyList newProps) {

        this.jiggle = jiggle;
        this.newProps = newProps;
        PhasePickerIF picker = jiggle.getPhasePicker();
        chanParmList = (picker == null) ? (java.util.List) null : picker.getChannelParms();

        try {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public void actionPerformed(ActionEvent e) {
        if (newProps == null) return;
        JComboBox src = (JComboBox) e.getSource();
        String pickerName = (String) src.getSelectedItem();

        if (pickerName != null && !pickerName.equals("unknown") && !pickerName.equals(selectedPickerName))  {

            if (src != pickerNameBox) {
                pickerNameBox.setSelectedItem(pickerName);
                return;
            }

            selectedPickerName = pickerName;

            if (!newProps.getProperty("picker.selected.name", "").equals(selectedPickerName)) {
                newProps.setProperty("picker.selected.name", selectedPickerName);
                //System.out.println("INFO: property reset picker.selected.name=" + pickerClassnameField.getText());
                //chanParmList.clear(); ? ACTION update preferences dialog parms panel?
                //better to imbed parms config as popup dialog activated by button in this panel
            }
            else return; // or not ??

            chanParmList.clear();

            configureByProperties();
        }
    }

    public void addPickerChangeListener(ActionListener l) {
        pickerNameBox.addActionListener(l);
    }

    /** Build the GUI. */
    private void initGraphics() throws Exception {

        setLayout(new BorderLayout());

        channelParmTextArea = new JTextArea();
        channelParmTextArea.setToolTipText("Add a line of channel parms or edit to change, when done press \"Save\"");
        channelParmTextArea.setEditable(true);
        channelParmTextArea.setBackground(Color.white);
        channelParmTextArea.setRows(24);
        channelParmTextArea.setColumns(60);
        new JTextClipboardPopupMenu(channelParmTextArea);


         // Picker's channel parms action buttons
        ActionListener actionListener =  new SetSaveLoadChannelParmsActionHandler();
        setParmsButton = new JButton("Set");
        setParmsButton.addActionListener(actionListener);
        setParmsButton.setToolTipText("Set active picker's channel parms to current text area values.");

        JButton loadParmsButton = new JButton("Load");
        loadParmsButton.addActionListener(actionListener); 
        loadParmsButton.setToolTipText("Read and set active picker's channel parms from a specified file.");

        saveParmsButton = new JButton("Save");
        saveParmsButton.addActionListener(actionListener); 
        saveParmsButton.setToolTipText("Set, then write active picker's channel parms as shown in text area to a specified file.");

        Box channelButtonBox = Box.createHorizontalBox();
        channelButtonBox.add(Box.createHorizontalGlue());
        channelButtonBox.add(setParmsButton);
        channelButtonBox.add(saveParmsButton);
        channelButtonBox.add(loadParmsButton);
        channelButtonBox.add(Box.createHorizontalGlue());

        Box selectPanel = Box.createVerticalBox();
        Box hbox = Box.createHorizontalBox();
        JLabel jlbl = new JLabel("Selected Picker: ");
        hbox.add(jlbl);
        selectedPickerName = (newProps == null) ? "" : newProps.getProperty("picker.selected.name", "");

        String [] types = new String [] { "unknown" };
        if (newProps != null)  {
          //types = props.getStringArray("picker.namelist");
          types = DPpicker.getPickerNames(newProps);
          if (types == null || types.length == 0) types = new String [] { "unknown" };
        }
        // add AFTER box's picker names are populated else adding items fires events
        pickerNameBox = new JComboBox(types);
        pickerNameBox.setPreferredSize(new Dimension(80,30));
        pickerNameBox.setMaximumSize(new Dimension(120,30));
        pickerNameBox.addActionListener(DPpickerParms.this);
        hbox.add(pickerNameBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(Box.createHorizontalGlue());
        selectPanel.add(hbox);

        /*
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Class name: ");
        jlbl.setToolTipText("Selected picker classname");
        hbox.add(jlbl);

        pickerClassnameField = new JLabel();
        pickerClassnameField.setToolTipText("Classname specified for picker by property");
        selectedPickerName = (newProps == null) ? "" : newProps.getProperty("picker.selected.name", "");
        String str =  (newProps == null) ? "" : newProps.getProperty("picker.class."+selectedPickerName, "");
        pickerClassnameField.setText(str);
        hbox.add(pickerClassnameField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        selectPanel.add(hbox);
        */

        /*
        hbox = Box.createHorizontalBox();
        jlbl  = new JLabel("Properties filename: ");
        jlbl.setPreferredSize(new Dimension(120,24));
        jlbl.setToolTipText("Selected picker properties file");
        hbox.add(jlbl);
        pickerPropsFilename = (newProps == null) ? "" : newProps.getUserFileNameFromProperty("picker.props."+selectedPickerName);
        pickerPropFileLabel = new JLabel(pickerPropsFilename);
        pickerPropFileLabel.setToolTipText("Classname specified for picker by property");
        hbox.add(pickerPropFileLabel);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        selectPanel.add(hbox);
        */

        //
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Parm filename: ");
        jlbl.setToolTipText("Picker channel parameters file");
        hbox.add(jlbl);
        pickerParmFileLabel = new JLabel();
        pickerParmFileLabel.setPreferredSize(new Dimension(180,30));
        pickerParmFileLabel.setMaximumSize(new Dimension(360,30));
        pickerParmFileLabel.setToolTipText("Picker parameters file specified in Picker properties");
        hbox.add(pickerParmFileLabel);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        selectPanel.add(hbox);
        //

        TitledBorder titledBorder =
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, 
                        new Color(148, 145, 140)),"Channel Picking Parmeters");
        this.setBorder(titledBorder);
        this.add(selectPanel, BorderLayout.NORTH);
        JScrollPane jsp = new JScrollPane(channelParmTextArea);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(jsp, BorderLayout.CENTER);
        this.add(channelButtonBox, BorderLayout.SOUTH);

        configureByProperties();
    }

    private void configureByProperties() {

        pickerPropsFilename = newProps.getUserFileNameFromProperty("picker.props."+selectedPickerName);
        if (pickerPropsFilename != null) {

          String str = null;
          GenericPropertyList gpl = null;
          if (jiggle != null && jiggle.getPhasePicker() != null) { // init with current existing props
              gpl = new GenericPropertyList();
              gpl.setFiletype("jiggle");
              gpl.setUserPropertiesFileName(pickerPropsFilename);
              setPickerChannelTextArea(jiggle.getPhasePicker());
              pickerNameBox.setSelectedItem(selectedPickerName);
              //pickerPropFileLabel.setText(pickerPropsFilename);
              //pickerClassnameField.setText(jiggle.getPhasePicker().getClass().getName());
          }
          else {
              gpl = new GenericPropertyList(pickerPropsFilename, (String)null); // init loads props from disk file
              gpl.setFiletype("jiggle");
              setPickerChannelTextArea(gpl);
              pickerNameBox.setSelectedItem(selectedPickerName);
              //pickerPropFileLabel.setText(pickerPropsFilename);
              //pickerClassnameField.setText(jiggle.getPhasePicker().getClass().getName());
          }

        }
    }

    protected boolean hasChangedParms() {
        return hasChangedParms;
    }

    public boolean hasChanged() {
        return hasChangedParms;
    }

    public java.util.List getPickerChannelParms() {
        return (chanParmList == null) ? new java.util.ArrayList(0) : chanParmList;
    }

    private void doSetPickerChannelParms() {
        if (jiggle == null || jiggle.getPhasePicker() == null) {
             JOptionPane.showMessageDialog(getTopLevelAncestor(),
                       "Cannot set picker Parms, picker object is null", 
                       "Set Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
             return;
        }

        String str = channelParmTextArea.getText();
        if (str.length() == 0)  {
             if (chanParmList == null) chanParmList = new ArrayList(301);
             else chanParmList.clear();
             hasChangedParms = true; // set flag so processing of preferences dialog can test 
             //
             JOptionPane.showMessageDialog(getTopLevelAncestor(),
                       "Empty text area, all channels will use default parameters", 
                       "Set Picker Channel Parameters", JOptionPane.WARNING_MESSAGE);
             //
             return;
        }

        System.out.println("INFO: Properties dialog setting picker channel parms for: " + selectedPickerName);

        BufferedReader reader = null;
        try  {
            // parse properties from text area string
            reader = new BufferedReader(new StringReader(str));
            String line = null;
            PhasePickerIF picker = jiggle.getPhasePicker();
            //picker.clearChannelParms();

            if (chanParmList == null) chanParmList = new ArrayList(301);
            else chanParmList.clear();
            hasChangedParms = true; // set flag so processing of preferences dialog can test 

            while (true) {
                line = reader.readLine();
                if (line == null) break; 
                if (((AbstractPicker)picker).isComment(line)) continue;
                //picker.addChannelParm(picker.parseChannelParm(line)); 
                PickerParmIF parm = picker.parseChannelParm(line); 
                if (parm != null) chanParmList.add(parm);
                else throw new NullPointerException("Error parsing picker parm from text line");
            }
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                       ex.getMessage(), 
                       "Set Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
            try {
                if (reader != null) reader.close();
            }
            catch (Exception ex2) {
                System.err.println(ex2.getMessage());
            }
        }
    }

    protected void setPickerChannelTextArea(PhasePickerIF picker) {

        if (channelParmTextArea == null) return;

        if (picker == null) {
            channelParmTextArea.setText("# No Picker");
            pickerParmFileLabel.setText("");
            return;
        }

        selectedPickerProps = picker.getProperties(new GenericPropertyList());
        selectedPickerProps.setFiletype("jiggle");
        pickerParmFileLabel.setText(selectedPickerProps.getProperty(prefix+"chanParmFile", ""));

        java.util.List aList = picker.getChannelParms();
        if (aList == null) {
            picker.loadChannelParms();
        }

        // Add header at top, then parm values
        channelParmTextArea.setText( picker.getDefaultParm().getFormattedHeader() );

        if (aList == null || aList.size() <= 0) {
            channelParmTextArea.append("#");
            channelParmTextArea.append( picker.getDefaultParm().toFormattedString() );
            channelParmTextArea.append("\n");
            channelParmTextArea.setEditable(true);
            channelParmTextArea.setBackground(Color.LIGHT_GRAY);
            setParmsButton.setEnabled(true);
            saveParmsButton.setEnabled(true);
            return;
        }

        for (int idx=0; idx < aList.size(); idx++) {
            channelParmTextArea.append( ((PickerParmIF)aList.get(idx)).toFormattedString() );
            channelParmTextArea.append("\n");
            channelParmTextArea.setEditable(true);
            channelParmTextArea.setBackground(Color.WHITE);
            setParmsButton.setEnabled(true);
            saveParmsButton.setEnabled(true);
        }
    }

    /** Populate the text area with picker channel configuration settings.  */
    protected void setPickerChannelTextArea(GenericPropertyList gpl) {

        if (channelParmTextArea == null) return;

        selectedPickerProps = gpl;

        String chanParmFile = gpl.getProperty(prefix+"chanParmFile", "");
        pickerParmFileLabel.setText(chanParmFile);

        if (chanParmFile.length() == 0) {
            pickerParmFileLabel.setText("");
            channelParmTextArea.setText("");
        }
        else {
            chanParmFile = gpl.getUserFileNameFromProperty(prefix+"chanParmFile");
            setPickerChannelTextArea(chanParmFile);
        }

    }

    protected void setPickerChannelTextArea(String filename) {

        if (channelParmTextArea == null) return;

        pickerParmFileLabel.setText(filename);
        channelParmTextArea.setText("");
        if (filename == null) return;

        File file = null;
        try { 
            file = new File(filename); // reset current file to be read
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        if (! file.exists()) {
            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                "File not found:\n"+filename, 
                "Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = "";
            while (line != null) {
                line = br.readLine();
                channelParmTextArea.append(line);
                channelParmTextArea.append("\n");
            }
            channelParmTextArea.setCaretPosition(0);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        channelParmTextArea.setEditable(true);
        channelParmTextArea.setBackground(Color.WHITE);
        setParmsButton.setEnabled(true);
        saveParmsButton.setEnabled(true);
    }

    private class SetSaveLoadChannelParmsActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

        String cmd = evt.getActionCommand();

        try {

          boolean success = true;

          if (cmd == "Set") {
            doSetPickerChannelParms();
            return;
          }
          else {
            if (selectedPickerProps == null) {
                 JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Cannot save/load picker Parms, picker props is null", 
                           "Save/Load Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
                 return;
            }
            if (newProps == null) {
                JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Cannot save/load picker Parms, input Jiggle properties object is null", 
                           "Save/Load Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
                return;
            }

            JFileChooser jfc = new JFileChooser(selectedPickerProps.getUserFilePath());
            //JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            String filename = selectedPickerProps.getUserFileNameFromProperty(prefix+"chanParmFile");
            String oldName = filename;
            if (filename != null) {
                jfc.setSelectedFile(new File(filename));
                oldName = jfc.getSelectedFile().getAbsolutePath();
            }
            File file = null;

            if (cmd == "Save") {
              if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                  String str = channelParmTextArea.getText();
                  if (str.length() == 0)  {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Cannot save empty text", 
                           "Save Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
                     return;
                  }
                  // ? hasChangedParms = true; // set flag so processing of preferences dialog can 
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  System.out.println("INFO: Saving user edited channel parameters for: " +
                      selectedPickerName + " to file: " + filename);

                  //if (file.exists()) 
                  //    file.renameTo(file.getAbsolutePath()+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));


                  //selectedPickerProps.setProperty(prefix+"chanParmFile", filename);

                  // Parse and write parms from textArea to file
                  doSetPickerChannelParms();

                  BufferedReader reader = null;
                  BufferedWriter writer = null;
                  try  {
                      reader = new BufferedReader(new StringReader(str));
                      writer = new BufferedWriter(new FileWriter(filename));
                      String line = null;
                      while (true) {
                          line = reader.readLine();
                          if (line == null) break; 
                          writer.write(line);
                          writer.newLine();
                      }
                      writer.close();
                  }
                  catch (Exception ex) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           ex.getMessage(),
                           "Save Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
                      try {
                          if (reader != null) reader.close();
                          if (writer != null) writer.close();
                      }
                      catch (Exception ex2) {
                          System.err.println(ex2.getMessage());
                      }
                  }
              }
              else return;
            }
            else if (cmd == "Load") {
              if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  if (! file.exists()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           filename + " file D.N.E. !", 
                           "Load Picker Channel Parameters", JOptionPane.ERROR_MESSAGE);
                     return;
                  }

                  System.out.println("INFO: Loading channel parameters for: " + selectedPickerName +
                      " from file: " + filename);

                  //selectedPickerProps.setProperty(prefix+"chanParmFile", filename);
                  setPickerChannelTextArea(filename);
                  //doSetPickerChannelParms(); // do we want to do this, or force user to set?
              }
              else return;
            }

            if (oldName == null || ! oldName.equals(filename)) { // prompt to change chanParmFile property
                if ( JOptionPane.showConfirmDialog(null,
                    "Set new filename as picker's chanParmFile property value for " + selectedPickerName + "?",
                    "Channel Parm Filename Changed",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                  //System.out.println(jfc.getCurrentDirectory().getAbsolutePath() + " : " + newProps.getUserFilePath());
                  if (jfc.getCurrentDirectory().getAbsolutePath().equals(selectedPickerProps.getUserFilePath()) ) {
                      filename = jfc.getSelectedFile().getName(); // relative to jiggle USER dir
                  } // else its the full absolute directory path not in jiggle USER dir 
                  selectedPickerProps.setProperty(prefix+"chanParmFile", filename);

                  pickerParmFileLabel.setText(filename);

                  hasChangedParms = true;
                }
            }

          } // end of else block

          if (! success) notifyFailure(cmd, "Channel");

        }
        catch(Exception ex) {
          ex.printStackTrace();
          notifyFailure(cmd, "Channel");
        }
      }

    }

    private void notifyFailure(String cmd, String type) {
        JOptionPane.showMessageDialog(null,
            cmd + " failed , check text and messages!",
             type + " Properties", JOptionPane.PLAIN_MESSAGE);
    }
}

package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.FileFilter;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.SolutionTN;
import org.trinet.jasi.TN.MagnitudeTN;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.jasi.magmethods.AmpMagnitudeMethod;
import org.trinet.jdbc.datasources.SQLDataSourceIF;
import org.trinet.storedprocs.waveformrequest.RequestCardTN;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModel;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModelIF;

public class JiggleMenuBar extends JMenuBar {

    private Object menuActionCmd;  // last menu selection  - do we want to save this?

    private Jiggle jiggle;
    private HTMLHelp helpBrowser = null;

    // Move this element to Jiggle main, we need to kill frame and null this when evid in mv changes
    protected MecBrowserFrame mecBrowser = null;

    private LongDumpMenuListener longAL = null;

    protected String postingGroup = "PostProc";
    protected String postingSrcTab = "Jiggle";
    protected String postingState = "Selected";
    protected int postingRank = 100;
    protected int postingResult = 0;

    public JiggleMenuBar(Jiggle jiggle) {
        this.jiggle = jiggle;
        addMenuItems();

    }

    protected void setEventEnabled(boolean tf) { // add others if needed
      if (tf) {
        enableMenuActionItem("Event");
        enableMenuActionItem("Sort...");
        enableMenuActionItem("Event data info");
        enableMenuActionItem("Delete disk cache for selected event");
        enableMenuActionItem("Delete disk cache for selected station");
        enableMenuActionItem("Delete disk cache for selected channel");
        enableMenuActionItem("List event cached waveform files");
      }
      else {
        disableMenuActionItem("Event");
        disableMenuActionItem("Sort...");
        disableMenuActionItem("Event data info");
        disableMenuActionItem("List event cached waveform files");
      }
    }

    protected void setLocationServerEnabled(boolean tf) { // add others if needed
      if (tf) {
        enableMenuActionItem("Hyp2000 arc input file contents");
        enableMenuActionItem("Hyp2000 arc output file contents");
        enableMenuActionItem("Hyp2000 print file contents");
      }
      else {
        disableMenuActionItem("Hyp2000 arc input file contents");
        disableMenuActionItem("Hyp2000 arc output file contents");
        disableMenuActionItem("Hyp2000 print file contents");
      }
    }

    private boolean viewLoaded() {
        if (jiggle.mv != null && jiggle.mv.getSelectedSolution() != null  &&
            jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null &&
            jiggle.wfScroller.groupPanel.wfpList != null) {
                  return true;
        }

        JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
            "No event loaded into viewer",
            "Missing Waveform Views", JOptionPane.PLAIN_MESSAGE);

        return false; // no valid view string
    }

    // Should Actions object instances be declared for the MenuBar items?
    // Add an item to the main menu. Note: using this method adds items to enable/disable later.
    private JMenuItem addToMenu(String itemActionCmd, JMenu menu, ActionListener al, boolean enable) {
        return addToMenu(itemActionCmd, menu, al, null, enable);
    }

    private JMenuItem addToMenu(String itemActionCmd, JMenu menu, ActionListener al, String tooltip, boolean enable) {
      JMenuItem m = new JMenuItem(itemActionCmd);
      if (tooltip != null) m.setToolTipText(tooltip);
      if (al != null) m.addActionListener(al);
      m.setEnabled(enable);
      return menu.add(m);
    }

    private JMenuItem addSubMenu(JMenu subMenu, JMenu mainMenu, ActionListener al, boolean enable) {
        return addToMenu(subMenu.getActionCommand(), mainMenu, al, enable);
    }

    public void disableMenuActionItem(String actionCmd) {
      JMenuItem item = getMenuBarActionItem(actionCmd);
      if (item != null) item.setEnabled(false);
    }
    public void enableMenuActionItem(String actionCmd) {
      JMenuItem item = getMenuBarActionItem(actionCmd);
      if (item != null) item.setEnabled(true);
    }

    // Finds 1st match with similar action command string, null if no match
    public JMenuItem getMenuBarActionItem(String actionCmd) {
       MenuElement [] me = getSubElements();
       int elementCount = me.length;
       if (elementCount == 0) return null; // empty

       JMenuItem foundMenuItem = null;
       JMenuItem menuItem = null;

       for (int i= 0; i < elementCount; i++) { // loop over menus
         MenuElement element = me[i];
         if (element == null) continue;  // should never be null
         if (element instanceof JMenuItem) {
           menuItem = (JMenuItem) element;
           if ( menuItem.getActionCommand().equals(actionCmd) ) {
             foundMenuItem = menuItem;
             break;
           }
         }
         if (menuItem instanceof JMenu) {
           menuItem = getMenuBarActionItem((JMenu) menuItem, actionCmd);
           if (menuItem != null) {
             foundMenuItem = menuItem;
             break;
           }
         }
       }
       return foundMenuItem;
    }

    private JMenuItem getMenuBarActionItem(JMenu menu, String actionCmd) {
       int itemCount = menu.getItemCount();
       if (itemCount <= 0) return null; 

       JMenuItem foundMenuItem = null;
       JMenuItem subMenuItem = null;

       for (int j = 0; j < itemCount; j++) {
           subMenuItem = menu.getItem(j);
           if (subMenuItem == null) continue; // possible
           if (subMenuItem.getActionCommand().equals(actionCmd) ) {
             foundMenuItem = subMenuItem;
             break;
           }
           if (subMenuItem instanceof JMenu) {
             subMenuItem = getMenuBarActionItem((JMenu)subMenuItem, actionCmd);
             if (subMenuItem != null) {
               foundMenuItem = subMenuItem;
               break;
             }
           }
       }
       return foundMenuItem;
    }

    // Do we need to save the event data, the e.getSource()?
    // beware of any worker threads actions on states, can't predict
    // multiple listener effects.
    private void saveMenuEventData(ActionEvent e) {
        menuActionCmd = e.getActionCommand();
        //menuActionObj = e.getSource();
    }

    private void addMenuItems() {
      add(makeFileMenu());
      //add(makeSortMenu()); // moved to Event menu
      add(makeViewMenu());
      add(makeEventMenu());
      add(makeOptionsMenu());
      add(makePropertiesMenu());
      add(makeChanCacheMenu());
      add(makeWfCacheMenu());
      add(makeInfoMenu());
      add(makeDumpMenu());
      add(makeSolServerMenu());
      add(makeHelpMenu());

      if (jiggle != null && jiggle.getProperties().getBoolean("networkModeWAN")) {
          setBackground(Color.pink);
          setToolTipText("When pink, network mode WAN settings are in effect");
      }
    }

    private JMenu makeVelModelMenu() {

      final JMenu modelMenu = new JMenu("Velocity model...");
      modelMenu.setToolTipText("Change velocity model to use for currently loaded waveforms");

      final VelocityModelList modelList = jiggle.getProperties().getVelocityModelList();

      if (modelList.size() == 0) { // no defined model, so make a default
            UniformFlatLayerVelocityModelIF defaultModel = TravelTime.createDefaultModel();
            if (defaultModel != null) {
               modelList.addObject(defaultModel); 
               //modelList.setSelected(defaultModel.getName()); 
               //jiggle.getProperties().setDefaultVelocityModelName(defaultModel.getName());
               jiggle.setVelocityModel(modelList, defaultModel.getName());
            }
      }

      ActionListener al = new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final String modelName = e.getActionCommand();
                System.out.println("INFO: Jiggle set default velocity model name to : " + modelName);
                JMenuItem jmi = getMenuBarActionItem(((UniformFlatLayerVelocityModelIF) modelList.getSelected()).getName());
                jmi.setBackground(modelMenu.getBackground());
                jmi = getMenuBarActionItem(modelName);
                jmi.setBackground(Color.lightGray);

                jiggle.setVelocityModel(modelList, modelName);

                SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        if (jiggle.mv != null) jiggle.mv.alignViews(); // in case the velocity model, or alignmode mode changed -aww 2009/04/01
                        if (jiggle.statusBar != null) jiggle.statusBar.updateVelModelLabel();
                        if (jiggle.pickPanel != null) jiggle.pickPanel.repaint();
                        if (jiggle.wfScroller != null) jiggle.wfScroller.repaint();
                    }
                });
            }
      };

      UniformFlatLayerVelocityModel selected = (UniformFlatLayerVelocityModel) modelList.getSelected();
      //System.out.println("DEBUG selected model: " + selected);
      JMenuItem jmi = null;
      UniformFlatLayerVelocityModel model = null;
      for (int idx = 0; idx < modelList.size(); idx++ ) {
          model = (UniformFlatLayerVelocityModel) modelList.get(idx);
          if (model != null) {
            jmi = addToMenu(model.getName(), modelMenu, al, true);
            if (model == selected) jmi.setBackground(Color.lightGray);
          }
      }

      return modelMenu;

    }

    private JMenu makeWSModelMenu() {
      JMenu modelMenu = new JMenu("Waveform source/model...");
      modelMenu.setToolTipText("Change waveform source and channel time window model pair");
      ActionListener al = new ModelMenuListener();

      String oldCTWM = jiggle.getProperties().getProperty("currentChannelTimeWindowModel","");
      boolean isWS = (jiggle.getProperties().getInt("waveformReadMode") == AbstractWaveform.LoadFromWaveServer);

      JMenuItem jmi = null;

      jmi = addToMenu("DB event", modelMenu, al,"from Database using DataSource model", true);
      if (!isWS && oldCTWM == DEFAULT_DS_CTWM) jmi.setBackground(Color.lightGray);
      // trigger not sensible unless set includeDataSourceWf=true and includeAllComponents=false? -aww
      //addToMenu("DB trigger", modelMenu, al,"from Database using Trigger model", true);
      jmi = addToMenu("DB picks", modelMenu, al,"from Database using PicksOnly model", true);
      if (!isWS && oldCTWM == DEFAULT_PO_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("DB w/o picks", modelMenu, al,"from Database using MissingPicks model", true);
      if (!isWS && oldCTWM == DEFAULT_MP_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("DB named", modelMenu, al,"from Database using NamedList model ", true);
      if (!isWS && oldCTWM == DEFAULT_NL_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("DB Ml", modelMenu, al,"from Database using WADataSource model", true);
      if (!isWS && oldCTWM == DEFAULT_WA_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("DB Md", modelMenu, al,"from Database using MdDataSource model", true);
      if (!isWS && oldCTWM == DEFAULT_MD_CTWM) jmi.setBackground(Color.lightGray);
      modelMenu.addSeparator();

      // JB, PowerLaw first time use overhead (long wait) channel response gain queried from db 
      // unless master channel list is set to be used as candidate list via property setting
      jmi = addToMenu("WS power law", modelMenu, al,"from WaveServer using PowerLaw model", true);
      if (isWS && oldCTWM == DEFAULT_PL_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("WS trigger", modelMenu, al,"from WaveServer using Trigger model", true);
      if (isWS && oldCTWM == DEFAULT_TG_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("WS energy", modelMenu, al,"from WaveServer using EnergyWindow model ", true);
      if (isWS && oldCTWM == DEFAULT_EG_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("WS picks", modelMenu, al,"from WaveServer using PicksOnly channels model", true);
      if (isWS && oldCTWM == DEFAULT_PO_CTWM) jmi.setBackground(Color.lightGray);
      // MissingPicks as implemented uses db AssocWaE so need class variant extending PowerLaw or like model for missing picks for WS
      //addToMenu("WS w/o picks", modelMenu, al,"from WaveServer using MissingPickPowerLaw model", true); // too slow!!!
      jmi = addToMenu("WS w/o picks", modelMenu, al,"from WaveServer using MissingPickSimple model", true);
      if (isWS && oldCTWM == DEFAULT_MPSWS_CTWM) jmi.setBackground(Color.lightGray);
      jmi = addToMenu("WS named", modelMenu, al,"from WaveServer using NamedList model ", true);
      if (isWS && oldCTWM == DEFAULT_NL_CTWM) jmi.setBackground(Color.lightGray);
      modelMenu.addSeparator();
      return modelMenu;
    }

    protected static String DEFAULT_SP_CTWM = "org.trinet.jasi.SimpleChannelTimeModel"; // not used below (uses mag,distance cutoff)
    protected static String DEFAULT_JB_CTWM = "org.trinet.jasi.JBChannelTimeWindowModel"; // not used below (uses mag, distance, instr gain)

    protected static String DEFAULT_EG_CTWM = "org.trinet.jasi.EnergyWindowModel"; // all channels by predicted TT model

    protected static String DEFAULT_PL_CTWM = "org.trinet.jasi.PowerLawTimeWindowModel";
    protected static String DEFAULT_DS_CTWM = "org.trinet.jasi.DataSourceChannelTimeModel";
    protected static String DEFAULT_TG_CTWM = "org.trinet.jasi.TriggerChannelTimeWindowModel";
    protected static String DEFAULT_NL_CTWM = "org.trinet.jasi.NamedChannelTimeWindowModel";
    protected static String DEFAULT_PO_CTWM = "org.trinet.jasi.PicksOnlyChannelTimeModel";
    protected static String DEFAULT_MP_CTWM = "org.trinet.jasi.MissingPicksChannelTimeModel";
    protected static String DEFAULT_MPWS_CTWM = "org.trinet.jasi.MissingPickPowerLawTimeWindowModel";
    protected static String DEFAULT_MPSWS_CTWM = "org.trinet.jasi.MissingPickSimpleChannelTimeModel";
    protected static String DEFAULT_WA_CTWM = "org.trinet.jasi.WADataSourceChannelTimeModel";
    protected static String DEFAULT_MD_CTWM = "org.trinet.jasi.MdDataSourceChannelTimeModel";

    private class ModelMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            String cmd = evt.getActionCommand();
            JiggleProperties jp = jiggle.getProperties();
            int oldMode = jp.getInt("waveformReadMode");
            String oldCTWM = jp.getProperty("currentChannelTimeWindowModel","");
            String newCTWM = "";
            int newMode = -1;
            if (cmd.startsWith("WS")) { // WaveServer
              newMode = AbstractWaveform.LoadFromWaveServer;
              jp.setProperty("waveformReadMode", newMode);
              jp.setupWaveServerGroup();
              AbstractWaveform.setWaveDataSource(jp.getWaveSource());

              if (cmd.equals("WS power law")) {
                  newCTWM = DEFAULT_PL_CTWM;
              }
              else if (cmd.equals("WS trigger")) {
                  newCTWM = DEFAULT_TG_CTWM;
              }
              else if (cmd.equals("WS picks")) {
                  newCTWM = DEFAULT_PO_CTWM;
              }
              else if (cmd.equals("WS w/o picks")) {
                  //newCTWM = DEFAULT_MPWS_CTWM; // too slow !!!
                  newCTWM = DEFAULT_MPSWS_CTWM;
              }
              else if (cmd.equals("WS named")) {
                  newCTWM = DEFAULT_NL_CTWM;
              }
              else if (cmd.equals("WS energy")) {
                  newCTWM = DEFAULT_EG_CTWM;
              }
            }
            else { // DataSource
              newMode = AbstractWaveform.LoadFromDataSource;
              jp.setProperty("waveformReadMode", newMode);
              AbstractWaveform.setWaveDataSource(jp.getWaveSource());

              if (cmd.equals("DB event")) {
                  newCTWM = DEFAULT_DS_CTWM;
              }
              else if (cmd.equals("DB trigger")) {
                  newCTWM = DEFAULT_TG_CTWM;
              }
              else if (cmd.equals("DB named")) {
                  newCTWM = DEFAULT_NL_CTWM;
              }
              else if (cmd.equals("DB picks")) {
                  newCTWM = DEFAULT_PO_CTWM;
              }
              else if (cmd.equals("DB w/o picks")) {
                  newCTWM = DEFAULT_MP_CTWM;
              }
              else if (cmd.equals("DB Ml")) {
                  newCTWM = DEFAULT_WA_CTWM;
              }
              else if (cmd.equals("DB Md")) {
                  newCTWM = DEFAULT_MD_CTWM;
              }
            }

            if (newMode == oldMode && newCTWM.equals(oldCTWM)) {
                return;
            }

            if (! newCTWM.equals(oldCTWM)) {
                if (! jp.setChannelTimeWindowModel(newCTWM)) {
                  System.out.println("INFO: adding model:" + newCTWM + "\n to model list defined by property \"channelTimeWindowModelList\"");
                  jp.getChannelTimeWindowModelList().add(newCTWM, jp); // add model to list
                  jp.synchProperties(); // redefines list property value
                  if (! jp.setChannelTimeWindowModel(newCTWM)) {
                      System.err.println("ERROR: unable to add model:" + newCTWM + "\n to ChannelTimeWindowModel list");
                      JOptionPane.showMessageDialog(jiggle,
                          "Unable to add model to list specified by property \"channelTimeWindowModelList\"",
                          "Missing Channel Time Window Model", JOptionPane.PLAIN_MESSAGE);
                  }
                }

                // check wf cache for loaded event -aww 2010/07/02
                Solution oldSol = (jiggle.mv == null ) ? null : jiggle.mv.getSelectedSolution();
                if (oldSol != null && AbstractWaveform.cache2FileDir != null) {
                    File file = new File(AbstractWaveform.cache2FileDir);  
                    if (file.exists()) {
                        final long evid = oldSol.getId().longValue();
                        File [] files = file.listFiles(
                            new java.io.FileFilter() {
                              public boolean accept(File file) {
                                  String filename = file.getAbsolutePath();
                                  if (!filename.endsWith(".seg")) return false;
                                  return (filename.indexOf(String.valueOf(evid)) >= 0);
                              }
                            }
                        );

                        if (files.length > 0) {
                            int answer = JOptionPane.showConfirmDialog(JiggleMenuBar.this.jiggle,
                              "Wavesource or WindowModel changed: delete event's waveform cache before reset?",
                              "WaveSource/WindowModel Change", JOptionPane.YES_NO_OPTION);

                            if (answer == JOptionPane.YES_OPTION)
                                AbstractWaveform.clearCacheDir(evid, null, null);
                        }
                    }
                }
            }

            if (jiggle.mv != null) {
                jiggle.mv.setChannelTimeWindowModel(jp.getCurrentChannelTimeWindowModel());
                System.out.println("Set masterview channel time window model to: " + jiggle.mv.getChannelTimeWindowModel().getModelName());
            }

            jiggle.resetMenuAndToolBars();
            jiggle.updateStatusBar();

            if ( jiggle.mv.getSelectedSolution() != null ) {
                if (JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(jiggle,
                          "Reload all data for selected event from database tables\nNO = Keeps current view picks, amps, and coda",
                          "ChannelTimeWindowModel Changed", JOptionPane.YES_NO_OPTION)) {
                    jiggle.refreshWFViewList();
                } else {
                    jiggle.reloadSolution();
                }
            }
        }
    }

    private JMenu makeFileMenu() {
      JMenu fileMenu = new JMenu("File");
      ActionListener al = new FileMenuListener();
      addToMenu("Open event by id...", fileMenu, al,"Enter id of event to load into view", true);
      addToMenu("Open catalog selection", fileMenu, al, "Load event selected in catalog into view", true);
      addToMenu("Close event", fileMenu, al, "Refresh GUI display, no event loaded into view", false);
      fileMenu.addSeparator();

      fileMenu.add(makePrintMenu());
      fileMenu.addSeparator();

      addToMenu("Exit", fileMenu, al, true);

      return fileMenu;
    }

    private JMenu makePrintMenu() {
      JMenu printMenu = new JMenu("Print...");
      ActionListener al = new PrintMenuListener();
      addToMenu("Whole window", printMenu, al, true);
      addToMenu("Group panel", printMenu, al, true);
      addToMenu("Zoom panel", printMenu, al, true);
      addToMenu("Zoom & Group", printMenu, al, true);
      //addToMenu("Group (all)", printMenu, al, false);
      return printMenu;
    }

    private JMenu makeSortMenu() {
      JMenu sortMenu = new JMenu("Sort waveforms...", true);
      ActionListener al = new SortMenuListener();
      addToMenu("by distance from epicenter", sortMenu, al, true);
      addToMenu("by distance from selected channel", sortMenu, al, true);
      sortMenu.setEnabled(false); // event not loaded? aww
      return sortMenu;
    }

    private JMenu makeViewMenu() {
      JMenu viewMenu = new JMenu("View", true);
      viewMenu.add(makeWaveMenu());
      viewMenu.add(makeCatMapMenu());
      return viewMenu;
    }

    private class WfCacheMenuActionListener implements ActionListener {

        public void actionPerformed(ActionEvent evt) {
          saveMenuEventData(evt);

          if (AbstractWaveform.cache2FileDir == null || AbstractWaveform.cache2FileDir.trim().length() == 0) {
            JOptionPane.showMessageDialog(jiggle, "Waveform cache directory is undefined!",
                        "Waveform Cache", JOptionPane.PLAIN_MESSAGE);
            return;
          }

          final String cmd = evt.getActionCommand();
          jiggle.initStatusGraphicsForThread("Disk Wavform Cache", cmd + "..."); // pops status

          org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {

              int count = 0;
              String str = null;

              public Object construct() { // any GUI graphics in method should invokeLater
                try {
                  if ( cmd.equals("List all cached waveform files") || cmd.equals("List event cached waveform files") ) {
                    File file = new File(AbstractWaveform.cache2FileDir);  
                    if (! file.exists()) {
                      JOptionPane.showMessageDialog(jiggle, "Waveform cache directory does not exist!\n" + AbstractWaveform.cache2FileDir,
                        "Waveform Cache", JOptionPane.PLAIN_MESSAGE);
                        return null;
                    }

                    File [] files = file.listFiles(
                        new java.io.FileFilter() {
                          public boolean accept(File file) {
                              String filename = file.getAbsolutePath();
                              if (!filename.endsWith(".seg")) return false;
                              if (cmd.indexOf("event") >= 0) {
                                Solution sol = jiggle.mv.getSelectedSolution();
                                if (sol == null) return false;
                                return (filename.indexOf(String.valueOf(sol.getId().longValue())) >= 0);
                              }
                              return true;
                          }
                        }
                    );

                    count = files.length;
                    StringBuffer sb = new StringBuffer((count+1)*64);
                    if (count > 0) {
                        for (int ii = 0; ii<count; ii++) {
                          sb.append(files[ii].getAbsolutePath()).append("\n");
                        }
                    }
                    sb.append("#Total of " + count + " files found in " + AbstractWaveform.cache2FileDir);
                    str = sb.toString();
                  }
                  else if (cmd.startsWith("Delete")) {
                    if (cmd.endsWith("selected event")) {
                      Solution sol = jiggle.mv.getSelectedSolution();
                      if (sol != null) count = AbstractWaveform.clearCacheDir(sol.getId().longValue(), null, null);
                    }
                    else if (cmd.endsWith("selected station")) {
                      Solution sol = jiggle.mv.getSelectedSolution();
                      if (sol != null) {
                        WFView wfvSel = jiggle.mv.masterWFViewModel.get();
                        if (wfvSel != null) {
                            count = AbstractWaveform.clearCacheDir(sol.getId().longValue(), null, wfvSel.getChannelObj().getSta());
                        }
                      }
                    }
                    else if (cmd.endsWith("selected channel")) {
                      Solution sol = jiggle.mv.getSelectedSolution();
                      if (sol != null) {
                        WFView wfvSel = jiggle.mv.masterWFViewModel.get();
                        if (wfvSel != null) {
                            count = AbstractWaveform.clearCacheDir(sol.getId().longValue(), wfvSel, null);
                        }
                      }
                    }
                    else if (cmd.endsWith("all events")) {
                        count = AbstractWaveform.clearCacheDir(0l, null, null);
                    }
                  }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
              } // end construct method

              public void finished() { // GUI update here
                  jiggle.resetStatusGraphicsForThread(); // unpops status
                  if (str != null) jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true);
                  System.out.println("Jiggle INFO: " + cmd +  ", total files: " + count);
              }

          }; // end of SwingWorker
          sw.start(); // start long task worker thread

        } // end of actionPerformed
    } // end of inner class

    private JMenu makeSolServerMenu() {
      final JMenu solServerMenu = new JMenu("SolServer");
      addToMenu("Hyp2000 available command files", solServerMenu, longAL, "list of the hyp2000 command files available on the server", true);
      addToMenu("Hyp2000 command file name", solServerMenu, longAL, "name of the active command file this server session", true);
      addToMenu("Hyp2000 command file contents", solServerMenu, longAL, "contents of the active command file this server session", true);
      //deprecated: velocity models can be complex, defined in multiples files and the simple case here only worked for single file CRH - aww 2015/11/10
      //addToMenu("Hyp2000 velocity model file contents", solServerMenu, longAL, "contents of the velocity model file this server session", true);
      addToMenu("Hyp2000 station file contents", solServerMenu, longAL, "contents of the station file this server session", true);

      addToMenu("Hyp2000 print file contents", solServerMenu, longAL, ".prt file on the server for last event located this session ", false);
      addToMenu("Hyp2000 arc input file contents", solServerMenu, longAL, ".arcin file on the server for last event located this session", false);
      addToMenu("Hyp2000 arc output file contents", solServerMenu, longAL, ".arcout file on the server for last event located this session", false);
      //solServerMenu.addSeparator();
      return solServerMenu;
    }
    private JMenu makeWfCacheMenu() {
      final JMenu cacheMenu = new JMenu("WaveCache");
      ActionListener al = new WfCacheMenuActionListener();
      addToMenu("List all cached waveform files", cacheMenu, al, true);
      cacheMenu.addSeparator();
      addToMenu("List event cached waveform files", cacheMenu, al, false);
      cacheMenu.addSeparator();
      addToMenu("Delete disk cache for selected channel", cacheMenu, al, false);
      addToMenu("Delete disk cache for selected station", cacheMenu, al, false);
      addToMenu("Delete disk cache for selected event", cacheMenu, al, false);
      addToMenu("Delete disk cache for all events", cacheMenu, al, true);
      cacheMenu.addSeparator();
      return cacheMenu;
    }

    private JMenu makeWaveMenu() {
      final JMenu waveMenu = new JMenu("Waveforms...");

      ActionListener al = new MainSplitMenuListener();

      boolean wfInTab = jiggle.getProperties().getBoolean("waveformInTab");
      String itemText = (jiggle.getProperties().getInt("mainSplitOrientation") == JSplitPane.HORIZONTAL_SPLIT) ?
            "Vertical split" : "Horizontal split";
      JMenuItem splitItem = addToMenu(itemText, waveMenu, al, !wfInTab);
      splitItem.setVisible(! wfInTab);
      al = new ViewMenuListener(splitItem);
      itemText = (wfInTab) ?  "Waveforms in SplitPane" : "Waveforms in TabPane";
      addToMenu(itemText, waveMenu, al, true);
      waveMenu.addSeparator();

      al = new WaveformPanelSetupListener();
      addToMenu("Waveform group panel setup...", waveMenu, al, true);
      waveMenu.addSeparator();
     // Check box in menu- toggle show/noshow red channel name labels in viewport 
      JMenu alignMenu = new JMenu("Alignment mode...");
      ButtonGroup bg = new ButtonGroup();
      int alignMode = jiggle.getProperties().getInt("masterViewWaveformAlignMode", 0);

      JRadioButtonMenuItem rbMenuItem = new JRadioButtonMenuItem("T");
      rbMenuItem.setToolTipText("align on absolute start times");
      ActionListener aListener = new AlignmentModeListener();
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((alignMode == 0)); 
      alignMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("P");
      rbMenuItem.setToolTipText("align on predicted P times");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((alignMode == 1)); 
      alignMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("S");
      rbMenuItem.setToolTipText("align on predicted S times");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((alignMode == 2)); 
      alignMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      String vel = jiggle.getProperties().getProperty("masterViewWaveformAlignVel","");
      rbMenuItem = new JRadioButtonMenuItem("V "+vel);
      rbMenuItem.setToolTipText("align by reduced velocity time");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((alignMode == 3)); 
      alignMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      alignMenu.addSeparator();
      JCheckBoxMenuItem jcbmi = new JCheckBoxMenuItem("Scroll to group center");
      jcbmi.setToolTipText("Scroller arrows scroll selection to center of group view");
      jcbmi.addActionListener(aListener);
      jcbmi.setSelected(WFScroller.scrollerMode == WFScroller.SCROLL_TO_CENTER); 
      alignMenu.add(jcbmi);

      alignMenu.addSeparator();
      jcbmi = new JCheckBoxMenuItem("Triaxial mode group/zoom scroll synch"); 
      jcbmi.setToolTipText("In triaxial mode, horizontal scrolling of group panel scrolls upper zoom panel's window");
      jcbmi.setSelected(jiggle.getProperties().getBoolean("triaxialScrollZoomWithGroup")); 
      //jcbmi.setSelected(WFScroller.triaxialScrollZoomWithGroup);
      jcbmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBoxMenuItem jcb = (JCheckBoxMenuItem) evt.getSource();
                WFScroller.triaxialScrollZoomWithGroup = jcb.isSelected(); 
                jiggle.getProperties().setProperty("triaxialScrollZoomWithGroup", jcb.isSelected());
            }

      });
      alignMenu.add(jcbmi);

      waveMenu.add(alignMenu);
      waveMenu.addSeparator();

      // Check box in menu- toggle show/noshow red channel name labels in viewport 
      JMenu labelMenu = new JMenu("Show channel labels...");
      bg = new ButtonGroup();

      int chanLabelMode = jiggle.getProperties().getInt("channelLabelMode", 0);

      rbMenuItem = new JRadioButtonMenuItem("Starting");
      rbMenuItem.setToolTipText("labels painted at the starting time of the panel view");
      aListener = new ChannelLabelListener();
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((chanLabelMode == 0)); 
      labelMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("Leading");
      rbMenuItem.setToolTipText("labels painted at the leading edge of the scrolled viewport");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((chanLabelMode == 1)); 
      labelMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("Trailing");
      rbMenuItem.setToolTipText("labels painted at the trailing edge of the scrolled viewport");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((chanLabelMode == 2)); 
      labelMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("Both");
      rbMenuItem.setToolTipText("labels painted at boths edges of the scrolled viewport");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((chanLabelMode == 3)); 
      labelMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      rbMenuItem = new JRadioButtonMenuItem("None");
      rbMenuItem.setToolTipText("no labels painted on panels");
      rbMenuItem.addActionListener(aListener);
      rbMenuItem.setSelected((chanLabelMode == 4)); 
      labelMenu.add(rbMenuItem);
      bg.add(rbMenuItem);

      labelMenu.addSeparator();
      jcbmi = new JCheckBoxMenuItem("Use full names", ChannelName.useFullName); 
      jcbmi.setToolTipText("net.sta.seedchan.channel.location.channelsrc");
      jcbmi.addActionListener( new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            saveMenuEventData(e);
            JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
            ChannelName.useFullName = item.isSelected();
            jiggle.getProperties().setChannelUseFullName(ChannelName.useFullName);
            if (jiggle.wfScroller != null) jiggle.wfScroller.repaint();
            if (jiggle.pickPanel != null) jiggle.pickPanel.makeChannelLabel(); // redraw sta label
         }
      });
      labelMenu.add(jcbmi);

      waveMenu.add(labelMenu);
      waveMenu.addSeparator();

      // Check box in menu - to smooth drawing of jagged timeseries
      JCheckBoxMenuItem antialiasCheckItem = new JCheckBoxMenuItem("Antialias waveform");
      antialiasCheckItem.addActionListener(new AntialiasCheckBoxListener());
      antialiasCheckItem.setSelected(jiggle.getProperties().getBoolean("antialiasWaveform"));
      waveMenu.add(antialiasCheckItem);

      // Check box in menu
      JCheckBoxMenuItem pickFlagFontCheckItem = new JCheckBoxMenuItem("Pick flag font (large) bold type");
      pickFlagFontCheckItem.addActionListener(new PickFlagFontCheckBoxListener());
      pickFlagFontCheckItem.setSelected(jiggle.getProperties().getProperty("pickFlagFont","").equalsIgnoreCase("BIG"));
      waveMenu.add(pickFlagFontCheckItem);

      // Check box in menu
      JCheckBoxMenuItem pickFlagColorCheckItem = new JCheckBoxMenuItem("Color picks by phase type");
      pickFlagColorCheckItem.addActionListener(new PickFlagColorCheckBoxListener());
      pickFlagColorCheckItem.setSelected(jiggle.getProperties().getBoolean("pickFlag.colorByPhase", false));
      waveMenu.add(pickFlagColorCheckItem);

      // Check box in menu
      JCheckBoxMenuItem rowHeaderCheckItem = new JCheckBoxMenuItem("Show row headers");
      rowHeaderCheckItem.addActionListener(new RowHeaderCheckBoxListener());
      rowHeaderCheckItem.setSelected(jiggle.getProperties().getBoolean("showRowHeaders"));
      waveMenu.add(rowHeaderCheckItem);

      // Check box in menu
      JCheckBoxMenuItem segmentCheckItem = new JCheckBoxMenuItem("Show segments");
      segmentCheckItem.addActionListener(new SegmentCheckBoxListener());
      segmentCheckItem.setSelected(jiggle.getProperties().getBoolean("showSegments"));
      waveMenu.add(segmentCheckItem);

      // Check box in menu - toggle show/noshow samples when trace is expanded
      JCheckBoxMenuItem showSamplesCheckItem = new JCheckBoxMenuItem("Show samples");
      showSamplesCheckItem.addActionListener(new ShowSampleCheckBoxListener());
      showSamplesCheckItem.setSelected(jiggle.getProperties().getBoolean("showSamples"));
      waveMenu.add(showSamplesCheckItem);

      // Check box in menu- toggle show/noshow green phase queue bars
      JCheckBoxMenuItem phaseCueCheckItem = new JCheckBoxMenuItem("Show phase cues");
      phaseCueCheckItem.addActionListener(new PhaseCueCheckBoxListener());
      phaseCueCheckItem.setSelected(jiggle.getProperties().getBoolean("showPhaseCues"));
      waveMenu.add(phaseCueCheckItem);

      // Check box in menu- toggle show/noshow observations
      ActionListener pfal = new PickFlagCheckBoxListener();
      JCheckBoxMenuItem pickFlagCheckItem = new JCheckBoxMenuItem("Show pick,amp,coda flags");
      pickFlagCheckItem.setActionCommand("flags");
      pickFlagCheckItem.setSelected(true); // always true at start
      pickFlagCheckItem.addActionListener(pfal);
      //pickFlagCheckItem.setSelected(jiggle.getProperties().getBoolean("showPickFlags")); // not a property !
      waveMenu.add(pickFlagCheckItem);

      // Check box in menu- toggle show/noshow arrival deltim (weight) uncertainty seconds
      pickFlagCheckItem = new JCheckBoxMenuItem("Show pick weight delta times");
      pickFlagCheckItem.setActionCommand("deltas");
      pickFlagCheckItem.setSelected(jiggle.getProperties().getBoolean("showDeltimes",true));
      pickFlagCheckItem.addActionListener(pfal);
      waveMenu.add(pickFlagCheckItem);

      // Check box in menu- toggle show/noshow arrival deltim (weight) uncertainty seconds
      pickFlagCheckItem = new JCheckBoxMenuItem("Show traveltime residuals");
      pickFlagCheckItem.setActionCommand("resids");
      pickFlagCheckItem.setSelected(jiggle.getProperties().getBoolean("showResiduals",true));
      pickFlagCheckItem.addActionListener(pfal);
      waveMenu.add(pickFlagCheckItem);

      // Check box in menu- toggle show/noshow time tick scale borders on wfpanels
      ActionListener tsal = new ShowTimeScaleCheckBoxListener();
      JCheckBoxMenuItem timeScaleCheckItem = new JCheckBoxMenuItem("Show time scale");
      timeScaleCheckItem.setActionCommand("timescale");
      timeScaleCheckItem.setSelected(jiggle.getProperties().getBoolean("wfpanel.showTimeScale",true));
      timeScaleCheckItem.addActionListener(tsal);
      waveMenu.add(timeScaleCheckItem);

      // Check box in menu- toggle show/noshow panel time minute labels
      timeScaleCheckItem = new JCheckBoxMenuItem("Show time labels");
      timeScaleCheckItem.setActionCommand("timelabel");
      timeScaleCheckItem.setSelected(jiggle.getProperties().getBoolean("wfpanel.showTimeScaleLabel",true));
      timeScaleCheckItem.addActionListener(tsal);
      waveMenu.add(timeScaleCheckItem);

      // Check box in menu- toggle show/noshow vertical line at cursor time
      JCheckBoxMenuItem cursorTimeLineCheckItem = new JCheckBoxMenuItem("Show cursor time line");
      cursorTimeLineCheckItem.addActionListener(new CursorTimeCheckBoxListener());
      cursorTimeLineCheckItem.setSelected(jiggle.getProperties().getBoolean("showCursorTimeAsLine"));
      waveMenu.add(cursorTimeLineCheckItem);

      // Check box in menu- toggle show/noshow vertical line at cursor amp
      JCheckBoxMenuItem cursorAmpLineCheckItem = new JCheckBoxMenuItem("Show cursor amp line");
      cursorAmpLineCheckItem.addActionListener(new CursorAmpCheckBoxListener());
      cursorAmpLineCheckItem.setSelected(jiggle.getProperties().getBoolean("showCursorAmpAsLine"));
      waveMenu.add(cursorAmpLineCheckItem);

      // Check box in menu- toggle show/noshow 0 bias
      //JCheckBoxMenuItem centerLineCueCheckItem = new JCheckBoxMenuItem("Show center lines");
      //centerLineCheckItem.addActionListener(new CenterLineCheckBoxListener());
      //centerLineCheckItem.setSelected(jiggle.getProperties().getBoolean("showCenterLines"));
      //waveMenu.add(centerLineCheckItem);
 
      return waveMenu;
    }


    private JMenu makeCatMapMenu() {
      JMenu catMapMenu = new JMenu("Map...");

      boolean mapInSplit = jiggle.getProperties().getBoolean("mapInSplit");
      String itemText = (jiggle.getProperties().getInt("mapSplitOrientation") == JSplitPane.HORIZONTAL_SPLIT) ?
            "Vertical split" : "Horizontal split";
      ActionListener al = new CatalogMapSplitMenuListener();
      JMenuItem splitItem = addToMenu(itemText, catMapMenu, al, mapInSplit);
      splitItem.setVisible(mapInSplit);

      al = new CatalogMapViewMenuListener(splitItem);
      itemText = (mapInSplit) ?  "Separate window" : "Split with catalog"; 
      splitItem = addToMenu(itemText, catMapMenu, al, true);

      splitItem = addToMenu("Reset map", catMapMenu, al, true);
      splitItem.setToolTipText("Reset map panel by the currently set map properties");
      splitItem.setVisible(true);

      return catMapMenu;
    }

    private JMenu makeEventMenu() { // solutionMenu is class data member
      JMenu solutionMenu = new JMenu("Event", true);
      solutionMenu.setEnabled(false);  // no event loaded yet

      ActionListener al = new EventMenuListener();

      addToMenu("Add magnitude...", solutionMenu, al, "Add new preferred magnitude to event", true);
      addToMenu("Edit comment...", solutionMenu, al, "Add/change event database comment", true);
      addToMenu("Edit origin...", solutionMenu, al, "Set the event location parameters", true);
      addToMenu("Set trial origin by channel", solutionMenu, al, "Set trial origin values to selected channel's P-time and lat,lon", true);
      solutionMenu.addSeparator();

      //JMenu sortMenu = (JMenu) getMenuBarActionItem("Sort...");  // test if sort in MenuBar:
      //if (sortMenu == null) makeSortMenu();
      JMenu sortMenu = makeSortMenu();
      sortMenu.setEnabled(true);
      solutionMenu.add(sortMenu);
      solutionMenu.addSeparator();

      addToMenu("Locate", solutionMenu, al, "with selected solution server", true);
      solutionMenu.addSeparator();

      ActionListener al2 = new MagMenuListener();
      final JiggleProperties props =  jiggle.getProperties();
      String [] magTypes = props.getMagMethodTypes();
      // magType+"Disable" user property controls whether magmethod item is enabled
      // by default since menu shows all magmethods listed in properties file 
      for (int i = 0; i<magTypes.length; i++) {
        addToMenu("Calculate "+magTypes[i], solutionMenu, al2,
                "from a rescan of all waveform timeseries",
                ! props.getBoolean(magTypes[i].toLowerCase()+"Disable") );
      }
      solutionMenu.addSeparator();

      for (int i = 0; i<magTypes.length; i++) {
          addToMenu("Recalculate "+magTypes[i], solutionMenu, al2,
                "from data observations currently in magnitude's panel listing",
                ! props.getBoolean(magTypes[i].toLowerCase()+"Disable") );
      }
      solutionMenu.addSeparator();

      addToMenu("Delete event", solutionMenu, al, "flag event deleted in database", true);
      addToMenu("Undelete event", solutionMenu, al, "flag event undeleted, but db/catalog view are updated only if user saves event", true);
      solutionMenu.addSeparator();

      ActionListener al3 = new PickMenuListener();
      addToMenu("Delete all arrivals", solutionMenu, al3, true);
      addToMenu("Delete all auto arrivals", solutionMenu, al3, true);
      solutionMenu.addSeparator();
      solutionMenu.add(makeStripMenu("arrivals", al3));
      //addToMenu("Unassociate arrivals",  solutionMenu, al3, true);
      solutionMenu.addSeparator();

      ActionListener al4 = new AmpMenuListener();
      addToMenu("Delete all amps", solutionMenu, al4, true);
      addToMenu("Delete all auto amps", solutionMenu, al4, true);
      solutionMenu.addSeparator();
      solutionMenu.add(makeStripMenu("amps", al4));
      //addToMenu("Unassociate amps",  solutionMenu, al4, true);
      solutionMenu.addSeparator();

      ActionListener al5 = new CodaMenuListener();
      addToMenu("Delete all codas", solutionMenu, al5, true);
      addToMenu("Delete all auto codas", solutionMenu, al5, true);
      solutionMenu.addSeparator();
      solutionMenu.add(makeStripMenu("codas", al5));
      //addToMenu("Unassociate codas",  solutionMenu, al5, true);
      solutionMenu.addSeparator();

      ActionListener al6 = new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (jiggle.solLock == null) {
                jiggle.popInfoFrame("Unlock Event", "Is Jiggle event locking DISABLED by property setting?");
                return;
              }
              long oldId = jiggle.solLock.getId();
              jiggle.solLock.setLockingWorks(true);
              jiggle.solLock.setId(0l);
              jiggle.solLock.setLockingWorks(jiggle.solLock.unlock()); // delete the "0" row from jasi_event_lock table
              if (oldId > 0l) {
                jiggle.solLock.setId(oldId);
                boolean tf = jiggle.solLock.lock(); // reset lock, if already set no-op
                JOptionPane.showMessageDialog(jiggle, "Event locking supported : " + jiggle.solLock.isSupported(),
                        "Reset Event Lock", JOptionPane.PLAIN_MESSAGE);
              }
              System.out.println("INFO: reset of event locking " + ((jiggle.solLock.isSupported()) ? " succeeded!" : " failed!"));
          }
      };
      addToMenu("Reset event lock",  solutionMenu, al6, true);
      solutionMenu.addSeparator();
      /* Add this when resolved, if ever ... aww 2015/03/23
      addToMenu("Update bad channel list",  solutionMenu, 
             new ActionListener() {
                 public void actionPerformed(ActionEvent e) {
                     jiggle.updateBadChannelList();
                 }
             }
      , true);
      solutionMenu.addSeparator();
      */

      ActionListener al7 = new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              saveMenuEventData(e);
              final String cmd = e.getActionCommand();
              if (!confirmAction(cmd)) return;

              jiggle.initStatusGraphicsForThread("Event edit...", cmd); // pops status

              org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {

                  boolean  updateTabs = false;
                  String magType = null;

                  public Object construct() { // any GUI graphics in method should invokeLater
                      Solution sol = jiggle.mv.getSelectedSolution();
                      if (sol == null) return null;

                      if (cmd.startsWith("Reset ALL phases deltim values")) {
                          int cnt = sol.phaseList.size();
                          for (int idx=0; idx<cnt; idx++) ((Phase)sol.phaseList.get(idx)).qualityToDeltaTime();
                          return null;
                      }

                      if (cmd.equals("Delete all auto obs")) { // aww 2010/09/16
                          jiggle.mv.addToPhaseUndoList(sol.phaseList.getAutomaticReadings(), true);
                          int cnt = sol.phaseList.eraseAllAutomatic();
                          System.out.println("INFO: count of automatic arrs erased: " + cnt);

                          jiggle.mv.addToAmpUndoList(sol.ampList.getAutomaticReadings(), true);
                          cnt = sol.ampList.eraseAllAutomatic();
                          System.out.println("INFO: count of automatic amps erased: " + cnt);
                              Magnitude mag = sol.getPrefMagOfType("l");
                              if (mag != null) mag.getReadingList().eraseAllAutomatic();

                          jiggle.mv.addToCodaUndoList(sol.codaList.getAutomaticReadings(), true);
                          cnt = sol.codaList.eraseAllAutomatic();
                          System.out.println("INFO: count of automatic coda erased: " + cnt);
                              mag = sol.getPrefMagOfType("d");
                              if (mag != null) mag.getReadingList().eraseAllAutomatic();

                          jiggle.mv.makeLocalLists(); // refresh readings seen in views -aww 2008/02/26
                          updateTabs = true;
                      }
                      else if (cmd.equals("Restore ALL observations") ) {
                          jiggle.mv.undoPhases(true);
                          jiggle.mv.undoAmps(true);
                          jiggle.mv.undoCodas(true);
                          updateTabs = true;
                      }
                      else if (cmd.startsWith("Delete all obs")) {
                          jiggle.mv.addToPhaseUndoList(sol.phaseList, true);
                          int cnt = sol.phaseList.eraseAll();
                          System.out.println("INFO: count of arrs erased: " + cnt);

                          jiggle.mv.addToAmpUndoList(sol.ampList, true);
                          cnt = sol.ampList.eraseAll();
                          System.out.println("INFO: count of amps erased: " + cnt);
                              Magnitude mag = sol.getPrefMagOfType("l");
                              if (mag != null) mag.getReadingList().eraseAll();

                          jiggle.mv.addToCodaUndoList(sol.codaList, true);
                          cnt = sol.codaList.eraseAll();
                          System.out.println("INFO: count of coda erased: " + cnt);
                              mag = sol.getPrefMagOfType("d");
                              if (mag != null) mag.getReadingList().eraseAll();

                          jiggle.mv.makeLocalLists(); // refresh readings seen in views -aww 2008/02/26
                          updateTabs = true;
                      }
                      if (cmd.startsWith("Set bogus") || cmd.endsWith("Set bogus")) {
                          sol.setLatLonZ(new org.trinet.util.gazetteer.LatLonZ(0.,0.,0.));
                          sol.dummyFlag.setValue(1l);
                          sol.setStale(false);
                          Magnitude newMag = Magnitude.create();
                          newMag.assign(sol);
                          magType = "h";
                          newMag.setType(magType);
                          newMag.setDependsOnOrigin(false);
                          newMag.value.setValue(0.);
                          newMag.algorithm.setValue("HAND");
                          sol.setPreferredMagnitude(newMag);
                          Iterator iter = sol.getPrefMags().iterator();
                          Magnitude mag = null;
                          while (iter.hasNext()) {
                              mag = (Magnitude) iter.next();
                              if (mag != newMag) {
                                  mag.setDeleteFlag(true);
                                  mag.setStale(false);
                                  mag.setNeedsCommit(false);
                              }
                          }
                          updateTabs = true;
                      }
                      return null; // could return Integer number
                  }

                  public void finished() { // GUI update here
                      jiggle.resetStatusGraphicsForThread(); // unpops status
                      if (updateTabs) {
                          //jiggle.updateMagTab("n"); // ?? hmmm, not sure why "n" here so try below -aww
                          jiggle.updateMagTab(magType); // try this, 2011/10/13 -aww
                          jiggle.updateLocationTab();
                      }
                      jiggle.updateStatusBarCounts();
                  }

              }; // end of SwingWorker
              sw.start(); // start long task worker thread
          }
      };

      addToMenu("Delete all observations", solutionMenu, al7,
              "Delete all arrivals, amps, and codas", true);
      addToMenu("Delete all auto obs", solutionMenu, al7,
              "Delete all automatic arrivals, amps, and codas", true);
      solutionMenu.addSeparator();

      addToMenu("Set bogus origin, prefmag", solutionMenu, al7,
              "Set lat,lon,z = 0 and set event prefmag Mh = 0, removing all other magnitude associations", true);
      solutionMenu.addSeparator();

      addToMenu("Delete all obs + Set bogus", solutionMenu, al7,
              "Delete all observations and set bogus origin, magnitude (e.g. teleseism with local event attributes)", true);
      solutionMenu.addSeparator();


      JMenu undoMenu = new JMenu("Undo...");
      undoMenu.setToolTipText("Restore deleted observations");
      solutionMenu.add(undoMenu);
      addToMenu("Restore arrivals", undoMenu, al3, "Replace current with picks last deleted", true);
      addToMenu("Restore amps", undoMenu, al4, "Replace current with amps last deleted", true);
      addToMenu("Restore codas", undoMenu, al5, "Replace current with codas last deleted", true);
      addToMenu("Restore ALL observations", undoMenu, al7,
              "Replace current with the last deleted arrivals, amps, and codas", true);
      solutionMenu.addSeparator();

      addToMenu("Reset ALL phases deltim values", solutionMenu, al7,
              "Reset pick uncertainties (deltim) to the mapped hypoinverse weight values", true);
      solutionMenu.addSeparator();

      //if (!jiggle.getProperties().getBoolean("pcsPostingEnabled")) return solutionMenu; //bail 

      parsePostingState( jiggle.getProperties().getProperty("pcsPostingState", "PostProc Jiggle Selected 100 0") );

      // New "hidden" property menu item for testing
      ActionListener al8 = new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (!viewLoaded()) return ;
              Solution sol = jiggle.mv.getSelectedSolution();
              if (sol == null || sol.getId().isNull()) {
                JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle, 
                    "Event id invalid", "Post Id Failed", JOptionPane.ERROR_MESSAGE);
                return;
              }
              long evid = sol.getId().longValue();
              String postingStr = String.format("%s %s %s %d %d", postingGroup, postingSrcTab, postingState, postingRank, postingResult);
              postingStr = (String) JOptionPane.showInputDialog(JiggleMenuBar.this.jiggle,
                  "Group  SrcTab  State  Rank  Result :\n", "Post "+evid+ " for", JOptionPane.PLAIN_MESSAGE, null, null, postingStr);
              doPost(sol, postingStr);
          }
      };
      JMenuItem jmi = addToMenu("Post...", solutionMenu, al8, "Post event to specified PCS state processing", true);
      jmi.setEnabled(jiggle.getProperties().getBoolean("pcsPostingEnabled")); 
      solutionMenu.addSeparator();

      final class PostActionListener implements ActionListener {
          String postItemLabel = null;
          String postItemState = null;
          PostActionListener(String label, String state) {
              this.postItemLabel = label;
              this.postItemState = state;
          }

          public void actionPerformed(ActionEvent evt) {
              if (!viewLoaded()) return;
              Solution sol = jiggle.mv.getSelectedSolution();
              if (sol == null || sol.getId().isNull()) {
                JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle, 
                    "Event id invalid", postItemLabel+" Failed", JOptionPane.ERROR_MESSAGE);
                return;
              }
              doPost(sol, postItemState);
          }
      }
      // Optional extra posts: could list an array of labels in String property
      String[] postItemLabels = jiggle.getProperties().getStringArray("pcsPosting.menuItem.labels");
      //System.out.println("DEBUG postItemLabels length="+postItemLabels.length);
      if (postItemLabels != null && postItemLabels.length > 0) {
          ActionListener al20 = null;
          String postItemState = null;
          for (int idx=0; idx<postItemLabels.length; idx++) {
              postItemState = jiggle.getProperties().getProperty("pcsPosting."+postItemLabels[idx]+".state");
              if (postItemState != null) {
                al20 = new PostActionListener(postItemLabels[idx], postItemState);
                jmi = addToMenu(postItemLabels[idx], solutionMenu, al20, "Post event for " + postItemState, true);
                jmi.setEnabled(jiggle.getProperties().getBoolean("pcsPostingEnabled")); 
                solutionMenu.addSeparator();
              }
          }
          //solutionMenu.addSeparator();
      }
      // XXXX

      jmi = addToMenu("Focal mec...", solutionMenu, new FocalMecListener(), "Show focal mechanism", true);
      jmi.setEnabled((jiggle.getProperties().getProperty("focmec.URL.path") != null)); 
      solutionMenu.addSeparator();

      if (props.getBoolean("rcgMenuOptionEnabled")) {
          ActionListener al9 = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

              if (!viewLoaded()) return;

              WFViewList wfvList = jiggle.mv.wfvList;
              int cnt = wfvList.size();
              if (cnt == 0) return;

              boolean doAll = evt.getActionCommand().startsWith("ALL");
              String opts = (doAll) ? "ALL channels" : "ONLY channels with OBSERVATIONS";
              int answer = JOptionPane.showConfirmDialog(JiggleMenuBar.this.jiggle,
                              "Create waveform requests for " + opts +", missing archived database waveforms?", 
                              "Make RCG Requests", JOptionPane.YES_NO_OPTION);
              if (answer != JOptionPane.OK_OPTION) return;

              Solution sol = jiggle.mv.getSelectedSolution();
              long evid = sol.getId().longValue();
              if (evid <= 0l) {
                JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle, 
                    "Event id invalid", "Make RCG Requests Failed", JOptionPane.ERROR_MESSAGE);
                return;
              }

              boolean doCommit = true;
              CallableStatement cs = null;
              Connection conn = null;

              try {

                conn = DataSource.getConnection();
                if ( conn.isClosed() ) {  // check that valid connection exists
                    System.err.println("ERROR: Jiggle Make RCG Waveform Requests, database connection is closed");
                    return;
                }

                // NOTE: RCID=REQSEQ.NEXTVAL, RequestType='T', PRIORITY=1 would be set inside the called stored function 
                // property for value STAAUTH passed into DB FUNCTION like "SCEDC"
                String rcgAuth = props.getProperty("rcgAuth", EnvironmentInfo.getNetworkCode());
                String rcgStaAuth = props.getProperty("rcgStaAuth", EnvironmentInfo.getNetworkCode());
                String rcgSubSrc = props.getProperty("rcgSubsource", "Jiggle");

                WFView wfv = null;
                ChannelableSetList csList = new ChannelableSetList(wfvList);
                for (int idx=0; idx<cnt; idx++) {
                    wfv = (WFView) wfvList.get(idx);
                    if (wfv != null && (doAll || (wfv.hasPhases() || wfv.hasAmps() || wfv.hasCodas())) ) {
                        //timeSpan = jiggle.mv.getChannelTimeWindowModel().getTimeWindow( wfv.getChannelObj().getHorizontalDistance() );
                        csList.add(wfv);
                    }
                }
                // csList should contain list of channels

                cnt = csList.size();
                int [] results = null;
                TimeSpan timeSpan = null;
                ChannelName cn = null;
                String sql = "{ CALL requestcard.batch_t_request(?,?,?,?,?,?,?,?,?,?,?,?) }";
                cs = conn.prepareCall(sql);
                for (int idx=0; idx<cnt; idx++) {
                        wfv = wfvList.get( (Channelable)csList.get(idx) );
                        timeSpan = wfv.getViewSpan();
                        cn = wfv.getChannelObj().getChannelName();
                        cs.setLong(1, evid);
                        cs.setString(2, rcgAuth);
                        cs.setString(3, rcgSubSrc);
                        cs.setString(4, rcgStaAuth);
                        cs.setString(5, cn.getNet());
                        cs.setString(6, cn.getSta());
                        cs.setString(7, cn.getSeedchan());
                        cs.setString(8, cn.getLocation());
                        cs.setString(9, cn.getChannel());
                        cs.setDouble(10, timeSpan.getStart());
                        cs.setDouble(11, timeSpan.getEnd());
                        cs.setInt(12, 0); // 0 = no commit; 1= commit

                        cs.addBatch();    // and one more statement to the batch

                        if (JasiDatabasePropertyList.debugSQL) {
                            System.out.println("Jiggle Make RCG requests addBatch: requestCard.batch_t_request("+
                                evid+",'"+rcgAuth+"','"+rcgSubSrc+"','"+rcgStaAuth+"','"+
                                cn.getSta()+"','"+cn.getSeedchan()+"','"+cn.getLocationString()+"','"+cn.getChannel()+"',"+
                                TimeSpan.timeToString(timeSpan.getStart())+","+TimeSpan.timeToString(timeSpan.getEnd())+",0)"
                            );
                       }
                }

                results = cs.executeBatch();
                // RH: original logic was to only commit if all were successful, changed it to commit when at least some
                  // were successful.
                doCommit = (results.length > 0);
                  // Commit everything
                  try {
                      if (doCommit) {
                          System.out.println("INFO: Made " + results.length + " new RCG requests for evid: " + evid);
                          conn.commit();
                      }
                      else  {
                          System.out.println("INFO: Making of new RCG requests failed for evid: " + evid);
                          conn.rollback();
                      }
                  } catch (SQLException ex) {
                      ex.printStackTrace();
                  }
                // RH: still interesting to know which one may have failed, report:
                for (int i=0; i<results.length;i++) {
                    if (results[i] <= 0) {
                        //doCommit = false;
                        System.out.println("Jiggle: RequestCard.batch_t_request statement number " + i + " result: "+results[i]);
                    }
                }

              } catch (SQLException ex) {
                ex.printStackTrace();
              }
              finally {
                  try {
                    if (cs != null) cs.close();
                  }
                  catch (SQLException ex) {}
              }



            }
          };

          JMenu rcgMenu = new JMenu("RCG...");
          String opts = "Create WF requests";
          addToMenu("ONLY channel views having a pick,amp, or coda without a db waveform", rcgMenu, al9, opts, true);
          rcgMenu.addSeparator();
          addToMenu("ALL channel views without db waveforms", rcgMenu, al9, opts, true);
          //addToMenu("RCG...", solutionMenu, null, "Create waveform requests for channels missing an event associated waveform in database", true);
          rcgMenu.setToolTipText("Create waveform requests for channels missing an event associated waveform in database");
          solutionMenu.add(rcgMenu);
          solutionMenu.addSeparator();
      }

      return solutionMenu;
    }

    private void doPost(Solution sol, String postingStr) {
          if (postingStr == null) {
              System.out.println("INFO: Posting of evid: " +sol.getId().longValue()+ " aborted.");
              return;
          }
          boolean status = parsePostingState(postingStr); 
          if (!status) {
              JOptionPane.showMessageDialog(jiggle, postingStr, "Syntax Error? Posting Aborted", JOptionPane.ERROR_MESSAGE);
              return;
          }
          sol.postId(postingGroup, postingSrcTab, postingState, postingRank, postingResult);
          System.out.println("INFO: Posted " + sol.getId().longValue() + " for " + postingStr);
    }

    private boolean parsePostingState(String postingStr) {
        boolean status = true;
        try {
            StringTokenizer toke = new StringTokenizer((postingStr == null) ? "" : postingStr, " ,\t");
            if (toke.countTokens() != 5) {
                System.out.println("ERROR: posting state string input requires 5 tokens: Group SrcTab State Rank Result");
                status = false;
            }
            else {
                postingGroup = toke.nextToken();
                postingSrcTab = toke.nextToken();
                postingState = toke.nextToken();
                postingRank = Integer.parseInt(toke.nextToken());
                postingResult = Integer.parseInt(toke.nextToken());
            }
        }
        catch (Exception ex) {
            status = false;
            System.err.println("ERROR: Jiggle Exception parsePosting state: " + ex.getMessage());
        }

        return status;
    }

    private JMenu makeStripMenu(String readingType, ActionListener al) {
      JMenu stripMenu = new JMenu("Strip " + readingType);
      addToMenu("by residual", stripMenu, al, true);
      addToMenu("by distance", stripMenu, al, true);
      return stripMenu;
    }

    private JMenu makeOptionsMenu() {
      JMenu optionsMenu = new JMenu("Model", true);
      ActionListener al = new OptionsMenuListener();
      //addToMenu("Edit WaveServer groups...",  optionsMenu, al, true); // removed, group not init'ed, instead just edit thru Properties menuitem -aww
      optionsMenu.add(makeWSModelMenu());
      optionsMenu.add(makeVelModelMenu());
      addToMenu("Event catalog...",  optionsMenu, al,"Change catalog's event selection model", true);
      return optionsMenu;
    }

    private JMenu makePropertiesMenu() {
      JMenu prefMenu = new JMenu("Properties", true);
      ActionListener al = new PropertiesMenuListener();
      addToMenu("Edit properties...",  prefMenu, al, "Modify property values", true);
      addToMenu("Save properties...",  prefMenu, al, "Write property values to a file", true);
      addToMenu("Load properties...",  prefMenu, al, "Read property values from a file", true);
      addToMenu("Property info", prefMenu, new QuickDumpMenuListener(), "Show property values in message tab", true); 
      return prefMenu;
    }

    private JMenu makeChanCacheMenu() {
      JMenu chanCacheMenu = new JMenu("ChanCache");
      // ChannelList caching options
      ActionListener al = new ChanCacheMenuListener();
      addToMenu("Refresh channel cache",  chanCacheMenu, al, "Run background thread to update channel cache file with most current data in database", true);
      addToMenu("Save channel cache",  chanCacheMenu, al, "Write channel data currently in memory to a cache file", true); // aww in lieu of autosave
      addToMenu("Read channel cache...", chanCacheMenu, al, "Read channel data from a cache file into memory", true);
      chanCacheMenu.addSeparator();
      if (longAL == null) longAL = new LongDumpMenuListener();
      JMenu dmpMenu = new JMenu("Dump...");
      addToMenu("Channel cache waveform info" , dmpMenu, longAL, true);
      addToMenu("Channel cache lat,lon info", dmpMenu, longAL, true);
      addToMenu("Channel cache all info", dmpMenu, longAL, true);
      chanCacheMenu.add(dmpMenu);

      return chanCacheMenu;
    }

    private JMenu makeInfoMenu() {
      JMenu infoMenu = new JMenu("Info", true);
      ActionListener al = new InfoMenuListener();

      addToMenu("Database connection info", infoMenu, al, true);
      addToMenu("Location server info", infoMenu, al, true);
      addToMenu("Waveform server info", infoMenu, al, true);
      addToMenu("Event data info", infoMenu, al, false); // disable, no mv event on init
      addToMenu("Waveform panel scaling info", infoMenu, al, true);
      addToMenu("Memory usage info", infoMenu, al, true);

      return infoMenu;
    }

    private JMenu makeDumpMenu() {
      JMenu dumpMenu = new JMenu("Dump"); 
      ActionListener quickAL = new QuickDumpMenuListener();
      addToMenu("Property info", dumpMenu, quickAL, true);
      addToMenu("Database connection info", dumpMenu, quickAL, true);
      addToMenu("Event locks info", dumpMenu, quickAL, true);
      addToMenu("Velocity model info", dumpMenu, quickAL, true);
      dumpMenu.addSeparator();
      if (longAL == null) longAL = new LongDumpMenuListener();
      ActionListener longAL2 = new LongDumpMenuListener2();
      ActionListener quickAL2 = new QuickDumpMenuListener2();
      JMenu zoomMenu = new JMenu("Picking panel...");
      addToMenu("Waveform info", zoomMenu, quickAL2, true);
      addToMenu("Waveform comment", zoomMenu, quickAL2, true);
      addToMenu("Wavelet(s) info", zoomMenu, quickAL2, true);
      dumpMenu.add(zoomMenu);
      dumpMenu.addSeparator();
      JMenu groupMenu = new JMenu("Group panel...");
      addToMenu("View span details", groupMenu, quickAL2, true);
      groupMenu.addSeparator();
      addToMenu("View span w/segment details", groupMenu, longAL2, true);
      groupMenu.addSeparator();
      addToMenu("View span w/packet details", groupMenu, longAL2, true);
      groupMenu.addSeparator();
      addToMenu("Channels (by dist name only)", groupMenu, quickAL2, true);
      addToMenu("Channels (by dist with dist/az)", groupMenu, quickAL2, true);
      addToMenu("Channels (by dist with travel times)", groupMenu, longAL, true);
      groupMenu.addSeparator();
      addToMenu("Channels (by name NET.STA)", groupMenu, quickAL2, true);
      addToMenu("Channels (by name NET.STA with dist/az)", groupMenu, quickAL2, true);
      groupMenu.addSeparator();
      addToMenu("Channels (by name STA.NET)", groupMenu, quickAL2, true);
      addToMenu("Channels (by name STA.NET with dist/az)", groupMenu, quickAL2, true);
      groupMenu.addSeparator();
      addToMenu("Channels of user selected panels", groupMenu, longAL2, true);
      addToMenu("Channels w/o waveform rows in db", groupMenu, longAL2, true);
      groupMenu.addSeparator();
      addToMenu("Channels with loaded timeseries (dist order)", groupMenu, quickAL2, true);
      addToMenu("Channels with loaded timeseries (name order)", groupMenu, quickAL2, true);
      groupMenu.addSeparator();
      addToMenu("Loaded timeseries amps,bias info", groupMenu, quickAL2, true);
      addToMenu("All timeseries amps,bias info", groupMenu, longAL2, "loads ALL timeseries, takes long time to run!", true);
      addToMenu("Clipped timeseries amps,bias info", groupMenu, longAL2, "loads ALL timeseries , takes long time to run!", true);
      addToMenu("Active panels amps,bias info", groupMenu, longAL2, "loads timeseries in non-hidden panels, takes long time to run!", true);
      groupMenu.addSeparator();
      addToMenu("All timeseries starting noise level", groupMenu, longAL2, "Noise level of starting 5-secs of time series", true);
      addToMenu("All timeseries suspected clipped", groupMenu, longAL2, "loads ALL timeseries, takes long time to run!", true);
      dumpMenu.add(groupMenu);
      dumpMenu.addSeparator();
      addToMenu("Magnitude info", dumpMenu, quickAL2, true);
      addToMenu("Mec info", dumpMenu, quickAL2, true);
      addToMenu("Origin info", dumpMenu, quickAL2, true);
      dumpMenu.addSeparator();
      addToMenu("Alarm history", dumpMenu, quickAL2, true);
      addToMenu("Magnitude history", dumpMenu, quickAL2, true);
      addToMenu("Origin history", dumpMenu, quickAL2, true);
      addToMenu("Mec history", dumpMenu, quickAL2, true);
      dumpMenu.addSeparator();
      //These might be moved into Long category:
      addToMenu("Phase info", dumpMenu, quickAL2, true);
      addToMenu("Amp info", dumpMenu, quickAL2, true);
      addToMenu("Coda info", dumpMenu, quickAL2, true);
      addToMenu("Strong motion info", dumpMenu, longAL2, true);
      dumpMenu.addSeparator();
      addToMenu("Deleted phase info", dumpMenu, quickAL2, true);
      addToMenu("Deleted amp info", dumpMenu, quickAL2, true);
      addToMenu("Deleted coda info", dumpMenu, quickAL2, true);
      addToMenu("All deleted info", dumpMenu, quickAL2, true);
      dumpMenu.addSeparator();

      addToMenu("Picker info", dumpMenu, longAL, true);
      if (jiggle.getProperties().getBoolean("debug")) {
        dumpMenu.addSeparator();
        addToMenu("MasterView info", dumpMenu, longAL, true);
      }
      dumpMenu.addSeparator();
      addToMenu("WF request info", dumpMenu, quickAL2, true);
      dumpMenu.addSeparator();
      JMenu postMenu = new JMenu("Postings in db...");
      addToMenu("Posts for loaded event", postMenu, quickAL2, true);
      addToMenu("Posts for any event", postMenu, longAL, true);
      dumpMenu.add(postMenu);

      return dumpMenu;
    }

    private JMenu makeHelpMenu() {
      JMenu helpMenu = new JMenu("Help", true);
      helpMenu.add(new JMenuItem(new MouseHelpAction()));
      helpMenu.add(new JMenuItem(new PickingHelpAction()));
      helpMenu.add(new JMenuItem(new SetupHelpAction()));
      ActionListener al = new HelpMenuListener();
      addToMenu("Help browser", helpMenu, al, true);
      addToMenu("About", helpMenu, al, true);
      return helpMenu;
    }

  private class RowHeaderCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        if (jiggle.wfScroller != null) {
            if (jiggle.wfScroller.triaxialMode && jiggle.wfScroller.triaxialScrollZoomWithGroup) {
                JOptionPane.showMessageDialog(getTopLevelAncestor(), 
                    "NOTE: Time scrolling synch enabled in group triaxial view mode requires channel header display be turned off\n" +
                    "Disable this option in group panel menu item to show the channel headers on the waveform panels",
                    "Triaxial Group Panel Scroller Time Synch",
                    JOptionPane.PLAIN_MESSAGE);
                item.setSelected(false);
                return;
            }
            jiggle.wfScroller.setShowRowHeader(item.isSelected());
            jiggle.wfScroller.repaint();
        }
        jiggle.getProperties().setProperty("showRowHeaders", item.isSelected()); // moved to outside of if condition block -aww 2011/03/25
    }
  }

  private class PickFlagCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        //jiggle.getProperties().setProperty("showPickFlags", item.isSelected());
        String cmd = (String) menuActionCmd;
        if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null && jiggle.wfScroller.groupPanel.wfpList != null) {
            if (cmd.equals("flags")) {
                jiggle.wfScroller.groupPanel.wfpList.setShowPickFlags(item.isSelected());
            }
            else if (cmd.equals("deltas")) {
                jiggle.wfScroller.groupPanel.wfpList.setShowDeltaTimes(item.isSelected());
            }
            else if (cmd.equals("resids")) {
                jiggle.wfScroller.groupPanel.wfpList.setShowResiduals(item.isSelected());
            }
            jiggle.wfScroller.repaint();
        }

        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
          if (cmd.equals("flags")) {
              jiggle.pickPanel.zwfp.setShowPickFlags(item.isSelected());
          }
          else if (cmd.equals("deltas")) {
              jiggle.pickPanel.zwfp.setShowDeltaTimes(item.isSelected());
          }
          else if (cmd.equals("resids")) {
              jiggle.pickPanel.zwfp.setShowResiduals(item.isSelected());
          }
          jiggle.pickPanel.repaint();
        }

        if (cmd.equals("resids")) jiggle.getProperties().setProperty("showResiduals", item.isSelected());
        else if (cmd.equals("deltas")) jiggle.getProperties().setProperty("showDeltimes", item.isSelected());
    }
  }

  private class PickFlagFontCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();

        boolean tf = item.isSelected();

        if (tf) {
          PickFlag.setBigFont();
          jiggle.getProperties().setProperty("pickFlagFont", "BIG");
        }
        else {
          PickFlag.setSmallFont();
          jiggle.getProperties().setProperty("pickFlagFont", "small");
        }

        if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null && jiggle.wfScroller.groupPanel.wfpList != null) {
            jiggle.wfScroller.repaint();
        }

        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
          jiggle.pickPanel.repaint();
        }

    }
  }

  private class PickFlagColorCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();

        boolean tf = item.isSelected();

        PickFlag.setMarkerColorByPhase = tf;
        jiggle.getProperties().setProperty("pickFlag.colorByPhase", tf);

        if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null && jiggle.wfScroller.groupPanel.wfpList != null) {
            jiggle.wfScroller.repaint();
        }

        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
          jiggle.pickPanel.repaint();
        }

    }
  }

  private class PhaseCueCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        jiggle.getProperties().setProperty("showPhaseCues", item.isSelected());

        if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null && jiggle.wfScroller.groupPanel.wfpList != null) {
            jiggle.wfScroller.groupPanel.wfpList.setShowPhaseCues(item.isSelected());
            jiggle.wfScroller.repaint();
        }

        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
          jiggle.pickPanel.zwfp.setShowPhaseCues(item.isSelected());
          jiggle.pickPanel.repaint();
        }

    }
  }

  private class CursorTimeCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        jiggle.getProperties().setProperty("showCursorTimeAsLine", item.isSelected());
        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
              jiggle.pickPanel.zwfp.setShowCursorTimeAsLine(item.isSelected());
              jiggle.pickPanel.repaint();
        }
    }
  }

  private class CursorAmpCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        jiggle.getProperties().setProperty("showCursorAmpAsLine", item.isSelected());
        if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
              jiggle.pickPanel.zwfp.setShowCursorAmpAsLine(item.isSelected());
              jiggle.pickPanel.repaint();
        }
    }
  }

  private class AlignmentModeListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JMenuItem item = (JMenuItem) e.getSource();
        String cmd = item.getActionCommand();
        try {
          if (cmd.startsWith("Scroll to")) {
              WFScroller.scrollerMode = item.isSelected() ? WFScroller.SCROLL_TO_CENTER : WFScroller.SCROLL_TO_VISIBLE;
              return;
          }
          int mode = -1;
          if ( cmd.equals("T") && item.isSelected() ) {
              mode = 0;
          }
          else if ( cmd.equals("P") && item.isSelected() ) {
              mode = 1;
          }
          else if ( cmd.equals("S") && item.isSelected() ) {
              mode = 2;
          }
          else if ( cmd.startsWith("V ") && item.isSelected() ) {
              String vel = jiggle.getProperties().getProperty("masterViewWaveformAlignVel", "");
              vel = JOptionPane.showInputDialog(jiggle, "Reduction Velocity", vel);
              if (vel != null) {
                double v = Double.parseDouble(vel);
                if (v >= 0.3) { // speed of sound in air 
                  mode = 3;
                  jiggle.getProperties().setProperty("masterViewWaveformAlignVel", v);
                  item.setText("V "+v);
                }
              }
          }
          if (mode >= 0) {
            jiggle.getProperties().setProperty("masterViewWaveformAlignMode", mode); 
            if (jiggle.wfScroller != null) {
              jiggle.mv.setAlignmentVelocity(jiggle.getProperties().getDouble("masterViewWaveformAlignVel"));
              jiggle.mv.setAlignmentMode(mode);
              jiggle.mv.alignViews();
              jiggle.wfScroller.groupPanel.resetPanelBoxSize();
              jiggle.wfScroller.groupPanel.revalidate();
              jiggle.wfScroller.repaint();
            }
          }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
  }

  private class ChannelLabelListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JRadioButtonMenuItem item = (JRadioButtonMenuItem) e.getSource();
        String cmd = item.getActionCommand();
        try {
          int mode = -1;
          if ( cmd.equals("Starting") && item.isSelected() ) {
              mode = 0;
          }
          else if ( cmd.equals("Leading") && item.isSelected() ) {
              mode = 1;
          }
          else if ( cmd.equals("Trailing") && item.isSelected() ) {
              mode = 2;
          }
          else if ( cmd.equals("Both") && item.isSelected() ) {
              mode = 3;
          }
          else if ( cmd.equals("None") && item.isSelected() ) {
              mode = 4;
          }
          if (mode >= 0) {
            jiggle.getProperties().setProperty("channelLabelMode", mode); 
            if (jiggle.wfScroller != null) {
                jiggle.wfScroller.showChannelLabels(mode);
                jiggle.wfScroller.repaint();
            }
          }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
  }

  private class ShowSampleCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
       saveMenuEventData(e);
       JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
       jiggle.getProperties().setProperty("showSamples", item.isSelected());
       if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
            jiggle.pickPanel.zwfp.setShowSamples(item.isSelected());
            jiggle.pickPanel.zwfp.repaint();
       }
    }

  }


  private class ShowTimeScaleCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
       saveMenuEventData(e);
       JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
       boolean tf = item.isSelected();
       if (e.getActionCommand().equals("timescale")) {
           jiggle.getProperties().setProperty("wfpanel.showTimeScale", tf);
           if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null) {
               jiggle.pickPanel.zwfp.setShowTimeScale(tf);
               jiggle.wfScroller.groupPanel.setShowTimeScale(tf);
               jiggle.wfScroller.groupPanel.repaint();
               jiggle.wfScroller.revalidate();
           }
       }
       else {
           jiggle.getProperties().setProperty("wfpanel.showTimeScaleLabel", tf);
           if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
               jiggle.pickPanel.zwfp.setShowTimeLabel(tf);
               jiggle.pickPanel.zwfp.repaint();
               jiggle.wfScroller.groupPanel.setShowTimeLabel(tf);
               jiggle.wfScroller.groupPanel.repaint();
               jiggle.wfScroller.revalidate();
           }
       }
    }

  }

  private class SegmentCheckBoxListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
       saveMenuEventData(e);
       JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
       jiggle.getProperties().setProperty("showSegments", item.isSelected());
       if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
           jiggle.pickPanel.zwfp.setShowSegments(item.isSelected());
           jiggle.pickPanel.zwfp.repaint();
       }
    }
  }

  private class AntialiasCheckBoxListener implements ActionListener {
    // handle a menu selection event
    public void actionPerformed (ActionEvent e) {
      JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
      jiggle.getProperties().setProperty("antialiasWaveform", item.isSelected());
      if (jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
          jiggle.pickPanel.zwfp.setAntialias(item.isSelected());
          jiggle.pickPanel.zwfp.repaint();
      }
    }
  }

  private class FileMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Exit")) {
            if (jiggle.confirmShutDown()) jiggle.stop();
        }
        else if (cmd.equals("Open event by id...")) {
            jiggle.loadIdDialog();
            enableMenuActionItem("Close event");
        }
        else if (cmd.equals("Open catalog selection")) {
            Solution [] solArray = jiggle.catPane.getSelectedSolutions();
            if (solArray != null && solArray.length > 0) {
              jiggle.loadSolution(solArray[0].id.longValue(), jiggle.isNetworkModeLAN()); // false=>WAN, don't check for existance ?
              enableMenuActionItem("Close event");
            }
            else {
             JOptionPane.showMessageDialog(jiggle,
                           "No CATALOG event id selected, must select an id to load",
                           "Load Waveforms", JOptionPane.ERROR_MESSAGE);
            }
        }
        else if (cmd.equals("Close event")) {
            if (jiggle.mv.getSelectedSolution() != null) // first test for solution loaded -aww
                jiggle.remakeGUI();
            disableMenuActionItem("Close event");
        }

    }
  }

  private class SortMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);

        if (jiggle.mv.getSelectedSolution() == null) return; // first test for solution is loaded -aww

        String cmd = e.getActionCommand();
        if (cmd.equals("by distance from selected channel")) {
          jiggle.resortWFViews(jiggle.mv.masterWFViewModel.get().getChannelObj().getLatLonZ()); //aww
        }
        else if (cmd.equals("by distance from epicenter")) {
          jiggle.resortWFViews();
        }
        //"Alphabetically"
        // "north to south"
    }
  }

  private class ViewMenuListener implements ActionListener {
    JMenuItem splitItem;
    public ViewMenuListener(JMenuItem splitItem) {
        this.splitItem = splitItem;
    }
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JMenuItem menuItem = (JMenuItem)(e.getSource());
        String cmd = e.getActionCommand();

        String viewText = null;
        if (cmd.equals("Waveforms in SplitPane"))  {
            if (splitItem != null) {
              splitItem.setEnabled(true);
              splitItem.setVisible(true);
            }
            viewText = "Waveforms in TabPane";
        }
        else  {
            if (splitItem != null) {
              splitItem.setEnabled(false);
              splitItem.setVisible(false);
            }
            viewText = "Waveforms in SplitPane";
        }
        menuItem.setText(viewText);
        jiggle.toggleTabSplit();
    }
  }

  private class CatalogMapViewMenuListener implements ActionListener {
    JMenuItem splitItem;

    public CatalogMapViewMenuListener(JMenuItem splitItem) {
        this.splitItem = splitItem;
    }
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JMenuItem menuItem = (JMenuItem)(e.getSource());
        String cmd = e.getActionCommand();

        boolean splitUpdate = false;
        boolean mapPropertiesUpdate = false;

        if (cmd.equals("Split with catalog"))  {
            if (splitItem != null) {
              splitItem.setEnabled(true);
              splitItem.setVisible(true);
            }
            menuItem.setText("Separate window");
            jiggle.getProperties().setProperty("mapInSplit", true);
            splitUpdate = true;
        }
        else if (cmd.equals("Separate window"))  {
            if (splitItem != null) {
              splitItem.setEnabled(false);
              splitItem.setVisible(false);
            }
            menuItem.setText("Split with catalog");
            jiggle.getProperties().setProperty("mapInSplit", false);
            splitUpdate = true;
        }
        else if (cmd.equals("Reset map")) {
            mapPropertiesUpdate = true;
        }
        if (jiggle.mapPanel != null) jiggle.updateMapFrame(splitUpdate, mapPropertiesUpdate);
    }
  }

  private class CatalogMapSplitMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JMenuItem menuItem = (JMenuItem)(e.getSource());
        String cmd = e.getActionCommand();
        if (cmd.equals("Horizontal split")) {
            jiggle.getProperties().setProperty("mapSplitOrientation", JSplitPane.HORIZONTAL_SPLIT);
            if (jiggle.catMapSplit != null) jiggle.catMapSplit.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            menuItem.setText("Vertical split");  // repaints automatically
        }
        else if (cmd.equals("Vertical split")) {
            jiggle.getProperties().setProperty("mapSplitOrientation", JSplitPane.VERTICAL_SPLIT);
            if (jiggle.catMapSplit != null) jiggle.catMapSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
            menuItem.setText("Horizontal split");    // repaints automatically
        }
    }
  }

  private class MainSplitMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        JMenuItem menuItem = (JMenuItem)(e.getSource());
        String cmd = e.getActionCommand();
        if (cmd.equals("Horizontal split")) {
            jiggle.getProperties().setProperty("mainSplitOrientation", JSplitPane.HORIZONTAL_SPLIT);
            if (jiggle.mainSplit != null) jiggle.mainSplit.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            menuItem.setText("Vertical split");  // repaints automatically
        }
        else if (cmd.equals("Vertical split")) {
            jiggle.getProperties().setProperty("mainSplitOrientation", JSplitPane.VERTICAL_SPLIT);
            if (jiggle.mainSplit != null) jiggle.mainSplit.setOrientation(JSplitPane.VERTICAL_SPLIT);
            menuItem.setText("Horizontal split");    // repaints automatically
        }
    }
  }

  private class ChanCacheMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      saveMenuEventData(e);
      final String cmd = e.getActionCommand();
      jiggle.initStatusGraphicsForThread("Channel Cache", cmd); // pops status
      org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
        public Object construct() { // any GUI graphics in method should invokeLater
          if (cmd.equals("Refresh channel cache")) {
            // lookUp current list and write new cache in background thread
            // could pop a Date Dialog to prompt user to input Channel activity
            // date or just use MasterView.getSelectedSolution().getDateTime() ?
            System.out.println("Refreshing channel cache ...");
            // Below runs in separate thread and takes about 100 secs at scedc
            jiggle.refreshMasterCache();
          }
          else if (cmd.equals("Save channel cache")) {
            SwingUtilities.invokeLater( new Runnable() {
                public void run() {
                    System.out.println("Writing channel cache ...");
                    Cursor c = jiggle.getCursor();
                    jiggle.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    MasterChannelList.get().writeToCache();
                    jiggle.setCursor(c);
                }
            });
          }
          else if (cmd.equals("Read channel cache...")) {
            boolean hadError = true;
            String errorMsg = null;
            try {
              JFileChooser jfc = new JFileChooser(jiggle.getProperties().getUserFilePath());
              String filename = jiggle.getProperties().getUserFileNameFromProperty("channelCacheFilename");
              if (filename != null) jfc.setSelectedFile(new File(filename));

              if (jfc.showDialog(jiggle, "Select") == JFileChooser.APPROVE_OPTION) {

                  File file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();

                  if (file.exists()) {
                      ChannelList cList = ChannelList.readFromCache(filename);
                      if (cList.isEmpty()) {
                        errorMsg = "Channel cache file is empty, or read i/o failed, check message log!";
                      }
                      else {
                          if (jfc.getCurrentDirectory().getAbsolutePath().equals(jiggle.getProperties().getUserFilePath()) ) {
                            filename = jfc.getSelectedFile().getName();
                          }

                          MasterChannelList.set(cList);
                          jiggle.getProperties().setProperty("channelCacheFilename", filename);
                          System.out.println("INFO: read channel cache from file : " + filename);

                          hadError = false; // all ok now
                      }
                  }
                  else {
                      errorMsg = "No-op selected file D.N.E. !";
                  }
              }
              else hadError = false; // all ok now
            }
            catch(Exception ex) {
                ex.printStackTrace();
                errorMsg = "Change of channel cache failed with exception, check message log!";
            }

            if (hadError) {
                JOptionPane.showMessageDialog(jiggle, errorMsg, "Read Channel Cache", JOptionPane.ERROR_MESSAGE);
            }
          }
          return null;
        }
        public void finished() { // GUI update here
          jiggle.resetStatusGraphicsForThread(); // unpops status
        }

      }; // end of SwingWorker

      sw.start(); // start long task worker thread
    }

  }

  private class OptionsMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Event catalog...")) {
            /* // removed 2008/10/20 -aww
            EventSelectionDialog eventSelectionDialog = new EventSelectionDialog((JFrame)jiggle, "Event Selection", true, jiggle.eventProps);
            eventSelectionDialog.setVisible(true); // show the dialog
            // if OK set Jiggle properties to the new selection properties
            if (eventSelectionDialog.getButtonStatus() == JOptionPane.OK_OPTION) {
                jiggle.setEventProperties(eventSelectionDialog.getEventSelectionProperties());
                if (jiggle.getProperties().getBoolean("debug")) 
                    System.out.println("Jiggle DEBUG EventSelectionProperties after reset:\n"+jiggle.eventProps.listToString());
                jiggle.resetCatPanel(); // repopulate the catalog table
            }
            */
            jiggle.catPane.doFilterOption("Edit", jiggle.getEventProperties()); // replaces above 2008/10/20 -aww
        }
        /* Removed since dialog init was changed so that waveserver groups are not setup on startup
        else if (cmd.equals("Edit WaveServer groups...")) {
            WaveServerGroupEditDialog dialog = new WaveServerGroupEditDialog(jiggle.getProperties());
        }
        */
    }
  }

  private class PropertiesMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Edit properties..."))  {
            jiggle.doPreferencesDialog();
        }
        else if (cmd.equals("Save properties...")) {
            jiggle.savePropertiesDialog();
        }
        else if (cmd.equals("Load properties...")) {
            File file = new File(jiggle.getProperties().getUserPropertiesFileName());
            JFileChooser jfc = new JFileChooser(file);
            jfc.setSelectedFile(file);
            //jfc.setFileFilter(new MyFileFilter());
            //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
            
            if (jfc.showOpenDialog(jiggle) == JFileChooser.APPROVE_OPTION) {
                file = jfc.getSelectedFile();
                String filepath = file.getAbsolutePath();
                if (file.exists()) {
                  System.out.println("Jiggle loading properties from file: " + filepath);
                  JiggleProperties jp = new JiggleProperties(filepath, jiggle.getProperties().getDefaultPropertiesFileName());
                  jiggle.setProperties(jp); // changes EventSelectionProperties 
                  jiggle.reset();
                }
                else {
                     JOptionPane.showMessageDialog(jiggle, filepath + " D.N.E. !", 
                           "Load Jiggle Properties", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
  }

    private class MyFileFilter extends javax.swing.filechooser.FileFilter {

        public String getDescription() {
            return ".props";
        }

        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            String str = f.getName();
            int idx = str.lastIndexOf('.');
            String ext = null;
            if (idx > 0 &&  idx < str.length() - 1) {
                ext = str.substring(idx+1).toLowerCase();
            }
            if (ext != null) {
                if (ext.equalsIgnoreCase("props")) return true;
            }
            return false;
        }
    }


  private class WaveformPanelSetupListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Waveform group panel setup...")) {
            GroupPanelConfigDialog dia =
                         new GroupPanelConfigDialog(jiggle, "Waveform Group Panel Setup", true );
            dia.setChannelCount(jiggle.getProperties().getInt("tracesPerPage"));
            dia.setTimeWindow(jiggle.getProperties().getDouble("secsPerPage"));

            dia.setVisible(true);

            if (dia.getButtonStatus() == JOptionPane.OK_OPTION) {
               jiggle.getProperties().setProperty("tracesPerPage", dia.getChannelCount());
               jiggle.getProperties().setProperty("secsPerPage", dia.getTimeWindow());
               // Below is needed to synch new scroller fullTime status in resetGUI with current scroller:
               if (jiggle.wfScroller != null) jiggle.wfScroller.setShowFullTime((dia.getTimeWindow() < 0.));
               jiggle.resetGUI(false); // do not bring waveform tab into focus
            }
        }
    }
  }

  private class InfoMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        // Note MasterView counts over all solutions not just the selected one 
        // is this "pooling" of meaningful or an artifact of earlier design? 
        // Thus no we have no magnitude count list for selected event - aww
        StringBuffer msg = new StringBuffer(512);
        if (cmd.equals("Event data info")) {

            if (!viewLoaded()) return ;

            Solution sol = jiggle.mv.getSelectedSolution();
            msg.append("Event id = ").append(sol.id.isNull() ? "?" : sol.id).append("\n");
            msg.append("Prefor id= ").append(sol.prefor.isNull() ? "?" : sol.prefor).append("\n");
            msg.append("Prefmag id(s) = ");
            if (sol.magnitude != null) {
              msg.append(sol.magnitude.getTypeString());
              msg.append(":");
              msg.append(sol.magnitude.magid.isNull() ? "?" : sol.magnitude.magid);
              Iterator iter = sol.getPrefMags().iterator();
              Magnitude mag = null;
              while (iter.hasNext()) {
                mag = (Magnitude) iter.next();
                if (mag != sol.getPreferredMagnitude()) {
                    msg.append("  ");
                    msg.append(mag.getTypeString());
                    msg.append(":");
                    if (mag.magid.isNull()) msg.append("?");
                    else msg.append(mag.magid.toString());
                }
              }
            }
            msg.append("\n");
            msg.append("Stations = ").append(jiggle.mv.wfvList.getStationCount()).append("\n");
            msg.append("Phases = ").append(sol.getPhaseList().size()).append("\n");
            msg.append("Amps = ").append(sol.getAmpList().size()).append("\n");
            msg.append("Codas = ").append(sol.getCodaList().size()).append("\n");
            msg.append("WaveViews = ").append(jiggle.mv.getWFViewCount()).append("\n");
            msg.append("Waveforms = ").append(jiggle.mv.getWaveformCount()).append("\n");
            msg.append("DBWaveforms = ").append((sol == null) ? 0 : sol.countWaveforms()).append("\n");
            msg.append("Selected view WFSegments = ");
            msg.append(jiggle.mv.getSelectedWFSegmentCount()).append("\n");
            // what good is returning value below, views don't necessarily have wf:
            //msg.append("#Total WFSegments = ").append(jiggle.mv.getWFSegmentCount());
        }
        else if (cmd.equals("Memory usage info")) {
            Runtime runtime = Runtime.getRuntime();
            double max  = 0.;
            //jdk1.4 (double) (runtime.maxMemory()/1000000l); // aww 02/24/2005
            double free  = (double) (runtime.freeMemory()/1000000l);
            double total = (double) (runtime.totalMemory()/1000000l);
            double used = ((double)(total - free) / (double)total ) * 100.0;
            Format f = new Format("%6.1f");
            msg.append("JVM Memory usage: \n");
                msg.append("  max   = ").append(f.form(max)).append(" MB\n");
                msg.append("  total = ").append(f.form(total)).append(" MB\n");
                msg.append("  free  = ").append(f.form(free)).append(" MB\n");
                msg.append("  used  = ").append(f.form(used)).append(" %\n");
        }
        else if (cmd.equals("Database connection info")) {
            msg.append(jiggle.getConnectionInfo());
        } 
        else if (cmd.equals("Location server info")) {
            Object obj = jiggle.solSolverDelegate; 
            if (obj == null) msg.append("None");
            else msg.append(obj.toString());
        } 
        else if (cmd.equals("Waveform server info")) {
            Object obj = jiggle.getProperties().getWaveSource();
            if (obj instanceof SQLDataSourceIF) msg.append(((SQLDataSourceIF)obj).describeConnection());
            else if (obj != null) msg.append(obj.toString());
            else msg.append("None");
        }
        else if (cmd.equals("Waveform panel scaling info")) {
            msg.append("Waveform panel min,max range scaled by ").append(jiggle.getProperties().getProperty("wfpanel.scaleBy","data"));
        }

        JTextArea jta = new JTextArea(msg.toString());
        jta.setBackground(jiggle.getBackground());
        jta.setEditable(false);
        new JTextClipboardPopupMenu(jta);
        jiggle.popInfoFrame(cmd, jta);

    }
  }
  private class PrintMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Whole window")) {
            PrintUtilities.printComponent((Component)jiggle);
        }
        else if (cmd.equals("Zoom & Group") && jiggle.wfSplit != null) {
            PrintUtilities.printComponent((Component)jiggle.wfSplit);
        }
        else if (cmd.equals("Zoom panel") && jiggle.pickPanel != null && jiggle.pickPanel.zwfp != null) {
            PrintUtilities.printComponent((Component)jiggle.pickPanel.zwfp);
        }
        else if (cmd.equals("Group panel") && jiggle.wfScroller != null) {
            PrintUtilities.printComponent((Component)jiggle.wfScroller.getViewport());
        }
//        else if (cmd.equals("Group (all)") && jiggle.wfScroller != null) {
//          PrintUtilities.printComponent((Component)jiggle.wfScroller.groupPanel);
//        }
    }
  }
  private class EventMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        if (cmd.equals("Add magnitude...")) {
            jiggle.editEventParams(EventEditDialog.MagTab);
        }
        else if (cmd.equals("Edit comment...")) {
            jiggle.addComment();
        }
        else if (cmd.equals("Edit origin...")) {
            jiggle.editEventParams(EventEditDialog.LLZTab);
        }
        else if (cmd.equals("Set trial origin by channel")) {
            Solution sol = jiggle.mv.getSelectedSolution();
            if (sol == null) {
                jiggle.popInfoFrame("Set Trial Origin Data", "No selected solution");
                return;
            }
            WFView wfvSel = jiggle.mv.masterWFViewModel.get();
            if (wfvSel == null) {
                jiggle.popInfoFrame("Set Trial Origin Data", "No selected channel view");
                return;
            }
            Channel ch = wfvSel.getChannelObj(); 
            if (!ch.hasLatLonZ()) {
                jiggle.popInfoFrame("Set Trial Origin Data", "Selected channel has no lat, lon");
                return;
            }
            sol.setLatLonZ(ch.getLatLonZ());
            sol.depth.setValue(sol.getDepth()+5.); // approx. geoid is 5km - ch elev km - aww 2015/10/10
            sol.mdepth.setValue(5.); // model depth has no channel elev adjustment - aww 2015/10/10
            sol.dummyFlag.setValue(0l);
            //
            PhaseList phList = (PhaseList) sol.getPhaseList().getAssociatedByChannel(ch);
            boolean havePtime = false;
            boolean haveStime = false;
            Phase ph = null;
            double sTime = 0.;
            double pTime = 0.;
            for (int idx=0; idx<phList.size(); idx++) {
                ph = (Phase) phList.get(idx);
                if (ph.isP()) {
                   pTime = ph.getTime();
                   sol.setTime( pTime - 1.0 ); // P-time
                   havePtime = true;
                }
                if (ph.isS()) {
                   sTime = ph.getTime(); // S-time
                   haveStime = true;
                }
            }
            //
            if (!havePtime) {
                int answer = JOptionPane.showConfirmDialog(JiggleMenuBar.this.jiggle,
                              "No P-time for selected channel, set origin time to view center time?",
                              "Set Trial Origin Data", JOptionPane.YES_NO_OPTION);

                if (answer == JOptionPane.YES_OPTION) {
                    sol.setTime( jiggle.mv.masterWFWindowModel.getTimeSpan().getCenter() ); // center time
                }
            } 
            else if (haveStime) {
               sol.setTime( pTime - (sTime-pTime) );
            }
            //
            jiggle.editEventParams(EventEditDialog.LLZTab);
        }
        else if (cmd.equals("Delete event")) {
            jiggle.deleteCurrentSolution();
        }
        else if (cmd.equals("Undelete event")) {
             Solution sol =  jiggle.mv.getSelectedSolution();
             if ( sol != null && ! sol.isValid()) {
               System.out.println("INFO: Jiggle undeleting event: " + sol.getId().longValue());
               sol.validFlag.setValue(1);
               sol.setDeleteFlag(false);
               /*
               // Don't update same evid solution in CatalogPanel (jiggle.catPane)'s list if any, catalog should show the DB view of data
               if (jiggle.catSolList != null) {
                  int idx = jiggle.catSolList.getIndex(sol);
                  if (idx >= 0) {
                    sol = (Solution) jiggle.catSolList.get(idx);
                    if ( sol != null) {
                      sol.validFlag.setValue(1);
                      sol.setDeleteFlag(false);
                      if (jiggle.catPane != null )jiggle.catPane.update(); // ?? is this needed ??
                    }
                  }
               }
               */
             }
        }
        else if (cmd.equals("Locate")) {
            jiggle.locate();
        }
    }
  }

  private class MagMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        // TODO: split out into an Action the magnitude calc functions which can be
        // shared with ToolBar, an action customized to disable mags via properties 
        saveMenuEventData(e);
        final String cmd = e.getActionCommand();
        int idx = cmd.indexOf(" m"); // needs to be lowercase was "M" , fixed 03/23/2005 -aww
        if (idx < 0) return; // bail if not matching above template -aww

        String magType = cmd.substring(idx+1);  // must have m prefix + subscript
        if (cmd.startsWith("Calc") ) jiggle.calcMagFromWaveforms(magType);
        else jiggle.calcMag(magType);
    }
  }
  private class PickMenuListener implements ActionListener {
    //
    //WorkerStatusFrame wsf = new WorkerStatusFrame();
    //{ wsf.setBeep(jiggle.beep); }
    //
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        final String cmd = e.getActionCommand();
        if (!confirmAction(cmd)) return;

        jiggle.initStatusGraphicsForThread("Pick Removal", cmd); // pops status
        //wsf.pop("Pick removal", cmd, true);

        org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
          // write string to TAB_message if set not null below:
          String str = null;
          public Object construct() { // any GUI graphics in method should invokeLater
            //
            // NOTE Since this method shouldn't do graphic updates in external thread,
            // if the view's WFPanel objects are listeners via ListDataStateListener to reading list
            // changes then to be safe you may want to wrap those graphics update calls like: 
            // SwingUtilities.invokeLater(new Runnable() { public void run() { graphics_call; } });
            //

            if (!viewLoaded()) return null;

            Solution sol = jiggle.mv.getSelectedSolution();
            if (sol == null) return null;

            if (cmd.equals("Delete all arrivals")) {
                //flagged deleted, picks still in list, assigned to sol:
                //    sol.phaseList.deleteAll();
                // removed from list, picks not deleted, assigned to sol:
                //    sol.phaseList.removeAll();
                // removed from list and NOT assigned to sol, but not deleted:
                //    sol.phaseList.unassociateAll();

                jiggle.mv.addToPhaseUndoList(sol.phaseList, true);
                // removed from list and deleted, but still assigned to sol:
                int cnt = sol.phaseList.eraseAll();
                System.out.println("INFO: count of arrs erased: " + cnt);
                jiggle.mv.updatePhaseLists(); // refresh phases seen views -aww 2008/02/26
            }
            else if (cmd.equals("Delete all auto arrivals")) {
                jiggle.mv.addToPhaseUndoList(sol.phaseList.getAutomaticReadings(), true);
                int cnt = sol.phaseList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic arrs erased: " + cnt);
                jiggle.mv.updatePhaseLists(); // refresh phases seen views -aww 2008/02/26
            }
            else if (cmd.equals("Restore deleted arrivals")) {
                // removed from list and NOT assigned to sol, not flagged deleted:
                jiggle.mv.undoPhases(true);
            }
            else if (cmd.equals("by residual")) {
                double cutoff = jiggle.getProperties().getDouble("pickStripValue");
                jiggle.mv.stripPhasesByResidual(cutoff, sol);
            }
            else if (cmd.equals("by distance")) {
                double cutoff = jiggle.getProperties().getDouble("pickStripDistance");
                jiggle.mv.stripPhasesByDistance(cutoff, sol);
            }
            return null; // could return Integer number
          }
          public void finished() { // GUI update here
            //wsf.unpop();
            jiggle.resetStatusGraphicsForThread(); // unpops status
            jiggle.updateStatusBarCounts();
          }

        }; // end of SwingWorker
        sw.start(); // start long task worker thread
    }
  }

  private boolean confirmAction(String cmd) {
    if ( jiggle.getProperties().isSpecified("confirmMenuDeleteAction") &&
         (jiggle.getProperties().getBoolean("confirmMenuDeleteAction") == false) ) return true;

    return (JOptionPane.showConfirmDialog(jiggle,cmd,"Confirm Menu Action",JOptionPane.YES_NO_OPTION)
            == JOptionPane.YES_OPTION);
  }

  private class AmpMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        final String cmd = e.getActionCommand();
        if (!confirmAction(cmd)) return;

        jiggle.initStatusGraphicsForThread("Amp Removal", cmd); // pops status

        org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
          // write string to TAB_message if set not null below:
          String str = null;
          public Object construct() { // any GUI graphics in method should invokeLater

            if (!viewLoaded()) return null;

            Solution sol = jiggle.mv.getSelectedSolution();
            if (sol == null) return null;

            if (cmd.equals("Delete all amps")) {
                // removed from list and deleted, but still assigned to sol:
                // Like ActiveWFPanel need to get wfv ampList members assoc with sol and erase them
                jiggle.mv.addToAmpUndoList(sol.ampList, true);
                int cnt = sol.ampList.eraseAll();
                System.out.println("INFO: count of amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAll();
                jiggle.mv.updateAmpLists(); // refresh amps seen views -aww 2008/02/26
            }
            else if (cmd.equals("Delete all auto amps")) {
                jiggle.mv.addToAmpUndoList(sol.ampList.getAutomaticReadings(), true);
                int cnt = sol.ampList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic amps erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("l");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                jiggle.mv.updateAmpLists(); // refresh amps seen views -aww 2008/02/26
            }
            else if (cmd.equals("Restore deleted amps")) {
                // removed from list and NOT assigned to sol, not flagged deleted:
                jiggle.mv.undoAmps(true);
            }
            /*
            else if (cmd.equals("Unassociate amps")) {
                // removed from list and NOT assigned to sol, not flagged deleted:
                sol.ampList.unassociateAll();
            }
            */
            else if (cmd.equals("by residual")) {
                double cutoff = jiggle.getProperties().getDouble("ampStripValue");
                jiggle.mv.stripAmpsByResidual(cutoff, sol);
            }
            else if (cmd.equals("by distance")) {
                double cutoff = jiggle.getProperties().getDouble("ampStripDistance");
                jiggle.mv.stripAmpsByDistance(cutoff, sol);
            }

            return null; // could return Integer number
          }
          public void finished() { // GUI update here
            jiggle.resetStatusGraphicsForThread(); // unpops status
            jiggle.updateStatusBarCounts();
          }

        }; // end of SwingWorker
        sw.start(); // start long task worker thread
    }
  }

  private class CodaMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        final String cmd = e.getActionCommand();
        if (!confirmAction(cmd)) return;

        jiggle.initStatusGraphicsForThread("Coda Removal", cmd); // pops status
        //wsf.pop("Coda removal", cmd, true);

        org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
          // write string to TAB_message if set not null below:
          String str = null;
          public Object construct() { // any GUI graphics in method should invokeLater

            if (!viewLoaded()) return null;

            Solution sol = jiggle.mv.getSelectedSolution();
            if (sol == null) return null;

            if (cmd.equals("Delete all codas")) {
                // removed from list and deleted, but still assigned to sol:
                // Like ActiveWFPanel need to get wfv ampList members assoc with sol and erase them
                jiggle.mv.addToCodaUndoList(sol.codaList, true);
                int cnt = sol.codaList.eraseAll();
                System.out.println("INFO: count of coda erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAll();
                jiggle.mv.updateCodaLists(); // refresh coda seen in views -aww 2008/02/26
            }
            else if (cmd.equals("Delete all auto codas")) {
                jiggle.mv.addToCodaUndoList(sol.codaList.getAutomaticReadings(), true);
                int cnt = sol.codaList.eraseAllAutomatic();
                System.out.println("INFO: count of automatic coda erased: " + cnt);
                    Magnitude mag = sol.getPrefMagOfType("d");
                    if (mag != null) mag.getReadingList().eraseAllAutomatic();
                jiggle.mv.updateCodaLists(); // refresh coda seen in views -aww 2008/02/26
            }
            else if (cmd.equals("Restore deleted codas")) {
                // removed from list and NOT assigned to sol, not flagged deleted:
                jiggle.mv.undoCodas(true);
            }
            /*
            else if (cmd.equals("Unassociate codas")) {
                // removed from list and NOT assigned to sol, not flagged deleted:
                sol.codaList.unassociateAll();
            }
            */
            else if (cmd.equals("by residual")) {
                double cutoff = jiggle.getProperties().getDouble("codaStripValue");
                jiggle.mv.stripCodasByResidual(cutoff, sol);
            }
            else if (cmd.equals("by distance")) {
                double cutoff = jiggle.getProperties().getDouble("codaStripDistance");
                jiggle.mv.stripCodasByDistance(cutoff, sol);
            }

            return null; // could return Integer number
          }
          public void finished() { // GUI update here
            jiggle.resetStatusGraphicsForThread(); // unpops status
            jiggle.updateStatusBarCounts();
          }

        }; // end of SwingWorker
        sw.start(); // start long task worker thread
    }
  }

  private class QuickDumpMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        String str = null;
        // write string to TAB_message if set not null below:
        if (cmd.equals("Property info")) { // quick
            StringBuffer sb = new StringBuffer(8192);
            sb.append("------- JiggleProperties ---- File =");
            sb.append( jiggle.getProperties().getUserPropertiesFileName() );
            sb.append("\n").append( jiggle.getProperties().listToString() );
            sb.append("\n------- EventProperties  ---- File =");
            sb.append( jiggle.eventProps.getUserPropertiesFileName() );
            sb.append("\n").append( jiggle.eventProps.listToString() );
            if (jiggle.solSolverDelegate != null) {
                sb.append("\n------- Magnitude Engine Properties  -----\n");
                sb.append(jiggle.solSolverDelegate.magEnginePropertiesString());
                sb.append("\n------- Magnitude Method Properties -----\n");
                sb.append(jiggle.solSolverDelegate.magMethodPropertiesString());
            }
            sb.append("\n------- EnvironmentInfo ----");
            sb.append("\n").append( EnvironmentInfo.getString() );
            str = sb.toString();

        }
        else if (cmd.equals("Event locks info")) { // quick
            if (jiggle.solLock == null || ! jiggle.solLock.isSupported()) {
              str = "Event locking is not supported by the current data source.";
            } else {
              ArrayList list = (ArrayList) jiggle.solLock.getAllLocks();

              if (list == null) {
                str = "There are no active event locks.";
              } else {
                StringBuffer sb = new StringBuffer(512);
                sb.append("----- Currently lock events = ").append(list.size()).append(" ------\n");
                sb.append(SolutionLock.getHeaderString()).append("\n");
                for (int i = 0; i< list.size(); i++) {
                  sb.append(((SolutionLock)list.get(i)).toFormattedString()).append("\n");
                }
                str = sb.toString();
              }
            }
        }
        else if (cmd.equals("Database connection info")) { // quick
            str = jiggle.getConnectionInfo();
        }
        else if (cmd.equals("Velocity model info")) { // quick
            StringBuffer sb = new StringBuffer(1024);
            sb.append("Current traveltime velocity model:\n");
            sb.append(TravelTime.getInstance().getModel().toString());
            sb.append("----------------------------------\n");
            sb.append("Available velocity models:\n");
            sb.append(jiggle.getProperties().getVelocityModelList().toString());
            str = sb.toString();
        }
        else if (cmd.equals("Channel time window info")) { // quick
            str = null;
            ChannelableList cList = jiggle.mv.getChannelTimeWindowModel().getChannelTimeWindowList();
            if (cList != null) str =  cList.toString();
            if (str == null || str.length() == 0) str = "No channel time windows available.";
        }

        if (str != null) jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true);
    }
  }

  private class QuickDumpMenuListener2 implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        String str = null;

        if (!viewLoaded()) return;

        if (cmd.equals("Magnitude info") || cmd.equals("Magnitude history")) { // quick
            SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
            if (solTN1 == null) {
              jiggle.updateMagTab();
              jiggle.selectTab(Jiggle.TAB_MAGNITUDE);
              return;
            }

            ArrayList mList =  new ArrayList(10);
            mList = solTN1.getMagList();
            if (mList.size() == 0) {
                str = "No magnitudes found for event " + solTN1.getId().longValue() + "\n";
            }
            else {
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                sb.append("Current preferred magnitudes by type in event loaded in viewer:\n");
                formatMagnitude(sb, mList, solTN1, true);
                sb.append("\nDatabase preferred magnitudes by type:\n");
                Magnitude mag = Magnitude.create();
                mList = (ArrayList) mag.getPrefMagOfTypeBySolution(solTN1); 
                formatMagnitude(sb, mList, solTN1, false);
                if (cmd.endsWith("history")) {
                    sb.append("\nDatabase magnitude history:\n");
                    mList = (ArrayList) Magnitude.create().getAllBySolution(solTN1);
                    formatMagnitude(sb, mList, solTN1, false);
                }
                str = sb.toString();
                System.out.println(str);
            }
        }
        else if (cmd.equals("Loaded timeseries amps,bias info")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFView wfv [] =  jiggle.mv.wfvList.getArray();
              StringBuffer sb = new StringBuffer(132*wfv.length+132);
              AbstractWaveform wf = null;
              int count = 0;
              sb.append("                                                                      Segs            Clip\n");
              sb.append("Channel              StartTime               EndTime                  |  Samps Units  |         MinAmp");
              sb.append("         MaxAmp           Bias    SDist\n"); 
              final  Format f8 = new Format("%8.1f");
              for (int i=0; i < wfv.length; i++) {
                  wf = (AbstractWaveform) wfv[i].getWaveform();
                  if (wf != null && wf.hasTimeSeries()) {
                      sb.append(wf.toAmpInfoString()).append(" ").append(f8.form(wfv[i].getDistance())).append("\n");
                      count++;
                  }
              }
              sb.append("#Total loaded waveforms = ").append(count).append("\n");
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No waveforms.";
        }
        else if (cmd.startsWith("Channels (by dist")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFView wfv [] =  jiggle.mv.wfvList.getArray();
              StringBuffer sb = new StringBuffer(132*wfv.length+132);
              int count = 0;
              for (int i=0; i < wfv.length; i++) {
                  sb.append(String.format("%-16s", wfv[i].getChannelObj().toDelimitedSeedNameString(".")));
                  if (cmd.endsWith("with dist/az)")) sb.append(String.format(" %6.1f %3.0f",
                          wfv[i].getChannelObj().getDistance(),wfv[i].getChannelObj().getAzimuth()));
                  sb.append("\n");
                  count++;
              }
              sb.append("#Total channels = ").append(count).append("\n");
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No views with channels.";
        }
        else if (cmd.startsWith("Channels (by name")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFViewList wList =  new WFViewList(jiggle.mv.wfvList);
              if (cmd.indexOf("STA.NET") < 0) wList.sortByStation();
              else wList.sortByStation2();
              WFView[] wfv  = wList.getArray();
              StringBuffer sb = new StringBuffer(132*wfv.length+132);
              int count = 0;
              for (int i=0; i < wfv.length; i++) {
                  sb.append(String.format("%-16s", wfv[i].getChannelObj().toDelimitedSeedNameString(".")));
                  if (cmd.endsWith("with dist/az)")) sb.append(String.format(" %6.1f %3.0f",
                          wfv[i].getChannelObj().getDistance(),wfv[i].getChannelObj().getAzimuth()));
                  sb.append("\n");
                  count++;
              }
              sb.append("#Total channels = ").append(count).append("\n");
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No views with channels.";
        }
        else if (cmd.equals("Channels with loaded timeseries (dist order)")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFView wfv [] =  jiggle.mv.wfvList.getArray();
              StringBuffer sb = new StringBuffer(132*wfv.length+132);
              int count = 0;
              for (int i=0; i < wfv.length; i++) {
                  if (wfv[i].hasTimeSeries() ) {
                      sb.append(wfv[i].getChannelObj().toDelimitedSeedNameString(".")).append("\n");
                      count++;
                  }
              }
              sb.append("#Total channels with loaded waveforms = ").append(count).append("\n");
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No channels with waveform samples.";
        }
        else if (cmd.equals("Channels with loaded timeseries (name order)")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFView wfv [] =  jiggle.mv.wfvList.getArray();
              ArrayList aList = new ArrayList(wfv.length);
              StringBuffer sb = new StringBuffer(132*wfv.length+132);
              int count = 0;
              for (int i=0; i < wfv.length; i++) {
                  if (wfv[i].hasTimeSeries() ) {
                      aList.add(wfv[i].getChannelObj());
                      count++;
                  }
              }
              Collections.sort(aList, new ChannelNameSorter());
              for (int i=0; i < count; i++) {
                  sb.append(((Channel)aList.get(i)).toDelimitedSeedNameString(".")).append("\n");
              }
              sb.append("#Total channels with loaded waveforms = ").append(count).append("\n");
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No channels with waveform samples.";
        }
        else if (cmd.equals("View span details")) {
            str = null;
            if (jiggle.mv.wfvList != null) {
              WFView wfv [] =  jiggle.mv.wfvList.getArray();
              StringBuffer sb = new StringBuffer(132*wfv.length+80);
              sb.append("#Total panels = " + wfv.length + "\n"); 
              for (int i=0; i < wfv.length; i++) {
                  sb.append(wfv[i].dumpToString()).append("\n");
              }
              str = sb.toString();
            }
            if (str == null || str.length() == 0) str = "No waveform panel list.";
        }
        else if (cmd.equals("Waveform comment")) { // quick
            WFView wfv = jiggle.mv.masterWFViewModel.get();
            str = "No waveform";
            if (wfv != null) {
                str = jiggle.mv.masterWFViewModel.get().getComment();
                if (str == null || str.length() == 0) str = "No comment";
            }
        }
        else if (cmd.equals("Waveform info")) { // quick
            WFView wfv = jiggle.mv.masterWFViewModel.get();
            if (wfv != null) {
              StringBuffer sb = new StringBuffer(2048);
              sb.append(wfv.toString()).append("\n");
              Waveform wf = wfv.getWaveform();
              if (wf != null) {
                  sb.append(wf.toString());

                  TimeSpan [] tsArray = wf.getPacketSpans();
                  sb.append("\nPacket spans:\n");
                  for (int idx = 0; idx < tsArray.length; idx++) {
                      sb.append( tsArray[idx].toEpochTimeString() ).append(" ").append( tsArray[idx].toString() ).append("\n");
                  }

                  WFSegment [] wfsArray = wf.getArray();
                  sb.append("\nSegment spans:\n");
                  for (int idx = 0; idx < wfsArray.length; idx++) {
                      sb.append( wfsArray[idx].getTimeSpan().toEpochTimeString() ).append(" ").append( wfsArray[idx].toString() ).append("\n");
                  }

              }
              Waveform wf2 = (jiggle.pickPanel != null) ? jiggle.pickPanel.zwfp.getWf() : null;
              if ( wf2 != null && wf2 != wf ) {
                  sb.append("\nFiltered waveform info:\n");
                  sb.append(wf2.toString());
              }
              str = sb.toString();
            }
            else str = "No waveform view.";
        }
        else if (cmd.equals("Wavelet(s) info")) { // quick
            WFView wfv = jiggle.mv.masterWFViewModel.get();
            if (wfv != null) {
              Waveform wf = wfv.getWaveform();
              // Note the segList for each wavelet in group is clear when group is loaded, ie. wavelet segList.size() == 0
              if (wf != null) {
                  StringBuffer sb = new StringBuffer(2048);
                  sb.append("\nWaveform view wavelet info:\n");
                  sb.append(((WaveletGroup)wf).toWaveletString());
                  Waveform wf2 = (jiggle.pickPanel != null) ? jiggle.pickPanel.zwfp.getWf() : null;
                  if ( wf2 != null && wf2 != wf ) {
                      sb.append("\nFiltered waveform wavelet info:\n");
                      sb.append(((WaveletGroup)wf2).toWaveletString());
                  }
                  str = sb.toString();
              }
              else str = "No wavelet info.";
            }
            else str = "No waveform view.";
        }
        else if ( cmd.equals("Posts for loaded event") ) {

             SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
             if (solTN1 == null) str =  "No event selection in masterview.";
             else { 
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                sb.append("Evid            Group           Source          State           Rank   Result  LoadDate\n");
                long id = 0l;
                org.trinet.pcs.ProcessControl.setConnection(DataSource.getConnection());
                id = solTN1.getId().longValue();
                org.trinet.pcs.StateRow [] sra = org.trinet.pcs.StateRow.getInDateOrder( solTN1.getId().longValue(), true );
                if (sra != null && sra.length > 0) {
                   for (int jdx = 0; jdx<sra.length; jdx++) {
                       sb.append(sra[jdx].toOutputString(true));
                       sb.append("\n");
                   }
                }
                else sb.append(id).append(" has no posts in database\n");

                str = sb.toString();
                System.out.println(str);
             }
        } 
        else if ( cmd.equals("Alarm history") ) {
             SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
             if (solTN1 == null) str =  "No event selection in masterview.";
             else { 
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                long id = solTN1.getId().longValue();
                sb.append( SolutionTN.getAlarmHistory( id ) );
                sb.append("\n");
                str = sb.toString();
                System.out.println(str);
             }
        } 
        else if ( cmd.equals("WF request info") ) { // quick
             SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
             if (solTN1 == null) str =  "No event selection in masterview.";
             else { 
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                long id = solTN1.getId().longValue();
                if ( DataSource.tableExists("REQUEST_CARD") ) {
                  RequestCardTN [] rc = 
                      RequestCardTN.fetchRequestsOrderBy( id, "substr(TO_CHAR(LDDATE),1,16) desc, PCSTORED,RETRY,NET,STA,SEEDCHAN,LOCATION" );
                  if ( rc == null || rc.length == 0) sb.append("0 request_card rows found for evid " + id);
                  else {
                    sb.append(rc.length + " requests sorted by LdDate(YYYY-MM-DD HH:MM) descending PCS,Retry,NET.STA.CHN.LOC ascending\n");
                    sb.append(RequestCardTN.getStringHeader());
                    for (int idx=0; idx < rc.length; idx++) {
                        sb.append(rc[idx].toString()).append("\n");
                    }
                  }
                }
                else {
                    sb.append("REQUEST_CARD table is not accessible\n");
                }
                sb.append("\n");
                str = sb.toString();
                System.out.println(str);
             }
        }
        else if ( cmd.equals("Mec info") || cmd.equals("Mec history") ) { // quick
             SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
             if (solTN1 == null) str =  "No event selection in masterview.";
             else { 
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                long id = solTN1.getId().longValue();
                sb.append("Database preferred of type:\n"); 
                sb.append( SolutionTN.getPrefMecDesc( id, solTN1.getPrefmecValue(), false, true ) );
                sb.append("\n");
                if (cmd.endsWith("history")) {
                  sb.append("Database mec history:\n"); 
                  sb.append( SolutionTN.getPrefMecDesc( id, solTN1.getPrefmecValue(), true, false ) );
                }
                str = sb.toString();
                System.out.println(str);
             }
        }
        else if ( cmd.equals("Origin info") || cmd.equals("Origin history") ) { // quick
             SolutionTN solTN1 = (SolutionTN) jiggle.mv.getSelectedSolution();
             if (solTN1 == null) str =  "No event selection in masterview.";
             else { 
                StringBuilder sb = new StringBuilder(2560); // one mongo string
                ArrayList aList = new ArrayList(10);
                //sb.append("Currently loaded event:\n");
                sb.append("Current origin value of event loaded in viewer:\n");
                aList.add(solTN1);
                formatSolution(sb, aList, solTN1, true);
                sb.append("\nDatabase preferred origins by type:\n");
                if ( DataSource.tableExists("EVENTPREFOR") ) {
                    aList = (ArrayList) solTN1.getPreforOfTypeById(solTN1.getId().longValue()); 
                    formatSolution(sb, aList, solTN1, false);
                }
                else {
                    sb.append("  EVENTPREFOR table is not accessible\n");
                }
                if (cmd.endsWith("history")) {
                  sb.append("\nDatabase origin history:\n");
                  aList = (ArrayList) solTN1.getAllById(solTN1.getId().longValue());
                  formatSolution(sb, aList, solTN1, false);
                }
                else {
                }

                str = sb.toString();
                System.out.println(str);
             }
        }

        else if (cmd.equals("Phase info")) { // medium time
             str = jiggle.mv.phasesToString();    // one mongo string
             if (str == null || str.length() == 0) str = "No phases.";
        }
        else if (cmd.equals("Amp info")) {
            str = jiggle.mv.ampsToString();
            if (str == null || str.length() == 0) str = "No amps.";
        }
        else if (cmd.equals("Coda info")) {
            str = jiggle.mv.codasToString();
            if (str == null || str.length() == 0) str = "No codas.";
        }
        else if (cmd.equals("Deleted phase info")) { // medium time
             if (jiggle.mv.phUndoList.size() == 0) str = "No deleted phases.";
             else str = jiggle.mv.phUndoList.toNeatString(true);    // one mongo string
        }
        else if (cmd.equals("Deleted amp info")) { // medium time
             if (jiggle.mv.amUndoList.size() == 0) str = "No deleted amps.";
             else str = jiggle.mv.amUndoList.toNeatString(true);    // one mongo string
        }
        else if (cmd.equals("Deleted coda info")) { // medium time
             if (jiggle.mv.coUndoList.size() == 0) str = "No deleted codas.";
             else str = jiggle.mv.coUndoList.toNeatString(true);    // one mongo string
        }
        else if (cmd.equals("All deleted info")) { // medium time ? one large string
             int cnt = (jiggle.mv.phUndoList.size() + jiggle.mv.amUndoList.size() + jiggle.mv.coUndoList.size());
             StringBuilder sb = new StringBuilder(132*(cnt+3));
             if (jiggle.mv.phUndoList.size() == 0) sb.append("No deleted phases.\n\n");
             else sb.append(jiggle.mv.phUndoList.toNeatString(true)).append("\n");
             if (jiggle.mv.amUndoList.size() == 0) sb.append("No deleted amps.\n\n");
             else sb.append(jiggle.mv.amUndoList.toNeatString(true)).append("\n");
             if (jiggle.mv.coUndoList.size() == 0) sb.append("No deleted codas.\n\n");
             else sb.append(jiggle.mv.coUndoList.toNeatString(true)).append("\n");
             sb.append("#Total deleted (undo) count: ").append(cnt).append("\n");  
             str = sb.toString();
        }
        if (str != null) jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true);
    }

    public void formatMagnitude(StringBuilder sb, ArrayList mList, SolutionTN mvSol, boolean appendKey) {
                if (mvSol.getNeedsCommit()) sb.append("NOTICE: Uncommitted changes exist for selected event!\n"); 
                sb.append("Evid             Mag  Type Source   Algorithm       Auth   #Sta #Chl Error  ");
                sb.append("Gap Dist  Qual Who    LdDate                         Orid/Magid           DJOE\n");
                Format df7 = new Format("%5.2f");
                Format sf8 = new Format("%-8.8s");
                Format sf15 = new Format("%-15.15s");
                Format sf4 = new Format("%-6.6s");
                Format df4 = new Format("%4d");
                MagnitudeTN magTN = null;
                String oldType=null;
                String newType=null;
                for (int idx=0; idx<mList.size(); idx++) {
                    magTN = (MagnitudeTN) mList.get(idx);

                    newType = magTN.getTypeString();
                    if (oldType != null && !oldType.equals(newType)) {
                        sb.append("\n");
                    }
                    oldType=newType;

                    sb.append(sf15.form(String.valueOf(mvSol.getEvidValue()))).append(" ");
                    sb.append(df7.form(magTN.getMagValue())).append(" ");
                    sb.append(String.format("%-4.4s", newType)).append(" ");
                    sb.append(sf8.form(magTN.getSource())).append(" ");
                    sb.append(sf15.form(magTN.algorithm.toString())).append(" ");
                    sb.append(String.format("%-4.4s", magTN.getAuthority())).append("   ");
                    sb.append(df4.form(magTN.getStationsUsed())).append(" ");
                    sb.append(df4.form(magTN.getReadingsUsed())).append(" ");
                    sb.append(df7.form(magTN.getResidual())).append(" ");
                    sb.append(df4.form(Math.round(magTN.getAzimuth()))).append(" ");
                    sb.append(df4.form(Math.round(magTN.getDistance()))).append(" ");
                    sb.append(df7.form(magTN.quality.doubleValue())).append(" ");
                    sb.append(sf4.form(magTN.getWho().toString())).append(" ");
                    sb.append(magTN.getTimeStamp().toDateString().substring(0,19)).append(" ");
                    sb.append(String.format("%15s", magTN.getOridValue()));
                    sb.append( (!mvSol.originHasChanged() && mvSol.getPreforValue() == magTN.getOridValue() ) ? "+" : "-" );
                    sb.append(String.format("%-15s", magTN.getMagidValue())).append(" ");
                    sb.append( magTN.isLastPrefmagId() ? "+" : "-" );
                    sb.append( magTN.isPrefmagOfType() ? "+" : "-" );
                    sb.append( magTN.isOriginPrefmagId() ? "+" : "-" );
                    sb.append( magTN.isEventPrefmag() ? "+" : "-" );
                    sb.append("\n");
                }
                sb.append("\n");
                if (appendKey) {
                  sb.append("Between Orid Magid:\n");
                  sb.append(" + = magnitude orid is Jiggle event prefor\n");
                  sb.append(" - = magnitude orid is not Jiggle event prefor\n");
                  sb.append("DJOE key:\n");
                  sb.append("D=+ magid is prefmag of magtype in Db\n");
                  sb.append("J=+ magid is prefmag of magtype in Jiggle\n");
                  sb.append("O=+ magid is prefmag of its associated origin in Db\n");
                  sb.append("E=+ magid is prefmag of event in Jiggle\n");
                  sb.append("\n");
                }
    }
    public void formatSolution(StringBuilder sb, ArrayList sList, SolutionTN mvSol, boolean appendKey) {
                if (mvSol.getNeedsCommit()) sb.append("NOTICE: Uncommitted changes exist for selected event!\n"); 
                sb.append("DJOE ");
                sb.append(Solution.getNeatStringHeader());
                sb.append(" ");
                sb.append(Solution.getShortErrorStringHeader());
                sb.append(" ");
                sb.append("ZHT");
                sb.append(" ");
                sb.append("Load Date          ");
                sb.append(" ");
                sb.append("           Orid/Prefmag");
                sb.append("\n");
                SolutionTN solTN = null;
                String newType = null;
                String oldType = null;
                for (int idx=0; idx<sList.size(); idx++) {
                        solTN = (SolutionTN) sList.get(idx);

                        newType =  solTN.getOriginType();
                        if (oldType != null && !oldType.equals(newType)) {
                            sb.append("\n");
                        }
                        oldType = newType;

                        sb.append( (solTN.getPreforValue() == solTN.getOridValue()) ? "+" : "-" );
                        sb.append( (!mvSol.originHasChanged() && mvSol.getPreforValue() == solTN.getOridValue()) ? "+" : "-" );
                        sb.append(
                            (mvSol.magnitude != null &&  solTN.magnitude != null && !mvSol.hasChanged() && !mvSol.magnitude.hasChanged() && 
                            ((MagnitudeTN)mvSol.magnitude).getOridValue() == ((MagnitudeTN)solTN.magnitude).getOridValue() ) ? "+" : "-" 
                        );
                        sb.append(
                            (mvSol.magnitude != null &&  solTN.magnitude != null && !mvSol.hasChanged() && !mvSol.magnitude.hasChanged() && 
                            ((MagnitudeTN)mvSol.magnitude).getMagidValue() == ((MagnitudeTN)solTN.magnitude).getMagidValue() ) ? "+" : "-" 
                        );
                        sb.append(" ");
                        sb.append(solTN.toNeatString());
                        sb.append(" ");
                        sb.append(solTN.toShortErrorString());
                        sb.append(" ");
                        sb.append(solTN.depthFixed.toString().substring(0,1).toUpperCase());
                        sb.append(solTN.locationFixed.toString().substring(0,1).toUpperCase());
                        sb.append(solTN.timeFixed.toString().substring(0,1).toUpperCase());
                        sb.append(" ");
                        sb.append(solTN.getTimeStamp().toDateString().substring(0,19));
                        sb.append(" ");
                        sb.append(String.format("%15s", solTN.getOridValue()));
                        sb.append("/");
                        sb.append(String.format("%-15s", solTN.getPrefmagValue())).append(" ");
                        sb.append("\n");
                }
                sb.append("\n");
                if (appendKey) {
                  sb.append("DJOE key:\n");
                  sb.append("D=+ orid is event prefor in Db\n");
                  sb.append("J=+ orid is event prefor in Jiggle\n");
                  sb.append("O=+ prefmag orid matches event prefor prefmag in Db\n");
                  sb.append("E=+ prefmag magid matches event prefmag in Db\n");
                  sb.append("\n");
                  //sb.append("At end of summary line:\n");
                  //sb.append(" + = Event prefmag in Db\n");
                  //sb.append(" - = not Event prefmag in Db\n");
                }
    }

  }

  private class LongDumpMenuListener implements ActionListener {
    //
    //WorkerStatusFrame wsf = new WorkerStatusFrame();
    //{ wsf.setBeep(jiggle.beep); }
    //
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        final String cmd = e.getActionCommand();

        //wsf.pop("Dump info", "Getting"+cmd+" data ...", true);
        jiggle.initStatusGraphicsForThread("Dump info", "Getting "+cmd+" data ..."); // pops status

        org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
          // write string to TAB_message if set not null below:
          String str = null;
          public Object construct() { // any GUI graphics in method should invokeLater
            if (cmd.equals("Channel cache lat,lon info")) { // long time
              ChannelList chList = MasterChannelList.get();
              chList.sortByStation();
              int count = chList.size();

              StringBuffer sb  = new StringBuffer(32*count+132);
              sb.append("Cache file: ").append(chList.getCacheFilename());
              sb.append(" contains ").append(count).append(" channels.\n");
              sb.append(chList.toString());
              str = sb.toString();
            }
            else if (cmd.equals("Channel cache all info")) { // long time
              ChannelList chList = MasterChannelList.get();
              chList.sortByStation();
              int count = chList.size();
              StringBuffer sb  = new StringBuffer(1024*count+132);
              sb.append("Cache file: ").append(chList.getCacheFilename());
              sb.append(" contains ").append(count).append(" channels.\n");
              sb.append(chList.toDumpString());
              str = sb.toString();
            }
            else if (cmd.equals("Channel cache waveform info")) { // long time
              ChannelList cList = MasterChannelList.get();
              Solution sol = jiggle.mv.getSelectedSolution();
              if (sol != null) {
                StringBuffer sb = new StringBuffer(cList.size()*48+256);
                cList.distanceSort(sol); // aww 6/11/2004
                sb.append("Prefix \"+\" => waveform loaded for cached channel at the specified distance.\n");
                sb.append(jiggle.mv.getChannelChecklistString(cList));
                str = sb.toString();
              }
              else str = "No event data loaded for channel cache waveform info.";
            }
            else if (cmd.equals("MasterView info")) { // long time
              str = (jiggle.mv != null) ?
                jiggle.mv.dumpToString() : "MasterView is null.";
            }
            // Hyp2000
            else if (cmd.equals("Hyp2000 command file name")) {
              if (jiggle.solSolverDelegate != null) {
                 str = "Command file name: " + jiggle.solSolverDelegate.getInstName() + "\n"; 
              }
              else str = "Solution service is null";
            }
            else if (cmd.equals("Hyp2000 command file contents")) {
              if (jiggle.solSolverDelegate != null) {
                 str = jiggle.solSolverDelegate.getInstFileContents(); 
              }
              else str = "Solution service is null";
            }
            else if (cmd.equals("Hyp2000 available command files")) {
              str = (jiggle.solSolverDelegate != null) ?
                jiggle.solSolverDelegate.getInstFileList() : "Solution service is null";
            }
            // velocity models can be complex and defined in multiples files - CRH is simple case here removed - aww
            //else if (cmd.equals("Hyp2000 velocity model file contents")) {
            //  str = (jiggle.solSolverDelegate != null) ?
            //    jiggle.solSolverDelegate.getCrhFileContents() : "Solution service is null";
            //}
            else if (cmd.equals("Hyp2000 station file contents")) {
              str = (jiggle.solSolverDelegate != null) ?
                jiggle.solSolverDelegate.getStaFileContents() : "Solution service is null";
            }
            else if (cmd.equals("Hyp2000 arc input file contents")) {
              str = (jiggle.solSolverDelegate != null) ?
                jiggle.solSolverDelegate.getArcInFileContents() : "Solution service is null";
            }
            else if (cmd.equals("Hyp2000 arc output file contents")) {
              str = (jiggle.solSolverDelegate != null) ?
                jiggle.solSolverDelegate.getArcOutFileContents() : "Solution service is null";
            }
            else if (cmd.equals("Hyp2000 print file contents")) {
              str = (jiggle.solSolverDelegate != null) ?
                jiggle.solSolverDelegate.getPrtFileContents() : "Solution service is null";
            }
            else if (cmd.equals("Picker info")) {
              if (jiggle.picker == null) str = "Automatic picker is undefined (null), must first make an autopick";
              else {

                StringBuilder sbp = new StringBuilder(2048);
                sbp.append(jiggle.picker.getProperties( new GenericPropertyList() ).listToString());

                java.util.List aList = jiggle.picker.getChannelParms();
                if (aList == null) aList = new ArrayList(0);
                Collections.sort(aList);
                StringBuilder sb = new StringBuilder(sbp.length() + (aList.size()+6)*132);
                sb.append(sbp);
                sb.append("\n");

                // Start with #, a commment
                sb.append(String.format("#PickEW channel parms for %d channels%n", (aList == null) ? 0 : aList.size()));
                PickerParmIF parm = null;
                for (int idx=0; idx<aList.size(); idx++) {
                    parm = (PickerParmIF) aList.get(idx);
                    if (idx == 0) sb.append(parm.getFormattedHeader()); // add header at top
                    //sb.append(parm.toParseableString();
                    sb.append(parm.toFormattedString());
                    sb.append("\n");
                }
                str = sb.toString();
              }
            }
            else if (cmd.equals("Channels (by dist with travel times)")) {
               TravelTime tt = TravelTime.getInstance();
               tt.setSource(jiggle.mv.getSelectedSolution());
               str = null;
               if (jiggle.mv.wfvList != null) {
                 WFView wfv [] =  jiggle.mv.wfvList.getArray();
                 StringBuilder sb = new StringBuilder(132*wfv.length+132);
                 sb.append("Traveltime velocity model: "+ tt.getModel().getName());
                 sb.append("\nNt.Sta.Chn.Lc        km  az      P      S    S-P\n");
                 int count = 0;
                 double dist = Channel.NULL_DIST;
                 double ptime = 0.;
                 double stime = 0.;
                 double smp = 0.;
                 long az = 0l;
                 DoubleRange ps = null;
                 for (int i=0; i < wfv.length; i++) {
                     dist = wfv[i].getChannelObj().getDistance();
                     if (dist == Channel.NULL_DIST) {
                        az = 0;
                        ptime = 0.;
                        stime = 0.;
                        smp = 0.;
                     }
                     else {
                        az = Math.round(wfv[i].getChannelObj().getAzimuth());
                        ps = tt.getTTps(dist);
                        ptime = ps.getMinValue();
                        stime = ps.getMaxValue();
                        smp = stime - ptime; 
                     }
                     sb.append(String.format("%-16s", wfv[i].getChannelObj().toDelimitedSeedNameString(".")));
                     sb.append(String.format(" %6.1f %3d %6.2f %6.2f %6.2f", dist, az, ptime, stime, smp));
                     sb.append("\n");
                     count++;
                 }
                 sb.append("#Total channels = ").append(count).append("\n");
                 str = sb.toString();
                     
               }
               if (str == null || str.length() == 0) str = "No views with channels.";
            }
            else if ( cmd.equals("Posts for any event") ) {
                StringBuilder sb = new StringBuilder(4096); // one mongo string
                sb.append("Evid            Group           Source          State           Rank   Result  LoadDate\n");
                org.trinet.pcs.ProcessControl.setConnection(DataSource.getConnection());
                org.trinet.pcs.StateRow [] sra = org.trinet.pcs.StateRow.get();
                if (sra != null && sra.length > 0) {
                   for (int jdx = 0; jdx<sra.length; jdx++) {
                       sb.append(sra[jdx].toOutputString(true));
                       sb.append("\n");
                   }
                }
                else sb.append("No posts found in database\n");

                str = sb.toString();
            }

            return str;
          }

          public void finished() { // GUI update here
            //wsf.unpop();
            jiggle.resetStatusGraphicsForThread(); // unpops status
            if (str != null) jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true);
          }
        }; // end of SwingWorker

        sw.start(); // start long task worker thread
    }
  }

  private class LongDumpMenuListener2 implements ActionListener {
    public void actionPerformed(ActionEvent e) {

        if (!viewLoaded()) return;

        saveMenuEventData(e);
        final String cmd = e.getActionCommand();

        //wsf.pop("Dump info", "Getting"+cmd+" data ...", true);
        jiggle.initStatusGraphicsForThread("Dump info", "Getting "+cmd+" data ..."); // pops status

        org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
          // write string to TAB_message if set not null below:
          String str = null;
          public Object construct() { // any GUI graphics in method should invokeLater

            Solution sol = jiggle.mv.getSelectedSolution();

            if (jiggle.wfScroller == null || jiggle.wfScroller.groupPanel == null) return null;

            if (cmd.startsWith("View span w")) {
                str = null;
                if (jiggle.mv.wfvList != null) {
                  WFView wfv [] =  jiggle.mv.wfvList.getArray();
                  StringBuffer sb = new StringBuffer(132*wfv.length+80);
                  sb.append("#Total panels = " + wfv.length + "\n"); 
                  for (int i=0; i < wfv.length; i++) {
                    sb.append(wfv[i].dumpToString()).append("\n");
                    Waveform wf = wfv[i].getWaveform();
                    if (wf != null) {
                      sb.append(wf.toString());

                      if (cmd.endsWith("packet details")) {
                        TimeSpan [] tsArray = wf.getPacketSpans();
                        sb.append("\nPacket spans:\n");
                        for (int idx = 0; idx < tsArray.length; idx++) {
                          sb.append( tsArray[idx].toEpochTimeString() ).append(" ").append( tsArray[idx].toString() ).append("\n");
                        }
                      }

                      WFSegment [] wfsArray = wf.getArray();
                      sb.append("\nSegment spans:\n");
                      for (int idx = 0; idx < wfsArray.length; idx++) {
                          sb.append( wfsArray[idx].getTimeSpan().toEpochTimeString() ).append(" ").append( wfsArray[idx].toString() ).append("\n");
                      }
                    }
                  }
                  str = sb.toString();
                }
                if (str == null || str.length() == 0) str = "No waveform panel list.";
            }
            else if (cmd.equals("Channels of user selected panels")) {
                WFPanelList wfpList = jiggle.wfScroller.groupPanel.wfpList;
                int cnt = wfpList.size();
                StringBuilder sb = new StringBuilder((cnt+1)*80);
                WFPanel wfp = null;
                int scnt = 0;
                for (int idx=0; idx<cnt; idx++) {
                    wfp = (WFPanel) wfpList.get(idx);
                    if (wfp.wfv != null && wfp.wfv.isSelected()) {
                        sb.append(wfp.wfv.getChannelObj().toDelimitedSeedNameString()).append("\n");
                        scnt++;
                    }
                }
                sb.append("#Total selected view channels: " + scnt + "\n");
                str = sb.toString();
            }
            else if (cmd.equals("Channels w/o waveform rows in db")) {

                if (sol == null) return "No event selection in masterview\n";

                ArrayList aList = (ArrayList) AbstractWaveform.createDefaultWaveform().getBySolution(sol);
                int cnt = aList.size();
                ChannelableList dbWfList = new ChannelableList(aList);
                aList.clear();
                WFPanelList wfpList = jiggle.wfScroller.groupPanel.wfpList;
                cnt = wfpList.size();
                aList = new ArrayList(cnt);
                StringBuilder sb = new StringBuilder((cnt+1)*80);
                Waveform wf = null;
                WFPanel wfp = null;
                for (int idx=0; idx<cnt; idx++) {
                    wfp = (WFPanel) wfpList.get(idx);
                    if (wfp.wfv != null) {
                      wf = wfp.wfv.getWaveform();
                      if (wf != null && !dbWfList.hasChannelId(wf)) {
                        sb.append(wf.getChannelObj().toDelimitedSeedNameString()).append(" ");
                        sb.append(wf.getTimeSpan()).append("\n");
                        aList.add(wf);
                      }
                    }
                }
                cnt = aList.size();
                sb.append("#Total channels w/o db waveform rows: " + cnt + "\n");
                if (cnt > 0) {
                    int answer = JOptionPane.showConfirmDialog(JiggleMenuBar.this.jiggle,
                          "Select views of channels w/o db waveforms?",
                          "Select Views", JOptionPane.YES_NO_OPTION);

                    if (answer == JOptionPane.YES_OPTION) {
                        for (int idx = 0; idx < cnt; idx++) {
                          wf = (Waveform) aList.get(idx); // those missing db wfs
                          wfp = wfpList.get(wf); // via get(channelable) method
                          if (wfp.wfv != null) {
                              wfp.wfv.setSelected(true);
                              wfp.setRowHeaderBackground();
                          }
                        }
                       
                        if (!jiggle.getProperties().getBoolean("showRowHeaders"))
                            JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
                              "Show row headers on waveform panels in scroller to review selections (pink header background)",
                              "Selected Waveform Views", JOptionPane.PLAIN_MESSAGE);

                        // Next what ?
                        // Edit these selected views with JTree Dialog ?
                        // Use different menu option to process "selected" views ?
                        // User clicking OK invokes loop over selections e.g. calling a stored procedure
                        // NOTE loadingTimeSeries could cause side-effect (scroller vert scrollbar setting CacheMgr idx in WFViewList)
                    }
                }
                str = sb.toString();
            }
            else if (cmd.equals("All timeseries suspected clipped")) {

                Thread t = Thread.currentThread();
                t.setName("ClipSwingWorkerWFLoad");

                WFPanelList wfpList = jiggle.wfScroller.groupPanel.wfpList;
                int cnt = wfpList.size();
                StringBuilder sb = new StringBuilder((cnt+2)*132);
                Waveform wf = null;
                WFPanel wfp = null;
                double clip = 0.;
                double min = 0.;
                double max = 0.;
                double clipFactorA = jiggle.getProperties().getDouble("clippingAmpScalar.analog", AmpMagnitudeMethod.DEFAULT_A_CLIP);
                double clipFactorDA = jiggle.getProperties().getDouble("clippingAmpScalar.digital.acc", AmpMagnitudeMethod.DEFAULT_DA_CLIP);
                double clipFactorDV = jiggle.getProperties().getDouble("clippingAmpScalar.digital.vel", AmpMagnitudeMethod.DEFAULT_DV_CLIP);
                boolean localLoad = false;
                ArrayList aList = new ArrayList(cnt);
                sb.append("Clipping amp scalar analog: ").append(String.format("%3.2f",clipFactorA));
                sb.append(" digital vel: ").append(String.format("%3.2f", clipFactorDV));
                sb.append(" acc: ").append(String.format("%3.2f", clipFactorDA)).append("\n");
                sb.append("Channel         Min           Max           Clip         TimeSpan\n");
                for (int idx=0; idx<cnt; idx++) {
                    localLoad = false;
                    wfp = (WFPanel) wfpList.get(idx);
                    if (wfp.wfv != null) {
                      wf = wfp.wfv.getWaveform();
                      if (wf != null) {

                        clip = wfp.wfv.getChannelObj().getClipAmpValue((sol == null) ?
                                    (java.util.Date) null : (java.util.Date) sol.getDateTime());

                        if (Double.isNaN(clip) || clip <= 0.) continue;

                        if  (clip <= 4096.) clip = clip*clipFactorA;
                        else clip = (wfp.wfv.getChannelObj().isAcceleration()) ? clip*clipFactorDA : clip*clipFactorDV;

                        if (!wf.hasTimeSeries()) {
                            // STILL Need to code thread name so to have cache mgr thread blocked by this SwingWorker thread
                            // VertScrollBar listener action resets mgr which unloads
                            localLoad = wfp.wfv.loadTimeSeries();
                        }
                        if (wf.hasTimeSeries()) {
                              min = Math.abs(wfp.wfv.getWaveform().getMaxAmp());
                              max = Math.abs(wfp.wfv.getWaveform().getMinAmp());
                              if (min > clip || max > clip) {
                                sb.append(String.format("%-14s ", wf.getChannelObj().toDelimitedSeedNameString()));
                                sb.append(String.format("%13.6e %13.6e %13.6e ", min, max, clip));
                                sb.append(wf.getTimeSpan()).append("\n");
                                aList.add(wf);
                              }
                              if (localLoad) wfp.wfv.unloadTimeSeries();
                        }
                      }
                    }
                }
                cnt = aList.size();
                sb.append("#Total channels suspected as clipped: " + cnt + "\n");
                if (cnt > 0) {
                    int answer = JOptionPane.showConfirmDialog(JiggleMenuBar.this.jiggle,
                          "Flag waveforms of suspected channels as clipped?",
                          "Clipped Waveforms", JOptionPane.YES_NO_OPTION);

                    if (answer == JOptionPane.YES_OPTION) {
                        for (int idx = 0; idx < cnt; idx++) {
                          wf = (Waveform) aList.get(idx); // those suspected as clipped wfs
                          wfp = wfpList.get(wf); // via get(channelable) method
                          if (wfp.wfv != null) {
                              wfp.wfv.getWaveform().setClipped(true);
                              // wfp.wfv.setSelected(true); // ?
                              wfp.setRowHeaderBackground();
                          }
                        }
                    }
                }
                str = sb.toString();
            }
            else if (cmd.equals("All timeseries starting noise level")) {

                Thread t = Thread.currentThread();
                t.setName("NoiseSwingWorkerWFLoad");
                java.util.List aList = jiggle.wfScroller.groupPanel.wfpList; 
                StringBuffer sb = new StringBuffer(80*aList.size());
                Waveform wf = null;
                TimeSpan ts = new TimeSpan();
                boolean localLoad = false;
                float noise = 0.f;
                String wfUnits = null;
                ChannelGain cg= null;
                double gain = 1.;
                String gUnits = null;
                String filterStr = null;
                DateTime dt = null;
                //double snr = 0.;
                for (int i=0; i < aList.size(); i++) {
                    localLoad = false;
                    wf = ((WFPanel) aList.get(i)).getWf();
                    if (wf == null) continue;
                    if (! wf.hasTimeSeries()) {
                        // STILL Need to code thread name so to have cache mgr thread blocked by this SwingWorker thread
                        // VertScrollBar listener action resets mgr which unloads
                        if (wf.loadTimeSeries()) localLoad = true;
                        else continue;
                    }
                    ts.setStart(wf.getEpochStart());
                    ts.setEnd(wf.getEpochStart()+5.);
                    noise = wf.scanForNoiseLevel(ts);
                    wfUnits = Units.getString(wf.getAmpUnits());
                    filterStr = wf.getFilterName();
                    if (filterStr == null) filterStr = "";
                    dt = new DateTime(wf.getEpochStart(), true);
                    cg = wf.getChannelObj().getGain(dt, Channel.getDbLookup());
                    gain  = (cg.isValidNumber()) ?  Math.abs(cg.doubleValue()) : 1.;
                    gUnits = wfUnits;
                    if (cg.isVelocity()) gUnits = "vel";
                    else if (cg.isAcceleration()) gUnits = "acc"; 

                    //snr = wf.getPeakSample().value;
                    //if (noise != 0.) snr = snr/noise;

                    sb.append(
                        String.format("%10.3f %-8s %10.5e %-8s %s %-16s %-24s",
                            noise, wfUnits, noise/gain, gUnits, ts, wf.getChannelObj().toDelimitedSeedNameString("."), filterStr)
                      //  String.format("%10.3f %-8s %s %-16s %-24s",
                      //      noise, wfUnits, ts, wf.getChannelObj().toDelimitedSeedNameString("."), filterStr)
                    ).append("\n");
                    if (localLoad) wf.unloadTimeSeries(); 
                }

                str = sb.toString();
                if (str.length() == 0) str = "No waveforms in panels.";
            }
            else if (cmd.endsWith("amps,bias info")) {
                Thread t = Thread.currentThread();
                t.setName("AmpBiasSwingWorkerWFLoad");
                java.util.List aList = jiggle.wfScroller.groupPanel.wfpList; 
                StringBuffer sb = new StringBuffer(132*(aList.size()+2));
                AbstractWaveform wf = null;
                TimeSpan ts = new TimeSpan();
                boolean localLoad = false;
                int count = 0;
                sb.append("                                                                      Segs            Clip\n");
                sb.append("Channel              StartTime               EndTime                  |  Samps Units  |         MinAmp");
                sb.append("         MaxAmp           Bias    SDist\n"); 
                WFPanel wfp = null;
                final  Format f8 = new Format("%8.1f");
                for (int i=0; i < aList.size(); i++) {
                    localLoad = false;
                    wfp = (WFPanel) aList.get(i);
                    if (cmd.startsWith("Active panels") && !wfp.wfv.isActive) continue; 
                    wf = (AbstractWaveform) wfp.getWf();
                    if (wf == null) continue;
                    if (! wf.hasTimeSeries()) {
                        // STILL Need to code thread name so to have cache mgr thread blocked by this SwingWorker thread
                        // VertScrollBar listener action resets mgr which unloads
                        if (wf.loadTimeSeries()) localLoad = true;
                        else continue;
                    }
                    if (wf != null && wf.hasTimeSeries()) {
                          if (cmd.startsWith("Clipped timeseries") && !wf.isClipped()) continue;
                          sb.append(wf.toAmpInfoString()).append(" ").append(f8.form(wfp.wfv.getDistance())).append("\n");
                          count++;
                          if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
                    }
                }
                sb.append("#Total waveforms = ").append(count).append("\n");
                str = sb.toString();
                if (str == null || str.length() == 0) str = "No waveforms.";
            }
            else if (cmd.equals("Strong motion info")) {
                if (sol == null) str = "No event selection in masterview.\n";
                else {
                  if ( DataSource.tableExists("ASSOCEVAMPSET") ) {
                      AmpList ampList = new AmpList(Amplitude.create().getByValidAmpSet(sol.getId().longValue(), "sm", null));
                      ampList.insureLatLonZinfo(sol.getDateTime());
                      ampList.distanceSort(sol);
                      if (ampList.size() > 0) {
                          StringBuilder sb = new StringBuilder((ampList.size()+1)*256);
                          sb.append(sol.getNeatStringHeader()).append("\n");
                          sb.append(sol.toNeatString()).append("\n");
                          sb.append(ampList.toNeatString(true));
                          str = sb.toString();
                      }
                      else str = "No strong motion amps found for event\n";
                  }
                  else str = "ASSOCEVAMPSET table is not accessible\n";
                }
            }

            return str;
          }

          public void finished() { // GUI update here
            //wsf.unpop();
            jiggle.resetStatusGraphicsForThread(); // unpops status
            if (str != null) jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true);
          }
        }; // end of SwingWorker

        sw.start(); // start long task worker thread
    }
  }

  private class HelpMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        Properties jp = jiggle.getProperties();

        if (cmd.equals("Help browser")) {
            if (helpBrowser != null) {
              if ( (helpBrowser.getExtendedState() & Frame.ICONIFIED) == 1) {
                if ((helpBrowser.getExtendedState() | Frame.ICONIFIED) == 1) 
                   helpBrowser.setExtendedState(Frame.NORMAL); // normal restore -aww
                else
                   helpBrowser.setExtendedState(Frame.MAXIMIZED_BOTH); // flags set to max, restore -aww
              }
              helpBrowser.toFront();
              return;
            }

            //System.out.println ("HELP: "+jp.getProperty("helpFile"));
            helpBrowser = new HTMLHelp( jp.getProperty("helpFile",
                    jp.getProperty("webSite","http://pasadena.wr.usgs.gov/jiggle") + "/JiggleProperties.html"),
                    "Jiggle Help Browser");
            helpBrowser.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent evt) {
                    helpBrowser = null;
                }
            });
        }
        else if (cmd.equals("About")) {
            StringBuffer msg = new StringBuffer(132);
            msg.append("<html>");
            msg.append("Jiggle version ").append(jiggle.versionNumber).append("<br>");
            msg.append("Author: " ).append(jiggle.authorName).append("<br>");
            msg.append("For more info see: <br>").append(VersionChecker.getAddress());
            msg.append("</html>");
            jiggle.popInfoFrame(cmd, msg.toString());
        }
    }
  }

  //
  private class FocalMecListener implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        saveMenuEventData(e);
        String cmd = e.getActionCommand();
        Properties jp = jiggle.getProperties();

        if (mecBrowser != null) {
              if ( (mecBrowser.getExtendedState() & Frame.ICONIFIED) == 1) {
                if ((mecBrowser.getExtendedState() | Frame.ICONIFIED) == 1) 
                   mecBrowser.setExtendedState(Frame.NORMAL); // normal restore
                else
                   mecBrowser.setExtendedState(Frame.MAXIMIZED_BOTH);
              }
              mecBrowser.toFront();
              return;
        }

        //System.out.println ("DEBUG mecBrowser focmec.URL.path  : "+jp.getProperty("focmec.URL.path"));
        String url = jp.getProperty("focmec.URL.path");
        //System.out.println ("DEBUG mecBrowser focmec.URL.fileTemplate: "+jp.getProperty("focmec.URL.fileTemplate"));
        // e.g. fileTemplate = "ci%d.cifm1.html";
        String fileTemplate = jp.getProperty("focmec.URL.fileTemplate");
        if ( url == null) {
            JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
                    "focmec.URL.path property is undefined", "Missing Property", JOptionPane.PLAIN_MESSAGE);
        }
        else {

          Solution sol = jiggle.mv.getSelectedSolution();
          if (sol == null) return;
          long evid = sol.getId().longValue();

          /*
          int mknt = JasiDbReader.getCountBySQL(DataSource.getConnection(),
                  "select 1 from mec where mecid=(select e.prefmec from event e where e.evid="+evid+")");
          if ( mknt <= 0 ) {
              JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
                    "No prefmec mechanism found for selected event","No mechanism", JOptionPane.PLAIN_MESSAGE);
              System.out.println("Jiggle INFO: No prefmec found for selected event");
              return;
          }
          */

          mecBrowser = new MecBrowserFrame();
          jiggle.addPropertyChangeListener(mecBrowser);
          mecBrowser.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
          mecBrowser.setSize(720, 1000);

          JPanel jpanel = new JPanel();
          jpanel.setLayout(new GridLayout(0,1));

          int knt = 0;
          String fileurl = null;
          try {
            for ( int idx=1; idx<4; idx++) {
                fileurl = null;
                if (fileTemplate != null) fileurl = url + String.format(fileTemplate, evid, idx);
                //System.out.println("DEBUG JiggleMenuBar mecBrowser file url = " + fileurl); 
                /*
                HtmlPane jpane = new HtmlPane(fileurl);
                if ( ! jpane.hasValidPane() ) continue;
                jpane.setVerbose((knt < 1)); // kludge to suppress file not found messages for missing url mecs
                jpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                jpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                jpanel.add(jpane);
                knt++;
                */

                java.awt.Image image = null;
                try {
                    java.net.URL URL = new java.net.URL(fileurl);
                    image = javax.imageio.ImageIO.read(URL);
                } catch (java.io.IOException ex) {
        	        System.out.println(ex.getMessage()); // e.printStackTrace();
                }
                if ( image != null) {
                  //
                  Image nimage = image.getScaledInstance(-640, 880, Image.SCALE_SMOOTH);
                  if (nimage == null) {
                     JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
                    "Cannot rescale image", "Image Not Scaled", JOptionPane.PLAIN_MESSAGE);
                  }
                  else image = nimage;
                  //

                  JLabel label = new JLabel(new ImageIcon(image));
                  //label.setSize(1020, 640);
                  jpanel.add(new JScrollPane(label));
                  knt++;
                }
  
            }
          }
          catch(Exception ex2) {
          }

          if (knt > 0) {
              mecBrowser.getContentPane().add(jpanel);
              mecBrowser.setVisible(true);
          }
          else {
              mecBrowser.dispose();
              JOptionPane.showMessageDialog(JiggleMenuBar.this.jiggle,
                    "No file found:\n" + fileurl, "Missing Mechanism", JOptionPane.PLAIN_MESSAGE);
          }

          JiggleMenuBar.this.jiggle.addWindowListener(new WindowAdapter() { // close swarm frame upon jiggle window close
                public void windowClosing(WindowEvent evt) {
                    if (mecBrowser != null) {
                        mecBrowser.dispose();
                    }
                }
          });

          mecBrowser.addWindowListener(new WindowAdapter() {
                public void windowClosed(WindowEvent evt) {
                    mecBrowser = null;
                }
          });

        }
      }
    }
    final class MecBrowserFrame extends JFrame implements PropertyChangeListener {

              MecBrowserFrame() {
                super("Focal Mec");
              }

              public void propertyChange(PropertyChangeEvent evt) {
                  String propName = evt.getPropertyName();
                  //System.out.println("DEBUG MecBrowser propName: " + propName);
                  if (propName.equals(Jiggle.NEW_MV_PROPNAME)) {
                      mecBrowser.dispose();
                  }
              }
    }
    //
} //end of JiggleMenuBar class

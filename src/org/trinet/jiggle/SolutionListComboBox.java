package org.trinet.jiggle;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;

import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.DateTime;

/**
 * SolutionListBox extends JComboBox for the selection of events.
 * Supports color coding.
 */

public class SolutionListComboBox extends JComboBox implements ChangeListener, ListDataStateListener
{
    //ActiveSolutionList solList;
    SolutionList solList;
    SelectionListener selectionListener = new SelectionListener();
    //boolean debug = true;
    boolean debug = false;

    public SolutionListComboBox()
    {
      setEnabled(false);
    }

/**
 * Allow selection from a list of Solutions in the MasterView.
 * They are displayed as ID's.
 * The events in the JComboBox are color coded so the user can
 * connect phases with events by color.
 */
    public SolutionListComboBox(SolutionList solList)
    {
      this.solList = solList ;
      this.addItemListener(selectionListener);
//      solList.addChangeListener(this);

      makeList();
    }

/**
 *
 */
    public void makeList()
    {
      setEditable(false);
      setMaximumRowCount(4);                    // items displayed in scrolling window

      //setMinimumSize(new Dimension(20, getSize().height)); // reset width
      setMinimumSize(new Dimension(96, 16)); // reset width

      populateList();

      setRenderer(new SolutionItemRenderer());

    }

/**
* Override parent method because it causes java.lang.IndexOutOfBoundsException:
* starting in v1.2 when there are no items in the list.
*/
public void removeAllItems()
    {
        if (getItemCount() > 0) super.removeAllItems();
    }
/**
 * Shuffle solutions from list into the comboBox.
 */
    public void populateList() {

    // Must STOP events here!!!!!!!
       removeItemListener(selectionListener);

       removeAllItems();            // clear any old crap

       if (debug) System.out.println ("Populate list, size = "+ solList.size());

       // add Item's (if any) with unique colors
       if (solList.size() > 0) {
          Solution sol[] = solList.getArray();

          Solution selSol = (Solution)solList.getSelected(); //aww
          int idx = -1;

          for (int i = 0; i < sol.length; i++) {
            if (sol[i] == selSol) idx = i;
            SolutionItem solItem =
                new SolutionItem(sol[i], solList.getColorOf(sol[i]) );
            // this adds item to the end and fires ContentsChanged event
            addItem( solItem );
          }

          // must correct set JComboBox item as selected
          if (idx > -1) setSelectedIndex(idx);
        }
       // turn events back on
       addItemListener(selectionListener);
    }

/**
 * A SolutionItem is a JLabel that is put in the JComboBox.
 * This is necessary because I wanted to
 * make the text items in the JComboBox COLOR CODED to the Solutions in the list.
 * :. I had to render them explicitely (see SolutionItemRenderer)
 * Based on example in Core Java, by Topley, pg. 608
 */
        class SolutionItem extends JLabel {

            Solution sol;
            Color foregroundColor = Color.black;        //text color
            Color textColor = Color.black;

            public SolutionItem(Solution sol, Color clr) {
                super(String.valueOf(sol.id.longValue()));
                this.sol = sol;
                foregroundColor = clr;
            }

        } // end of internal class SolutionItem

/**
 * JComboBox only knows has to draw text or icons.
 * :. We must provide this ListCellRenderer
 * to paint the COLORED text items in the JComboBox.
 * This is a generic renderer and each specific
 * "SolutionItem" object is passed as 'Object value'
 */
        protected class SolutionItemRenderer extends JLabel implements ListCellRenderer        {
             int iconSize = 12;

             public Component getListCellRendererComponent(
                              JList list,
                              Object value,
                              int index,
                              boolean isSelected,
                              boolean cellHasFocus)
              {

                 this.setOpaque(true);

                 SolutionItem oi = (SolutionItem) value;
                 if (oi != null ) {
                     this.setIcon(new ColorFillIcon(oi.foregroundColor, iconSize));

                     this.setText( (String) oi.getText() );        // set label text

                     // set a background color that won't make the text disappear
                     this.setBackground(isSelected ? Color.gray : Color.lightGray);
                     // set background/foreground colors
                     this.setForeground(oi.textColor);
                 } else {                                        // no item
                     this.setIcon(null);
                     this.setText("");
                     this.setBackground(Color.lightGray);
                     this.setForeground(Color.black);
                 }
                 return this;
             }

        } // end of internal class SolutionItemRenderer

/**
 * Set the selected solution by ID number.
 */
public void setSelectedId(long id, boolean notify) {

    Solution sol[] = solList.getArray();

    for (int i = 0; i < sol.length; i++) {

            if (sol[i].id.longValue() == id) {

//                setSelectedIndex(i);
// set selected via the solList otherwise get infinite loop
          solList.setSelected(sol[i], notify);
                return;
            }
        }

}

/**
 * Set the selected solution.
 */
public void setSelected(Solution sol, boolean notify) {
    if (debug) System.out.println ("SolListComboBox.setSelected: notify= "+notify);
    solList.setSelected(sol, notify);
}


/**
 * return the evid that was selected
 */
public long getSelectedId()
{
    SolutionItem oi = (SolutionItem) getSelectedItem();
    if (oi != null) {
        // the items are labels
      String selText = ((JLabel) oi).getText();
      return Integer.valueOf( selText.trim() ).intValue();            // convert to int
    }

    return 0;
}
/**
 * return the Solution that was selected
 */
public Solution getSelectedSolution() {
    long id = getSelectedId();
    if (id == 0) return null;
    return solList.getById(id);
}

/** Handle a solution change from EXTERNAL source (i.e. solList). */
  public void stateChanged(ChangeEvent e) {
    Object arg = e.getSource();
    if (debug) System.out.println ("SolListComboBox.stateChange: ");
    if (arg instanceof Solution || arg instanceof SolutionList) {
      updateListInEventQueue();
    }
  }
// ListDataStateListener for list changes - aww
    public void intervalAdded(ListDataStateEvent e) {
        if (! (e.getSource() instanceof SolutionList)) return;
        updateListInEventQueue();
    }
    public void intervalRemoved(ListDataStateEvent e) {
        if (! (e.getSource() instanceof SolutionList)) return;
        updateListInEventQueue();
    }
    public void contentsChanged(ListDataStateEvent e) {
        if (! (e.getSource() instanceof SolutionList)) return;
        updateListInEventQueue();
    }
    public void orderChanged(ListDataStateEvent e) {
        if (! (e.getSource() instanceof SolutionList)) return;
        //updateListInEventQueue(); // a no-op, since contents the same
    }
    public void stateChanged(ListDataStateEvent e) {
        if (! (e.getSource() instanceof SolutionList)) return;
        updateListInEventQueue();
    }
    private void updateListInEventQueue() {
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                    populateList();
                }
            }
        );
    }

// //////////////////////////////////////////////////////
/** Tell all listeners when new item is selected. */
    class SelectionListener implements ItemListener {

        // this is the action taken when the selected item is changed
          public void itemStateChanged(ItemEvent e) {

//                SolutionListComboBox jc = (SolutionListComboBox) e.getSource();
//             Solution sol = jc.getSelectedSolution();

               // get sol selected by JComboBox
             Solution sol = getSelectedSolution();
             if (debug) System.out.println ("SolutionHandler.itemStateChanged ->"+ sol.id.toString());
             solList.setSelected( sol, true );
       }

    }
/*
    public static void main(String s[]) {

        double hoursBack = 3;
        final int secondsPerHour = 60*60;

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        System.out.println ("Making connection...");
        DataSource init = TestDataSource.create();  // make connection
        init.setWriteBackEnabled(true);

// must distinguish between 'java.util.Date'  and 'java.sql.Date'
        double now = new DateTime.getTrueSeconds();
        double then = now - (secondsPerHour * hoursBack);

        EventSelectionProperties props = new EventSelectionProperties();
        // subdir default to "jiggle" application name
        props.setFiletype("jiggle"); // test file found in the user's home "jiggle" subdir
        props.setFilename("eventSelection.props"); // name of event selection props file
        props.reset(); // read the props from file
        props.setup(); // configure local environment from props

        props.setTimeSpan(then.getTrueSeconds(), now.getTrueSeconds()); // UTC true seconds now - aww 2008/08/10
        props.setEventValidFlag(true);

        System.out.println ("Fetching: "+then.toString() +" -> "+ now.toString());

        SolutionList catList = new SolutionList(props);

//        ActiveSolutionList catMod = new ActiveSolutionList(catList);
        SolutionList catMod = new SolutionList(catList);

        if (catMod.size() > 0)
        {
          SolutionListComboBox box = new SolutionListComboBox(catMod);

          int fakeSelected = catMod.size() - 1;        // select last entry
              box.setSelectedIndex(fakeSelected);

          // show new selections as they are made
            box.addActionListener(new ActionListener()
            {
              public void actionPerformed(ActionEvent e)
                 {
                 SolutionListComboBox cb = (SolutionListComboBox) e.getSource();
                  System.out.println (" New selection = " + cb.getSelectedId());  // debug
                 }
             });

          frame.getContentPane().add(box);

        } else {
          System.out.println ("No events in catalog");
//          System.exit(0);        // seems to cause a hang
        }

        frame.setSize(200, 200);

        frame.pack();
        frame.setVisible(true);

    }
*/
}

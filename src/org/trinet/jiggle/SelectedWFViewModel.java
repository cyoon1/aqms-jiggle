package org.trinet.jiggle;

import java.util.*;
import javax.swing.event.*;

import org.trinet.jasi.Channelable;

/**
Model that keeps track of which WFView is currently selected. Listeners can
register to be notified of changes to the selected WFView.
*/
public class SelectedWFViewModel extends ChangeModel {
     // make fields volatile so that readers get last updated reference change -aww 2009/04/24
     volatile WFView wfv = null;

     volatile int viewsPerPage = 1;

     public SelectedWFViewModel() { }

     /** Create a model with a pre-selected WFPanel. */
     public SelectedWFViewModel(WFView wfview) {
         set(wfview);
     }

     /** Set the selected WFPanel. Return true if the selected panel is changed
     * by the call, otherwise returns false. Returns false 
     * is the same as the currently selected one. */
     public boolean set(WFView wfview) {
         return set(wfview, true);
     }

     /** Set the selected WFPanel.
     * Return true if the selected panel is changed by the call, otherwise return false.
     * Fires change event if the view changed, or if notify is set 'true' (e.g. view same as currently selected one). */
     public boolean set(WFView wfview, boolean notify) {
         boolean different = (wfv != wfview);
         if (notify || different) {  // try always firing -aww 2009/03/31
           synchronized (this) {
             wfv = wfview;
             fireStateChanged(this);
           } // end of synch
         }
         return different;
     }

     public boolean set(WFView wfview, int mode) {
         boolean different = (wfv != wfview);
         synchronized (this) {
             wfv = wfview;
             fireStateChanged(this, mode);
         }
         return different;
     }

     /** Selected the WFPanel for this Channel. Return true if the selected panel is changed
     * by the call, otherwise returns false. Returns false if the Channel is null,
     * is not in the WFViewList or
     * is the same as the currently selected one. */
     public boolean set(Channelable chan, WFViewList wfvList) {
         WFView wfview = wfvList.get(chan);
         if (wfview == null || wfview == wfv)  return false;
         set(wfview);
         return true;
     }

     /** Re-sets the currently selected WFView. This really just fires a new
      StateChanged event. Returns false if no WFView is selected. */
     public boolean reset() {
         if (wfv == null) return false;
         synchronized (this) {
           fireStateChanged(this);
         }
         return  true;
     }

     /** Return the currently selected WFView. */
     public WFView get() {
         return wfv;
     }

    /** Defines show many views will be jumped in a call to pageUp or pageDown. */
    public void setViewsPerPage(int pageViewCount) {
        viewsPerPage = pageViewCount;
    }

    public int getViewsPerPage() {
        return viewsPerPage;
    }

    public void pageUp(WFViewList wfvList, boolean wrap) {
        selectJump(wfvList, -viewsPerPage, wrap);
    }

    public void pageDown(WFViewList wfvList, boolean wrap) {
        selectJump(wfvList, viewsPerPage, wrap);
    }

    /**
     * Given a WFViewList, select the next WFView in the list. This model retains no
     * state information relative to the list so it must search the list for the
     * current selected WFView before it determines the "next" one. If the currently
     * selected WFView is at the end of the list we will wrap to the first on the
     * list.  Returns the index of the next WFView. If there is no currently selected
     * WFView or it is not in the list or the list is empty nothing happens and
     * returns -1.  */
    public int selectNext(WFViewList wfvList) {
        return selectJump(wfvList, 1, true);
        /*
        if (wfvList == null || wfvList.size() == 0) return -1;
        // find the currently selected WFView in the list
        int index = wfvList.indexOf(wfv);
        if (index != -1) {      // is in list
        if (++index > wfvList.size()-1) index = 0;  // wrap if necessary
        set((WFView) wfvList.get(index));
        }
        return index;
        */
    }

    /** Set as selected the next WFView in the given list.
    Returns the index of the previous WFView in the list.
    Returns -1 if no WFViews in the list. */
    public int selectPrevious(WFViewList wfvList) {
        return selectJump(wfvList, -1, true);
        /*
        if (wfvList == null || wfvList.size() == 0) return -1;
        // find the currently selected WFView in the list
        int index = wfvList.indexOf(wfv);
        if (index != -1) {      // is in list
        if (--index < 0) index = wfvList.size()-1;  // wrap if necessary
        set((WFView)wfvList.get(index));
        }
        return index;
        */
    }

    /** Set as selected the WFView in the given list that is offset from the current
    * selection by this much. Can be positive or negative. If value is beyond either end
    * of the list it will wrap if 'wrap' is true.
    Returns the index of the previous WFView in the list.
    Returns -1 if no WFViews in the list. */
    public int selectJump(WFViewList wfvList, int jumpIncrement, boolean wrap) {
        if (wfvList == null || wfvList.size() == 0) return -1;

        // find the currently selected WFView in the list
        int index = wfvList.indexOf(wfv);

        /*
        index += jumpIncrement;
        if (index < 0) {
           if (wrap) {
              index += wfvList.size();       // remember, index is negative here
           } else {
              index = 0;
           }
        } else if (index > wfvList.size()-1) {
           if (wrap) {
              index -= wfvList.size();
           } else {
              index = wfvList.size()-1;
           }
        }
        set((WFView)wfvList.get(index));
        */

        WFView wfv = null;
        int cnt = Math.abs(jumpIncrement);

        if (jumpIncrement < 0) {
          for (int i=index-1; i>=0; i--) {
            wfv = (WFView)wfvList.get(i);
            if (wfv.isActive) {
                cnt--;
                if (cnt == 0) {
                  index = i;
                  break;
                }
            }
          }
          if (wrap && cnt > 0) {
            for (int i=wfvList.size()-1; i>=0; i--) {
              wfv = (WFView)wfvList.get(i);
              if (wfv.isActive) {
                cnt--;
                if (cnt == 0) {
                  index = i;
                  break;
                }
              }
            }
          }
        }
        else {
          for (int i=index+1; i<wfvList.size(); i++) {
            wfv = (WFView)wfvList.get(i);
            if (wfv.isActive) {
                cnt--;
                if (cnt == 0) {
                  index = i;
                  break;
                }
            }
          }
          if (wrap && cnt > 0) {
            for (int i=0; i<wfvList.size(); i++) {
              wfv = (WFView)wfvList.get(i);
              if (wfv.isActive) {
                cnt--;
                if (cnt == 0) {
                  index = i;
                  break;
                }
              }
            }
          }
        }
        if (wfv != null) set(wfv,1);
        return index;
    }

    /** Set as selected the first WFView in the given list.
    Returns the index of the first WFView in the list.
    Returns -1 if no WFViews in the list. */
    public int selectFirst(WFViewList wfvList) {

        if (wfvList == null || wfvList.size() == 0) return -1;
        int index = 0;
        //set((WFView)wfvList.get(index));

        WFView wfv = null;
        for (int i=0; i<wfvList.size(); i++) {
            wfv = (WFView)wfvList.get(i);
            if (wfv.isActive) {
                index = i;
                break;
            }
        }
        set(wfv,1);

        return index;
    }

    /** Set as selected the last in the given list.
    Returns the index of the last WFView in the list.
    Returns -1 if no WFViews in the list. */
    public int selectLast(WFViewList wfvList) {

        if (wfvList == null || wfvList.size() == 0) return -1;
        int index = wfvList.size()-1;
        // set((WFView)wfvList.get(index));
        WFView wfv = null;
        for (int i=index; i>=0; i--) {
            wfv = (WFView)wfvList.get(i);
            if (wfv.isActive) {
                index = i;
                break;
            }
        }
        set(wfv,1);

        return index;

    }

    /** Notify all listeners that have registered interest for notification on
     * this event type.  The event instance is created on each call to reflect the
     * current state. <p> The return from changeEvent.getSource() is this class. */
     protected void fireStateChanged(Object src, int mode) {

         // bail if events are turned off
         if (!getFireEvents()) return;

        // bail if no listeners
        if (listenerList.getListenerCount() == 0) return;

        // Guaranteed to return a non-null array, :. no NULL check needed
        Object[] listeners = listenerList.getListenerList();

        ViewChangeEvent changeEvent = new ViewChangeEvent(src, mode);

        // Notify the listeners last to first. Note: 'listeners' is an array of
        // PAIRS of ListenerType/Listener, hence the weird indexing.
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ChangeListener.class) {
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }
}

class ViewChangeEvent extends ChangeEvent {
    int mode = 0;
    ViewChangeEvent(Object src) {
        super(src);
    }

    ViewChangeEvent(Object src, int mode) {
        super(src);
        this.mode = mode;
    }
}

package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.PhaseDescription;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public abstract class AbstractPreferencesPanel extends JPanel {

    protected boolean updateGUI = false;
    protected boolean resetGUI = false;

    protected JiggleProperties newProps;  // properties to be edited by this object

    public AbstractPreferencesPanel(JiggleProperties props) {

        newProps = props;

        try {
            initGraphics();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    abstract protected void initGraphics();

    protected JCheckBox makeCheckBox(String text, String prop) {
        return makeCheckBox(text, text, prop);
    }

    protected JCheckBox makeCheckBox(String text, String toolTip, String prop) {

        JCheckBox cb = new JCheckBox();
        cb.setText(text);
        cb.setSelected(newProps.getBoolean(prop));
        cb.setToolTipText(toolTip);
        cb.addItemListener(new BooleanPropertyCheckBoxListener(prop));
        cb.setAlignmentX(Component.LEFT_ALIGNMENT);
        return cb;
    }

    protected JCheckBox makeResetCheckBox(String text, String prop) {
        return makeResetCheckBox(text, text, prop);
    }

    protected JCheckBox makeResetCheckBox(String text, String toolTip, String prop) {
        JCheckBox cb = new JCheckBox();
        cb.setText(text);
        cb.setSelected(newProps.getBoolean(prop));
        cb.setToolTipText(toolTip);
        cb.addItemListener(new ResetBooleanCheckBoxListener(prop));
        cb.setAlignmentX(Component.LEFT_ALIGNMENT);
        return cb;
    }

    protected JCheckBox makeUpdateCheckBox(String text, String prop) {
        return makeUpdateCheckBox(text, text, prop);
    }

    protected JCheckBox makeUpdateCheckBox(String text, String toolTip, String prop) {
        JCheckBox cb = new JCheckBox();
        cb.setText(text);
        cb.setSelected(newProps.getBoolean(prop));
        cb.setToolTipText(toolTip);
        cb.addItemListener(new UpdateBooleanCheckBoxListener(prop));
        cb.setAlignmentX(Component.LEFT_ALIGNMENT);
        return cb;
    }

    // Used only if property change doesn't require update of GUI 
    protected class MiscDocListener implements DocumentListener {
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        /** Make changes to properties "on the fly". */
        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            // Property change doesn't require update of GUI 
            newProps.setProperty(prop, jtf.getText().trim());
        }
    }

    protected class UpdateDocListener implements DocumentListener {
        private JTextField jtf = null;
        private String prop = null;

        public UpdateDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        /** Make changes to properties "on the fly". */
        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            newProps.setProperty(prop, jtf.getText().trim());
            updateGUI = true;
        }
    }

    protected class ResetDocListener implements DocumentListener {
        private JTextField jtf = null;
        private String prop = null;

        public ResetDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        /** Make changes to properties "on the fly". */
        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            newProps.setProperty(prop, jtf.getText().trim());
            resetGUI = true;
        }
    }

    private class BooleanPropertyCheckBoxListener implements ItemListener { // inner class
        private String prop = null;

        public BooleanPropertyCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean state =(e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, state);
        }
    } // end of inner class

    protected class UpdateBooleanCheckBoxListener implements ItemListener {
        private String prop = null;

        public UpdateBooleanCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean state =(e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, state);
            updateGUI = true;
        }
    }

    protected class ResetBooleanCheckBoxListener implements ItemListener {
        private String prop = null;

        public ResetBooleanCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean state =(e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, state);
            resetGUI = true;
        }
    }
}

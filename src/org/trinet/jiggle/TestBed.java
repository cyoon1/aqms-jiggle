package org.trinet.jiggle;

import javax.swing.*;
//import java.awt.Cursor;
//import java.awt.Toolkit;
import java.awt.*;
import java.awt.event.*;
import org.trinet.util.DateTime;
import org.trinet.util.graphics.*;

// Junk class for testing - aww
public class TestBed {

    public static void main(String args[]) {

    double t1 = Double.NaN;
    double t2 = Double.NaN;

    System.out.println ("t1 == t2 -> "+ (t1 == t2));
    System.out.println ("t1 == Double.NaN -> "+ (t1 == Double.NaN));

    //                 t1 == t2 -> false
    //                 t1 == Double.NaN -> false

    double val2, val1;
    for (int i = 0; i< 30; i++) {

     val1 = 1.0;
     val2 = val1 + Math.pow (10, -i);

     System.out.println ( val1 + "  "+val2+ " "+ DateTime.areEqual(val1, val2) );
    }
// make a main frame
        JFrame frame = new JFrame("Test");
/*
        JPanel jpanel1 = new JPanel();
        JPanel jpanel2 = new JPanel();

        jpanel1.setSize(200,200);
        jpanel2.setSize(200,200);
        jpanel1.setBackground(Color.yellow);
        jpanel2.setBackground(Color.white);

        // make a split pane
        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                           false,       // don't repaint until resizing is done
                           jpanel1,      // top component
                           jpanel2);
        // cursor
        Cursor unpickCursor = createCursor("unpickCursor.gif");
        Cursor mCursor = createCursor("mCursor.gif");
        Cursor aCursor = createCursor("aCursor.gif");

        split.setCursor(aCursor);
        jpanel1.setCursor(unpickCursor);
        jpanel2.setCursor(mCursor);
        frame.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

//        frame.setCursor(null);   // error

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

           frame.getContentPane().add(split);
 */
//        frame.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));


        JPanel jpanel1 = new JPanel();
        jpanel1.setSize(200,200);
        jpanel1.setBackground(Color.yellow);
        jpanel1.setCursor(new Cursor(Cursor.HAND_CURSOR));

        ImageIcon anim = new ImageIcon("images/gearsAnim.gif");
        JLabel pic = new JLabel(anim);
        jpanel1.add(pic);
        
        frame.getContentPane().add(jpanel1);

        frame.pack();
        frame.setVisible(true);

        System.out.println (frame.getCursor());
 /*
        System.out.println (split.getCursor().getName());
        System.out.println (jpanel1.getCursor().getName());
        System.out.println (jpanel2.getCursor().getName());
 */
//        frame.setSize(width, height);        // must be done AFTER setVisible
}

/** Java cursors must follow these rules...    <br>
 *  1) Be a .gif (others may work but I haven't tried it)  <br>
 *  2) Be 2 colors (usually black and transparent) <br>
 *  3) The background must be set to "transparent", otherwise you'll get a white
 *     block as the background.   <br>
 *  4) Must be 32x32 (you can make the actual cursor smaller by not using the
 *     full 32x32 canvas.   <br>
 *  5) You must define the hotspot  <br>
*/
     static Cursor createCursor (String filename) {
            JPanel panel = new JPanel();
            Toolkit kit = panel.getToolkit();

            System.out.println ("best size = "+kit.getBestCursorSize(16, 16).toString());
            System.out.println ("best size = "+kit.getBestCursorSize(32, 32).toString());
            System.out.println ("max colors= "+kit.getMaximumCursorColors());

                Image image = IconImage.getImage(filename);

            Point hotspot = new Point(1, 1);

            Cursor cursor = null;
            try {
              cursor =  kit.createCustomCursor(image, hotspot, filename);
            }  catch (Exception ex) {
             System.out.println (ex.toString());
             return null;
            }
            return cursor;
     }
} 

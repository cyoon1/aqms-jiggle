package org.trinet.jiggle;

/**
 * A Menu item chooser for the masterview's selected solution event type.
 */
import java.util.*;
import java.awt.Dimension;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class EventTypeChooserMenu extends JPopupMenu implements ListDataStateListener {

    private EventType [] eventTypes = EventTypeMap3.getMap().getEventTypes();
    private SolutionPanel solPanel = null;

    protected EventType selectedEventType = null;

    /** Create an EventTypeChooserMenu. Set the current selection to item given. */
    public EventTypeChooserMenu(SolutionPanel solPanel, String [] choiceList, String selectedTypeId) {
        super();
        this.solPanel = solPanel;
        initMenu(choiceList, selectedTypeId);
    }

    protected void initMenu(String [] choiceList, String selectedTypeId) {

        if (choiceList == null) {
          initMenu((EventType []) null, selectedTypeId);
          return;
        }

        // Allow only those input choices that match known EventTypeMap3 codes
        ArrayList myChoices = new ArrayList(choiceList.length);
        for (int i = 0; i< choiceList.length; i++) {
          for (int j = 0; j< eventTypes.length; j++) {
            //if (eventTypes[j].name.equals(choiceList[i])) { // <== if input where name labels
            if (eventTypes[j].code.equals(choiceList[i])) {
                myChoices.add(eventTypes[j]);
            }
          }
        }
        EventType [] choices = (EventType []) myChoices.toArray(new EventType [myChoices.size()]);

        initMenu(choices, selectedTypeId);
    }

    private void initMenu(EventType [] choices, String selectedTypeId) {

        if (getComponentCount() > 0) removeAll();

        HashMap menuMap = new HashMap(11); // category menu
        JMenu jmenu = null;
        JMenuItem jmi = null;
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setSelectedItemByJasiType(evt.getActionCommand());
            }
        };

        // Group any user specified event types as separate menuitems at top part of menu
        if (choices != null && choices.length > 0) {
          for (int i = 0; i< choices.length; i++) {
            jmi = new JMenuItem(choices[i].name);
            jmi.addActionListener(al);
            jmi.setToolTipText(choices[i].desc);
            add(jmi);
          }
          addSeparator();
        }

        String dbCode = (selectedTypeId == null) ? EventTypeMap3.UNKNOWN : selectedTypeId;

        for (int i = 0; i< eventTypes.length; i++) {

            if (eventTypes[i].code.equals(dbCode)) selectedEventType = eventTypes[i];

            jmenu = (JMenu) menuMap.get(eventTypes[i].group);
            if (jmenu == null) {
                jmenu = new JMenu(eventTypes[i].group+"...");
                menuMap.put(eventTypes[i].group, jmenu);
            }
            jmi = new JMenuItem(eventTypes[i].name);
            jmi.setToolTipText(eventTypes[i].desc);
            jmi.addActionListener(al);
            jmenu.add(jmi);
        }

        Collection coll = menuMap.values();
        ArrayList aList = new ArrayList(coll);
        Comparator cp = new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((JMenu) o1).getActionCommand().compareTo( ((JMenu) o2).getActionCommand() );
            }

            public boolean equals(Object o) {
                return true;
            }
        };

        Collections.sort(aList, cp);

        Iterator iter = aList.iterator();
        while (iter.hasNext()) {
           jmenu = (JMenu) iter.next();
           add(jmenu);
        }
    }

    public void setSelectedItemByJasiType(String name) {
        //System.out.println("EventTypeChooserMenu setSelectedItemByJasiType name: " + name);
        for (int idx = 0; idx < eventTypes.length; idx++) {
            if (eventTypes[idx].name.equals(name)) {
                selectedEventType = eventTypes[idx];
                if (solPanel != null) {
                    solPanel.setEventType();
                }
                return;
            }
        }
        System.out.println("EventTypeChooserMenu setSelectedItemByJasiType FAILED TO FIND TYPE: " + name);
    }

    public void setSelectedItemByDbType(String code) {
        //System.out.println("EventTypeChooserMenu setSelectedItemByDbType code: " + code);
        for (int idx = 0; idx < eventTypes.length; idx++) {
            if (eventTypes[idx].code.equals(code)) {
                selectedEventType = eventTypes[idx];
                if (solPanel != null) {
                    solPanel.setEventType();
                }
                return;
            }
        }
        System.out.println("EventTypeChooserMenu setSelectedItemByDbType, FAILED TO FIND TYPE: " + code);
    }

    protected void updateEventTypeSelection(Solution sol) {
      final String name = sol.getEventTypeString();
      //System.out.println("EventTypeChooserMenu updateEventTypeSelection name: " + name);
      if (selectedEventType.name.equals(name)) return; // aww
      SwingUtilities.invokeLater(
          new Runnable() {
              public void run() {
                  setSelectedItemByJasiType(name);
              }
          }
      );
    }

    // ListDataStateListener for list changes -aww
    public void intervalAdded(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateEventTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void intervalRemoved(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateEventTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void contentsChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateEventTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void orderChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateEventTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void stateChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateEventTypeSelection((Solution)((SolutionList)obj).getSelected());
    }

} // EventTypeChooserMenu

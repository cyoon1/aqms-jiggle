package org.trinet.jiggle;
import javax.swing.event.*;
import org.trinet.util.*;

// Respond to changes of the MasterView SolutionList
// e.g. selected Solution (from the JiggleToolBar.SolutionListComboBox)
// Update GUI tabs and title
public class JiggleSolutionListListener implements ChangeListener, ListDataStateListener { // aww

        private Jiggle jiggle;

        public JiggleSolutionListListener(Jiggle jiggle) {
          this.jiggle = jiggle;
        }

        public void stateChanged(ChangeEvent changeEvent) {
          //Object arg = changeEvent.getSource();
          jiggle.updateTextViews(); // does do an invokeLater
        }
        public void intervalAdded(ListDataStateEvent e) {
          jiggle.updateTextViews();
        }
        public void intervalRemoved(ListDataStateEvent e) {
          jiggle.updateTextViews();
        }
        public void contentsChanged(ListDataStateEvent e) {
          jiggle.updateTextViews();
        }
        public void orderChanged(ListDataStateEvent e) {
          jiggle.updateTextViews();
        }
        public void stateChanged(ListDataStateEvent e) {
          jiggle.updateTextViews();
          if (e.getStateChange().getStateName().equals("selected") ) {
            if (jiggle.mv != null && jiggle.mv.getAlignmentMode() != MasterView.AlignOnTime) {
                 javax.swing.SwingUtilities.invokeLater( new Runnable() {
                     public void run() {
                         System.out.println("INFO: Selected solution changed resorting waveform views...");
                         jiggle.resortWFViews();
                         jiggle.mv.clearUndoLists();
                     }
                 });
            }
          }
        }

} // end of class




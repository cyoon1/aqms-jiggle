package org.trinet.jiggle;

import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;

import org.trinet.jasi.picker.AutoPickGeneratorIF;
import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.GenericPropertyList;

/**
 * A Dialog panel for reviewing/editing phase picker properties.
 * Properties defined within the input GenericPropertiesList define picker classname(s)
 * and/or associated properties filesname(s), if any. These properties have the
 * prefixes "picker.class." and "picker.props." before the picker name, which 
 * is typically the String returned by PhasePickerIF .xetName().
 * The Picker to be used for autopicking is selected by setting the property 
 * picker.selected.name to the picker name String.
 */
public class DPpicker extends JPanel implements ActionListener {

    private GenericPropertyList newProps = null;
    private String prefix = AbstractPicker.getPropertyPrefix();

    /** The name of th ecurrently selected picker */
    private String selectedPickerName = null;
    private GenericPropertyList selectedPickerProps = null;

    private JComboBox pickerNameBox = null;
    private JTextField pickerClassnameField = null;
    private JTextField pickerPropFileField = null;

    private JTextArea propsTextArea = null;

    private boolean hasChangedPicker = false;
    private boolean hasChangedProps = false;

    private AutoPickGeneratorIF jiggle = null;

    public DPpicker(GenericPropertyList newProps) {
        this(null, newProps);
    }

    /** Allow selection of waveform reading mode. */
    public DPpicker(AutoPickGeneratorIF jiggle, GenericPropertyList newProps) {

        this.jiggle = jiggle;
        this.newProps = newProps;
        PhasePickerIF picker = jiggle.getPhasePicker();

        try {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    /** Build the GUI. */
    private void initGraphics() throws Exception {

        setLayout(new BorderLayout());

        propsTextArea = new JTextArea();
        propsTextArea.setToolTipText("Add or change a property value, then press \"Save\"");
        propsTextArea.setEditable(true);
        propsTextArea.setBackground(Color.white);
        propsTextArea.setRows(12);
        propsTextArea.setColumns(60);
        new JTextClipboardPopupMenu(propsTextArea);

        String [] types = new String [] { "unknown" };
        if (newProps != null)  {
          //types = props.getStringArray("picker.namelist");
          types = getPickerNames(newProps);
          if (types == null || types.length == 0) types = new String [] { "unknown" };
        }
        pickerNameBox = new JComboBox(types);

        // add AFTER box's picker names are populated else adding items fires events
        selectedPickerName = (newProps == null) ? "" : newProps.getProperty("picker.selected.name", "");

        // Picker properties action buttons
        ActionListener actionListener =  new SetSaveLoadPickerPropsActionHandler();
        JButton setPropsButton = new JButton("Set");
        setPropsButton.addActionListener(actionListener);
        setPropsButton.setToolTipText("Set the selected picker properties from text area values.");

        JButton loadPropsButton = new JButton("Load");
        loadPropsButton.addActionListener(actionListener); 
        loadPropsButton.setToolTipText("Read and set the selected picker properties from a specified file.");

        JButton savePropsButton = new JButton("Save");
        savePropsButton.addActionListener(actionListener); 
        savePropsButton.setToolTipText("Set, then write the selected picker properties as shown in text area to a specified file.");

        JButton removePropsButton = new JButton("Remove");
        removePropsButton.addActionListener(actionListener); 
        removePropsButton.setToolTipText("Remove selected/input property key (e.g. misspelled)");

        Box pickerButtonBox = Box.createHorizontalBox();
        pickerButtonBox.add(Box.createHorizontalGlue());
        pickerButtonBox.add(setPropsButton);
        pickerButtonBox.add(savePropsButton);
        pickerButtonBox.add(loadPropsButton);
        pickerButtonBox.add(removePropsButton);
        pickerButtonBox.add(Box.createHorizontalGlue());

        Box selectPanel = Box.createVerticalBox();

        // Do this after creating buttons, action causes reference to button, must be not null
        int idx = ((DefaultComboBoxModel) pickerNameBox.getModel()).getIndexOf(selectedPickerName);
        if (idx >= 0) pickerNameBox.setSelectedIndex(idx);
        else pickerNameBox.setSelectedIndex(0);

        pickerNameBox.addActionListener(DPpicker.this);

        Box hbox = Box.createHorizontalBox();
        JLabel jlbl = new JLabel("Selected Picker : ");
        hbox.add(jlbl);
        pickerNameBox.setPreferredSize(new Dimension(80,30));
        pickerNameBox.setMaximumSize(new Dimension(120,30));
        hbox.add(pickerNameBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(Box.createHorizontalGlue());
        selectPanel.add(hbox);

        hbox = Box.createHorizontalBox();
        Box vbox = Box.createVerticalBox();
        pickerClassnameField = new JTextField(48);
        new JTextClipboardPopupMenu(pickerClassnameField);
        pickerClassnameField.setToolTipText("Classname specified for picker by Jiggle property");
        selectedPickerName = (newProps == null) ? "" : newProps.getProperty("picker.selected.name", "");
        String str =  (newProps == null) ? "" : newProps.getProperty("picker.class."+selectedPickerName, "");
        pickerClassnameField.setText(str);
        pickerClassnameField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (newProps == null) return;
                if (!newProps.getProperty("picker.class."+selectedPickerName, "").equals(pickerClassnameField.getText())) {
                    newProps.setProperty("picker.class."+selectedPickerName, pickerClassnameField.getText());
                    System.out.println("INFO: property reset picker.class." + selectedPickerName + "=" + pickerClassnameField.getText());
                    hasChangedPicker = true;
                }
            }
        });
        JButton aButton1 = new JButton("Class name ");
        aButton1.setPreferredSize(new Dimension(120,24));
        aButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (newProps == null) return;
                if (!newProps.getProperty("picker.class."+selectedPickerName, "").equals(pickerClassnameField.getText())) {
                    newProps.setProperty("picker.class."+selectedPickerName, pickerClassnameField.getText());
                    System.out.println("INFO: property reset picker.class." + selectedPickerName + "=" + pickerClassnameField.getText());
                    hasChangedPicker = true;
                }
            }
        });
        aButton1.setToolTipText("Press to set selected picker to the classname specified in text field");
        hbox.add(aButton1);
        hbox.add(pickerClassnameField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        selectPanel.add(vbox);

        vbox = Box.createVerticalBox();
        hbox = Box.createHorizontalBox();
        pickerPropFileField = new JTextField(48);
        pickerPropFileField.setEditable(false);
        pickerPropFileField.setToolTipText("Property file pathname specified for picker by Jiggle property");
        new JTextClipboardPopupMenu(pickerPropFileField);
        selectedPickerName = (newProps == null) ? "" : newProps.getProperty("picker.selected.name", "");
        str =  (newProps == null) ? "" : newProps.getProperty("picker.props."+selectedPickerName, "");
        pickerPropFileField.setText(str);
        JLabel aButton2 = new JLabel("Properties file: ");
        aButton2.setPreferredSize(new Dimension(120,24));
        hbox.add(aButton2);
        hbox.add(pickerPropFileField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        selectPanel.add(vbox);

        TitledBorder titledBorder =
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, 
                        new Color(148, 145, 140)),"Picker Properties");
        this.setBorder(titledBorder);
        this.add(selectPanel, BorderLayout.NORTH);
        JScrollPane jsp = new JScrollPane(propsTextArea);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        this.add(jsp, BorderLayout.CENTER);
        this.add(pickerButtonBox, BorderLayout.SOUTH);

        configureByProperties();
    }

    public void actionPerformed(ActionEvent e) {
        if (newProps == null) return;
        JComboBox src = (JComboBox) e.getSource();
        String pickerName = (String) src.getSelectedItem();
        if (pickerName != null && !pickerName.equals("unknown") && !pickerName.equals(selectedPickerName))  {

            if (src != pickerNameBox) {
                pickerNameBox.setSelectedItem(pickerName);
                return;
            }

            selectedPickerName = pickerName;
            if (!newProps.getProperty("picker.selected.name", "").equals(selectedPickerName)) {
                newProps.setProperty("picker.selected.name", selectedPickerName);
                System.out.println("INFO: property reset picker.selected.name=" + pickerClassnameField.getText());
                
                //
                //chanParmList.clear(); ? ACTION update preferences dialog parms panel?
                //better to imbed parms config as popup dialog activated by button in this panel
                //

                hasChangedPicker = true;
            }
            else return; // or not ??

            configureByProperties();
        }
    }

    public void addPickerChangeListener(ActionListener l) {
        pickerNameBox.addActionListener(l);
    }

    private void configureByProperties() {
        String pickerPropsFilename = newProps.getUserFileNameFromProperty("picker.props."+selectedPickerName);
        if (pickerPropsFilename != null) {

          GenericPropertyList gpl = null;
          if (jiggle != null && jiggle.getPhasePicker() != null) { // init with current existing props
              gpl = new GenericPropertyList();
              gpl.setFiletype("jiggle");
              gpl.setUserPropertiesFileName(pickerPropsFilename);
              setPickerPropsTextArea(jiggle.getPhasePicker().getProperties(gpl));
          }
          else {
              gpl = new GenericPropertyList(pickerPropsFilename, (String)null); // init loads props from disk file
              gpl.setFiletype("jiggle");
              setPickerPropsTextArea(gpl);
          }

          if (pickerClassnameField != null) 
            pickerClassnameField.setText(newProps.getProperty("picker.class."+selectedPickerName));

          if (pickerPropFileField != null) 
            pickerPropFileField.setText(newProps.getProperty("picker.props."+selectedPickerName));

          selectedPickerProps = gpl;

        }
    }
    /** Return array of picker name Strings declared by property names of 
     * of the form "picker.class."+pickerName that will be available to GUI. */
    public static String [] getPickerNames(GenericPropertyList gpl) {
        Enumeration e = gpl.propertyNames();
        int idx = -1;
        String prop = null;
        ArrayList aList = new ArrayList();
        while (e.hasMoreElements()) {
            prop = (String) e.nextElement();
            if (prop.startsWith("picker.class.")) {
              aList.add(prop.substring(13));
            }
        }
        return (String []) aList.toArray(new String[aList.size()]);
    }

    protected boolean hasChangedProps() {
        return hasChangedProps;
    }
    protected boolean hasChangedPicker() {
        return hasChangedPicker;
    }

    public boolean hasChanged() {
        return (hasChangedProps || hasChangedPicker);
    }

    /** Return the selected picker name.*/
    public String getSelectedPickerName() {
        return selectedPickerName;
    }

    /** Set the selected picker name.*/
    public void setSelectedPickerName(String name) {
        pickerNameBox.setSelectedItem(name);
    }

    public GenericPropertyList getSelectedPickerProps() {
        return selectedPickerProps;
    }

    private void doSetPickerProperties() {
        System.out.println("INFO: Properties dialog setting picker properties for: " + selectedPickerName);
        if (selectedPickerProps == null) selectedPickerProps = new GenericPropertyList(); 
        selectedPickerProps.setProperties( propsTextArea.getText() ); // parse properties from text area string
        hasChangedProps = true;
    }

    /** Populate the text area with picker properties.  */
    protected void setPickerPropsTextArea(GenericPropertyList gpl) {
        propsTextArea.setText("");
        if (gpl == null)  return;
        StringBuffer sb = new StringBuffer(1024);
        sb.append(gpl.listToString()).append("\n");
        propsTextArea.setText(sb.toString());
        propsTextArea.setCaretPosition(0);
    }

    private class SetSaveLoadPickerPropsActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

        if (selectedPickerName == null) return;

        String cmd = evt.getActionCommand();

        try {

          boolean success = true;

          if (cmd == "Set") {
            doSetPickerProperties();
          }
          else {
            if (newProps == null) {
                JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Cannot save/load picker properties, input Jiggle properties object is null", 
                           "Save/Load Picker Properties", JOptionPane.ERROR_MESSAGE);
                return;
            }

            JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            String filename = newProps.getUserFileNameFromProperty("picker.props."+selectedPickerName);
            String oldName = filename;
            if (filename != null) {
                jfc.setSelectedFile(new File(filename));
                oldName = jfc.getSelectedFile().getAbsolutePath();
            }
            File file = null;

            if (cmd == "Save") {
              if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                  doSetPickerProperties();
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  System.out.println("INFO: Saving user edited picker properties for: " +
                      selectedPickerName + " to file: " + filename);

                  //if (file.exists()) 
                  //    file.renameTo(file.getAbsolutePath()+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));

                  selectedPickerProps.setUserPropertiesFileName(filename);
                  success = selectedPickerProps.saveProperties("Saved by GUI"); 
              }
              else return; // nothing saved
            }
            else if (cmd == "Load") {
              if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  filename = file.getAbsolutePath();
                  if (! file.exists()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           filename + " file D.N.E. !", 
                           "Load Picker Properties", JOptionPane.ERROR_MESSAGE);
                     return;
                  }

                  System.out.println("INFO: Loading properties for: " + selectedPickerName +
                      " from file: " + filename);

                  if (selectedPickerProps == null) selectedPickerProps = new GenericPropertyList();
                  selectedPickerProps.setUserPropertiesFileName(filename);
                  selectedPickerProps.reset();
              }
              else return; // nothing loaded
            }
            else if (cmd == "Remove") {
                String str = propsTextArea.getSelectedText();
                if (str == null || str.length() == 0) return; // no-op
                StringTokenizer toke =  new StringTokenizer(propsTextArea.getSelectedText(), "=:");
                String key = null;
                if (toke.countTokens() > 0) {
                    key = toke.nextToken().trim();
                }
                key = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter a property name (selected text key) to remove from list", key);
                if (key != null && key.length() > 0) {
                    if (selectedPickerProps != null) selectedPickerProps.remove(key);
                }
                else return;  //no-op
            }

            if (oldName == null || ! oldName.equals(filename)) { // prompt to change newProps picker.props.<NAME> property
                if ( JOptionPane.showConfirmDialog(null,
                    "Set new filename as the picker.props." + selectedPickerName + " value?",
                    "Picker Properties Filename Changed",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                  //System.out.println(jfc.getCurrentDirectory().getAbsolutePath() + " : " + newProps.getUserFilePath());
                  if (jfc.getCurrentDirectory().getAbsolutePath().equals(newProps.getUserFilePath()) ) {
                      filename = jfc.getSelectedFile().getName(); // relative to jiggle USER dir
                  } // else its the full absolute directory path not in jiggle USER dir 
                  newProps.setProperty("picker.props."+selectedPickerName, filename);
                  pickerPropFileField.setText(newProps.getProperty("picker.props."+selectedPickerName));
                  hasChangedProps = true;
                }
             }

          } // end of else block

          if (! success) notifyFailure(cmd, "Picker");

          setPickerPropsTextArea(selectedPickerProps);

        }
        catch(Exception ex) {
          ex.printStackTrace();
          notifyFailure(cmd, "Picker");
        }
      }

    }

    private void notifyFailure(String cmd, String type) {
        JOptionPane.showMessageDialog(null,
            cmd + " failed , check text and messages!",
             type + " Properties", JOptionPane.PLAIN_MESSAGE);
    }
}

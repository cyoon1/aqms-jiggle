package org.trinet.jiggle;

import java.net.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import org.trinet.util.EpochTime;

/** Gets current Jiggle version number from the home server and compares it to the
* given version string. */
public class VersionChecker {

     static final String homeAddress = "http://pasadena.wr.usgs.gov/jiggle/";

     static final String defFile = "jiggleversion.html";

     static final String downloadAddress =
        homeAddress+"index.html";

     static String version = "unknown";
     static String address = homeAddress;
     static VersionChecker vc;

     public VersionChecker() {
        this(null);
     }

     public VersionChecker(String address) {

         this.address = (address == null) ? homeAddress : address;
         boolean badAddress = false;

         if (! address.endsWith("/") ) address = address + "/";

         try {
           URL url = new URL(this.address+defFile);

           BufferedReader in = new BufferedReader(
                               new InputStreamReader(
                               url.openStream()));

           version = (String) in.readLine();
           in.close();
         }
         catch (MalformedURLException ex) {
           badAddress = true;
         }
         catch (IOException ex) {
           badAddress = true;
         }
         if (badAddress) System.err.println("ERROR - VersionChecker init failed for address: " +
                 this.address + " version: " + version);

         vc = this;

     }
/** Parses version string of form "yyyy.MM.dd" and returns an double true seconds. */
     public static double versionStringToDate(String versionString) {
       return EpochTime.stringToEpoch(versionString, "yyyy.MM.dd"); // now UTC seconds - aww 2008/02/10
     }

     /** Return the URL of the Jiggle home server as a string. */
    public static String getAddress() {
       return address;
    }
     /** Return the URL of the Jiggle home server as a string. */
    public static String getDownloadAddress() {
       return downloadAddress;
    }
    /** Return the currently releases Jiggle version number. Returns "unknown"
    * if home URL cannot be reached. */
    public static String getVersion() {
      if (vc == null) {
        vc = new VersionChecker();
      }
      return version;
    }
    /** Compare 'thisVersion' to the version found at the home web site.
     Returns:<br>
     -1   if current version is older (out-of-date)<br>
      0   if versions match<br>
      1   if current version is newer <br>
    Returns 0 if Jiggle home site cannot be reached. */
    public static int versionDifference(String thisVersion) {
      double thisVer = versionStringToDate(thisVersion);
      String expStr = getVersion();
      if (expStr == null || expStr.equals("unknown")) return 0;

      double expectedVer = versionStringToDate(expStr);
      double diff = expectedVer - thisVer;
      if (diff == 0) return 0;
      if (diff < 0) return -1;
      return 1;
    }
    /** Returns true if 'thisVersion' matches the version found at the home web site.
    Assumes we're current and returns true if Jiggle home site cannot be reached. */
    public static boolean isCurrent(String thisVersion) {
       if (getVersion().equals("unknown")) return true;
       return getVersion().equals(thisVersion);
    }
/*
    public final static class Tester {
      public static void main(String args[]) {
        String versionNumber  = "2001.05.11";
        System.out.println(versionNumber+ "  "+VersionChecker.versionDifference(versionNumber));

        versionNumber  = "2003.05.05";
        System.out.println(versionNumber+ "  "+VersionChecker.versionDifference(versionNumber));

        versionNumber  = "2004.05.11";
        System.out.println(versionNumber+ "  "+VersionChecker.versionDifference(versionNumber));

//      if (!VersionChecker.isCurrent(versionNumber)) {
//          String msg = "WARNING: Your version of Jiggle is out-of-date.\n"+
//                       "Download latest version "+VersionChecker.getVersion()+ " from:\n"+
//                     VersionChecker.getAddress() ;
//        String title = "Jiggle Out of Date "+versionNumber;
//        JOptionPane.showMessageDialog(null,
//                                      msg, title,
//                                      JOptionPane.WARNING_MESSAGE);
//      }

     }
   } // end of Tester
*/
}

package org.trinet.filters;

import java.util.*;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;

import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * Abstract generic filter. Intended to provide the methods that are common to
 * all filters that deal with Waveform objects. The real filtering work is done
 * in filterSegment() that must be implemented in the concrete instances of this
 * class.
 * */
public abstract class AbstractWaveformFilter implements WaveformFilterIF {

    protected boolean copyInput = true;

    /** The instrument gain as derived from the waveforms ChannelGain.
    * @See: ChannelGain*/
    protected ChannelGain gain;

    /** Descriptive name of the filter. */
    protected String description = "UNDEFINED";

    protected int type = -1;

    /** The segment being filtered. */
    protected WFSegment seg;

    /** Samples per second for the current segment.*/
    protected double sps;

    /** The Waveform object to be filtered*/
    protected Waveform wfIn = null;

    /** Set true if taper should be applied before filtering. */
    protected boolean DefaultApplyTaper = false;
    protected boolean applyTaper = DefaultApplyTaper;
    protected double cosineTaperAlpha = 0.05;    // 0.0 < value < 1.0
    protected boolean reverse = false;

    protected AbstractWaveformFilter() { }

    public void setDescription(String str) {
      description = str;
    }

    public void copyInputWaveform(boolean tf) {
        copyInput = tf;
    }

    public boolean isReversed() {
        return reverse;
    }
    public void setReversed(boolean tf) {
        reverse = tf;
    }

    public void setOrder(int order) {}
    public int getOrder() {
        return 0;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
    public String getType(int type) {
        return FilterTypes.getType(type);
    }

    /** Name describing this Waveform data filter. */
    public String getDescription( ) {
      return description;
    }

    public void setApplyTaper(boolean tf) {
      applyTaper = tf;
    }

    public boolean getApplyTaper() {
      return applyTaper ;
    }

    /** Set 'alpha' of cosine taper, must be between 0.0 and 1.0
     * @see AmplitudeCosineTaper */
    public boolean setCosineTaperAlpha(double value) {
      if (value >= 0.0 && value <= 1.0) {
        cosineTaperAlpha = value;
        return true;
      }
      return false;
    }

    public double getCosineTaperAlpha() {
        return cosineTaperAlpha;
    }

    /** Return a filtered time series. If the object is not a Waveform the original
     *  object is returned unchanged. */
    public Object filter(Object in) {
      // only know how to filter Waveform objects so check.
      if (in instanceof Waveform) return filter((Waveform)in);
      return in;
    }

    public FilterIF getFilter(String type) {
        return (this.type == FilterTypes.getType(type)) ? this : null;
    }
    public FilterIF getFilter(float sampleRate) {
        return this;
    }
    public FilterIF getFilter(int sampleRate) {
        return getFilter((float)sampleRate);
    }
    public FilterIF getFilter(int type, int sampleRate) {
        return (this.type == type) ? this : null;
    }

    public FilterIF getFilter(Waveform wave) {
      return getFilter((float)wave.getSampleRate());
    }

    /** Filter one Waveform <bf>in place</bf>.
    * Returns reference to a copy of the input Waveform 
    * Filter fails and returns null when input is null,
    * input does not have a channel, channel does not have valid gain value, 
    * or if the sample units of the waveform and the gain do not match.
    */
    public Waveform filter(Waveform wf) {

      if (setWaveform(wf) == null) return null;

      Waveform wf2 = getWaveform();

      // Filter each segment, otherwise time gaps would wreak havoc.
      // filter modifies data samples stored in waveform segment list
      WFSegment seg[] = wf2.getArray();
      if (seg == null) return wf2; // no timeseries to filter, bail

      boolean wfSegFilterStatus = true; // bug fix 04/25/2005 aww, used to be false
      for (int i=0; i < seg.length; i++) {
          filterSegment(seg[i]);
          wfSegFilterStatus &= seg[i].isFiltered();
          if (! wfSegFilterStatus)  {
            System.err.println("DEBUG "+getClass().getName()+" WARNING failed for WFSegment: " + i);
            System.err.println("DEBUG wfseg: " + seg[i]);
          }
      }

      // Unless overridden by subtype, setFilterName forces also
      // sets waveform "isFiltered()" boolean attribute TRUE.
      wf2.setFilterName(getDescription());

      // set true only if filterSegment succeeded for all - what should default be, all or any? -aww
      wf2.setIsFiltered(wfSegFilterStatus);

      // Recalculate bias, max, min, etc. for "filtered" waveform
      wf2.scanForBias();
      wf2.scanForAmps();

      // set units only after all is said and done
      setUnits(wf2);

      return wf2;

    }

    protected void setGain(ChannelGain gain) {
      this.gain = gain;
    }

    protected ChannelGain getGain() {
      return gain;
    }

    /** Return the value of the gain corrected to CGS scaling. */
    public double getGainFactor(){
      //return gain.doubleValue(); // 02/22/2006 -aww
      //Scale gain by CGS inverse since counts/(units) 3,000,000 cnts/m/s -> 30,000 cnts/cm/s
      return Units.convertFromCommon(gain.doubleValue(), ChannelGain.getResultingUnits(gain));
    }

    /** Set the waveform to be filtered.
    * Returns null if input is null or does not have required filter attributes,
    * otherwise returns the reference to the waveform to be filtered. 
    */
    public Waveform setWaveform(Waveform wf) {

      // reset current references
      wfIn = null;
      gain = null;

      if (wf == null) {
        System.err.println("Filter: "+getDescription()+" Error : cannot filter null waveform.");
        return null;
      }

      Channel chan = wf.getChannelObj();
      if (chan == null) {
        System.err.println("Filter: "+getDescription()+" Error : cannot filter waveform with null channel.");
        return null; 
      }

      // check for legal waveform units
      if ( !unitsAreLegal(wf.getAmpUnits()) ) {
        String desc = getDescription()+" "+wf.getChannelObj().toDelimitedSeedNameString();
        System.err.println("Filter: "+desc+ " Error : invalid waveform Amp units of "+
                            Units.getString(wf.getAmpUnits()));
        return null;
      }

       // check for legal sample rate
      if (!sampleRateIsLegal(wf.getSampleRate())){
        String desc = getDescription()+" "+wf.getChannelObj().toDelimitedSeedNameString();
        System.err.println("Filter: "+desc+" Error: invalid waveform sample rate = "+wf.getSampleRate());
        return null;
      }

      //note: cgain is a ChannelGain object and is time dependent
      ChannelGain cgain = chan.getGain(wf.getEpochStart()); // should this do a db lookUp ?
      if (cgain.isNull()) {   // null or NaN
        String desc = getDescription()+" "+wf.getChannelObj().toDelimitedSeedNameString();
        System.err.println("Filter: "+desc+" Error: missing waveform gain for date: " +
                LeapSeconds.trueToString(wf.getEpochStart())); // UTC datetime -aww 2008/02/08
         return null;
      }
      setGain(cgain);

      wfIn = wf; // alias this instance member to input reference

      return wfIn; // all OK
    }

    /** If <i>copyInput</i> is <i>true</i> returns a copy of the waveform set by setWaveform(Waveform).
     * else returns the original.  Default value is <i>true</i>.
     * **/
    public Waveform getWaveform() {
      return (copyInput) ? AbstractWaveform.copyWf(wfIn) : wfIn;
    }

    /** Set the units of the resulting waveform (after filtering) to the units produced by this
    * filter. */
    abstract protected void setUnits(Waveform wf) ;

    /** Return true if the units of the actual samples in the waveform are
     * legal for this filter. */
    public boolean unitsAreLegal(Waveform wf) {
      return unitsAreLegal(wf.getAmpUnits());
    }

    /** Return true if the units of the raw waveform are legal for this filter. */
    abstract public boolean unitsAreLegal(int units) ;

    public void setSampleRate(int sampleRate) {
       setSampleRate((double) sampleRate);
    }

    /** Set the sample rate for the current segment. */
    public void setSampleRate(double sampleRate) {
        // sps = (wfIn == null) ? 0.0 : sampleRate;
        sps = sampleRate;      // DDG 6/2/06 need to set BEFORE WF is set  
    }
 
    /** Integral rounded rate in samples per second. */
    public int getSampleRate() {
      return (int)Math.round(sps);
    }

    /** Seconds per sample. */
    protected double getSampleInterval() {
      return 1.0/sps;
    }

    /** Sampling rate. */
    public double getSps() {
      return sps;
    }

    protected void setWFSegment(WFSegment seg) {
      this.seg = seg;
      setSampleRate(1.0/seg.getSampleInterval());
    }

    /** Filter one WFSegment. This is where the actual filtering work is done. */
    abstract protected WFSegment filterSegment(WFSegment seg) ;

    /** Returns true if the given sample rate is legal for this filter. */
    abstract protected boolean sampleRateIsLegal(double rate) ;
    /*
    // Override this if your filter is valid for other than 20, 80, & 100 sps.
    protected boolean sampleRateIsLegal(double rate) {
      return (rate  ==  20.0 || rate  ==  80.0 || rate  == 100.0 ) ;
    }
    */

    /** Return this timeseries demeaned (bias removed).
    * A bias will be calculated over the whole time series and then removed. */
    public static float[] demean(float[] ts) {
      return demean(ts, calcBias(ts));
    }

    /** Return this timeseries demeaned (bias removed).
    * A bias will be calculated from startSample to endSample and then removed. */
    public static float[] demean(float[] ts, int startSample, int endSample) {
      return demean(ts, calcBias(ts, startSample, endSample));
    }

    /** Return this timeseries demeaned (bias removed). */
    public static float[] demean(float[] ts, double bias) {
      for (int i = 0; i < ts.length; i++) {
        ts[i] -= bias;
      }
      return ts;
    }

    /** Calculate the bias for the time series from startSample to endSample. */
    public static float calcBias(float[] ts) {
      return calcBias(ts, 0, ts.length)  ;
    }

    /** Calculate the bias for the time series from startSample to endSample.
        Returns 0.0 if nonsense start or end samples are given. */
    public static float calcBias(float[] ts, int startSample, int endSample) {
      // sanity checks
      if (ts.length < startSample ||
          ts.length < endSample ||
          startSample < 0 ||
          endSample < startSample) return 0.0f;

      double sum = 0;
      for (int i = startSample; i < endSample; i++) {
        sum += ts[i];
      }
      return (float) (sum / (double) (endSample - startSample));
    }

    // data dump for debugging
    public static void debugDump(Waveform wfx) {

      if (wfx == null) {
        System.out.println("AbstractWaveformFilter debugDump input Waveform is NULL. ");
        return;
      }

      // name set after wfx is filtered
      System.out.println("AbstractWaveformFilter debugDump FilterType = "+wfx.getFilterName());
      System.out.println("  Channel = "+wfx.getChannelObj().toDumpString());

      System.out.print("  Waveform timeSpan = "+wfx.getTimeSpan().toString());
      if (wfx.hasTimeSeries()) {
        System.out.print(" : timeseries nsegs =  "+ wfx.getSegmentList().size() );
      } else {
        System.out.print(" : no timeseries.");
      }
      //System.out.println("Expected samps = "+ wfx.sampleCount.toString() );
      System.out.print("  Actual samps   = "+ wfx.samplesInMemory() );
      System.out.print(" max = "+ wfx.getMaxAmp());
      System.out.print(" min = "+ wfx.getMinAmp());
      System.out.println(" bias = "+ wfx.getBias());  //
      //System.out.println("  gain factor = "+wfx.getChannelObj().getGain(wfx.getEpochStart()));

      float sample[] ;

      WFSegment segs[] = wfx.getArray();
      for (int i = 0; i < segs.length; i++) {
        System.out.println( segs[i].toString() );
        sample = segs[i].getTimeSeries();
        for (int k = 0; k < 10; k++){
          System.out.print(" "+sample[k]);
        }
        System.out.println();
      }
    }

    /**
    * Convert raw waveform (in counts) to ground motion units (as described by the gain) and
    * high pass filter using Hiroo's "H1". Works with any sample rate, either vel or accel.<p>
    * WARNING: do not pass this method a timeseries that has already been converted.
    * */

    static final double qHighPass = 0.998;   // qb, qp, qs

    protected float[] convertToGroundMotion(float[] ts) {

      if (ts.length < 3) return ts;

      float newts[] = new float[ts.length];
      double gf = getGainFactor();
      double b0 = (2.0 / (1.0 + qHighPass)); // reciprocol of (1 + q) / 2
      double devisor = b0 * gf;  // moosh these together of efficiency

      for (int i = 1; i < ts.length -2; i++) {
        newts[i] = (float) ( (ts[i] - ts[i-1])/devisor + qHighPass * newts[i-1] );
      }
//    newts[0] = newts[1]; // backfill the 1st sample

      // Set waveform units to the correct type - noop below since a copy, not the original
      //getWaveform().setAmpUnits(ChannelGain.getResultingUnits(getGain())); // no such method in ChannelGain!

      return newts;
    }

    public String toString() {
      return getDescription() + " " +
          " type: " + type +
          " isTapered = "+ getApplyTaper() +
          " alpha: " + cosineTaperAlpha +
          " reversed: " + isReversed() +
          " sps: " + sps +
          " gain: " + gain;
    }
}

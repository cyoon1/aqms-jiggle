package org.trinet.filters;
import org.trinet.util.FilterIF;

public class FilterTypes {

    public static final int HIGHPASS            = 0;
    public static final int BANDPASS            = 1;
    public static final int NARROW_BANDPASS     = 2;
    public static final int LOWPASS             = 3;
    public static final int VERY_LOWPASS        = 4;
    public static final int NOTCH               = 5;
    public static final int WOOD_ANDERSON       = 6;
    public static final int WOOD_ANDERSON_BP    = 7;
    public static final int WOOD_ANDERSON_HP    = 8;
    public static final int WOOD_ANDERSON_EH    = 9;
    public static final int VELOCITY            = 10;
    public static final int ACCELERATION        = 11;
    public static final int DISPLACEMENT        = 12;
    public static final int SP03                = 13;
    public static final int SP10                = 14;
    public static final int SP30                = 15;
    public static final int WA_EHK              = 16;
    public static final int EHG                 = 17;

    public static final String [] TYPES = {
        "HIGHPASS", "BANDPASS", "NARROW_BANDPASS", "LOWPASS", "VERY_LOWPASS",
        "NOTCH", "WOOD-ANDERSON", "WOOD-ANDERSON_BP", "WOOD-ANDERSON_HP", "WOOD-ANDERSON_EH",
        "VELOCITY", "ACCELERATION", "DISPLACEMENT", "SP0.3", "SP1.0",
        "SP3.0", "WA_EHK", "EHG"
    };

    public static int getType(String ftype) {
        for (int ii=0; ii< TYPES.length; ii++) {
            if ( TYPES[ii].equalsIgnoreCase(ftype)) return ii;
        }
        return -1;
    }

    public static String getType(int ftype) {
        return (ftype >= 0 && ftype < TYPES.length) ? TYPES[ftype] : "UNKNOWN"; 
    }

    public static FilterIF getFilter(int ftype, int sampleRate, boolean reverse) {
        FilterIF f = null;
        switch (ftype) {
            case HIGHPASS:
            case BANDPASS:
            case NARROW_BANDPASS:
            case LOWPASS:
            case VERY_LOWPASS:
            case NOTCH:
                f = new ButterworthFilterSMC(ftype, sampleRate, reverse);
                break;
            case WOOD_ANDERSON:
                f = new WAFilter(ftype, sampleRate);
                break;
            case WOOD_ANDERSON_BP:
                f = new WABandPassFilter(ftype, sampleRate);
                break;
            case WOOD_ANDERSON_HP:
                f = new WAHighPassFilter(ftype, sampleRate);
                break;
            case WOOD_ANDERSON_EH:
                f = new WA_EHFilter(ftype, sampleRate);
                break;
            case VELOCITY:
                f = new VelocityFilter(ftype, sampleRate);
                break;
            case ACCELERATION:
                f = new AccelFilter(ftype, sampleRate);
                break;
            case DISPLACEMENT:
                f = new DisplacementFilter(ftype, sampleRate);
                break;
            case SP03:
            case SP10:
            case SP30:
                f = new RSAFilter(ftype, sampleRate);
                break;
            case WA_EHK:
                f = new WA_EHKFilter(ftype, sampleRate);
                break;
            case EHG:
                f = new EHGFilter(ftype, sampleRate);
                break;
        }
        //System.out.println("FilterTypes getFilter: " + ftype + " filter: " + ((f == null) ? "null" : f.getClass().getName()));
        return f;
    }
}


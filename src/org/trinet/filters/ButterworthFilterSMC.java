package org.trinet.filters;

import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;
import org.trinet.util.FilterIF;
import org.trinet.util.FloatFilterIF;
import org.trinet.util.NotchFilterIF;
import org.trinet.util.WaveformBandpassFilterIF;

/**
* There is a fundamental difference in the shapes of a causal and acausal filter.
* The response for a causal filter is given by eq. 15.8-6 in Kanasewich:<p>
*    Y = sqrt((f/fcut)**(2*n)/(1+(f/fcut)**(2*n))), where n = 2*nroll<p>
* The acausal filter is obtained by applying the causal filter twice.
* The consequence is that the response of the acausal filter is<p>
*    Y =      (f/fcut)**(2*n)/(1+(f/fcut)**(2*n)),  where n = 2*nroll<p>
* Note that for the same nroll as input, for f/fcut very much less than 1.
* the causal filter goes as<p>
*    Y -> (f/fcut)**(2*nroll)<p>
* whereas the acausal filter falls off more rapidly:<p>
*    Y -> (f/fcut)**(4*nroll)<p>
* Also note the difference when f = fcut:<p>
*   causal:  Y = 1/sqrt(2)<br>
*  acausal:  Y = 1/2<p>
* This is true no matter what value is chosen for nroll.  Because of this,
* it is not possible to make the response of the causal and acausal filters the
* same.  In particular, caution should be used if nroll is chosen so as to
* make the low-frequency asymptotes the same.  This requires nroll for the 
* acausal filter to be half that of the causal filter.  The problem is that
* the response for frequencies higher than fcut will be reduced for the
* acausal as compared to the causal filter.<p>
* */

public class ButterworthFilterSMC implements WaveformBandpassFilterIF, NotchFilterIF, FloatFilterIF, Cloneable {

    /** Descriptive string */
    private static final String BUTTERWORTH = "BW"; // Butterworth

    private static final double PI = Math.PI;

    public static final int MAX_POLES = 16;
    public static final double cosineTaperAlpha = 0.05;
    public static final int defaultSps = 100;

    private int type = FilterTypes.HIGHPASS;
    private boolean reverse = false;
    private int sampleRate = defaultSps;
    private String description = BUTTERWORTH;

    private boolean copyInput = false;

    // Filter variables configured by input parameters or properties
    private int nroll = 1;
    private double lf = 1.;
    private double hf = 0.;
    private double rf = 0.0; // notch freq
    private double dw = 0.01; // notch width


    // Filter type dependent variables:
    private double [] fact = new double[16];
    private double [] b1 = new double[16];
    private double [] b2 = new double[16];

    private double notchScalar = 0.;
    private double x1 = 0.;
    private double x2 = 0.;
    private double y1 = 0.;
    private double y2 = 0.;


    public ButterworthFilterSMC() {
    }

    public ButterworthFilterSMC(boolean enableReverseFiltering) {
         this(FilterTypes.HIGHPASS, defaultSps, enableReverseFiltering);
    }

    public ButterworthFilterSMC(int type, int sampleRate) {
       this(type, sampleRate, false);
    }

    public ButterworthFilterSMC(int type, boolean enableReverseFiltering) {
       this(type, defaultSps, enableReverseFiltering);
    }

    public ButterworthFilterSMC(int type, int sampleRate, boolean enableReverseFiltering) {
       this.sampleRate = sampleRate;
       setType(type);        // must set type BEFORE coefs
       setReversed(enableReverseFiltering);
    }

    /** If set true time-series will be reverse filtered to remove phase shift. */
    public void setReversed(boolean tf) {
        reverse = tf;
    }

    /** Return true if time-series will be reverse filtered (acausal) to remove phase shift. */
    public boolean isReversed() {
        return reverse;
    }

    /** Returns lowpass corner frequency value, 0 if none. */
    public double getLoHz() {
        return lf;
    }

    /** Returns highpass corner frequency value, 0 if none. */
    public double getHiHz() {
        return hf;
    }

    /** Returns notch reject frequency value, 0 if none. */
    public double getNotchHz() {
        return rf;
    }

    /** Returns notch width. */
    public double getNotchWidth() {
        return dw;
    }

    public int getOrder() {
        return 2*nroll;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public double getSps() {
        return (double)sampleRate;
    }

    public void setLoHz(double hz) {
        lf = hz;
        setCoefs(sampleRate);
    }

    public void setHiHz(double hz) {
        hf = hz;
        setCoefs(sampleRate);
    }

    public void setPassBand(double loHz, double hiHz) {
        lf = loHz;
        hf = hiHz;
        setCoefs(sampleRate);
    }

    public void setNotch(double hz, double dwidth) {
        rf = hz;
        dw = dwidth;
        setCoefs(sampleRate);
    }

    public void setOrder(int order) {
        this.nroll = order/2;
        setCoefs(sampleRate);
    }

    public void setSampleRate(double sampleRate) {
        setSampleRate((int)Math.round(sampleRate));
    }
    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
        setCoefs(sampleRate);
    }

    public String getDescription( ) {
        return description;
    }
 
    public void setDescription(String str) {
        description = str;
    }

    public int getType() {
        return type;
    }

    public String getBandString() {
        String typeStr = null;
        if (type == FilterTypes.HIGHPASS) {
             typeStr = "_" + lf + "_" + 2*nroll; 
        }
        else if (type == FilterTypes.BANDPASS) {
             typeStr = "_"+lf+"_"+hf + "_" + 2*nroll;
        }
        else if (type == FilterTypes.NARROW_BANDPASS) {
             typeStr = "_"+lf+"_"+hf + "_" + 2*nroll;
        }
        else if (type == FilterTypes.LOWPASS) {
             typeStr = "_" + hf + "_" + 2*nroll; 
        }
        else if (type == FilterTypes.VERY_LOWPASS) {
             typeStr = "_" + hf + "_" + 2*nroll; 
        }
        else {
            typeStr = "_"+rf+"_"+dw; 
        }
        return typeStr;
        
    }

    /** Set default filter type e.g. FilterTypes.HIGHPASS, FilterTypes.LOWPASS. */
    public void setType(int filterType) {
        type = filterType;
        nroll = 2;
        if (type == FilterTypes.HIGHPASS) {
             lf = 1.0;
             hf = 0.0;
        }
        else if (type == FilterTypes.BANDPASS) {
             lf = 0.5;
             hf = 19.99; // hf used to be 10. -aww 2011/04/12
             nroll = 2;
        }
        else if (type == FilterTypes.NARROW_BANDPASS) {
             lf = 0.5;
             hf = 10.0; // hf used to be 5. -aww 2011/04/12
             nroll = 2;
        }
        else if (type == FilterTypes.LOWPASS) {
             lf = 0.0;
             hf = 8.0;
        }
        else if (type == FilterTypes.VERY_LOWPASS) {
             lf = 0.0;
             hf = 1.0;
        }
        else {
            lf = 0.0;
            hf = 0.0;
            rf = .2;
            dw = 0.01;
            nroll = 1;
        }
        setCoefs(sampleRate);
    }

    private void setTypeString(String typeStr) {
        description = BUTTERWORTH + "_" + typeStr + getBandString();
    }

    public void setDefaultHighpass() {
      setType(FilterTypes.HIGHPASS);
    }
    public void setDefaultLowpass() {
      setType(FilterTypes.LOWPASS);
    }
    public void setDefaultBandpass() {
      setType(FilterTypes.BANDPASS);
    }
    public void setDefaultNotch() {
      setType(FilterTypes.NOTCH);
    }

    /** Set gain and filter coefficients for the given sample rate.  */
    private boolean setCoefs(int sampleRate) {
        this.sampleRate = sampleRate;
        boolean status = false;
        if (type == FilterTypes.HIGHPASS) {
            status = setCoefsHP(lf, sampleRate, nroll);
        }
        else if (type == FilterTypes.BANDPASS || type == FilterTypes.NARROW_BANDPASS) {
            status = setCoefsBP(lf, hf, sampleRate, nroll);
        }
        else if (type == FilterTypes.LOWPASS || type == FilterTypes.VERY_LOWPASS) {
            status = setCoefsLP(hf, sampleRate, nroll);
        }
        else { // if (type == FilterTypes.NOTCH) {
            status = setCoefsN(rf, sampleRate, dw);
        }

        //if (! status) throw new IllegalArgumentException("Invalid filter for rate: " + sampleRate);

        return status;
    }

    private void zeroCoefs() {
        for (int i=0; i < MAX_POLES; i++) {
            fact[i] = 0.;
            b1[i] = 0.;
            b2[i] = 0.;
        }
        notchScalar = 0.;
        zeroBuffers();
    }

    private void zeroBuffers() {
        x1 = 0.;
        x2 = 0.;
        y1 = 0.;
        y2 = 0.;
    }

    /** Return new filter instance with this instance's type settings for the input sample rate. */
    public FilterIF getFilter(int sampleRate) {
        ButterworthFilterSMC f = null;
        f = (ButterworthFilterSMC) this.clone(); // uses current type settings
        f.setSampleRate(sampleRate);
        return f; // used to be new ButterworthFilterSMC(type, sampleRate, reverse); // uses defaults for current type
    }

    /** Return new default filter instance for the input type and sample rate. */
    public FilterIF getFilter(int type, int sampleRate) {
        //System.out.println("DEBUG BW-SMC type: " + type + " rate: " + sampleRate);
        return new ButterworthFilterSMC(type, sampleRate, reverse);
    }

    /** Returns new instance of type specified by an input string type.
     * Input string must start with filter type like "HIGHPASS", case insensitive.
     * Filter coefficients are set to those of the current values of the
     * instance current sample rate and reverse filtering members.
     * */
    public FilterIF getFilter(String type) {
        return getFilter(FilterTypes.getType(type), sampleRate);
    }

    /** Returns input with its timeseries filtered in place.
     *  If input is not a float[] or Waveform the original object is
     *  returned unchanged. */
    public Object filter(Object in) {
       if (in instanceof float[]) return filter((float []) in);
       if (in instanceof Waveform) return filter((Waveform)in);
       return in;
    }

    public void copyInputWaveform(boolean tf) {
        copyInput = tf;
    }
    
    /** Returns a Waveform with time series filtered.
     * The original timeseries in the Waveform is altered by the filter
     * unless waveform copy flag is set <i>true</i> for this instance,
     * default value is <i>false</i>.
     * Returns 'null' if filtering fails.
     * @see #copyInputWaveform(boolean)
     * */
    public Waveform filter(Waveform wf) {

        if (wf == null) return null;

        Waveform wf2 = (copyInput) ? wf.copy() : wf; 
        if (wf2 == null) return null;

        //int rate = wf2.getSampleRate();
        //if (rate != getSampleRate()) setSampleRate(rate);

        WFSegment seg[] = wf2.getArray();
        if (seg != null) {
            for (int i=0; i < seg.length; i++) {
              filterSegment(seg[i]);
            }
            wf2.setFilterName(getDescription()); // sets isFiltered() == true
            wf2.scanForBias();
            wf2.scanForAmps();
        }

        return wf2;
    }

    protected WFSegment filterSegment(WFSegment seg) {
        //assume waveform sample rate is the same for all segments or do this:
        int rate = (int) Math.round(1.0/seg.getSampleInterval());
        if (rate != getSampleRate()) {
            setSampleRate(rate);
        }

        seg.setTimeSeries( filter(seg.getTimeSeries()) ); // setTimeSeries() recalcs bias, max, min.
        seg.setIsFiltered(true);
        return seg;
    }

    /** Remove bias (mean) from the time series in place. */
    private float [] demean(float [] values) {

        if (values == null) return values;
        int size = values.length;
        if (size == 0) return values;

        // calculate mean
        double sum = 0.;
        for (int idx = 0; idx < size; idx++) {
            sum += values[idx];
        }
        double mean = Math.round(sum/size);

        // remove mean
        for (int idx = 0; idx < size; idx++) {
            values[idx] = (float) (values[idx] - mean);
        }
        return values;

    }

    // STATIC METHODS TO CREATE FILTER TYPES FROM CONSTRUCTOR 
    public static ButterworthFilterSMC createBandpass(int rate, double lf, double hf, int norder, boolean reverse) {
        ButterworthFilterSMC bwf = new ButterworthFilterSMC(); 
        return bwf.setBandpass(rate, lf, hf, norder/2, reverse) ? bwf : null;
    }
    public static ButterworthFilterSMC createHighpass(int rate, double lf, int norder, boolean reverse) {
        ButterworthFilterSMC bwf = new ButterworthFilterSMC(); 
        return bwf.setHighpass(rate, lf, norder/2, reverse) ? bwf : null;
    }
    public static ButterworthFilterSMC createLowpass(int rate, double hf, int norder, boolean reverse) {
        ButterworthFilterSMC bwf = new ButterworthFilterSMC(); 
        return bwf.setLowpass(rate, hf, norder/2, reverse) ? bwf : null;
    }
    public static ButterworthFilterSMC createNotch(int rate, double rf, double dw, boolean reverse) {
        ButterworthFilterSMC bwf = new ButterworthFilterSMC(); 
        return bwf.setNotch(rate, rf, dw, reverse) ? bwf : null;
    }

    /** Change the type and configuration parameters of this filter instance.
    * Return false, a no-op, if norder &LT;1 || sampleRate &LT;=0 || (lf &LT;= 0 &AMP;&AMP; hf &LT;=0).<br> 
    * if input hf=0 setup as a highpass type.<br>
    * if input lf=0 setup as a lowpass type.<br>
    * otherwise setup as a bandpass type.<br>
    */
    public boolean setFilter(int sampleRate, double lf, double hf, int norder, boolean reverse) {
        //System.out.println("INFO: setFilter rate: " + sampleRate + " lf: " + lf + " hf: " + hf + " order: " + norder);
        if (norder < 1 || sampleRate <= 0.) return false;
        if (hf == 0.) return setHighpass(sampleRate, lf, norder/2, reverse); // locut
        if (lf == 0.) return setLowpass(sampleRate, hf, norder/2, reverse); // hicut
        return setBandpass(sampleRate, lf, hf, norder/2, reverse); // bandpass
    }
    private boolean setBandpass(int sampleRate, double lf, double hf, int nroll, boolean reverse) {
        if ( (lf <= 0. && hf <= 0.) ||  lf == hf) return false; 
        this.reverse = reverse;
        this.type = FilterTypes.BANDPASS;
        return setCoefsBP(lf, hf, sampleRate, nroll);
    }
    private boolean setHighpass(int sampleRate, double lf, int nroll, boolean reverse) {
        if (lf <= 0. || nroll < 1 || sampleRate <= 0.) return false;
        this.reverse = reverse;
        this.type = FilterTypes.HIGHPASS;
        return setCoefsHP(lf, sampleRate, nroll);
    }
    private boolean setLowpass(int sampleRate, double hf, int nroll, boolean reverse) {
        if (hf <= 0. || nroll < 1 || sampleRate <= 0.) return false;
        this.reverse = reverse;
        this.type = FilterTypes.LOWPASS;
        return setCoefsLP(hf, sampleRate, nroll);
    }
    public boolean setNotch(int sampleRate, double rf, double dw, boolean reverse) {
        if (rf <= 0. || dw <= 0. || dw >= 1. || sampleRate <= 0.) return false;
        this.reverse = reverse;
        this.type = FilterTypes.NOTCH;
        return setCoefsN(rf, sampleRate, dw);
    }

    private float filterSample(float ts, int k) {
        double fp = (double) ts;
        switch (type) {
            case FilterTypes.HIGHPASS:
                fp = filterSampleHP(fp, k);
                break;
            case FilterTypes.BANDPASS:
            case FilterTypes.NARROW_BANDPASS:
                fp = filterSampleBP(fp, k);
                break;
            case FilterTypes.LOWPASS:
            case FilterTypes.VERY_LOWPASS:
                fp = filterSampleLP(fp, k);
                break;
            case FilterTypes.NOTCH:
                fp = filterSampleN(fp);
                break;
        }

        y2 = y1; // previous filtered
        y1 = fp; // current filtered
        x2 = x1; // previous unfiltered
        x1 = (double) ts; // current unfiltered

        return (float) fp;
    }

    private double filterSampleBP(double ts, int k) {
        return (fact[k-1]*(ts - x2) - b1[k-1]*y1 - b2[k-1]*y2);
    }
    private double filterSampleHP(double ts, int k) {
        return (fact[k-1]*(ts - 2.*x1 + x2) - b1[k-1]*y1 - b2[k-1]*y2);
    }
    private double filterSampleLP(double ts, int k) {
        return (fact[k-1]*(ts + 2.*x1 + x2) - b1[k-1]*y1 - b2[k-1]*y2);
    }
    private double filterSampleN(double ts) {
        return (fact[0] * (ts + notchScalar*x1 + x2) - b1[0]*y1 - b2[0]*y2);
    }

    private int getFilteredSeriesLength(float [] ts) {
        if (! reverse) return ts.length;
        double samps = 0.;
        switch (type) {
            case FilterTypes.HIGHPASS:
                samps = (3.*nroll*sampleRate)/lf; // highpass
                break;
            case FilterTypes.LOWPASS:
            case FilterTypes.VERY_LOWPASS:
                samps = (3.*nroll*sampleRate)/hf; // lowpass
                break;
            case FilterTypes.BANDPASS:
            case FilterTypes.NARROW_BANDPASS:
                samps = Math.max( (3.*nroll*sampleRate)/lf, (6.*nroll*sampleRate)/(hf-lf) ); // bandpass
                break;
            case FilterTypes.NOTCH:
                samps = 3.*Math.PI/dw; // notch
                break;
        }
        return ts.length + (int) Math.floor(samps);
    }

    /**
     * Filter input timeseries values and return input array with filter values.
     * Demeans values and applies a cosine taper to the input values.
     * Forward filters input (causal), and when reverse filtering is enabled (acausal) 
     * <i>reverse==true</i>, reverse filters the forward output and returns result.
     * @see #setReversed(true).
     * */
    public float[] filter(float [] in) {
        float [] values = demean(in);
        values = org.trinet.util.AmplitudeCosineTaper.taper(values, cosineTaperAlpha);

        int ns = getFilteredSeriesLength(in);

        if (reverse) {
            values = new float[ns];
            System.arraycopy(in,0,values,0,in.length); // create expanded array with extra padded length
        }

        values = doFilter(values);

        if (reverse) {
            values = doReverseFilter(values);
            System.arraycopy(values,0,in,0,in.length); // copy reversed series to original 
        }

        return in;
       
    }

    private int getCoefCount() {
        return (type == FilterTypes.BANDPASS || type == FilterTypes.NARROW_BANDPASS) ? 2*nroll : nroll;
    }

    /** Forward filter input timeseries in place and return result. */
    private float [] doFilter(float [] in) {
        if (in == null) return in;
        int size = in.length;
        int knt = getCoefCount();
        // For each pole-zero stage filter timeseries
        //System.out.println("DEBUG BW-SMC len: " + size + " doFilter npoles: " + 2*knt);
        for (int k = 1; k <= knt; k++) {
          //System.out.println("DEBUG k: " +k+ " fact: " +fact[k-1]+ " b1: " +b1[k-1] + " b2: " + b2[k-1]);
          zeroBuffers();
          for (int idx=0; idx < size; idx++) {
            in[idx] = filterSample(in[idx], k);
          }
        }
        return in;
    }

    /** Acausal filter, reverse filter input timeseries in place and return result. */
    private float [] doReverseFilter(float [] in) {
        if (in == null) return in;
        int size = in.length;
        int knt = getCoefCount();
        // For each pole-zero stage filter timeseries
        //System.out.println("DEBUG BW-SMC len: " + size + " doReverseFilter npoles: " + knt*2);
        for (int k = 1; k <= knt; k++) {
            zeroBuffers();
            for (int idx=size-1; idx > -1; idx--) {
              in[idx] = filterSample(in[idx], k);
            }
        }
        return in;
    }

    /**
    *  Notch filter (see Kanasewich, Time Sequence Analysis in 
    *    Geophysics, Third Edition, University of Alberta Press, 
    *    1981; Shanks, Feb. 1967 Geophysics).
    *  Adapted from NOTCH.FOR SMC Written by W. B. Joyner 01/07/97.
    *  Input rf is the reject frequency and its notch dwidth must be a small value less than 1,
    *  the smaller the dwidth value, the narrower the notch, .01 suggested for trial value.
    */
    //For reverse filtering (acausal filter) the timeseries is extended by 3.*PI/dwidth.
    private boolean setCoefsN(double rf, int sampleRate, double dwidth) {

        if (sampleRate <= 0 || sampleRate <= 2*rf) {
            System.out.println(FilterTypes.getType(this.type)+
                    " rf: "+rf+" rate: "+sampleRate+" <-- invalid sampleRate");
            return false;
        }

        zeroCoefs();

        this.sampleRate = sampleRate;
        this.nroll = 1;
        this.rf = rf;
        this.dw = dwidth;

        double r = 1.+dw;
        double w = 2.*Math.PI*rf/sampleRate;
        double cosw = Math.cos(w);
        this.notchScalar = -2.*cosw;

        fact[0] = (1. + 2.*cosw/r + 1./(r*r))/(2.*(1.+cosw));
        b1[0] = notchScalar/r;
        b2[0] = 1./(r*r);
        
        setTypeString(FilterTypes.getType(type));

        return true;
    }

    /**
    * Butterworth bandpass filter npoles = 2*norder (norder&LT;=8)
    * (see Kanasewich, Time Sequence Analysis in Geophysics, Third Edition, 
    *    University of Alberta Press, 1981).
    *  Adpated from BAND.FOR written by W.B. Joyner 01/07/97.
    *  Input lf, hf are the low and high cutoff frequencies hertz.
    *  Causal, use reverse filter (acausal) for zero phase shift.
    */
    // For reverse, acausal filter, input timeseries array is extended by:
    // MAX( (3.*norder*sampleRate)/lf, (6.*norder*sampleRate)/(hf-lf) )
    private boolean setCoefsBP(double lf, double hf, int sampleRate, int nroll) {

        if (sampleRate <= 0 || sampleRate <= 2*lf || sampleRate <= 2*hf) {
            System.out.println(FilterTypes.getType(this.type)+
                    " lf: "+lf+" hf:"+hf+" rate: "+sampleRate+" <-- invalid sampleRate");
            return false;
        }

        zeroCoefs();

        this.sampleRate = sampleRate;
        this.lf = lf;
        this.hf = hf;
        this.nroll = nroll;

        double dt = 1./(double)sampleRate;
        double w1 = 2.*PI*lf;
        w1 = 2.*Math.tan(w1*dt/2.)/dt;

        double w2 = 2.*PI*hf;
        w2 = 2.*Math.tan(w2*dt/2.)/dt;
  
        double pre = 0.;
        double pim = 0.;
        double argre = 0.;
        double argim = 0.;
        double rho = 0.;
        double theta = 0.;
        double sjre = 0.;
        double sjim = 0.;
        double bj = 0.;
        double cj = 0.;
        double con = 0.;

        for (int k=1; k <= nroll; k++) {
            pre = -Math.sin( PI*(double)(2*k-1)/(double)(4*nroll) );
            pim =  Math.cos( PI*(double)(2*k-1)/(double)(4*nroll) );
            argre = (pre*pre-pim*pim) * ((w2-w1)*(w2-w1))/4.- w1*w2;
            argim = 2.* pre*pim * (w2-w1)*(w2-w1)/4.;
            rho = Math.pow((argre*argre + argim*argim),0.25);
            theta = PI+Math.atan2(argim,argre)/2.;

            for (int i=1; i <= 2; i++) {
                sjre = pre*(w2-w1)/2.+Math.pow(-1,i)*rho*(-Math.sin(theta-PI/2.));
                sjim = pim*(w2-w1)/2.+Math.pow(-1,i)*rho*Math.cos(theta-PI/2.);
                bj = -2.*sjre;
                cj = sjre*sjre+sjim*sjim;
                con = 1./(2./dt+bj+cj*dt/2.);
                fact[2*k+i-3] = (w2-w1)*con;
                b1[2*k+i-3] = (cj*dt-4./dt)*con;
                b2[2*k+i-3] = (2./dt-bj+cj*dt/2.)*con;
            }

        }

        setTypeString(FilterTypes.getType(type));

        return true;
    }

    /**
    * Butterworth lowpass filter (norder &LT;=8) (see Kanasewich, 
    * Time Sequence Analysis in Geophysics, Third Edition, 
    * University of Alberta Press,1981)
    * Adapted form HICUT.FOR in SMC package written by W. B. Joyner 01/07/97
    * Input hf is the high cutoff frequency hertz.
    * Reverse filter for zero phase shift (acausal)
    */
    // For acausal reverse filter  timeseries length is extended by +(3.*norder*sampleRate)/hf
    private boolean setCoefsLP(double hf, int sampleRate, int nroll) {

        if (sampleRate <= 0 || sampleRate <= 2*hf) {
            System.out.println(FilterTypes.getType(this.type)+
                    " hf:"+hf+" rate: "+sampleRate+" <-- invalid sampleRate");
            return false;
        }

        zeroCoefs();

        this.lf = 0.;
        this.hf = hf;
        this.sampleRate = sampleRate;
        this.nroll = nroll;

        double dt = 1./(double)sampleRate;
        double w1 = 2.*Math.PI*hf;
        w1 = 2.*Math.tan(w1*dt/2.)/dt;

        double d = 0;
        double d2 = Math.pow(2./(w1*dt),2);

        for (int k = 1; k <= nroll; k++) {
          d  = Math.sin(Math.PI*(double)(2*k-1)/(double)(4*nroll))/(w1*dt);
          fact[k-1] = 1. / (1. + d2 + 4.*d);
          b1[k-1]   = 2. * fact[k-1] * (1.- d2);
          b2[k-1]   = fact[k-1] * (1. + d2 - 4.*d);
        }

        setTypeString(FilterTypes.getType(type));

        return true;
    }

    /**
    *  Butterworth highpass filter (norder &LT;= 8)
    * (see Kanasewich, Time Sequence Analysis in Geophysics, Third Edition, University of Alberta Press, 1981).
    * Adapted from LOCUT.FOR in SMC package written by W. B. Joyner 01/07/97.
    * Input lf is the low cutoff frequency hertz.
    * Reverse filter for zero phase shift (acausal)
    */
    // For acausal filtering, timeseries array is extended to length of (3.*norder*sampleRate)/lf.
    private boolean setCoefsHP(double lf, int sampleRate, int nroll) {

        if (sampleRate <= 0 || sampleRate <= 2*lf) {
            System.out.println(FilterTypes.getType(this.type)+
                    " lf: "+lf+" rate: "+sampleRate+" <-- invalid sampleRate");
            return false;
        }

        zeroCoefs();

        this.lf = lf;
        this.hf = 0.;
        this.sampleRate = sampleRate;
        this.nroll = nroll;

        double dt = (double) 1./sampleRate;
        double w1 = 2.*Math.PI*lf;
        w1 = 2.*Math.tan(w1*dt/2.)/dt;
        double d = 0;
        double d2 = Math.pow(w1*dt/2.,2);

        for (int k = 1; k <= nroll; k++) {
          d = Math.sin(Math.PI*(double)(2*k-1)/(double)(4*nroll))*w1*dt;
          fact[k-1] = 1./(1.+ d2 + d);
          b1[k-1] = 2. * fact[k-1] * (d2 - 1.);
          b2[k-1] = fact[k-1] * (1. + d2 - d);
        }

        setTypeString(FilterTypes.getType(type));

        return true;

    }

    /** Return true if the units of the actual samples in the waveform are
     * legal for this filter. */
    public boolean unitsAreLegal(Waveform wf) {
      return unitsAreLegal(wf.getAmpUnits());
    }

    /** Return true if the units of the raw waveform are legal for this filter. */
    public boolean unitsAreLegal(int units) {
      return true;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(description).append(" type= ").append(type).append("\n");
        sb.append(" reverse=  ").append(reverse);
        sb.append(" cosineTaperAlpha= ").append(cosineTaperAlpha).append("\n");
        sb.append(" sampleRate= ").append(sampleRate);
        if (type == FilterTypes.NOTCH)
          sb.append(" rf= ").append(rf).append(" dw= ").append(dw).append("\n");
        else 
          sb.append(" norder= ").append(nroll*2);
          sb.append(" lf= ").append(lf).append(" hf= ").append(hf).append("\n");
        return sb.toString();
    }

    public Object clone() {
        Object obj = null;
        try {
           obj = super.clone(); 
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return obj;
    }
}

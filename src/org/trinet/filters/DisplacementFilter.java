package org.trinet.filters;

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** Recursive integration filter to Displacement.
 * If the input timeseries is ACCELERATION it will be transformed
 * to cm/sec from counts and integrated to velocity.
 * If input is already a VELOCITY it will be transformed to cm/sec from counts,
 * high pass filtered and the bias will be removed.
 * Returns acceleration in cm/sec. Only intended for use on 20, 80, and 100 sps data.<p>
 *
 * The displacement record is Butterworth filtered to remove long period signal.
 * This is necessary to get the same result as wee see from the RT implementation of
 * this algorithm, AmpGen. <p>
 *
 * Implementation of the technique
 * developed by Hiroo Kanamori described in "Continuous Monitoring of Ground-Motion
 * Parameters", BSSA, V89, pp. 311-316, Feb. 1999 as implemented in Hiroo Kanimori's
 * FORTRAN subroutine recres6().
 *
 * @see AbstractWaveformFilter
 * */
public class DisplacementFilter extends AbstractWaveformFilter {

  public static final String FILTER_NAME = "Displacement";
  protected static boolean DEFAULT_APPLY_TAPER = true;
  protected boolean applyButterworth = true;

  public DisplacementFilter() {
    this(FilterTypes.DISPLACEMENT, 0);
  }

  public DisplacementFilter(int sampleRate) {
    this(FilterTypes.DISPLACEMENT, sampleRate);
  }

  public DisplacementFilter(int type, int sampleRate) {
    setDescription(DisplacementFilter.FILTER_NAME);
    setApplyTaper(DisplacementFilter.DEFAULT_APPLY_TAPER);
    //setCosineTaperAlpha(0.5);    // normally 0.05 - IS THIS RIGHT? we need to check with Kanamori -aww
    setCosineTaperAlpha(0.05);    // ? we need to check with Kanamori -aww
    setType(type);
    setSampleRate(sampleRate);
  }

  /** Override parent filter function to convert accelerations to velocities
   * early on rather than in filterSegment(). */
//     public Waveform filter (Waveform wf) {
//       // If its raw accel have to change to vel first.
//       if ( wf.getChannelObj().isAcceleration()  && wf.getAmpUnits() == Units.COUNTS) {
//         wf  = new VelocityFilter().filter(AbstractWaveform.copyWf(wf));  // make Vel
//       }
//       // RawVel or Vel -> disp
//       return super.filter(wf);
//     }

/** Returns this same segment with the time-series Filtered.*/
// NOTE: no accelerations should ever get here, they are handled in filter()
  protected WFSegment filterSegment (WFSegment seg) {

    float ts[] = seg.getTimeSeries();

    if (ts.length < 3) return seg;    // not enough to filter

    setWFSegment(seg);
    // Remove bias: note 'scanIt' is 'false' because we don't yet want to recalc bias, max/min, etc.
    seg.setTimeSeries( demean(ts), false );

    double dt = seg.getSampleInterval();
    double v3 = 0.0, v2 = 0.0;

    double b0 =  2.0 / ( 1.0 + qHighPass );
    double mult = dt / (2.0 * b0) ;  // moosh these together of efficiency
    double divisor = b0 * getGainFactor(); // moosh these together of efficiency
    float disp[] = new float[ts.length];

    //  its a raw velocity
    if ( seg.getChannelObj().isVelocity()) {

      //
      // Note sure which is more efficient - I suspect doing it raw here
      //
      // convert ts[] into true ground motion: counts -> cm/sec (vel)
      //.// float vel[] = convertToGroundMotion(ts);

      // Integrate velocity to displacement  (Yes, highpass filter is applied again )
      for (int i = 1; i < ts.length -2; i++) {
        v3      = (ts[i]-ts[i-1])/divisor + qHighPass * v2 ;              // counts -> cm/sec (vel)
        disp[i] = (float) ( (v3 + v2) * mult + (qHighPass * disp[i-1]) ); // integrate to disp
        v2=v3;
        //.// disp[i] = (float) ( (vel[i] + vel[i-1]) * mult + (qHighPass * disp[i-1]) ); // integrate to disp
      }
      seg.setIsFiltered(true);       // flag segment as filtered

    // its a raw acceleration
    } else if ( seg.getChannelObj().isAcceleration()) {

      // convert ts[] into true ground motion: counts -> cm/sec^2 (accel)
      float acc[] = convertToGroundMotion(ts);

      // Double integrate to displacement  (Yes, highpass filter is applied each time )
      for (int i = 1; i < ts.length -2; i++) {
        v3      = (float) ( (acc[i] + acc[i-1]) * mult + (qHighPass * v2) );  // integrate to vel
        disp[i] = (float) ( (v3 + v2) * mult + (qHighPass * disp[i-1]) );     // integrate to disp
        v2=v3;
      }
      seg.setIsFiltered(true);       // flag segment as filtered

    } else {
      System.err.println("DisplacementFilter: illegal input waveform, check gain units for: " + seg.getChannelObj().toDelimitedSeedNameString());
      seg.setIsFiltered(false);       // flag segment as unfiltered
      return seg;
    }

    // filter the bastard again to remove weird long-period signal
    if (applyButterworth) {
      GeneralButterworthFilter butt = new GeneralButterworthFilter();
      butt.setSampleRate(1./seg.getSampleInterval()); // added 2012/06/26 -aww
      disp = butt.filter(disp);
    }

    // new time series
    seg.setTimeSeries(disp);

    return seg;
  }

  /** This is a two-step process: 1) accel -> vel, 2) vel -> disp. You must be careful to only
   * do the conversion from counts to ground motion ( * gain) once.*/
//  protected WFSegment rawAccelToDisp (WFSegment seg) {
//    VelocityFilter vfilter = new VelocityFilter();
//    vfilter.setWaveform(getWaveform());
//
//    seg.setTimeSeries( vfilter.accelRawToVel(seg.getTimeSeries()));   // accel -> velocity
//    seg.setTimeSeries( velToDisp(seg.getTimeSeries()) );              // vel -> disp
//    return seg;
//  }

  /** Set the units for the resulting waveform. */
  public void setUnits(Waveform wf) {
    wf.setAmpUnits(Units.CM);         // disp = cm
  }

  /** Return true if the units are legal for this filter. */
  public boolean unitsAreLegal(int units) {
    return (units == Units.COUNTS );      // raw counts only
//    return (units == Units.COUNTS ||    // raw
//            units == Units.CMS ||       // vel
//            units == Units.CMSS );      // accel
  }

  /** Returns true if the given sample rate is legal for this filter.
   * Overrides parent method because ALL rates are legal for acceleration filter. */
  protected boolean sampleRateIsLegal (double rate) {
    return true ;
  }

  /**
    * Recursive filter that does three things; <br>
    * 1) Converts from counts to whatever the gain units are in<br>
    * 2) high pass filters using Hiroo's "H1". <br>
    * 3) integrates from velocity to displacement<br>
    * Works with any sample rate. Input must be raw counts.<p>
    * WARNING: do not pass this method a waveform that has already been filtered.
    * Based on Hiroo's equation (12).
    *
    * */

   protected float[] velRawToDisp (float[] ts) {
     if (ts.length < 3) return ts;

     // <1> filter and multiply by gain to velocity (cm/sec)
     float vel[] = convertToGroundMotion(ts);

     return velToDisp(vel);
   }

   /**
    * Integrate a true velocity record (cm/sec) to acceleration (cm/sec^2)
    * Highpass filter is applied in the process.
    *
    * Input must be a timeseries in units of velocity.
    *
    * Based on Hiroo's equation (12)
    *
    */

   protected float[] velToDisp (float[] vel) {
     if (vel.length < 3) return vel;
     double dt = seg.getSampleInterval();
     double mult = dt * (1.0 + qHighPass) ;  // moosh these together of efficiency

     // Integrate to displacement
     float disp[] = new float[vel.length];
     // yes, Highpass filter is applied a 2nd time
     for (int i = 1; i < vel.length -2; i++) {
       disp[i] = (float) ( mult * (vel[i] + vel[i-1])/2.0 + (qHighPass * disp[i-1]) );
     }

     return disp;
   }


  /**
    * Recursive filter that does three things; <br>
    * 1) Converts from counts to whatever the gain units are in<br>
    * 2) high pass filters using Hiroo's "H1". <br>
    * 3) integrates from acceleration to velocity, then<br>
    * 4) integrates from velocity to displacement<br>
    * Works with any sample rate.<p>
    * WARNING: do not pass this method a waveform that has already been filtered.
    * Input must be raw counts.
    * Based on Hiroo's equation (12).
    * */
//   protected float[] accelRawToDisp (float[] acc) {
//
//     // <1> filter and multiply by gain and integrate to vel (cm/sec)
//     VelocityFilter vfilter = new VelocityFilter();
//     vfilter.setWaveform(this.getWaveform());   // needed to setup parameters
//     //vfilter.setWFSegment(seg );
//     float vel[] = vfilter.accelRawToVel(acc);
//
//     // <2> Integrate to velocity
//     return velToDisp(vel);
//
//   }

}


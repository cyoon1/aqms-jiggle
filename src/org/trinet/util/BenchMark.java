package org.trinet.util;

import java.util.*;
import java.io.PrintStream;
import org.trinet.util.DateTime;
import org.trinet.util.Format; // CoreJava printf-like Format class
/**
 * Do timestamping for application benchmarking. The "prefix" is just a string that is
 * printed before the elapse time when the print() is called.
 */
public class BenchMark {
    
    // Global stream used for all benchmark instances
    private static PrintStream o = System.out;
    
    String prefix = "";
    long t0;
    Format df = new Format("%9.4f");
    
    /**
     * Create a BenchMark object with this string as the default printing prefix.
     * Sets starttime to now.
     */
    public BenchMark(String str) {
        prefix = str;
        t0 = System.currentTimeMillis();
    }

    public BenchMark(PrintStream out, String str) {
        o = out;
        prefix = str;
        t0 = System.currentTimeMillis();
    }

    /**
     * Create a BenchMark object with no default printing prefix.
     * Sets starttime to now.
     */
    public BenchMark() {
        t0 = System.currentTimeMillis();
    }

    public BenchMark(PrintStream out) {
        o = out;
        t0 = System.currentTimeMillis();
    }
    
    /**
     * Print the current benchmark time with the default string "prefix" if any.
     */
    public void print() {
        print(prefix);
    }
    public void print(PrintStream out) {
        print(prefix, out);
    }
    /**
     * Print the current benchmark time with this string as the prefix.
     */
    public void print(String str) {
        o.println(toString(str));
    }
    public void print(String str, PrintStream out) {
        out.println(toString(str));
    }


    public void printTimeStamp() {
        print(toTimeStampString());
    }
    public void printTimeStamp(String str) {
        o.println(toTimeStampString(str));
    }
    public void printTimeStamp(String str, PrintStream out) {
        out.println(toTimeStampString(str));
    }
    /**
     * Reset the time counter to now.
     */
    public void reset() {
        t0 = System.currentTimeMillis(); 
    }
    
    public long getMillis() {
        return System.currentTimeMillis() - t0;
    }
    
    public double getSeconds() {
        return (double) (getMillis()/1000.);
    }
    
    public String toString() {
        return toString(prefix);
    }

    public String toTimeStampString() {
        return toTimeStampString(prefix);
    }

    public String toString(String str) {
        return (str + " " + df.form(getSeconds()) + " elapsed sec");
    }

    // Note DateTime calls LeapSeconds (if you require DataSource to init LeapSeconds don't call this before db connection creation.
    public String toTimeStampString(String str) {
        return toString(str) + " at " + new DateTime().toString(); // use UTC date time - aww 2008/02/08
    }

    public static void setPrintStream(PrintStream out) {
        o = out;
    }
} // end of class

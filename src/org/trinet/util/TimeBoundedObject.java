package org.trinet.util;

/**
 * An object that is associated with a particular time span. For example, a channel
 * gain has a particular value only for a period of time. It will change when
 * components are changed or, perhaps, when it is recalibrated.
 */

public class TimeBoundedObject implements TimeBoundedObjectIF {

  public TimeBoundedObject() {
  }

  /** Returns 'true' if epochtime is in the time span for which this object
   * is valid. */
  public boolean isValidAt(double epochtime) {
    return timespan.contains(epochtime);
  }

  /** Return the timespan for for which this object is valid. */
  public TimeSpan getTimeSpan() {
    return timespan;
  }

  /** Set the timespan for for which this object is valid. */
  public void setTimespan(TimeSpan timespan) {
    timespan.set(timespan);
  }

}

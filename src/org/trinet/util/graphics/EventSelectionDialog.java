package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;

import org.trinet.jasi.EventSelectionProperties;
import org.trinet.util.*;

/**
Presents a centered, tabbed dialog to set values of EventSelectionProperties.
Note that this class modifies a private version of the EventSelectionProperties
that are passed as a parameter. It can't change the original directly. The caller
must call getEventSelectionProperties() to see the revised properties.
This should only be done if a call to getButtonStatus() returns
JOptionPane.OK_OPTION indicating the user wants to apply the new properties.
*/

public class EventSelectionDialog extends JDialog {

     /** Origin Time selection */
     private EventSelectionTimePanel timePanel = null;
     private EventSelectionRegionPanel regionPanel = null;
     private EventSelectionFlagPanel flagPanel = null;
     private EventSelectionOriginPanel originPanel = null;
     private EventSelectionMagPanel magPanel = null;

     // The local EventSelectionProperties
     private EventSelectionProperties props;

     /** Status for dialog when closed. */
     private int returnValue = JOptionPane.CLOSED_OPTION;

     public EventSelectionDialog(Frame frame, String title, boolean modal,
                                       EventSelectionProperties eventProps) {
          super(frame, title, modal);

          // make a copy, so we can back out incase [CANCEL] is pressed
          //Below constructor sets the "default" map, not primary map, thus can't "remove" a property -aww
          //props = new EventSelectionProperties(eventProps);
          props = (EventSelectionProperties) eventProps.clone(); // copy of input -aww 01/17/2006

          try  {
               jbInit();
               pack();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }

     public EventSelectionDialog(EventSelectionProperties eventProps) {
          this(null, "", true, eventProps);
     }

     /** Create the GUI */
     private void jbInit() throws Exception {

          JButton cancelButton = new JButton();
          cancelButton.setText("CANCEL");
          cancelButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    cancelButton_actionPerformed(e);
               }
          });

          JButton okButton = new JButton();
          okButton.setText("OK");
          okButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    okButton_actionPerformed(e);
               }
          });

          JButton resetButton = new JButton();
          resetButton.setText("CLEAR PANELS");
          resetButton.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(ActionEvent e) {
                   regionPanel.clear();
                   flagPanel.clear();
                   originPanel.clear();
                   magPanel.clear();
               }
          });

          JPanel mainPanel = new JPanel();
          mainPanel.setLayout(new BorderLayout());
          JPanel buttonPanel  = new JPanel();
          buttonPanel.add(okButton);
          buttonPanel.add(cancelButton);
          buttonPanel.add(new JLabel("    "));
          buttonPanel.add(resetButton);
          mainPanel.add(buttonPanel, BorderLayout.SOUTH);

// selection panels
          JTabbedPane tabPane = new JTabbedPane();
          timePanel = new EventSelectionTimePanel(props);
          tabPane.addTab("Time Range", new JScrollPane(timePanel));

          regionPanel = new EventSelectionRegionPanel(props);
          tabPane.addTab("Region", new JScrollPane(regionPanel));

          flagPanel = new EventSelectionFlagPanel(props);
          tabPane.addTab( "Event Prefor Type ", new JScrollPane(flagPanel)); 

          originPanel = new EventSelectionOriginPanel(props);
          tabPane.addTab("Prefor Attributes", new JScrollPane(originPanel)); 

          magPanel = new EventSelectionMagPanel(props);
          tabPane.addTab("Prefmag Attributes", new JScrollPane(magPanel));

          mainPanel.add(tabPane, BorderLayout.CENTER);

          getContentPane().add(mainPanel);
     }

      /** Set the time span for the date range chooser*/
     public TimeSpan getTimeSpan() {
        return timePanel.getTimeSpan();
     }

     /** Return the EventSelectionProperties */
     public EventSelectionProperties getEventSelectionProperties() {
            return props;
     }

 
/**
 * Pop dialog asking if preferences should be saved to a file.
 */
    public void saveDialog() {

    //pop confirming  yes/no dialog:
           int yn = JOptionPane.showConfirmDialog(
                   null, "Save edited properties to a user's directory file?",
                   "Save Properties",
                   JOptionPane.YES_NO_OPTION);

	    if (yn == JOptionPane.YES_OPTION) {
                   String str = JOptionPane.showInputDialog("Save to filename: ", props.getFilename());
                   if (str != null) props.setFilename(str);
    		   props.saveProperties();
	    }
    }


      // [OK] button
     private void okButton_actionPerformed(ActionEvent e) {

          returnValue = JOptionPane.OK_OPTION;

          // We have to "update" the shared properties map with current panel settings
          timePanel.updateProperties();
          flagPanel.updateProperties();
          originPanel.updateProperties();
          magPanel.updateProperties();
          regionPanel.updateProperties();

          //saveDialog(); // removed 2008/10/20 -aww

          setVisible(false);
     }

     // [Cancel] button
     private void cancelButton_actionPerformed(ActionEvent e) {
          returnValue = JOptionPane.CANCEL_OPTION;
          setVisible(false);
     }


     public int getButtonStatus() {
        return returnValue;
     }

     /** Override to center dialog on screen */
     public void setVisible(boolean tf) {
            if (tf) centerDialog();
            super.setVisible(tf);
     }
/**
 * Center the dialog on the screen
 */
   public void centerDialog() {
     Dimension screenSize = this.getToolkit().getScreenSize();
	Dimension size = this.getSize();
	screenSize.height = screenSize.height/2;
	screenSize.width = screenSize.width/2;
	size.height = size.height/2;
	size.width = size.width/2;
	int y = screenSize.height - size.height;
	int x = screenSize.width - size.width;
	this.setLocation(x,y);
    }

/*
     public static void main(String s[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter()
	{
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });
// Private properties
// properties instance by default uses EnvironmentInfo.getApplicationName.toLowerCase() as user's home subdir
// re: getFiletype(), if not defined it default to "jasi", thus <user_home_root_path>/jasi/filename
	EventSelectionProperties props = new EventSelectionProperties();
        props.setFiletype("jiggle"); // test file found in the user's home "jiggle" subdir
        props.setFilename("eventProperties"); // name of event selection props file
        props.reset(); // read the props from file
        props.setup(); // configure local environment from props
        props.dumpProperties();

        EventSelectionDialog dialog =
                 new EventSelectionDialog(frame, "Event Selection", true, props);

        dialog.setVisible(true);

        System.out.println("-- Event Selection Properties BEFORE --");

        props.dumpProperties();

        System.out.println("-----------");

        if (dialog.getButtonStatus() == JOptionPane.OK_OPTION) {
           props = dialog.getEventSelectionProperties();
        }

        System.out.println("-- Event Selection Properties AFTER --");

        props.dumpProperties();

        System.out.println("-----------");

	frame.setSize(200,30);	// this has no effect!
        frame.pack();
        frame.setVisible(true);

    }
*/
}

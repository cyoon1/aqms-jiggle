package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;

public class CheckTreeCellRenderer extends JPanel implements TreeCellRenderer{ 
    private CheckTreeSelectionModel selectionModel; 
    private DefaultTreeCellRenderer delegate; 
    private TristateCheckBox checkBox = new TristateCheckBox(); 
 
    public CheckTreeCellRenderer(DefaultTreeCellRenderer delegate, CheckTreeSelectionModel selectionModel){ 
        this.delegate = delegate; 
        delegate.setLeafIcon(null);
        delegate.setClosedIcon(null);
        delegate.setOpenIcon(null);
        this.selectionModel = selectionModel; 
        setLayout(new BorderLayout()); 
        setOpaque(false); 
        checkBox.setOpaque(false); 
    } 
 
 
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        d.setSize(d.width+20, d.height);
        return d;
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus){ 
        Component renderer = delegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus); 
        TreePath path = tree.getPathForRow(row); 
        //selectionModel = (CheckTreeSelectionModel) tree.getSelectionModel(); // NOT the same model as this constructor init
        //if (selectionModel.isPathSelected(path)) System.out.println("Renderer path selected: " + path);
        removeAll(); 
        if(path!=null && ! leaf) {
            if(selectionModel.isPathSelected(path, true))  {
            //if(selectionModel.isPathSelected(path))  {
                //checkBox.setState(Boolean.TRUE); 
                checkBox.setState(TristateCheckBox.SELECTED); 
                //System.out.println("R-SELECTED : " + path);
            }
            else  {
                //checkBox.setState(selectionModel.isPartiallySelected(path) ? null : Boolean.FALSE); 
                if (selectionModel.isPartiallySelected(path)) {
                        checkBox.setState(TristateCheckBox.DONT_CARE);
                        //System.out.println("R-PART SELECTED : " + path);
                }
                else {
                        checkBox.setState(TristateCheckBox.NOT_SELECTED); 
                        //System.out.println("R-NOT SELECTED : " + path);
                }
            }
            this.add(checkBox, BorderLayout.WEST); 
        }
        this.add(renderer, BorderLayout.CENTER); 
        return this; 
    } 
} 


package org.trinet.util.graphics;

import java.awt.*;
import java.util.*;
import org.trinet.util.Format;
import org.trinet.util.GenericPropertyList;

/**
 * Extends class Color to add new colors and the concept of an ordered color list. Also,
 * controls the definition of certain general use colors, e.g. the color of an unassociated
 * pick.
 */
public class ColorList {

// The following public static Colors were created using a Java tool at:
//  http://www.cadvision.com/ewertb/Java_public static ColorPicker.html

// Also see: http://cloford.com/resources/colours/500col.htm

//                      color                    R    G    B
    public static Color tan         = new Color (225, 190, 145);
    public static Color brown       = new Color (160, 125,  80);
    public static Color medBrown    = new Color (190, 155, 100);
    public static Color navyBlue    = new Color ( 30,  20, 100);
    public static Color skyBlue     = new Color (165, 220, 255);
    public static Color aliceBlue   = new Color (240, 248, 255);  // very pale
    public static Color medBlue     = new Color (125, 170, 255);
    public static Color slateBlue   = new Color (160, 180, 200);
    public static Color aquamarine  = new Color (127, 255, 212);
    public static Color brickRed    = new Color (170,  30,  70);
    public static Color peaGreen    = new Color (160, 175,  60);
    public static Color kellyGreen  = new Color ( 20, 160,  20);
    public static Color olive       = new Color (105, 135,  20);
    public static Color purple      = new Color (165, 115, 200);
    public static Color plumb       = new Color (175,   0, 255);
    public static Color lavender    = new Color (225, 170, 220);
    public static Color buff        = new Color (220, 220, 175);
    public static Color gold        = new Color (255, 225,   0);
    public static Color mint        = new Color (175, 255, 175);
    public static Color violet      = new Color (105,  45, 165);
    public static Color indigo      = new Color ( 70,  65, 175);
    public static Color burntOrange = new Color (255, 150,   0);
    public static Color litesmoke   = new Color (240, 240, 240); // a very light gray

    /** Special color to be used for unassociated PickMarkers, etc. */
    public static Color UNASSOC = Color.gray;

    /** Special color to be used for active (changing) PickMarkers, etc. */
    public static Color ACTIVE = gold;

    /** This is the ordered color list used for multiple events in the same view. */
    public static Color colorArray[] = { // Rearranged default color order, timer's request - aww 2008/07/31
       Color.red,
       Color.green,
       brickRed,
       peaGreen,
       purple,
       mint,
       burntOrange,
       skyBlue,
       tan,
       lavender,
       Color.pink,
       buff
    };


    public ColorList() {}

    public static int getColorCount() {
        return colorArray.length;
    }

    /**
     * Return a color that corrisponds to this int in the color list.
     * Uses modulus operator to "wrap" and repeat color order if value exceeds list size.
     */
    public static Color getColor(int i) {
        return  colorArray[i%colorArray.length] ;
    }

    public static String toPropertyString() {
        int size = colorArray.length;
        if (size == 0) return "";
        StringBuffer sb = new StringBuffer(24*size);
        Color c = null;
        String hexValue = null; 
        for (int idx = 0; idx < size; idx++) {
            c = colorArray[idx];
            hexValue = (c == null) ? "" : Integer.toHexString(c.getRGB()); 
            sb.append("color.solution.").append(idx).append(" = ").append(hexValue).append("\n");
        }
        return sb.toString();
    }

    public static void toProperties(GenericPropertyList props) {
        int size = colorArray.length;
        if (size == 0) return;
        Format f = new Format ("%#02d");
        for (int idx = 0; idx < size; idx++) {
            props.setProperty("color.solution."+f.form(idx), colorArray[idx]);
        }
    }

    public static void fromProperties(GenericPropertyList props) {
        ArrayList aList = new ArrayList(20);
        String key = null;
        for (Enumeration e = props.keys(); e.hasMoreElements();)  { // alpha sorted order key
            key = (String) e.nextElement();
            if ( ! key.startsWith("color.solution.") ) continue; // skip 
            // getColor() returns null if unparseable
            aList.add(props.getColor(key));
        }
        if (aList.size() > 0) colorArray = (Color []) aList.toArray(new Color[aList.size()]);
    }
}

package org.trinet.util.graphics.text;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.*;
import java.text.*;

public class ComboBoxNumberRenderer extends JLabel implements ListCellRenderer {
  int fontSize=12;
  int length = 0;
  Font font = null;
  DecimalFormat df;
  private static String DEFAULT_PATTERN = "#.#";

  public ComboBoxNumberRenderer() { 
    this(null, 0, 0, DEFAULT_PATTERN);
  }
  
  public ComboBoxNumberRenderer(int fontSize) { 
    this(null, fontSize, 0, DEFAULT_PATTERN);
  }

  public ComboBoxNumberRenderer(int fontSize, int length) { 
    this(null, fontSize, length, DEFAULT_PATTERN);
  }
  
  public ComboBoxNumberRenderer(Font font) { 
    this(font, 0, 0, DEFAULT_PATTERN);
  }
  
  public ComboBoxNumberRenderer(Font font, int fontSize) { 
    this(font, fontSize, 0, DEFAULT_PATTERN);
  }
  
  public ComboBoxNumberRenderer(Font font, int fontSize, int length, String pattern) { 
    if (fontSize > 0) this.fontSize = fontSize;
    if (length > 0) this.length = length;
    if (font != null) {
	if (fontSize > 0) this.font = font.deriveFont((float) fontSize);
	else this.font = font;
    }
    else this.font = new Font("Monospaced", Font.PLAIN, fontSize);
    setFont(font);

    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
      df.applyPattern(pattern);
    }

    setHorizontalAlignment(JLabel.RIGHT);
//    setHorizontalAlignment(JLabel.LEFT);
    setToolTipText("Value in cell must be a number");
  }
  
  public int getFontSize() {
    return fontSize;
  }

  public void setFontSize(int fontSize) {
    if (fontSize > 0) this.fontSize = fontSize;
    font = font.deriveFont((float) fontSize);
    setFont(font);
  }
 
  public Component getListCellRendererComponent( JList jlist, Object value,
			 int index, boolean isSelected, boolean cellHasFocus) {
    if (value == null) setText(null);
    else setText(value.toString());
    return this;
  }

// overide setText(String value) method of super class
// which is called by the super class method getTableCellRendererComponent
    public void setText(String value) {
	if (value == null || value.length() == 0 ) super.setText(null);
	else {
	    try {
		Number number = df.parse(value); 
		String tmpStr = null;
		if (number instanceof Integer) tmpStr = df.format(((Integer) number).longValue());
		else if (number instanceof Long) tmpStr = df.format(((Long) number).longValue());
		else if (number instanceof Double) tmpStr = df.format(((Double) number).doubleValue());
		super.setText(tmpStr);
	    }
	    catch (ParseException ex) {
		ex.printStackTrace();
		super.setText("");
	    }
	}
    }
}

package org.trinet.util.graphics.text;

import java.text.*;
import java.awt.Toolkit;
import javax.swing.text.*; 

public class LetterDigitDocument extends LetterDocument {
    protected int strLength;

    public LetterDigitDocument() {}

    public LetterDigitDocument(int cols) {
	super(cols);
    }

    protected boolean hasNonLetter(String inputString) {
	StringCharacterIterator charIter = new StringCharacterIterator(inputString);
	for (char result = charIter.first(); result != CharacterIterator.DONE; result = charIter.next()) {
	    if (! Character.isLetterOrDigit(result)) {
                    Toolkit.getDefaultToolkit().beep();
		    System.err.println("LetterDocument - text not a letter or digit");
		    return true;
	    }
	}
        return false;
    }
}

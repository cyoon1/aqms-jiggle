package org.trinet.util.graphics.text;
public interface NullableEditorIF {
 public boolean isNullAllowed();
 public void setNullAllowed(boolean value);
}

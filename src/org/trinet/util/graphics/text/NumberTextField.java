// TODO : enable toggle from insert to an overstrike text mode, enable an undo editor
package org.trinet.util.graphics.text;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;

// InputVerification kludges 1.2 for an InputVerifier set/get found in jdk 1.3
// Use below for Java v1.2.2
//public class NumberTextField extends JTextField implements InputVerification {

// This is specific to v1.3
public class NumberTextField extends JTextField {
    public static final int LONG_TYPE = 0 ;
    public static final int INT_TYPE = 1 ;
    public static final int FLOAT_TYPE = 2 ;
    public static final int DOUBLE_TYPE = 4 ;
    public static final int OBJECT_TYPE = 8 ;
    protected int dataType;

    protected DecimalFormat numberFormat;
    protected String actionCommand = "";
    protected boolean audioOn = true;
    protected boolean informInvalidityOnFocusLost = false;
    protected boolean transferFocusOnAction = false;
    protected JTextClipboardPopupMenu clipboardMenu;

/*
    protected static final int DEFAULT_COLUMNS = 12;
    protected static final DecimalFormat DEFAULT_FORMAT = (DecimalFormat) NumberFormat.getNumberInstance();
    static {
        DEFAULT_FORMAT.setGroupingUsed(false);
        DEFAULT_FORMAT.setDecimalSeparatorAlwaysShown(false);
        DEFAULT_FORMAT.applyPattern("#0.###");
        DEFAULT_FORMAT.setParseIntegerOnly(false);
    }
*/
    protected NumberTextField(Document document, int columns, DecimalFormat format) {
//      super(document, "", columns); // attempts setText("") formatted as number, which fails setText verify
        super(document, null, columns);
        if (format == null) throw new NullPointerException("NumberTextField null format parameter");
        numberFormat = format;
        initField();
    }

/*
    public NumberTextField() {
        this(new FormattedNumberDocument(DEFAULT_FORMAT, 0), DEFAULT_COLUMNS, DEFAULT_FORMAT);
    }

    public NumberTextField(int columns) {
        this(new FormattedNumberDocument(DEFAULT_FORMAT, 0), columns, DEFAULT_FORMAT);
    }
*/
    public NumberTextField(int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
    }

    public NumberTextField(float value, int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
        setValue(value);
    }

    public NumberTextField(double value, int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
        setValue(value);
    }

    public NumberTextField(int value, int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
        setValue(value);
    }

    public NumberTextField(long value, int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
        setValue(value);
    }

    public NumberTextField(Object value, int columns, DecimalFormat format) {
        this(new FormattedNumberDocument(format, 0), columns, format);
        setValue(value);
    }

    protected void initField() {
        addValidityListeners();
        setInputVerifier(new NumberTextFieldVerifier());
        clipboardMenu = new JTextClipboardPopupMenu(this);
    }
    public void setAddUndo() { clipboardMenu.setAddUndo();}

/* Test key board input
    protected void processKeyEvent(KeyEvent evt) {
        System.out.println("evt.paramString: " + evt.paramString());
        System.out.println("keyCode: " + evt.getKeyCode());
        System.out.println("keyChar: " + evt.getKeyChar());
        System.out.println("keyText: " + evt.getKeyText(evt.getKeyCode()));
        if(evt.getID() == KeyEvent.KEY_PRESSED && evt.getKeyCode() == KeyEvent.VK_TAB) {
                if (textChanged || ! validityValue) {
//                    textChanged = false;
                }
                if (! validityValue) {
//                    evt.consume();
                }
        }
        super.processKeyEvent(evt);
    }
*/
/*
* Overrides base class implementation for setting field text string.
* @throws InvalidInputException input text fails validation
*/
    public void setText(String text) {
        Number value = stringToNumber(text);
        boolean valid = accepts(value);
        if (valid) {
            super.setText(text);
            notifyValueChanged(value, valid);
        }
        else {
//          notifyInputBoundsViolated(value);
            throw new InvalidInputException("NumberTextField setText(String) invalid input parameter: " + value);
        }
    }

/*
* override base class implementation
*/
    public Dimension getPreferredSize() {
        Dimension size =  super.getPreferredSize();
        Insets insets = getInsets();
        size.width += (insets.left  + insets.right);
        return size;
    }

/*
* override base class implementation
*/
    protected int getColumnWidth() {
        return getFontMetrics(getFont()).charWidth('5');
    }

    public int getDataType() {
        return dataType;
    }

    public Number stringToNumber(String text) {
        Number retVal = null;
        if (text.equals("")) return retVal;
        try {
            retVal = numberFormat.parse(text);
        } catch (ParseException ex) { // should not happen: insertString properly formats field data
            if (audioOn) Toolkit.getDefaultToolkit().beep();
            System.err.println("NumberTextField.stringToNumber(String) : " + ex.toString());
        }
        return retVal;
    }

/** Gets the number value equivalent of the input text.
* @throws InvalidInputException input text fails validation
*/
    public Number getNumberValue() {
        Number value = stringToNumber(getText());
        if (! accepts(value))
            throw new InvalidInputException("NumberTextField getNumberValue() input fails verification : " + value);
        return value;
    }

    public Object getValue() {
//        return getNumberValue();
        return stringToNumber(getText());
    }

    public double getDoubleValue() {
        double retVal = 0.0;
        Number value = getNumberValue();
        return (value == null) ? retVal : value.doubleValue();
    }

    public float getFloatValue() {
        float retVal = 0.f;
        Number value = getNumberValue();
        return (value == null) ? retVal : value.floatValue();
    }

    public int getIntValue() {
        int retVal = 0;
        Number value = getNumberValue();
        return (value == null) ? retVal : value.intValue();
    }

    public long getLongValue() {
        long retVal = 0l;
        Number value = getNumberValue();
        return (value == null) ? retVal : value.longValue();
    }

    public void setValue(int value) {
        dataType = INT_TYPE;
        setText(numberFormat.format((long)value));
    }

    public void setValue(long value) {
        dataType = LONG_TYPE;
        setText(numberFormat.format(value));
    }

    public void setValue(float value) {
        dataType = FLOAT_TYPE;
        setText(numberFormat.format((double)value));
    }

    public void setValue(double value) {
        dataType = DOUBLE_TYPE;
        setText(numberFormat.format(value));
    }

    public void setValue(Object value) {
        dataType = OBJECT_TYPE;
        if (value == null) setText("");
        else if (! (value instanceof DataObject)) setText(numberFormat.format(value));
        else {
            if (((DataObject) value).isNull()) setText("");
            else if ( (value instanceof DataDouble) || (value instanceof DataFloat)) {
                setText(numberFormat.format(((DataDouble) value).doubleValue()));
            }
            else setText(numberFormat.format(((DataObject) value).longValue()));
        }
    }

    public String getActionCommand() {
        return this.actionCommand;
    }

    public void setActionCommand(String command) {
        this.actionCommand = command;
        super.setActionCommand(command);
    }

    private boolean textChanged = true;

    private boolean validityValue = false;
    private Number numberValue;

    private boolean nullAllowed = false;
    private Number defaultValue = null;

    private RangeLimits limits;
    private ConstraintErrorActionStates limitsErrorActionStates;

    private InputVerifier inputVerifier;

    public void setConstraintErrorActionStates(boolean log, boolean audio, boolean reset, boolean popup) {
        if (limitsErrorActionStates == null) {
            limitsErrorActionStates = new ConstraintErrorActionStates(log, audio, reset, popup);
        }
        else {
            limitsErrorActionStates.setErrorActionStates(log, audio, reset, popup);
        }
    }

/**
 * Input into field is bounds checked.
*/
    public RangeLimits getLimits() {
        return (limits == null) ? null : (RangeLimits) limits.clone();
    }

    public void setLimits(Number minLimit, Number maxLimit) {
        if (limits == null) limits = new RangeLimits(minLimit, maxLimit);
        else limits.setLimits(minLimit, maxLimit);
        if (limits.isLimited()) this.setToolTipText(limits.toString());

        if (limitsErrorActionStates == null) limitsErrorActionStates = new ConstraintErrorActionStates();
    }

    public void setInputVerifier(InputVerifier inputVerifier) {
        this.inputVerifier = inputVerifier;
    }

    public InputVerifier getInputVerifier() {
        return inputVerifier;
    }

    public boolean verifyValid() {
        InputVerifier iv = getInputVerifier();
        if (iv == null) {
            validityValue = true;
            notifyValueChanged(stringToNumber(getText()), validityValue);
            return validityValue;
        }
        else return iv.shouldYieldFocus(this);
    }

    public boolean accepts(Number value) {
        return checkInputConstraints(value);
    }

    private void addValidityListeners() {
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (verifyValid() && transferFocusOnAction) transferFocus();
            }
        });

        getDocument().addDocumentListener( new DocumentListener() {
            public void changedUpdate(DocumentEvent evt) {
                textChanged = true;
            }
            public void insertUpdate(DocumentEvent evt) {
                textChanged = true;
            }
            public void removeUpdate(DocumentEvent evt) {
                textChanged = true;
            }
        });

        addFocusListener(new FocusListener()  {

            private boolean verifying = false;

            public void focusGained(FocusEvent evt) {
                ((JTextComponent) evt.getSource()).selectAll();
//              System.out.println("focusGained evt.toString: " + evt.paramString() + " " + evt.getSource().getClass().getName());
            }

            public void focusLost(FocusEvent evt) {
//              System.out.println("focusLost evt.toString: " + evt.paramString() + " " + evt.getSource().getClass().getName());
                if (evt.isTemporary()) return;
//              if (textChanged ) {
                if (textChanged || (! validityValue && informInvalidityOnFocusLost)) {
                    textChanged = false;
                    if (verifying)  return;
                    verifying = true;
                    verifyValid();
                    verifying = false;
                }
            }
        });

    }

    public boolean isTransferFocusOnAction() {
        return transferFocusOnAction;
    }

    public void setTransferFocusOnAction(boolean value) {
        transferFocusOnAction = value;
    }

    public boolean isNullAllowed() {
        return nullAllowed;
    }

    public void setNullAllowed(boolean value) {
        nullAllowed = value;
    }

    public boolean hasDefaultValue() {
        return (defaultValue == null) ? false : true;
    }

    public Number getDefaultValue() {
        return defaultValue;
    }

/** Sets the default value to reset to upon error.
* @throws IllegalArgumentException default value is outside of limit bounds.
*/
    public void setDefaultValue(Number defaultValue) {
        if (limits != null && limits.isLimited()) {
           if (limits.excludes(defaultValue))
                 throw new IllegalArgumentException("DefaultValue Outside limit bounds.");
        }
        this.defaultValue = defaultValue;
        setToolTipText(getToolTipText() + " Default: " + defaultValue);
    }

/** Checks inputs for limit constraint violations.
* If input is null, returns true if nulls are allowed or a default value is declared.
* Resets empty field to default if any, else returns false.
* Returns true if input is within limit bounds.
*/
    protected boolean checkInputConstraints() {
        return checkInputConstraints(stringToNumber(getText()));
    }

    protected boolean checkInputConstraints(Number value) {
        if (value == null) {
            if (nullAllowed) return true;
            else {
                if (hasDefaultValue()) {
                    setValue(defaultValue);
                    return true;
                }
                else return false;
            }
        }
        return (limits == null || limits.contains((Object) value)) ;
    }

    protected void notifyInputBoundsViolated(Object value) {
        if (limitsErrorActionStates == null) return;

        if (limitsErrorActionStates.isAudioOnError()) Toolkit.getDefaultToolkit().beep();
        if (limitsErrorActionStates.isLogOnError()) {
            System.err.println("NumberTextField input Error " + getActionCommand() + " " + limits.toString() + " ; " +
                                String.valueOf(value) + " is out of bounds.");
        }
        if (limitsErrorActionStates.isPopupOnError()) {
            Container container = getTopLevelAncestor();
            if (container != null && container.isShowing())
                InfoDialog.informUser(container, "ERROR", getActionCommand() + " " +
                                  limits.toString() + " ; " + String.valueOf(value) + " is out of bounds.");
        }
        if (limitsErrorActionStates.isResetOnError()) resetValue();
    }

    public void resetValue() {
        if (hasDefaultValue()) setValue(defaultValue);
        else if (nullAllowed) setText("");
    }

    private void notifyValueChanged(Number newNumberValue, boolean newValidityValue) {
        setValidityBackground(newValidityValue);
        if ( ((newNumberValue != numberValue ) && (newNumberValue == null || ! newNumberValue.equals(numberValue))) ||
                (newValidityValue != validityValue) ) {
            Number oldNumberValue = numberValue;
            numberValue = newNumberValue;
            boolean oldValidityValue = validityValue;
            validityValue = newValidityValue;
            fireValueChanged("value", oldNumberValue, numberValue, newValidityValue);
        }
    }

    private void setValidityBackground(boolean validityValue) {
        if(validityValue) setBackground(Color.white);
        else setBackground(Color.pink);
    }

    public boolean isInformInvalidityOnFocusLost() {
        return informInvalidityOnFocusLost;
    }

    public void setInformInvalidityOnFocusLost(boolean value) {
        informInvalidityOnFocusLost = value;
    }

/*
    private void notifyValueChanged(Number newNumberValue) {
        if (  (newNumberValue != numberValue ) &&
              (newNumberValue == null || ! newNumberValue.equals(numberValue)) ) {
            Number oldNumberValue = numberValue;
            numberValue = newNumberValue;
            firePropertyChange("value", oldNumberValue, numberValue);
        }
    }

    private void notifyValidityChanged(boolean newValidityValue) {
        if (newValidityValue != validityValue) {
            validityValue = newValidityValue;
            if(validityValue) setBackground(Color.white);
            else setBackground(Color.pink);
            firePropertyChange("validity", ! oldValidityValue, validityValue);
        }
    }
*/
    class NumberTextFieldVerifier extends InputVerifier {
        public boolean shouldYieldFocus(JComponent comp) {
            NumberTextField ntf = (NumberTextField) comp;
            Number value = ntf.stringToNumber(ntf.getText());
            boolean valid = ntf.checkInputConstraints(value);
            ntf.notifyValueChanged(value, valid);
            if (! valid) ntf.notifyInputBoundsViolated(value);
            return valid;
        }

        public boolean verify(JComponent comp) {
            return ((NumberTextField) comp).checkInputConstraints();
        }
    }

    public synchronized void addValueChangedListener(ValueChangedListener listener) {
        listenerList.add(ValueChangedListener.class, listener);
    }

    public synchronized void removeValueChangedListener(ValueChangedListener listener) {
       listenerList.remove(ValueChangedListener.class, listener);
    }

    protected void fireValueChanged(ValueChangedEvent event) {
        Object[] listeners = listenerList.getListenerList();
        for (int idx = listeners.length-2; idx >= 0; idx -= 2) {
            if (listeners[idx] == ValueChangedListener.class) {
                ((ValueChangedListener) listeners[idx+1]).valueChanged(event);
            }
        }
    }

    protected void fireValueChanged(String name, Object oldValue, Object newValue, boolean newValidity) {
        Object[] listeners = listenerList.getListenerList();
        ValueChangedEvent event = null;
        for (int idx = listeners.length-2; idx >= 0; idx -= 2) {
            if (listeners[idx] == ValueChangedListener.class) {
                if (event == null) event = new ValueChangedEvent(this, name, oldValue, newValue, newValidity);
                ((ValueChangedListener) listeners[idx+1]).valueChanged(event);
            }
        }
    }

} // end of NumberTextField class


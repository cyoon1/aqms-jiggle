package org.trinet.util.graphics.text;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

public class ComboBoxTextEditor implements ComboBoxEditor, FocusListener {
  protected int fontSize=12;
  protected int length;
  protected Font font;
  protected JTextField editorComponent;
//  protected Object item;

  public ComboBoxTextEditor() { 
    this(null, 0, 0);
  }
  
  public ComboBoxTextEditor(int fontSize) { 
    this(null, fontSize, 0);
  }
  
  public ComboBoxTextEditor(int fontSize, int length) { 
    this(null, fontSize, length);
  }
  
  public ComboBoxTextEditor(Font font) { 
    this(font, 0, 0);
  }
  
  public ComboBoxTextEditor(Font font, int fontSize) { 
    this(font, fontSize, 0);
  }
  
  public ComboBoxTextEditor(Font font, int fontSize, int length) { 
    if (fontSize > 0) this.fontSize = fontSize;
    if (length > 0) this.length = length;
    if (font != null) {
	if (fontSize > 0) this.font = font.deriveFont((float)fontSize);
	else this.font = font;
    }
    else this.font = new Font("Monospaced", Font.PLAIN, fontSize);

    if (length > 0) editorComponent = new JTextField(length);
    else editorComponent = new JTextField();
    editorComponent.setFont(font);
    editorComponent.setHorizontalAlignment(JTextField.LEFT);
  }
  
  public int getFontSize() {
    return fontSize;
  }

  public void setFontSize(int fontSize) {
    if (fontSize > 0) this.fontSize = fontSize;
    font = font.deriveFont((float) fontSize);
    editorComponent.setFont(font);
  }

  public void addActionListener(ActionListener al) {
    editorComponent.addActionListener(al);
  }

  public void removeActionListener(ActionListener al) {
    editorComponent.removeActionListener(al);
  }

  public Component getEditorComponent() {
    return editorComponent;
  }

  public void selectAll() {
    editorComponent.selectAll();
  }

  public Object getItem() {
      return editorComponent.getText();
//    if (item == null || item instanceof String) return editorComponent.getText();
//    else return item;
  }

  public void setItem(Object item) {
    if (item != null)  editorComponent.setText(item.toString());
    else  editorComponent.setText("");
//    this.item = item;
  }

  public void focusGained(FocusEvent fe) {
    selectAll();
  }

  public void focusLost(FocusEvent fe) {
  }

}

package org.trinet.util.graphics.text;
import javax.swing.text.*; 
import java.text.*; 
public class NumberTextFieldFixed extends NumberTextField {
    public NumberTextFieldFixed(float value, int columns, DecimalFormat f) {
        this(new Double(value), columns, f);
    }
    public NumberTextFieldFixed(double value, int columns, DecimalFormat f) {
        this(new Double(value), columns, f);
    }
    public NumberTextFieldFixed(int value, int columns, DecimalFormat f) {
        this(new Long(value), columns, f);
    }
    public NumberTextFieldFixed(long value, int columns, DecimalFormat f) {
        this(new Long(value), columns, f);
    }
    public NumberTextFieldFixed(Object value, int columns, DecimalFormat f) {
        this(columns, f);
        super.setValue(value);
    }
    public NumberTextFieldFixed(int columns, DecimalFormat f) {
        super(new FormattedNumberDocument(f, columns), columns, f);
    }
}

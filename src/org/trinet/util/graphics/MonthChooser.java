package org.trinet.util.graphics;

//import java.awt.*;
//import java.awt.event.*;
import javax.swing.*;
//import javax.swing.border.*;


public class MonthChooser extends JComboBox{

    static final String[] monthString = {"JAN",	// display months as strings
			    "FEB",
			    "MAR",
			    "APR",
			    "MAY",
			    "JUN",
			    "JUL",
			    "AUG",
			    "SEP",
			    "OCT",
			    "NOV",
			    "DEC"};

     public MonthChooser() {

            super( (Object[]) monthString);
//            setSelectedIndex(mo);
            setEditable(false);

     }
} 

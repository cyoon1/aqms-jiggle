package org.trinet.util.graphics;

/**
 * Extention of JComboBox for choosing an integer from a range. JComboBoxes handle lists of
 * Objects and are generic. This is specific to a list of Integer objects.
 */
import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.*;

import java.awt.event.*;
import org.trinet.util.graphics.text.*;


public class NumberChooser extends JComboBox {

// the value
  double selected;
  boolean editable = true;

  public NumberChooser() {
      setEditable(editable);
      addListeners();
  }

/**
 * Create a chooser that displays integers with the given range.
 * The variable 'selected' will change dynamically with the value of the chooser box so
 * that no special getValue method needs to be called by the caller. By default the box
 * is editable.
 * Values in ComboBox list are Strings. Values outside the given range are not tested for.
 */
 /*
public NumberChooser(int min, int max, int interval, int sel)
{


// Can't do this because you end up with a decimal number in the chooser if you do
 //      this ((double) min, (double) max, (double) interval, (double) sel);

    selected = sel;

    // make a vector of selection values

    for (int i = min; i <= max; i = i + interval)
    {
        addItem( String.valueOf(i) );                // add the selection to the box list
//        addItem( ""+i );                // add the selection to the box list
    }
    int nchar = String.valueOf(max).length();

    setSelectedItem( String.valueOf( (int) selected) );




      setEditable(editable);
      addListeners();
}
*/
/**
 * Create a chooser that displays decimal numbers with the given range.
 * The variable 'selected' will change dynamically with the value of the chooser box so
 * that no special getValue method needs to be called by the caller. By default the box
 * is editable.
 * Values in ComboBox list are Strings. Values outside the given range are not tested for.
 */
public NumberChooser(double min, double max, double interval, double sel)
{
    selected = sel;

    // make a vector of selection values
    // !!! THIS IS SLOW !!!!
    for (double i = min; i <= max; i = i + interval)
    {
        addItem( String.valueOf(i) );                // add the selection to the box list
//        addItem( ""+i );                // add the selection to the box list
    }
    int nchar = String.valueOf(max).length();
//    setEditor(new ComboBoxTextEditor(12, nchar));
//    setRenderer(new ComboBoxTextRenderer(12, nchar));
    setEditable(editable);
    setSelectedItem( String.valueOf(selected) );

    // make the width of the thing match the max value
    int wid = getFont().getSize();
    int hig = this.getHeight();
    setSize (wid * nchar, hig);

    addListeners();
}

/** Added action listeners */
private void addListeners() {
// the action listerner automatically updates 'selected' when some uses the pull-down
// part of the chooser OR hits <CR> after typing in the text area.
    addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              NumberChooser nc = (NumberChooser) e.getSource();
              selected = nc.getSelectedValue();
          }
    });

// This does the format check when the box gets edited
        ComboBoxEditor editor = getEditor();
        editor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              getSelectedValue();
              /*
              String str = (String) getSelectedItem();
              if (str == null) return;
              try {
               selected = Double.parseDouble(str);
              } catch (NumberFormatException evt) {
               System.out.println (" % Bad number format in NumberChooser: " + "\""+  str + "\"" );
               // fall back to previously selected value
               setSelectedItem( String.valueOf(selected) );
              }
              */
            }
        });

}

/**
 * Return the currently selected value. If there's a format error the previously
 * selected value is returned and restored to the chooser.
 */
public double getSelectedValue()
{
    String str = (String) getSelectedItem();
    if (str == null) return selected;
    try {
      selected = Double.parseDouble(str);
    }
    catch (NumberFormatException e)
    {
//v1.2        Toolkit.getDefaultToolkit().beep();
        System.out.println (" % Bad number format in NumberChooser: " + "\""+  str + "\"" );
    // fall back to previously selected value
        setSelectedItem( String.valueOf(selected) );
        return selected;
    }

    return selected;
}

/*
    public static void main(String s[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        final NumberChooser numberChooser = new NumberChooser(-2, 20, 1, 3);

//        frame.getContentPane().add("North", numberChooser);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add (numberChooser, BorderLayout.CENTER);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {

           public void actionPerformed(ActionEvent e) {
               System.out.println ("value = "+numberChooser.getSelectedValue());
           }
        });

        panel.add(okButton, BorderLayout.SOUTH);
        frame.getContentPane().add(panel);


        frame.pack();
        frame.setVisible(true);

    }
*/
// INNER CLASS
    class MyComboBoxEditor extends BasicComboBoxEditor {

     public MyComboBoxEditor() {
        super();
     }
           public void focusGained (FocusEvent e) {
              super.focusGained(e);
              //System.out.println ("Focus gained.");
           }
           public void focusLost (FocusEvent e) {
              super.focusLost(e);
              NumberChooser nc = (NumberChooser) e.getComponent();
              selected = nc.getSelectedValue();
              //System.out.println ("Focus Lost: value = "+selected+ "  -> "+nc.getSelectedValue() );
           }

    }

} // end of class


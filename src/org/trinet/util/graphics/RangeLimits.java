package org.trinet.util.graphics;

/**
* Specifies a lower and a upper limit (bound) for a range of type Number.
*/
public class RangeLimits extends org.trinet.util.NumberRange implements Constraints, Cloneable {

/**
 * Default constructor, no limits.
 */ 
    
    public RangeLimits() {
        super();
    }
/**
* Specify limit, null == no limit.
*/
    public RangeLimits(Number min, Number max) {
	super(min, max);
    }

/** Returns string of the form "Min: min Max: max" */
    public String toString() {
	String str = "Min: ";
	if (hasMinLimit()) str = str + min.toString();
	else str += "none ";
	if (hasMaxLimit()) str = str + " Max: " + max.toString();
	else str += "none";
	
	return str;
    }

/** Override default */
    public Object clone() {
        return (RangeLimits) super.clone();
    }

/** Returns a double representing the minimum limit. */
    public double getMinValue() { return min.doubleValue(); }

/** Returns a double representing the maximum limit. */
    public double getMaxValue() { return max.doubleValue(); }

    public boolean contains(Object dataObject) {
        return ! excludes(dataObject);
    }

    public boolean excludes(Object dataObject) {
        return (dataObject instanceof Number) ? excludes((Number) dataObject) : true;
    }

    public boolean excludes(Number dataObject) {
	    if (dataObject instanceof Long) {
		return excludes(((Long) dataObject).longValue());
	    }
	    else if (dataObject instanceof Double) {
		return excludes(((Double) dataObject).doubleValue());
	    }
	    else if (dataObject instanceof Integer) {
		return excludes(((Integer) dataObject).intValue());
	    }
	    else if (dataObject instanceof Float) {
		return excludes(((Float) dataObject).floatValue());
	    }
	    else if (dataObject instanceof Short) {
		return excludes(((Short) dataObject).shortValue());
	    }
	    else if (dataObject instanceof Byte) {
		return excludes(((Byte) dataObject).byteValue());
	    }
	    return true;
    }

    public boolean excludes(long value) {
	if (hasMinLimit()) {
	    if (value < min.longValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.longValue()) return true;
	}
	return false;
    }

    public boolean excludes(double value) {
	if (hasMinLimit()) {
	    if (value < min.doubleValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.doubleValue()) return true;
	}
	return false;
    }

    public boolean excludes(int value) {
	if (hasMinLimit()) {
	    if (value < min.intValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.intValue()) return true;
	}
	return false;
    }

    public boolean excludes(float value) {
	if (hasMinLimit()) {
	    if (value < min.floatValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.floatValue()) return true;
	}
	return false;
    }

    public boolean excludes(short value) {
	if (hasMinLimit()) {
	    if (value < min.shortValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.shortValue()) return true;
	}
	return false;
    }

    public boolean excludes(byte value) {
	if (hasMinLimit()) {
	    if (value < min.byteValue()) return true;
	}
	if (hasMaxLimit()) {
	    if (value > max.byteValue()) return true;
	}
	return false;
    }
} // end of RangeLimits class

package org.trinet.util.graphics;

import javax.swing.*;
import java.net.URL;
import java.awt.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.*;
import javax.swing.text.*;
import javax.swing.event.*;

/*
 * This class is based on /opt/jdk1.2beta4/demo/jfc/Metalworks/src/MetalworksHelp.java
 * @version 1.4 04/24/98
 * @author Steve Wilson
 * Copyright 1998 by Sun Microsystems, Inc.,
 * Modified by DDG Jan. 12, 1999. Allowed caller to define help file, changed to JFrame from JInternalFrame
 */
public class HtmlPane extends JScrollPane implements HyperlinkListener {

    JEditorPane htmlPane;
    boolean verbose = true;

    public HtmlPane(String urlFile) {
        this(urlFile, true);
    }

    public HtmlPane(String urlFile, boolean openUrl) {
        if (openUrl) openURL(urlFile);
    }

    public void setVerbose(boolean tf) { verbose = tf; }

    public boolean openURL(String urlFile) {
        String s = urlFile;
        if (! s.toLowerCase().startsWith("http:") ) {
            if (! s.toLowerCase().startsWith("file:") ) {
                File f = new File (s);
                s = f.getAbsolutePath();
                s = "file:"+s;
            }
        }
        URL url = null;
        try {
            url = new URL(s);
            htmlPane = new JEditorPane(s);
            htmlPane.setEditable(false);
            htmlPane.addHyperlinkListener(this);
            JViewport vp = getViewport();
            vp.add(htmlPane);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            htmlPane = null;
        } catch (IOException e) {
            if (e instanceof java.net.ConnectException || e instanceof java.io.FileNotFoundException ) { 
                if (verbose) System.err.println("HtmlPane unable open URL: " + url);
            }
            else e.printStackTrace();
            htmlPane = null;
        }        
        return hasValidPane();
    }

    public boolean hasValidPane() {
        //System.out.println("DEBUG HtmlPane hasValidPane():" + (htmlPane != null));
        return (htmlPane != null);
    }

    /**
     * Notification of a change relative to a 
     * hyperlink.
     */
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            linkActivated(e.getURL());
        }
    }

    /**
     * Follows the reference in an
     * link.  The given url is the requested reference.
     * By default this calls <a href="#setPage">setPage</a>,
     * and if an exception is thrown the original previous
     * document is restored and a beep sounded.  If an 
     * attempt was made to follow a link, but it represented
     * a malformed url, this method will be called with a
     * null argument.
     *
     * @param u the URL to follow
     */
    protected void linkActivated(URL u) {
        Cursor c = htmlPane.getCursor();
        Cursor waitCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
        htmlPane.setCursor(waitCursor);
        SwingUtilities.invokeLater(new PageLoader(u, c));
    }

    /**
     * Temporary class that loads synchronously (although
     * later than the request so that a cursor change
     * can be done).
     */
    class PageLoader implements Runnable {
        
        URL url;
        Cursor cursor;

        PageLoader(URL u, Cursor c) {
            url = u;
            cursor = c;
        }

        public void run() {
            if (url == null) {
                // restore the original cursor
                htmlPane.setCursor(cursor);

                // automatic validation is activated.
                Container parent = htmlPane.getParent();
                parent.repaint();
            } else {
                Document doc = htmlPane.getDocument();
                try {
                    htmlPane.setPage(url);
                } catch (IOException ioe) {
                    htmlPane.setDocument(doc);
                    if (ioe instanceof java.io.FileNotFoundException) System.err.println("File not found:"+ ioe.getMessage());
                    getToolkit().beep();
                } finally {
                    // schedule the cursor to revert after the paint has happended.
                    url = null;
                    SwingUtilities.invokeLater(this);
                }
            }
        }
    }
}

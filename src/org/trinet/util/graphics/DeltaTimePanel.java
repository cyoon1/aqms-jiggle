package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import org.trinet.util.*;
/** A time chooser that has two elements; a value and units. Generally used to
* define a delta or duration. <p>
*
* Available time units are: "Minutes", "Hours", "Days", "Weeks", "Years".
*/
public class DeltaTimePanel extends JPanel {

     BorderLayout borderLayout1 = new BorderLayout();
     JLabel    mainLabel = new JLabel();
     JComboBox unitsBox  = new JComboBox();
     NumberChooser valueBox  = new NumberChooser(0, 1, 1, 0);

     JPanel    mainPanel = new JPanel();

     // LOGIC IN THIS CLASS DEPENDS ON ALL THE ARRAYS BEING CORRECTLY ALIGNED

     // don't do months because I don't want to deal with the complexity now.
     // Besides for a delta we don't know WHICH month we're measuring from.
     // static final String unitName[] = {"Minutes", "Hours", "Days", "Weeks", "Years"};

     // second in each time unit (NOTE: year assumes 365 days/yr, :. leap year
     // are not handled correctly).
     // static final double secondsIn[] = {60., 3600., 86400., 604800., 31536000.};

     // Lists of semi-reasonable values for each unit type
     String scValues[] = {"30", "60", "90", "120", "360", "600"};
     String mnValues[] = {"1", "5", "10", "15", "20", "25", "30", "45", "60"};
     String hrValues[] = {"1", "6", "12", "24", "48", "72", "96"};
     String dyValues[] = {"1", "2", "3", "4", "5", "6", "7", "14", "21", "30"};
     String wkValues[] = {"1", "2", "4", "8", "52"};
     String yrValues[] = {"1", "2", "3", "4", "5", "10", "20"};

     /** Array of arrays defined above. Position in list is significant. */
     Object valueArray[] = {scValues, mnValues, hrValues, dyValues, wkValues, yrValues};

     /** The current value. Used as fallback if user enters garbage. */
     double defaultValue;

     /** Create a DeltaTimePanel with the given text label. */
     public DeltaTimePanel(String label) {
       this();
       mainLabel.setText(label);
     }

     /** */
     public DeltaTimePanel() {
          try  {
               jbInit();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }
     /** GUI build method used by JBuilder design tool. */
     private void jbInit() throws Exception {
          this.setLayout(borderLayout1);
          mainLabel.setText("Choose delta time");
          unitsBox.setToolTipText("Select units");
          addItems(unitsBox, TimeUnits.getKnownUnitsNames());
          unitsBox.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    unitsBox_actionPerformed(e);
               }
          });

          valueBox.setToolTipText("Select or type value");
          valueBox.setEditable(true);
          addValues(valueBox, (String[]) valueArray[TimeUnits.HOUR]);

          this.add(mainLabel, BorderLayout.NORTH);
          this.add(mainPanel, BorderLayout.CENTER);
          mainPanel.add(unitsBox);
          mainPanel.add(valueBox);

          // start with hours
          unitsBox.setSelectedIndex(TimeUnits.HOUR);    // hours
     }

     /** Add an array of items to a JComboBox. */
     void addItems(JComboBox combo, String[] itemList) {

          combo.setModel(new DefaultComboBoxModel(itemList));

          /*
          try {
            //combo.removeAllItems();
          } catch (IndexOutOfBoundsException ex) {} // does this if empty

          if (itemList.length > 0) {
             for (int i = 0; i < itemList.length; i++) {
                 combo.addItem(itemList[i]);
             }
          }
          */
     }

          /** Add an array of items to a JComboBox. */
     void addValues(JComboBox combo, String[] itemList) {
          addItems(combo, itemList);
          if (itemList.length > 0) defaultValue = Double.parseDouble(itemList[0]);
     }

     /** Set the value combo box value. If the value is not in the box is will be added. */
     public void setValue(double value) {
        setValue(""+value);  // force to string
     }
     /** Set the value combo box value. If the value is not in the box is will be added. */
     public void setValue(String str) {
        valueBox.setSelectedItem(str);  // force to string
     }

     /** Set the units combo box value. If the string is not in the box is will be added. */
     public void setUnits(String units) {
         int idx = TimeUnits.getUnitIndex(units);
         if (idx >= 0) unitsBox.setSelectedIndex(idx);
     }

     /** Enable/disable all the active components that make up this component.*/
     public void setEnabled(boolean tf) {
        mainLabel.setEnabled(tf);
        unitsBox.setEnabled(tf);
        valueBox.setEnabled(tf);
     }

     /** If units are changed, change the list of values. */
     void unitsBox_actionPerformed(ActionEvent e) {

         // Set units list to values approriate for the unit type
         // (expects unitName array and valueArray arrays to be aligned)
         int item = unitsBox.getSelectedIndex();

         addValues(valueBox, (String[]) valueArray[item]);

     }

     /** Returns a TimeSpan object that begins delta time before NOW and ends NOW. */
     public TimeSpan getTimeSpan() {
         return  getTimeSpan(new DateTime().getTrueSeconds()); // changed to UTC - aww 2008/02/07
     }

     /** Returns a TimeSpan object that begins delta time before this time and end
     * at the given time. */
     public TimeSpan getTimeSpan(double epochSecs) {
         return new TimeSpan(epochSecs - getSeconds(), epochSecs);
     }

     /**
     * Return delta as a duration in seconds. If there's a format error in the
     * value entered the previously selected value is returned.
     */
     public double getSeconds() {
         double val = getValue();
         // note this short-cut, rather than calling getUnits
         int index = unitsBox.getSelectedIndex();

         return val * TimeUnits.getSecondsIn(index);
     }

     /** Return the value. Note that you must also use getUnits() to know how
     * to interpret this value. */
     public double getValue() {
          String str = (String) valueBox.getSelectedItem();
          try {
              defaultValue = Double.parseDouble(str);
          }
          catch (NumberFormatException e) {
             //v1.2 Toolkit.getDefaultToolkit().beep();
             System.out.println(" % Bad number format in NumberChooser: " + "\""+  str + "\"" );
             // fall back to previously selected value
             valueBox.setSelectedItem( String.valueOf(defaultValue) );
             return defaultValue;
           }

           return defaultValue;
    }

     /** Return the units string. "Minute", "Hour", etc. 
     * @see: TimeUnits */
      public String getUnits() {
          return (String) unitsBox.getSelectedItem();
      }

/*
  static public final class Tester {
     public static void main(String[] args) {
        JFrame frame = new JFrame("Delta Time Panel");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        final DeltaTimePanel panel = new DeltaTimePanel("Choose a delta");
        frame.getContentPane().add("Center", panel);

        JButton button = new JButton("Show Values");
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(panel.getValue()+"  "+panel.getUnits()+ "   "+panel.getSeconds());
               }
          });
        frame.getContentPane().add("South", button);
        frame.pack();
        frame.setVisible(true);
    }
  }
*/
} 

// Base class for a generic cell editor - read comments about descendent classes and the editorComponent overrides !!!
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.util.EventObject;
import java.io.Serializable;

// Stripped down DefaultCellEditor (noCheckbox or noComboList constructors also no delegated inner class editor component)
// Extensions of this class should implement constructors that addActionListener to editorComponent

public class AbstractCellEditor implements CellEditor, TableCellEditor, TreeCellEditor, ActionListener, ItemListener, Serializable {
  protected EventListenerList listenerList = new EventListenerList();
  protected Object value;
  protected ChangeEvent changeEvent = null;
  protected int clickCountToStart = 2;

  public AbstractCellEditor() {} // empty default constructor

// Must add cast of component type and then addActionListener in constructor for inhereiting decendent classes
//  public NumberTextFieldEditor () {
//    ((NumberTextField)editorComponent).addActionListener(this);
//  }

// Methods to implement DefaultCellEditor like behavoir, should be overidden by decendent classes
  public boolean startCellEditing(EventObject anEvent) {
    return true;
  }
  public Component getComponent() {
    return null;
  }
  public void setCellEditorValue(Object value) {
// An example of how extending class as textfield type should override this method:
//    if (value != null) ((NumberTextField) editorComponent).setText(value.toString());
//    else ((NumberTextField)editorComponent).setText("");
    this.value = value;
  }
  public void setClickCountToStart(int count) {
    clickCountToStart = count;
  }
  public int getClickCountToStart() {
    return clickCountToStart;
  }
// Start Cell editor implementation
  public Object getCellEditorValue() {
// An example of how extending class as textfield type should override this method:
//    return ((NumberTextField)editorComponent).getText();
    return value;
  }
  public boolean isCellEditable(EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      return (((MouseEvent)anEvent).getClickCount() < clickCountToStart) ?
        false : true;
    }
    else if (anEvent instanceof TableCellEditEvent) {
    //else if (anEvent instanceof ActionEvent) {
       return true;
    }
    else {
       return false;
    }
  }
  public boolean shouldSelectCell(EventObject anEvent) {
    if (anEvent instanceof MouseEvent) {
      return (((MouseEvent)anEvent).getClickCount() < 1) ?
        false : true;
    }
    return true;
  }
  public boolean stopCellEditing() {
//  Debug.println("AbstractCellEditor stopCellEditing fireEditingStopped");
    fireEditingStopped();
    return true;
  }
  public void cancelCellEditing() {
//  Debug.println("AbstractCellEditor cancelCellEditing fireEditingCanceled");
    fireEditingCanceled();
  }
  public void addCellEditorListener(CellEditorListener l) {
    listenerList.add(CellEditorListener.class, l);
  }
  public void removeCellEditorListener(CellEditorListener l) {
    listenerList.remove(CellEditorListener.class, l);
  }
// End of CellEditor interface implementation

// Implement TreeCellEditor interface
  public Component getTreeCellEditorComponent( JTree tree, Object value,
        boolean isSelected, boolean expanded, boolean leaf, int row) {
    return null;
  }

// Implement TableCellEditor interface
  public Component getTableCellEditorComponent( JTable table, Object value,
              boolean isSelected, int row, int column) {
    return null;
// Inhereiting decendent classes should implement override, for example:
//    setCellEditorValue(value);
//    return editorComponent;
  }

// methods for implementation of listener signals
  protected void fireEditingStopped() {
    Object[] listeners = listenerList.getListenerList();
// listenerlist array of pairs (listenerclasstype,listenername) thus the indexing by 2
    for (int i = listeners.length-2; i>=0; i-=2) {
      if (listeners[i] == CellEditorListener.class) {
        if (changeEvent == null)
          changeEvent = new ChangeEvent(this);
        ((CellEditorListener)
        listeners[i+1]).editingStopped(changeEvent);
      }         
    }
  }
  protected void fireEditingCanceled() {
    Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length-2; i>=0; i-=2) {
      if (listeners[i]==CellEditorListener.class) {
        if (changeEvent == null)
          changeEvent = new ChangeEvent(this);
        ((CellEditorListener)
        listeners[i+1]).editingCanceled(changeEvent);
      }         
    }
  }

// Implementing ActionListener interface
  public void actionPerformed(ActionEvent e) {
//  Debug.println("AbstractCellEditor actionPerformed fireEditingStopped" + e.getSource());
    fireEditingStopped();
  }

// Implementing ItemListener interface
  public void itemStateChanged(ItemEvent e) {
//  Debug.println("AbstractCellEditor itemStateChanged fireEditingStopped" + e.getSource());
    fireEditingStopped();
  }
}

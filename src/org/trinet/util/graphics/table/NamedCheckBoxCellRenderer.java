package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;

public class NamedCheckBoxCellRenderer extends CheckBoxCellRenderer {
    private String colName;

    public NamedCheckBoxCellRenderer (String colName, Object value) {
        super(value);
        this.colName = colName;
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
        if (table.getColumnName(col).equalsIgnoreCase(colName)) {
          return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        }
        else {
          System.out.println("CB renderer return NULL: mismatch of table column name");
          return null;
        }
    }
}

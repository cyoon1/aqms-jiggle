package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.Arrays;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.graphics.*;
public class JasiSolutionListTable extends AbstractJasiListTable {
    public JasiSolutionListTable(JasiSolutionListTableModel tm) {
            super(tm);
            loadTableProperties("SolutionListTable.props");
    }
    protected boolean  configureColEditorRendererByName(TableCellEditorRenderer tcer,
              int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer, modelIndex, colName);
          if (setValues) return true;
          // ZF tf, HF tf, TF tf, PR priority long, STALE tf
          // V validFlag=select binary, BFLG dummyFlag=bogus binary
          // possible: altSolList.size,altMagList.size 
          if (colName.equals("ETYPE")) {
            // "ETYPE {"eq", "qb", "sn", "nt", "st", "uk"};
            tcer.tcEditor =
               new ComboBoxCellEditor("ETYPE", EventTypeMap3.getMap().getJasiTypeArray());
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("MT")) {
            // "MT "l","h","c","d","e","w","s","a","b"
            tcer.tcEditor = new ComboBoxCellEditor("MT", new String [] {
              "l","h","c","d","e","w","lr","s","a","b"
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("TYPE")) {
            tcer.tcEditor = new ComboBoxCellEditor("TYPE", new String [] {
              "H", "C", "A"
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          return setValues;
    }
    public boolean isCellEditable(int rowIndex, int colIndex) {
        Solution sol = (Solution) getSelectedJasiCommitable();
        if (sol == null) return super.isCellEditable(rowIndex,colIndex);
        if (sol.isLocked() && !sol.lockIsMine()) {
          String str = "EVENT " + sol.getId().toString() + " IS LOCKED.\n" 
            + sol.getLockString();
          InfoDialog.informUser(null, "ERROR", str, null);
          return false;
        }
        if (!sol.isLocked() && !sol.lock()) {
          String str = "UNABLE TO LOCK EVENT " + sol.getId().toString() + "\n" 
            + sol.getLockString();
          InfoDialog.informUser(null, "ERROR", str, null);
          return false;
        }
        return super.isCellEditable(rowIndex,colIndex);
    }
}

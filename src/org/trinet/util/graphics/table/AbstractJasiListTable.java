// modify tableModel references through sorter methods: track row index in model
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;
public abstract class AbstractJasiListTable extends JTable implements JasiListTableIF {
    private JTable  tableRowHeader;
    protected String propFileName;
    protected TextLogger aLog = new TextLogger();
    protected JasiListTableModelIF tableModel;
    private TableSorterAbs sorter;
    private JScrollPane  tableScrollPane;
    protected Font font;
    private int fontSize = 12;
    private static final String DEFAULT_SELECTION_MODE = "single";
    private String selectionMode = DEFAULT_SELECTION_MODE;
    protected JasiPropertyList inputProperties;
    protected String [] tableColumnNameOrder;
    protected boolean [] tableColumnShown;
    protected boolean [] tableColumnEditable;
    protected boolean [] tableColumnNullable;
    protected int [] tableColumnCellWidthPadding;
    protected int [] tableColumnFractionDigits;
    private static final int ID_COLUMN = 0;
    protected ArrayList hiddenColumnList = new ArrayList();
    protected ArrayList keyColumnList = new ArrayList();
    protected TableColumnModel defaultColumnModel = null;
    protected JTableClipboardPopupMenu viewClipboard;
    protected JTableClipboardPopupMenu headerClipboard;
    protected class TableCellEditorRenderer {
        TableCellEditor   tcEditor = null;
        TableCellRenderer tcRenderer = null;
    }
    protected TableCellEditorRenderer tcer = new TableCellEditorRenderer();

    protected static final String PATTERN9 = "#0.000000"; // generic four byte float
    public static final int MAX_FRACTION_ELEMENTS=16;
    public static final int DEFAULT_COLUMN_WIDTH_PADDING=2; // arbitrary pixels
    public static final int MAX_COLUMN_WIDTH=200;    // arbitrary pixels, wider than a datetime
    public static final int MAX_RMK_COLUMN_WIDTH=60; // arbitrary pixels
    public static final int MINIMUM_ROW_HEIGHT = 20; // arbitrary pixels depends on font

// Table constructor
    protected AbstractJasiListTable(JasiListTableModelIF tm) {
        //this.tableModel = tm;
        setJasiTableModel(tm);
    }

    protected boolean configureColEditorRendererByName(TableCellEditorRenderer tcer,
            int modelIndex, String colName) {
          boolean setValues = false;
           if (colName.toUpperCase().indexOf("ID") >=  0) {
            // renderer must be mapped to sorter, not tableModel, rendered color relies on sorted row order
            tcer.tcRenderer =
                new DeletedReadingRowCellNumberRenderer((AbstractTableModel) sorter,
                                tableModel.getDeleteColumnIndex());
            ((DeletedReadingRowCellNumberRenderer ) tcer.tcRenderer).setDefaultBackground(Color.yellow);
            ((DeletedReadingRowCellNumberRenderer ) tcer.tcRenderer).setDeletedRowColor(Color.red);
            setValues = true;
           }
           else if (colName.toUpperCase().indexOf("TIME") >= 0) {
            tcer.tcEditor = new DateTimeEditor(font); // need here leap sec aware DateTime class 2008/02/7 -aww
            tcer.tcRenderer = new DateTimeRenderer(); // need here leap sec aware DateTime class 2008/02/7 -aww
            setValues = true;
          }
          else if (colName.equals("PFLG")) {
            tcer.tcEditor = new ComboBoxCellEditor("PFLG", new String [] {"H","F"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("PFLG", new String [] {"a","h","f","A","H","F"});
            setValues = true;
          }
          else if (colName.equals("DFLG")) {
            tcer.tcEditor = new NamedCheckBoxCellEditor("DFLG", "false");
            tcer.tcRenderer = new NamedCheckBoxCellRenderer("DFLG", "false");
            setValues = true;
          }
          else if (colName.equalsIgnoreCase("RMK") 
                  // || colName.equalsIgnoreCase("REMARK") || colName.equalsIgnoreCase("COMMENT")
                  ) {
            tcer.tcEditor = new TextCellEditor(font);
            tcer.tcRenderer = new ColorableTextCellRenderer(font) {
                public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected,
                                boolean hasFocus, int row, int col) {
                     Component comp = super.getTableCellRendererComponent(jtable, value, isSelected,
                                     hasFocus, row, col);
                    ((JComponent) comp).setToolTipText(value.toString());
                    return comp;
                }
            };
            setValues = true;
          }
          return setValues;
    }
    protected boolean configureColEditorRendererByClass(TableCellEditorRenderer tcer,
                    int modelIndex, Class aClass) {
          String pattern = null;                // format pattern buffer
          boolean setValues = false;
          if (aClass.getName().indexOf("String") >= 0) {
            tcer.tcEditor = new TextCellEditor(font);
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (aClass.getName().indexOf("Double") >= 0) {
            int fractionWidth = tableModel.getColumnFractionDigits(modelIndex);
            if (fractionWidth >= 0) {
              if (fractionWidth > MAX_FRACTION_ELEMENTS - 1 ) fractionWidth = MAX_FRACTION_ELEMENTS - 1;
              pattern = getNumberFormatPattern(fractionWidth);
              tcer.tcEditor = new FormattedNumberEditor_FW(pattern);
              tcer.tcRenderer = new FormattedNumberRenderer(pattern);
            }
            else {
               tcer.tcEditor = new FormattedNumberEditor_FW(PATTERN9);
               tcer.tcRenderer = new FormattedNumberRenderer(PATTERN9);
            }
            setValues = true;
          }
          else if (aClass.getName().indexOf("Long") >= 0 || aClass.getName().indexOf("Integer") >= 0 ||
            aClass.getName().indexOf("Number") >= 0 ) {
            pattern = getNumberFormatPattern(0);
            tcer.tcEditor = new FormattedNumberEditor_FW(pattern);
            tcer.tcRenderer = new FormattedNumberRenderer(pattern);
            setValues = true;
          }
          else if (aClass.getName().indexOf("Boolean") >= 0)  {
            tcer.tcEditor = new CheckBoxCellEditor(Boolean.FALSE);
            tcer.tcRenderer = new CheckBoxCellRenderer(Boolean.FALSE);
            setValues = true;
          }
          return setValues;
    }

    public final void setColumnNameOrder(String [] names) {
       if (names != null) tableColumnNameOrder = names;
    }
    public final String [] getColumnNameOrder() {
        return tableColumnNameOrder;
    }

    public String [] getTableColumnHeaders() {
        int count = getColumnCount();
        if (count < 1) return null;
        String [] headers = new String[count];
        TableColumnModel tcm = getColumnModel();
        for (int i = 0; i < count; i++) {
            headers[i] = (String) tcm.getColumn(i).getHeaderValue();
        }
        return headers;
    }

    public int sortedToModelRowIndex(int tableRowIndex) {
        return ((sorter == null) ||
            (tableRowIndex < 0) ||
            (tableRowIndex > sorter.indexes.length-1)
           ) ? -1 : sorter.indexes[tableRowIndex];
    }
    public int modelToSortedRowIndex(int modelRowIndex) {
        for (int idx=0; idx < sorter.getRowCount(); idx++) {
            if (sorter.indexes[idx] == modelRowIndex) return idx;
        }
        return -1;
    }

    public int getSelectedRowIndex() {
        return (tableRowHeader == null) ? -1 : tableRowHeader.getSelectedRow();
    }
    public int getSelectedRowModelIndex() {
        int idx = getSelectedRowIndex();
        return (idx < 0) ? idx : sortedToModelRowIndex(idx);
    }
    public JasiCommitableIF getSelectedJasiCommitable() {
        int idx = getSelectedRowModelIndex();
        if (idx < 0) return null;
        return tableModel.getJasiCommitable(idx);
    }
/** Returns array containing indices of all rows selected in table row header.
 *  Returns zero-length array if none are selected.
 *  */
    public int [] getSelectedRowIndices() {
        return tableRowHeader.getSelectedRows();
    }
/** Returns array containing selected rows in table row header. */
    public JasiCommitableIF [] getSelectedJasiCommitables() {
        if (tableRowHeader == null) return null;
        int selected [] = tableRowHeader.getSelectedRows();
        int nselected = selected.length;
        if (nselected == 0) return null;
        JasiCommitableIF [] readings = new JasiCommitableIF[nselected];
        ActiveArrayListIF jrList = tableModel.getList();
        if (nselected > jrList.size())
            throw new ArrayIndexOutOfBoundsException("table selectedRow count > model list.size().");
        for (int index = 0; index < nselected; index++) {
          readings[index] =
              (JasiCommitableIF) jrList.get(sortedToModelRowIndex(selected[index]));
        }
        return readings;
    }

/** Returns long array containing selected event ids from table row header.  */
    public long [] getSelectedIds() {
        if (tableRowHeader == null) return null;
        int rowIds[] = tableRowHeader.getSelectedRows();
        int nrows = rowIds.length;
        if (nrows == 0) return null;
        long [] ids = new long [nrows];
        try {
          for (int index = 0; index < nrows; index++) {
            ids[index] =
              Long.parseLong((tableModel.getValueAt(sortedToModelRowIndex(rowIds[index]), ID_COLUMN)).toString());
              //((DataObject) tableModel.getValueAt(sortedToModelRowIndex(rowIds[index]), ID_COLUMN)).longValue();
          }
        } 
        catch (NumberFormatException ex) { ex.printStackTrace();}
        return ids;
    }
 
    public TableSorterAbs getTableSorter() {
        return sorter;
    }

    public JasiListTableModelIF getTableModel() {
         return this.tableModel;
    }

    public JTable getRowHeader() {
         return tableRowHeader;
    }

    public void cancelTableCellEditing() {
       if (isEditing()) {
         getCellEditor().cancelCellEditing();
       }
       JTable aTable = getRowHeader();
       if (aTable != null && aTable.isEditing()) {
         aTable.getCellEditor().cancelCellEditing();
       }
    }

    public void stopTableCellEditing() {
       if (isEditing()) {
         getCellEditor().stopCellEditing();
       }
       JTable aTable = getRowHeader();
       if (aTable != null && aTable.isEditing()) {
         aTable.getCellEditor().stopCellEditing();
       }
    }

    /* 
    public JTable getTable() {
         return this;
    }
    */

    private final class TableHeaderRenderer extends DefaultTableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object value,
                             boolean isSelected, boolean hasFocus, int row, int column) {
            if (table != null) {
                JTableHeader header = table.getTableHeader();
                if (header != null) {
                    setForeground(header.getForeground());
                    setBackground(header.getBackground());
                    setFont(header.getFont());
                }
            }

            setText((value == null) ? "" : value.toString());
//            setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
            setHorizontalAlignment(JLabel.CENTER);
            return this;
        }
    }

    public JScrollPane createTableScrollPane(JasiListTableModelIF tableModel) {
      //this.tableModel = tableModel;
      setJasiTableModel(tableModel);
      return createTableScrollPane();
    }
    public void setJasiTableModel(JasiListTableModelIF tm) {
      if (tm == null) return;
      this.tableModel = tm;
      if (sorter != null) tableModel.removeTableModelListener(sorter);
      if (sorter == null) sorter = new TableSorterAbs();
      sorter.setModel(tableModel);
      super.setModel(sorter);
    }
    public JScrollPane createTableScrollPane() {
        //Debug.println("Creating JTable scrollpane for " + getClass().getName());
          //loadTableProperties(); // not here
        //if (sorter == null) sorter = new TableSorterAbs();
        //sorter.setModel(tableModel);
        //setModel(sorter);
        if (viewClipboard == null) viewClipboard = new JTableClipboardPopupMenu(); 
        viewClipboard.setComponent(this);
        // save here default TableColumnModel having every TableModel column?
        if (font == null) font = new Font("Monospaced", Font.PLAIN, fontSize);
        setTableAttributes(this); // setup table selection/color attributes
        configureTableColumnAttributes(); // does sizing b4 header or hidden removal
        // after attrib, b4 rowheader?? test it aww
        setColumnModel(configureColumnOrder(true));
        configureTableRowHeader(this);
        removeHiddenColumnsFromTable(this);
        configureTableListeners();
        setPreferredScrollableViewportSize(getPreferredSize());
        return createScrollPane();
    }
    protected TableColumnModel configureColumnOrder(boolean logMissingNames) {
        TableColumnModel tcmTmp = new DefaultTableColumnModel();
        int maxCol = tableColumnNameOrder.length; 
        for (int i = 0; i < maxCol; i++) {
            String colName = tableColumnNameOrder[i];
            try {
                tcmTmp.addColumn(defaultColumnModel.getColumn(defaultColumnModel.getColumnIndex(colName)));
            }
            catch (IllegalArgumentException ex) {
                if (logMissingNames) 
                aLog.logTextnl("Check column order by name - No such table column name:" +
                   colName + " for " + getClass().getName());
            }
        }
        return tcmTmp;
    }
// size tables and put in scrollpane
    protected JScrollPane createScrollPane () {
        JScrollPane scrollpane = null;
        tableRowHeader.setPreferredScrollableViewportSize(tableRowHeader.getPreferredSize());
        if (getColumnCount() <= 0) {
          aLog.logTextnl(getClass().getName() + " table has no columns");
          scrollpane = new JScrollPane(tableRowHeader);
        }
        else scrollpane = new JScrollPane(this);
        scrollpane.setRowHeaderView(tableRowHeader);
        scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, tableRowHeader.getTableHeader());
        tableScrollPane = scrollpane;
        return tableScrollPane;
    }

    // add models event listeners
    protected void configureTableListeners() {
        tableRowHeader.getColumnModel().addColumnModelListener(this);
        sorter.addMouseListenerToHeaderInTable(this); // listener in the TableHeader as the sorter UI.
        getColumnModel().addColumnModelListener(tableRowHeader);
        sorter.addMouseListenerToHeaderInTable(tableRowHeader);
        //sorter.addTableModelListener(this); // removed since automatic with JTable model assignment 10/24/2002
        //sorter.addTableModelListener(tableRowHeader); // removed since automatic with JTable model assignment
        //
        //Enable below code to make columns resized upon list updates between panels aww 03/03
        //however this introduces more overhead since all updates, inserts cause column resizing.
        /*
        sorter.addTableModelListener(new TableModelListener() {
            public void tableChanged(TableModelEvent e) {
              if (e.getType() != TableModelEvent.DELETE) {
                int rowIdx1 = e.getFirstRow();
                if ( rowIdx1 != TableModelEvent.HEADER_ROW) { // ignore header changes, for now
                   if (rowIdx1 == e.getLastRow() ) { // one row changed, could loop over range
                     resetTableCellWidths(modelToSortedRowIndex(rowIdx1));
                   }
                   else reinitializeTableCellWidths(); // do all, guess
                }
              }
            }
        });
        */

        // sync rowheader selection with table cell selection
        ListSelectionModel selectionModel = tableRowHeader.getSelectionModel();
        //selectionModel.addListSelectionListener(new TableRowHeadSelectionListener());
        this.setSelectionModel(selectionModel); // use same selection model for both tables;
        selectionModel.addListSelectionListener(new TableEditListener());
        //selectionModel.addListSelectionListener(new TableViewSelectionListener());
        addMouseListener(new TableViewMouseAdapter());
        tableRowHeader.addMouseListener(new TableRowHeaderMouseAdapter());
        /*
        KeyAdapter ka = new TableKeyAdapter();
        addKeyListener(ka);
        tableRowHeader.addKeyListener(ka);
        */
        configureEditorKeyBindings();
    }
    protected void configureEditorKeyBindings() {
        ActionMap actionMap = new ActionMap();
        actionMap.setParent(getActionMap());
        actionMap.put("tableCancelEditKeyBinding", new TableCancelEditKeyAction());
        actionMap.put("tableStopEditKeyBinding", new TableStopEditKeyAction());
        actionMap.put("tableStartEditKeyBinding", new TableStartEditKeyAction());
        setActionMap(actionMap);
        InputMap inputMap = new InputMap();
        inputMap.setParent(getInputMap());
        inputMap.put(
            KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK),
            "tableCancelEditKeyBinding");
        inputMap.put(
            KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK),
            "tableStopEditKeyBinding");
        inputMap.put(
            KeyStroke.getKeyStroke(KeyEvent.VK_E, Event.CTRL_MASK),
            "tableStartEditKeyBinding");
        setInputMap(JComponent.WHEN_FOCUSED,inputMap);
        setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT,inputMap);
    }
    protected class TableCancelEditKeyAction extends AbstractAction {
        public TableCancelEditKeyAction() {
            super("CancelCellEdit");
            setEnabled(true);
        }
        public void actionPerformed(ActionEvent evt) {
            cancelTableCellEditing();
        }
    }
    protected class TableStopEditKeyAction extends AbstractAction {
        public TableStopEditKeyAction() {
            super("StopCellEdit");
            setEnabled(true);
        }
        public void actionPerformed(ActionEvent evt) {
            stopTableCellEditing();
        }
    }
    protected class TableStartEditKeyAction extends AbstractAction {
        public TableStartEditKeyAction() {
            super("StartCellEdit");
            setEnabled(true);
        }
        public void actionPerformed(ActionEvent evt) {
           int icol = getSelectedColumn();
           int irow = getSelectedRow();
           if (irow > -1 && icol > -1) {
             editCellAt(irow, icol, new TableCellEditEvent(this));
           }
        }
    }
    protected class TableViewMouseAdapter extends MouseAdapter {
        public void mousePressed(MouseEvent evt) {
          int clickCount = evt.getClickCount();
          int mask = evt.getModifiers();
          //Debug.println(getClass().getName()+" TV Clicks:"+clickCount+" TV Edit? "+isEditing()+" Btn: "+mask);
          //also could use the ALT_MASK or CTRL_MASK
          if ( mask == (MouseEvent.BUTTON1_MASK | InputEvent.ALT_MASK) && clickCount >= 1) {
            aLog.logTextnl(" EDIT cancelled for " + getClass().getName());
            editingCanceled(new ChangeEvent(this));
            repaint();
          }
          else if ( (mask == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK)) 
                    && clickCount >= 1) {
            if (getSelectedRowCount() == 1) {
              resetRowColumnSizes(AbstractJasiListTable.this, getSelectedRow());
              revalidate();
              tableScrollPane.revalidate();
            }
          }
        }
    }
    protected class TableRowHeaderMouseAdapter extends MouseAdapter {
        public void mousePressed(MouseEvent evt) {
          //Debug.println(getClass().getName()+" TRH Clicks:"+evt.getClickCount()+" Edit? "+tableRowHeader.isEditing());
          //also could use the ALT_MASK or CTRL_MASK
          if ( (evt.getModifiers() == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK)) 
               && evt.getClickCount() >= 1) {
            if(tableRowHeader.getSelectedRowCount() == 1) {
              resetRowColumnSizes(AbstractJasiListTable.this, getSelectedRow());
              resetRowColumnSizes(tableRowHeader, tableRowHeader.getSelectedRow());
              resetRowHeader(tableRowHeader, tableScrollPane);
              tableScrollPane.revalidate();
            }
          }
          //else aLog.logTextnl("Shift failed" + getClass().getName());
        }
    }

    public class EditorMouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent evt) {
          int clickCount = evt.getClickCount();
          int mask = evt.getModifiers();
          if ( mask == (MouseEvent.BUTTON1_MASK | InputEvent.ALT_MASK) && clickCount >= 1) {
//           Debug.println(EML Canceling edit for " + getClass().getName());
             editingCanceled(new ChangeEvent(this));
          }
          else if ( mask == (MouseEvent.BUTTON1_MASK | InputEvent.CTRL_MASK) && clickCount >= 1) {
//              Debug.println(EML Stopping edit for " + getClass().getName());
                editingStopped(new ChangeEvent(this));
          }
          repaint();
        }
    }
    class TableEditListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
        if (! e.getValueIsAdjusting()) return; // AWW change 8/29/2002
        if (isEditing()) editingStopped(new ChangeEvent(this));
        if (tableRowHeader.isEditing()) tableRowHeader.editingStopped(new ChangeEvent(this));
      }
    }
    /*
    class TableRowHeadSelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
          //System.out.println("rowheader selection: " + e.toString());
      }
    }
    class TableViewSelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
          //System.out.println("tableview selection: " + e.toString());
      }
    }

    class TableKeyAdapter extends KeyAdapter {
      public void keyPressed(KeyEvent evt) {
        System.out.println("press evt.paramString: " + evt.paramString());
        System.out.println("key pressed: " + (evt.getID() == KeyEvent.KEY_PRESSED)); 
      }
      public void keyReleased(KeyEvent evt) {
        System.out.println("reles evt.paramString: " + evt.paramString());
        System.out.println("key released: " + (evt.getID() == KeyEvent.KEY_RELEASED));
      }
      public void keyTyped(KeyEvent evt) {
        System.out.println("typed evt.paramString: " + evt.paramString());
        System.out.println("keyCode: " + (int) evt.getKeyCode());
        System.out.println("keyChar: " + (int) evt.getKeyChar());
        System.out.println("keyText: " +
                        evt.getKeyText(evt.getKeyCode()));
        System.out.println( " key typed: " + (evt.getID() == KeyEvent.KEY_TYPED));
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK);
        System.out.println("ks keyCode: " + (int) ks.getKeyCode());
        System.out.println("ks keyChar: " + (int) ks.getKeyChar());
        ks = KeyStroke.getKeyStrokeForEvent(evt);
        System.out.println("ksevt keyCode: " + (int) ks.getKeyCode());
        System.out.println("ksevt keyChar: " + (int) ks.getKeyChar());
        // evt.getKeyCode() == KeyEvent.VK_TAB) {
      }
    }
    */

    public void reinitializeTableCellWidths() {
        final Runnable doUpdate = new Runnable() {
          public void run() {
            try {
              initColumnSizes(AbstractJasiListTable.this);
              initColumnSizes(tableRowHeader);
              resetRowHeader(tableRowHeader, tableScrollPane);
            }
            catch (Exception ex) {
              System.err.println(ex.toString());
            }
          }
        };
        SwingUtilities.invokeLater(doUpdate);
    }
    public void resetTableCellWidths() {
      resetTableCellWidths(getSelectedRowIndex());
    }
    public void resetTableCellWidths(int index) {
      if (index < 0 || index > getRowCount()) return; 
      resetRowColumnSizes(tableRowHeader, index);
      resetRowHeader(tableRowHeader, tableScrollPane);
      resetRowColumnSizes(AbstractJasiListTable.this, index);
      setPreferredScrollableViewportSize(getPreferredSize()); // is this needed, testadd
      //tableScrollPane.revalidate();
      this.revalidate();
    }

    public JScrollPane createTableScrollPane(int fontSize) {
        if (fontSize > 0) this.fontSize = fontSize;
        return createTableScrollPane();
    }

    public JScrollPane createTableScrollPane(Font font) {
        if (font != null) this.font = font;
        return createTableScrollPane();
    }

    public static String getNumberFormatPattern(int scale) {
      StringBuffer pattern = new StringBuffer("#0");
      if (scale > 0) {
        char [] frac = new char[scale];
        Arrays.fill(frac, '0');
        pattern.append('.');
        pattern.append(frac);
      }
      return pattern.toString();
    }

    public int getColumnHeaderWidth(JTable table, TableColumn column) {
        Component comp = column.getHeaderRenderer().
                  getTableCellRendererComponent(
                      table, column.getHeaderValue(), 
                          true, true, 0, 0);
        return comp.getPreferredSize().width;
    }
    public void initColumnSizes(JTable table) {
        int rowCount = (table == null) ? 0:table.getRowCount();
        if (rowCount <= 0) return; // use last default
        int checkRows; // perhaps would be better to used subset for speed?
        if (rowCount > 200) checkRows = 200;  
        else checkRows = rowCount;
        for (int icol = 0; icol < table.getColumnCount(); icol++) {
          int maxWidth = 0;
          for (int irow = rowCount-checkRows; irow < rowCount; irow++) {
            int cellWidth = getRenderedCellWidth(table, irow, icol);
            if (cellWidth > maxWidth) maxWidth = cellWidth;
          }
          table.getColumnModel().getColumn(icol).setPreferredWidth(maxWidth);
        }
    } 

    public boolean resetRowColumnSizes(JTable table, int irow) {
        if (cellIndexOutOfBounds(table,irow,0)) return false;
        for (int icol = 0; icol < table.getColumnCount(); icol++) {
          TableColumn col = table.getColumnModel().getColumn(icol);
          //col.setPreferredWidth(getRenderedCellWidth(table,irow,icol));
          col.setPreferredWidth(Math.max(col.getPreferredWidth(),getRenderedCellWidth(table,irow,icol)));
        }
        return true;
    } 

    protected boolean cellIndexOutOfBounds(JTable table, int irow, int icol) {
        return (table == null || irow < 0 || irow > table.getRowCount()-1 ||
            icol < 0 || icol > table.getColumnCount()-1) ? true : false;
    }

    public int getRenderedCellWidth(JTable table, int irow, int icol) {
        if (cellIndexOutOfBounds(table, irow, icol)) return -1;
        TableColumn column = table.getColumnModel().getColumn(icol);
        Component comp = table.getCellRenderer(irow,icol).
                getTableCellRendererComponent(
                    table, table.getValueAt(irow,icol),
                    true, true, irow, icol);
        int colWidth = Math.max(getColumnHeaderWidth(table,column), comp.getPreferredSize().width) +
                 DEFAULT_COLUMN_WIDTH_PADDING  +
                 tableColumnCellWidthPadding[table.convertColumnIndexToModel(icol)];
        int maxColumnWidth =
          (((String) column.getHeaderValue()).equalsIgnoreCase("RMK")) ? MAX_RMK_COLUMN_WIDTH : MAX_COLUMN_WIDTH; 
        return Math.min(maxColumnWidth, colWidth);
    } 

    public int getPreferredRowWidth(JTable table, int irow) {
        if (cellIndexOutOfBounds(table, irow, 0)) return -1;
        int rowWidth = 0;
        for (int icol = 0; icol < table.getColumnCount(); icol++) {
          rowWidth += getRenderedCellWidth(table, irow, icol);
        }
        return rowWidth;
    }

    public boolean resetRowHeader(JTable table, JScrollPane scrollpane) {
        if (table == null || scrollpane == null) return false;
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        scrollpane.setRowHeaderView(table);
        scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, table.getTableHeader());
        return true;
    }

    public int updateDB(int rowIndex) {
      int modelIndex = sortedToModelRowIndex(rowIndex);
      return (modelIndex >= 0) ?
        tableModel.updateDB(modelIndex) : modelIndex;
    }

    public int updateDB() {
        return tableModel.updateDB();
    }

    public int getSelectionMode(String modeStr) {
      if (modeStr.equalsIgnoreCase("single")) {
        return ListSelectionModel.SINGLE_SELECTION;
      }
      else if (modeStr.equalsIgnoreCase("single_interval")) {
        return ListSelectionModel.SINGLE_INTERVAL_SELECTION;
      }
      else if (modeStr.equalsIgnoreCase("multiple_interval")) {
        return ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
      }
      System.err.println(getClass().getName() + " unknown selection mode: " + modeStr);
      return -1;
    }

    public void setSelectionMode(String modeStr) {
      if (modeStr.equalsIgnoreCase("single")) {
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
      }
      else if (modeStr.equalsIgnoreCase("single_interval")) {
        setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION );
      }
      else if (modeStr.equalsIgnoreCase("multiple_interval")) {
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
      }
      else System.err.println(getClass().getName() +
                 " unknown selection mode: " + modeStr);
    }
    public void setSelectionMode(int mode) {
        if (mode == ListSelectionModel.SINGLE_SELECTION || 
            mode == ListSelectionModel.SINGLE_INTERVAL_SELECTION ||
            mode == ListSelectionModel.MULTIPLE_INTERVAL_SELECTION) {
                if (tableRowHeader != null) tableRowHeader.setSelectionMode(mode);
                super.setSelectionMode(mode);
        }
        else System.err.println(getClass().getName() +
                 " unknown selection mode: " + mode);
    }
    protected void setTableAttributes(JTable aTable) {
        aTable.setRowHeight(MINIMUM_ROW_HEIGHT);
        aTable.setFont(font);
        aTable.setShowGrid(true);
        //aTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); // used to be default multiple_interval
        //Changed below to use value set by property, else instance default. - aww 02/09/2005
        aTable.setSelectionMode(getSelectionMode(selectionMode)); // use property setting
        aTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF );
        aTable.setCellSelectionEnabled(true); //? used to be false - test aww ;
        aTable.setRowSelectionAllowed(true);
        aTable.setColumnSelectionAllowed(false);
        aTable.setForeground(Color.black);
        aTable.setBackground(new Color(1.f,1.f,0.7f));
        aTable.setSelectionForeground(Color.red);
        aTable.setSelectionBackground(Color.white);
        aTable.setRowHeight(MINIMUM_ROW_HEIGHT);
        CalendarDateRenderer cdRenderer = new CalendarDateRenderer(); // need here leap sec aware DateTime class 2008/02/7 -aww
        cdRenderer.setHorizontalAlignment(JLabel.LEFT);
        aTable.setDefaultRenderer(java.util.Date.class, cdRenderer); // need here leap sec aware DateTime class 2008/02/7 -aww
        configureTableColumnHeader(aTable);
    }
    protected void configureTableColumnAttributes() {
        String pattern = null;                // format pattern buffer
        //TableColumnModel tcm = getColumnModel();
        defaultColumnModel = getColumnModel();
        for (int i = 0; i<getColumnCount(); i++) {
          TableColumn tc = defaultColumnModel.getColumn(i);
          tc.setHeaderRenderer(new TableHeaderRenderer());
          tcer.tcRenderer = tc.getCellRenderer();
          tcer.tcEditor = tc.getCellEditor();
          String colName =  (String) tc.getHeaderValue();
          Class aClass = getColumnClass(i);
          int indexViewToModel = convertColumnIndexToModel(i);
           //Debug.println(getClass().getName()+" idx:"+i+" idxModel:"+indexViewToModel+" class: "+aClass.getName());
          if (! configureColEditorRendererByName(tcer, indexViewToModel, colName))
             configureColEditorRendererByClass(tcer, indexViewToModel, aClass);
          if (tcer.tcRenderer instanceof ColorableRendererIF &&
             ! tableModel.isKeyColumn(indexViewToModel) )
                ((ColorableRendererIF) tcer.tcRenderer).setDefaultBackground(tableModel.isCellEditable(0,indexViewToModel) ?
                        (tableColumnNullable[indexViewToModel] ? Color.green:Color.cyan) :Color.pink);
          if (tcer.tcEditor instanceof AbstractCellEditor)
            ((AbstractCellEditor) tcer.tcEditor).getComponent().addMouseListener(new EditorMouseListener());
          if (tcer.tcEditor instanceof NullableEditorIF)
              ((NullableEditorIF)tcer.tcEditor).setNullAllowed(tableColumnNullable[indexViewToModel]); 
          tc.setCellEditor(tcer.tcEditor);
          tc.setCellRenderer(tcer.tcRenderer);
        }
        initColumnSizes(this);
    }

    protected void configureTableRowHeader(JTable aTable) {
        tableRowHeader = new JTable(sorter); 
        if (headerClipboard == null) headerClipboard = new JTableClipboardPopupMenu(); 
        headerClipboard.setComponent(tableRowHeader);
        createTableRowHeaderColumnModel(aTable);
        setTableAttributes(tableRowHeader);
        tableRowHeader.setForeground(Color.black);
        tableRowHeader.setBackground(Color.yellow);
        tableRowHeader.setSelectionForeground(Color.blue);
        tableRowHeader.setSelectionBackground(Color.orange);
        //tableRowHeader.setToolTipText("Keys highlighted yellow (shift-click over cell resizes rowheader width)");
        //initColumnSizes(tableRowHeader);

    }

    protected void createTableRowHeaderColumnModel(JTable aTable) {
        TableColumnModel tcm = aTable.getColumnModel();
        TableColumnModel tcm2 = new DefaultTableColumnModel();
        int nkeyCols = 0;
        TableColumn col = null;
        int maxCol = tcm.getColumnCount();
        for (int i = 0; i < maxCol; i++) {
            col = tcm.getColumn(i);
            int idxModel = aTable.convertColumnIndexToModel(i);
            if (tableModel.isKeyColumn(idxModel)) {
                //Debug.println(getClass().getName() + " tcm2 key col: " + tableModel.getColumnName(idxModel));
                tcm.removeColumn(col);
                tcm2.addColumn(col);
                keyColumnList.add(col);
                nkeyCols++;
                maxCol--;
            }
        }
        tableRowHeader.setColumnModel(tcm2);
    }
    protected void removeHiddenColumnsFromTable(JTable aTable) {
        TableColumnModel tcm = aTable.getColumnModel();
        TableColumn col = null;
        int maxCol = tcm.getColumnCount();
        for (int i = 0; i < maxCol; i++) {
            col = tcm.getColumn(i);
            int idxModel = aTable.convertColumnIndexToModel(i);
            if (tableModel.isColumnHidden(idxModel)) {
                //Debug.print(getClass().getName()+" hidden column: "+tableModel.getColumnName(idxModel));
                //Debug.println(" table col idx: "+i+" modelIndex: "+idxModel);
                tcm.removeColumn(col);
                maxCol--; i--;  // note need to decrement both, else out of sync
                hiddenColumnList.add(col);
            }
        }
    }

    protected void configureTableColumnHeader(JTable aTable) {
        aTable.getTableHeader().setForeground(Color.red);
        aTable.getTableHeader().setBackground(new Color(.85f,.85f,.85f));
        aTable.getTableHeader().setToolTipText("dbl-click sort descends; shift-dbl-click sort ascends");
        aTable.getTableHeader().setFont(font);
    }

    public TextLogger getTextLogger() {
        return aLog;
    }
    public void setTextLogger(TextLogger logger) {
        if (logger != null) aLog = logger;
        if (tableModel != null) tableModel.setTextLogger(logger);
    }

    /* Implement 1.4 Preferences API, to replace prop file?
    protected JasiPreferences userPrefs = 
            JasiPreferences.userNodeForPackage(this);
    protected JasiPreferences sysPrefs =
            JasiPreferences.systemNodeForPackage(this);
    public getPreferences() {
        if (sysPrefs.getBoolean("debug", false)) { Debug.enable(); }

        selectionMode = userPrefs.getString("selection", sysPrefs.getString("selection", DEFAULT_SELECTION_MODE)); 
        setSelectionMode(selectionMode);

        tableColumnShown = userPrefs.getBooleanArray("show",sysPrefs.getBooleanArray("show", null));
        if (tableColumnShown == null || tableColumnShown.length == 0)
           tableColumnShown = tableModel.getTableColumnShown();
        else tableModel.setTableColumnShown(tableColumnShown);

        tableColumnEditable = userPrefs.getBooleanArray("editable",sysPrefs.getBooleanArray("editable"), null));
        if (tableColumnEditable == null || tableColumnEditable.length == 0)
          tableColumnEditable = tableModel.getTableColumnEditable();
        else tableModel.setTableColumnEditable(tableColumnEditable);

        tableColumnNullable = userPrefs.getBooleanArray("nullable",sysPrefs.getBooleanArray("nullable"), null));
        if (tableColumnNullable == null || tableColumnNullable.length == 0)
          tableColumnNullable = tableModel.getTableColumnNullable();
        else tableModel.setTableColumnNullable(tableColumnNullable);

        tableColumnCellWidthPadding = userPrefs.getIntArray("padding",sysPrefs.getIntArray("padding"), null));
        if (tableColumnCellWidthPadding == null || tableColumnCellWidthPadding.length == 0) 
          tableColumnCellWidthPadding = tableModel.getTableColumnCellWidthPadding();
        else tableModel.setTableColumnCellWidthPadding(tableColumnCellWidthPadding);

        tableColumnFractionDigits = userPrefs.getIntArray("fraction",sysPrefs.getIntArray("fraction"), null));
        if (tableColumnFractionDigits == null || tableColumnFractionDigits.length == 0) 
          tableColumnFractionDigits = tableModel.getTableColumnFractionDigits();
        else tableModel.setTableColumnFractionDigits(tableColumnFractionDigits);

        // order table not model property
        tableColumnNameOrder = userPrefs.getStringArray("order",sysPrefs.getStringArray("order"), null));
        if (tableColumnNameOrder == null || tableColumnNameOrder.length == 0)
          tableColumnNameOrder = tableModel.getTableColumnNames();
    }

    protected void savePreferences() {
        StringBuffer showStr = new StringBuffer(256);
        StringBuffer nullableStr = new StringBuffer(256);
        StringBuffer editableStr = new StringBuffer(256);
        StringBuffer paddingStr = new StringBuffer(256);
        StringBuffer fracStr = new StringBuffer(256);
        StringBuffer orderStr = new StringBuffer(256);
        int ncol = tableModel.getColumnCount();  
        for (int index = 0; index < ncol; index++) {
          showStr.append(tableColumnShown[index]).append(" ");
          nullableStr.append(tableColumnNullable[index]).append(" ");
          editableStr.append(tableColumnEditable[index]).append(" ");
          paddingStr.append(tableColumnCellWidthPadding[index]).append(" ");
          fracStr.append(tableColumnFractionDigits[index]).append(" ");
        }
        ncol = tableColumnNameOrder.length;
        for (int index = 0; index < ncol; index++) {
          //if ( tableColumnShown[tableModel.findColumn(tableColumnNameOrder[index]) ])
          orderStr.append(tableColumnNameOrder[index]).append(" ");
        }
        userPrefs.put("show",showStr.toString());
        userPrefs.put("editable",editableStr.toString());
        userPrefs.put("nullable",nullableStr.toString());
        userPrefs.put("padding",paddingStr.toString());
        userPrefs.put("fraction",fracStr.toString());
        userPrefs.put("order",orderStr.toString());
        userPrefs.put("selection",selectionMode);
        userPrefs.put("debug",Debug.isEnabled());
        userPrefs.flush();
        aLog.logTextnl("Saved " + userPrefs.toString());
    }
    */

    protected void setPropertyFileName(String name) {
        propFileName = name;
    }
    protected void loadTableProperties(String name) {
        setPropertyFileName(name);
        loadTableProperties();
    }
    protected void loadTableProperties() {
        inputProperties = new JasiPropertyList();
        inputProperties.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
        inputProperties.setFilename(propFileName);
        inputProperties.reset();
        inputProperties.setup(); // force reset of local Environment netcode ? 11/05/2004
        //System.out.println("DEBUG AJLT input prop user file: "+inputProperties.getUserPropertiesFileName());
        //System.out.println("DEBUG AJLT input prop default file: "+inputProperties.getDefaultPropertiesFileName());
        String tmpStr = inputProperties.getProperty("debug");
        if (tmpStr != null && Boolean.valueOf(tmpStr).booleanValue()) {
          Debug.enable();
          System.out.println(Lines.STAR_TEXT_LINE);
          System.out.println("Loading properties for table "+getClass().getName() + " from file : " + propFileName);
          System.out.print(inputProperties.listToString());
          System.out.println(Lines.STAR_TEXT_LINE);
        }
        selectionMode = inputProperties.getProperty("selection", DEFAULT_SELECTION_MODE);
        setSelectionMode(selectionMode);  // prop set instance member -aww 02/09/2005

        tableColumnShown = inputProperties.getBooleanArray("show");
        if (tableColumnShown == null || tableColumnShown.length == 0) {
           tableColumnShown = tableModel.getTableColumnShown();
        }
        else tableModel.setTableColumnShown(tableColumnShown);

        tableColumnEditable = inputProperties.getBooleanArray("editable");
        if (tableColumnEditable == null || tableColumnEditable.length == 0)
          tableColumnEditable = tableModel.getTableColumnEditable();
        else tableModel.setTableColumnEditable(tableColumnEditable);

        tableColumnNullable = inputProperties.getBooleanArray("nullable");
        if (tableColumnNullable == null || tableColumnNullable.length == 0)
          tableColumnNullable = tableModel.getTableColumnNullable();
        else tableModel.setTableColumnNullable(tableColumnNullable);

        tableColumnCellWidthPadding = inputProperties.getIntArray("padding");
        if (tableColumnCellWidthPadding == null || tableColumnCellWidthPadding.length == 0) 
          tableColumnCellWidthPadding = tableModel.getTableColumnCellWidthPadding();
        else tableModel.setTableColumnCellWidthPadding(tableColumnCellWidthPadding);
        tableColumnFractionDigits = inputProperties.getIntArray("fraction");
        if (tableColumnFractionDigits == null || tableColumnFractionDigits.length == 0) 
          tableColumnFractionDigits = tableModel.getTableColumnFractionDigits();
        else tableModel.setTableColumnFractionDigits(tableColumnFractionDigits);

        // order not model property, defaults to model names if no property
        tableColumnNameOrder = inputProperties.getStringArray("order");
        if (tableColumnNameOrder == null || tableColumnNameOrder.length == 0)
          tableColumnNameOrder = tableModel.getTableColumnNames();

        // pass input properties to TableModel
        if (tableModel != null) tableModel.setProperties(inputProperties);

    }

    protected void listTableInputProperties() {
      inputProperties.list(System.out);
    }

    protected void saveTableProperties() {
        if (propFileName == null) {
          aLog.logTextnl("No property file set for : " + getClass().getName());
          return;
        }
        JasiPropertyList props = (inputProperties == null) ?
            new JasiPropertyList() : new JasiPropertyList(inputProperties);
        props.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
        props.setFilename(propFileName);
        System.out.println(props.getDefaultFilePath());

        StringBuffer showStr = new StringBuffer(256);
        StringBuffer nullableStr = new StringBuffer(256);
        StringBuffer editableStr = new StringBuffer(256);
        StringBuffer paddingStr = new StringBuffer(256);
        StringBuffer fracStr = new StringBuffer(256);
        StringBuffer orderStr = new StringBuffer(256);
        int ncol = tableModel.getColumnCount();  
        for (int index = 0; index < ncol; index++) {
          showStr.append(tableColumnShown[index]).append(" ");
          nullableStr.append(tableColumnNullable[index]).append(" ");
          editableStr.append(tableColumnEditable[index]).append(" ");
          paddingStr.append(tableColumnCellWidthPadding[index]).append(" ");
          fracStr.append(tableColumnFractionDigits[index]).append(" ");
        }
        String [] headers = getTableColumnHeaders();
        ncol = headers.length; //ncol=tableColumnNameOrder.length;
        for (int index = 0; index < ncol; index++) {
          //if ( tableColumnShown[tableModel.findColumn(tableColumnNameOrder[index]) ])
          //orderStr.append(tableColumnNameOrder[index]).append(" ");
          orderStr.append(headers[index]).append(" ");
          
        }
        props.setProperty("show",showStr.toString());
        props.setProperty("editable",editableStr.toString());
        props.setProperty("nullable",nullableStr.toString());
        props.setProperty("padding",paddingStr.toString());
        props.setProperty("fraction",fracStr.toString());
        props.setProperty("order",orderStr.toString());

        props.setProperty("selection",selectionMode);

        String tmp = props.getProperty("defaultDataSrc");
        props.setProperty("defaultDataSrc", (tmp == null) ? "mung": tmp);   // text parser lookup prop

        tmp = props.getProperty("defaultDataAuth");
        props.setProperty("defaultDataAuth", (tmp == null) ? EnvironmentInfo.getNetworkCode() : tmp);   // text parser lookup prop

        tmp = props.getProperty("logModelChanges");// AbstractJasiListPanel prop
        props.setProperty("logModelChanges", (tmp == null) ? "false" : tmp);// AbstractJasiListPanel prop

        tmp = props.getProperty("inputParserName");// AbstractJasiListPanel prop
        props.setProperty("inputParserName", (tmp == null) ? null : tmp);// AbstractJasiListPanel prop

        props.saveProperties();
        aLog.logTextnl("property file : " + props.getUserPropertiesFileName());
    }

    // implementation of dialog checkbox/toolbar menu item
    // columns matching names map to indexes in model
    // hidden array vs. current model view, toggle them in the tcm.
    protected ArrayList getHiddenColumns() {return hiddenColumnList;}
    protected void addTableColumn(TableColumn col, int colIndex) {
        addColumn(col); // append new column to end first
        if (hiddenColumnList.contains(col)) hiddenColumnList.remove(col);
        tableColumnShown[col.getModelIndex()] = true;
        //if ( colIndex > -1 && colIndex < getColumnCount() ) moveColumn(getColumnCount()-1, colIndex); // no, model colIndex does not map to actual view index
        reconfigureTableColumnOrder();
    }

    private void reconfigureTableColumnOrder() {
        TableColumnModel tcm = configureColumnOrder(false);
        for (int idx = 0; idx < hiddenColumnList.size(); idx++) {
          tcm.removeColumn((TableColumn) hiddenColumnList.get(idx));
        }
        for (int idx = 0; idx < keyColumnList.size(); idx++) {
          tcm.removeColumn((TableColumn) keyColumnList.get(idx));
        }
        /* replaced this code with above keyColumnList
        int maxCol = defaultColumnModel.getColumnCount();
        for (int idx = 0; idx < maxCol; idx++) {
          if (tableModel.isKeyColumn(idx)) {
            tcm.removeColumn(
              tcm.getColumn(
                 tcm.getColumnIndex(tableModel.getColumnName(idx))
              )
            );
          }
        }
        */
        setColumnModel(tcm);
    }
    protected void addTableColumn(TableColumn col) {
        addTableColumn(col, -1);
    }
    protected void removeTableColumn(int idx) {
        TableColumnModel model = getColumnModel();
        int maxIdx = model.getColumnCount();
        if (idx > -1 && idx < maxIdx) {
            removeColumn(model.getColumn(idx));
            tableColumnShown[convertColumnIndexToModel(idx)] = false;
        }
    }
    protected void removeTableColumn(TableColumn col) {
        removeColumn(col);
        // no convertColumnIndexToModel see remove above?
        tableColumnShown[col.getModelIndex()] = false;
        if (! hiddenColumnList.contains(col)) hiddenColumnList.add(col);
    }
    protected void removeSelectedTableColumn() {
        int idx = getSelectedColumn();
        if (idx > -1) {
            TableColumn col = getColumnModel().getColumn(idx);
            removeColumn(col);
            // no convertColumnIndexToModel see remove above?
            tableColumnShown[col.getModelIndex()] = false;
        }
    }
    public boolean addOrReplaceRow(JasiCommitableIF aRow) {
        return tableModel.addOrReplaceRow(aRow);
    }
    public boolean deleteRow(int rowIndex) {
        int modelIndex = sortedToModelRowIndex(rowIndex);
        if (modelIndex == -1) return false;
        boolean retVal = tableModel.deleteRow(modelIndex);
        if (retVal) {
          clearSelection();
          //resetTableCellWidths(); why reset here? , removed 03/03 aww
        }
        return retVal;
    }
    public boolean copyRow(int rowIndex) {
        return copyRow(rowIndex, true);
    }
    public boolean copyRow(int rowIndex, boolean after) {
        int modelIndex = sortedToModelRowIndex(rowIndex);
        if (modelIndex == -1) return false;
        boolean retVal = false;
        retVal = tableModel.copyRowAt(modelIndex, after);
        if (after) modelIndex++;
        int newRowIndex = modelToSortedRowIndex(modelIndex);
        setRowSelectionInterval(newRowIndex, newRowIndex);
        resetScrollBarPosition(newRowIndex);
        return retVal;
    }
    public boolean appendRow(int rowIndex, JasiCommitableIF aRow) {
        return insertRow(rowIndex, aRow, true);
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF aRow) {
        return insertRow(rowIndex, aRow, false);
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF aRow, boolean after) {
        int modelIndex = 0;
        int rowCount = getRowCount();
        if (rowCount == 0 && rowIndex > 0) return false; 
        if (rowCount > 0) {
          modelIndex = sortedToModelRowIndex(rowIndex);
          if (modelIndex == -1) return false;  
          modelIndex = (after) ? modelIndex+1 : modelIndex;
        }
        boolean retVal = false;
        retVal = tableModel.insertRow(modelIndex, aRow);
        if (retVal) {
        // could create update thread here to defer table selection update
          //final int idx = modelIndex;
          //final Runnable doUpdate = new Runnable() {
          //  public void run() {
          //    try {
                selectModelRow(modelIndex);
          //    }
          //    catch (Exception ex) {
          //      System.err.println(ex.toString());
          //    }
          //  }
          //};
          //SwingUtilities.invokeLater(doUpdate);
        }
        return retVal;
    }
    protected void selectModelRow(int modelRowIndex) {
      int newRowIndex = modelToSortedRowIndex(modelRowIndex);
      //System.out.println("DEBUG AJLT modelIdx: "+modelRowIndex+" table idx: "+newRowIndex);
      if (newRowIndex > -1 && newRowIndex < getRowCount()) {
        setRowSelectionInterval(newRowIndex, newRowIndex);
        resetScrollBarPosition(newRowIndex);
      }
    }
    protected void resetScrollBarPosition(int rowIndex) {
      if (tableScrollPane != null) {
        resetTableCellWidths();
        JScrollBar jsb = tableScrollPane.getVerticalScrollBar();
        int newValue = getRowHeight()*(rowIndex-1);
        int max = jsb.getMaximum();
        if (max-newValue <= jsb.getModel().getExtent()) {
          jsb.setValue(max); // doesn't work thus kludge:
            Rectangle r = new Rectangle();
            computeVisibleRect(r);
            r.translate(0,getRowHeight());
            scrollRectToVisible(r);
        }
        else jsb.setValue(newValue);
      }
    }
 }

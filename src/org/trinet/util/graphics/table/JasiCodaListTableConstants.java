package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/** Define constants used by a CodaListTableModel. These are the columns in a
 *  JTable that contains a CodaList. */
public interface JasiCodaListTableConstants {
    public static final int MAX_FIELDS   = 42;
    public static final int NET          = 0;
    public static final int STA          = 1;
    public static final int CHN          = 2;
    public static final int AU           = 3;
    public static final int SUBSRC       = 4;
    public static final int CHNSRC       = 5;
    public static final int SEED         = 6;
    public static final int LOC          = 7;
    public static final int PHASE        = 8;
    public static final int QUALITY      = 9;
    public static final int DATETIME     = 10;
    public static final int RESIDUAL     = 11;
    public static final int DISTANCE     = 12;
    public static final int AZIMUTH      = 13;
    public static final int WEIGHT       = 14;
    public static final int AUTH         = 15;
    public static final int SOURCE       = 16;
    public static final int DESCRIPTION  = 17;
    public static final int PROCESSING   = 18;
    public static final int DELETE       = 19;
    public static final int UNITS        = 20;
    public static final int TAU          = 21;
    public static final int AFIX         = 22;
    public static final int AFREE        = 23;
    public static final int QFIX         = 24;
    public static final int QFREE        = 25;
    public static final int ALGORITHM    = 26;
    public static final int WSIZE        = 27;
    public static final int WCOUNT       = 28;
    public static final int WTIME        = 29;
    public static final int AMP          = 30;
    public static final int ERR          = 31;
    public static final int RMS          = 32;
    public static final int CMAG   = 33;
    public static final int MCORR  = 34;
    public static final int MAG    = 35;
    public static final int TYPE   = 36;
    public static final int MAGID  = 37;
    public static final int ORID   = 38;
    public static final int COID   = 39;
    public static final int EVID   = 40;
    public static final int IN_WGT   = 41;

// End of member identifier constants
    public static final Class [] columnClasses = {
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataString.class, DataString.class, DataString.class, DataString.class, Boolean.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataString.class, DataDouble.class, DataLong.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataString.class, DataLong.class, DataLong.class, DataLong.class,
        DataLong.class, DataLong.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 2,
        2, 2, 2, 2, 2,
        0, 0, 0, 0, 0,
        2, 2, 2, 2, 2,
        2, 0, 2, 0, 2,
        2, 2, 2, 2, 2,
        2, 0, 0, 0, 0,
        0, 0
    };

    public static final String [] columnNames = {
    "NET","STA","CHAN","AU","SSRC",
    "CSRC","SEED","LOC","PH","Q",
    "DATETIME","RES","DIST","AZ","WT",
    "AUTH","SRC","DESC","PFLG","DFLG",
    "UNITS","TAU","AFIX","AFREE","QFIX",
    "QFREE","ALGO","WSIZE","WCOUNT","WTIME",
    "AMP","ERR","RMS","CMAG","MCORR",
    "MAG","TYPE","MAGID","ORID", "COID",
    "EVID","IWT"
    };

    public static final boolean [] columnAKey = {
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, true,
        false, false
    };

    public static final boolean [] columnEditable = {
        true, true, true, true, true,
        true, true, true, true, false,
        true, false, false, false, false,
        true, true, true, true, true,
        true, true, false, false, false,
        false, true, false, false, false,
        false, false, false, false, false,
        false, true, false, false, false,
        false, true
    };

    public static final boolean [] columnNullable = {
        false, false, false, false, false,
        false, false, false, false, true,
        false, true, true, true, true,
        false, false, true, false, false,
        false, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, false, true, true, false,
        true, true
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true
    };

    //pixels to add for combo editors etc.
    public static final int [] cellWidthPadding = {
        0, 0, 0, 0, 0,
        0, 0, 0, 10, 0,
        0, 0, 0, 0, 0,
        0, 0, 20, 0, 0,
        30, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 10
    };
}

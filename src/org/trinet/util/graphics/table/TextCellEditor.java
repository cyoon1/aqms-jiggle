package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import org.trinet.util.graphics.text.*;

public class TextCellEditor extends AbstractCellEditor {
  protected int fontSize=12;
  //protected Font font;
  protected JTextField component;
  private JTextClipboardPopupMenu clipboard;

  public TextCellEditor() { 
    this(-1, null);
  }
  public TextCellEditor(int fontSize) { 
    this(fontSize, null);
  }
  public TextCellEditor(Font font) { 
    this(-1, font);
  }
  protected TextCellEditor(int fontSize, Font font) { 
    super();
    component = new JTextField();
    if (fontSize > 0) this.fontSize = fontSize;
    if (font == null) 
        component.setFont(new Font("Monospaced", Font.PLAIN, this.fontSize));
    else {
        if (font.getSize() != this.fontSize) component.setFont(font.deriveFont(this.fontSize));
        else component.setFont(font);
    }
    component.setHorizontalAlignment(JTextField.LEFT);
    component.addActionListener(this);
    // aww 6/04 added focus listener for selecting text:
    component.addFocusListener(
        new FocusListener() {
            public void focusGained(FocusEvent evt) {
                ((JTextComponent) evt.getSource()).selectAll();
//              System.out.println("focusGained evt.toString: "+evt.paramString()+" "+evt.getSource().getClass().getName());
            }
            public void focusLost(FocusEvent evt) {
//              System.out.println("focusLost evt.toString: "+evt.paramString()+" "+evt.getSource().getClass().getName());
            }
        }
    );
    if (clipboard == null) clipboard = new JTextClipboardPopupMenu(component);
  }
  
  public int getFontSize() {
    return component.getFont().getSize();
  }
  public void setFontSize(int fontSize) {
    if (fontSize > 0 && this.fontSize != fontSize) {
      this.fontSize = fontSize;
      component.setFont(component.getFont().deriveFont((float)fontSize));
    }
  }
  public void setFont(Font font) {
    if (font != null) 
        if (font.getSize() != this.fontSize) component.setFont(font.deriveFont(this.fontSize));
        else component.setFont(font);
    }

//Override AbstractCellEditor methods
  public boolean startCellEditing(EventObject anEvent) {
    //if(anEvent == null) component.requestFocus(); 
    if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    component.requestFocus(); 
    return true;
  }

  public Component getComponent() {
    return component;
  }

// Methods below should be overridden in decendent classes 
  public void setCellEditorValue(Object value) {
    if (value != null)  component.setText(value.toString());
    else  component.setText("");
    this.value = value;
  }

  public Object getCellEditorValue() {
    return component.getText();
  }

  public Component getTableCellEditorComponent( JTable table, Object value,
        boolean isSelected, int row, int column) {
    setCellEditorValue(value);
    return component;
  }
}

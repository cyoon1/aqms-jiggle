package org.trinet.util.graphics.table;
import java.awt.Font;
import java.beans.*;
import java.text.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
public abstract class AbstractJasiSolutionAssocPanel extends AbstractJasiListPanel {
    protected Solution aSol;

    protected AbstractJasiSolutionAssocPanel() {this(null,false);}

    protected AbstractJasiSolutionAssocPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
    }

    protected boolean initParser() {
      boolean status = super.initParser();
      if (status) {
        //System.out.println("Time: " + LeapSeconds.trueToString(aSol.getTime(), "yyyy-MM-dd:HH:mm:00.00"));
        inputParser.parseDateTime(
          LeapSeconds.trueToString(aSol.getTime(), "yyyy-MM-dd:HH:mm:00.00") // changed for UTC 2008/02/07 -aww
        );
      }
      return status;
    }
    public int commit() {
      //do not return super.commit();
      return updateList(); // just update list, TN commit() at solution level
    }

    protected void setAssocSolution(Solution sol) {
        aSol = sol;
        AbstractJasiAssociationTableModel model = (AbstractJasiAssociationTableModel) getModel();
        model.setAssocSolution(sol);
        setList(sol);
    }
    public Solution getAssocSolution() { return aSol;}

    protected boolean isStale() {
      return (aSol == null) ? false : aSol.isStale();
    }
    protected void setStale(boolean tf) {
      if (aSol != null) aSol.setStale(tf);
    }

/** Sets the event data list and creates new table.  */
    protected abstract void setList(Solution sol);

    protected void stripRows() {
      if (jopStrip == null) jopStrip = new StripReadingOptionPane();
      jopStrip.presentDialog();
      String choiceStr = jopStrip.getSelectionString();
      int count = 0;
      if (choiceStr.equals("KM") || choiceStr.equals("BOTH")) {
        double distCut = ((StripReadingOptionPane)jopStrip).getDistanceCutoffValue();
      // need strip methods to return counts for decision to setStale
      count += 
        ((JasiListTableModelAssocSolIF) getModel()).stripByDistance(distCut);  //JasiReadingModel
        aLog.logText("distCutoff: "+distCut+" ");
      }
      if (choiceStr.equals("RMS") || choiceStr.equals("BOTH")) {
        double rmsCut = jopStrip.getRMSCutoffValue();
        aLog.logText("rmsCutoff: " +rmsCut+" ");
      // need strip methods to return counts for decision to setStale
      count +=
        ((JasiListTableModelAssocSolIF) getModel()).stripByResidual(rmsCut);  //JasiReadingModel
      }
      if (count > 0) {
        if (panelName.equals("Phase")) setStale(true); // redundant, if done by strip method
        else aSol.setNeedsCommit(true); // no location dependent data
      }
      aLog.logTextnl(panelName + ": stripped rows: " + count);
    }

// JasiSolutionAssocPanel inner classes
    protected class StripReadingOptionPane extends StripOptionPane {
        NumberTextFieldFixed numberField;
        public  StripReadingOptionPane () {
            super();
        }
        protected Object [] createInputs() {
            createRMSComboBox();
            createDistanceNumberField();
            return new Object [] {
              new JLabel("Enter distance cutoff"), numberField,
              new JLabel("Enter rms cutoff"), jcb
            };
        }
        protected void createDistanceNumberField() {
            DecimalFormat df = null;
            NumberFormat nf = NumberFormat.getNumberInstance();
            if (nf instanceof DecimalFormat) {
              df = (DecimalFormat) nf;
              df.setGroupingUsed(false);
              df.setDecimalSeparatorAlwaysShown(false);
              df.applyPattern("#0.");
            }
            numberField = new NumberTextFieldFixed(999., 6, df);
            numberField.setHorizontalAlignment(NumberTextFieldFixed.RIGHT);
            numberField.setFont(new Font("Monospaced", Font.PLAIN, 12));
        }
        protected void createOptions() {
           super.setOptions(new String [] {"KM","RMS","BOTH","CANCEL"});
           super.setInitialValue("CANCEL");
        }
        public double getDistanceCutoffValue() {
            return numberField.getDoubleValue();
        }
    }
// enable listener logic here since stateChange listener on inputList doesn't discriminate
/*
    public void propertyChange(PropertyChangeEvent e) {
        debugProperty(e);
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
        // model list reset needed if model list not same as input list
        // change AbstractJasiListPanel regarding inputList remake of table 
          Object newValue = e.getNewValue();
          if (newValue instanceof Solution) {
            if ((Solution) newValue == aSol) setList(aSol); // or informModel(); // just update fields  
          }
          else if (newValue instanceof Magnitude) {
            if ( ((Magnitude) newValue).getAssociatedSolution() == aSol) {
                    setList(aSol);
            }
          }
        }
    }
*/
} // end of AbstractJasiSolutionAssocPanel class

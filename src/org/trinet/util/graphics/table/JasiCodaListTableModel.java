// What is the coupling between units and codatype???
package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.coda.TN.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Model represents a list of jasi Coda objects (CodaList) in a JTable. */
public class JasiCodaListTableModel extends AbstractJasiMagAssocTableModel
    implements JasiCodaListTableConstants {

    public JasiCodaListTableModel() {
        super();
        maxFields = JasiCodaListTableConstants.MAX_FIELDS;
        deleteColumnIndex = JasiCodaListTableConstants.DELETE;
        tableColumnNames = JasiCodaListTableConstants.columnNames; 
        tableColumnClasses = JasiCodaListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiCodaListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiCodaListTableConstants.columnAKey;
        tableColumnEditable = JasiCodaListTableConstants.columnEditable;
        tableColumnNullable = JasiCodaListTableConstants.columnNullable;
        tableColumnShown = JasiCodaListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiCodaListTableConstants.cellWidthPadding;
    }

    public JasiCodaListTableModel(CodaList codaList) {
        this(codaList, false);
    }

    public JasiCodaListTableModel(Solution sol, boolean isCellEditable) {
        this(sol.getCodaList(), isCellEditable);
    }

    public JasiCodaListTableModel(CodaList codaList, boolean isCellEditable) {
        this();
        setList(codaList);
        setCellEditable(isCellEditable);
    }

    protected Coda getCoda(int rowIndex) {
        return (Coda) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Coda coda = getCoda(rowIndex);
        switch (colIndex) {
          case NET:
            return new DataString(coda.getChannelObj().getNet());
          case STA:
            return new DataString(coda.getChannelObj().getSta());
          case CHN:
            return new DataString(coda.getChannelObj().getChannel());
          case AU:
            return new DataString(coda.getChannelObj().getAuth());
          case SUBSRC:
            return new DataString(coda.getChannelObj().getSubsource());
          case CHNSRC:
            return new DataString(coda.getChannelObj().getChannelsrc());
          case SEED:
            return new DataString(coda.getChannelObj().getSeedchan());
          case LOC:
            return new DataString(coda.getChannelObj().getLocation());
          case PHASE:
            return new DataString(coda.descriptor.getCodaTypeName());
          case QUALITY:
            return (coda.quality.isNull()) ? NULL_DATADOUBLE : coda.quality; 
            //return (coda.quality.isNull()) ? NULL_DATADOUBLE : new DataDouble(PhaseDescription.toWeight(coda.quality.doubleValue()));
          case DATETIME:
            return new DataDouble(coda.getTime());
          case RESIDUAL:
            if (coda.getChannelMag() == null) return NULL_DATADOUBLE;
            if (! coda.channelMag.residual.isNull()) return coda.channelMag.residual;
            double mag = (coda.getAssociatedMag() == null) ?
                   -9. : coda.getAssociatedMag().value.doubleValue();
            if (mag == -9.) return NULL_DATADOUBLE;
            return (coda.channelMag.value.isNull()) ?
              NULL_DATADOUBLE : new DataDouble(Math.abs(coda.channelMag.value.doubleValue() - mag));
          case DISTANCE:
            //return coda.getChannelObj().dist;
            double value = coda.getHorizontalDistance(); // don't use slant coda.getDistance();
            return (value == Channel.NULL_DIST) ? NULL_DATADOUBLE : new DataDouble(value);
          case AZIMUTH:
            //return coda.getChannelObj().azimuth;
            value = coda.getAzimuth();
            return (Double.isNaN(value)) ? NULL_DATADOUBLE : new DataDouble(value);
          case IN_WGT:
            //return (coda.getChannelMag()==null) ? NULL_DATADOUBLE : new DataDouble(PhaseDescription.toWeight(coda.getChannelMag().getInWgt())); // actual a double
            return (coda.getChannelMag()==null) ? NULL_DATALONG :
              new DataLong(PhaseDescription.toWeight(coda.getChannelMag().getInWgt())); // view as hypoinverse int wt 0-4
          case WEIGHT:
            return (coda.getChannelMag()==null) ? NULL_DATADOUBLE :
              coda.getChannelMag().weight;
          case AUTH:
            return new DataString(coda.authority.toString());
          case SOURCE:
            return new DataString(coda.source.toString());
          case DESCRIPTION:
            return new DataString(coda.descriptor.getDescription());
          case PROCESSING:
            return new DataString(coda.processingState.toString());
          case DELETE:
            return new DataString(String.valueOf(coda.isDeleted()));
          case UNITS :
            return coda.ampUnits;
          case TAU   :
            return coda.tau;
          case AFIX   :
            return coda.aFix;
          case AFREE   :
            return coda.aFree;
          case QFIX   :
            return coda.qFix;
          case QFREE   :
            return coda.qFree;
          case ALGORITHM   :
            return coda.algorithm;
          case WSIZE   :
            return coda.windowSize;
          case WCOUNT   :
            return coda.windowCount;
          case WTIME   :
            ArrayList al = (ArrayList) coda.getWindowTimeAmpPairs();
            if (al.size() > 0) {
              // 1st window time value if using old-style relative tau time:
              //return new DataDouble(coda.getTime() + ((TimeAmp) al.get(0)).getTimeValue());
              // else value for use of absolute time base:
              return ((TimeAmp) al.get(0)).getTime();
            }
            else return NULL_DATADOUBLE;
          case AMP   :
            al = (ArrayList) coda.getWindowTimeAmpPairs();
            // 1st window amp value: 
            return (al.size() > 0) ?
              ((TimeAmp) al.get(0)).getAmp() : NULL_DATADOUBLE;
          case ERR   :
            return coda.uncertainty;
          case RMS   :
            return coda.residual;
          case CMAG  :
            return (coda.getChannelMag() == null) ? NULL_DATADOUBLE :
              coda.getChannelMag().value;
          case MCORR :
            return (coda.getChannelMag() == null) ? NULL_DATADOUBLE :
              coda.getChannelMag().correction;
          case MAG   :
            return (coda.getAssociatedMag() == null) ? NULL_DATADOUBLE :
              coda.getAssociatedMag().value;
          case TYPE:
            return new DataString(coda.descriptor.getDurationTypeName());
          case MAGID :
            return (coda.getAssociatedMag() == null) ? NULL_DATALONG :
              coda.getAssociatedMag().magid;
          case EVID:
            return (coda.getAssociatedSolution() == null) ? NULL_DATALONG :
               coda.getAssociatedSolution().getId();
          case ORID:
            if (coda instanceof org.trinet.jasi.coda.TN.CodaTN) {
              SolutionTN solTN = (SolutionTN) coda.getAssociatedSolution();
              return (solTN != null) ? solTN.orid : NULL_DATALONG;
            }
            else return NULL_DATALONG ;
          case COID:
            if (coda instanceof org.trinet.jasi.coda.TN.CodaTN) {
               org.trinet.jasi.coda.TN.CodaTN codaTN = (org.trinet.jasi.coda.TN.CodaTN) coda;
               return (codaTN.getCoid() > 0) ?
                 new DataLong(codaTN.getCoid()) : NULL_DATALONG ;
            }
            else return NULL_DATALONG;
          default:
            return "";
        }
    }
//residual = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.RMS);
    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Coda coda = getCoda(rowIndex);
        boolean validCase = true;
        boolean effectsMagnitude = true;
      try {
        switch (colIndex) {
          case NET:
            coda.getChannelObj().setNet(value.toString());
            break;
          case STA:
            coda.getChannelObj().setSta(value.toString());
            break;
          case CHN:
            String chn = value.toString();
            coda.getChannelObj().setChannel(chn);
            // kludge need to have props set or new subclass for implementing particulars
            if (chn.length() > 0 && coda.getChannelObj().getLocation().equals(defaultChannelLocation)) {
              setValueAt(getSeedchanMap(chn), rowIndex, SEED);
            }
            //coda.getChannelObj().setChannel(value.toString());
            break;
          case AU:
            coda.getChannelObj().setAuth(value.toString());
            break;
          case SUBSRC:
            effectsMagnitude = false;
            coda.getChannelObj().setSubsource(value.toString());
            break;
          case CHNSRC:
            coda.getChannelObj().setChannelsrc(value.toString());
            break;
          case SEED:
            coda.getChannelObj().setSeedchan(value.toString());
            break;
          case LOC:
            coda.getChannelObj().setLocation(value.toString());
            break;
          case PHASE:
             coda.setCodaType(CodaType.getCodaType(value.toString()));
             break;
          case QUALITY:
            // don't sync with in_wgt here, infinite loop, the value change
            // should make mag stale to force in_wgt change upon mag solve.
            coda.quality.setValue(value);
            // could make Coda descriptor like Phase descriptor and have the "weight" included
            //coda.descriptor.setWeight(value);  // valueToInt(value);
            //Below is analog of phase, Coda description using hypoinv 0-4 wts:
            //coda.quality.setValue(PhaseDescription.toQuality(valueToInt(value)));
            break;
          case DATETIME:
            coda.setTime(((DataDouble)value).doubleValue());
            break;
          case RESIDUAL:
            effectsMagnitude = false;
            if (coda.getChannelMag()!=null) coda.getChannelMag().residual.setValue(value);
            break;
          case DISTANCE:
            effectsMagnitude = false;
            //coda.getChannelObj().dist.setValue(value);
            //coda.setDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            coda.setHorizontalDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            break;
          case AZIMUTH:
            effectsMagnitude = false;
            //coda.getChannelObj().azimuth.setValue(value);
            coda.setAzimuth(((DataDouble)value).doubleValue());
            break;
          case IN_WGT:
            //coda.getChannelMag().inWgt.setValue(value);
            Double q = new Double(PhaseDescription.toQuality(valueToInt(value)));
            setValueAt(q, rowIndex, QUALITY); // force sync with quality
            coda.getChannelMag().inWgt.setValue(q);
            break;
          case WEIGHT:
            effectsMagnitude = false;
            coda.getChannelMag().weight.setValue(value);
            break;
          case AUTH:
            effectsMagnitude = false;
            coda.authority.setValue(value);
            break;
          case SOURCE:
            effectsMagnitude = false;
            coda.source.setValue(value);
            break;
          case DESCRIPTION:
            coda.setPhaseDescription(value.toString());
            break;
          case PROCESSING:
            effectsMagnitude = false;
            coda.processingState.setValue(value);
            break;
          case DELETE:
            boolean isDeleted = Boolean.valueOf(value.toString()).booleanValue();
            // Toggling delete flag in reading could evoke a "stale" event notification
            // Do not unassociate, it nulls references.
            // coda.setDeleteFlag(isDeleted); // assume doesn't unassociate() references

            // Code below implies GUI Magnitude level modification
            if (this.mag != null) {
              if (isDeleted) mag.delete(coda); // test 03/03 aww
              else mag.undelete(coda); // test 03/03 aww
              // should be unecessary unless delete nulled a ref
              // below has coupling, does mag.sol too:
              if (! isDeleted) coda.associate(this.mag);
            }
            // Code below implies GUI Solution level panel modification
            // normally only edit from the Magnitude not the Solution panel
            // need to reload Amps and recalculate a prefMag afterwards:
            else if (this.sol != null) { // force prefMag stale?
              if (isDeleted) sol.delete(coda); // test 03/03 aww
              else sol.undelete(coda); // test 03/03 aww
              if (! isDeleted) { 
                Magnitude prefMag = this.sol.getPreferredMagnitude();
                if (prefMag != null && prefMag.isCodaMag()) {
                   // If prefMag codaList was not already loaded from DataSource 
                   // association of this coda with it here is futile for reloc
                   // same logic as above, coupled does this.sol too:
                   coda.associate(prefMag);
                }
                // redundant, unless nulled by deletion:
                else coda.associate(this.sol);
              }
            }
            // below needed only if values are were reset by delete state change: 
            //fireTableCellUpdated(rowIndex, EVID);
            //fireTableCellUpdated(rowIndex, ORID);
            //fireTableCellUpdated(rowIndex, MAGID);
            break;
          case UNITS :
            coda.ampUnits.setValue(value);
            break;
          case TAU   :
            coda.tau.setValue(value);
            break;
          case AFIX   :
            coda.aFix.setValue(value);
            break;
          case AFREE   :
            coda.aFree.setValue(value);
            break;
          case QFIX   :
            coda.qFix.setValue(value);
            break;
          case QFREE   :
            coda.qFree.setValue(value);
            break;
          case ALGORITHM   :
            effectsMagnitude = false;
            coda.algorithm.setValue(value);
            break;
          case WSIZE   :
            effectsMagnitude = false;
            coda.windowSize.setValue(value);
            break;
          case WCOUNT   :
            effectsMagnitude = false;
            coda.windowCount.setValue(value);
            break;
          case WTIME   :
            ArrayList al = (ArrayList) coda.getWindowTimeAmpPairs();
            if (al.size() > 0) {
              TimeAmp ta = (TimeAmp) al.get(0);
              //al.clear(); // don't clear, may have first and last window values  - aww 05/04
              //al.add(new TimeAmp((DataDouble) value, (DataDouble) getValueAt(rowIndex,AMP))); // aww 05/04
              al.set(0, new TimeAmp((DataDouble) value, (DataDouble) getValueAt(rowIndex,AMP)));
            }
            break;
          case AMP   :
            al = (ArrayList) coda.getWindowTimeAmpPairs();
            if (al.size() > 0) {
              //TimeAmp ta = (TimeAmp) al.get(0);
              //al.clear(); // don't clear, may have first and last scan window values  - aww 05/04
              //al.add(new TimeAmp((DataDouble) getValueAt(rowIndex,WTIME),(DataDouble) value)); // aww 05/04
              al.set(0, new TimeAmp((DataDouble) getValueAt(rowIndex,WTIME),(DataDouble) value)); // aww 05/04
            }
            break;
          case ERR   :
            effectsMagnitude = false;
            coda.uncertainty.setValue(value);
            break;
          case RMS   :
            effectsMagnitude = false;
            coda.residual.setValue(value);
            break;
          case CMAG  :
            if (coda.getChannelMag() != null)
                coda.getChannelMag().value.setValue(value);
            break;
          case MCORR :
            if (coda.getChannelMag() != null)
                coda.getChannelMag().correction.setValue(value);
            break;
          case MAG   :
            effectsMagnitude = false;
            if (coda.getAssociatedMag() != null)
                coda.getAssociatedMag().value.setValue(value);
            break;
          case TYPE:
             coda.setDurationType(CodaDurationType.getDurationType(value.toString()));
             break;
          case MAGID :
            validCase = false;
            //if (coda.getAssociatedMag() != null) coda.getAssociatedMag().magid.setValue(value);
            break;
          case EVID:
            validCase = false;
            // don't ever enable this here dominoes effect all.
            //if (coda.getAssociatedSolution() != null)
            //coda.getAssociatedSolution().setId(((DataLong)value).longValue());
            break;
          case ORID:
            validCase = false;
            // don't ever enable this here dominoes effect all.
            //if (coda instanceof CodaTN) {
            //  if (coda.getAssociatedSolution() != null)
            //    ((SolutionTN) coda.getAssociatedSolution()).dbaseSetPrefor((DataObject)value));
            //}
            break;
          case COID:
            validCase = false;
            //if (coda instanceof org.trinet.jasi.coda.CodaTN) ((org.trinet.jasi.coda.CodaTN) coda).setCoid(((DataObject)value).longValue());
            break;
          default :
            validCase = false;
        }
      }
      catch (Throwable thrown) {
         System.out.println("Exception, attempt to setValueAt failed for " + getClass().getName());
         return;
      }
        if (validCase) {
            coda.setNeedsCommit(true);
            if (coda.isAssociatedWithMag() && effectsMagnitude) {
              coda.getAssociatedMag().setStale(true);
            }
            fireTableCellUpdated(rowIndex, colIndex);
            //if (colIndex != PROCESSING && !coda.isHuman() ) // revert final?
            //if (coda.processingState.toString().equals(STATE_AUTO_TAG))
            if (coda.isAuto())
               setValueAt(JasiProcessingConstants.STATE_HUMAN_TAG, rowIndex, PROCESSING);
        }
    }

    // initRowValues is nework specific re subtype defaults, could implement factory method to get default or subclass.
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        Coda newCoda = (Coda) super.initRowValues(aRow);
        newCoda.descriptor.setCodaType(CodaType.P);
        newCoda.descriptor.setDurationType(CodaDurationType.H);
        newCoda.qFix.setValue(1.8);
        newCoda.algorithm.setValue("HAND");
        newCoda.ampUnits.setValue(CalibrUnits.getString(CalibrUnits.COUNTS));
        // not null units, what about tau or amp?
        return newCoda;
    }

}

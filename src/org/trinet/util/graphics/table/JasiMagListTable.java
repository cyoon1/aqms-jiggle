package org.trinet.util.graphics.table;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
public class JasiMagListTable extends AbstractJasiListTable {
    public JasiMagListTable(JasiMagListTableModel tm) {
            super(tm);
            loadTableProperties("MagListTable.props");
    }
    protected boolean  configureColEditorRendererByName(TableCellEditorRenderer tcer,
              int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer, modelIndex, colName);
          if (colName.equals("MT")) {
            tcer.tcEditor = new ComboBoxCellEditor("MT", new String [] {
              "l","h","d","c","e","w","lr","s","a","b" //,"l1","l2","lg","z","B","un","n"
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          return setValues;
    }
}

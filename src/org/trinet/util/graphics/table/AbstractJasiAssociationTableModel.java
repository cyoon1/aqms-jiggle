//TODO: change name to AbstractJasiSolAssocTableModel
package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.graphics.*;

public abstract class AbstractJasiAssociationTableModel
        extends AbstractJasiListTableModel implements JasiListTableModelAssocSolIF {

    protected Solution sol;
    //must delete from Solution input list needed for LocEng, MagEng
    //protected ArrayList backUpList = null;  // override purgeRows to save clone of original list here

    protected AbstractJasiAssociationTableModel() {
        super();
    }

    protected AbstractJasiAssociationTableModel(ActiveArrayListIF associatedList) {
        this(associatedList, false);
    }

    protected AbstractJasiAssociationTableModel(ActiveArrayListIF associatedList, boolean isCellEditable) {
        this();
        setList(associatedList);
        setCellEditable(isCellEditable);
    }

    public void setAssocSolution(Solution sol) { this.sol = sol;}
    public Solution getAssocSolution() { return this.sol;}

    public void setList(ActiveArrayListIF aList) {
        super.setList(aList);
        if (this.modelList.size() > 0) {
           setAssocSolution(
             ((JasiSolutionAssociationIF) getJasiCommitable(0)).getAssociatedSolution()
           );
        }
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        JasiSolutionAssociationIF jassoc =
            (JasiSolutionAssociationIF) aRow;
        jassoc.assign(sol); // does not update list state
        return jassoc;
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF jasiCommitable) {
       if (jasiCommitable == null) return false;
       SolutionAssocJasiObject solAssocObj = (SolutionAssocJasiObject) jasiCommitable;
       //not sure about need for the null test below:
       if (this.sol != null && solAssocObj.getAssociatedSolution() != this.sol) solAssocObj.assign(this.sol);
       return super.insertRow(rowIndex, jasiCommitable);
    }
    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (sol == null) {
          aLog.logTextnl("ERROR Assoc Panel Solution is null!");
          new Exception("Null Solution").printStackTrace();
          return false;
        }
        else {
          if (sol.isLocked() && !sol.lockIsMine()) {
            String str = "EVENT " + sol.getId().toString() + " IS LOCKED."
            + sol.getLockString();
            InfoDialog.informUser(null, "ERROR", str, null);
            aLog.logTextnl(str);
            return false;
          }
          if (! sol.isLocked() && ! sol.lock()) {
            String str = "UNABLE TO LOCK EVENT " + sol.getId().toString() + "\n"
            + sol.getLockString();
            InfoDialog.informUser(null, "ERROR", str, null);
            aLog.logTextnl(str);
            return false;
          }
        }
        return super.isCellEditable(rowIndex,colIndex);
    }
    // To avoid repeating code where just one method invocation differs
    // could use reflection to delegate tasks to generic method and by
    // passing in name of method to invoke on object.
    public int stripByResidual(double value) {
      if (value == 0. || Double.isNaN(value)) return 0;
      double absValue = Math.abs(value);
      //(AbstractSolutionAssocListIF) modelList).stripByResidual(absValue);
      int count = 0;
      int rows = modelList.size();
      JasiAssociationFitIF dataRow = null;
      for (int idx = 0; idx < rows; idx++) {
        dataRow = (JasiAssociationFitIF) modelList.get(idx);
        if (Math.abs(dataRow.getResidual()) >= absValue) {
          //if (dataRow instanceof JasiReadingIF) ((JasiReadingIF) dataRow).setReject(true); // up weight the reading -aww 08/23/2007 replaced
          //else
            dataRow.setDeleteFlag(true); // delete();
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }
    public int stripByDistance(double value) {
      if (value <= 0. || Double.isNaN(value)) return 0;
      //(AbstractSolutionAssocListIF) modelList).stripByDistance(value);
      int count = 0;
      int rows = modelList.size();
      JasiAssociationFitIF dataRow = null;
      for (int idx = 0; idx < rows; idx++) {
        dataRow = (JasiAssociationFitIF) modelList.get(idx);
        if (dataRow.getDistance() >= value) { // epicentral or horizontal distance too big
          //if (dataRow instanceof JasiReadingIF) ((JasiReadingIF) dataRow).setReject(true); // up weight the reading -aww 08/23/2007 replaced
          //else
            dataRow.setDeleteFlag(true); // delete();
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }
}

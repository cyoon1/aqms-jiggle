package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class AmpMmTextParser extends DefaultAmpTextParser {
  public AmpMmTextParser() {
    super();
    dialogTitle = "<sta> <chan> <value(mm)> [yyyy-MM-dd:HH:mm:ss.s] [type(S)]";
  }
  public JasiCommitableIF createRow() {
      Amplitude aAmp = (Amplitude) super.createRow();
      if (aAmp != null) aAmp.value.setValue(amplitude/10.);
      Debug.println("DEBUG parsed amp: " + aAmp.toString());
      return aAmp;
  }
}

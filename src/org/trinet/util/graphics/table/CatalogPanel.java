package org.trinet.util.graphics.table;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
// DK CLEANUP import org.trinet.jdbc.table.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.graphics.*;

import org.trinet.jasi.EventSelectionProperties;

/** Displays a SolutionList in a JTable in a JPanel and allows table data to be sent to database.
* example code on usage: <BR>

        CatalogPanel catPanel = new CatalogPanel(solutionList, textArea, false, false); <BR>
        catPanel.addWindowListener(windowFrame); <BR>
        windowFrame.getContentPane().add(catPanel); <BR>
        catPanel.setUpdateDB(true); <BR>

*/

// 1/22/02 DDG added "COMMENT"

public class CatalogPanel extends JPanel implements Observer, TableModelListener {

    public static final String VERSION = "1.20000202";
    private static final int ID_COLUMN = 0;
    private static final String ORIGIN = "Origin";
    private static final String EVENT = "Event";
    private static final JLabel statusLabel = new JLabel(" ");
    private static final ImageIcon LIGHT_BULB = new ImageIcon(IconImage.getImage("lightBulb.jpg"));
    private static final ImageIcon DARK_BULB = new ImageIcon(IconImage.getImage("darkBulb.jpg"));

    //private String [] columnOrderByName = CatalogTableConstants.columnNames;
    protected String [] columnOrderByName = {
            //"DATETIME", "LAT", "LON", "MAG", "MTYP", "MMETH", "MOBS", "MSTA", "Z", "AUTH",
            //"SRC", "GAP", "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED", "S",
            //"FM", "Q", "V", "B", "ETYPE", "ST", "ZF", "HF", "TF", "WRECS",
            //"PR", "COMMENT", "CM", "VM", "VER","LDDATE"
            "EX_ID", "DATETIME", "OT", "LAT", "LON", "METHOD","Me", "Mw", "Ml", "Mlr",
            "Md", "Ml-Md", "MAG", "MTYP", "MMETH", "MOBS", "MSTA", "MERR", "Z", "PM",
            "AUTH", "SRC", "GAP", "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED",
            "AUSE", "S", "FM", "Q", "V", "B", "ETYPE", "GT", "ST", "ZF",
            "HF", "TF", "WRECS", "PR", "COMMENT", "OWHO", "MWHO", "CM", "VM", "VER",
            "LDDATE"
    };

    private JTextArea textArea = null;
//    private Connection connection = null;
    private SolutionList solutionList = null;
    private TablePanel tablePanel;

   /** JPanel containing action buttons */
    private JPanel buttonPanel;
    private boolean updateDB = false;
    private boolean tableModified = false;

//    private DataSource dataSource = new DataSource(); // unfortunately assumes default connection parameters;

    ListDataStateAdapter myLDSL = new ListDataStateAdapter() {
        public void stateChanged(ListDataStateEvent e) {
            //System.out.println("DEBUG CatalogPanel stateChanged for solution list.");
            if (e.getSource() == getSolutionList()) {
                if (e.getStateChange().getStateName().equals("selected") ) {
                    int irow = tablePanel.catalogTable.modelToSortedRowIndex( getSolutionList().getSelectedIndex() );
                    //System.out.println("DEBUG CatalogPanel selection for solution list row: " + irow);
                    tablePanel.catalogTable.getRowHeader().changeSelection(irow, 0, false, false);
                }
            }
        }
    };

// CatalogPanel Constructors follow:

/** Constructor sets defaults SolutionList == null, JTextArea == null, updateDB == false.
*/
    public CatalogPanel() { }

    public CatalogPanel(String [] tableColumnNames) {
        setColumnOrder(tableColumnNames);
        initPanel();   // Need to rework via manangingFocus()?
    }

    public CatalogPanel(SolutionList solutionList) {
        this(solutionList, null, false);
    }

    public CatalogPanel(SolutionList solutionList, JTextArea textArea) {
        this(solutionList, textArea, false);
    }

/** Constructor
* @param solutionList org.trinet.jasi.SolutionList can be null.
* @param textArea javax.swing.JTextArea to which output is appended can be null.
* @param updateDB true == allows table edits and database updates.
*/
    public CatalogPanel(SolutionList solutionList, JTextArea textArea, boolean updateDB) {
        initSolutionList(solutionList);
        this.textArea     = textArea;
        this.updateDB     = updateDB;
        initPanel();   // Need to rework via manangingFocus()?
    }

    public void updateTable() {
        if (tablePanel != null) tablePanel.updateTable();
    }

// Begin CatalogPanel methods
    private void initPanel() {
        setLayout(new BorderLayout());
        JPanel masterPanel = new JPanel();
        masterPanel.setLayout(new BorderLayout());
        tablePanel = new TablePanel();
        masterPanel.add(tablePanel, BorderLayout.CENTER);
        this.add(masterPanel,BorderLayout.CENTER);

        statusLabel.setForeground(Color.red);
        statusLabel.setHorizontalAlignment(JLabel.LEFT);
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(BorderFactory.createEtchedBorder());
        statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        statusPanel.add(statusLabel);
        this.add(statusPanel, BorderLayout.SOUTH);
//        Debug.println("CatalogPanel layout prefSize: " + ((BorderLayout) getLayout()).preferredLayoutSize(this));
        if (solutionList != null) tablePanel.startTableThread();
    }

    public String [] getTableColumnHeaders() {
        if (tablePanel == null || tablePanel.catalogTable == null) return null;
        return tablePanel.catalogTable.getTableColumnHeaders();
    }

    /*
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Syntax main args: hostName [hrsback]");
            System.exit(0);
        }

        System.out.println("Creating new database connection for default account on:" + args[0]);

        DataSource ds   = TestDataSource.create(args[0]);

        long now = System.currentTimeMillis()/1000;
        long then = now - 14400;
        if (args.length > 1 ) {
//            Debug.println("input hours back args[1]: " + args[1]);
            try {
                then = now - (long) (3600 * Integer.parseInt(args[1]));
            }
                catch (NumberFormatException ex) {
                System.err.println("ERROR Input argument cannot be parsed as integer hours: " + args[1]);
                System.exit(0);
            }
        }
        EventSelectionProperties eventProps = new EventSelectionProperties();
        //eventProps.setFiletype("jiggle"); // test file i/o in the user's home "jiggle" subdir
        eventProps.setTimeSpan(then, now);

        DataSource.setWriteBackEnabled(true);

        SolutionList sl = new SolutionList();
        //        sl.setConnection(connect);
        sl.setProperties(eventProps);
        sl.fetchByProperties();
//        if (sl.size() > 1) sl.getSolution(1).delete();
        JTextArea jta = new JTextArea(10,132);
        jta.setEditable(true);

//        CatalogPanel cp = new CatalogPanel(sl, jta, true);
        // String [] testCols = { "DATETIME", "LAT", "LON", "MAG", "MTYP", "Z", "AUTH", "SRC", "GAP", "DIST", };
        String [] testCols = {
            "DATETIME", "LAT", "LON", "MAG", "MTYP", "MMETH", "Z", "AUTH", "SRC", "GAP", "DIST",
            "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED", "S", "FM", "Q",
            "ETYPE", "ST", "ZF", "HF", "TF", "WRECS", "PR", "VER","COMMENT" };
        CatalogPanel cp = new CatalogPanel(testCols);

        cp.setSolutionList(sl);
        cp.setUpdateDB(true);

         // Next line of code you have to add to a listener or in main app after table is created.
        //String [] saveHeader = cp.getTableColumnHeaders();
        JFrame aFrame = new JFrame();
        aFrame.addWindowListener(new WindowAdapter () {
            public void windowClosing(WindowEvent evt) {
                Window wnd = evt.getWindow();
                wnd.setVisible(false);
                wnd.dispose();
                System.exit(0);
            }
        });

        cp.addWindowListener(aFrame);
        aFrame.setTitle("Catalog");

        JTabbedPane jtb = new JTabbedPane();
        JPanel testPanel = new JPanel();
        testPanel.setLayout(new BorderLayout());
        JButton jbTest = new JButton("Nada");
        testPanel.add(jbTest, BorderLayout.NORTH);
        testPanel.add(jtb, BorderLayout.CENTER);

        jtb.insertTab("Catalog", null, cp, "solutions", 0);

        JScrollPane textScrollPane = new JScrollPane(jta,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JSplitPane split =
            new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                           false,                // don't repaint until resizing is done
                           testPanel,                // top component
                           textScrollPane);        // bottom component
        split.setOneTouchExpandable(true);

        //        aFrame.getContentPane().add(testPanel, BorderLayout.CENTER);
        aFrame.getContentPane().add(split, BorderLayout.CENTER);
        aFrame.pack();
//        Debug.println("aFrame layout prefSize: "
//                 + ((BorderLayout) aFrame.getLayout()).preferredLayoutSize(aFrame));
//        Debug.println("aFrame insets: " + aFrame.getInsets().toString());
//      aFrame.setBounds(50,100,229,202);
        aFrame.setBounds(50,100,256,256);
        aFrame.setVisible(true);
        split.setDividerLocation((int) (0.75*aFrame.getSize().height));

    } // end of main test method
*/
    /** Return the JPanel containing the action button. Can be used to add more
     *  buttons. */
    public JPanel getButtonPanel() {
       return buttonPanel;
    }

/** Required method to implement TableModelListener interface */
    public void tableChanged(TableModelEvent event) {
//        Debug.println("TME getSource(): " + event.getSource());
        tableModified = true;
    }

    public void update() { // added to force kludge after solved if aliased list, else not needed aww 11/03
//        System.err.println("DEBUG CatalogPanel update fire");
        tablePanel.catalogTable.getModel().fireTableDataChanged();
    }

/** Required method to implement the observer interface.
*/
    public void update(Observable observable, Object arg) {
        logTextnl("DEBUG CatalogPanel update() : Observable = " + observable.getClass().getName());
        logTextnl("DEBUG CatalogPanel update() : Argument   = " + arg.getClass().getName());
        //logTextnl("DEBUG CatalogPanel update() : fireTableDataChanged()");
        if (org.trinet.jasi.SolutionList.class.isInstance(arg)) {
                setSolutionList((SolutionList) arg);
        }
// note the the catalog table data model is aliased solutionList reference in the class's constructor.
//        tablePanel.catalogTable.getModel().fireTableDataChanged();
    }
/**
 * Returns Solution array containing selected rows in table row header.
*/
    public Solution [] getSelectedSolutions() {
        if (tablePanel.catalogTable.getRowHeader() == null) return null;
        int selected [] = tablePanel.catalogTable.getRowHeader().getSelectedRows();
        int nselected = selected.length;
        if (nselected == 0) return null;
        Solution [] solutions = new Solution [nselected];

        SolutionList solutionList = tablePanel.catalogTable.getModel().getList();
        Solution [] master = solutionList.getArray();
        int masterLength = master.length;

        if (nselected > masterLength)
                 throw new ArrayIndexOutOfBoundsException("CatalogPanel: getSelectedSolutions() SelectedRows>SolutionList.size().");

        for (int index = 0; index < nselected; index++) {
            solutions[index] = master[tablePanel.catalogTable.getSortedRowModelIndex(selected[index])];
        }
        return solutions;
    }

/** Returns long array containing selected event ids from table row header.
*/
    public long [] getSelectedIds() {
        if (tablePanel.catalogTable.getRowHeader() == null) return null;
        int rowIds[] = tablePanel.catalogTable.getRowHeader().getSelectedRows();
        int nrows = rowIds.length;
        if (nrows == 0) return null;
        long [] ids = new long [nrows];
        CatalogTableModel ctm = tablePanel.catalogTable.getModel();
        for (int index = 0; index < nrows; index++) {
            ids[index] =
                ((DataObject) ctm.getValueAt(tablePanel.catalogTable.getSortedRowModelIndex(rowIds[index]), ID_COLUMN)).longValue();
        }
        return ids;
    }

    public boolean hasNullConnection() {
        if (solutionList == null) return true;
        else return (solutionList.getConnection() == null) ;
    }

    public int updateDB() {
        return tablePanel.updateDB();
    }

    public boolean hasModifiedTable() {
        return tableModified;
    }

    public boolean hasUnmodifiedTable() {
        return ! tableModified;
    }

    public void unsetTableModified() {
        tableModified = false;
    }

    public boolean isModifiable() {
        return (hasUpdateEnabled() && ! hasNullConnection());
    }

    public boolean isNotModifiable() {
        return (hasUpdateDisabled() || hasNullConnection());
    }

/** Returns true is table model data exists. */
    public boolean hasValidTable() {
        return tablePanel.hasValidTable();
    }
    public boolean hasInvalidTable() {
        return ! hasValidTable();
    }

    public boolean isCommittable() {
        if (hasValidTable()) return isModifiable();
        return false;
    }

    public int commitModifiedTable() {
        if (hasUnmodifiedTable()) return 0;
        return commit();
    }

    public int commit() {
        if (isCommittable()) {
            boolean status = false;
            if (hasModifiedTable()) {
                status = commitConfirmed("modified table");
            }
            else {
                status = commitConfirmed("unmodified table");
            }
            if (status) return updateDB();
        }
        return 0;
    }

    private boolean commitConfirmed(String message) {
        return ( JOptionPane.showConfirmDialog(this,
                "Commit " + message,
                "Verify Commit", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION );
    }

    public void setWhereIsEngine(WhereIsEngine eng) {
        tablePanel.whereFrom = eng;
    }

/** Set the column order */
    public void setColumnOrder(String [] order) {
        columnOrderByName = order;
    }

/** Sets the event solution list and creates new table.
*/
    public void setSolutionList(SolutionList list) {
        commitModifiedTable();
        initSolutionList(list);
//      tablePanel.catalogTable.getModel().setList(list);
        tablePanel.startTableThread();
    }

    protected void initSolutionList(SolutionList list) {
        if (solutionList != null) {
            solutionList.removeListDataStateListener(myLDSL);
        }
        solutionList = (SolutionList) list ;
        if (solutionList != null) {
            solutionList.addListDataStateListener(myLDSL);
        }
    }


    public SolutionList getSolutionList() {
        //return tablePanel.catalogTable.getModel().getList();
        return solutionList; // 
    }

    public boolean hasNullSolutionList() {
        return (solutionList == null);
    }

/** Sets the JTextArea to which output text information is appended.
*/
    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }

    public JTextArea getTextArea() {
        return this.textArea;
    }

/** Toggles allowing enabling of database updates from table using panel actions.
* @param updateDB true == allow updates, default == false.
*/
    public void setUpdateDB(boolean updateDB) {
        this.updateDB = updateDB;
        tablePanel.catalogTable.getModel().setCellEditable(updateDB);
        if (! updateDB) tablePanel.hideModifyButtons();
    }

/** Return the value of the database update switch.
*/
    public boolean hasUpdateEnabled() {
        return this.updateDB;
    }

/** Return negation of the value of the database update switch.
*/
    public boolean hasUpdateDisabled() {
        return ! this.updateDB;
    }


/** Adds a window listener to Window into which this component is added.
* Needed to cleanup editing, or save work,  if window is closed inadvertently.
*/
    public void addWindowListener() {
        JRootPane jrp =  this.getRootPane();
        if (jrp != null) this.addWindowListener((Window) jrp.getParent());
    }

/** Adds a window listener to specified Window.
* Needed to cleanup editing, or save work, if specified input window is closed inadvertently.
*/
    public void addWindowListener(Window window) {
        window.addWindowListener(this.new WindowCloser());
    }

    void logText(String text) {
        if (textArea != null) textArea.append(text);
        else System.out.print(text);
    }

    void logTextnl(String text) {
        if (textArea != null) textArea.append(text + "\n");
        else System.out.println(text);
    }

    public void stopTableCellEditing() {
        if (tablePanel != null) {
            if (tablePanel.catalogTable != null) {
                JTable jtable = tablePanel.catalogTable.getTable();
                if (jtable != null) {
                    if (jtable.isEditing()) {
                        jtable.getCellEditor().stopCellEditing();
                    }
                    jtable = tablePanel.catalogTable.getRowHeader();
                    if (jtable.isEditing()) {
                        jtable.getCellEditor().stopCellEditing();
                    }
                }
            }
        }
    }

// CatalogPanel inner classes
    private class WindowCloser extends WindowAdapter {
        public void windowClosing(WindowEvent evt) {
            stopTableCellEditing();
        }
    }

// Inner class containing essential tables and actions
    private class TablePanel extends JPanel implements Runnable {
        CatalogTable catalogTable;
        private JScrollPane spane;
        private Object inputEventSrc;
        private Thread tableThread;
        private Runnable tableThreadPanelUpdate = new UpdateTablePanel();

        private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
        private Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);

        private boolean imageFlag = true;
        private JLabel waitRequestLabel = new JLabel();
        {
            waitRequestLabel.setText("Table results displayed here");
            waitRequestLabel.setHorizontalAlignment(JLabel.CENTER);
        }
        private javax.swing.Timer tableTimer = new javax.swing.Timer(250, new tableTimerActionListener());

        JButton jbInsert = new JButton("Insert");
        JButton jbDelete = new JButton("Delete");
        JButton jbCommit = new JButton("Commit");
        JButton jbFile = new JButton("File");
        JButton jbWheres = new JButton("Where");

        protected WhereIsEngine whereFrom = null;
        private int nrows = 0;
        private boolean interruptRequestFlag = false;
        private boolean tableRowHeadSelectionFlag;

// TablePanel constructor most user gui activity is performed by listeners of TablePanel class
        TablePanel () {
            TablePanel.this.setLayout(new BorderLayout());
            TablePanel.this.add(waitRequestLabel, BorderLayout.CENTER);

            CatalogTableModel ctm = new CatalogTableModel();
            ctm.addTableModelListener(CatalogPanel.this);
            ctm.setCellEditable(CatalogPanel.this.hasUpdateEnabled());
            catalogTable = new CatalogTable(ctm);
            if (columnOrderByName == null) System.out.println("column name order not initialized");
            catalogTable.setColumnNameOrder(columnOrderByName);
            configureButtonPanel();

        } // end of TablePanel() constructor

// TablePanel method to run thread to get data from database and generate table
        private void startTableThread() {
            if (spane != null) {
                commitModifiedTable();
                try {
//            Debug.println("DEBUG: getModel().setList()");
                    TablePanel.this.catalogTable.getModel().setList(solutionList);
                    catalogTable.initColumnSizes(catalogTable.getTable());
//                    spane.revalidate();   // what if column widths change in new list?
                }
                catch(NullPointerException ex) {
                    ex.printStackTrace();
                    return;
                }
/*
                TablePanel.this.remove(spane);
                spane = null;
                TablePanel.this.add(waitRequestLabel, BorderLayout.CENTER);
                disableModifyButtons();
                jbFile.setEnabled(false);
*/
                TablePanel.this.revalidate();
//        Debug.println("DEBUG: unsetTableModified()");
                unsetTableModified();
                return;
            }
//            TablePanel.this.catalogTable.getModel().createList(solutionList);
            tableThread = new Thread(TablePanel.this, "solutionListTable");
            tableTimer.start();
            tableThread.start();
            setCursor(waitCursor);
        }

        public void run() {
            Thread thisThread = Thread.currentThread();
            nrows = 0;
            if (hasNullSolutionList()) {
                System.err.println("CatalogPanel TablePanel Thread run() SolutionList object is null.");
                updateTable();
                return;
            }
            try {
                thisThread.sleep(10l);
                TablePanel.this.catalogTable.getModel().createList(solutionList);
                nrows = CatalogPanel.this.solutionList.size();
                if (nrows > 0) {
                    createTable();
                    unsetTableModified();
                }
                else if (nrows <= 0) {
//                    if (thisThread.isInterrupted() || interruptRequestFlag) resetConnection();
                    logTextnl("CatalogPanel No solutions found in list.");
                }
            }
            catch (InterruptedException ex) {
                System.err.println("Stopping CatalogPanel table creation thread");
            }
            catch (Exception ex) {
                System.err.println("Generic exception caught by run() this CatalogPanel table creation thread.");
                System.err.println(ex.getMessage());
                ex.printStackTrace();
            }
            finally {
                // Better to use a invokeLater(runnable) to make panel changes in the event-dispatch thread:
                updateTable();
            }
        }

        public void updateTable() {
           SwingUtilities.invokeLater(tableThreadPanelUpdate);
        }

        private void configureButtonPanel() {
            jbInsert.setActionCommand("Insert");
            jbInsert.setToolTipText("Adds a new row to catalog table; DB insert on commit.");
            jbInsert.addActionListener(new ModifyTableRowsActionListener());
            jbInsert.setMargin(new Insets(2,2,2,2));

            jbCommit.setActionCommand("Commit");
            jbCommit.setToolTipText("Updates DB with changes to this catalog.");
            jbCommit.addActionListener(new ModifyTableRowsActionListener());
            jbCommit.setMargin(new Insets(2,2,2,2));

            jbDelete.setActionCommand("Delete");
            jbDelete.setToolTipText("Deletes a row from the table; DB delete on commit.");
            jbDelete.addActionListener(new ModifyTableRowsActionListener());
            jbDelete.setMargin(new Insets(2,2,2,2));

            jbFile.setActionCommand("File");
            jbFile.setToolTipText("Saves the field contents of table to ASCII file.");
            jbFile.addActionListener(new SaveTableActionListener());
            jbFile.setMargin(new Insets(2,2,2,2));

            jbWheres.setActionCommand("Where");
            jbWheres.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    int rowid = catalogTable.getRowHeader().getSelectedRow();
                    if (rowid >= 0) {
                        rowid = catalogTable.getSortedRowModelIndex(rowid);
                        CatalogTableModel ctm = catalogTable.getModel();
                        double lat = ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("LAT"))).doubleValue();
                        double lon = ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("LON"))).doubleValue();
                        long id =    ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("ID"))).longValue();

//                        String whereString = Wheres.where(lat, lon);
//                        String whereString = WheresFrom.where(lat, lon);
                        Connection conn = solutionList.getConnection();
                        if (conn != null) {
                        /****
                         *  DK 11/05/2002
                         *  Can't get a hook to the Jiggle configfile down in here(CatalogPanel)
                         *  so we will call create() with no arguments, and hope
                         *  that someone else has already called create() and a
                         *  default WhereIsEngine has been set externally - aww 10/03
                         ****/
                            if (whereFrom == null) {
                              //whereFrom = WhereIsEngine.create();
                              System.err.println("WhereIsEngine null in CatalogPanel; invoking application must set engine first!");
                              System.err.println("See setWhereIsEngine(WhereIsEngine eng) method.");
                            }
                            else {
                              final String whereString = whereFrom.where(lat, lon);
                              logTextnl("Where id: " + id + "\n" + whereString);
                              JOptionPane.showMessageDialog(CatalogPanel.this,
                                 whereString, "Where", JOptionPane.PLAIN_MESSAGE);
                            }
                        }
                    }
                }
            });
            jbWheres.setToolTipText("Report closest to selected row in table.");
            jbWheres.setEnabled(false);
            jbWheres.setMargin(new Insets(2,2,2,2));

            buttonPanel = new JPanel();
            FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
            flowLayout.setAlignment(FlowLayout.CENTER);
            flowLayout.setHgap(0);
            buttonPanel.add(jbInsert);
            buttonPanel.add(jbCommit);
            buttonPanel.add(jbDelete);
            buttonPanel.add(jbFile);
            buttonPanel.add(jbWheres);
            TablePanel.this.add(buttonPanel, BorderLayout.NORTH);
//            Debug.println("jpModify prefSize: " + jpModify.getPreferredSize().toString());

            if (CatalogPanel.this.hasUpdateDisabled()) TablePanel.this.hideModifyButtons();
            disableModifyButtons();
            jbFile.setEnabled(false);
            jbWheres.setEnabled(false);
//            jbFile.setVisible(false);

        }

        private void enableModifyButtons() {
            if (isNotModifiable()) return;
            jbInsert.setEnabled(true);
            jbDelete.setEnabled(true);
            jbCommit.setEnabled(true);
        }

        private void disableModifyButtons() {
            jbInsert.setEnabled(false);
            jbDelete.setEnabled(false);
            jbCommit.setEnabled(false);
        }

        private void showModifyButtons() {
            jbInsert.setVisible(true);
            jbDelete.setVisible(true);
            jbCommit.setVisible(true);
        }

        private void hideModifyButtons() {
            jbInsert.setVisible(false);
            jbDelete.setVisible(false);
            jbCommit.setVisible(false);
        }

        private void createTable() {
            spane = catalogTable.createTable();

//            spane.setToolTipText("Table results.");
//            spane.setForeground(Color.black);
//            spane.setBackground(Color.lightGray);
        }

        private boolean hasInvalidTable() {
            return ! this.hasValidTable();
        }

        private boolean hasValidTable() {
            if (catalogTable.getTable() == null || hasNullSolutionList()) {
//                logTextnl("No rows in Catalog table - execute new request.");
//                InfoDialog.informUser(CatalogPanel.this, "ERROR", "No rows in table - execute new request.", statusLabel);
                return false;
            }
            else return true;
        }

        private boolean insertRow() {
            tableRowHeadSelectionFlag = catalogTable.getRowHeader().getCellSelectionEnabled();
            catalogTable.getRowHeader().setCellSelectionEnabled(true);

            Solution sol = Solution.create();
//            sol.id.setValue(SeqIds.getNextSeq(solutionList.getConnection(), "EVSEQ"));
            sol.id.setValue(sol.getNextId());

            int rowid = catalogTable.getRowHeader().getSelectedRow();
            int modelRowId = 0;
            if (rowid < 0) {
                rowid = catalogTable.getModel().getList().size();
                modelRowId = rowid;
            }
            else {
                modelRowId = catalogTable.getSortedRowModelIndex(rowid);
            }

            boolean retVal = catalogTable.getModel().insertRow(modelRowId, sol);
            if (retVal == true) {
                logTextnl("Inserted row at index: " + rowid + " into table.");
                catalogTable.getRowHeader().setCellSelectionEnabled(tableRowHeadSelectionFlag);
            }
            else logTextnl("Unable to insert row at index: " + rowid + " into table.");

            catalogTable.getRowHeader().setRowSelectionInterval(rowid,rowid);
            resetSelectedRowColumnSizes();

            return retVal;
        }

        private void resetSelectedRowColumnSizes() {
            int rowid = catalogTable.getRowHeader().getSelectedRow();
            catalogTable.resetRowColumnSizes(catalogTable.getRowHeader(), rowid);
            catalogTable.resetRowHeader(catalogTable.getRowHeader(), TablePanel.this.spane);
            spane.revalidate();
        }

        protected int updateDB() {
            if (hasInvalidTable()) {
                InfoDialog.informUser(CatalogPanel.this, "ERROR", "Unable to update DB; no table to update.", statusLabel);
                return -1;
            }
            if (isNotModifiable()) {
                InfoDialog.informUser(CatalogPanel.this, "ERROR", "Unable to update DB; table not write back enabled.", statusLabel);
                return -1;
            }
/* Is not thread safe with multiple table panels and a static data source.
            if (dataSource != null) {
                Connection priorConnect = DataSource.getConnection();
                boolean priorUpdatable = DataSource.isWriteBackEnabled();
                dataSource = new DataSource(solutionList.getConnection(), updateDB);
            }
*/
            int nrow = catalogTable.updateDB();
//            dataSource = new DataSource(priorConnect, priorUpdatable);

            if (nrow < 0) {
                InfoDialog.informUser(CatalogPanel.this, "ERROR", "Unable to update DB; re-edit table and retry.", statusLabel);
                return -1;
            }
            else {
                logTextnl("Updated " + nrow + " rows in catalog.");
            }
            unsetTableModified();
            return nrow;
        }

        private boolean rejectAction(String action) {
            return (JOptionPane.showConfirmDialog(CatalogPanel.this, action, "Confirm", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION);
        }

        private boolean deleteRow() {
            int rowid = catalogTable.getRowHeader().getSelectedRow();
            if (rowid < 0) {
//                logTextnl("No selected row in table - make a selection and retry delete.");
                InfoDialog.informUser(CatalogPanel.this, "ERROR", "No selected row in table - make a selection and retry delete.", statusLabel);
                return false;
            }

            if (rejectAction("Delete")) return false;

            boolean retVal = catalogTable.getModel().deleteRow(catalogTable.getSortedRowModelIndex(rowid));
            if (retVal == true) logTextnl("Deleted row at index: " + rowid + " from table.");
            else logTextnl("Unable to delete row at index: " + rowid + " from table.");

            catalogTable.getTable().clearSelection();
            catalogTable.getRowHeader().clearSelection();

        // Is this next line necessary only if insertRow toggles flag?
            catalogTable.getRowHeader().setCellSelectionEnabled(tableRowHeadSelectionFlag);

            return retVal;
        }

// TablePanel event listener inner classes
        private class UpdateTablePanel implements Runnable {
            public void run() {
                tableTimer.stop();
                if (nrows > 0) remove(waitRequestLabel);
                else {
                    waitRequestLabel.setText("Table results displayed here");
                    waitRequestLabel.setIcon(null);
                }
                setCursor(defaultCursor);

                if (nrows > 0) {
//                  Debug.println("DEBUG: hasModifiedTable(): " + hasModifiedTable());
                    statusLabel.setText("Table has " + nrows + " rows");
                    TablePanel.this.add(spane, BorderLayout.CENTER);
                    enableModifyButtons();
                    jbFile.setEnabled(true);
                    jbFile.setVisible(true);
                    jbWheres.setEnabled(true);
                    jbWheres.setVisible(true);
                    if (updateDB) showModifyButtons();
                }
                else if (nrows < 0) {
                    InfoDialog.informUser(CatalogPanel.this, "ERROR",
                            "Table creation failed; check input.", statusLabel);
                }
                else if (nrows == 0) {
                    System.out.println("No table rows satisfy input properties");
                }

                // make sure selected sol is selected in JTable
                final int selRow = CatalogPanel.this.solutionList.getSelectedIndex();
                if (selRow >= 0) {
                    final JTable table = CatalogPanel.this.tablePanel.catalogTable.getTable();
                    /// scroll selected to visible
                    SwingUtilities.invokeLater(
                      new Runnable() {
                        public void run() {
                          if (table != null) {
                             table.setRowSelectionInterval(selRow, selRow);
                             table.scrollRectToVisible(table.getCellRect(selRow, 0, true));
                          }
                       }
                      } );
                }
                CatalogPanel.this.tablePanel.revalidate();
                tableThread = null;
                interruptRequestFlag = false;
              }
        }

        private class ModifyTableRowsActionListener implements ActionListener {
            public void actionPerformed(ActionEvent evt) {
                statusLabel.setText(" ");
                String cmd = evt.getActionCommand();
//                Object src = evt.getSource();
                if (hasInvalidTable()) return;
                if (cmd.equals("Insert")) {
                    insertRow();
                }
                else if (cmd.equals("Commit")) {
                    commit();
                }
                else if (cmd.equals("Delete")) {
                    deleteRow();
                }
            }
        }

        private class SaveTableActionListener implements ActionListener {
            public void actionPerformed(ActionEvent evt) {
                statusLabel.setText(" ");
                if (catalogTable.getModel().getRowCount() > 0 ) {
                  SaveCatalogTableToASCII saveToTextFile = new SaveCatalogTableToASCII();
                  saveToTextFile.save (catalogTable, (Frame) getTopLevelAncestor());
                }
                else {
//                  logTextnl("No rows in table - execute new request.");
                  InfoDialog.informUser(CatalogPanel.this, "ERROR", "No rows in table - execute new request.", statusLabel);
                }
            }
        }

        private class tableTimerActionListener implements ActionListener {
            public void actionPerformed(ActionEvent evt) {
                waitRequestLabel.setText("table rendering wait...");
                waitRequestLabel.setIcon(imageFlag ? LIGHT_BULB : DARK_BULB);
                imageFlag = ! imageFlag;
                waitRequestLabel.repaint();
            }
        }
    } // end of TablePanel class
} // end of CatalogPanel class

package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
public class DegMinSolutionTextParser extends AbstractJasiTableRowTextParser {
  protected String eventType; 
  protected double lat;
  protected double latm;
  protected double lon;
  protected double lonm;
  protected double z;

  public DegMinSolutionTextParser() {
    super();
    parserName = "Solution";
    dialogTitle = "<yyyy-MM-dd:HH:mm:ss.s> [latd] [latm] [lond] [lonm] [z] [etype]>";
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = new GenericSolutionList();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        lat = 0.;
        latm = 0.;
        lon = 0.;
        lonm = 0.;
        z = 0.;
        eventType = EventTypeMap3.EARTHQUAKE;
        if (strToke.countTokens() < 1) {
          System.err.println("Error: Input solution has too few field tokens");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseDateTime(strToke.nextToken())) {
          System.err.println("Error: Unable to parse solution datetime string");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (strToke.hasMoreTokens()) {
          try {
            lat = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution latd");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            latm = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution latm");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            lon = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution lond");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            lonm = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution lonm");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            z = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution z");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          eventType = strToke.nextToken();
        }
        Debug.println(
          LeapSeconds.trueToString(datetime) //for UTC - aww 2008/02/07 
           + " " +lat+ " " +latm+ " " +lon+ " " +lonm+ " " +z
        ); 
        Solution aRow = (Solution) createRow();
        aRow.setNeedsCommit(false); // why false?, may want this removed
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Solution aSolution = (Solution) tableModel.initRowValues(Solution.create());
      aSolution.setTime(datetime);
      if (lon < 0. && lonm > 0.) lonm = -lonm;
      aSolution.setLatLonZ(lat+latm/60., lon+lonm/60., z);
      if ( ! aSolution.getLatLonZ().isNull() ) {
          // valid, not bogus dummy
          aSolution.validFlag.setValue(1);
          aSolution.dummyFlag.setValue(0);
      }
      aSolution.eventType.setValue( EventTypeMap3.getMap().toJasiType(eventType) );
      aSolution.eventAuthority.setValue(defaultAuth);
      aSolution.eventSource.setValue(defaultSource);
      Debug.println("input sol: " + aSolution.toString());
      return aSolution;
  }
}

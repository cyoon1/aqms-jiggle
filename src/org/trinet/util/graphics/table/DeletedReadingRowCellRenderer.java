package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.jasi.*;

/** Renderer to color table cells of deleted readings
*/
public class DeletedReadingRowCellRenderer extends ColorableTextCellRenderer {
    protected Color deletedRowColor = Color.red;
    protected AbstractTableModel tableModel;
    protected int referenceColumnIndex;

    public DeletedReadingRowCellRenderer (AbstractTableModel tableModel, int referenceColumnIndex) {
        this(tableModel, referenceColumnIndex, Color.white, Color.pink);
    }

    public DeletedReadingRowCellRenderer (AbstractTableModel tableModel, int referenceColumnIndex, Color defaultBackgroundColor, Color deletedRowColor) {
        super();
        this.tableModel = tableModel;
        this.referenceColumnIndex = referenceColumnIndex;
        setDefaultBackground(defaultBackgroundColor);
        setDeletedRowColor(deletedRowColor);
    }

    public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
        Component comp =  super.getTableCellRendererComponent(jtable, value, isSelected, hasFocus, row, col);
        if (! isSelected) {
          if (Boolean.valueOf((String)tableModel.getValueAt(row, referenceColumnIndex).toString()).booleanValue()) {
            comp.setBackground(deletedRowColor); 
          }
          else  {
            comp.setBackground(defaultBackground);
          }
        }
        return comp;
    }

    public void setDeletedRowColor(Color deletedRowColor) {
        this.deletedRowColor = deletedRowColor;
    }
}

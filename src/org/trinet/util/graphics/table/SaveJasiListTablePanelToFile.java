package org.trinet.util.graphics.table;
import java.text.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.graphics.*;

public class SaveJasiListTablePanelToFile extends SaveTablePanelToFile {

    private static final String PATTERN = "0.000000000000";
    private DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
    {
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }

    JCheckBox cascadeAssocPanels;
    JCheckBox rawValueFormat;
    boolean cascade = true;
    boolean raw = false;

    public boolean cascade() { return cascade;}

    public void setDefaultSaveFileName(String name) { fileNameString = name; }

    protected JPanel createOptionsPanel() {
        JPanel optionsPanel = super.createOptionsPanel();
        cascadeAssocPanels = new JCheckBox("All subpanels");
        cascadeAssocPanels.setSelected(cascade);
        rawValueFormat = new JCheckBox("Raw decimals");
        rawValueFormat.setSelected(raw);
        optionsPanel.add(cascadeAssocPanels);
        optionsPanel.add(rawValueFormat);
        return optionsPanel;
    }
    protected void processDialogOptions() {
        super.processDialogOptions();
        cascade = cascadeAssocPanels.isSelected();
        if (cascade) {  // should be done through listener
           append = true;
           appendToFile.setSelected(append);
        }
        raw = rawValueFormat.isSelected();
    }

    protected int [] getTableRows(AbstractJasiListPanel listPanel, boolean selected) {
            return super.getTableRows(listPanel, (useDialog && selected));
    }
    public String formatValueString(int irow, int icol) {
        if (raw) return super.formatValueString(irow, icol);

        TableModel tableModel = activePanel.getModel();
        int fracDigits =
          ((AbstractJasiListTableModel) tableModel).getTableColumnFractionDigits()[icol];
        String retVal = null;
        if (fracDigits <= 0 ) {
          retVal =
            StringSQL.valueOf(
                  tableModel.getValueAt(activePanel.sortedToModelRowIndex(tableRows[irow]), icol)
            );
        }
        else {
          Object value =
            tableModel.getValueAt(activePanel.sortedToModelRowIndex(tableRows[irow]), icol);
          df.applyPattern(PATTERN.substring(0, 2+fracDigits));
          if (value instanceof DataDouble) {
            DataDouble dd = (DataDouble) value;
            if (dd.isNull()) retVal =  StringSQL.valueOf(dd);
            else {
              retVal = df.format(dd.doubleValue());
            }
          }
          else if (value instanceof Double) {
            Double dd = (Double) value;
              retVal = df.format(dd);
          }
          else retVal = super.formatValueString(irow, icol);
        }
        return retVal;
    }
}

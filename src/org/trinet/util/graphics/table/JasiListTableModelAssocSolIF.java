package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
public interface JasiListTableModelAssocSolIF extends JasiListTableModelIF {
    public Solution getAssocSolution() ;
    public int stripByResidual(double value) ;
    public int stripByDistance(double value) ;
}

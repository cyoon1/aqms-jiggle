package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
// sta chan datetime quality phaseDesc
public class DefaultPhaseTextParser extends AbstractJasiReadingTextParser {
  protected String quality ; 
  protected String fm;
  protected int wt;

  public DefaultPhaseTextParser() {
    super();
    parserName = "Phase";
    dialogTitle = "<sta> <chan> <yyyy-MM-dd:HH:mm:ss.s> <pd = <qual(i|e)><type(P|S)>[wt(0-4)][fm(C|+|U|D|-)]>";
    defaultPhase = "P";
    phase = defaultPhase;
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = PhaseList.create();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        if (strToke.countTokens() < 3) {
          System.err.println("Error: Input phase has too few field tokens");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseChannelName(strToke)) {
          System.err.println("Error: Input channel not known");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseDateTime(strToke.nextToken())) {
          System.err.println("Error: Unable to parse phase datetime string");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }

        String phDesc = (strToke.hasMoreTokens()) ? strToke.nextToken() : "";
        if (! parsePhaseDesc(phDesc)) {
          System.err.println("Error: Invalid phase descriptor string");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        Debug.println(sta +" "+  seedchan +" "+
          quality + phase + wt + fm + " " +
          LeapSeconds.trueToString(datetime) //for UTC - aww 2008/02/07 
        ); 
        JasiCommitableIF aRow = createRow();
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  protected boolean parsePhaseDesc(String aStr) {
    // override last phase setting to defaults
    phase = defaultPhase;
    quality = "i";
    wt = -1;
    fm = "..";
    boolean status = true;
    switch (aStr.length()) {
      case (4) :
        String aToken = aStr.substring(3,4).toUpperCase();
        if (aToken.equals("U") || aToken.equals("C")) fm = "c.";
        else if (aToken.equalsIgnoreCase("D")) fm = "d.";
        else if (aToken.equalsIgnoreCase("+")) fm = "+.";
        else if (aToken.equalsIgnoreCase("-")) fm = "-.";
        else if (aToken.equalsIgnoreCase(".")) fm = "..";
        else {
          System.err.println("Error: Unable to parse fm type");
          status = false;
        }
      case (3) :
        try {
          wt = Integer.parseInt(aStr.substring(2,3));
        }
        catch (NumberFormatException ex) {
          System.err.println("Error: Unable to parse phase weight");
          status = false;
        }
      case (2) :
        aToken = aStr.substring(1,2).toUpperCase();
        if (aToken.equals("P") || aToken.equals("S")) {
          phase = aToken;
        }
        else {
          System.err.println("Error: Unable to parse phase type");
          status = false;
        }
      case (1) :
        aToken = aStr.substring(0,1).toLowerCase();
        if (aToken.equals("i") ) {
          quality = aToken;
          if (wt == -1) wt = 1;
        }
        else if ( aToken.equals("e") ) {
          quality = aToken;
          if (wt == -1) wt = 3;
        }
        else if ( aToken.equals("w") ) {
          quality = aToken;
          if (wt == -1) wt = 4;
        }
        else {
          System.err.println("Error: Unable to parse phase quality");
          status = false;
        }
        //if (aStr.length() == 1) {
         // System.err.println("Error: Required phase type missing from descriptor");
         // status = false;
        //}
        break;
      case (0) : // special default case zero input:
        if (wt == -1) wt = 1;
        break;
      default :
          System.err.println("Error: Invalid phase descriptor length: " + aStr.length());
    }
    return status;
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Phase aPhase = (Phase) tableModel.initRowValues(Phase.create());
      aPhase.description.set(phase, quality, fm, wt);
      setChannel(aPhase,defaultNetworkCode,sta,chan,seedchan);
      aPhase.setTime(datetime);
      Debug.println("DEBUG parsed phase: " + aPhase.toString());
      return aPhase;
  }
}

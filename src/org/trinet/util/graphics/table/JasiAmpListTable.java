package org.trinet.util.graphics.table;
import java.awt.Color;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
public class JasiAmpListTable extends AbstractJasiListTable {
    public JasiAmpListTable(JasiAmpListTableModel tm) {
         super(tm);
         loadTableProperties("AmpListTable.props");
    }
    protected boolean configureColEditorRendererByName(TableCellEditorRenderer tcer,
                    int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer, modelIndex, colName);
          if (setValues) return true;
          if (colName.equals("UNITS")) {
            tcer.tcEditor = new ComboBoxCellEditor("UNITS", new String [] {
              "unknown", "counts", "counts/(cm/sec)", "counts/(cm/sec2)",
              "seconds", "m", "cm", "mm", "m/sec",
              "m/sec^2", "cm/sec", "cm/sec^2", "mm/sec", "mm/sec^2",
              "microns", "ns", "ergs", "iovs", "spa"
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("PH")) {
            tcer.tcEditor = new ComboBoxCellEditor("PH", new String [] {"P","S"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("TYPE")) {
            tcer.tcEditor = new ComboBoxCellEditor("TYPE", new String [] {
              "UNKNOWN", "HEL", "WA", "WAS", "PGA", "PGV", "PGD", "WAC", "WAU",
              "VI2", "SP.3","SP1.0","SP3.0","ML100","ME100","EGY"
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("CLP")) {
            tcer.tcEditor = new ComboBoxCellEditor("CLP", new DataLong [] {
              new DataLong(0l), new DataLong(1l), new DataLong(2l) 
            });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("HALF")) {
            tcer.tcEditor = new NamedCheckBoxCellEditor("HALF", "false");
            tcer.tcRenderer = new NamedCheckBoxCellRenderer("HALF", "false");
            setValues = true;
          }
          else if (colName.equals("WT")) {
            int fractionWidth = tableModel.getColumnFractionDigits(modelIndex);
            ZeroWtRenderer zWtRenderer = null;
            if (fractionWidth >= 0) {
              if (fractionWidth > MAX_FRACTION_ELEMENTS - 1 ) fractionWidth = MAX_FRACTION_ELEMENTS - 1;
              String pattern = getNumberFormatPattern(fractionWidth);
              tcer.tcEditor = new FormattedNumberEditor_FW(pattern);
              zWtRenderer = new ZeroWtRenderer(pattern);
            }
            else {
               tcer.tcEditor = new FormattedNumberEditor_FW(PATTERN9);
               zWtRenderer = new ZeroWtRenderer(PATTERN9);
            }
            tcer.tcRenderer = zWtRenderer;
            zWtRenderer.setZeroWtColor(Color.lightGray);
            setValues = true;
          }
          else if (colName.equals("IWT")) {
            tcer.tcEditor = new ComboBoxCellEditor("IWT", new String [] {"0","1","2","3","4"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("Q", new String [] {"0","1","2","3","4"});
            setValues = true;
          }
          return setValues;
    }
}

package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.io.*;
import org.trinet.jdbc.*;
import org.trinet.util.graphics.*;

public class SaveTablePanelToFile implements ActionListener {
    // TODO add listeners for state change on button components
    // then remove them as member attributes.
    ButtonGroup group ; //= new ButtonGroup();
    JDialog aDialog;
    /*
    JRadioButton xmlDelimited;
    JRadioButton tabDelimited;
    JRadioButton commaDelimited;
    JRadioButton spaceDelimited;
    */
    JCheckBox includeHeaderRow;
    JCheckBox onlySelectedRows;
    JCheckBox appendToFile;
    JCheckBox tableModelColumns;

    Frame parentFrame;
    int [] tableRows;
    String delimiter;
    boolean useDialog = true;
    boolean canceled = false;
    boolean includeHeader = false;
    boolean allColumns = false;
    boolean onlySelected = true;
    boolean append = true;
    AbstractJasiListPanel activePanel;
    String fileNameString;
    JFileChooser fChooser;

    // hidden? order? of columns - use table/model values?
    public boolean save(AbstractJasiListPanel listPanel, Frame aFrame) {
        return save(listPanel, aFrame, true);
    }
    public boolean save(AbstractJasiListPanel listPanel,
                    Frame aFrame, boolean useDialog) {
        parentFrame = aFrame;
        activePanel = listPanel;
        this.useDialog = useDialog;
        JTable listTable = activePanel.getTable();
        if (listTable == null || listTable.getColumnCount() < 1) {
          System.err.println(activePanel.getPanelName() +
              " Input table has no columns to save");
          return false;
        }
        BufferedWriter bw = null;
        if (useDialog) {
          showDialog();
          if (canceled) return true; // ok
          processDialogOptions();
          if (onlySelected && listTable.getSelectedRowCount() == 0) {
            System.err.println(activePanel.getPanelName() +
                " No rows SELECTED for table save selected operation");
            Toolkit.getDefaultToolkit().beep();
            return false;
          }
        }
        System.out.println("SaveTablePanelToFile b4 createFileWriter");
        bw = createFileWriter(getSaveFileName(useDialog));
        if (bw == null) {
          System.err.println("SaveTablePanelToFile failed creating file writer");
          return false;
        }
        tableRows = getTableRows(activePanel, onlySelected);
        if (tableRows.length < 1) {
          System.err.println(activePanel.getPanelName() +
              " Table has no data for save operation");
          return false;
        }
        // use fraction digits to format numbers?
        if (writeData(bw, activePanel)) return true;
        else {
          System.err.println(activePanel.getPanelName() +
              " Table write operation FAILURE");
          return false;
        }
    }
    protected void showDialog() {
        createDialog();
        aDialog.setVisible(true);
    }
    private void createDialog() {
        if (aDialog == null)  {
          aDialog = new JDialog(parentFrame, "Table to ASCII file", true);
          Container contentPane = aDialog.getContentPane();
          contentPane.setLayout(new BorderLayout(5, 5));
          contentPane.add(createOptionsPanel(), BorderLayout.CENTER);
          contentPane.add(createControlButtonPanel(), BorderLayout.SOUTH);
          aDialog.pack();
        }
    }
    private void createButtonGroup(JPanel optionsPanel) {
        JRadioButton xmlDelimited = new JRadioButton("XML");
        xmlDelimited.setEnabled(false);
        xmlDelimited.setActionCommand("xml");
        JRadioButton tabDelimited = new JRadioButton("Tab");
        tabDelimited.setActionCommand("\t");
        JRadioButton commaDelimited = new JRadioButton("Comma");
        commaDelimited.setActionCommand(",");
        commaDelimited.setSelected(true);
        JRadioButton spaceDelimited = new JRadioButton("Space");
        spaceDelimited.setActionCommand(" ");
        group = new ButtonGroup();
        group.add(spaceDelimited);
        group.add(tabDelimited);
        group.add(commaDelimited);
        group.add(xmlDelimited);
        JPanel groupPanel = new JPanel();
        groupPanel.setLayout(new BoxLayout(groupPanel,BoxLayout.Y_AXIS));
        groupPanel.add(new JLabel("Column delimiter"));
        groupPanel.add(spaceDelimited);
        groupPanel.add(tabDelimited);
        groupPanel.add(commaDelimited);
        groupPanel.add(xmlDelimited);
        optionsPanel.add(groupPanel);
    }
    public void actionPerformed(ActionEvent event) {
        canceled = event.getActionCommand().equalsIgnoreCase("Cancel");
        aDialog.setVisible(false);
    }
    private JPanel createControlButtonPanel() {
        JButton OKButton = new JButton("Save");
        OKButton.setActionCommand("Save");
        OKButton.addActionListener(this);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(this);
        JPanel controlButtonPanel = new JPanel();
        controlButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        controlButtonPanel.add(OKButton);
        controlButtonPanel.add(cancelButton);
        return controlButtonPanel;
    }
    protected JPanel createOptionsPanel() {
        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new BoxLayout(optionsPanel,BoxLayout.Y_AXIS));
        optionsPanel.setBorder(new javax.swing.border.LineBorder(Color.black, 1));
        createButtonGroup(optionsPanel);
        includeHeaderRow = new JCheckBox("Include header row");
        includeHeaderRow.setSelected(includeHeader);
        optionsPanel.add(includeHeaderRow);
        onlySelectedRows = new JCheckBox("Selected Rows");
        onlySelectedRows.setSelected(onlySelected);
        optionsPanel.add(onlySelectedRows);
        tableModelColumns = new JCheckBox("All columns");
        tableModelColumns.setSelected(allColumns);
        optionsPanel.add(tableModelColumns);
        appendToFile = new JCheckBox("Append to file");
        appendToFile.setSelected(append);
        optionsPanel.add(appendToFile);
        return optionsPanel;
    }
    private String getDelimiterOption() {
        ButtonModel bm = group.getSelection();
        return (bm == null) ? " " : bm.getActionCommand();
    }
    protected void processDialogOptions() {
        delimiter = getDelimiterOption();
        onlySelected = onlySelectedRows.isSelected();
        includeHeader = includeHeaderRow.isSelected();
        allColumns = tableModelColumns.isSelected();
        append = appendToFile.isSelected();
    }
    protected int [] getTableRows(AbstractJasiListPanel listPanel, boolean useSelected) {
        JTable listTable = listPanel.getTable();
        int rowCount = listTable.getRowCount();
        int [] rowNumbers  = null;
        if (useSelected) { 
          rowNumbers = listTable.getSelectedRows();
          rowCount = rowNumbers.length;
        }
        else {
          rowNumbers = new int[rowCount];
          for(int ii = 0; ii < rowCount; ii++) rowNumbers[ii] = ii;
        }
        return rowNumbers;
    }

    protected String getSaveFileName(boolean useDialog) {
      if (useDialog) {
        if (fChooser == null) fChooser = new JFileChooser();
        fChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        fChooser.setDialogTitle( "Save content to file");
        if (fileNameString != null)
            fChooser.setSelectedFile(new File(fileNameString));
        if (JFileChooser.APPROVE_OPTION == fChooser.showSaveDialog(parentFrame)) {
          fileNameString = fChooser.getCurrentDirectory().getAbsolutePath() +
              File.separator +
              fChooser.getName(fChooser.getSelectedFile());
        }
        else {
          System.out.println(activePanel.getPanelName() +
              "No file name selected, canceling save");
          Toolkit.getDefaultToolkit().beep();
          canceled = true;
          fileNameString = null;
        }
      }
      return fileNameString;
    }
    protected BufferedWriter createFileWriter(String fileName) {
        if (fileName == null) return null;
        BufferedWriter bw = null;
        try {
          bw = new BufferedWriter(new FileWriter(fileName, append));
          System.out.println(activePanel.getPanelName() +
              " Saving table to file: "+fileName);
        }
        catch(IOException ex) {
          ex.printStackTrace();
          return null;
        }
        return bw;
    }
    protected boolean writeData(BufferedWriter writer, AbstractJasiListPanel aPanel) {
        boolean status = true;
        try {
          JTable keyTable = aPanel.getTable().getRowHeader();
          JTable nkTable = aPanel.getTable();
          if (delimiter.equalsIgnoreCase("xml")) { return status;}
          else {
            if (includeHeader) {
              writer.write(aPanel.panelName+delimiter);
              writeTableHeader(writer, keyTable, false);
              writeTableHeader(writer, nkTable, true);
            }
            for (int ii = 0; ii < tableRows.length; ii++) {
              writer.write(aPanel.panelName+delimiter);
              writeTableData(writer, keyTable, ii, false);
              writeTableData(writer, nkTable, ii, true);
            }
          }
        }
        catch(IOException ex) {
          status = false;
          ex.printStackTrace();
        }
        finally {
          try {
            writer.close();
          }
          catch (IOException ex2) { ex2.printStackTrace();}
        }
        return status;
    }
    protected void writeTableHeader(BufferedWriter writer, JTable aTable,
            boolean terminate) throws IOException {
            TableModel tableModel = aTable.getModel();
            int columnCount = (allColumns) ?
                    tableModel.getColumnCount() : aTable.getColumnCount();
            if (columnCount == 0) return;
            int izero = (allColumns) ? 0 : aTable.convertColumnIndexToModel(0);
            writer.write(tableModel.getColumnName(izero));
            for (int ii = 1; ii < columnCount; ii++) {
              int jj = (allColumns) ? ii : aTable.convertColumnIndexToModel(ii);
              writer.write(delimiter+tableModel.getColumnName(jj));
            }
            if (terminate) writer.newLine();
            else writer.write(delimiter);
    }
    protected void writeTableData(BufferedWriter writer, JTable aTable,
                    int irow, boolean terminate) throws IOException {
        if (tableRows.length > 0) {
            TableModel tableModel = aTable.getModel();
            int columnCount = (allColumns) ?
                    tableModel.getColumnCount() : aTable.getColumnCount();
            if (columnCount == 0) return;
            int izero = (allColumns) ? 0 : aTable.convertColumnIndexToModel(0);
            writer.write(formatValueString(irow,izero));
            for (int jj = 1; jj < columnCount; jj++) {
                int kk = (allColumns) ? jj : aTable.convertColumnIndexToModel(jj);
                writer.write(delimiter + formatValueString(irow, kk));
            }
            if (terminate) writer.newLine();
            else writer.write(delimiter);
        }
    }

    public String formatValueString(int irow, int icol) {
        TableModel tableModel = activePanel.getModel();
        return StringSQL.valueOf(
                  tableModel.getValueAt(activePanel.sortedToModelRowIndex(tableRows[irow]), icol)
               );
    }
}

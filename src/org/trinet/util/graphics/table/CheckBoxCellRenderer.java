package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

public class CheckBoxCellRenderer extends JCheckBox implements TableCellRenderer, ColorableRendererIF {
    protected Color defaultBackground = getBackground();
    protected Color defaultForeground = getForeground();
    //protected Color focusBackground = getBackground();
    private Border focusBorder = BorderFactory.createLineBorder(Color.black,2);
    private Border noFocusBorder = BorderFactory.createEmptyBorder(1,1,1,1);

    public CheckBoxCellRenderer (Object value) {
        super();
        setHorizontalAlignment(SwingConstants.CENTER);
        setSelected(Boolean.valueOf(value.toString()).booleanValue());
        setBorderPainted(true);
        setBorderPaintedFlat(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {

          this.setSelected(Boolean.valueOf(value.toString()).booleanValue());

          if (hasFocus) {
             setBorder(focusBorder);
          }
          else setBorder(noFocusBorder);

          if (isSelected) {
             setBackground(table.getSelectionBackground());
             setForeground(table.getSelectionForeground());
          }
          else  {
             setForeground(table.getForeground());
             setBackground(defaultBackground);
          }
          return this;
    }

    public void setDefaultForeground(Color c) {
        defaultForeground = c;
        super.setForeground(c);
    }
    public void setDefaultBackground(Color c) {
        defaultBackground = c;
        super.setBackground(c);
    }
}

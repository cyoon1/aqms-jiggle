package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/** Define constants used by a AmpListTableModel. These are the columns in a
 *  JTable that contains a AmpList. */
public interface JasiAmpListTableConstants {
    public static final int MAX_FIELDS   = 38;
    public static final int NET          = 0;
    public static final int STA          = 1;
    public static final int CHN          = 2;
    public static final int AU           = 3;
    public static final int SUBSRC       = 4;
    public static final int CHNSRC       = 5;
    public static final int SEED         = 6;
    public static final int LOC          = 7;
    public static final int PHASE        = 8;
    public static final int QUALITY      = 9;
    public static final int DATETIME     = 10;
    public static final int RESIDUAL     = 11;
    public static final int DISTANCE     = 12;
    public static final int AZIMUTH      = 13;
    public static final int WEIGHT       = 14;
    public static final int AUTH         = 15;
    public static final int SOURCE       = 16;
    public static final int COMMENT      = 17;
    public static final int PROCESSING   = 18;
    public static final int DELETE       = 19;
    public static final int WTIME  = 20;
    public static final int WDUR   = 21;
    public static final int VALUE  = 22;
    public static final int UNITS  = 23;
    public static final int TYPE   = 24;
    public static final int HALF   = 25;
    public static final int ERR    = 26;
    public static final int PER    = 27;
    public static final int SNR    = 28;
    public static final int CLP    = 29;
    public static final int CMAG   = 30;
    public static final int MCORR  = 31;
    public static final int MAG    = 32;
    public static final int MAGID  = 33;
    public static final int ORID   = 34;
    public static final int AMPID  = 35;
    public static final int EVID   = 36;
    public static final int IN_WGT   = 37;

/* Remember to match class with tableModel.getValue return type */
    public static final Class [] columnClasses = {
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataDouble.class, DataDouble.class, DataDouble.class, String.class, String.class,
        Boolean.class, DataDouble.class, DataDouble.class, DataDouble.class, DataLong.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataLong.class, DataLong.class,
        DataLong.class, DataLong.class, DataLong.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 2,
        2, 2, 1, 1, 2,
        0, 0, 0, 0, 0,
        2, 2, 3, 0, 0,
        0, 0, 2, 2, 0,
        2, 2, 2, 0, 0,
        0, 0, 0
    };

    public static final String [] columnNames = {
    "NET","STA","CHAN","AU","SSRC",
    "CSRC","SEED","LOC","PH","Q",
    "DATETIME","RES","DIST","AZ","WT",
    "AUTH","SRC","RMK","PFLG","DFLG",
    "WTIME","WDUR","VALUE","UNITS","TYPE",
    "HALF","ERR","PER","SNR","CLP",
    "CMAG","MCORR","MAG","MAGID", "ORID",
    "AMPID","EVID","IWT"
    };

    public static final boolean [] columnAKey = {
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        true, false, false
    };

    public static final boolean [] columnEditable = {
        true, true, true, true, true,
        true, true, true, true, false,
        true, false, false, false, false,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        false, false, false, false, false,
        false, false, true
    };

    public static final boolean [] columnNullable = {
        false, false, false, false, false,
        false, false, false, false, true,
        false, true, true, true, true,
        false, false, true, false, false,
        false, false, false, false, false,
        false, true, true, true, false,
        true, true, true, true, true,
        false, true, true
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true
    };

    //pixels to add for combo editors etc.
    public static final int [] cellWidthPadding = {
        0, 0, 0, 0, 0,
        0, 0, 0, 10, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 30, 20,
        0, 0, 0, 0, 10,
        0, 0, 0, 0, 0,
        0, 0, 10
    };
}

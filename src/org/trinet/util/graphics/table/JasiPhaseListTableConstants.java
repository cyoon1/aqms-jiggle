package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/** Define constants used by a PhaseListTableModel. These are the columns in a
 *  JTable that contains a PhaseList. */
public interface JasiPhaseListTableConstants {
    public static final int MAX_FIELDS   = 29;
    public static final int NET          = 0;
    public static final int STA          = 1;
    public static final int CHN          = 2;
    public static final int AU           = 3;
    public static final int SUBSRC       = 4;
    public static final int CHNSRC       = 5;
    public static final int SEED         = 6;
    public static final int LOC          = 7;
    public static final int PHASE        = 8;
    public static final int EI           = 9;
    public static final int FM           = 10;
    public static final int QUALITY      = 11;
    public static final int DATETIME     = 12;
    public static final int RESIDUAL     = 13;
    public static final int DELAY        = 14;
    public static final int DISTANCE     = 15;
    public static final int AZIMUTH      = 16;
    public static final int EMA          = 17;
    public static final int WEIGHT       = 18;
    public static final int IN_WGT       = 19;
    public static final int IMPORTANCE   = 20;
    public static final int AUTH         = 21;
    public static final int SOURCE       = 22;
    public static final int COMMENT      = 23;
    public static final int PROCESSING   = 24;
    public static final int DELETE       = 25;
    public static final int ORID         = 26;
    public static final int ARID         = 27;
    public static final int EVID         = 28;

// End of member identifier constants
    public static final Class [] columnClasses = {
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataLong.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataLong.class, DataLong.class, DataLong.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 2, 2, 2,
        1, 1, 1, 2, 2,
        3, 0, 0, 0, 0,
        0, 0, 0, 0
    };

    public static final String [] columnNames = {
    "NET","STA","CHAN","AU","SSRC",
    "CSRC","SEED","LOC","PH","EI",
    "FM","Q","DATETIME","RES","DELAY",
    "DIST","AZ","EMA","WT","IWT",
    "IMP", "AUTH", "PSRC","RMK","PFLG",
    "DFLG","ORID", "ARID","EVID"
    };

    public static final boolean [] columnAKey = {
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, true, false
    };

    public static final boolean [] columnEditable = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, false, false,
        false, false, false, false, true,
        false, true, true, true, true,
        true, false, false, false
    };

    public static final boolean [] columnNullable = {
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, true, true,
        true, true, true, true, true,
        true, false, false, true, false,
        false, true, false, true
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true
    };

    public static final int [] cellWidthPadding = {
        0, 0, 0, 0, 0,
        0, 0, 0, 10, 10,
        30, 10, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0
    };
}

package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Model represents a list of jasi Magnitude objects in a JTable. */
public class JasiMagListTableModel extends AbstractJasiAssociationTableModel
    implements JasiMagListTableConstants {

    // If a magnitude is removed, removes its assoc readings from Solution too?
    protected boolean deleteMagCullsDataFromSolList = false; // menu/property setting ?

    public JasiMagListTableModel() {
        super();
        maxFields = JasiMagListTableConstants.MAX_FIELDS;
        deleteColumnIndex = JasiMagListTableConstants.DELETE;
        tableColumnNames = JasiMagListTableConstants.columnNames; 
        tableColumnClasses = JasiMagListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiMagListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiMagListTableConstants.columnAKey;
        tableColumnEditable = JasiMagListTableConstants.columnEditable;
        tableColumnNullable = JasiMagListTableConstants.columnNullable;
        tableColumnShown = JasiMagListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiMagListTableConstants.cellWidthPadding;
    }

    public JasiMagListTableModel(MagList magList) {
        this(magList, false);
    }

    public JasiMagListTableModel(Solution sol, boolean isCellEditable) {
        this(sol.getMagList(), isCellEditable);
    }

    public JasiMagListTableModel(MagList magList, boolean isCellEditable) {
        this();
        setList(magList);
        setCellEditable(isCellEditable);
    }

    protected void setDeleteMagCullsDataFromSolList(boolean tf) { // could set from panel property/menu?
      deleteMagCullsDataFromSolList = tf;
    }

    protected Magnitude getMagnitude(int rowIndex) {
        return (Magnitude) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Magnitude mag = getMagnitude(rowIndex);
        switch (colIndex) {
            case VALUE:
                return mag.value;
            case SUBSCRIPT:
                return mag.subScript;
            case AUTH:
                return mag.authority;
            case SOURCE:
                return mag.source;
            case METHOD:
                return mag.algorithm;
            case USED_STATIONS:
                // needs subtype amp/coda access equivalence implemented in Magnitude/CodaList
                return new DataLong(mag.getStationsUsed());
            case USED_CHNLS:
                // needs subtype amp/coda access equivalence implemented in Magnitude/CodaList
                return new DataLong(mag.getReadingsUsed());
            case READINGS:
                // needs subtype amp/coda access equivalence implemented in Magnitude/CodaList
                return new DataLong(mag.getReadingsCount());
            case ERROR:
                return mag.error;
            case GAP:
                return mag.gap;
            case DISTANCE:
                return mag.distance;
            case QUALITY:
                return mag.quality;
            case PROCESSING:
                return mag.processingState;
            case DELETE:
                return (mag.isDeleted()) ? Boolean.TRUE : Boolean.FALSE;
            case PREFERRED:
                return (mag.isPreferred()) ? Boolean.TRUE : Boolean.FALSE;
            case TYPEPREF:
                return (mag.isPreferredOfType()) ? Boolean.TRUE : Boolean.FALSE;
            case STALE:
                return (mag.isStale()) ? Boolean.TRUE : Boolean.FALSE;
            case EVID:
                return (sol == null) ? NULL_DATALONG : sol.getId();
            case ORID:
                long orid = 0l;
                if (mag instanceof DbOriginAssocIF) {
                  orid = ((DbOriginAssocIF)mag).getOridValue();
                }
                return (orid > 0) ?  new DataLong(orid) : NULL_DATALONG;
            case MAGID:
                return mag.magid;
            case REMARK: // not defined in magnitude 
                return NULL_DATASTRING;
            default:
                return "";
        }
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always? what about validCase == false?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Magnitude mag = getMagnitude(rowIndex);
        boolean validCase = true;
        switch (colIndex) {
            case VALUE:
                mag.value.setValue(value);
                break;
            case SUBSCRIPT:
                mag.subScript.setValue(value);
                setValueAt("HAND", rowIndex, METHOD); // mag.algorithm.setValue("HAND"); // added 05/29/2007 -aww
                if (sol != null) 
                    setValueAt("true", rowIndex, TYPEPREF); // sol.setPrefMagOfType(mag); // added 05/29/2007 -aww
                break;
            case AUTH:
                mag.authority.setValue(value);
                break;
            case SOURCE:
                mag.source.setValue(value);
                break;
            case METHOD:
                mag.algorithm.setValue(value);
                break;
            case USED_STATIONS:
                // Magnitude prompts reading list for weighted elements, thus can't set this.
                validCase = false;
                break;
            case USED_CHNLS:
                // Magnitude prompts reading list for weighted elements, thus can't set this.
                validCase = false;
                break;
            case READINGS:
                // Magnitude prompts reading list for weighted elements, thus can't set this.
                validCase = false;
                break;
            case ERROR:
                mag.error.setValue(value);
                break;
            case GAP:
                mag.gap.setValue(value);
                break;
            case DISTANCE:
                mag.distance.setValue(value);
                break;
            case QUALITY:
                mag.quality.setValue(value);
                break;
            case PROCESSING:
                mag.processingState.setValue(value);
                break;
            case DELETE:
               if (Boolean.valueOf(value.toString()).booleanValue()) {
                 if (! mag.isPreferred()) mag.setDeleteFlag(true);
                 else System.err.println(
                        "ERROR: Preferred Magnitude cannot be deleted."
                        );
               }
               else {
                 mag.undelete();
                 //mag.assign(sol); // updates orid
                 mag.sol = sol; // doesn't update orid
                 // if prefmag undeletable, shouldn't need to ever set here:
                 //if (mag.isPreferred()) sol.setPreferredMagnitude(mag);
                 // mag should already be in MagList list so likely no-op:
                 //else sol.associate(mag);
               }
               fireTableCellUpdated(rowIndex, EVID);
               fireTableCellUpdated(rowIndex, ORID);
               break;
            case STALE:
                mag.setStale(Boolean.valueOf(value.toString()).booleanValue());
                break;
            case PREFERRED:
                validCase = false;
                if (sol != null) {
                  if (Boolean.valueOf(value.toString()).booleanValue()) {
                     Magnitude prefmag = sol.getPreferredMagnitude();
                     if (prefmag != mag) {
                       int prefIndex = -1;
                       if (prefmag != null) {
                         prefIndex = getList().indexOf(prefmag);
                       }
                       sol.setPreferredMagnitude(mag);
                       if (prefIndex > -1)  // reset prior prefmag cell's checkbox
                         fireTableCellUpdated(prefIndex, colIndex);
                     }
                  }
                }
                break;
            case EVID:
                validCase = false;
                // don't do this, domino effects all
                //if (sol != null) sol.setId(((DataLong) value).longValue());
                break;
            case ORID:
                validCase = false;
                // don't do this, domino effects all
                break;
            case MAGID:
                validCase = false;
                break;
            case REMARK:
                validCase = false;
                break;
            case TYPEPREF:
                validCase = false;
                if ( sol != null ) {
                  if ( Boolean.valueOf(value.toString()).booleanValue() ) {
                    Magnitude prefmag = sol.getPrefMagOfType(mag.getTypeSubString());
                    int prefIndex = -1;
                    if ( prefmag != mag ) {
                      if (prefmag != null) prefIndex = getList().indexOf(prefmag);
                      sol.setPrefMagOfType(mag);
                      if (prefIndex > -1) fireTableCellUpdated(prefIndex, colIndex);
                    }
                  }
                  else {
                    sol.removePrefMagOfType(mag);
                  }
                  System.out.println("DEBUG JasiMagListTableModel prefMagMap:\n"+sol.prefMagMapString());
                }
                break;
            default:
               validCase = false;
        }
        fireTableCellUpdated(rowIndex, colIndex);
        if (validCase) {
            mag.setNeedsCommit(true); // for valid changes
            //if (colIndex != PROCESSING && !mag.isHuman() ) // revert final?
            //if (mag.processingState.toString().equals(STATE_AUTO_TAG))
            if (mag.isAuto()) setValueAt(JasiProcessingConstants.STATE_HUMAN_TAG, rowIndex, PROCESSING);
        }
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        Magnitude newMag = (Magnitude) super.initRowValues(aRow);
        newMag.setUniqueId();
        // not null: magnitude value and type
        newMag.subScript.setValue("x");
        newMag.value.setValue(0.0);
        // Force first one entered to be set the preferred,
        // otherwise user must set amongst choices
        Magnitude prefmag = this.sol.getPreferredMagnitude();
        if (prefmag == null || prefmag.hasNullValue())
            this.sol.setPreferredMagnitude(newMag); // 3/25/2005 -aww
        return newMag;
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF aRow) {
       if (aRow == null) return false;
       if (sol != null) {
         Magnitude mag = (Magnitude) aRow;
         if (getRowCount() < 1) sol.setPreferredMagnitude(mag);  // aww 12/06/2002
         else {
             mag.assign(sol); // selected solution 
             sol.setPrefMagOfType(mag); // test here, not sure about this here ? 03/21/2007 - aww
         }
       }
       return super.insertRow(rowIndex, aRow);
    }


    /** Override tests for preferred Magnitude and 
     * delegates to associated solution.
     */
    public boolean deleteRow(int rowIndex) {
        Magnitude oldMag = getMagnitude(rowIndex);
        // Test for preferred, delete it or not?
        if (oldMag == null || oldMag.isPreferred()) return false;
        boolean retVal = super.deleteRow(rowIndex);
        // property test says totally remove row, then readings from sol too?
        if (retVal && deleteRemovesRowFromList && deleteMagCullsDataFromSolList) { 
            Solution aSol = oldMag.getAssociatedSolution();
            // unassociate mag from solution, removing its assoc readings:
            if (aSol != null) aSol.erase(oldMag);
        }
        if (retVal) {
          if (logChanges) aLog.logTextnl("deleted "+oldMag.toNeatString());
          //fireTableRowsDeleted(rowIndex, rowIndex);
        }
        return retVal;
    }

    public boolean copyRowAt(int rowIndex, boolean after) {
      if (rowIndex < 0 || rowIndex > getRowCount()-1) return false;
      boolean retVal = false;
      Magnitude jcRow = (Magnitude) modelList.get(rowIndex);
      Magnitude newRow = null;
      try {
        newRow =
          (Magnitude) JasiObject.newInstance(jcRow.getClass().getName());
        initRowValues(newRow);
        newRow.assign(jcRow.getAssociatedSolution()); //no list append 
        if (after) rowIndex++;
        modelList.add(rowIndex, newRow);
        if (logChanges) aLog.logTextnl("copied " + jcRow.toString());
        //fireTableRowsInserted(rowIndex, rowIndex);
        retVal = true;
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      return retVal;
    }

    /* create boolean checkbox column in tablemodel to flag alternate magnitude commit?
    public int updateDB(int index) {
      Magnitude oldMag = getMagnitude(rowIndex);
      return (oldMag.isPreferred() || getValueAt(index, COMMIT) == Boolean.TRUE) ?
          super.updateDB(index) : 0;
    }
    */
}

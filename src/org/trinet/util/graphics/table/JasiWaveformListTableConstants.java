package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/**
  * Define constants used by a WaveformListTableModel.
  * These are the columns in a JTable that contains a WaveformList.
  * */

public interface JasiWaveformListTableConstants {
    public static final int MAX_FIELDS   = 9;
    public static final int NET          = 0;
    public static final int STA          = 1;
    public static final int CHN          = 2;
    public static final int AU           = 3;
    public static final int SUBSRC       = 4;
    public static final int CHNSRC       = 5;
    public static final int SEED         = 6;
    public static final int LOC          = 7;
    public static final int WFID         = 8;

// End of member identifier constants
    public static final Class [] columnClasses = {
        DataString.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataLong.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0
    };

    public static final String [] columnNames = {
    "NET","STA","CHAN","AU","SSRC",
    "CSRC","SEED","LOC","ID"
    };

    public static final boolean [] columnAKey = {
        false, false, false, false, false,
        false, false, false, true 
    };

    public static final boolean [] columnEditable = {
        true, true, true, true, true,
        true, true, true, false 
    };

    public static final boolean [] columnNullable = {
        false, false, false, false, false,
        false, false, false, false 
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true
    };

    public static final int [] cellWidthPadding = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0
    };
}

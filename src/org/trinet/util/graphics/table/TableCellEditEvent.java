package org.trinet.util.graphics.table;
import javax.swing.event.ChangeEvent;

public class TableCellEditEvent extends ChangeEvent {
    public TableCellEditEvent (Object source) { super(source);}
}

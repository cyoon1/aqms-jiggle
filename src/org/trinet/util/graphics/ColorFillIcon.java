package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*; 

/**
 * Creates a square icon of the given color and size with a black border.
 * Based on an example in : Core Java, by Kim Topley, pg. 262
 */
public class ColorFillIcon implements Icon
{

public static final int BORDER_SIZE = 1;
public static final int DEFAULT_SIZE = 32;

// Icon state
protected int width;
protected int height;
protected int borderSize;
protected int fillHeight;
protected int fillWidth;

protected Color fillColor;
protected Color shadow;

public ColorFillIcon
  ( Color fill,
    int width,       
    int height, 
    int borderSize)
{
    this.fillColor = fill;
    this.borderSize = borderSize;
    this.width = width;
    this.height = height;

    this.shadow = Color.black;
    this.fillWidth  = width  - 2 * borderSize;
    this.fillHeight = height - 2 * borderSize;
}

public ColorFillIcon (Color fill, int size)
{
    this(fill, size, size, BORDER_SIZE);
}

public ColorFillIcon (Color fill)
{
    this(fill, DEFAULT_SIZE, DEFAULT_SIZE, BORDER_SIZE);
}

public void setShadow (Color c)
{
    shadow = c;
}

public void setFillColor (Color c)
{
    fillColor = c;
}

public int getIconWidth()
{
    return width;
}

public int getIconHeight()
{
    return height;
}

public void paintIcon ( Component comp, Graphics g, int x, int y)
{
    Color savedColor = g.getColor();   // remember current graphics color so we can set it back later

    if (borderSize > 0)	    // draw the border
    {
      g.setColor(shadow);
      for (int i = 0; i < borderSize; i++)  // draw enough rects for border thickness
      {
	g.drawRect (x+i, y+i, width -2*i-1, height-2*i-1);
      }
    }

    // fill with color
    g.setColor(fillColor);
    g.fillRect(x+borderSize, y+borderSize, fillWidth, fillHeight);

    g.setColor(savedColor);	// reset graphics drawing color

}

} // end of class


package org.trinet.util.graphics;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PasswordDialog {
    private static final int DEFAULT_FIELD_LENGTH = 16;
    private String password ;
    private String dialogTitle;
    private int fieldLength;

    private JComponent passwordComponent;
    private JPasswordField passwordField;

    public PasswordDialog(String password) {
	this(password, "Password", DEFAULT_FIELD_LENGTH);
    }

    public PasswordDialog(String password, String dialogTitle) {
	this(password, dialogTitle, DEFAULT_FIELD_LENGTH);
    }

    public PasswordDialog(String password, String dialogTitle, int fieldLength) {
	this.password = password;
	this.dialogTitle = dialogTitle;
	this.fieldLength = fieldLength;
	makePasswordComponent();
    }

    private void makePasswordComponent() {
	passwordComponent = new JPanel();
        passwordComponent.setLayout(new GridLayout(2,2));
	passwordField = new JPasswordField(fieldLength);
	passwordField.addActionListener(new PasswordFieldActionListener());
        JTextField userField = new JTextField(32);
        userField.requestFocus();
        userField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
	          passwordField.requestFocus();
                }
        });
	passwordComponent.add(new JLabel("User:"));
	passwordComponent.add(userField);
	passwordComponent.add(new JLabel("Password:"));
	passwordComponent.add(passwordField);
    }

    public boolean checkPassword() {
	return checkPassword(null);
    }

    public boolean checkPassword(Component parentComponent) {
//	JOptionPane.showMessageDialog(parentComponent, passwordComponent, dialogTitle, JOptionPane.PLAIN_MESSAGE);
	int status = JOptionPane.showConfirmDialog(parentComponent, passwordComponent, dialogTitle,
		 JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	if (status == JOptionPane.CANCEL_OPTION) return false;
	String inputText = String.valueOf(passwordField.getPassword());
	passwordField.setText("");
	if (inputText.equals(password)) { return true; }
	else {
	    JOptionPane.showMessageDialog(parentComponent, "Wrong password entered: " + inputText, "Password Error", JOptionPane.ERROR_MESSAGE);	
	    return false;
	}
    }

    private class PasswordFieldActionListener implements ActionListener {
	public void actionPerformed(ActionEvent evt) {
	    if (String.valueOf(passwordField.getPassword()).equals("")) return;
	    else {
		((JDialog) passwordField.getRootPane().getParent()).dispose();  // generates a JOptionPane.CLOSED_OPTION result.
	    }
	}
    }
/*
    static public final class Tester {
      public static final void main(String [] args)  {
	PasswordDialog pwd2 = new PasswordDialog("TEST", "TEST PASSWORD", 4);
	System.out.println("checkPassword : " + pwd2.checkPassword());
	JFrame jf = new JFrame();
	jf.setBounds(200,200,400,300);
	jf.setVisible(true);
	System.out.println("checkPassword in Frame notnull: " + pwd2.checkPassword(jf));
	System.exit(0);
      } // end of main
  } // end of Tester
*/
} // end of PasswordDialog class

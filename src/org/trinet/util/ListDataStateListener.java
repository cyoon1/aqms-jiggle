package org.trinet.util;
import java.util.*;
//import java.io.Serializable;
public interface ListDataStateListener extends EventListener, java.io.Serializable {
  public void intervalAdded(ListDataStateEvent e);
  public void intervalRemoved(ListDataStateEvent e);
  public void contentsChanged(ListDataStateEvent e);
  public void orderChanged(ListDataStateEvent e);
  public void stateChanged(ListDataStateEvent e);
}

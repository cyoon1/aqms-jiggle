package org.trinet.util;
import org.trinet.filters.FilterTypes;
import org.trinet.jasi.*;

/** Butterworth filter for waveform timeseries.
* Only works with 20, 25, 40, 50, 80, 100, 200, 250 and 500 sps data.
* Filter coefficients were calculated for 4 poles.
* Filter choices are highpass at 1 Hz corner,
* lowpass at a 8 Hz corner, 
* a very lowpass with at a 1 Hz corner,
* bandpass with 0.5 Hz and 10 Hz corners,
* or narrow bandpass with 0.5 Hz and 5 Hz corners.
* You may choose to reverse filter to eliminate phase shift.
* A cosine taper is applied. */
public class GeneralButterworthFilter implements FloatFilterIF, WaveformFilterIF, Cloneable {
    // 4th order high,lowpass,verylowpass; 2nd order bandpass 0.5,10 and narrowbandpass 0.5,5 Hz
    public static final int NZEROS = 4; // 4th order
    public static final int NPOLES = 4;

    public static final double cosineTaperAlpha = 0.05;
    public static final int defaultSps = 100;

    /** Descriptive string */
    private static final String BUTTERWORTH = "Butterworth";

    private int type = FilterTypes.HIGHPASS;
    private boolean reverse = false;
    private int sampleRate = defaultSps;

    private double lf = 1.;
    private double hf = 0.;

    // default to values for Highpass corner at 1 Hz for 100 sps
    protected String description = BUTTERWORTH;
    private double  gain  =  gainH100;
    private double  coef1 = -0.8485559993;
    private double  coef2 =  3.5335352195;
    private double  coef3 = -5.5208191366;
    private double  coef4 =  3.8358255406;

    private double [] xv = new double [NZEROS+1];
    private double [] yv = new double [NPOLES+1];

    // HIGHPASS corner at 1Hz
    private static final double  gainH500  = 1.016554412; // gain 500 sps
    private static final double  gainH250  = 1.033383657;
    private static final double  gainH200  = 1.041903016;
    private static final double  gainH100  = 1.085574782;
    private static final double  gainH80   = 1.108101438;
    private static final double  gainH50   = 1.178584698;
    private static final double  gainH40   = 1.228117167;
    private static final double  gainH25   = 1.390125419;
    private static final double  gainH20   = 1.510537881;

    // VERY_LOWPASS corner at 1Hz
    private static final double  gainVL500  = 6.522112693e+08; // gain 500 sps
    private static final double  gainVL250  = 4.143150297e+07;
    private static final double  gainVL200  = 1.710822297e+07;
    private static final double  gainVL100  = 1.112983215e+06;
    private static final double  gainVL80   = 4.649932749e+05; 
    private static final double  gainVL50   = 7.522343863e+04;
    private static final double  gainVL40   = 3.201129162e+04;
    private static final double  gainVL25   = 5.458037903e+03;
    private static final double  gainVL20   = 2.400388646e+03;

    // LOWPASS corner at 8 Hz
    private static final double gainL500 = 1.78044852e+05; // gain 500 sps
    private static final double gainL250 = 1.25675248e+04;
    private static final double gainL200 = 5.45803790e+03;
    private static final double gainL100 = 4.47448975e+02;
    private static final double gainL80 = 2.07282095e+02;
    private static final double gainL50 = 4.37250073e+01;
    private static final double gainL40 = 2.14671018e+01;
    private static final double gainL25 = 4.86346964e+00;
    private static final double gainL20 = 2.31028705e+00;

    /* EXTREMELY_LOWPASS at corner 0.1 Hz
    private static final double  gainEL500 = 6.42682787e+12; // gain 500 sps
    private static final double  gainEL250 = 4.02328255e+11;
    private static final double  gainEL200 = 1.64931148e+11;
    private static final double  gainEL100 = 1.03504674e+10;
    private static final double  gainEL80 = 4.24823053e+09;
    private static final double  gainEL50 = 6.52211269e+08;
    private static final double  gainEL40 = 2.68236628e+08;
    private static final double  gainEL25 = 4.14315030e+07;
    private static final double  gainEL20 = 1.71082230e+07;
    */

    // BANDPASS corners at 0.5 to 10 Hz
    private static final double  gainB500  = 2.98507510e+02; // gain 500 sps
    private static final double  gainB250  = 8.06501351e+01;
    private static final double  gainB200  = 5.35695125e+01;
    private static final double  gainB100  = 1.58707026e+01;
    private static final double  gainB80   = 1.09530351e+01;
    private static final double  gainB50   = 5.18241175e+00;
    private static final double  gainB40   = 3.66886705e+00;
    private static final double  gainB25   = 1.71589059e+00;
    private static final double  gainB20   = 1.00000002e+00;

    // BANDPASS corners at 0.5 to 5 Hz
    private static final double  gainNB500  = 1.28339154e+03; // gain 500 sps
    private static final double  gainNB250  = 3.33422784e+02;
    private static final double  gainNB200  = 2.17448705e+02;
    private static final double  gainNB100  = 5.95060593e+01;
    private static final double  gainNB80   = 3.97524799e+01;
    private static final double  gainNB50   = 1.75028467e+01;
    private static final double  gainNB40   = 1.20459921e+01;
    private static final double  gainNB25   = 5.66847699e+00;
    private static final double  gainNB20   = 4.00702090e+00;

    private boolean copyInput = false;

    public GeneralButterworthFilter() {
        setType(FilterTypes.HIGHPASS);
    }

    public GeneralButterworthFilter(boolean enableReverseFiltering) {
         this(FilterTypes.HIGHPASS, defaultSps, enableReverseFiltering);
    }

    public GeneralButterworthFilter(int type, int sampleRate) {
       this(type, sampleRate, false);
    }

    public GeneralButterworthFilter(int type, boolean enableReverseFiltering) {
       this(type, defaultSps, enableReverseFiltering);
    }

    public GeneralButterworthFilter(int type, int sampleRate, boolean enableReverseFiltering) {
       this.sampleRate = sampleRate;
       setType(type);        // must set type BEFORE coefs
       setReversed(enableReverseFiltering);
    }

    public void copyInputWaveform(boolean tf) {
        copyInput = tf;
    }
    
    // set freqs are noops because we have defined fixed filters
    //public void setPassBand(double loHz, double hiHz) {}
    //public void setLoHz(double hz) {}
    //public void setHiHz(double hz) {}
    public void setOrder(int order) {};
    public int getOrder() {
        return (type == FilterTypes.BANDPASS || type == FilterTypes.NARROW_BANDPASS) ? 2 : 4;
    }

    /** Returns lowpass corner frequency value, 0 if none. */
    public double getLoHz() {
        return lf;
    }

    /** Returns highpass corner frequency value, 0 if none. */
    public double getHiHz() {
        return hf;
    }

    public void setSampleRate(double sampleRate) {
        setSampleRate((int)Math.round(sampleRate));
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
        setCoefs(sampleRate);
    }

    public int getSampleRate() {
        return this.sampleRate;
    }

    public double getSps() {
        return (double)sampleRate;
    }

    public int getType() {
        return type;
    }

    public void setDescription(String str) {
      description = str;
    }

    public String getDescription( ) {
      return description;
    }
 
    /** Set filter type: FilterTypes.HIGHPASS or FilterTypes.LOWPASS, corner at 1Hz. */
    public void setType(int filterType) {
        type = filterType;
        String typeStr = null;
        if (type == FilterTypes.HIGHPASS) {
             typeStr = "HIGHPASS_1Hz"; 
             lf = 1.0;
             hf = 0.0;
        }
        else if (type == FilterTypes.BANDPASS) {
             typeStr = "BANDPASS_.5_10Hz";
             lf = 0.5;
             hf = 10.0;
        }
        else if (type == FilterTypes.NARROW_BANDPASS) {
             typeStr = "BANDPASS_.5_5Hz"; 
             lf = 0.5;
             hf = 5.0;
        }
        else if (type == FilterTypes.LOWPASS) {
             typeStr = "LOWPASS_8Hz"; 
             lf = 0.0;
             hf = 8.0;
        }
        else if (type == FilterTypes.VERY_LOWPASS) {
             typeStr = "LOWPASS_1Hz"; 
             lf = 0.0;
             hf = 1.0;
        }
        else {
            type = FilterTypes.HIGHPASS;
            typeStr = "HIGHPASS_1Hz"; 
            lf = 1.0;
            hf = 0.0;
        }

        description = BUTTERWORTH + "_" + typeStr;
        setCoefs(sampleRate);
    }

    public void setHighpass() {
      setType(FilterTypes.HIGHPASS);
    }

    public void setLowpass() {
      setType(FilterTypes.LOWPASS);
    }

    public void setBandpass() {
      setType(FilterTypes.BANDPASS);
    }


    /** Set gain and filter coefficients for the given sample rate.
    Values are derived from filter design code of Fisher.
    See: http://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html<br>
    Fourth order filters with one corner at 1.0 Hz.  <br>
    If this is not called class defaults to 100 sps filter.
    */
    public void setCoefs(int sampleRate) {
        this.sampleRate = sampleRate;
        if (type == FilterTypes.HIGHPASS) {
            setCoefsHP();  // same coefs for HP LP if corner is same at 1 Hz
        }
        else if (type == FilterTypes.LOWPASS) {
            setCoefsLP();  // same coefs for HP LP if corner is same at 1 Hz
        }
        else if (type == FilterTypes.BANDPASS) {
            setCoefsBP();
        }
        else if (type == FilterTypes.NARROW_BANDPASS) {
            setCoefsNBP();
        }
        //else if (type == FilterTypes.VERY_LOWPASS) setCoefsVLP();
        else setCoefsVLP();
    }

    // Bandpass 2nd order 0.5 to 10 Hz
    private void setCoefsBP() {
        if (sampleRate == 100) {
            coef1=-4.31175178e-01;
            coef2=2.02526655e+00;
            coef3=-3.74572261e+00;
            coef4=3.15136149e+00;
        }
        else if (sampleRate == 40) {
            coef1=-1.73070748e-01;
            coef2=4.05049025e-01;
            coef3=-1.17307075e+00;
            coef4=1.93386010e+00;
        }
        else if (sampleRate == 80) {
            coef1=-3.51365404e-01;
            coef2=1.66771558e+00;
            coef3=-3.26101128e+00;
            coef4=2.94405080e+00;
        }
        else if (sampleRate == 50) {
            coef1=-2.06671985e-01;
            coef2=8.19263685e-01;
            coef3=-1.95096469e+00;
            coef4=2.33508240e+00;
        }
        else if (sampleRate == 200) {
            coef1=-6.55741658e-01;
            coef2=2.88683838e+00;
            coef3=-4.80476091e+00;
            coef4=3.57364420e+00;
        }
        else if (sampleRate == 250) {
            coef1=-7.13458293e-01;
            coef2=3.08686268e+00;
            coef3=-5.03244992e+00;
            coef4=3.65903703e+00;
        }
        else if (sampleRate == 500) {
            coef1=-8.44653587e-01;
            coef2=3.51944010e+00;
            coef3=-5.50479722e+00;
            coef4=3.83001013e+00;
        }
        else if (sampleRate == 20) {
            coef1=-9.99999956e-01;
            coef2=-4.44288280e-08;
            coef3=1.99999996e+00;
            coef4=4.44288299e-08;
        } 
        else if (sampleRate == 25) {
            coef1=-3.47665395e-01;
            coef2=-1.93936128e-01;
            coef3=8.15708586e-01;
            coef4=6.87445015e-01;
        }
        else {
          // e.g. this filter method is called by WFPanel.createAlternateWF
            System.err.println(" ERROR: GenericButterWorthFilter.setCoefs(rate) coefficients undefined for rate: " +
                    sampleRate+ " defaulting to 100 sps.");
            setCoefs(100);
            return;
        }

        setGain();
    }

    // Bandpass 2nd order 0.5 to 5 Hz
    private void setCoefsNBP() {
        if (sampleRate == 100) {
            coef1=-6.70457906e-01;
            coef2=2.93042722e+00;
            coef3=-4.84628980e+00;
            coef4=3.58623981e+00;
        }
        else if (sampleRate == 40) {
            coef1=-3.70586684e-01;
            coef2=1.72625814e+00;
            coef3=-3.30291382e+00;
            coef4=2.94476738e+00;
        }
        else if (sampleRate == 80) {
            coef1=-6.06759034e-01;
            coef2=2.69979125e+00;
            coef3=-4.57337369e+00;
            coef4=3.48015288e+00;
        }
        else if (sampleRate == 50) {
            coef1=-4.50445430e-01;
            coef2=2.08257332e+00;
            coef3=-3.79268442e+00;
            coef4=3.15946330e+00;
        }
        else if (sampleRate == 200) {
            coef1=-8.18789928e-01;
            coef2=3.43397697e+00;
            coef3=-5.41114191e+00;
            coef4=3.79594934e+00;
        }
        else if (sampleRate == 250) {
            coef1=-8.52192252e-01;
            coef2=3.54194224e+00;
            coef3=-5.52707640e+00;
            coef4=3.83732411e+00;
        }
        else if (sampleRate == 500) {
            coef1=-9.23142312e-01;
            coef2=3.76560927e+00;
            coef3=-5.76176140e+00;
            coef4=3.91929429e+00;
        }
        else if (sampleRate == 20) {
            coef1=-1.77578119e-01;
            coef2=4.60665534e-01;
            coef3=-1.17757812e+00;
            coef4=1.86549482e+00;
        } 
        else if (sampleRate == 25) {
            coef1=-2.19653984e-01;
            coef2=8.74805556e-01;
            coef3=-1.96749776e+00;
            coef4=2.29905536e+00;
        }
        else {
          // e.g. this filter method is called by WFPanel.createAlternateWF
            System.err.println(" ERROR: GenericButterWorthFilter.setCoefs(rate) coefficients undefined for rate: " +
                    sampleRate+ " defaulting to 100 sps.");
            setCoefs(100);
            return;
        }

        setGain();
    }

    // Lowpass 4 pole butterworth corner at 1 Hz 
    public void setCoefsVLP() {
        setCoefsHP();
    }
    // Highpass 4 pole butterworth corner at 1 Hz 
    public void setCoefsHP() {
        if (sampleRate == 100) {
            coef1 = -0.8485559993;
            coef2 =  3.5335352195;
            coef3 = -5.5208191366;
            coef4 =  3.8358255406;
        }
        else if (sampleRate == 40) {
            coef1 = -0.6630104844;
            coef2 =  2.9240526562;
            coef3 = -4.8512758825;
            coef4 =  3.5897338871;
        }
        else if (sampleRate == 80) {
            coef1 = -0.8144059977;
            coef2 =  3.4247473473;
            coef3 = -5.4051668617;
            coef4 =  3.7947911031;
        }
        else if (sampleRate == 50) {
          coef1 = -0.7199103273;
          coef2 = 3.1159669252;
          coef3 = -5.0679983867;
          coef4 = 3.6717290892 ;
        }
        else if (sampleRate == 200) {
          coef1 = -0.9211819292;
          coef2 =  3.7603495077;
          coef3 = -5.7570763791;
          coef4 =  3.9179078654;
        }
        else if (sampleRate == 250) {
          coef1 =  -0.9364332432;
          coef2 =   3.8072324572;
          coef3 =  -5.8051254211;
          coef4 =   3.9343258208;
        }
        else if (sampleRate == 500) {
          coef1 = -0.9676955438;
          coef2 =  3.9025587848;
          coef3 = -5.9020258615;
          coef4 =  3.9671625959;
        }
        else if (sampleRate == 20) {
          coef1 = -0.4382651423;
          coef2 =  2.1121553551;
          coef3 = -3.8611943490;
          coef4 =  3.1806385489;
        } 
        else if (sampleRate == 25) {
          coef1 = -0.5174781998;
          coef2 =  2.4093428566;
          coef3 = -4.2388639509;
          coef4 =  3.3440678377;
        }
        else {
          // e.g. this filter method is called by WFPanel.createAlternateWF
            System.err.println(" ERROR: GenericButterWorthFilter.setCoefs(rate) coefficients undefined for rate: " +
                    sampleRate+ " defaulting to 100 sps.");
            setCoefs(100);
            return;
        }

        setGain();
    }

    // Lowpass 4 pole butterworth corner at 8 Hz
    public void setCoefsLP() {
        if (sampleRate == 100) {
            coef1=-2.64454816e-01;
            coef2=1.40348467e+00;
            coef3=-2.86739911e+00;
            coef4=2.69261099e+00;
        }
        else if (sampleRate == 40) {
            coef1=-3.01188750e-02;
            coef2=1.82675698e-01;
            coef3=-6.79978527e-01;
            coef4=7.82095198e-01;
        }
        else if (sampleRate == 80) {
            coef1=-1.87379492e-01;
            coef2=1.05466541e+00;
            coef3=-2.31398841e+00;
            coef4=2.36951301e+00;
        }
        else if (sampleRate == 50) {
            coef1=-6.32116957e-02;
            coef2=4.08070952e-01;
            coef3=-1.12276608e+00;
            coef4=1.41198350e+00;
        }
        else if (sampleRate == 200) {
            coef1=-5.17478200e-01;
            coef2=2.40934286e+00;
            coef3=-4.23886395e+00;
            coef4=3.34406784e+00;
        }
        else if (sampleRate == 250) {
            coef1=-5.90703467e-01;
            coef2=2.67291985e+00;
            coef3=-4.55851191e+00;
            coef4=3.47502240e+00;
        }
        else if (sampleRate == 500) {
            coef1=-7.68872744e-01;
            coef2=3.27743279e+00;
            coef3=-5.24600331e+00;
            coef4=3.73735339e+00;
        }
        else if (sampleRate == 20) {
            coef1=-1.87379492e-01;
            coef2=-1.05466541e+00;
            coef3=-2.31398841e+00;
            coef4=-2.36951301e+00;
        } 
        else if (sampleRate == 25) {
            coef1=-4.33896119e-02;
            coef2=-2.81749812e-01;
            coef3=-8.68333374e-01;
            coef4=-1.09635960e+00;
        }
        else {
          // e.g. this filter method is called by WFPanel.createAlternateWF
            System.err.println(" ERROR: GenericButterWorthFilter.setCoefs(rate) coefficients undefined for rate: " +
                    sampleRate+ " defaulting to 100 sps.");
            setCoefs(100);
            return;
        }

        setGain();
    }

    /* Extremely lowpass 4 pole butterworth corner at 0.1 Hz
    public void setCoefsELP() {
        if (sampleRate == 100) {
            coef1 = -9.83715268e-01;
            coef2 = 3.95101244e+00;
            coef3 = -5.95087843e+00;
            coef4 = 3.98358126e+00;
        }
        else if (sampleRate == 40) {
            coef1 = -9.59783654e-01;
            coef2 = 3.87853055e+00;
            coef3 = -5.87770027e+00;
            coef4 = 3.95895332e+00;
        }
        else if (sampleRate == 80) {
            coef1 = -9.79685669e-01;
            coef2 = 3.93884917e+00;
            coef3 = -5.93864009e+00;
            coef4 = 3.97947658e+00;
        }
        else if (sampleRate == 50) {
            coef1 = -9.67695544e-01;
            coef2 = 3.90255878e+00;
            coef3 = -5.90202586e+00;
            coef4 = 3.96716260e+00;
        }
        else if (sampleRate == 200) {
            coef1 = -9.91824224e-01;
            coef2 = 3.97543915e+00;
            coef3 = -5.97540555e+00;
            coef4 = 3.99179062e+00;
        }
        else if (sampleRate == 250) {
            coef1 = -9.93454015e-01;
            coef2 = 3.98034057e+00;
            coef3 = -5.98031905e+00;
            coef4 = 3.99343250e+00;
        }
        else if (sampleRate == 500) {
            coef1 = -9.96721634e-01;
            coef2 = 3.99015952e+00;
            coef3 = -5.99015414e+00;
            coef4 = 3.99671625e+00;
        }
        else if (sampleRate == 20) {
            coef1 = -9.21181929e-01;
            coef2 = 3.76034951e+00;
            coef3 = -5.75707638e+00;
            coef4 = 3.91790787e+00;
        } 
        else if (sampleRate == 25) {
            coef1 = -9.36433243e-01;
            coef2 = 3.80723246e+00;
            coef3 = -5.80512542e+00;
            coef4 = 3.93432582e+00;
        }
        else {
          // e.g. this filter method is called by WFPanel.createAlternateWF
            System.err.println(" ERROR: GenericButterWorthFilter.setCoefs(rate) coefficients undefined for rate: " +
                    sampleRate+ " defaulting to 100 sps.");
            setCoefs(100);
            return;
        }

        setGain();
    }
    */

    /** Set gain for the given sample rate and filter type.
    Values are derived from filter design code of Fisher.
    See: http://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html<br>
    Fourth order filters with one corner at 1.0 Hz.
    */
    private void setGain() {
       if (type == FilterTypes.HIGHPASS) {
         if (sampleRate == 100) {
            gain = gainH100;
         } else if (sampleRate == 80) {
            gain = gainH80;
         } else if (sampleRate == 40) {
            gain = gainH40;
         } else if (sampleRate == 50) {
            gain = gainH50;
         } else if (sampleRate == 200) {
            gain = gainH200;
         } else if (sampleRate == 500) {
            gain = gainH500;
         } else if (sampleRate == 20) {
            gain = gainH20;
         } else if (sampleRate == 25) {
            gain = gainH25;
         }
       } else if (type == FilterTypes.BANDPASS) {
         if (sampleRate == 100) {
            gain = gainB100;
         } else if (sampleRate == 80) {
            gain = gainB80;
         } else if (sampleRate == 40) {
            gain = gainB40;
         } else if (sampleRate == 50) {
            gain = gainB50;
         } else if (sampleRate == 200) {
            gain = gainB200;
         } else if (sampleRate == 500) {
            gain = gainB500;
         } else if (sampleRate == 20) {
            gain = gainB20;
         } else if (sampleRate == 25) {
            gain = gainB25;
         }
       } else if (type == FilterTypes.NARROW_BANDPASS) {
         if (sampleRate == 100) {
            gain = gainNB100;
         } else if (sampleRate == 80) {
            gain = gainNB80;
         } else if (sampleRate == 40) {
            gain = gainNB40;
         } else if (sampleRate == 50) {
            gain = gainNB50;
         } else if (sampleRate == 200) {
            gain = gainNB200;
         } else if (sampleRate == 500) {
            gain = gainNB500;
         } else if (sampleRate == 20) {
            gain = gainNB20;
         } else if (sampleRate == 25) {
            gain = gainNB25;
         }
       } else if (type == FilterTypes.LOWPASS) {
         if (sampleRate == 100) {
            gain = gainL100;
         } else if (sampleRate == 80) {
            gain = gainL80;
         } else if (sampleRate == 40) {
            gain = gainL40;
         } else if (sampleRate == 50) {
            gain = gainL50;
         } else if (sampleRate == 200) {
            gain = gainL200;
         } else if (sampleRate == 500) {
            gain = gainL500;
         } else if (sampleRate == 20) {
            gain = gainL20;
         } else if (sampleRate == 25) {
            gain = gainL25;
         }
       } else { // if (type == FilterTypes.VERY_LOWPASS) 
         if (sampleRate == 100) {
            gain = gainVL100;
         } else if (sampleRate == 80) {
            gain = gainVL80;
         } else if (sampleRate == 40) {
            gain = gainVL40;
         } else if (sampleRate == 50) {
            gain = gainVL50;
         } else if (sampleRate == 200) {
            gain = gainVL200;
         } else if (sampleRate == 500) {
            gain = gainVL500;
         } else if (sampleRate == 20) {
            gain = gainVL20;
         } else if (sampleRate == 25) {
            gain = gainVL25;
         }
       }
    }

    /** If set true time-series will be reverse filtered to remove phase shift. */
    public void setReversed(boolean tf) {
        reverse = tf;
    }

    public boolean isReversed() {
        return reverse;
    }

    /** Return the current filter type for input sample rate. */
    public FilterIF getFilter(int sampleRate) {
        return new GeneralButterworthFilter(type, sampleRate, reverse);
    }

    /** Return the correct filter for the sample rate. */
    public FilterIF getFilter(int type, int sampleRate) {
        return new GeneralButterworthFilter(type, sampleRate, reverse);
    }

    /** Returns new instance of type specified by an input string type.
     * Filter coefficients are set to those of the current values of the
     * instance current sample rate and reverse filtering members.
     * */
    public FilterIF getFilter(String type) {
        return getFilter(FilterTypes.getType(type), sampleRate);
    }

    /** Returns passed object with timeseries filtered in place.
     *  If the object passed is not float[] or Waveform the original object is
     *  returned unchanged. */
    public Object filter(Object in) {
       if (in instanceof float[]) return filter((float []) in);
       if (in instanceof Waveform) return filter((Waveform)in);

       return in;
    }

    /** Filter the time-series in place. Applies a cosine taper. Will reverse filter
    if setReverse(true). */
    public float[] filter(float [] in) {
       float [] values = demean(in);
       values = org.trinet.util.AmplitudeCosineTaper.taper(values, cosineTaperAlpha);
       values = doFilter(values);
       return (reverse) ? doReverseFilter(values) : values;
    }

    /** Returns Waveform with time series filtered.
     * The original timeseries in the Waveform is altered by the filter
     * unless waveform copy flag is set <i>true</i> for this instance,
     * Default value is <i>false</i>.
     * Returns 'null' if filtering fails.
     * @see #copyInputWaveform(boolean)
     * */
    public Waveform filter(Waveform wf) {

        if (wf == null) return null;

        Waveform wf2 = (copyInput) ? wf.copy() : wf; 
        if (wf2 == null) return null;

        WFSegment seg[] = wf2.getArray();
        if (seg != null) {
            for (int i=0; i < seg.length; i++) {
              filter(seg[i].getTimeSeries());  // filter the float[] in place
              seg[i].scanTimeSeries();
              seg[i].setIsFiltered(true);
            }
            wf2.setFilterName(getDescription()); // sets isFiltered() == true
        }

        // Recalculate bias, max, min, etc.
        wf2.scanForBias();
        wf2.scanForAmps();

        return wf2;
     }


    /** Remove bias (mean) from the time series in place. */
    public float [] demean(float [] values) {

        if (values == null) return values;
        int size = values.length;
        if (size == 0) return values;

        // calculate mean
        double sum = 0.;
        for (int idx = 0; idx < size; idx++) {
            sum += values[idx];
        }
        double mean = Math.round(sum/size);

        // remove mean
        for (int idx = 0; idx < size; idx++) {
            values[idx] = (float) (values[idx] - mean);
        }
        return values;

    }

    private void zeroBuffers() {
        java.util.Arrays.fill(xv, 0.);
        java.util.Arrays.fill(yv, 0.);
    }

    /** Forward filter. */
    private float [] doFilter(float [] in) {
        if (in == null || gain == 0.) return in;
        zeroBuffers();
        int size = in.length;
        for (int idx=0; idx < size; idx++) {
            in[idx] = filterSample(in[idx]);
        }
        return in;
    }

    /** Reverse filter to undo phase shift. */
    private float [] doReverseFilter(float [] in) {
        if (in == null || gain == 0.) return in;
        zeroBuffers();
        int size = in.length;
        for (int idx=size-1; idx > -1; idx--) {
            in[idx] = filterSample(in[idx]);
        }
        return in;
    }

    private float filterSample(float value) {
        if (type == FilterTypes.HIGHPASS)
            return filterSampleHP(value);
        else if (type == FilterTypes.BANDPASS || type == FilterTypes.NARROW_BANDPASS) 
            return filterSampleBP(value);
        else  // LOWPASS or VERY_LOWPASS
            return filterSampleLP(value);
    }

    /** 4 pole lowpass filter a single sample in the time series. */
    private float filterSampleLP(float value) {
        initBuffers(value);
        yv[4] = (xv[0] + xv[4]) + 4 * (xv[1] + xv[3]) + 6 * xv[2]
                 + (coef1 * yv[0]) + (coef2 * yv[1])
                 + (coef3 * yv[2]) + (coef4 * yv[3]);
        return  (float) yv[4];
    }

    /** 4 pole highpass filter a single sample in the time series. */
    private float filterSampleHP(float value) {
        initBuffers(value);
        yv[4] = (xv[0] + xv[4]) - 4 * (xv[1] + xv[3]) + 6 * xv[2]
                 + (coef1 * yv[0]) + (coef2 * yv[1])
                 + (coef3 * yv[2]) + (coef4 * yv[3]);
        return  (float) yv[4];
    }

    // 2nd order BP = 4 poles
    /** 2nd order bandpass filter a single sample in the time series. */
    private float filterSampleBP(float value) {
        initBuffers(value);
        yv[4] = (xv[0] + xv[4]) - 2 * xv[2]
                 + (coef1 * yv[0]) + (coef2 * yv[1])
                 + (coef3 * yv[2]) + (coef4 * yv[3]);
        return (float) yv[4];
    }

    private void initBuffers(float value) {
        xv[0] = xv[1];
        xv[1] = xv[2];
        xv[2] = xv[3];
        xv[3] = xv[4]; 
        xv[4] = (double)value/gain;
        yv[0] = yv[1];
        yv[1] = yv[2];
        yv[2] = yv[3];
        yv[3] = yv[4]; 
    }

    /** Return true if the units of the actual samples in the waveform are
     * legal for this filter. */
    public boolean unitsAreLegal(Waveform wf) {
      return unitsAreLegal(wf.getAmpUnits());
    }

    /** Return true if the units of the raw waveform are legal for this filter. */
    public boolean unitsAreLegal(int units) {
      return true;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("description = ").append(description).append("\n");
        sb.append("NZEROS = ").append(NZEROS).append(" NPOLES = ").append(NPOLES).append("\n");
        sb.append("cosineTaperAlpha = ").append(cosineTaperAlpha);
        sb.append(" reverse =  ").append(reverse).append("\n");
        sb.append("defaultSps = ").append(defaultSps);
        sb.append(" sampleRate = ").append(sampleRate).append("\n");
        sb.append("gain= ").append(gain).append(" coef1= ").append(coef1);
        sb.append(" coef2= ").append(coef2).append(" coef3= ").append(coef3);
        sb.append(" coef4= ").append(coef4).append("\n");
        return sb.toString();
    }
}

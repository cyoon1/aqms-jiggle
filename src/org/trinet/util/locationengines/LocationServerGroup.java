package org.trinet.util.locationengines;

import java.util.*;

/**
* A mapped list of named LocationServers.
*/
public class LocationServerGroup implements java.io.Serializable, Cloneable {

    //protected static String defaultLocationEngineClassName = "org.trinet.util.locationengines.LocationEngineHypoInverse";

    private static final String del1 = "/"; // separator between groups
    // use "+" in lieu of " " as delimiter to allow spaces in the group name
    private static final String del2 = "+"; // separators between fields
    private static final String del3 = ":"; // separators between url:port

    private HashMap servers = new HashMap(9);
    //private Properties props = null;;

    private String selected = null;

    public LocationServerGroup(Properties props) {
        setProperties(props);
    }

    public void setProperties(Properties props) {
        //this.props = props;
        configureFromProperties(props);
    }

    public int size() {
        return servers.size();
    }

    public void clear() {
        servers.clear();
    }

    public LocationServiceDescIF remove(String serviceName) {
        return (LocationServiceDescIF) servers.remove(serviceName);
    }

    public LocationServiceDescIF remove(LocationServiceDescIF service) {
        return (LocationServiceDescIF) servers.remove(service.getServiceName());
    }

    public boolean hasService(String name) {
        return servers.containsKey(name);
    }

    public String getSelectedServiceName() {
        return selected;
    }

    public LocationServiceDescIF getSelectedService() {
        return (LocationServiceDescIF) servers.get(selected);
    }

    public LocationServiceDescIF getService(String name) {
        return (LocationServiceDescIF) servers.get(name);
    }

    public Collection getServices() {
        return servers.values();
    }

    public Collection getServiceNames() {
        return servers.keySet();
    }

    public boolean changeServiceDesc(String name, String type, int timeout, String url, int port) {
        LocationServiceDescIF s = getService(name);
        if (s == null) return false;
        s.setLocatorName(type);
        s.setServerTimeoutMillis(timeout);
        s.setIPAddress(url);
        s.setPort(port);
        return true;
    }

    public boolean setSelected(String name) {
        LocationServiceDescIF s = (LocationServiceDescIF) servers.get(name);
        if (s == null) return false;
        //props.setProperty("locationServerSelected", name);
        //props.setProperty("locationEngineType", s.getLocatorName());
        //props.setProperty("locationEngineTimeoutMillis", String.valueOf(s.getServerTimeoutMillis()));
        //props.setProperty("locationEngineAddress", s.getIPAddress());
        //props.setProperty("locationEnginePort", String.valueOf(s.getPort()));
        //props.setProperty("locationServerGroupList", toServiceListPropertyString());

        selected = name;

        return true;
    }

    public void setSelected(LocationServiceDescIF s) {
        addService(s);
        setSelected(s.getServiceName());
    }

    public void addService(LocationServiceDescIF s) {
        servers.put(s.getServiceName(), s);
        //props.setProperty("locationServerGroupList", toServiceListPropertyString());
    }

    public boolean addService(String propertyString) {
        LocationServiceDescIF s = toService(propertyString);
        if (s == null) return false; 
        addService(s);
        return true;
    }

    public boolean addService(String name, String type, int millis, String url, int port) {
        LocationServiceDescIF s = toService(name, type, millis, url, port);
        if (s == null) return false; 
        addService(s);
        return true;
    }

    public LocationServiceDescIF toService(String name, String type, int millis, String url, int port) {
        StringBuffer sb = new StringBuffer(128);
        sb.append(name).append(del2);
        sb.append(type).append(del2);
        sb.append(millis).append(del2);
        sb.append(url).append(del3).append(port);
        return toService(sb.toString());
    }

    public LocationServiceDescIF toService(String propertyString) {
        LocationServiceDescIF s = null; 
        try {
            StringTokenizer strTok2 = new StringTokenizer(propertyString, del2);
            String name = strTok2.nextToken(); // service name like serveram6500
            String type = strTok2.nextToken(); // locator type name like HYP2000
            int timeout = Integer.parseInt(strTok2.nextToken()); // timeout millisecs

            // Should type only be locator name or also indicate a its configuration like model setup?
            // Could parse out loc engine class name string or find via engineType to class map lookup class
            // locationEngineClassName = EngineTypes.getClassByType(engineType)

            // now get the url:port pairs
            int port = -1;
            String address = null;
            while (strTok2.hasMoreTokens()) {
                StringTokenizer strTok3 = new StringTokenizer(strTok2.nextToken(), del3);
                address = strTok3.nextToken();
                port = Integer.parseInt(strTok3.nextToken());
            }

            if (address != null && port >= 0) {
                //s = (LocationServiceDescIF) AbstractLocationEngineService.create(locationEngineClassName);
                // type = getLocatorName(); 
                s =  new LocationServerDesc();
                s.setServiceName(name);
                s.setLocatorName(type);
                s.setServerTimeoutMillis(timeout);
                s.setIPAddress(address);
                s.setPort(port);
            }

        } catch (Exception ex) {
              ex.printStackTrace();
              System.err.println("Bad syntax in property string:" + propertyString);
        }

        return s;
    }

    public String toServiceListPropertyString() {
        if (size() == 0) return "";
        StringBuffer sb = new StringBuffer(128*(size()+1));
        sb.append(del1);
        Iterator iter = servers.values().iterator();
        while (iter.hasNext()) {
          sb.append(toServiceListPropertyString((LocationServiceDescIF) iter.next())).append(del1);
        }
        return sb.toString();
    }

    public static String toServiceListPropertyString(LocationServiceDescIF s) {
        // use "+" as delimiter to allow spaces in the group name
        StringBuffer sb = new StringBuffer(128);
        sb.append(s.getServiceName()).append(del2);
        sb.append(s.getLocatorName()).append(del2);
        sb.append(s.getServerTimeoutMillis()).append(del2);
        sb.append(s.getIPAddress()).append(del3).append(s.getPort());
        return sb.toString();
    }

    public String toString() {
        Iterator iter = servers.values().iterator();
        StringBuffer sb = new StringBuffer(servers.size()*80);
        LocationServiceDescIF s = null;
        sb.append("Selected service = ").append(selected).append("\n");
        while (iter.hasNext()) {
           s = (LocationServiceDescIF) iter.next();
           sb.append(s.toString());
           sb.append("\n");
        }
        return sb.toString();
    }

    private void configureFromProperties(Properties props) {
        String selectedNameStr = props.getProperty("locationServerSelected", "default");
        String groupListStr = props.getProperty("locationServerGroupList");
        if (groupListStr == null) {
            StringBuffer sb = new StringBuffer(128);
            sb.append(del1);
            sb.append(selectedNameStr).append(del2);
            sb.append(props.getProperty("locationEngineType", "unknown")).append(del2);
            sb.append(props.getProperty("locationEngineTimeoutMillis", "10000")).append(del2);
            sb.append(props.getProperty("locationEngineAddress", "mickey.gps.caltech.edu")).append(del3);
            sb.append(props.getProperty("locationEnginePort", "6500"));
            sb.append(del1);
            groupListStr= sb.toString();
        }
        servers = parsePropertyString(groupListStr);

        if (! setSelected(selectedNameStr) && servers.size() > 0) {
            Collection c = servers.values();
            Iterator iter = c.iterator();
            setSelected(((LocationServiceDescIF) iter.next()).getServiceName()); 
        }

        if (hasService(selectedNameStr)) updateProperties(props); // -aww 2008/02/20 to synch with setting of selected name in group if any
    }

    private HashMap parsePropertyString(String str) {
        // Note if connection are open to server services you need to close them first before clear !!!
        servers.clear();

        StringTokenizer strTok1 = new StringTokenizer(str, del1); // parse the list
        try {
          while (strTok1.hasMoreTokens()) {
              LocationServiceDescIF s = toService(strTok1.nextToken());
              if (s != null) servers.put(s.getServiceName(), s);
          }
        } catch (Exception ex) {
              ex.printStackTrace();
              System.err.println("Bad syntax in property string:" +str);
              return null;
        }

        return  servers;
    }

    public void updateProperties(Properties props) {
        props.setProperty("locationServerSelected", getSelectedServiceName());
        LocationServiceDescIF s = getSelectedService();
        if (s != null) {
            props.setProperty("locationEngineType", s.getLocatorName());
            props.setProperty("locationEngineTimeoutMillis", String.valueOf(s.getServerTimeoutMillis()));
            props.setProperty("locationEngineAddress", s.getIPAddress());
            props.setProperty("locationEnginePort", String.valueOf(s.getPort()));
        }
        props.setProperty("locationServerGroupList", toServiceListPropertyString());
    }

    public Object clone() {
        LocationServerGroup lsg = null;
        try {
            lsg = (LocationServerGroup) super.clone();
            lsg.servers = (HashMap) this.servers.clone();
            //lsg.props = new Properties(this.props);;
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return lsg;
    }

    class LocationServerDesc implements java.io.Serializable, Cloneable, Comparable, LocationServiceDescIF {
        String name = null;
        String type = "unknown";
        String ipAddress = null;
        int port = -1;
        int timeout = 0;

        public String getLocatorName() {
            return type;
        }
        public String getServiceName() {
            return name;
        }
        public String getIPAddress() {
            return ipAddress;
        }
        public int getPort() {
            return port;
        }
        public int getServerTimeoutMillis() {
            return timeout;
        }

        public void setLocatorName(String type) {
            this.type =  type;
        }
        public void setServiceName(String name) {
            this.name =  name;
        }
        public void setServerTimeoutMillis(int timeout) {
            this.timeout =  timeout;
        }
        public void setIPAddress(String ipAddress) {
            this.ipAddress =  ipAddress;
        }
        public void setPort(int port) {
            this.port =  port;
        }

        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (! (obj instanceof LocationServiceDescIF)) return false;
            LocationServiceDescIF lsd = (LocationServiceDescIF) obj;
            return ( (getPort() == lsd.getPort()) &&
                     getIPAddress().equals(lsd.getIPAddress()) &&
                     getLocatorName().equals(lsd.getLocatorName()) &&
                     getServiceName().equals(lsd.getServiceName())
                   ); 
        }

        public String describeConnection() {
            return toString();
        }
        
        public String toString() {
            StringBuffer sb =  new StringBuffer(128); 
            sb.append(" Service=").append(name);
            sb.append(" Type=").append(type);
            sb.append(" @").append(ipAddress).append(":").append(port);
            sb.append(" Timeout=").append(timeout);
            return sb.toString();
        }

        public int compareTo(Object obj) {
            return toString().compareTo(obj.toString());
        }

        public Object clone() {
            LocationServerDesc lsd = null;
            try {
                lsd = (LocationServerDesc) super.clone(); 
            }
            catch (CloneNotSupportedException ex) {
                ex.printStackTrace();
            }
            return lsd;
        }
    }
/*
    public static final void main(String args []) {
        Properties props = new Properties();
        String str = "/cat+10+serveram.gps.caltech.edu:6500/dog+100+serveram.gps.caltech.edu:6501/hog+1000+serveram.gps.caltech.edu:6502/";
        props.setProperty("locationServerGroupList", str);
        props.setProperty("locationServerSelected", "dog");
        props.list(System.out);
        LocationServerGroup lsg = new LocationServerGroup(props);
        System.out.println(lsg.toString());        
        props.list(System.out);

        System.out.println("--------------------------------------------------------------------");
        lsg.setSelected("hog");
        System.out.println("Selected name: " + lsg.getSelectedServiceName());
        props.list(System.out);
    }
*/
}

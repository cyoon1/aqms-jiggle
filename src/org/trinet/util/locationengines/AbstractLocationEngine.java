package org.trinet.util.locationengines;
import java.beans.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * Base case which can be used by subclass implementations providing a Solution
 * location using a remote location server.
 */
public abstract class AbstractLocationEngine extends AbstractSolutionAssocDataEngine implements LocationEngineIF {

    protected Solution solvedSolution = null; // the result of the engine calculation, like the currentSol

    /** List of phases passed to location server. */
    protected PhaseList currentPhaseList; // updated by returned results

    /** True if current solution should be used as trial (beginning) location.
     *  Default is 'false'. */
    protected boolean useTrialLoc = false;
    protected boolean useTrialOriginTime = false;

    protected LatLonZ trialLoc = null;

    /** Protected, use factory method to create concrete subclass instance. */
    protected AbstractLocationEngine() { }

    /* Factory Method creates an instance of a LocationEngine service. */
    public static LocationEngineIF create(String sClassName) {
        LocationEngineIF locEng = null;

        try {
          locEng = (LocationEngineIF) Class.forName(sClassName).newInstance();
        }
        catch (ClassNotFoundException ex) {
          ex.printStackTrace();
        }
        catch (InstantiationException ex) {
          ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
          ex.printStackTrace();
        }
      return locEng;
    }

    /** Set properties, like use trial Location. */
    public void initializeEngine() {
      super.initializeEngine();
      useTrialLoc = props.getBoolean("useTrialLocation");
      useTrialOriginTime = props.getBoolean("useTrialOriginTime");
      setLocatorName(props.getProperty("locationEngineType","unknown"));
    }

    public void reset() {
        super.reset();           // resets result and status solveSuccess
        solvedSolution = null;   // the solved result, usually same as currentSol, but could be clone
        currentPhaseList = null; // used to calculate result
        //trialLoc = null; // ? likewise, keep it or not for next Solution ?
    }

    /** String describing name of Solution locator, e.g. Hypo2000, HypoElipse, GLASS. */
    public void setLocatorName(String name) {
        setEngineName(name);
    }
    /** Returns the String describing name of locator (Hypo2000, HypoElipse, GLASS). */
    public String getLocatorName() {
        return getEngineName();
    }

    /** Required by GenericEngineIF, a wrapper around solve(Solution). */
    public boolean solve(Object data) {
      solveSuccess = false;
      solveSuccess = (data instanceof Solution) ? solve((Solution) data) : false;
      return solveSuccess;
    }

    /**
     * Locate a given solution, wrapper around solve(Solution), reduces ambiguity
     * when multiple engines are doing different tasks with Solution data.
     */
    public boolean locate(Solution sol) {
      return solve(sol);
    }
    
    /**
     * Calculate a solution given a Solution with a populated PhaseList.
     * Returns true on success and false if list is emply.
     */
    public boolean solve(Solution sol) {
      return solve(sol, sol.getPhaseList());
    }
    
    /**
     * Calculate a solution given a Solution and an ArrivalList.
     * Returns true on success and false if phase list is emply.
     */
    public boolean solve(Solution sol, PhaseList phaseList) {
        setSolution(sol); // assumes setSolution does an engine reset();

        //System.out.println("DEBUG LocationEngine: useEventTypeFixDepth? " + props.getBoolean("useEventTypeFixDepth"));
        if ( props.getBoolean("useEventTypeFixDepth") ) {
            EventType eventType = EventTypeMap3.getEventTypeByJasiType(sol.eventType.toString());
            if ( eventType.isDepthFixed() ) {
                //System.out.println("DEBUG LocationEngine: eventType.isDepthFixed? " + eventType.isDepthFixed() );
                double fixZ = props.getDouble("fixDepth", eventType.fixDepth);
                //System.out.println("DEBUG LocationEngine: fixDepth= " + fixZ);
                if (! (sol.mdepth.doubleValue() == fixZ)) { // used for depth in hypoinverse trial terminator record -aww 2015/06/11
                    sol.mdepth.setValue(fixZ); // depth in hypoinverse trial terminator record -aww 2015/06/11
                    // NOTE: static Solution.calcOriginDatum should be enabled only for CRH CRT models (non-geoidal type), default false returns 0.
                    double datum = sol.calcOriginDatumKm();
                    // subtract datum correction to the fixZ mdepth here -aww 2015/06/12
                    sol.depth.setValue((Double.isNaN(datum)) ? fixZ : fixZ - datum); // -aww 2015/06/12 
                    sol.setStale(true);
                }
                if (sol.depthFixed.booleanValue() == false) {
                    sol.depthFixed.setValue(true);
                    sol.setStale(true);
                }
            }
        }

        return solve(phaseList);
    }
    
    /**
     * Calculate a solution given an ArrivalList.
     * Returns true on success and false if phase list is emply.
     */
    abstract public boolean solve(PhaseList phaseList);
    
    abstract public boolean solveFromWaveforms(Solution sol, List wfList);

    public Object getResult() { // Engine IF method
        return solvedSolution; // default to last calculated input success
    } 


    /** Set 'true' if current solution should be used as trial (beginning)
     * location for next location attempt. Default is 'false'.
     * */
    public void setUseTrialLocation(boolean tf) {
        useTrialLoc = tf;
    }
    /** Returns 'true' if current solution will be used as trial (beginning) location.*/
    public boolean getUseTrialLocation() {
        return useTrialLoc;
    }

    /** Set 'true' to use current solution origin time for trial.*/
    public void setUseTrialOriginTime(boolean tf) {
        useTrialOriginTime = tf;
    }
    /** Returns 'true' if current solution origin time will be used as trial.*/
    public boolean getUseTrialOriginTime() {
        return useTrialOriginTime;
    }

    public void setTrialLocation(LatLonZ latLonZ) {
        trialLoc = latLonZ;
    }
    public void setTrialLocation(double lat, double lon, double z) {
        trialLoc = new LatLonZ(lat,lon,z);
    }
    public LatLonZ getTrialLocation() {
        return trialLoc;
    }
    /**
     * If the trial location latLonZ is set (not null and latLonZ.isValid()),
     * set the location of the input Solution to that of trial location
     * if it differs from the trial location.
     */
    public void setTrialLocationOf(Solution aSol) {
      //if (! useTrialLoc) return; // don't force it  - removed 2014/03/26 -aww

      // Is there a trial value, if not bail
      // Note test won't pass a zero depth to fix if location is null 
      if (trialLoc == null || trialLoc.isNull()) return; // no

      // save the old location to compare with new trial
      LatLonZ oldLoc = aSol.getLatLonZ();

      // set newLoc values only for the non-zero trial values 
      LatLonZ newLoc = (LatLonZ) oldLoc.clone();

      double value = trialLoc.getLat();
      if (value != 0.) newLoc.setLat(value);

      value = trialLoc.getLon();
      if (value != 0.) newLoc.setLon(value);

      value = trialLoc.getZ();
      value = 0.01; // avoid 0 depth in hypoinverse trial for some crustal models - aww 2015/10/10
      aSol.mdepth.setValue(value); // depth in hypoinverse trial terminator record -aww 2015/06/11
      double datum = aSol.calcOriginDatumKm();
      newLoc.setZ(Double.isNaN(datum) ? value : value - datum); // approximate geoid depth, check for valid datum -aww 2015/10/10

      // set solution latLonZ to newLoc only if it's a change
      if (oldLoc.getLat()  != newLoc.getLat() || oldLoc.getLon() != newLoc.getLon() || (Math.abs(oldLoc.getZ() - newLoc.getZ()) > 0.011 ) ) {
        aSol.setLatLonZ(newLoc);
        aSol.setStale(true); // not needed if above setLatLonZ does it - aww
      }
    }
 
    // could add more methods for testing, setting fixed depth, loc etc.
    public int getMinPhaseCount() {
        return DEFAULT_MIN_SOLUTION_PHASES;
    }
}   // end of AbstractLocationEngine class


// Should we have a generic LocationEngine and be able to "set"
// the location method (analogous to Magnitude calc)? -aww
//
//
// Edited MagnitudeMethodIF to produce this IF prototype 
//
package org.trinet.util.locationengines;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public interface LocationMethodIF extends ConfigurationSourceIF {
    /** Known methods subtype Class constants */
    public static final int  HYPOINV_LOCATION_METHOD  = 0;
    /*
    public static final String HYPOINV_BASE_CLASS_NAME = "HypoinvLocationMethod";
    */

   // Return name of method
   public String getMethodName();

   // Substring describing the subtype.
   public String getLocationTypeString();

   //String describe methods result/state
   public String getResultsString();
   // Initialize default values.
   public void setDefaultProperties();

   // set derived results dependent data to an initial state (cleanup after calcs). 
   public void reset();

   public void initializeMethod(); // ? return boolean or throws IllegalStateException ?

   // Configure method parameter settings from properties, initializes.
   public void setProperties(GenericPropertyList props);
   public boolean setProperties(String propertyFileName);
   public GenericPropertyList getProperties();
}

//TODO:
//Make LocationMethodIF analogous to a MagMethodIF
//identify methods with common function in both and
//then rename them to an "abstraction" for similar
//invocation signature.
//
//Isolate methods in the respective interfaces that are
//particular to requirements of mag/location calculations
//
/*
   // Reading (observation) data value generator for this method type:
    public void setWaveformFilter(WaveformFilterIF filter);
    public WaveformFilterIF getWaveformFilter();
    public boolean hasWaveformFilter();
    public Waveform filter(Waveform wfIn);
    public void setScanWaveformTimeWindow(boolean tf);
    public boolean getScanWaveformTimeWindow();

   // Apply correction to Channel traveltime calculation
    public void setGlobalCorrection(double correction);
    public double getGlobalCorrection();

   // Return cutoff (chanel rejection) distance proportional to Magnitude size
    public double getDistanceCutoff(double magValue);
    public double getDistanceCutoff(Magnitude aMag);
    public boolean isWithinDistanceCutoff(Channelable obj, double magValue);

   // Channel meets criteria appropiate for this location method
    public boolean isIncludedComponent(Channelable obj);

    // Input is of type appropiate for method to calculate location.
    public boolean isIncludedReadingType(JasiReadingIF jr);
    public Class getReadingClass();

   // Reject channel with source distance dist >  distKm.
    public void setMaxDistance(double distKm);
    public double getMaxDistance();

   // Include channel with source distance < distKm.
    public void setMinDistance(double distKm); 
    public double getMinDistance();

    // Maximum number of channels to include in location calcuation.
    public int getMaxChannels();
    public void setMaxChannels(int maxChannels);

    public void setLookUp(boolean tf);

    // Set true, channels without corrections are rejected from summary calculation. 
    public void setRequireCorrection(boolean tf);
    public boolean getRequireCorrection();

    // Reject phases whose observed traveltime residual > value.
    public void setTrimResidual(double value);
    public double getTrimResidual();

    public Phase createAssocPhaseDataFromWaveform(Solution aSol, Waveform wf);
    public List createAssocPhaseDataFromWaveformList(Solution aSol, List wfList);

    public Double getTravelTimeCorr(Phase jr) throws WrongDataTypeException;
    public boolean hasTravelTimeCorr(Phase jr) throws WrongDataTypeException;

    // If the input channel distance from source is greater than the last set
    // set cutoff distance and not less than the last set mininum distance,
    // the input attributes are set such that is not included in the 
    // calculation. A no-op if no trim distance cutoff value was set. 
    // Returns true if input is trimmed, i.e. attributes were modified to indicate
    // rejection, false if input is acceptable, or a no-op.
    public boolean trimByDistance(JasiReadingIF jr, double cutoff); // ?
    // the value last set for the trim residual, input attributes are set such that
    // it is not included in the calculation.
    // A no-op if no trim maximum distance cutoff value was set. 
    // Returns true if input is trimmed, i.e. attributes were modified to indicate
    // rejection, false if input is acceptable, or a no-op.
    public boolean trimByResidual(JasiReadingIF jr, double residual); // ?

    //public boolean trimBySummaryStatistics(Solution sol);
    public double getInputWeight(JasiReadingIF jr);
    public void assignWeight(JasiReadingIF jr) throws WrongDataTypeException ;

    // Returns true if input reading list elements were changed based upon filter criteria.
    public boolean filterDataBeforeLocation(Solution sol) throws WrongDataTypeException ;
    public boolean filterDataAfterLocation(Solution sol) throws WrongDataTypeException ;
    //Undeleted, valid data, weight can be 0.
    public List getValidPhaseData(Solution sol);
    //Undeleted, valid data, with weight > 0.
    public List getDataUsedForLocation(Solution sol);
    public List getDataUsedForLocation(List aList);

    // Returns true if reading data is sufficient to contribute to Location.
    public boolean isValid(JasiReadingIF jr);

    public int getMinValidReadings();
    public void setMinValidReadings(int count);

    // Flags input as virtually deleted.
    public void deleteReading(JasiReadingIF jr);
    // Sets input summary weight to zero, implementation could also delete reading.
    public void rejectReading(JasiReadingIF jr);
    //
    public boolean summaryChannelType(Channelable type) ;
    public boolean filterChannelType(Channelable type) ;
    public boolean acceptChannelType(Channelable type) ;

    public boolean summaryChannelType(String type) ;
    public boolean filterChannelType(String type) ;
    public boolean acceptChannelType(String type) ;

    public String [] getSummaryChannelTypes() ;
    public String [] getFilterChannelTypes() ;
    public String [] getAcceptableChannelTypes() ;

    public void logHeader();
    public void logChannelResidual(JasiReadingIF jr);

    public void setDeleteRejected(boolean tf) ;
    public void setIncludedReadingTypes(Object [] types);

    // like mag data, but should "calibration" be changed to "correction"? 
    public ChannelDataTypeMapListIF createDefaultCorrectionList() ;
    public void setDefaultCalibrationFactory(AbstractChannelCalibration calibr);
    public void setCalibrList(ChannelDataTypeMapListIF aList) ;
    public ChannelCorrectionDataIF getCalibrationFor(JasiReadingIF jr) ;
    public ChannelCorrectionDataIF getCalibrationFor(Channelable chan, java.util.Date date) ;
    public void loadCalibrations(java.util.Date date) ;
*/

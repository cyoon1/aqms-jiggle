package org.trinet.util.locationengines;

public interface LocationServiceDescIF {
        public String getLocatorName() ;
        public String getServiceName() ;
        public String getIPAddress() ;
        public int getPort() ;
        public int getServerTimeoutMillis() ;
        public void setLocatorName(String type) ;
        public void setServiceName(String name) ;
        public void setServerTimeoutMillis(int timeout) ;
        public void setIPAddress(String ipAddress) ;
        public void setPort(int port) ;
        public String describeConnection();
}

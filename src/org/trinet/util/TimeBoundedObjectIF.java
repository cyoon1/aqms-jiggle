package org.trinet.util;
/**
 * An object that is associated with a particular time span. For example, a channel
 * gain has a particular value only for a period of time. It will change when
 * components are changed or, perhaps, when it is recalibrated.
 */
public interface TimeBoundedObjectIF {

  /** The timespan for for which this object is valid. */
  TimeSpan timespan = new TimeSpan();

  /** Returns 'true' if epochtime is in the time span for which this object
   * is valid. */
  abstract public boolean isValidAt(double epochtime) ;

  /** Return the timespan for for which this object is valid. */
  abstract public TimeSpan getTimeSpan() ;

  /** Set the timespan for for which this object is valid. */
  abstract public void setTimespan(TimeSpan timespan);

}

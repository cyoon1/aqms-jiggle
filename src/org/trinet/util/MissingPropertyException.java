
package org.trinet.util;
public class MissingPropertyException extends RuntimeException {
    public MissingPropertyException() {
  super();
    }

    public MissingPropertyException(String message) {
  super(message);
    }
}

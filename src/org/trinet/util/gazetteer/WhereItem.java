package org.trinet.util.gazetteer;
/** Holds the data represented by a gazetteer table row object and 
* the DistanceAzimuthElevation from the gazetteer reference location to a target location.
* Includes a method to change the reference target.
* The equals(...), compareTo(...) methods are provided for distance comparison to enable sorting capability. 
* fromWhereString(...) formats the distance, azimuth and elevation data for summary reporting.
*/
public class WhereItem implements WhereSummary {
    DistanceAzimuthElevation distAzElev;
    GazetteerData data;

    public WhereItem(GazetteerData data) {
        this.data = data;
    }

    public WhereItem(Geoidal target, GazetteerData data) {
        this(data);
        setDistanceAzimuthElevation(target);
    }

/** Sets the distance, azimuth, and elevation using the specified input target from the gazetteer location data.
* Returned object has the default depth/elevation units set to kilometers.
*/
    public void setDistanceAzimuthElevation(Geoidal target) {
        distAzElev = DistanceAzimuthElevation.create(GeoidalUnits.DEGREES);
        distAzElev.setDistanceAzimuthElevation(data.getGeoidal(), target.getGeoidal());
    }

/* Sets the distance, azimuth, and elevation using the specified input target from the gazetteer location data.
* Depth/elevation input parameter units assumed to be kilometers.
*/
    public void setDistanceAzimuthElevation(double targetLat, double targetLon, double targetZ) {
        distAzElev = DistanceAzimuthElevation.create(GeoidalUnits.DEGREES);
        distAzElev.setDistanceAzimuthElevation(data.getLat(), data.getLon(),
                                GeoidalConvert.toKm(data.getZ(), data.getZUnits()),
                                targetLat, targetLon, targetZ);
    }

/** Returns the horizontal datum distance to the target from this gazetteer location, in the specified distance units.
*/
    public double getDistance(GeoidalUnits distanceUnits) {
        return distAzElev.getDistance(distanceUnits);
    }

/** Returns the horizontal datum distance to the target from this gazetteer location, in the default units.
*/
    public double getDistance() {
        return distAzElev.getDistance(null);
    }

/** Returns the DistanceAzimuthElevation of the target relative to this gazetteer location.
*/
    public DistanceAzimuthElevation getDistanceAzimuthElevation() {
        return distAzElev;
    }

/** Returns the GazetteerData object for this gazetteer location.
*/
    public GazetteerData getGazetteerData() {
        return data;
    }

/** Uses distance comparision */
    public boolean equals(Object object) {
        if (object == null) return false;
        try {
            if (((WhereItem) object).getDistance() == this.getDistance()) return true; 
        }
        catch (ClassCastException ex) {
        }
        return false;
    }

/** Uses distance comparision */
    public int compareTo(Object object) throws ClassCastException {
        if (! (object instanceof WhereItem)) throw new ClassCastException ("WhereItem compareto()");
        if (((WhereItem) object).getDistance() == this.getDistance()) return 0; 
        else if (((WhereItem) object).getDistance() >= this.getDistance()) return -1; 
        else return 1;
    }

/** Returns two letter string code describing state in which this item is located.
* Returns empty string if state is undefined.
*/
    public String getState() {
        String state = data.getState();
        return ( (state == null) || state.equalsIgnoreCase("NULL") ) ? "" :  state.trim();
    }
        
/** Returns the name of the place described by this item.
* Returns empty string if name is undefined.
*/
    public String getName() {
        String name = data.getName();
        return ( (name == null) || name.equalsIgnoreCase("NULL") ) ? "" : name.trim();        
    }

/** Returns String of the form place name, state plus any database remark if includeRemark == true. */
    public String fromPlaceString(boolean includeRemark) {
        StringBuffer sb = new StringBuffer(512);
        //sb.append(" from "); - aww 06/15/2006
        sb.append(getName());
        String state = getState();
        if (state != "") {
            sb.append(", ");
            sb.append(state);
        }
        if (includeRemark) {
            String remark = getRemark();
            if (remark != "") {
                sb.append(" ");
                sb.append(remark);
            }
        }
        return sb.toString();
    }

/** Returns String reporting distance, azimuth, elevation to gazetteer placename.
*/
    public String fromWhereString(GeoidalUnits distanceUnits, boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithUnits(distanceUnits));
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

/** Convenience wrapper of fromWhereString(...). */
    public String fromWhereStringKm(boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithKm());
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

    public String fromWhereStringMiles(boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithMiles());
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

/** Returns the REMARK column string found in the gazetteer table.
*/
    public String getRemark() {
        String remark = data.getRemark();
        return ( (remark == null) || remark.equalsIgnoreCase("NULL") ) ? "" : remark.trim();
    }

/** String resulting from calling toString() on data member objects.
* Uses default distance units.
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toString()).append(" ").append(data.toString());
        return sb.toString();
    }

/** String resulting from calling toString() on data member objects. 
* Uses specified distance units.
*/
    public String toString(GeoidalUnits distanceUnits) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toString(distanceUnits)).append(" ").append(data.toString());
        return sb.toString();
    }
}

package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;

import org.trinet.jasi.DataSource;
import java.sql.*;
import java.util.*;

/** Every constructor and method assumes inputs are in units of decimal degrees and kilometers (depth or elevation).
*/
public abstract class WhereAmI {
    GeoidalUnits distanceUnits = GeoidalUnits.KILOMETERS;
    Geoidal reference;
    Connection conn;
    String prettyName = "";
    boolean includeRemark = true;
    Vector items;

/** Default constructor does not set a database connection or the reference target point for gazetteer table data comparison.
*/
    WhereAmI() { }

/** Constructor sets the database connection does not set the reference target point for gazetteer table data comparison.
*/
    WhereAmI(Connection conn) {
        setConnection(conn);
    }

/** Constructor sets a database connection and the reference target point for gazetteer table data comparison.
*/
    WhereAmI(Connection conn, Geoidal reference) {
        this(conn);
        this.reference = reference;
    }

/** Constructor the reference target point for gazetteer table data comparison, does not set a database connection.
*/
    WhereAmI(Geoidal reference) {
        this();
        this.reference = reference;
    }

/** Constructor the reference target point for gazetteer table data comparison, does not set a database connection.
* Depth or elevation in assumed to be in kilometers units.
*/
    WhereAmI(double lat, double lon, double z) {
        this((Geoidal) new LatLonZ(lat, lon, z));
    }

    private String getPrettyName() {
        return prettyName;
    }

/** Sets the distance units for the distance field in reporting strings returned by from(...) methods.
*/
    public void setDistanceUnits(GeoidalUnits distanceUnits) {
        if (distanceUnits == null) distanceUnits = GeoidalUnits.KILOMETERS;
        this.distanceUnits = distanceUnits;
    }

/** true == return if any, the database REMARK column appended to the reporting string.
*/
    public void setIncludeRemark(boolean includeRemark) {
        this.includeRemark = includeRemark;
    }

/** Sets the JDBC database connection for gazetteer table data retrieval.
*/
    public void setConnection(Connection conn) {
        this.conn = conn;
    }
    /**
     * Make a connection. If we are running in the context of the Oracle server,
     * make the "default" connection to the local environment. Otherwise, use
     * the hardwired info which is used for testing outside the server.
     */
    public Connection getConnection() {
      // use default if none defined
      if (conn == null) conn = DataSource.getConnection();
      try {
        if (conn.isClosed()) {
          System.out.println("Connection is closed, reestablishing...");
          conn = null;
          conn = DataSource.getConnection();
        }
      }
      catch (SQLException ex) {
        System.out.println("Error checking conn object.");
      }
      return conn;
    }

/** Sets the reference target point for gazetteer table data comparison, does not set a database connection.
*/
    public void setReference(Geoidal reference) {
        this.reference = reference;
    }

/** Sets the reference target point for gazetteer table data comparison, does not set a database connection.
* Depth or elevation in assumed to be in kilometers units.
*/
    public void setReference(double lat, double lon, double z) {
        this.reference = (Geoidal) new LatLonZ(lat, lon, z);
    }

/** Implemented by subclasses of different gazetteer datatypes.*/
    abstract protected Vector getDatabaseData() ;

/** Returns array resulting from comparison of reference point to gazetteer data.
* If input count exceeds the know gazetteer table entries the maximum entries are returned.
*/
    public WhereSummary [] getClosestItemsByCount(int count) {
        if (count <= 0) return null;
        if (reference == null) throw new NullPointerException("WhereAmI: getClosestItemsByCount null reference point object");

        Vector items = getDatabaseData();
        if (items == null || items.size() == 0) return null;

        int end = count;
        if (end > items.size()) end = items.size();

        double [] dist = new double [items.size()];
        for (int index = 0; index < items.size(); index++) {
           dist[index] = ((WhereSummary) items.elementAt(index)).getDistance(distanceUnits);
           // DDG added 5/6/03 - null lat/lon (like IRIGE) was returning NaN
           // which showed up 1st in sort order!!
           if (Double.isNaN(dist[index])) dist[index] = Double.MAX_VALUE;
        }

        int [] sortOrder = IndexSort.getSortedIndexes(dist);

        WhereSummary [] whereItems = new WhereSummary [end];
        for (int index = 0; index < end; index++) {
            whereItems[index] = (WhereSummary) items.elementAt(sortOrder[index]);
        }

//        System.out.println("return db item count: " + end);
        return whereItems;
    }

    public String reportClosestKmByCount(int count, boolean includeRemark) {
        return reportClosestByCount(count, includeRemark, GeoidalUnits.KILOMETERS);
    }

    public String reportClosestMilesByCount(int count, boolean includeRemark) {
        return reportClosestByCount(count, includeRemark, GeoidalUnits.MILES);
    }

/** Returns String resulting from comparison of the set reference target point to gazetteer data.
* Returns one line per count entry, if input count exceeds table entries the maximum entries are returned.
* If includeRemark == true concatenates REMARK column to reporting string.
* Distances are reported in the specified input units.
*/
    public String reportClosestByCount(int count, boolean includeRemark, GeoidalUnits distanceUnits) {
        WhereSummary [] whereItems = getClosestItemsByCount(count);
        if (whereItems == null || whereItems.length == 0) {
            return  ""; //  No data found for closest " + getPrettyName(); //  + "\n";
        }

        if (distanceUnits == null) distanceUnits = GeoidalUnits.KILOMETERS;

        StringBuffer sb = new StringBuffer(count * 80);
        int end = whereItems.length;
        for (int index = 0; index < end; index++) {
            if (index > 0) sb.append("\n");
            //sb.append(whereItems[index].fromWhereString(distanceUnits, includeRemark));
            sb.append(whereItems[index].fromWhereString(null, includeRemark));
        }
        return sb.toString();
    }

/** Returns String resulting from comparison of the set reference target point to gazetteer data.
* Returns one line per count entry, if input count exceeds table entries the maximum entries are returned.
* If includeRemark == true concatenates REMARK column to reporting string.
* Distances are reported in the specified input units.
*/
    public String reportClosestByCount(int count)
    {
      return(reportClosestByCount(count,this.includeRemark,this.distanceUnits));
    }


/** Sets the reference point to the specified input; returns the String describing the closest data entry
* resulting from a gazetteer comparison.
*/
    public String from(double lat, double lon) {
        return from(lat, lon, 0., 1);
    }
    public String from(double lat, double lon, int number) {
        return from(lat, lon, 0., number);
    }
/** Sets the reference point to the specified input; returns the String describing the closest data entry
* resulting from a gazetteer comparison.
* Depth or elevation in assumed to be in kilometers units.
*/
    public String from(double lat, double lon, double z) {
        return from(lat, lon, z, 1);
    }
/** Sets the reference point to the specified input; returns the reporting String resulting from a gazetteer comparison.
* Depth or elevation in assumed to be in kilometers units.
* Count is the number of entries to report.
*/
    public String from(int lat, double latmin, int lon, double lonmin, double  z, int number) {
        return from(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin),
                 z, number);
    }
/** Sets the reference point to the specified input; returns the reporting String resulting from a gazetteer comparison.
* Depth or elevation in assumed to be in kilometers units.
* Count is the number of entries to report.
*/
    public String from(double lat, double lon, double z, int number) {
        setReference(lat, lon, z);
        return reportClosestByCount(number, includeRemark, distanceUnits);
    }

}

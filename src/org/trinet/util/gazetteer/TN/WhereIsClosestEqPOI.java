package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestEqPOI extends WhereIsClosest {

    public WhereIsClosestEqPOI() {
    }
    public WhereIsClosestEqPOI(Connection conn) {
	super(conn);
    }
    public WhereIsClosestEqPOI(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestEqPOI(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestEqPOI(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.EQPOI(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}

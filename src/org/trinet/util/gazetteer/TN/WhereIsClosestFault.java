package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestFault extends WhereIsClosest {

    public WhereIsClosestFault() {
    }
    public WhereIsClosestFault(Connection conn) {
	super(conn);
    }
    public WhereIsClosestFault(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestFault(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestFault(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.FAULT(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}

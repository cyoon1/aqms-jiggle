package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereBasicEqPOI extends WhereBasicPt {
    { prettyName = "P.O.I."; }

    public WhereBasicEqPOI() {
        super();
    }
    public WhereBasicEqPOI(Connection conn) {
        super(conn);
    }
    public WhereBasicEqPOI(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereBasicEqPOI(Geoidal reference) {
        super(reference);
    }
    public WhereBasicEqPOI(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for EQ_POI type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(SQL_QUERY_STRING, GazetteerType.getCode(GazetteerType.EQ_POI)); 
    }
}

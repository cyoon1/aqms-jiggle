package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
// Tiger FIPS codes
public class USAStateType {
    public static final USAStateType AZ = new USAStateType(4, "AZ");
    public static final USAStateType CA = new USAStateType(6,"CA");
    public static final USAStateType NV = new USAStateType(32, "NV");
    public static final USAStateType OR = new USAStateType(41, "OR");
    public static final USAStateType WA = new USAStateType(53, "WA");

    private int code; 
    private String name; 

    private USAStateType(int code, String name) {
        this.code =  code;
        this.name =  name;
    }
    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public boolean equals(Object object) {
        if (! (object instanceof USAStateType)) return false;
        if (getCode() == ((USAStateType) object).getCode()) {
            if (getName().equals( ((USAStateType) object).getName())) return true; 
        }
        return false;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(24);
        sb.append(String.valueOf(code)).append(" ").append(name);
        return sb.toString();
    }
}

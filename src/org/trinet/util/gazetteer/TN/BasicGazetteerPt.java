/** Data holding object for Gazetteer table data */
package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;

public class BasicGazetteerPt implements GazetteerData {
    double lat;
    double lon;
    double z; // make positive depth downward 6/15/2004 aww
    String name;
    String state;
    String remark;

    public BasicGazetteerPt () {}

    public BasicGazetteerPt (double lat, double lon) {
	this(lat, lon, 0., null, null, null);
    }
    public BasicGazetteerPt (double lat, double lon, double z) {
	this(lat, lon, z, null, null, null);
    }
    public BasicGazetteerPt (double lat, double lon, double z, String name) {
	this(lat, lon, z, name, null, null);
    }
    public BasicGazetteerPt (double lat, double lon, double z, String name, String state, String remark) {
	this.lat = lat;
	this.lon = lon;
	this.z = z;
	this.name = name;
	this.state = state;
	this.remark = remark;
    }

    public Geoidal getGeoidal() { return this;}

    public LatLonZ getLatLonZ() { return new LatLonZ(lat,lon,z);}

    public double getLat() { return lat;}
    public void setLat(double lat) { this.lat = lat;}

    public double getLon() { return lon;}
    public void setLon(double lon) { this.lon = lon;}

    public double getZ() { return z;}
    public void setZ(double z) { this.z = z;}
    public void setZ(double z, GeoidalUnits units) {
      this.z = z;
      setZUnits(units);
    }

    public GeoidalUnits getZUnits() {return GeoidalUnits.KILOMETERS;}
    public void setZUnits(GeoidalUnits units) {;}

    public String getName() { return name;}
    public void setName(String name) { this.name = name;}

    public String getState() { return state;}
    public void setState(String state) { this.state = state;}

    public String getRemark() { return remark;}
    public void setRemark(String remark) { this.remark = remark;}

    /** Return 'true' if this is a valid point. That is, the values of latitude,
     * longitude and z are not all 0.0 which is the default if they have not been set.*/
    public boolean hasLatLonZ() {
      return  ! (lat == 0.0 && lon == 0.0 && z == 0.0);
    }
}

package org.trinet.util.gazetteer;

public class GeoidalConvert {
    public static final double EARTH_RADIUS_KM = 6371.0072 ; // authalic radius
    public static final double RADIANS_PER_DEGREE = 0.01745329;
    public static final double MILES_PER_KM = 0.62137;
    public static final double FEET_PER_KM = 3280.83;
    public static final double KM_PER_DEGREE =  RADIANS_PER_DEGREE * EARTH_RADIUS_KM;
    public static final double DEGREE_MINUTES = 60.;
    public static final double DEGREE_SECONDS = 3600.;

/** Array of 4-character strings describing 16 cardinal compass point direction, for example "ENE ".
*/
    public static final String [] compassPoints = { "N  ", "NNE", "NE ", "ENE", "E  ", "ESE", "SE ", "SSE",
                        "S  ", "SSW", "SW ", "WSW", "W  ", "WNW", "NW ", "NNW"};


  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double kmToFeet(double km) {
        if (Double.isNaN(km)) return Double.NaN;
        return km * FEET_PER_KM;
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double kmToMiles(double km) {
        if (Double.isNaN(km)) return Double.NaN;
        return km * MILES_PER_KM;
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double feetToKm(double feet) {
        if (Double.isNaN(feet)) return Double.NaN;
        return feet / FEET_PER_KM;
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double milesToKm(double miles) {
        if (Double.isNaN(miles)) return Double.NaN;
        return miles / MILES_PER_KM;
    }

    public static double toKm(double number, GeoidalUnits gu) {
        if (Double.isNaN(number)) return Double.NaN;
        /*        if (gu == GeoidalUnits.KILOMETERS )return number;
        else if (gu == GeoidalUnits.METERS )return number/1000;
        else if (gu == GeoidalUnits.FEET )return number/FEET_PER_KM;
        else if (gu == GeoidalUnits.MILES )return number/MILES_PER_KM;
        */
        // DDG 7/27/2000 - else fails on serialized version
        if (gu.equals(GeoidalUnits.KILOMETERS ))return number;
        else if (gu.equals(GeoidalUnits.METERS ))return number/1000;
        else if (gu.equals(GeoidalUnits.FEET ))return number/FEET_PER_KM;
        else if (gu.equals(GeoidalUnits.MILES ))return number/MILES_PER_KM;

        else throw new IllegalArgumentException("GeoidalUnits not valid:" + gu.toString());
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double toDecimalDegrees(int deg, double minutes) {
        return toDecimalDegrees(deg, minutes, 0.);
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double toDecimalDegrees(int deg, double minutes, double seconds) {
        if ( deg >= 0)
            return (double) deg + Math.abs(minutes)/DEGREE_MINUTES + Math.abs(seconds)/DEGREE_SECONDS;
        else
            return (double) deg - Math.abs(minutes)/DEGREE_MINUTES - Math.abs(seconds)/DEGREE_SECONDS;
    }

/** Returns horizontal distance between points, no elevation used in calculation.*/
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double horizontalDistanceKmBetween(double lat1, double lon1, double lat2, double lon2) {
        //double x = Xkm(lat1, lon1, lat2, lon2);
        //double y = Ykm(lat1, lat2);
        //return  (Double.isNaN(y)) ? x :Math.sqrt(x*x+ y*y);
        return greatCircleDistanceKm(lat1, lon1, lat2, lon2);
    }

/** Returns horizontal distance between points, no elevation used in calculation.*/
    public static double horizontalDistanceMilesBetween(double lat1, double lon1, double lat2, double lon2) {
        return horizontalDistanceKmBetween(lat1, lon1, lat2, lon2) * MILES_PER_KM;
    }

/** Returns planar distance between points, no elevation used in calculation.*/
    public static double horizontalDistanceKmBetween(Geoidal g1, Geoidal g2) {
        if (g1.hasLatLonZ() && g2.hasLatLonZ())
          return horizontalDistanceKmBetween(g1.getLat(), g1.getLon(), g2.getLat(), g2.getLon());
        else
          return Double.NaN;
    }

/** Returns planar distance between points, no elevation used in calculation.*/
    public static double horizontalDistanceMilesBetween(Geoidal g1, Geoidal g2) {
        return horizontalDistanceMilesBetween(g1.getLat(), g1.getLon(), g2.getLat(), g2.getLon());
    }

/** Returns distance between points, elevation used in calculation.
* Input depth/elevation parameter is assumed to be in km units.
*/
    public static double distanceKmBetween(double lat1, double lon1, double z1, double lat2, double lon2, double z2) {
        double hd = horizontalDistanceKmBetween(lat1, lon1, lat2, lon2);
        double vd = Zkm(z1, z2, GeoidalUnits.KILOMETERS);
        return (Double.isNaN(vd)) ? hd : Math.sqrt(hd*hd + vd*vd);
    }

/** Returns distance between points, elevation used in calculation.*/
    public static double distanceKmBetween(Geoidal g1, Geoidal g2) {
        double hd = horizontalDistanceKmBetween(g1,g2);
        double vd = Zkm(g1,g2);

        return (Double.isNaN(vd)) ? hd : Math.sqrt(hd*hd + vd*vd);
    }

/** Returns azimuth degrees from point1 to point2.*/
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double azimuthDegreesTo(double lat1, double lon1, double lat2, double lon2) {
        //double x = Xkm(lat1, lon1, lat2, lon2);
        //double delta = (lon1 - lon2);  
        // 1 > 2 same hemisphere or wrapped around
        //if ( (delta > 0. && delta < 180.) || (delta < -180.) ) {
        //    x = -x;
        //}
        //double y = Ykm(lat1, lat2);
        //double az = Math.atan2(x, y)/RADIANS_PER_DEGREE;
        double az = bearing(lat1, lon1, lat2, lon2);
        if (az < 0.0) az = az + 360.0;
        return az;
    }
/** Returns azimuth degrees from point1 to point2.*/
    public static double azimuthDegreesTo(Geoidal g1, Geoidal g2) {
        return azimuthDegreesTo(g1.getLat(), g1.getLon(), g2.getLat(), g2.getLon());
    }

/** Returns elevation degrees from point1 to point2.
* Input depth/elevation parameter is assumed to be in km units.
*/
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static double elevationDegreesTo(double lat1, double lon1, double z1, double lat2, double lon2, double z2) {
        //double x = Xkm(lat1, lon1, lat2, lon2);
        //double y = Ykm(lat1, lat2);
        //double dist = Math.sqrt(x*x+ y*y);
        double dist = greatCircleDistanceKm(lat1, lon1, lat2, lon2);
        double z = Zkm(z1, z2, GeoidalUnits.KILOMETERS);
        return (Double.isNaN(z)) ? 0. : Math.atan2(z, dist)/RADIANS_PER_DEGREE;
    }

/** Returns elevation degrees from point1 to point2.*/
    public static double elevationDegreesTo(Geoidal g1, Geoidal g2) {
        //double x = Xkm(g1.getLat(), g1.getLon(), g2.getLat(), g2.getLon());
        //double y = Ykm(g1.getLat(), g2.getLat());
        //double dist = Math.sqrt(x*x+ y*y);
        double dist = greatCircleDistanceKm(g1.getLat(), g1.getLon(), g2.getLat(), g2.getLon());
        double z = Zkm(g1, g2);
        return (Double.isNaN(z)) ? 0. : Math.atan2(z, dist)/RADIANS_PER_DEGREE;
    }

    private static double Xkm(double lat1, double lon1, double lat2, double lon2) {
        double dLon = Math.abs(lon1 - lon2); 
        if (dLon > 180.) dLon = dLon - 180.;
        dLon = Math.min(dLon, 180.-dLon);
        return (dLon) * KM_PER_DEGREE * Math.cos( ((lat2 - lat1)/2.0 + lat1) * RADIANS_PER_DEGREE );
    }
    private static double Ykm(double lat1, double lat2) {
        return (lat2 - lat1) * KM_PER_DEGREE;
    }
    private static double Zkm(double z1, double z2, GeoidalUnits gu) {
        return toKm(z2, gu) - toKm(z1, gu);
    }
    private static double Zkm(Geoidal g1, Geoidal g2) {
        return toKm(g2.getZ(), g2.getZUnits()) - toKm(g1.getZ(), g1.getZUnits());
    }

/** Returns String describing compass direction, for specified azimuth degrees, for example 200.0 == "SSW " */
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static String directionString(double azimuth) {
        double azTmp = azimuth + 11.25;
        if (azTmp >= 360.) azTmp = azTmp - 360.;
        int index = (int) (azTmp/22.5);
        return compassPoints[index];
    }

    ////////
    public static double greatCircleDistanceKm(double lat1, double long1, double lat2, double long2) { 
        final double degToRad= Math.PI / 180.0; 
        double phi1 = lat1 * degToRad; 
        double phi2 = lat2 * degToRad; 
        double lam1 = long1 * degToRad; 
        double lam2 = long2 * degToRad; 
        return EARTH_RADIUS_KM * Math.acos( Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1) ); 
    } 

    /*
    public static double bearing(double lat1, double long1, double lat2, double long2) { 
        final double degToRad = Math.PI / 180.0; 
        double phi1 = lat1 * degToRad; 
        double phi2 = lat2 * degToRad; 
        double lam1 = long1 * degToRad; 
        double lam2 = long2 * degToRad; 
        return ((Math.atan2(
                     Math.sin(lam2-lam1)*Math.cos(phi2), 
                     Math.cos(phi1)*Math.sin(phi2) - Math.sin(phi1)*Math.cos(phi2)*Math.cos(lam2-lam1)
                  ) * 180/Math.PI) +360.) % 360.; 
    } 
    */
    
    ///////
    public static double bearing(double lat1, double lon1, double lat2, double lon2){
        double longitude1 = lon1;
        double longitude2 = lon2;
        double latitude1 = Math.toRadians(lat1);
        double latitude2 = Math.toRadians(lat2);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
  
        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }

    ////////
    public static final void main(String[] args) {
        System.out.println("AzDeg to: " +GeoidalConvert.azimuthDegreesTo(
                    Double.parseDouble(args[0]),
                    Double.parseDouble(args[1]),
                    Double.parseDouble(args[2]),
                    Double.parseDouble(args[3])
              )
        );

        System.out.println("AzDeg to: " +GeoidalConvert.azimuthDegreesTo( 80.,179.5,79.5,-179.5));
        System.out.println("AzDeg to: " +GeoidalConvert.azimuthDegreesTo( 79.5,-179.5,80.,179.5));

        System.out.println("Horiz Km: " +GeoidalConvert.horizontalDistanceKmBetween(
                    Double.parseDouble(args[0]),
                    Double.parseDouble(args[1]),
                    Double.parseDouble(args[2]),
                    Double.parseDouble(args[3])
                    )
       );
        System.out.println("Elv Deg: " +GeoidalConvert.elevationDegreesTo(
                    Double.parseDouble(args[0]),
                    Double.parseDouble(args[1]),
                    0.,
                    Double.parseDouble(args[2]),
                    Double.parseDouble(args[3]),
                    1.
                    )
       );
        System.out.println("Elv Deg: " +GeoidalConvert.elevationDegreesTo(
                    Double.parseDouble(args[0]),
                    Double.parseDouble(args[1]),
                    0.,
                    Double.parseDouble(args[2]),
                    Double.parseDouble(args[3]),
                    1000.
                    )
       );
    }
    //
}

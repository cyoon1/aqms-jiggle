package org.trinet.util.gazetteer;

import java.util.Collection;
import java.sql.Connection;

public interface WhereIsEngineIF {
    public void setDistanceUnits(GeoidalUnits units) ;
    public void setDistanceUnitsKm() ;
    public void setDistanceUnitsMiles() ;

    public String where() ;
    public String where(double lat, double lon) ;
    public String where(int lat, double latmin, int lon, double lonmin, double  z) ;
    public String where(double lat, double lon, double z) ;
    public String whereType(double lat, double lon, double z, String type) ;

    //public String where(int lat, double latmin, int lon, double lonmin, double  z, int count) ;
    //public String where(double lat, double lon, double z, int count) ;
    //public String whereType(double lat, double lon, double z, int count, String type) ;

    public String whereType(String type) ;
    public Collection whereSummary() ;
    public WhereSummary whereSummaryType(String type) ;
    public void setReference(Geoidal latLonZ) ;
    public void setReference(double lat, double lon) ;
    public void setReference(double lat, double lon, double z) ;
    public void setConnection(Connection conn) ;
    public void closeStatements() ;
}

package org.trinet.util.gazetteer;
public interface DistanceBearingIF {

    public static final double NULL_DIST    = 99999.;
    public static final double NULL_AZIMUTH = Double.NaN;

    public void   setDistance(double distance);
    public double getDistance();
    public void   setAzimuth(double azimuth);
    public double getAzimuth();
}

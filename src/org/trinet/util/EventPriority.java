package org.trinet.util;
/** Given magnitude, origin time and quality for an event calculate the "priority" of the event. 
@deprecated 
@see org.trinet.jasi.EventPriority
*/
public class EventPriority {

      static final double magFactor = 5.0;
      static final double ageFactor = 1.0;
      static final double qualFactor = 2.0;

     public EventPriority() { }

     public static double getPriority (double mag, double age, double quality) {
            double safemag = (mag <= 0.) ?  .01 : mag; 
            // A larger magnitude, younger event, with lower quality has higher processing priority 
            return ( safemag*magFactor + (1.0/(age+0.1))*ageFactor + 1.0/(quality+0.1)*qualFactor);
     }

/*
     public static getPriorityOf (long evid) {

     }
     public static void main(String[] args) {

     for (double mag = 0; mag <= 6.0; mag = mag + 0.5) {
          for (double age = 0; age <= 6.0; age = age + 0.5) {
               for (double qual = 0; qual <= 1.0; qual = qual + 0.25) {

          System.out.println (mag+" "+age+" "+qual+"\t"+ (int) EventPriority.getPriority(mag, age, qual));

     }}}

     }
*/
}

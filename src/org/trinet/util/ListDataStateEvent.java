package org.trinet.util;
import javax.swing.event.*;
public class ListDataStateEvent extends ListDataEvent {
  public static final int ORDER_CHANGED = 8;
  public static final int STATE_CHANGED = 9;
  StateChange stateChange;

  public ListDataStateEvent(Object src, int type, int index0, int index1) {
    this(src, type, index0, index1, null);
  }

  public ListDataStateEvent(Object src, int type, int index0, int index1,
                  StateChange stateChange) {
    super(src, type, index0, index1);
    this.stateChange = stateChange;
  }

  public StateChange getStateChange() { return stateChange; }
}

package org.trinet.util;
import java.beans.*;

public interface ValueChangedListener extends PropertyChangeListener {
    public void valueChanged(ValueChangedEvent evt);
}

package org.trinet.util;

/**
* Specifies a lower and a upper limit (bound) for a range of character type.
*/
public class CharRange extends NumberRange implements Cloneable {

/**
 * Default constructor, no limits.
 */ 
    
    public CharRange() {
        super();
    }
/**
* Specify limit, null == no limit.
*/
    public CharRange(Character min, Character max) {
	this(min.charValue(), max.charValue());
    }

/** Returns string of the form "Min: min Max: max" */
    public CharRange(char min, char max) {
	super(new Integer((int) min), new Integer((int) max));
    }

/** Override default */
    public Object clone() {
        return (CharRange) super.clone();
    }

    public boolean after(Character value) {
        return after(value.charValue());
    }
    public boolean before(Character value) {
        return before(value.charValue());
    }
    public boolean excludes(Character value) {
        return excludes(value.charValue());
    }
    public boolean excludes(Character min, Character max) {
        return excludes(min.charValue(), max.charValue());
    }
    public boolean contains(Character min, Character max) {
        return contains(min.charValue(), min.charValue());
    }
    public boolean contains(Character value) {
        return contains(value.charValue());
    }
    public boolean overlaps(Character min, Character max) {
        return overlaps(min.charValue(), max.charValue());
    }
    public boolean within(Character min, Character max) {
        return within(min.charValue(), max.charValue());
    }
    public void setMin(Character min) {
        setMin(min.charValue());
    }
    public void setMax(Character max) {
        setMax(max.charValue());
    }
    public void setLimits(Character min, Character max) {
         setLimits(min.charValue(), max.charValue());
    }
    public void include(Character value) {
        include(value.charValue());
    }

    public boolean after(char value) {
        return after(new Integer((int) value));
    }
    public boolean before(char value) {
        return before(new Integer((int) value));
    }
    public boolean excludes(char value) {
        return excludes(new Integer((int) value));
    }
    public boolean excludes(char min, char max) {
        return excludes(new Integer((int) min), new Integer((int) max));
    }
    public boolean contains(char min, char max) {
        return contains(new Integer((int) min), new Integer((int) max));
    }
    public boolean contains(char value) {
        return contains(new Integer((int) value));
    }
    public boolean overlaps(char min, char max) {
        return overlaps(new Integer((int) min), new Integer((int) max));
    }
    public boolean within(char min, char max) {
        return within(new Integer((int) min), new Integer((int) max));
    }
    public void setMin(char min) {
        setMin(new Integer((int) min));
    }
    public void setMax(char max) {
        setMax(new Integer((int) max));
    }
    public void setLimits(char min, char max) {
        setLimits(new Integer((int) min), new Integer((int) max));
    }
    public void include(char value) {
        include(new Integer((int) value));
    }
} // end of CharRange class

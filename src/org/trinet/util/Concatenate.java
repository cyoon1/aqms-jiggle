package org.trinet.util;
import java.text.*;
import java.util.*;

/** Utility class with methods for fixed field width formatting of decimal numbers into strings.
* Format disables thousands, millions grouping "," and the decimal separator not shown for integers.
* format(...) methods returns the input StringBuffer appended with the formatted input number.
* If the method input StringBuffer is null, the method creates one by default.<BR>
* If you want all formatted numbers to have the same constant field width, specify a maximum integer
* width that is larger than the decimals in largest signed number being formatted, all numbers 
* with fewer decimal places will be left-padded.
*/
public class Concatenate {
  static NumberFormat nf;
  static DecimalFormat df;
  static FieldPosition fp;
  static char [] pad;
  static char [] zeroPattern;
  static {
    nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    fp = new FieldPosition(df.INTEGER_FIELD);
    pad = new char[132];
    Arrays.fill(pad,' '); // blank padding of field width.
    zeroPattern = new char[132];
    Arrays.fill(zeroPattern,'0'); // all digits visible even if zero.
  }

/** Default formatting assumes left blank padding of number strings. */
  public Concatenate() {}

/** Pad field width with specified char in the string format */
  public static void setPadChar(char padchar) {
    Arrays.fill(pad,padchar);
  }

/** Width of the formatted field is variable with a minimum of 1 integer place.
* Example: format(sb, number) creates a string of the form "#0". 
* Thus 0 would be formatted into a string as "0" and 1234 would be "1234".
*/
  static public StringBuffer format(StringBuffer sb, long number) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    return sb;
  }

/** Width of the formatted field is variable with a minimum of 1 integer place.
* Example: format(sb, number) creates a string of the form "#0.#".
* At least 1 numbers in the integer portion and zero or more fraction digits.
* Thus 3. would be formatted into a string as "3." and .70 as "0.7" and 1.234 as "1.234".
*/
  static public StringBuffer format(StringBuffer sb, double number) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0.#");
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    return sb;
  }

/** Mininum width of the formatted field = specified integerFieldWidth.
* Example: format(sb, number, 5) creates a string of the form "####0". A mininum string length of 5.
* Thus 3 would be formatted into the buffer as "    3". and 1234567 as "1234567".
*/
  static public StringBuffer format(StringBuffer sb, long number, int intFieldWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) sb.insert(istart,pad,0,npad);
    return sb;
  }

/** Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth).
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* Example: format(sb, number, 5, 3) creates a string of the form "##000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "  003" and 1234567 as "1234567".
*/
  static public StringBuffer format(StringBuffer sb, long number, int intFieldWidth, int nZeroWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    if (nZeroWidth > 1) formatPatternBuffer.append(zeroPattern, (int) 0, nZeroWidth-1);
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) sb.insert(istart,pad,0,npad);
    return sb;
  }

/** Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth + ".").
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* Example: format(sb, number, 5, 3) creates a string of the form "####0.000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "    3.000" and 1234567.89 as "1234567.890".
*/
  static public StringBuffer format(StringBuffer sb, double number, int intFieldWidth, int minFracWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0.");
    formatPatternBuffer.append(zeroPattern, (int) 0, minFracWidth);
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) sb.insert(istart,pad,0,npad);
    return sb;
  }

/** Returns the input StringBuffer appended with a formatted input number.
* If the input StringBuffer is null, creates one by default.
* Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth + ".").
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* The nZeroWidth parameter refers to the minimum number of integer places (left of the decimal).<BR>
* Example: format(sb, number, 5, 3, 2) creates a string of the form "###00.000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "   03.000" and 1234567 as "1234567.000".
*/
  static public StringBuffer format(StringBuffer sb, double number, int intFieldWidth, int minFracWidth, int nZeroWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    if (nZeroWidth > 1) formatPatternBuffer.append(zeroPattern, (int) 0, nZeroWidth-1);
    formatPatternBuffer.append(".");
    formatPatternBuffer.append(zeroPattern, (int) 0, minFracWidth);
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) sb.insert(istart,pad,0,npad);
    return sb;
  }

/**
  * Returns the input StringBuffer with input String appended,
  * creates new StringBuffer if input reference is null.
  * If specified input width <= 0 returns StringBuffer reference;
  * Field width must be <= 132 characters.
  * Padding is appended after the String if input String length is less than desired width.
  * Truncates String, ending it with  "^" if input String length is greater than desired width.
*/
  static public StringBuffer leftJustify(StringBuffer sb, String str, int intFieldWidth) {
    if (sb == null) sb = new StringBuffer(132);
    if (intFieldWidth <= 0) return sb;
    if (intFieldWidth > pad.length) throw new IllegalArgumentException("field width must be < " + pad.length);
    if (str == null) return sb.append(pad, 0, intFieldWidth);

    int padSize = intFieldWidth - str.length();
    if (padSize < 0) {
        sb.append(str.substring( 0,intFieldWidth-1));
        sb.append('^');
        return sb;
    }
    sb.append(str);
    return (padSize == 0) ? sb : sb.append(pad, 0, padSize);
  }
/**
  * Returns input StringBuffer appended with input String,
  * creates new StringBuffer if input reference is null.
  * If specified input width <= 0 returns StringBuffer reference;
  * Field width must be <= 132 characters.
  * Padding is inserted before the String if input String length is less than desired width.
  * Truncates String, starting it with "^" if input String length is greater than desired width.
*/
  static public StringBuffer rightJustify(StringBuffer sb, String str, int intFieldWidth) {
    if (sb == null) sb = new StringBuffer(132);
    if (intFieldWidth <= 0) return sb;
    if (intFieldWidth > pad.length) throw new IllegalArgumentException("field width must be < " + pad.length);
    if (str == null) return sb.append(pad, 0, intFieldWidth+1);

    int padSize = intFieldWidth - str.length();
    if (padSize < 0) {
        sb.append('^');
        sb.append(str.substring(Math.abs(padSize+1), str.length()));
        return sb;
    }
    if (padSize > 0) sb.append(pad, 0, padSize);
    return sb.append(str);
  }

/*
  public static final void main(String args [] ) {
      StringBuffer sb = new StringBuffer(128);
      sb.append(" begin:\"");
      Concatenate.format(sb, 100l ) ;
      sb.append("\" begin:\"");
      Concatenate.format(sb, 100.01) ;
      sb.append("\" begin:\"");
      Concatenate.format(sb, 100, 8 ) ;
      sb.append("\" begin:\"");
      Concatenate.format(sb, 100, 8, 4) ;
      sb.append("\" begin:\"");
      Concatenate.format(sb, 100.111, 8, 2) ;
      sb.append("\" begin:\"");
      Concatenate.format(sb, 100.111, 8, 2, 4) ;
      sb.append("\" begin:\"");
      Concatenate.leftJustify(sb, " test", 8) ;
      sb.append("\" begin:\"");
      Concatenate.rightJustify(sb, "TEST", 8) ;
      sb.append("\".End");
      System.out.println(sb.toString());
  }
*/
}

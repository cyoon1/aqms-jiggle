package org.trinet.util;
//import java.util.regex.*;
import jregex.Pattern; // aww 02/15/2005 try this pkg

public class StringMatcher {
    protected String [] regex;
    protected int size = 0;
    protected int flags = 0;
    protected Pattern [] patterns;

    public StringMatcher() {}

    public StringMatcher(String [] regex) {
      this(regex, 0);
    }
    public StringMatcher(String [] regex, int patternFlags) {
      createPatternList(regex, patternFlags);
    }

    public void createPatternList(String [] regex) {
      createPatternList(regex, 0);
    }

    public void createPatternList(String [] regex, int flags) {
      this.size = regex.length;
      this.regex = regex;
      this.flags = flags;
      patterns = new Pattern[size];
      for (int idx =0; idx < size; idx++) {
        //patterns[idx] = (flags > 0) ? Pattern.compile(regex[idx], flags) : Pattern.compile(regex[idx]); // jdk1.4
        patterns[idx] = (flags > 0) ? new Pattern(regex[idx], flags) : new Pattern(regex[idx]); // jregex
      }
    }

    public boolean matches(String input) {
      for (int idx =0; idx < size; idx++) {
        if (patterns[idx].matcher(input).matches()) return true;
      }
      return false;
    }

    public int getPatternCount() { return size; }

    public String [] getInputRegex() {
      return regex;
    }

    public String [] getPatternRegex() {
      /*
      String [] regex = new String [size];
      for (int idx=0; idx < size; idx++) {
         regex[idx] = patterns[idx].pattern();// jdk1.4
      }
      */
      return regex;
    }

    /*
  static public final class Tester {
    static public final void main(String [] args) {
      DbStringMatcher sm = new DbStringMatcher(new String [] {"ABC"});
      System.out.println("ABC sm.matches(HHZ) f: " + sm.matches("HHZ"));
      sm.createPatternList(new String [] {"HLZ"});
      System.out.println("HLZ sm.matches(HLZ) t: " + sm.matches("HLZ"));
      sm.createPatternList(new String [] {"H_Z"});
      System.out.println("H_Z sm.matches(HHZ) f: " + sm.matches("HHZ"));
      sm.createPatternList(new String [] {"H%"});
      System.out.println("H%  sm.matches(HHZ) f: " + sm.matches("HHZ"));
      //sm.createPatternList(new String [] {"HHZ"}, Pattern.CASE_INSENSITIVE); // jdk1.4
      sm.createPatternList(new String [] {"HHZ"}, Pattern.IGNORE_CASE); // jregex
      System.out.println("HHZ sm.matches(hhz) t: " + sm.matches("hhz"));
    }
  }
  */
}

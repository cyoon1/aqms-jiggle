package org.trinet.util;

public interface NotchFilterIF extends FilterIF {
    double getNotchHz();
    double getNotchWidth();
    //void setNotchHz(double hz);
    //void setNotchWidth(double dw);
    void setNotch(double hz, double dw);
    boolean setNotch(int sampleRate, double rf, double dw, boolean reverse);
}

package org.trinet.util;

import java.util.Arrays;

/**
 * Static methods for statistical calculations. Most of these are based on
 * methods in the Descriptive.java class of the package /cern/jet/stat that was
 * included in the "colt" download. They were modified to accept primative types
 * as arguments rather then a cern.colt.list.DoubleArrayList type specific to
 * the Cern pakage that required dragging along lots of other libraries. Here's
 * the original copywrite stuff:<p>
 *
<tt>
Copyright <A9> 1999 CERN - European Organization for Nuclear Research.
Permission to use, copy, modify, distribute and sell this software and its
documentation for any purpose is hereby granted without fee, provided that the
above copyright notice appear in all copies and that both that copyright notice
and this permission notice appear in supporting documentation.  CERN makes no
representations about the suitability of this software for any purpose.  It is
provided "as is" without expressed or implied warranty.</tt><p>
 *
 * @author peter.gedeck@pharma.Novartis.com
 * @author wolfgang.hoschek@cern.ch
 * @version 0.91, 08-Dec-99
 *
 * @author Doug Given
 * @version */

public class Stats  {

    /** All methods are static so you can never create an instance.
     */
    protected Stats() { }

/**
 * Returns the largest member of a data sequence.
 * @throws IllegalArgumentException if data array has no members.
  */
    public static double max (double[] data) {
        return max(data, data.length);
    }

    public static double max (double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double max = data[size-1];
        for (int i = size-1; --i >= 0;) {
                if (data[i] > max) max = data[i];
        }
        return max;
    }

/**
 * Returns the smallest member of a data sequence.
 * @throws IllegalArgumentException if data array has no members.
  */
    public static double min (double[] data) {
        return min(data, data.length);
    }

    public static double min (double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double min = data[size-1];
        for (int i = size-1; --i >= 0;) {
                if (data[i] < min) min = data[i];
        }
        return min;
    }
/**
 * Returns the arithmetic mean of a data sequence.
 * That is <tt>Sum( data[i] ) / data.size()</tt>.
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double mean(double[] data) {
        return mean(data, data.length);
    }

    public static double mean(double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        return sum(data, size)/size;
    }

/**
 * Returns the arithmetic average of a data sequence. This identical to the mean;
 * That is <tt>Sum( data[i] ) / data.size()</tt>.
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double average(double[] data) {
        return mean (data, data.length);
    }
/**
 * Returns the mean deviation of a dataset.
 * That is <tt>Sum (Math.abs(data[i]-mean)) / data.size())</tt>.
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double meanDeviation(double[] data) {
        return meanDeviation(data, mean(data));
    }
    public static double meanDeviation(double[] data, int length) {
        return meanDeviation(data, mean(data, length), length);
    }
/**
 * Returns the mean deviation of a dataset.
 * That is <tt>Sum (Math.abs(data[i]-mean)) / data.size())</tt>.
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double meanDeviation(double[] data, double mean) {
        return  meanDeviation(data, mean, data.length);
    }
    public static double meanDeviation(double[] data, double mean, int length) {

        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double sum=0;
        for (int i=size; --i >= 0;) {
            sum += Math.abs(data[i]-mean);
        }
        return sum/size;
    }

/**
 * Returns the median deviation of a dataset.
 * That is the median of values |data[i]-median(data)|.
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double medianDeviation(double[] data) {
        return medianDeviation(data, median(data));
    }
    public static double medianDeviation(double[] data, int length) {
        return medianDeviation(data, median(data, length), length);
    }
    public static double medianDeviation(double[] data, double median) {
        return  medianDeviation(data, median, data.length);
    }
    public static double medianDeviation(double[] data, double median, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        double [] dev = new double [size];

        for (int i=0; i<size; i++) {
            dev[i] = Math.abs(data[i]-median);
        }
        return median(dev);
    }

/**
 * Returns the median of an array of doubles. Does not alter the original array. *
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double median(double[] data) {
        return median(data, data.length);
    }
/**
 * Returns the median of an array of doubles. Does not alter the original array. *
 * @throws IllegalArgumentException if data array has no members.
 */
    public static double median(double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        if (size == 1) return data[0];

        double data2[] = new double[size];
        System.arraycopy(data, 0, data2, 0, size); // copy the data array
        Arrays.sort(data2);

        int k = size / 2;
        return (Stats.isEven(size)) ? ((data2[k]+data2[k-1])/2.0) : data2[k];
    }

    /**
     * Return true if the number is even, else false.
     */
    public static boolean isEven (int number) {
        return (number%2 == 0);
    }

    /**
     * Return true if the number is odd, else false.
     */
    public static boolean isOdd (int number) {
        return !isEven(number);
    }

/**
 * Returns the sample variance of a data sequence. See Numerical Recipes page 611.
 * That is <tt>(sumOfSquares - mean*sum) / (size - 1)</tt> with <tt>mean = sum/size</tt>.
 *
 * @param size the number of elements of the data sequence.
 * @param sum <tt>== Sum( data[i] )</tt>.
 * @param sumOfSquares <tt>== Sum( data[i]*data[i] )</tt>.
 */
    /* This was from cern but seemed bogus to me, so I did it ala Recipes
    public static double variance(double[] data) {
        return variance(data, data.length);
    }
    public static double variance(double[] data, int length) {
         int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        double sum = Stats.sum(data, size);
        double mean = mean(data, size);
        return (Stats.sumOfSquares(data, size) - mean*sum) / (size - 1);
   }
   */

    public static double variance(double[] data) {
        return variance(data, data.length);
    }

    public static double variance(double[] data, int length) {

	if (length <= 1) return 0.0;   // DDG - prevent returning NaN
         int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        double mean = mean(data, size);
        double sum = 0.0;
        double dif = 0.0;
        for (int i=0; i<size ; i++) {
            dif = data[i] - mean;
            sum += dif*dif;
        }
        return sum/(size - 1);
    }

/**
 * Returns the standard deviation which is just the square-root of the variance.
 */
    public static double standardDeviation(double[] data) {
        return Math.sqrt(Stats.variance(data, data.length));
    }
    public static double standardDeviation(double[] data, int length) {
        return Math.sqrt(Stats.variance(data, length));
    }

/**
 * Returns the RMS (Root-Mean-Square) of a data sequence.  That is
 * <tt>Math.sqrt(Sum( data[i]*data[i] ) / data.size())</tt>.  The RMS of data
 * sequence is the square-root of the mean of the squares of the elements in the
 * data sequence.  It is a measure of the average "size" of the elements of a
 * data sequence.
 *
 * @param sumOfSquares <tt>sumOfSquares(data) == Sum( data[i]*data[i] )</tt> of the data sequence.
 * @param size the number of elements in the data sequence.
 */
    public static double rms (double[] data) {
        return rms(data, data.length);
    }

    public static double rms (double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();
        return Math.sqrt(sumOfSquares(data, size)/size);
    }

    public static double rmsError(double[] data) {
        return rmsError(data, data.length);
    }

    public static double rmsError(double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double mean = mean(data, size);
        double errors[] = new double[size];
        for (int i=0; i<size ; i++) {
            errors[i] = data[i] - mean;
        }
        return rms(errors, size);
    }


/**
 * Sum of a data sequence.
 */
    public static double sum (double[] data) {
        return sum(data, data.length);
    }

    public static double sum (double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double sum = 0.0;
        for (int i = 0; i < size; i++) {
            sum += data[i];
        }
        return sum;
    }

/**
* Sum the squares of a data sequence.
*/
    public static double sumOfSquares (double[] data) {
        return sumOfSquares(data, data.length);
    }

    public static double sumOfSquares (double[] data, int length) {
        int size = length;
        if (size < 1 || size > data.length) throw new IllegalArgumentException();

        double sum = 0.0;
        for (int i = size-1; --i >= 0;) {
            sum += data[i]*data[i];
        }
        return sum;
    }

    /*
    public static void main(String args[]) {
        //        double[] data = { 2.4, 3.3, 5.534, 11.4, 234.2, 1.1, 32.12, 321.1, 67.4};
        //double[] data = { 2.04, 1.91, 1.89};
        double[] data = { 1., 2., 3., 4., 5., 6.};

        int k=234;
        System.out.println (" isEven():    "+ k+ " "+Stats.isEven(k));

        k=23;
        System.out.println (" isEven():    "+ k+ " "+Stats.isEven(k));

        System.out.println (" Mean:    "+ Stats.mean(data));
        System.out.println (" Median:  "+ Stats.median(data));
        System.out.println (" Min:     "+ Stats.min(data));
        System.out.println (" Max:     "+ Stats.max(data));
        //        System.out.println (" Mode:    "+ Stats.mode(data));
        System.out.println (" RMS:     "+ Stats.rms(data));
        System.out.println (" RMS err: "+ Stats.rmsError(data));
        System.out.println (" stdDev:  "+ Stats.standardDeviation(data));
        System.out.println (" var:     "+ Stats.variance(data));
        System.out.println (" meanDev: "+ Stats.meanDeviation(data));
   }        // end of main
   */
} // Stats

package org.trinet.util;
public interface CorrTypeIdIF {
  public static final String FINALIZED = "F";
  public static final String ESTIMATED = "C";
  public static final String DEFAULTED = "D";

  public static final String MC = "mc";
  public static final String MD = "md";
  public static final String ME = "me";
  public static final String MH = "mh";
  public static final String ML = "ml";
  public static final String ACC = "acc";
  public static final String VEL = "vel";
}

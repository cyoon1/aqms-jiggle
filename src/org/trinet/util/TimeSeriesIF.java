package org.trinet.util;

import java.util.*;

public interface TimeSeriesIF extends TimeBoundedDataIF {

    public void setSampleRate(double samplesPerSecond);
    public double getSampleRate();
    public void savePacketSpans();
    public TimeSpan[] getPacketSpans();
    public List getPacketSpanList();
    public void setPacketSpans(TimeSpan[] spanArray);
    public int getSegmentCount();
    public List getSegmentList();
    public void setSegmentList(Collection list);
    public void setSegmentList(Collection list, boolean resetPacketSpans);
    public void scanSegListForTimeQuality();
    public void setNoiseLevel(float value);
    public float getNoiseLevel();
    public float scanForNoiseLevel();
    public void setIsFiltered(boolean tf);
    public boolean isFiltered();

}

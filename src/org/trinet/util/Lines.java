package org.trinet.util;
public class Lines {
  public static final String EQUALS_TEXT_LINE =
    "================================================================================";
  public static final String MINUS_TEXT_LINE =
    "--------------------------------------------------------------------------------";
  public static final String PLUS_TEXT_LINE =
    "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
  public static final String STAR_TEXT_LINE =
    "********************************************************************************";
  public static final String DOT_TEXT_LINE =
    "................................................................................";
  public static final String POUND_TEXT_LINE =
    "################################################################################";
  public static final String CARET_TEXT_LINE =
    "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";
  public static final String ANGLE_L_TEXT_LINE =
    "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
  public static final String ANGLE_R_TEXT_LINE =
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
  public static final String ANGLE_LR_TEXT_LINE =
    "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
  public static final String ANGLE_RL_TEXT_LINE =
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
}


package org.trinet.util;

import org.trinet.jasi.Waveform;

public interface WaveformBandpassFilterIF extends WaveformFilterIF, BandpassFilterIF {}

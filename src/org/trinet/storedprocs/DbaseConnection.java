package org.trinet.storedprocs;
/**
 * DbaseConnection.java
 */

//import sqlj.runtime.ref.DefaultContext;
import java.sql.*;
import java.util.*;

import org.trinet.jasi.DataSource;

public class DbaseConnection  {

    /** The database connection */
    static public Connection conn = null;

    private DbaseConnection() { }

    /** If class static connection already exists, returns it, else creates a new static connection.
     * Internal connection if on db server, otherwise use the currently set DataSource description
     * to create new static connection.
     * Prints stack trace and returns null if exception occurred creating connection.
     */
    public static Connection create() {
      try {
        return createWithExceptions();
      }
      catch (Exception ex) {
        return null;
      }
    }

    /** If instance connection already exists, return it, else creates new instance connection.
     * An internal connection if on server, otherwise use the current DataSource description
     * Internal connection if on db server, otherwise use the currently set DataSource description
     * to create new static connection.
     * Prints stack trace and rethrows any exception occurring creating the connection.
     */
    public static Connection createWithExceptions() throws Exception {

      if (conn != null) return conn;  // already a connection

      try {
        if (isInsideDbase()) {
          conn = org.trinet.jasi.DataSource.createInternalDbServerConnection();
          //
          //above comparable to:
          //if (DataSource.isNull()) DataSource.createDefaultDataSource();
          //conn = DriverManager.getConnection("jdbc:default:connection:");
          //DataSource.set(conn);
          //
        } else { // not executing in server
          conn = DataSource.getConnection(); // get prevously created DataSource, throws exception datasource not initialized with valid ds
          if (conn == null || conn.isClosed()) {
              if (DataSource.getSource() != null) conn = DataSource.getNewConnect();
          }
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
        throw ex;
      }

      return conn;
    }

    /** If static connection already exists, return it, else creates new instance connection.
     * An internal connection if on server, otherwise use the current DataSource description
     * to create new connection.
     * Reports, but does not throw exception creating connection.
     */
    public static Connection getConnection() {
      // use default if none defined
      try {
        if (conn == null || conn.isClosed()) {
          conn = null;
          conn = create();
        }
      }
      catch (SQLException ex) {
        System.out.println("DbaseConnection: error checking conn object.");
      }
      return conn;
    }

    /**
     * Make a connection. If we are running in the context of the database server,
     * make the "default" connection to its local environment.
     * Rethrows any exception creating the connection.
     */
    public static Connection getConnectionWithExceptions() throws Exception {
      // use default if none defined
      try {
        if (conn == null || conn.isClosed()) {
          conn = null;
          conn = createWithExceptions();
        }
      }
      catch (Exception ex) {
        System.out.println("DbaseConnection: error checking conn object.");
        throw ex;
      }
      return conn;
    }

    /** Returns 'true' if we are running inside an database*/
    public static boolean isInsideDbase() {
      return (System.getProperty("oracle.server.version") != null) ;
    }
 
} // DbaseConnection



/**
 * PassingScore.java
 *
 *
 * Created: Tue Feb  8 16:03:38 2000
 *
 * @author Doug Given
 * @version
 */

package org.trinet.storedprocs.eventmatch;

import java.sql.*;

//import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.storedprocs.DbaseConnection;

    /**
     * Retreive the value of passingScore from the database. It is kept in a
     * table called: EventMatchPassingScore that has one column called
     * passingScore. <p>
     *
     * From SQL you would access it using: <br>
     * SQL> Select passingScore from EventMatchPassingScore;
     */
public class PassingScore  {


    private PassingScore() {

    }


    /**
     * Get the passing score from the database. It is kept in a table called:
     * EventMatchPassingScore that has one column called passingScore.
     */
public static double getPassingScore() {

      Connection conn = DbaseConnection.getConnection(); // aww 03/08/2005

      String sql = "Select passingScore from EventMatchPassingScore";

      Statement stmt = null;
      ResultSet rs = null;

      double score = 0.0;

      try {

        stmt = conn.createStatement();

        //if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
        rs = stmt.executeQuery(sql);

        if (rs.next()) {
          score = rs.getDouble("passingScore");
        }

      } catch (SQLException ex) {
              ex.printStackTrace(System.out);
      }
      finally {
        try {
            //close statement & resultset
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
        }
        catch(SQLException ex) { }
      }

      return score;
}

/*
  public static void main (String args[]) {
      System.out.println ("The passing score is "+ PassingScore.getPassingScore());
  }
*/
} // PassingScore

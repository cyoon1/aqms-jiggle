package org.trinet.storedprocs.formats;

import java.sql.*;

import org.trinet.formats.CubeFormat;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.Solution;
import org.trinet.jasi.TestDataSource;
import org.trinet.storedprocs.DbaseConnection;

/** Make various formatters available to the dbase via stored procedures.
 * This wrapper class is needed where you must create a (internal) dbase connection
 * to look something up -- like solution info. In other cases you can access
 * org.trinet.formats.CubeFormat directly from sql. */

// DDG 4/4/03

public class Formats {
  static Connection conn = null;

  public Formats () {};

  /** Return the Cube summary string for the event with this ID.
   * The string has the Menlo Park checksum char at the end.<p>
  <pre>
  Examples of CUBE event messages(char appears as last character in string):
  E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002hP
  E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   v
  </pre>
  *
  * @see CubeFormat */
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public static String getCubeEstring(long evid, String version) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return CubeFormat.toEventString(sol, version);
  }
  /** STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
   * Blank Version number
   * */
  public static String getCubeEstring(long evid) {
    //return getCubeEstring(evid, " "); // default was space, now default to db version -aww 2010/06/07
    return getCubeEstring(evid, null); // use db version -aww 2010/06/07
  }
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public static String getCubeEmailString(long evid, String version) {
      return getCubeEmailString(evid, version, 0);
  }
  public static String getCubeEmailString(long evid, String version, int flag) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return CubeFormat.toEmailString(sol, version, conn, flag);
  }
  
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  /**
   * Default to event database version 
  */
  public static String getCubeEmailString(long evid) {
      // added code below to get event version
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      //return getCubeEmailString(evid, " "); // default was space, now default to db version -aww 2010/06/07
      return CubeFormat.toEmailString(sol, conn); // use db version -aww 2010/06/07
  }
 
  public static String getCubeEmailString(long evid, int flag) {
      // added code below to get event version
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      //return getCubeEmailString(evid, " "); // default was space, now default to db version -aww 2010/06/07
      return CubeFormat.toEmailString(sol, conn, flag); // use db version -aww 2010/06/07
  }

  public static String getCubeCancelEmailString(long evid, String version, int flag) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return CubeFormat.toCancelEmailString(sol, version, conn, flag);
  }
  public static String getCubeCancelEmailString(long evid, String version) {
      return getCubeCancelEmailString(evid, version, 0);
  }
  public static String getCubeCancelEmailString(long evid) {
      return getCubeCancelEmailString(evid, null);
  }

  /**
   * Return the aftershock probability report.
   * @param evid
   * @param version
   * @return
   */
    public static String getProbabilityReport(long evid) {
        return Formats.getProbabilityReport(evid, (String) null);
    }

    public static String getProbabilityReport(long evid, String version) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn, false);
   }

  /**
   * Return the aftershock probability report with HTML tags.
   * @param evid
   * @param version
   * @return
   */
    public static String getProbabilityReport(long evid, String version, double mHigh, double mLow, double duration) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn, mHigh, mLow, duration, false);
    }

    public static String getProbabilityReport(long evid, String version,
            double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays,
            double mHigh, double mLow, double duration) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn,
                                                    logIntensity, timeDecayExponent, bValue, timeOffsetDays,
                                                    mHigh, mLow, duration, false);
 }
  /**
   * Return the aftershock probability report with HTML tags.
   * @param evid
   * @param version
   * @return
   */
  public static String getProbabilityReportHTML(long evid, String version) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn, true);
  } 
  /**
   * Return the aftershock probability report with HTML tags.
   * @param evid
   * @param version
   * @return
   */
    public static String getProbabilityReportHTML(long evid, String version, double mHigh, double mLow, double duration) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn, mHigh, mLow, duration, true);
    }

    public static String getProbabilityReportHTML(long evid, String version,
            double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays,
            double mHigh, double mLow, double dur) {
      conn = getConnection();
      Solution sol = Solution.create().getById(conn, evid);
      if (sol == null) return "";
      return AftershockProbabilityReport.getMessage(sol, version, conn,
                                                    logIntensity, timeDecayExponent, bValue, timeOffsetDays,
                                                    mHigh, mLow, dur, true);
    }

    public static void setProbabilityModel(double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays) {
      AftershockProbabilityReport.setModel(logIntensity, timeDecayExponent, bValue, timeOffsetDays);
    }
  /** Return an event delete message string for this event id.
   * Netcode is the 2-char FDSN network code.
   * The comment is optional and may be null.
   * If too long, comment will be truncated to 65 characters.<p>
   * Example: "DE 9884157CI  Deleted by operator"
   * */
    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static String getDeleteString(long evid, String netcode, String comment) {
        conn = getConnection();
        Solution sol = Solution.create().getById(conn, evid);
        if (sol == null) return "";
        return CubeFormat.toDeleteString(sol, netcode, comment);
    }
    /**
     * Figure out the netcode for the caller using Origin.Auth
     * @param evid
     * @param netcode
     * @param comment
     * @return
     */
    public static String getDeleteString(long evid, String comment) {
        conn = getConnection();
        Solution sol = Solution.create().getById(conn, evid);
        if (sol == null) return "";    
        return CubeFormat.toDeleteString(sol, comment);
    }    
    
    /**
     * Figure out the netcode for the caller using Origin.Auth
     * @param evid
     * @param netcode
     * @param comment
     * @return
     */
    public static String getDeleteString(long evid) {
        conn = getConnection();
        Solution sol = Solution.create().getById(conn, evid);
        if (sol == null) return ""; 
        return CubeFormat.toDeleteString(sol);
    }    
        
  // STATIC METHOD WAS CALLED BY ORACLE STORED PROCEDURE ! -REMOVED FROM FORMATS PKG -aww
//  public static String getCubeDstring(long evid, String netCode) {
//    return Formats.getCubeDstring(evid, netCode, null);
//  }
  // STATIC METHOD WAS CALLED BY ORACLE STORED PROCEDURE ! -REMOVED FROM FORMATS PKG -aww
  /** Return the CUBIC/QDDS delete message. 'netCode' is the FDSN network operator code.
   * Long comments will be truncated to 65 characters. */
//  public static String getCubeDstring(long evid, String netCode, String comment) {
//    conn = getConnection();
//    return CubeFormat.toDeleteString(evid, netCode, comment);
//  }
  /** Get an internal default connection if running in the dbase, a working TestDataSource
   * if running outside.
   */
  private static Connection getConnection() {
    // use default if none defined
//    if (conn == null) conn = DbaseConnection.create();
    if (conn == null) conn = DbaseConnection.getConnection();
    return conn;
  }

/** Really belongs elsewhere but put here during troubleshooting...
 * Has dummy arg because Oracle stored proc wouldn't work otherwise. */
/*  public static long getLatestEvent(long dummy) {

    conn = getConnection();

    long val = 0;
    try {
      Statement sm = conn.createStatement();

      String sql = "select evid from event where lddate = (select max(lddate) from event)";
      Statement stmt = conn.createStatement();

      ResultSet rs = stmt.executeQuery(sql);

      if (rs == null) return 0; // nothing found

      rs.next();
      val = rs.getLong("evid");

    }
    catch (SQLException ex) {
     System.err.println( ex.toString() );
    }
    return val;
  }
*/
// Done for troubleshooting of internal dbase connection
//Dbase: Oracle driver: Oracle JDBC driver URL: jdbc:oracle:kprb: username: TRINETDB
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public static String getConnectionInfo(long dummy) {
// SQL call spec seems to require an arg -- so I gave this a dummy one
    conn = getConnection();

    StringBuffer sb = new StringBuffer();

    try {
      sb.append("Dbase: "+conn.getMetaData().getDatabaseProductName());
      sb.append(" driver: "+conn.getMetaData().getDriverName());
      sb.append(" URL: "+conn.getMetaData().getURL());
      sb.append(" username: "+conn.getMetaData().getUserName());
    }
    catch (SQLException ex) {
      return ex.toString();
    }

    return sb.toString();

  }
/** Create an Earthworm StrongmotionII message for the amps from this event.
 * 'netcodes' is a list of Network ids to use like "CI AZ WR" */
//
// CRAP! The string returned can easily exceed the 4kb limit of an SQL varchar2
// CLOB's look complex. What to do? Perhaps, just not a candidate for a
// stored proc.
//
/*
  public static String getEW2message(long evid, String netcodes) {
    conn = getConnection();

    Db2Gmp db2gmp = new Db2Gmp(GroMoPacket.TYPE_EW2);

    // convert space delimited string to string array
    StringList list = new StringList(netcodes);
    db2gmp.setNetcodeList(list.toArray());

    return GroMoPacket.format(evid, db2gmp.getAmps(evid));

  }
  */

  /** Create a CISN Ground Motion Packet message for the amps from this event.
   * 'netcodes' is a list of Network ids to use like "CI AZ WR" */
//
// CRAP! The string returned can easily exceed the 4kb limit of an SQL varchar2
// CLOB's look complex. What to do? Perhaps, just not a candidate for a
// stored proc.
//
/*
    public static String getGMP(long evid, String netcodes) {
    conn = getConnection();
      Db2Gmp db2gmp = new Db2Gmp(GroMoPacket.TYPE_GMP);

      // convert space delimited string to string array
      StringList list = new StringList(netcodes);
      db2gmp.setNetcodeList(list.toArray());

      return GroMoPacket.format(evid, db2gmp.getAmps(evid));

  }
*/

/*
    public static void main(String[] args) {
      String host = "serverhu";
      //long evid = 12335691;
      long evid = 10671989;
    
      System.out.println("Making connection to "+host);
      DataSource ds = TestDataSource.create(host, "databasearw", "---", "---");
      Solution sol = Solution.create("TN").getById(evid);
      if (sol == null) {
        System.out.println("No event found with evid = "+evid);
      } else {
           System.out.println(getCubeEstring(evid));
           System.out.println(getCubeEstring(evid, "2"));
           System.out.println(getCubeEstring(evid, "Y"));
           System.out.println(getCubeEstring(evid, "a"));
           System.out.println(getCubeEstring(evid, "59"));
           System.out.println(getCubeEstring(evid, "??"));
           //System.out.println(getCubeEstring(evid, "YZ"));
           //System.out.println(getCubeEstring(evid, "aa"));
           System.out.println(getCubeEmailString(evid));
           System.out.println(getCubeEmailString(evid, "6"));
           System.out.println(getDeleteString(evid));
           System.out.println(getDeleteString(evid, "FA", "Dude!"));
           //System.out.println(getProbabilityReport(evid));
           System.out.println(getProbabilityReport(evid, "25"));
      }
      ds.close();
    }
*/

}

package org.trinet.storedprocs.formats;

import org.trinet.jasi.*;
import org.trinet.util.*; 
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.formats.*;

import java.sql.*;  // Connection class
/**
 * Java port of AftershocMsg.C originally written by Patrick Small. 
 * I think that was a port of Andy Michel's code.
 * @author Doug Given
 *
 * Example:
 * <code>
AFTERSHOCK PROBABILITY REPORT
Published on Mon Feb 11 10:38:35 2008 PDT

Southern California Seismic Network: a cooperative project of
U.S. Geological Survey, Pasadena, California
Caltech Seismological Laboratory, Pasadena, California

Version 1: This report supersedes any earlier probability reports about this event.

MAINSHOCK
 Magnitude   :   5.1  Ml
 Time        :   11 Feb 2008   10:29:32 AM PST
             :   11 Feb 2008   18:29:32 UTC
 Coordinates :   32 deg. 23.28 min. N,  115 deg. 16.38 min. W
                  23 mi. ( 37 km) SSE of Calexico, CA                                      
                  32 mi. ( 52 km) SSE of El Centro, CA                                     
 Event ID    :   14348196

STRONG AFTERSHOCKS (Magnitude 5 and larger) -

At this time (immediately after the mainshock) the probability of a strong and possibly damaging aftershock IN THE NEXT 7 DAYS is less than 10 PERCENT.
EARTHQUAKES LARGER THAN THE MAINSHOCK -

Most likely, the recent mainshock will be the largest in the sequence. However, there is a small chance (APPROXIMATELY 5 TO 10 PERCENT) of an earthquake equal to or larger than this mainshock in the next 7 days.
WEAK AFTERSHOCKS (Magnitude 3 to 5) -

In addition, approximately 1 to 12 SMALL AFTERSHOCKS are expected in the same 7-DAY PERIOD and may be felt locally.

This probability report is based on the statistics of aftershocks typical for California. This is not an exact prediction, but only a rough guide to expected aftershock activity. This probability report may be revised as more information becomes available.
Background Information About Aftershocks

Like most earthquakes, the recent earthquake is expected to be followed by numerous aftershocks. Aftershocks are additional earthquakes that occur after the mainshock and in the same geographic area. Usually, aftershocks are smaller than the mainshock, but occasionally an aftershock may be strong enough to be felt widely throughout the area and may cause additional damage, particularly to structures already weakened in the mainshock. As a rule of thumb, aftershocks of magnitude 5 and larger are considered potentially damaging.

Aftershocks are most common immediately after the mainshock; their average number per day decreases rapidly as time passes. Aftershocks are most likely to be felt in the first few days after the mainshock, but may be felt weeks, months, or even years afterwards. In general, the larger the mainshock, the longer its aftershocks will be felt.

Aftershocks tend to occur near the mainshock, but the exact geographic pattern of the aftershocks varies from earthquake to earthquake and is not predictable. The larger the mainshock, the larger the area of aftershocks. While there is no "hard" cutoff distance beyond which an earthquake is totally incapable of triggering an aftershock, the vast majority of aftershocks are located close to the mainshock. As a rule of thumb, a magnitude 6 mainshock may have aftershocks up to 10 to 20 miles away, while a magnitude 7 mainshock may have aftershocks as far as 30 to 50 miles away. 
 * </code>
 */

/*
 * The original .cfg file contained...
 * 
ScriptName                      General
TownFile                        ../cusp_towns.dat
FaultFile                       ../cusp_faults.dat
DBService                       databaseat
DBUser                          <db-user-account-name>
DBPasswd                        <user-account-passwd>
DBConnectionRetryInterval       5
DBMaxConnectionRetries          5
#
# Script-specific parameters
#
Mag1             5.0
Mag2             3.0
Duration         7.0
 */

public class AftershockProbabilityReport extends CubeFormat {

    // Aftershock model for California
    static A_MDL afterModel = new A_MDL(-1.85, 1.08, 0.91, 0.05); // general Calif. Model

    // Psychologically pleasing (?) round numbers for reporting percent probabilities
    static double percarray[] = {0.0,  1.0,  2.0,  3.0,  5.0, 10.0, 12.0, 15.0, 20.0, 25.0,
                                 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0};

    // default config values
    // Todo: make these runtime configurable?
    // Or set maq = eventMag - 1?
    static double magHigh  = 5.0;
    static double magLow   = 3.0;
    static double duration = 7.0;  // days

    static String dateFmt = "MMMM dd, yyyy @ HH:mm:ss z";
          
    // Dummy constructor
    AftershockProbabilityReport() { }

    /**
    * Return a big string containing the whole message. There will be no 
    * html formatting.
    * @param sol - the event for which the message is being made
    * @param version - string describing the event version # 
    * @param conn - the data source connection
    * @return
    */
    public static String getMessage(Solution sol, String version, Connection conn) {
        return getMessage(sol, version, conn, false);
    }

    /**
     * 
     * @param sol - the event for which the message is being made
     * @param version - string describing the event version # 
     * @param conn - the data source connection
     * @param mHigh - high mag in range to consider (default = 5.0)
     * @param mLow  - low mag in range to consider (default = 3.0)
     * @param dur - duration to consider (default = 7 days)
     * @param html - set true to include html formatting tags in the message
     * @return
     */        
    public static String getMessage(Solution sol, String version, Connection conn,
                        double mHigh, double mLow, double dur, boolean html) {
        magHigh = mHigh;
        magLow = mLow;
        duration = dur;
        return getMessage(sol, version, conn, html);
    }

    /**
     * Return a big string containing the whole message.
     * @param sol - the event for which the message is being made
     * @param version - string describing the event version # 
     * @param conn - the data source connection
     * @param html - set true to include html formatting tags in the message
     * @return
     */
    public static String getMessage(Solution sol, String version, 
                                                Connection conn, boolean html) {

        long   k1, k2;        /* Range of no. of aftershocks expected        */
        double nAfter;        /* Expected number of aftershocks        */
        double pz;                /* Probability of no aftershocks        */
        double t_secs;        /* Time after main shock (secs)                */
        double t_days;        /* Time after main shock (days)                */

        String numafterstr;
        String bigchancestr;
        String timeafterstr;
          
        double conf = 0.95;                /* Confidence level for number range        */

        // Get the origin time
        double ot = sol.getTime(); // UTC seconds time -aww 2008/02/22

        // Get the magnitude
        double mag = sol.getPreferredMagnitude().value.doubleValue();

        double now = new DateTime().getTrueSeconds(); // UTC seconds time -aww 2008/02/22
        // test //////////////////////////////////////////////////////////////
        //          mag = 5.4;
        //          ot = now-1200;
          
        t_secs = now - ot;
        t_days = t_secs / 86400.0;  // time since mainshock in days
  
        // Compute the probability of getting a large aftershock
        // 5.0 or larger in 7 days
        nAfter = N_expect(afterModel, magHigh - mag, 1000.0 - mag, t_days, 
                          t_days + duration);
        pz = 1.0 - Poisson(nAfter, 0);
          
        // Construct the text string for the large aftershock chance
        if (pz < 0.1) {
            bigchancestr = "less than 10 PERCENT";
        } else if (pz > 0.9) {
            bigchancestr = "greater than 90 PERCENT";
        } else {
            //String pcnt = ""+Math.round(10.0 * ((10.0 * pz) + 0.5));
            String pcnt = ""+Math.round(100.0 * pz);
            bigchancestr = "approximately "+pcnt+" PERCENT";
        }

        // Compute the expected number of small aftershocks
        nAfter = N_expect(afterModel, magLow - mag, magHigh - mag, t_days, 
                          t_days + duration);
        pz = Poisson(nAfter, 0);
        LongRange confLevel = Krange(nAfter, conf);
        k1 = confLevel.getMinValue();
        k2 = confLevel.getMaxValue();

        // change to "psychologically pleasing values?
        k1 = (long) Rounddown(k1);
        k2 = (long) Roundup(k2);
        // Construct the text string for the number of aftershocks
        if (k1 == 0) {
            if (k2 == 1) {
                  numafterstr = "approximately "+ k2+" SMALL AFTERSHOCK is";
            } else {
                  numafterstr = "up to approximately "+ k2+" SMALL AFTERSHOCKS are";
            }
        } else {
            numafterstr = "approximately "+k1+" to "+ k2+" SMALL AFTERSHOCKS are";
        }

        // Calculate time difference
        if (t_secs < 3600) {
            timeafterstr = "immediately";
        } else if (t_secs < 1.5 * 3600) {
            timeafterstr = "one hour";
        } else if (t_secs < 24 * 3600) {
            timeafterstr = Math.round(t_secs/3600) +" hours";
        } else if (t_secs < 1.5 * 86400) {
            timeafterstr = "one day";
        } else {
            timeafterstr = Math.round(t_secs/86400) + " days";
        }

        // Output formatted email
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          
        StringBuffer sb = new StringBuffer();
        String cr = "\n";

        String durStr  = ""+Math.round(duration);
        String magHighStr = ""+Math.round(magHigh);
        String magLowStr = ""+Math.round(magLow);
          
        // html formating is not fully worked out yet
        String h1 = "", h1x="";
        String h2 = "", h2x="";
        String h3 = "", h3x="";
        String cnt="", cntx="";  
        String code="", codex="";
        String it="", itx="";
        String hr="";
        if (html) {
                   h1 = "<h1>"; h1x="</h1>\n";
                   h2 = "<h2>"; h2x="</h2>\n";
                   h3 = "<h3>"; h3x="</h3>\n";
                   cnt="<center>"; cntx="</center>\n";
                   code="<pre>"; codex="</pre>";
                   it="<i>"; itx="</i>";
                   hr="<hr>\n";
                   cr = "<p>\n";   // is this right?
        }
          
        // Common Header
        sb.append(cnt);
        sb.append(h1+"PROBABILITY REPORT"+h1x+cr);
        String nowStr = LeapSeconds.trueToString(now, dateFmt); // UTC time -aww 2008/02/22
        sb.append(h3+"Published on "+nowStr+h3x+cr);
          
        // Site-specific header. "??" means get source net identity from dbase.
        HeaderStrings hs = new HeaderStrings("??", conn);
      
        // ORIGINATING NETWORK
        sb.append(hs.getString("SrcHeader").toString("\n")+cr+cr);          

        sb.append("Version "+ toVersionChar(sol, version) +
                      ": This report supersedes any earlier probability reports about this event."+cr);
        sb.append(cr);
      
        sb.append(cntx);
        // Mainshock info block
        sb.append(hr);
        sb.append(h3+"MAINSHOCK"+h3x+cr);
        // EVENT INFO
        sb.append(code);
        sb.append(getEventSummaryBlock(sol));
        sb.append(codex+cr);
      
        //  STRONG AFTERSHOCKS (Magnitude 5 and larger) -
        sb.append(h3+" STRONG AFTERSHOCKS (Magnitude "+magHighStr+" and larger) -"+h3x+cr);
        sb.append(cr);
      
        sb.append(" At this time ("+timeafterstr+" after the mainshock) the probability of a");
        sb.append(" strong and possibly damaging aftershock IN THE NEXT "+
                            durStr+" DAYS is "+bigchancestr+cr);
        sb.append(cr);
      
        sb.append(h3+" EARTHQUAKES LARGER THAN THE MAINSHOCK -"+h3x+cr);
        sb.append(cr);
        sb.append(" Most likely, the recent mainshock will be the largest in the sequence.");
        sb.append(" However, there is a small chance (APPROXIMATELY 5 TO 10 PERCENT) of an ");
        sb.append(" earthquake equal to or larger than this mainshock in the next "+durStr+" days."+cr);
        sb.append(cr);
      
        sb.append(h3+" WEAK AFTERSHOCKS (Magnitude "+magLowStr+" to "+magHighStr+") -"+h3x+cr);
        sb.append(cr);
        sb.append(" In addition, "+numafterstr+" expected in the same "+durStr+
                          "-DAY PERIOD and may be felt locally."+cr); 
        sb.append(cr);
      
        sb.append(it);
        sb.append(" This probability report is based on the statistics of aftershocks typical for"+
                          " California.  This is not an exact prediction, but only a rough guide to"+
                          " expected aftershock activity. This probability report may be revised as more"+
                          " information becomes available."+cr);
        sb.append(cr);
        sb.append(itx);
        sb.append(hr);
      
        // Three paragraphs of 'splainin'
        sb.append(cnt+h2+"Background Information About Aftershocks"+h2x+cntx+cr);
        sb.append(cr);
        sb.append(
            " Like most earthquakes, the recent earthquake is expected to be followed"+
            " by numerous aftershocks.  Aftershocks are additional earthquakes that" +
            " occur after the mainshock and in the same geographic area.  Usually," +
            " aftershocks are smaller than the mainshock, but occasionally an"+
            " aftershock may be strong enough to be felt widely throughout the area"+ 
            " and may cause additional damage, particularly to structures already"+ 
            " weakened in the mainshock.  As a rule of thumb, aftershocks of"+
            " magnitude 5 and larger are considered potentially damaging."+cr);
        sb.append(cr);
      
        sb.append(
            " Aftershocks are most common immediately after the mainshock; their" +
            " average number per day decreases rapidly as time passes.  Aftershocks" +
            " are most likely to be felt in the first few days after the mainshock," +
            " but may be felt weeks, months, or even years afterwards.  In general," +
          " the larger the mainshock, the longer its aftershocks will be felt."+cr);
        sb.append(cr);
      
        sb.append(
            " Aftershocks tend to occur near the mainshock, but the exact geographic"+ 
            " pattern of the aftershocks varies from earthquake to earthquake and is"+
            " not predictable.  The larger the mainshock, the larger the area of"+
            " aftershocks.  While there is no \"hard\" cutoff distance beyond which an"+
            " earthquake is totally incapable of triggering an aftershock, the vast" +
            " majority of aftershocks are located close to the mainshock.  As a rule"+
            " of thumb, a magnitude 6 mainshock may have aftershocks up to 10 to 20"+
            " miles away, while a magnitude 7 mainshock may have aftershocks as far"+
            " as 30 to 50 miles away."+cr);
          
        return sb.toString();
    }
        
    /**
     * Override the CubeFormat method to clip off quarry and fault location lines.
     * @param sol
     */
    public static String getEventSummaryBlock(Solution sol) {
          
        StringBuffer sb = new StringBuffer();
        double ot = sol.getTime(); // UTC seconds time -aww 2008/02/22
        LatLonZ llz = sol.getLatLonZ();
      
        // OPTIONAL QUARRY NOTICE
        String typedescr = "quake";
        //if (sol.isEventType(EventTypeMap.QUARRY)) {
        if (sol.isEventDbType(EventTypeMap3.QUARRY)) {
            typedescr = "blast";
        }      
          // EVENT INFO
        double mag = sol.magnitude.getMagValue();
        sb.append(" Magnitude   :   "+mag+ " " + sol.magnitude.getTypeString() +
                "  (A "+SeisFormat.magClassString(mag)+" "+typedescr+")"+cr);
        sb.append(" Time        :   "+ LeapSeconds.trueToPST(ot, pstPattern) +cr); // UTC seconds time -aww 2008/02/22
        sb.append("             :   "+ LeapSeconds.trueToString(ot, gmtPattern)+cr); // UTC seconds time -aww 2008/02/22
        sb.append(" Coordinates :   "+ LLZstringDM(llz) +cr);
        sb.append("             :   "+ LLZstringDec(llz) +cr);
        sb.append(" Depth       :   "+ depthString(sol.getDepth()) +cr);
        sb.append(" Quality     :   "+ SeisFormat.wordQuality(sol) +cr);
        sb.append(whereBlockShort(llz));         // <-- the difference
        sb.append(" Event ID    :   "+ sol.getAuthority()+" "+sol.getId()+cr);
      
        return sb.toString();
    }

    /**
     * Poisson distribution (either homogeneous or non-homogeneous)
     * Probability of k events when a are expected
     *         a = Expected number of events        
     *  k = Number of events observed        
     */

    static double Poisson(double a, long k) {
        double x = 1.0;
        for (int i=1; i<=k; i++) x *= a/(double)i;
        return x*Math.exp(-a);
    }


    /*
     * Confidence interval for the number events in a Poisson process
     * (either homogeneous or non-homogeneous)
     * This will yield the min & max number of aftershocks expected
     * within the time period. 
     */
    static LongRange Krange(double a, double conf)
      /* Expected number of events                */
      /* Confidence level desired                */
      /* Expected range                        */
    {
      double p, q;                /* poisson(m), poisson(n)                */
      double sum;                /* Total probability                        */
      long m,n; 
      
      if (a > 100.0) {
        /* Use asymptotic approximation (Normal distribution) */
        double        f = 2.0;        /* STUB (~95% confidence) */
        double        sigma = Math.sqrt(a);
        m = (long) Math.ceil(a - f*sigma);
        n = (long) Math.floor(a + f*sigma);
      } else {
        /* Accumulate probabilities until sum reaches conf */
        sum = 0.0;
        m = Math.round(a);
        p = Poisson(a, m);
        
        n = m + 1; 
        q = Poisson(a, n);
        
        while (sum < conf) {
          if (m >= 0 && p > q) {
                    sum += p;
                    p = Poisson(a, --(m));
          } else {
                    sum += q;
                    q = Poisson(a, ++(n));
          }
        }
        (m)++; (n)--;
      }
      return (new LongRange (m,n));
    }

    /**
     * Expected number of aftershocks in time interval [days] (t1, t2) and 
     * magnitude range (M1, M2),
     *
     *      t2
     *   INTEGRAL  [lambda(t, M2) - lambda(t, M1)] dt  ,
     *      t1
     *
     * where lambda(t, M) = 10^(a - b*(M-M_main)) / (t+c)^p  .
     *
     * Eqns. (3), (4) of Reasenberg and Jones, Science, v. 243, pp. 1173-1176 (1989)
     * as corrected by Reasenberg and Jones, Science, v. 265, pp. 1251-1252 (1994).
     */

    static double N_expect(A_MDL mdl, double dm1, double dm2, double t1, double t2)
      /* M1-M_main, M2-M_main                        */
      /* Time interval (after main shock)        */
    {
      double part1, part2;
      double q = 1.0 - mdl.p;
      
      part1 = Math.pow(10.0, mdl.a - mdl.b*dm1) - Math.pow(10.0, mdl.a - mdl.b*dm2);
      part2 = (q == 0.0 ? Math.log(t2+mdl.c) - Math.log(t1+mdl.c) :
               (Math.pow(t2+mdl.c, q)-Math.pow(t1+mdl.c, q))/q);
      return part1*part2;
    }


/**
 *   Scale large (>100) number into range [10, 100] 
 *   Returns the number and a factor (x10)
 */
    static Pair Scale(double val) {
        double l = 0.;
        double factor = 0.;
      
        if (val <= 100.0) {
            factor = 1.0;
            return new Pair (val, factor);
        }
        l = (double)Math.round(Math.log(val)/Math.log(10.0)) - 1.0;  
        factor = Math.exp(l * Math.log(10.0));

        return new Pair(l, factor);
    }


/**
 *  Round the 'val' up to the nearest value in the percarray[]
 * @param val
 */
    static double Roundup(double val) {
        double x = 0.;
        double f = 0.;
      
        Pair range = Scale(val);
     
        x = range.v1;
        f = range.v2;
      
        // Find next bigger value in table
        for (int i = 1; i < 18; i++) {
            if (x < percarray[i]) {
              // Scale back up
              return percarray[i]*f;
            }
        }
        return -1;   // failure
    }

/**
 * Round the 'val' down to the nearest value in the percarray[]
 * @param val
 */
    static double Rounddown(long val) {
        double x = 0.;
        double f = 0.;
        Pair range = Scale(val);
             
        x = range.v1;
        f = range.v2;
      
        // Find next smaller value in table
        for (int i = 1; i < 18; i++) {
            if (x < percarray[i]) {
                // Scale back up
                return percarray[i-1]*f;
            }
        }
      
        // Scale back up
        return percarray[17]*f;
    }
    
    public static String getMessage(Solution sol, String version, Connection conn,
            double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays,
            double mHigh, double mLow, double dur, boolean html) {
        setModel(logIntensity, timeDecayExponent, bValue, timeOffsetDays);
        return getMessage(sol, version, conn, mHigh, mLow, dur, html);
    }

    public static void setModel(double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays) {
        if (afterModel == null) afterModel = new A_MDL(logIntensity, timeDecayExponent, bValue, timeOffsetDays);
        else afterModel.setModel(logIntensity, timeDecayExponent, bValue, timeOffsetDays);
    }

    // was a typedef in the orgininal C++
    static class A_MDL {
    /* Parameters describing aftershock behavior */
        double a;                /* log10 of overall intensity level   */
        double p;                /* Time-decay exponent in Omori's Law */
        double b;                /* Slope of frequency-magnitude curve */
        double c;                /* Time offset (days)                 */
    
        A_MDL(double a, double p, double b, double c) {
            this.a = a;
            this.p = p;
            this.b = b;
            this.c = c;
        }

        public void setModel(double logIntensity, double timeDecayExponent, double bValue, double timeOffsetDays) {
            a = logIntensity;       /* log10 of overall intensity level   */
            p = timeDecayExponent;  /* Time-decay exponent in Omori's Law */
            b = bValue;             /* Slope of frequency-magnitude curve */
            c = timeOffsetDays;     /* Time offset (days)                 */
        }
    }

    static class Pair {
        /* Parameters describing aftershock behavior */
        double v1 = 0.;                
        double v2 = 0.;        
            
        Pair(double value1, double value2) {
            v1 = value1;
            v2 = value2;
        }
    }

    /*
    // Note when testing formats package in SQLPLUS BIND variable maximum string is varchar2(4000) !!!
    public static void main(String[] args) {
      String host = "serverhu";
      //long evid = 12335691;
      long evid = 10671989;
    
      System.out.println("Making connection to "+host);
      DataSource ds = TestDataSource.create(host, "databasearw", "---", "---");
      Solution sol = Solution.create("TN").getById(evid);
      if (sol == null) {
        System.out.println("No event found with evid = "+evid);
      } else {
         System.out.println("Default to source version:\n" + AftershockProbabilityReport.getMessage(sol, null, DataSource.getConnection(), false));
         System.out.println("Set version to 25:\n" + AftershockProbabilityReport.getMessage(sol, "25", DataSource.getConnection(), false));
      }
      ds.close();
    }
    */
}
    

package org.trinet.storedprocs.association;

import java.sql.*;
import java.util.ArrayList;

import org.trinet.jasi.*;
import org.trinet.formats.*;
import org.trinet.util.DateTime;
import org.trinet.util.EnergyWindow;
import org.trinet.jasi.TN.*;
import org.trinet.storedprocs.DbaseConnection;
import org.trinet.jdbc.datatypes.DataLong;

/**
 * <p> Does the job of associating unassociated Amps.
 * Intended to run as a stored procedure in the dbase but may be run outside. </p>
 */

/*
DDG Jan 2004   Additions & Changes

  Changed windowing from 2x S-P to EnergyWindow based on coda.
  Changed select to exclude channels w/o Channel_Data entries.
  Fixed bug in select that returned amps in old->new order.
  Changed logic so that if more than one event is a possible fit the one with the
  biggest mag is select. Before it had been the one w/ the closest S-arrival.
  Changed CleanUp stored proc to delete amps w/o Channel_Data entries.
  Created 'MoveAndAssoc2' procedure in dbase.
  Call it in 'batch' mode.

/*
Amplitude Association Issues

1) There is at several instances of an amp (PGD) that is earlier then the P-wave
arrival time at a station! These do not associate.
Example: The P-wave arrival time for event 14002288 at station BK.MHC is 22:04:16
(both observed and calculated). The station is only 15.4 km from the epicenter.
These appear to be random spikes on dead channels with only a few counts of amplitude.
Its still a mystery why the PGD is so early and not later where the (crossfeeding?)
amp is obviously larger on the timeseries at about 22:04:19.

2) Amps appear in the table twice with LDDATEs 5 sec apart! It only happens for BK and not CE amps!

They are sending everything twice? (no dup files)
Two instances of import running?

*/
public class AmpAssocEngine {

  static Connection conn = null;

  static PreparedStatement addStmt = null;
  static PreparedStatement delStmt = null;


  static String ampSetType = null;

  // OPTIONS
  /** If set 'true' reject an association if there are dups in dbase. */
  static boolean rejectDups = true;

  /** If true dump more debug output. */
  static boolean debug = false;

  /** If true dump more verbose output. */
  static boolean verbose = false;

  /** Report missing LatLonZ */
  static boolean logMissingLatLonZ = false;

  /** This size gap (secs) in the amp list is used as the delimiter
   * to dice list into logical chunks of amps */
  static double gapDelimiterSecs = 300.0;

  /** Maximun number of amps to allow in a group, default to 5000. <p>
   * Why this is here: theoretically the scheme of defining
   * amp groups with time gaps could be thwarted if something is producing amps at
   * a regular interval less than 'gapDelimiterSecs'. In this case a delimiting gap would never
   * exist and all amps would form a single group. This could cause out of memory errors
   * or make things really slow.
   */
  static int maxInGroup = 5000;

  /** Max number of amps to batch between commits, default 5000. */
  static int maxCommitSize = 5000;

  /** An optional alternate list of channels (probably from reading Ad Hoc list) */
  static ChannelList alternateList = null; // changed from ChannelableList 2010/01/18 -aww
  static ChannelList masterList = null; // added 2010/01/18 -aww

  static UnassocAmplitudeTNGetter ampGetter = null;

  static double daysBack = 1.0;

  static String ampSubsource = null;
//
  public AmpAssocEngine() { }

  public static void setAmpSetType(String type) {
      ampSetType = (type == null || type.trim().length() == 0) ? null : type;
  }

  public static void setConnection(Connection connection) {
    conn = connection;
  }

  public static Connection getConnection() {
    return conn;
  }

  public static void setDebug(boolean tf) {
      debug = tf;
      PointAssociator.debug = debug;
      //AmpAssoc.debug = debug; // inherited static from PointAssociator
      UnassocAmplitudeTNGetter.debug=debug;
  }

  /** Set verbose output flag. */
  public static void setLogMissingLatLonZ(boolean tf) {
    logMissingLatLonZ = tf;
  }

  /** Set verbose output flag. */
  public static void setVerbose(boolean tf) {
    verbose = tf;
  }

  /** Get verbose output flag. */
  public static boolean getVerbose() {
    return verbose;
  }

  /** Set the max window seconds preceeding the earliest amp time in group to query for for possible associated events. */
  public static void setEventCandidateWindowSize(double secs) {
      AmpAssoc.setCandidateTimeWindowSize(secs);
  }

  /** set event selection properties for candidate selection. */
  public static void setEventSelectionProperties(EventSelectionProperties props) {
      AmpAssoc.setEventSelectionProperties(props);
  }
  /** Get event selection properties for candidate selection. */
  public static EventSelectionProperties getEventSelectionProperties() {
      return AmpAssoc.getEventSelectionProperties();
  }

  /** If returns 'true' class will reject an association if there are dups in dbase. */
  public static boolean getRejectDups() {
    return rejectDups;
  }
  /** If set 'true' class will reject an association if there are dups in dbase. */
  public static void setRejectDups(boolean tf) {
    rejectDups = tf;
  }
  /** Set the group gap in seconds. */
  public static void setGroupGap(double secs) {
    gapDelimiterSecs = secs;
  }
  /** Get the group gap. */
  public static double getGroupGap() {
    return gapDelimiterSecs;
  }

  /** Set the maximum number of amps to return in one group, their amp times used for date range lookup of associated db events.
   * @see: UnassocAmplitudeTNGetter() */
 public static void setMaxInGroup(int max) {
   maxInGroup = max;
 }
 /** Get the maximum number of amps to return in one group, their amp times used for date range lookup of associated db events.
   * @see: UnassocAmplitudeTNGetter() */
 public static int getMaxInGroup() {
   return maxInGroup;
  }

  /** Set the maximum number of group amps to batch for a single commit. */
 public static void setMaxCommitSize(int max) {
   maxCommitSize = max;
 }

 /** Get the maximum number of group amps to batch for a single commit. */
 public static int getMaxCommitSize() {
   return maxCommitSize;
  }

  /** Set the alternate channel list. */
  public static void setAlternateChannelList(ChannelList list) { // changed from ChannelableList 2010/01/18 -aww
    alternateList = list;
  }

  /** Get the alternate channel list. */
  public static ChannelList getAlternateChannelList() { // changed from ChannelableList 2010/01/18 -aww
    return alternateList;
  }
  /** Set the master channel list, usually read from a cache file. */
  public static void setMasterChannelList(ChannelList list) { // added 2010/01/18 -aww
    masterList = list;
  }

  /** Get the master channel list. */
  public static ChannelList getMasterChannelList() { // added 2010/01/18 -aww
    return masterList;
  }

  /** Set the subsource of amps read from db, default is null, all subsources. */
  public static void setAmpSubsource(String source) {
    ampSubsource = source;
  }

  /** Get the subsource of amps read from db, default is null, all subsources. */
  public static String getAmpSubsource() {
    return ampSubsource;
  }

  /** Output controlled by 'verbose' flag. */
  public static void dump(String str) {
    if (debug || verbose) System.out.println("AmpAssocEngine: "+str);
  }

  /** Output controlled by 'verbose' flag. */
  public static void dumpError(String str) {
    if (debug || verbose) System.err.println("AmpAssocEngine: "+str);
  }

  // THIS IS THE STATIC METHOD CALLED BY THE ORACLE STORED PROCEDURE !
  /** Associate unassociated Amps. Get ALL amps from UnassocAmp table, try to assoc them,
   * and do dbase tasks that result.  Returns the number of amps associated. */
  public static int associateAmps() {

    if (! connectionOK()) return 0;

    int kntAssoced = 0;
    int totAssoced = 0;

    /* UnassocAmplitudeTNGetter will return smaller lists that contain only JasiReading objects clumped by time.
     * The value set in setDelimiterGapWindow() is used to recognized breaks between
     * clumps (groups). This is to increase efficiency.  */
    ampGetter = new UnassocAmplitudeTNGetter(getGroupGap(), getMaxInGroup());
    if ( ampSubsource != null) ampGetter.setAmpSubsource(ampSubsource);
    if (! ampGetter.initialize()) return 0;  // initQuery() removed from UnassocAmplitudeTNGetter constructor - aww 2009/08/31

    AmpAssoc.chanAssocMap.clear(); // reset associated solution mapping
    AmpAssoc.conn = getConnection();

    AmpList sublist = null;

    while (true) {
      sublist = ampGetter.getNextGroup();
      if (sublist == null || sublist.isEmpty()) break;

      // lookup lat/lon's of channels   !! could force Location Codes here
      insureLatLonZinfo(sublist);

      // cull out those amps w/o latlonz info - can't associate them
      //if (debug) System.out.println("DEBUG AmpAssocEngine: before cull sublist.size:"+ sublist.size() + " span: "+ sublist.getTimeSpan().toString());
      sublist = cullNullLatLonZ(sublist);

      if (!sublist.isEmpty()) {
        // do association logic, if an amp is associated its amp.associate(sol) method is called.
        // Don't really need to associate amps with solution amplist, could implement a method in AssocAmp class
        // to assign(sublist) (set the sol but don't add to it's internal list, which has less overhead than associate) -aww
        kntAssoced = AmpAssoc.associate(sublist);

        // do association work inside the dbase for those amps that got associated above (amp distance not null, i.e. < 99999.
        dump("Associated " + kntAssoced + " amps out of " + sublist.size() + " span: "+ sublist.getTimeSpan().toString() + " @ " + new DateTime());
        if (kntAssoced > 0) {
          if (debug) dump("\n"+sublist.toNeatString());  // optional output of result
          if ( ! connectionOK() ) {
              dumpError("associateAmps db connection failure... return");
              dump("Total Associated "+totAssoced+" amps @ " + new DateTime());
              AmpAssocEngine.closeAmpGetterConnection();
              return totAssoced;
          }
          //int total = sublist.size();
          //if ( total > 2000) {
              totAssoced += associateInDbBatch(sublist);
          //}
        }
      }

    }
    // added new method call to force closure of independent connection created for getter thread -aww 2010/04/23
    AmpAssocEngine.closeAmpGetterConnection();

    //System.out.println("Associated "+ totAssoced +" amps of "+ampList.size()+" this pass.");
    // housekeeping - remove unassociated amps older than expiration time
    cleanUp();

 /* DDG - test, unclosed connections were piling up on dbase, believe connection is the AmpGetter thread not this one, DataSource -aww 2010/04/23
     try {
          dump("associateAmps() closing dbase connection..."); 
          getConnection().close();
     } catch (SQLException e) {
          dump("Unable to close dbase connection: "+e.getMessage());
          e.printStackTrace();
     }
 */
         
    dump("Total Associated "+totAssoced+" amps @ " + new DateTime());
    return totAssoced;
  }
  /** Make sure all members have LatLonZ info.
   * If not try to look it up at the DataSource.
   * Return count of channels that have a LatLonZ. */
  static int insureLatLonZinfo(ChannelableListIF channelableList) {

        Channelable ch[] = channelableList.getChannelableArray();
        //Channelable chable;
        Channel chan = null;
        Channel chfound;

        // First list to check is the adhoc list  - aww 2010/01/21
        ChannelList list1 = (alternateList == null) ? masterList : alternateList;
        ChannelList list2 = masterList;  // no adhoc list, so default to masterList

        int knt = 0;   // count of channels with good LatLonZ
        java.util.Date date = new java.util.Date();
        boolean haveLLZ = false;

        for ( int i=0; i < ch.length; i++) {

          chan = ch[i].getChannelObj();

          // bug fix 2010/03/31 P.Lombard remove negation (!chan.getLatLonZ().isNull())) from test condition:
          if (chan.getLatLonZ().isNull()) { // missing LLZ, try is find it in channel lists
            haveLLZ = false;
            // 1st attempt to lookup channel, returns SELF if lookup fails
            // chfound = chan.lookUp(chan); // removed -aww 2010/01/18
            chfound = list1.lookUp(chan, date); // from cache -aww 2010/01/18

            if (! chfound.getLatLonZ().isNull()) { // found LatLonZ
                chan.setChannelObjData(chfound);
                //if (debug) System.out.println("DEBUG AmpAssocEngine: insureLatLonZinfo chan LLZ from list1:" + chan.toDelimitedSeedNameString("."));
                haveLLZ = true;
                knt++;
                // Set other channels of same type lower in list to speed things up
                knt += payItForward(chfound, ch, i+1);
            }
            else if (list2 != list1) { // no LLZ in 1st list, so if there is one, check the 2nd list 
                // replaced next 3 lines here below - aww 2010/01/18
                    //chable = list2.getByChannel(chan);
                    //if (chable != null) {
                    // chan.setChannelObjData(chable.getChannelObj());
                chfound = list2.lookUp(chan, date); // from cache -aww 2010/01/18
                if (! chfound.getLatLonZ().isNull()) { // found LatLonZ
                  chan.setChannelObjData(chfound);
                  //if (debug) System.out.println("DEBUG AmpAssocEngine: insureLatLonZinfo chan LLZ from list2:" + chan.toDelimitedSeedNameString("."));
                  haveLLZ = true;
                  knt++;
                  // Set other channels of same type lower in list - Speeds things up
                  //knt += payItForward(chable, ch, i+1); // removed -aww 2010/01/18
                  knt += payItForward(chfound, ch, i+1); //  added -aww 2010/01/18
                }
            }
            if (debug && !haveLLZ) {
                System.out.println("DEBUG AmpAssocEngine: insureLatLonZinfo no LLZ in any list for chan:" + chan.toDelimitedSeedNameString("."));
            }
          }  // end of lookup for missing LLZ for current chan 

        } // end of for loop
        return knt;
    }
    /** pay it forward: scan down the remaining list for channels for the same SNC
     * and set their channel objects too. This should minimize dbase accesses and
     * speed things up a bit. Returns count of channels changed. <p>
     * Assumes: 1) multiple channels of same type are in the amp list and
     *          2) this list scan is "cheaper" than dbase accesses */
    static public int payItForward(Channelable chan, Channelable[] ch, int startIndex) {
      int knt = 0;
      for (int k=startIndex+1; k<ch.length; k++) {
        if (ch[k].getChannelObj().equals(chan.getChannelObj()) &&
            ch[k].getChannelObj().getLatLonZ().isNull()) {
          ch[k].setChannelObjData(chan.getChannelObj());
          knt++;
        }
      }
      if (debug) {
          System.out.println("DEBUG AmpAssocEngine: payItForward for input chan:" + 
              chan.getChannelObj().toDelimitedSeedNameString(".") + " found count :" + knt);
      }
      return knt;
    }
    /** Return a new AmpList that is a subset of this list and
     * contains only those Amplitude objects that have a non-NULL LatLonZ.
     */
    static AmpList cullNullLatLonZ(AmpList amplist) {

      AmpList newList = AmpList.create();
      Amplitude amp[] = (Amplitude[]) amplist.toArray(new Amplitude[amplist.size()]);
      for (int i = 0; i<amp.length; i++) {
        if (!amp[i].getChannelObj().getLatLonZ().isNull()) {
          //if (debug) System.out.println("DEBUG AmpAssocEngine: HAVE LatLonZ for " + amp[i].getChannelObj().toDelimitedSeedNameString("."));
          newList.add(amp[i]);
        }
        else { // added 2010/01/19 -aww
            if (debug || logMissingLatLonZ) System.out.println("AmpAssocEngine: no LatLonZ " + amp[i].getChannelObj().toDelimitedSeedNameString("."));
            //if (verbose) System.out.println("No LLZ: "+amp[i].getChannelObj().toString());
        }
      }
      return newList;
    }
  /** Actually do the association in the dbase of all the amps in the list with
   * their associated solutions.
   * The details are implemented in a storedprocedure called assoc.moveAndAssoc().
   * Returns number of associated amps.
   * @see: associateInDb(Amplitude amp, long evid) */
  public static int associateInDbBatch(AmpList ampList) {

    // for 3-comp sta, at 6 amps per channels, 60 stations is ~1080 amps)

    // Create the prepared statements if they do not exist now
    if (getAddStatement() == null)  return 0;
    if (getDelStatement() == null)  return 0;

    long ampid = 0;
    long evid = 0;
    long orid = 0;

    Amplitude[] amp = ampList.getArray();

    ArrayList addList = new ArrayList(Math.min(amp.length, maxCommitSize));
    ArrayList delList = new ArrayList(Math.min(amp.length, maxCommitSize));

    // Return variable holds total count added by by db batch commits
    int cnt = 0;
    
    // add/delete amps using batch statements, loop over input amp list
    for (int i=0; i<amp.length; i++) {

        if ( ! amp[i].isAssociated()) continue;

        ampid = ((DataLong)amp[i].getIdentifier()).longValue();

        // reject duplicates
        if (getRejectDups() && duplicateExists(amp[i])) {
          dump("Rejecting duplicate amp:\n"+amp[i].toNeatString());

          try {
            delStmt.setLong(1, ampid);
            delStmt.addBatch();          // and one more statement to the batch
            delList.add(amp[i]);
          } catch (SQLException ex) {
            // constraint errors, etc. end up here
            ex.printStackTrace(System.err);
          }

          // delete from UnassocAmp table! !!!!!!!!!!!!!!!!!

        } else {

          evid  = amp[i].getAssociatedSolution().getId().longValue();
          orid  = ((DataLong)amp[i].getAssociatedSolution().getPreferredOriginId()).longValue();

          try {
            // addStmt parameters 
            addStmt.setLong(1, ampid);
            addStmt.setLong(2, evid);
            addStmt.setLong(3, orid);
            //if (ampSetType == null) throw new SQLException("AssocAmp.moveAndAssoc input ampsettype cannot be null"); 
            // statement attributes for ampset signature -aww 2010/11/22
            //NOTE:  Could amp[i].getSource()) vary from amp to amp within an ampset ? 
            addStmt.setString(4, EnvironmentInfo.getApplicationName());
            addStmt.setString(5, ampSetType);
            addStmt.setDouble(6, amp[i].getHorizontalDistance() );
            addStmt.setDouble(7, amp[i].getAzimuth() );

            if (debug) {
                System.out.print("DEBUG AmpAssocEngine associateInDbBatch ampid: " +ampid+ " evid: " +evid+ " orid: " +orid+
                    " source: " +EnvironmentInfo.getApplicationName()+ " ampSetType: " +ampSetType);
                System.out.println(" idx i: " + i + " dist: " + amp[i].getHorizontalDistance()  + " az: " + amp[i].getAzimuth() );
            }
            addStmt.addBatch(); // and one more statement to the batch
            addList.add(amp[i]);
          } catch (SQLException ex) {
            // constraint errors, etc. end up here
            ex.printStackTrace(System.err);
          }
        }

        if ( (addList.size() + delList.size()) >= maxCommitSize) {
            cnt += writeBatchToDb(evid, addList, delList);
        }

    } // end of loop

    // Execute remaining batched adds/deletes and commit,
    // note that this would be the only db write whenever maxCommitSize >= maxInGroup
    if ( (addList.size() + delList.size()) > 0 ) {
        cnt += writeBatchToDb(evid, addList, delList);
    }

    return cnt;

  }

  private static int writeBatchToDb(long evid, ArrayList addList, ArrayList delList ) {

      // batch amp counters for positive result return?
      int delKnt = 0;
      int addKnt = 0;
      int results[] = null;

      // first do the execute batch for the added associated amps
      if (addList.size() > 0 ) {
          try {
              results = addStmt.executeBatch();
              addKnt = 0;
              for (int j=0; j<results.length; j++) {
                if (results[j] <= 0) {
                    System.out.println("AmpAssocEngine: addStmt.executeBatch result ERROR? "+results[j] +
                        " for ampid: " + ((UnassocAmplitudeTN )addList.get(j)).getAmpid()
                    );
                }
                else addKnt++;
              }
          } catch (SQLException ex) {
                ex.printStackTrace(System.err);
                ex.getNextException().printStackTrace(System.err);
          }
          dump("db (added/total)=(" + addKnt + "/" + addList.size() + ") amps for evid: " + evid  + " @ " + new DateTime());
          addList.clear();
      }

      // do the execute batch for the delete of (duplicates) 
      if (delList.size() > 0 ) { 
          try {
              results = delStmt.executeBatch();
              delKnt = 0;
              for (int j=0; j<results.length; j++) {
                if (results[j] <= 0) {
                    System.out.println("AmpAssocEngine: delStmt.executeBatch result ERROR? "+results[j] +
                        " for ampid: " + ((UnassocAmplitudeTN )delList.get(j)).getAmpid()
                    );
                }
                else delKnt++;
              }
          } catch (SQLException ex) {
                ex.printStackTrace(System.err);
          }
          dump("db (deleted/total)=(" + delKnt + "/" + delList.size() + ") amps for evid: " + evid  + " @ " + new DateTime());
          delList.clear();
      }

      // commit
      //if ( (addKnt+delKnt) > 0 ) {
          try {
              getConnection().commit();
              dump("db commit for evid: " + evid  + " @ " + new DateTime());
          } catch (SQLException ex) {
              ex.printStackTrace(System.err);
          }
      //}

        return addKnt;  // return total added
  }

  /** Returns true if there are already amps like this one in the dbase.
   * That is, those that are associated with the same evid,
   * are from the same channel with the same amptype, value, and time
   * @See: AmplitudeTN.getDuplicate() */
  protected static boolean duplicateExists(Amplitude amp) {
    // Note we want to check the Amp table not the UnasocAmp table for the duplicates
    AmplitudeTN amptn = new AmplitudeTN();
    AmpList list = amptn.getDuplicate(getConnection(), amp); // not the DataSource connection here
    if (list == null || list.isEmpty()) return false;
    boolean duplicate = false;
    // added extra conditions test below -aww  2010/12/07
    // Have same channel and type in list check for time and value:
    for (int idx=0; idx<list.size(); idx++) {
        amptn = (AmplitudeTN) list.get(idx);
        duplicate = (amptn.getTime() == amp.getTime() && amptn.value.equalsValue(amp.value));
        if (duplicate) break;
    }
    return duplicate;  // there are dups!

  }
/*
"PreparedStatement objects eventually become more efficient than their Statement
counterparts after 65-125 executions" &
"Oracle supports batching only when PreparedStatement objects are used"
  - http://www.oreilly.com/catalog/jorajdbc/chapter/ch19.html
*/
private static Statement getAddStatement() {
  String addSQL = "";
  if (addStmt == null) {
    if (isPostgreSQL() ) {
       addSQL = "SELECT * FROM AssocAmp.MoveAndAssocAmp(?, ?, ?, ?, ?, ?, ?)"; // note batching restriction requires procedure call, not a function
    } else {
       addSQL = "{ CALL AssocAmp.MoveAndAssocAmp(?, ?, ?, ?, ?, ?, ?)}"; // note batching restriction requires procedure call, not a function
    }
    try {
      if (debug) System.out.println("DEBUG AmpAssocEngine ampSetType: " + ampSetType + " getAddStatement: "+ addSQL);
      addStmt = getConnection().prepareStatement(addSQL);
    } catch (SQLException ex) {
      ex.printStackTrace(System.err);
    }
  }

  return addStmt;
}
/*
"PreparedStatement objects eventually become more efficient than their Statement
counterparts after 65-125 executions" &
"Oracle supports batching only when PreparedStatement objects are used"
  - http://www.oreilly.com/catalog/jorajdbc/chapter/ch19.html
*/
private static Statement getDelStatement() {
  if (delStmt == null) {
    String delSQL = "Delete from UnassocAmp where ampid = ?";
    try {
      if (debug) System.out.println("DEBUG AmpAssocEngine getDelStatement: "+ delSQL);
      delStmt = getConnection().prepareStatement(delSQL);
    } catch (SQLException ex) {
      ex.printStackTrace(System.err);
    }
  }

  return delStmt;
}

/** Return if connected to a PostgreSQL database **/
  public static boolean isPostgreSQL() {
      boolean status = false;
      try {
          status = getConnection().getMetaData().getDatabaseProductName().toString().equalsIgnoreCase("postgresql");
      } catch (SQLException ex) {
          ex.printStackTrace(System.err);
      }
      return status;
  }

/** Return true if the dbase connection is good. */
  public static boolean connectionOK() {
    boolean status = true;
    try {
      Connection conn = getConnection();
      if (conn == null || conn.isClosed()) {

        // reset batch statements
        try {
          if (addStmt != null) addStmt.close();
          if (delStmt != null) delStmt.close();
        }
        catch (SQLException ex1) {
        }
        addStmt=null;
        delStmt = null;

        dump("connectionOK() creating new db connection..."); // added for debugging - aww 2010/04/23
        setConnection(DbaseConnection.getConnection()); // creates new one
        conn = getConnection();
        if (conn == null || conn.isClosed()) {
          System.err.println("AmpAssocEngine: Connection is null or closed!");
          status = false;
        }
      }
    } catch (SQLException ex) {
        status = false;
        //System.err.println(ex.getMessage());
        ex.printStackTrace(System.err);
    }
    return status;
  }

  /** Clean up old (expired) amps. Note: this deletes Amps with LDDATEs in
   * the UnassocAmp table more than one day old <b>not</b> DATETIME.
   * Returns the number of deleted Amps. */
  public static int cleanUp() {
    return cleanUp(daysBack);
  }

  /** Number of days back from current time of the oldest loaded amp to keep in unassocamp table. */
  public static double getDaysBack() {
      return daysBack;
  }

  /** Number of days back from current time of the oldest loaded amp to keep in unassocamp table. */
  public static void setDaysBack(double days) {
      daysBack = days;
  }

  /** Clean up old (expired) amps, whose lddate is olden than input days back. Returns the number of deleted Amps. */
  public static int cleanUp(double daysBack) {

    if (!connectionOK()) return 0;

    //     String sql = "{ ? = call AssocAmp.cleanUp()}";   // default is 7 days back
    //     String sql = "{ ? = call AssocAmp.cleanUp("+daysBack+")}";
    //     CallableStatement stmt = conn.prepareCall(sql);
    //     stmt.registerOutParameter(1, java.sql.Types.INTEGER);  // bind return value
    // Changed 3/1/04 because stored proc also removed SNCL's that aren't in Channel_data
    // and now we use an Alternate Ad Hoc list.
    // RH need to bifurcate here for Oracle vs PostgreSQL 
    String sql = null;
    if ( isPostgreSQL() )  {
        sql = "Delete from UnassocAmp where LDDATE < (SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) - INTERVAL '"+daysBack+" days' )" ;
    } else {
        sql = "Delete from UnassocAmp where LDDATE < (SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) - "+daysBack+" )" ;
    };
    Statement stmt = null;

    // handle dbase deletes (dupes)
    int status = 0;
    try {
      stmt = getConnection().createStatement();
      status = stmt.executeUpdate(sql);
      getConnection().commit();
    } catch (SQLException ex) {
      ex.printStackTrace(System.err);
      status = 0;
    }
    finally {
      try {
          if (stmt != null) stmt.close();
      } catch (SQLException ex) {
          ex.printStackTrace(System.err);
      }
    }

    dump("Cleanup deleted "+status+" dbase rows @ " + new DateTime());

    return status;
  }

  /** Return all unassociated amps sorted by date.*/
  // TO DANGEROUS -- THERE CAN BE BILLLLLIONS AND BILLLLLIONS.... //
  public AmpList getUnassocAmps() {
    UnassocAmplitudeTN ampTN = new UnassocAmplitudeTN();
    AmpList ampList = AmpList.create();
    ampList.fastAddAll(ampTN.getAll()); // for now just get everything, may need to be clever later
    return ampList;
  }

  /**
   * Execute an 'sql' command with no data return. Returns 'true' on success
   */
  public static boolean doSql(Connection conn, String sql) {

      if (! connectionOK()) return false;

      Statement stmt = null;
      boolean status = true;

      try {

        stmt = conn.createStatement();

        stmt.executeQuery(sql);


      } catch (SQLException ex) {
        ex.printStackTrace(System.err);
        status = false;
      }
      finally {
        try {
          if (stmt != null) stmt.close();
        } catch (SQLException ex) {}
      }

      return status;

    }

    // added new method call to force closure of independent connection created for getter thread -aww 2010/04/23
    public static void closeAmpGetterConnection() {

        if (ampGetter == null) return;

        try {
             ampGetter.closeConnection(); // note, this is per instance, not class static
        }
        catch (Throwable t) {
        }

        ampGetter = null;

    }

    public static void closeConnections() { // 2 possible db connections

        closeAmpGetterConnection(); // close the ampGetter "instance" connection

        // close this class static connection, if not null -aww 2010/01/18
        if (conn == null) return;
        try {
            conn.close();
        }
        catch (Throwable t) {
        }

        conn = null;
    }

    protected static void debugDump(AmpList ampList) {

        String str = "No readings.";
        Amplitude amp[] = ampList.getArray();

        if (amp.length > 0)  {

        str = amp[0].getNeatStringHeader() + "\n";

        for (int i=0; i<amp.length; i++) {
          str += amp[i].toNeatString();
          if (amp[i].isAssociated())
              str += " ** "+amp[i].getAssociatedSolution().getId().longValue();
              str += "\n";
          }
        }

        System.out.println(str);
    }

/*
  public static void main(String[] args) {

    //String propFilename = "c:/temp/UCB/AmpAssocEventProperties";
    String propFilename = "C:/CodeSpace/Testing/GMP/gmp2db.props";
            
//    System.out.println("Making connection...");
// //    DataSource ds = new TestDataSource("iron");
//    DataSource ds = TestDataSource.create("serverma");
//
//    AmpAssocEngine.cleanUp();
//    AmpAssocEngine.setVerbose(true);
//    AmpAssocEngine.setConnection(ds.getConnection());

    // This form of Constructor uses explicite path/file: does not use <userhome>
    EventSelectionProperties evtProps = new EventSelectionProperties(propFilename, null);
    AmpAssocEngine.setEventSelectionProperties(evtProps);

    System.out.println("Making connection...");
    DataSource ds = TestDataSource.create("serverma");

    String adHocFilename = "C:/CodeSpace/Testing/GMP/adhoc.lis";
    ChannelList adHocList = AdHocFormat.readInFile(adHocFilename);
    AmpAssocEngine.setAlternateChannelList(adHocList);

    associateAmps();
    //debugDump(list);
  }
*/
}

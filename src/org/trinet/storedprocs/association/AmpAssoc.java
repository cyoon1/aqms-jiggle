package org.trinet.storedprocs.association;

import java.sql.*;
import java.util.HashMap;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.UnassocAmplitudeTN;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;  /// LatLonZ
import org.trinet.storedprocs.DbaseConnection;
/**
 * Used by Oracle stored procedure and other code for associating Amp table rows with events.
 */

public class AmpAssoc extends PointAssociator {

  static HashMap chanAssocMap = new HashMap(17);

  public static PowerLawTimeWindowModel plm = new PowerLawTimeWindowModel(); // added to test using PowerLawTimeWindowModel -aww 2013/06/26

  /** Return the Solution this Amplitude should be associated with.
   * Returns null if no match.<p>
   * NOTE: This gets the amp from the UnassocAmp table and NOT
   * the Amp table. */
  public static Solution getAssocEvent(long ampid) {
    DbaseConnection.getConnection();
    Amplitude amp = (Amplitude) UnassocAmplitude.create().getById(new Long(ampid));  // Note UnassocAmplitudeTN not a subclass of UnassocAmp
    if (amp == null) return null;
    return getAssocEvent(amp);
  }
  /** Return the event ID for this Amplitude. Returns 0 if no match.<p>
   * NOTE: This gets the amp from the UnassocAmp table and NOT
   * the Amp table. */
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public static long getAssocEventId(long ampid) {
    Solution sol = getAssocEvent(ampid);
    if (sol == null) return 0;
    return sol.getId().longValue();
  }

  /** Return the event ID for this Amplitude. Returns 0 if no match. */
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public static long getAssocEventId(String net,
                                     String sta,
                                     String seedchan,
                                     String loc,
                                     double amptime,
                                     double ampvalue) {

    Solution sol = getAssocEvent(net, sta, seedchan, loc, amptime, ampvalue);
    if (sol == null) return 0;
    return sol.getId().longValue();
}

  /** Return the Solution this Amplitude should be associated with.
   * Returns null if no match. */
  public static Solution getAssocEvent(String net,
                                   String sta,
                                   String seedchan,
                                   String loc,
                                   double amptime,
                                   double ampvalue) {

      DbaseConnection.getConnection();
      // make an amp
      Amplitude amp = Amplitude.create();
      // note we put seedchan in channel also
      Channel ch = amp.getChannelObj();
      ch.setChannelName(net, sta, seedchan, loc, null, null, seedchan, null);
      amp.setTime(amptime);
      // look up latlonz (typically an invoking engine should have set it already via master ChannelList)
      amp.setChannelObj(ch.lookUp(ch, LeapSeconds.trueToDate(amptime) ) ); // amptime is true UTC seconds - aww 2008/02/08

      //data below here is used - aww
      amp.value.setValue(ampvalue);
      //amp.setType(AmpType.WAS);      // DDG 9/15/03
      //amp.units = Units.MM;
      //if (debug)  System.out.println(amp.toString());

      return getAssocEvent(amp);

  }

  /** Return the solution this Amplitude should be associated with. Assumes the Amplitude object has
   a valid ChannelObject that includes a good LatLonZ. Returns null if no match. */
  public static Solution getAssocEvent(Amplitude amp) {

    if (amp == null || amp.getChannelObj() == null) {
      return null;
    }
    // make sure dbase connection exists (incase we're being called static as a stored proc.)
    DbaseConnection.getConnection();

    LatLonZ llz = amp.getChannelObj().getLatLonZ();
    if (llz == null || llz.isNull()) {   // no latlonz, we'll have to look it up
      Channel ch = amp.getChannelObj();
      return getAssocEvent(ch.getNet(), ch.getSta(), ch.getSeedchan(), ch.getLocation(),
                                   amp.getTime(), amp.value.doubleValue());
    } else {
      return getAssocEvent(llz.getLat(), llz.getLon(),
                           llz.getZ(), amp.getTime());
    }
  }
  /** Return the event ID for this Amplitude. Assumes the Amplitude object has
   a valid ChannelObject that includes a good LatLonZ. Returns 0 if no match. */
  public static long getAssocEventId(Amplitude amp) {
    Solution sol = getAssocEvent(amp);
    if (sol == null) return 0;
    return sol.getId().longValue();
  }
  /** Attempt to associate all amps in this list, with solutions in the DataSource */
  public static int associate(AmpList ampList) {
    SolutionList solList = getCandidateSolutionList(ampList);
    if (solList == null || solList.isEmpty()) {
      if (debug) {
        System.out.println("* AmpAssoc: No events (empty list) for this amp group: "+
                           ampList.size()+ " amps "+
                           ampList.getTimeSpan().toString());
        //ampList.dump();
      }
      return 0;
    }
    return associate(ampList, solList);
  }
  /** Attempt to associate all amps in this list, with solutions in the SolutionList.
   * Returns the number of amps that were associated. */
  public static int associate(AmpList ampList, SolutionList solList) {
    int knt = 0;
    if (ampList == null || ampList.isEmpty() ||
        solList == null || solList.isEmpty()) return knt;

    //Could implement below to impose rejection of amps by some sta,seedchan,location in the model's reject properties
    //ampList = plm.filterListByChannelPropertyAttributes(ampList);
    //if ( ampList.isEmpty() ) return knt;

    Amplitude amp[] = (Amplitude []) ampList.toArray(new Amplitude[ampList.size()]);

    UnassocAmplitude uamp = null;
    // for each amp...
    boolean status = false;
    for (int i = 0; i< amp.length; i++){
      status = associate(amp[i], solList);
      if (knt == 0 && status) System.out.println(UnassocAmplitude.getNeatStringHeader());
      if (status) {
          if (amp[i] instanceof UnassocAmplitude) {
              uamp = (UnassocAmplitude) amp[i];  
              //chanAssocMap.put(uamp.getChannelObj().toDelimitedSeedNameString(), uamp.getAssociatedSolution());
              //if (uamp.getFileid().longValue() == 0l) {
              //    System.out.println("AmpAssoc UnassocAmplitude fileid:" + uamp.getFileid().longValue() +
              //            " for: " + uamp.getChannelObj().toDelimitedSeedNameString()); 
              //}
              chanAssocMap.put(uamp.getFileid(), uamp.getAssociatedSolution());
          }
          knt++;
      }
    }

    // However, if unasocamps from single input file straddle a getNext... grouping 
    // possibly some amps in file would not be associated 
    Solution aSol = null;
    for (int i = 0; i < amp.length; i++) {
        if (amp[i] instanceof UnassocAmplitude) {
            uamp = (UnassocAmplitude) amp[i];  
            if (! uamp.isAssociated()) {
                //aSol = (Solution) chanAssocMap.get(uamp.getChannelObj().toDelimitedSeedNameString()); 
                aSol = (Solution) chanAssocMap.get(uamp.getFileid()); 
                if (aSol != null) {
                    if (setAssocValues(uamp, aSol)) knt++;
                }
            }
        }
        else break;  // assume none in list are
    }

    return knt;
  }

  /**  Attempt to associate this amp with solutions in the SolutionList.
   * Returns 'true' if the amp is successfully associated.*/
  public static boolean associate(Amplitude amp, SolutionList solList) {
    if (amp == null || solList == null || solList.isEmpty()) return false;

    if (amp.isAssociated()) return true; // short-circuit here

    Solution sol = null;

//    if (debug) System.out.println("* Proc: "+amp.toString());

    LatLonZ llz = amp.getChannelObj().getLatLonZ();

    if (llz.isNull()) {
      Channel ch = amp.getChannelObj();
      // look up latlonz (typically an invoking engine should have set it already via master ChannelList)
      amp.setChannelObjData(ch.lookUp(ch, amp.getDateTime())); // amptime is true UTC seconds - aww 2008/02/08
    }
    llz = amp.getChannelObj().getLatLonZ();
    if (llz.isNull()) { // still no LatLonZ - can't associate
      if (debug) System.err.println("DEBUG AmpAssoc: skipping amp with no lat/lon \n"+ amp.toString());
      return false;
    }
    // look for "best fit" solution (list is those Solutions whose energy window overlaps the AmpList time range (thus AmpList must be time sorted)
    sol = getAssocEvent(llz, amp.getTime(), solList);

    boolean retVal = false;
    if (sol != null) {
      retVal = setAssocValues(amp, sol);   // KLUDGE to set dist & az of amp reading to ESAZ
    }
    if (debug) {
       if (! retVal) System.out.println("DEBUG AmpAssoc: No associated event found for amp:\n" + amp.toString());
    }
    return retVal;
  }

  /** Set the values that will be used to populate the AssocaAmO table. */
  private static boolean setAssocValues(Amplitude amp, Solution sol) {
      amp.calcDistance(sol); // 06/11/2004 aww, if this method setAzimuth - do this before the associate below 2013/06/26 -aww
      //Use PowerLawModel to determine if channel should have seismic energy, if not, skip it
      double mag = (sol.magnitude == null || sol.magnitude.hasNullType() || sol.magnitude.hasNullValue()) ?
                               plm.getDefaultNullMag() : sol.magnitude.getMagValue();
      Channel ch = amp.getChannelObj(); 
      boolean tf = true;
      // added to test using PowerLawTimeWindowModel -aww 2013/06/26
      tf = plm.shouldShowEnergy(ch, mag, ch.getDistance(), sol.getModelDepth()); // switch to model depth -aww 2015/10/10
      long id = (amp instanceof UnassocAmplitudeTN) ? ((UnassocAmplitudeTN) amp).getAmpid() : 0l; 
      if (tf) { 
          amp.associate(sol);   // associate it, does it do calcDistance azimuth correctly?
          // Note station to event azimuth, not HYP2000 compatible
          //amp.setAzimuth(amp.getChannelObj().getLatLonZ().azimuthTo(sol.getLatLonZ())); // SEAZ
          // Need event to station azimuth ESAZ like Hypo2000 output use azimuthFrom - aww
          //amp.setAzimuth(amp.getChannelObj().getLatLonZ().azimuthFrom(sol.getLatLonZ())); // ESAZ or
          amp.setAzimuth(sol.getLatLonZ().azimuthTo(amp.getChannelObj().getLatLonZ())); // 06/11/2004 aww
          // report each association amptime is true UTC seconds - aww 2008/02/08 
          System.out.println(amp.toNeatString() + ": ampid: " + id + " associated @ " + new DateTime().toString() +
              " to evid: "+ sol.getId().longValue() + " sse?: " + tf); 
          return true;
      }

      System.out.println(amp.toNeatString() + ": ampid: " + id + " skipped @ " + new DateTime().toString() +
          " no energy predicted for evid: "+ sol.getId().longValue() + " mag: " + mag); 
      //System.out.println(amp.toNeatString());

      return false;
  }
/*
 public static void main(String[] args) {

//    long evid = 12475624;
    long ampid = 18929714;

    TestDataSource.create("iron");

    long id = AmpAssoc.getAssocEventId(ampid);
    System.out.println("Result 0: evid = "+ id);
//
//    DbaseConnection.getConnection();
//
//    AmpList ampList = AmpList.create();
//    ampList.fastAddAll(Amplitude.create().getBySolution(evid));
//
//    if (ampList != null) {
//      Amplitude amps[] = ampList.getArray();
//
//      int result = 0;
//      for (int i = 0; i< amps.length; i++) {
//        System.out.println(amps[i].toString());
//        Channel ch = amps[i].getChannelObj();
//        id = getAssocEventId(ch.getNet(), ch.getSta(), ch.getSeedchan(), ch.getLocation(),
//                             amps[i].getTime(), amps[i].value.doubleValue());
//        System.out.println("Result: evid = "+ id);
//
//        id = getAssocEventId(amps[i]);
//        System.out.println("Result 2: evid = "+ id);
//
//        ampid = ((org.trinet.jasi.TN.AmplitudeTN)amps[i]).getAmpid();
//        id = getAssocEventId(ampid);
//        System.out.println("Result 3: evid = "+ id);
//      }
//    }
 }
 */
}

package org.trinet.apps;

import java.io.*;
import java.util.*;

/**
 * Just a test program from  http://leepoint.net/notes-java/41io/10file/07filetest.html
 * To see what the hell all the File methods mean.
 * */
 /*

 RESULTS ARE:

getName()          = FileTest.java
getAbsoluteFile().getName() = FileTest.java
exists()           = true
canRead()          = true
canWrite()         = true
getPath()          = T:\CodeSpace\TPP\src_cvs\org\trinet\apps\FileTest.java
getAbsolutePath()  = T:\CodeSpace\TPP\src_cvs\org\trinet\apps\FileTest.java
getCanonicalPath() = T:\CodeSpace\TPP\src_cvs\org\trinet\apps\FileTest.java
getAbsoluteFile()  = T:\CodeSpace\TPP\src_cvs\org\trinet\apps\FileTest.java
toURL()            = file:/T:/CodeSpace/TPP/src_cvs/org/trinet/apps/FileTest.java
getParent()        = T:\CodeSpace\TPP\src_cvs\org\trinet\apps
isAbsolute()       = true
isDirectory()      = false
isFile()           = true
isHidden()         = false
lastModified()     = 1077295340584 = Fri Feb 20 08:42:20 PST 2004
length()           = 2601

 */
class FileTest {
    public static void main(String[] args) {

      // must escape the MS back-slash
      String filename = "T:\\CodeSpace\\TPP\\src_cvs\\org\\trinet\\apps\\FileTestxx.java";
      //String filename = "T:\CodeSpace\TPP\src_cvs\org\trinet\apps\FileTest.java";

        //-- Make sure there is one parameter
//        if (args.length != 1) {
//            System.err.println("ERROR: must have 1 parameter");
//            System.exit(1);
//        }

        try {
//            File f = new File(args[0]);
            File f = new File(filename);
            long d;

            System.out.println("getName()          = " + f.getName());
            System.out.println("getAbsoluteFile().getName() = "
                              + f.getAbsoluteFile().getName());
            boolean exists = f.exists();
            System.out.println("exists()           = " + exists);
//            if (!exists) {
//                System.exit(1);
//            }
            System.out.println("canRead()          = " + f.canRead());
            System.out.println("canWrite()         = " + f.canWrite());
            System.out.println("getPath()          = " + f.getPath());
            System.out.println("getAbsolutePath()  = " + f.getAbsolutePath());
            System.out.println("getCanonicalPath() = " + f.getCanonicalPath());
            System.out.println("getAbsoluteFile()  = " + f.getAbsoluteFile());
            System.out.println("toURL()            = " + f.toURL());
// v1.4            System.out.println("toURI()            = " + f.toURI());
            System.out.println("getParent()        = " + f.getParent());
            System.out.println("isAbsolute()       = " + f.isAbsolute());
            boolean isDirectory = f.isDirectory();
            System.out.println("isDirectory()      = " + isDirectory);
            System.out.println("isFile()           = " + f.isFile());
            System.out.println("isHidden()         = " + f.isHidden());
            System.out.println("lastModified()     = " + (d = f.lastModified())
                               + " = " + new Date(d));
            System.out.println("length()           = " + f.length());
            if (isDirectory) {
                String[] subfiles = f.list();
                for (int i=0; i<subfiles.length; i++) {
                    System.out.println("file in this dir   = " + subfiles[i]);
                }
            }
        } catch (IOException iox) {
           System.err.println(iox);
        }
    }//endmethod main
}//endclass FileTest


/**
OUTPUT FILE FORMAT:<br>
Each output line consists of 10 tokens (9 comma separated values ending with newline).
A NULL column value is represented by zero length token.  Output table columns are:
type,lat,lon,z,name,state,county,map,datumh,datumv. The minimal non-null token values are
type,lat,lon, and name. The lat,lon format is (%+10.6f,%+11.6f).
<pre>
10,+34.156700,-118.755000,,Agoura Hills,,,,,
</pre>
*/

package org.trinet.apps;

import java.io.*;
import java.sql.*;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class DumpGazetteerPt {
            
    private static Statement ps = null;

    // fault code from gazetteertype table, could lookup value by query: "select code from gazetteertype where name='fault'";

    private static final String SELECT_SQL = "select gazid,type,lat,lon,z,name,state,county,map,datumh,datumv from gazetteerpt order by type,name"; 

    public static final void main(String[] args) {

        if (args.length != 6) {
            System.err.println("Usage: java DumpGazetteerPt dbhost dbname dbport user passwd outfilename");
            System.exit(-1);
        }

        String dbhost = "";
        String dbname = "";
        String dbport = "";
        String dbuser = "";
        String dbpass = "";
        String filename = "";

        try {
          dbhost = args[0];
          dbname = args[1];
          dbport = args[2];
          dbuser = args[3];
          dbpass = args[4];
          filename = args[5];
        }
        catch ( Exception ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }

        // Create a DataSource connection to input db here
        String url = String.format("jdbc:%s:@%s:%s:%s", AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL, dbhost, dbport, dbname);
        DataSource ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, dbuser, dbpass);    // make connection

        ds.setWriteBackEnabled(true);

        System.out.println("DumpGazetteerPt db:\n " + ds.describeConnection());
        System.out.println(" output file = " + filename);

        if (ds.getConnection() == null) {
            System.err.println("DumpGazetteerPt Error: null DataSource db connection!");
            System.exit(0);
        }

        boolean status = true;

        BufferedWriter bwrt = null;
        ResultSet rs = null;

        try {

            bwrt = new BufferedWriter(new FileWriter(filename));

            // Create new gazetteerline table row
            if (ps == null) {
                ps = DataSource.getConnection().createStatement();
            }
                
            rs = ps.executeQuery(SELECT_SQL);

            int gazid = 0;
            int type = 0;
            double lat = 0.;
            double lon = 0.;
            int z = -99999;
            String name = null;
            String state = null;
            String county = null;
            String map = null;
            String datumv = null;
            String datumh = null;
            while ( rs.next() ) {

                z = -99999;
                state = null;
                county = null;
                map = null;
                datumv = null;
                datumh = null;

                gazid = rs.getInt(1); // not null
                //type
                type = rs.getInt(2); // not null
                //lat
                lat = rs.getDouble(3); // not null
                //lon
                lon = rs.getDouble(4); // not null
                //z (elev meters)
                z = rs.getInt(5);
                if (rs.wasNull()) z = -99999;
                //name
                name = rs.getString(6); // not null
                //state
                state = rs.getString(7);
                //county
                county = rs.getString(8);
                //map
                map = rs.getString(9);
                //datumh
                datumh = rs.getString(10);
                //datumv
                datumv = rs.getString(11);

                // Now output pt description to file
                bwrt.write(String.valueOf(type)); // number not null
                bwrt.write(",");
                bwrt.write(String.format("%+10.6f",lat)); // lat not null
                bwrt.write(",");
                bwrt.write(String.format("%+11.6f",lon)); // lon not null
                bwrt.write(",");
                if (z != -99999) bwrt.write(String.format("%04d",z)); // z (meters) 
                bwrt.write(",");
                bwrt.write(name); // not null
                bwrt.write(",");
                if (state != null) bwrt.write(state);
                bwrt.write(",");
                if (county != null) bwrt.write(county);
                bwrt.write(",");
                if (map != null) bwrt.write(map);
                bwrt.write(",");
                if (datumh != null) bwrt.write(datumh);
                bwrt.write(",");
                if (datumv != null) bwrt.write(datumv);
                bwrt.write("\n");
            }
        }
        catch (Exception ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (ds != null) ds.close();
            }
            catch (SQLException ex) {
            }

            try {
              if (bwrt != null) {
                  bwrt.flush();
                  bwrt.close();
              }
            }
            catch (IOException ex) { }
        }

        System.exit((status ? 0 : -1));
    }

} // end of DumpGazetteerPt class

package org.trinet.apps;
import java.io.*;
import java.util.*;

import org.trinet.filters.TotalGroundMotionFilter;
import org.trinet.filters.AccelFilter;
import org.trinet.jiggle.WaveServerGroup;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.jdbc.datasources.*;

public class TriggerAmps {

    /**The jiggle-style properties object. */
    private static SolutionWfEditorPropertyList props;

    private static boolean debug = true;

    private static DataSource ds = null;

    private static String channelGroupName = "AmpGen";
    private static String sourceString = "TrigAmp";
    private static String authString = "CI";
    private static String ampOutputDir = "";

    /** seconds before P-wave to include in the scanned window */
    private static double preEventTime = 1.0;
    /** minimum length in seconds to scan */
    private static double minTimeWindow = 6.0;
    /** maximum length in seconds to scan */
    private static double maxTimeWindow = 120.0;
    /** the maximum distance in km out to which include stations. */
    private static double maxDistance = 500.0;
    /** If true write resulting Amplitudes to the database. */
    private static boolean writeToDB = false;

    /** accelerations must be above this value (cm/sec**2) */
    private static double minAcceleration= 10; // 100.0;
    /** velocities must be above this value (cm/sec) */
    private static double minVelocity=.10;

    private static ChannelTimeWindowModel ctwModel; // = new EnergyWindowModel();

    private static void setChannelGroupName(String name) {
        channelGroupName = name;
    }

    private static String getChannelGroupName() {
        return channelGroupName;
    }

    private static void doThreshold(AmpList inList, AmpList outList) {
        if (inList == null || inList.isEmpty()) return;

        Channel ch = ((Channelable) inList.get(0)).getChannelObj();
        Amplitude amp = null;

        if (ch.isAcceleration()) {
          amp = inList.getType(AmpType.PGA);
          if (amp == null) return;

          if (Math.abs(amp.getValue().doubleValue()) > minAcceleration) {  // threshold
              outList.add(amp);;
          }

        } else if (ch.isVelocity()) {
          amp = inList.getType(AmpType.PGV);
          if (amp == null) return;

          if (Math.abs(amp.getValue().doubleValue()) > minVelocity) {  // threshold
              outList.add(amp);;
          }
        }

    }

    private static boolean setPropertiesFile(String propertiesFile) {

        props = new SolutionWfEditorPropertyList();
        props.setUserPropertiesFileName(propertiesFile);
        props.reset();    // reads all the property files
        props.setup();

        // set values
        if (getProperties() != null) {
          if (props.isSpecified("channelGroupName")) channelGroupName = props.getProperty("channelGroupName");
          if (props.isSpecified("localNetCode"))     authString = props.getProperty("localNetCode");
          if (props.isSpecified("sourceName"))       sourceString = props.getProperty("sourceName");

          if (props.isSpecified("maxDistance"))      maxDistance = props.getDouble("maxDistance");
          if (props.isSpecified("maxTimeWindow"))    maxTimeWindow = props.getDouble("maxTimeWindow");
          if (props.isSpecified("minTimeWindow"))    minTimeWindow = props.getDouble("minTimeWindow");
          if (props.isSpecified("preEventTime"))     preEventTime = props.getDouble("preEventTime");
          if (props.isSpecified("writeToDB"))        writeToDB = props.getBoolean("writeToDB");
          if (props.isSpecified("debug"))            debug = props.getBoolean("debug");

          if (props.isSpecified("minAcceleration"))  minAcceleration = props.getDouble("minAcceleration");
          if (props.isSpecified("minVelocity"))      minVelocity = props.getDouble("minVelocity");

          ampOutputDir = props.getProperty("ampOutputDir","");
      }

      return (props != null);
    }

    private static SolutionWfEditorPropertyList getProperties() {
        return props;
    }

    // ////////////////////////////////////////
    public static final void main(String[] args) {
    
        String propfilename = "triggerAmps.props";
    
        // Print syntax if no args
        if (args.length < 2) {
          System.out.println ("Usage: TriggerAmps [evid] [properties-file]");
          System.exit(0);
        }
    
        // Get event ID
        long evid = 0;;
        if (args.length > 0) {
          Long val = Long.valueOf(args[0]);
          evid = val.longValue();
        }
    
        // Get input props filename
        if (args.length > 1) {
          propfilename = args[1];
        }
    
        // Read properties from file
        if (!setPropertiesFile(propfilename)) {
          System.err.println("TriggerAmps: Bad properties file: "+propfilename) ;
          stop(0);
        } else if (debug) {
          getProperties().dumpProperties();
          System.out.println();
        }

        // Set global app name from property
        EnvironmentInfo.setApplicationName(sourceString);
    
        // Setup benchmark for timing
        BenchMark bm = new BenchMark ();
    
        // Create db connection
        DbaseConnectionDescription dbd = getProperties().getDbaseDescription();
        if (debug) System.out.println ("TriggerAmps: Making connection to "+dbd.getURL());
        ds = new DataSource();
        ds.set(dbd); // aww 2008/06/26
    
        System.out.println("================== TriggerAmps Start @ " + new DateTime() + "==================");

        // Get waveforms input mode 
        int wfMode = getProperties().getInt("waveformReadMode"); // =0 database, =1 waveserver

        if (wfMode == AbstractWaveform.LoadFromDataSource) { // from database
          System.err.println("TriggerAmps: Using DataSourceChannelTimeModel");
          AbstractWaveform.setWaveDataSource(getProperties().getWaveSource());  //'waveformReadMode' doesn't matter
          ctwModel = new DataSourceChannelTimeModel(); // define the CTW model

        } else if (wfMode == AbstractWaveform.LoadFromWaveServer) { // from WaveServer
    
          System.err.println("TriggerAmps: Using TriggerChannelTimeWidowModel");
          // Make a WaveClient
          try {
            org.trinet.jiggle.WaveServerGroup waveClient = getProperties().getWaveServerGroup();
    
            if (waveClient == null || waveClient.numberOfServers() <= 0) {
              System.err.println(
                  "TriggerAmps: Error, no or bad waveservers in properties file. " );
              System.err.println("waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
              stop(0);
            }
    
            AbstractWaveform.setWaveDataSource(waveClient);  //'waveformReadMode' doesn't matter
    
            ctwModel = new TriggerChannelTimeWindowModel(); // define the CTW model ??
    
          } catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
          }
        } else {
          System.err.println("TriggerAmps: Error, you must define a legal value of 'waveformReadMode'.");
          stop(0);
        }
    
        if (debug) bm.printTimeStamp("TriggerAmps: Connections established,");
    
        // Load event info
        Solution sol = Solution.create().getById(evid);
        if (sol == null) {
           System.err.println("TriggerAmps: Error? unable to create Solution for evid, also check your database for valid event : "+evid);
           stop(0);
        }
        if (debug) {
            System.out.println(sol.toSummaryString());
        }
    
        ChannelableList chanList = null;

        // If cache file name is specified load channels
        String cacheFileName = ( getProperties() == null) ? 
            null : getProperties().getProperty("channelCacheFilename");
        if (cacheFileName != null && cacheFileName.trim().length() > 0) {
            cacheFileName = new java.io.File(propfilename).getParent() + GenericPropertyList.FILE_SEP + cacheFileName;
            System.out.println("TriggerAmps: Loading MasterChannelList from cache file : "  + cacheFileName);
            MasterChannelList.set(ChannelList.readFromCache(cacheFileName));
            // Don't load all active channels, instead revert to the named group channels !!!
            //chanList = MasterChannelList.get();
            //if (chanList == null | chanList.isEmpty()) MasterChannelList.smartLoad(sol.getDateTime());
            //chanList = MasterChannelList.get();
        }

        chanList = MasterChannelList.get();
        if (chanList == null || chanList.isEmpty()) { // no cache, so use named group candidate channel list
          Channel ch = Channel.create();
          // Speed up channel data loading by limiting what you want
          Channel.setLoadClippingAmps(false); // clipping levels not used by model or filter
          Channel.setLoadKnownCorrections(false); // !!!!! don't need station magnitude ?
          Channel.setLoadResponse(true);
    
          // Abandoned local ChannelTN_Quick class
          //ChannelableList chanList = ChannelTN_Quick.getChannelList("RCG-TRINET");
          System.out.println("TriggerAmps: Loading ChannelList from db for group name: "  + getChannelGroupName());
          chanList = ch.getNamedChannelList(getChannelGroupName());
          MasterChannelList.set(chanList);
        } 
        
        if (chanList == null || chanList.isEmpty()) {
            System.err.println("TriggerAmps: Channel list resulting from channelGroupName="+ getChannelGroupName()+" is empty.");
            stop(0);
        } else if (debug) {
            System.out.println("TriggerAmps: Channel list <"+getChannelGroupName()+ "> contains "+chanList.size()+" channels.");
        }
    
        // Setup time window model
        ctwModel.setProperties(props);
        ctwModel.setCandidateList(chanList);
        ctwModel.setSolution(sol); // do this after setting list, else it loads one from db, does distance sorts, too.


        // Override of above setup of time window model props (legacy config - deprecate this?)
        ctwModel.setMinWindowSize(minTimeWindow);
        ctwModel.setMaxWindowSize(maxTimeWindow);
        ctwModel.setPreEventSize(preEventTime);
        ctwModel.setMaxDistance(maxDistance) ;
    
        if (debug) bm.printTimeStamp("TriggerAmps: Channel time windows defined,");
    
        //TotalGroundMotionFilter tgmf = new TotalGroundMotionFilter();
        AccelFilter tgmf = new AccelFilter();
        tgmf.copyInputWaveform(false);
    
        // make CTW's
        ChannelableList wfList = (ChannelableList) ctwModel.getWaveformList();
    
        if (debug) bm.printTimeStamp("TriggerAmps: Model returned waveform list size:" + ((wfList == null) ? 0 : wfList.size()));
        // Group stations by Nt.Sta and sort by chn w/ HH_ 1st, then HL_ (or HN_)
        // Note: we're not distance sorted anymore! They're alphabetical.
        //StationGrouper stationGrouper = new StationGrouper(wfList);
        AmpList amplist = AmpList.create();
        ChannelableListIF sublist = null;
        AmpList smlist = null;
        Waveform wf = null;
        int knt = 0;
        Amplitude ap = null;
         
        //while (stationGrouper.hasMoreGroups()) {
    
          //sublist = stationGrouper.getNext();  // all comps of one sta
    
          //for ( int i=0; i < sublist.size(); i++) {
          for ( int i=0; i < wfList.size(); i++) {
    
            //wf = (Waveform) sublist.get(i);
            wf = (Waveform) wfList.get(i);
            if (wf.loadTimeSeries()) {
              //System.out.println("TriggerAmps: Processing: "+wf.getChannelObj().toDelimitedSeedNameString());
              //tgmf.filter(AbstractWaveform.copyWf(wf)); // I don't believe we need a copy here -aww 2010/11/16
              //tgmf.filter(wf); // create ground motions
              //smlist = tgmf.getPeaks();                // retrieve the 7 peaks
              //doThreshold(smlist, amplist);

              wf = tgmf.filter(wf); // create ground motions
              ap = wf.getPeakAmplitude();
              if (ap != null) {
                  ap.setType(AmpType.PGA);  // only when not doing the TGMF
                  if (Math.abs(ap.getValue().doubleValue()) >= minAcceleration) {  // threshold
                      amplist.add(ap);;
                  }
              }
              wf.unloadTimeSeries();
              knt++;
            } else {
              System.out.println("TriggerAmps: No timeseries for "+wf.getChannelObj().toDelimitedSeedNameString());
            }
            // grouper insures loop will always do HH_ before HL_, if they're all good skip HL_'s
    
          } // end of for loop
    
          // show progress every 100 channels processed
          if ( debug && ( knt % 100 ) == 0 ) bm.printTimeStamp("*Finishing "+ knt +" took");
        //} end of while
        bm.printTimeStamp("TriggerAmps: Calculations done, total wfs processed: " + knt);
        System.out.println("TriggerAmps: Calculated "+amplist.size()+" amps with ACC >= " + minAcceleration + " cmss");
         
        amplist.sortByTime();
        amplist.insureLatLonZinfo(new DateTime());
        org.trinet.util.gazetteer.LatLonZ llz = null;

        if (! sol.hasLatLonZ() && wfList.size() > 0) {
            Channel chan1 = ((Channelable)wfList.get(0)).getChannelObj();
            chan1 = chan1.lookUp(chan1, sol.getDateTime()); // lookup channel lat/lon info
            System.out.println("TriggerAmps: Setting event lat,lon to that of : " + chan1.toString());
            llz = chan1.getLatLonZ();
            sol.setLatLonZ(llz.getLat(), llz.getLon(), 0.1); //make depth=0.1 here - aww
        }
        amplist.calcDistances(sol);
    
        // set auth, source of all amps
        Amplitude amp[] = amplist.getArray();
        for ( int i=0; i < amp.length; i++) {
          amp[i].setAuthority(authString);
          amp[i].setSource(sourceString);
          amp[i].associate(sol);
          amp[i].magnitude = null;
        }
    
        // write results to dbase.
        if (knt <= 0 ) {
          System.out.println("TriggerAmps: No amps generated, check for invalid evid, missing waveforms");
        }
        else {
          knt = amplist.getStationCount();
          String msg = "TriggerAmps: Found " + knt +" stations with peak ACC >= " + minAcceleration + " cmss";
          System.out.println(msg);
          if (ampOutputDir.length() > 0  && knt > 0 ) {
            String filename = ampOutputDir + GenericPropertyList.FILE_SEP + sol.getId().longValue() + ".trigamps";
            System.out.println("Writing amp list to file: " + filename);
            try {
              File file = new File(filename);
              BufferedWriter bfw = new BufferedWriter(new FileWriter(file));
              bfw.write(msg);
              bfw.write("\n");
              bfw.write(Amplitude.getNeatStringHeader().substring(0,64));
              bfw.write(" Lat    Long");
              bfw.write("\n");
              for (int ii=0; ii<amp.length; ii++) {
                bfw.write(amp[ii].toNeatString().substring(0,64));
                llz = amp[ii].getChannelObj().getLatLonZ();
                bfw.write(String.format(" %6.3f %7.3f", llz.getLat(), llz.getLon()));
                bfw.write("\n");
              }
              bfw.flush();
              bfw.close();
            }
            catch (IOException ex) {
              ex.printStackTrace();
            }
          }
          System.out.println("NOTE: Event Lat,Lon set to the earliest triggered channel coordinates!");
          System.out.println(Solution.getNeatStringHeader());
          System.out.println(sol.toNeatString());
          System.out.println();
          System.out.println(Amplitude.getNeatStringHeader().substring(0,64) + " Lat    Long");
          for (int ii=0; ii<amp.length; ii++) {
              System.out.print(amp[ii].toNeatString().substring(0,64));
              llz = amp[ii].getChannelObj().getLatLonZ();
              System.out.print(String.format(" %6.3f %7.3f\n", llz.getLat(), llz.getLon()));
              System.out.println();
          }
        }
    
        stop(knt);
    }

    private static final void stop(int code) {
        if (ds != null) ds.close(); 
        System.out.println("================== TriggerAmps Stop @ " + new DateTime() + "==================");
        System.exit(code);
    }

}

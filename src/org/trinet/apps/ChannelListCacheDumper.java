package org.trinet.apps;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class ChannelListCacheDumper extends Object {
    public static void main(String args[]){

      if (args.length < 1) {
        System.out.println("Args:[user passwd host.domain dbname [cacheName default=channelList.cache, in work directory (user.dir)]");
        System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host   = args[2];
      String dbname = args[3];

      String cacheFile = (args.length > 4) ? args[4] : "channelList.cache";

      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;

      System.out.println("ChannelListCacheDumper: making db connection... ");
      DataSource ds = new DataSource (url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd);    // make connection
      System.out.println("ChannelListCacheDumper dataSource : " + ds.describeConnection());

      StringBuffer sb = new StringBuffer(256);
      sb.append(System.getProperty("user.dir", "."));
      sb.append(System.getProperty("file.separator"));
      sb.append(cacheFile);

      System.out.println("ChannelListCacheDumper reading data from cache file : "+sb.toString());
      ChannelList cl = ChannelList.readFromCache(sb.toString());

      if (cl == null) {
        System.out.println("ChannelListCacheDumper: input channelList null");
      }
      else if (cl.size() == 0) {
        System.out.println("ChannelListCacheDumper: input channelList size = 0");
      }
      else {
        sb.append("Dump");
        System.out.println("ChannelListCacheDumper: writing data to listing: "+sb.toString());
        cl.writeToFile(sb.toString());
      }
    }
}

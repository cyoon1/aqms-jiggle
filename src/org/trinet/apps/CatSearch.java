package org.trinet.apps;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.EventSelectionDialog;

//Author:       Doug Given
public class CatSearch extends JFrame {

    static boolean debug = true;

    public CatSearch() { }

    public static void main(String s[]) {

        JFrame frame = new JFrame("Cat Search");
        frame.addWindowListener(new WindowAdapter()
           {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });


        frame.setSize(200,30);        // this has no effect!
        frame.pack();
        frame.setVisible(true);

        System.out.println("Making connection...");
        DataSource init = TestDataSource.create();

        // Private properties
        EventSelectionProperties props = new EventSelectionProperties();
        // could use input s[] strings for these:
        props.setFiletype("jiggle"); // test file found in the user's home "jiggle" subdir
        props.setFilename("eventSelection.props"); // name of event selection props file
        props.reset(); // read the props from file
        props.setup(); // configure local environment from props

        if (debug) props.dumpProperties();

        EventSelectionDialog dialog =
                 new EventSelectionDialog(frame, "Event Selection", true, props);

        while (true) {
            dialog.setVisible(true);

            if (! (dialog.getButtonStatus() == JOptionPane.OK_OPTION)) break;

            props = dialog.getEventSelectionProperties();
            System.out.println ("-- Event Selection Properties AFTER --");
            props.dumpProperties();
            System.out.println ("-----------");
            System.out.println ("TimeSpan = "+ props.getTimeSpan().toString());

            int answer =
                JOptionPane.showConfirmDialog(null, "Property values ok?, press NO reset.", "Confirm Event Selection Properties",
                        JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (answer != JOptionPane.YES_OPTION) continue;

            dialog.saveDialog();

            SolutionList catList = new SolutionList(props);
            Solution sol[] = catList.getArray();

            if (sol.length == 0) System.out.println(" * No events found.");

            for (int i = 0; i < sol.length; i++) {
                System.out.println(sol[i].toSummaryString() +" "+ sol[i].source.toString() );
            }

        } // end while

        System.exit(0); // done with loop
    }
}

/** OUTPUT FILE FORMAT:
 For each fault line segment:
 a line declaring 2 CSV: the name of fault, the relative segment number
 1st line followed by one or more lines of CSV pairs of lat,lon ending with lat,lon pair = 0.,0.
 Repeat same pattern for each fault line segment until end of input file.
*/
package org.trinet.apps;

import java.io.*;
import java.sql.*;
import javax.sql.rowset.serial.SerialBlob;
import java.util.StringTokenizer;
import java.sql.Blob;
//import oracle.sql.BLOB;
//import oracle.jdbc.OracleStatement;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class DumpGazetteerLine {
            
    // fault code from gazetteertype table, could lookup value by query: "select code from gazetteertype where name='fault'";


    private static int type = 601;
    private static String format = "double";

    private static String segName = null;
    private static int segNumb = 1;

    private static Statement ps = null;
    //private static oracle.sql.BLOB sblob = null;
    private static java.sql.Blob sblob = null;

    private static final String SELECT_SQL = "select gazid,type,name,line,format,points from gazetteerline order by name,line"; 


    public static final void main(String[] args) {

        if (args.length != 6) {
            System.err.println("Usage: java DumpGazetteerLine dbhost dbname dbport user passwd faultfilename");
            System.exit(-1);
        }

        String dbhost = "";
        String dbname = "";
        String dbport = "";
        String dbuser = "";
        String dbpass = "";
        String filename = "";

        try {
          dbhost = args[0];
          dbname = args[1];
          dbport = args[2];
          dbuser = args[3];
          dbpass = args[4];
          filename = args[5];
        }
        catch ( Exception ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }

        // Create a DataSource connection to input db here
        String url = String.format("jdbc:%s:@%s:%s:%s", AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL, dbhost, dbport, dbname);
        DataSource ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, dbuser, dbpass);    // make connection

        ds.setWriteBackEnabled(true);

        System.out.println("DumpGazetteerLine db:\n " + ds.describeConnection());
        System.out.println(" output file = " + filename);

        if (ds.getConnection() == null) {
            System.err.println("DumpGazetteerLine Error: null DataSource db connection!");
            System.exit(0);
        }

        double lat = 0.;
        double lon = 0.;

        byte[] b = null;

        boolean status = true;

        BufferedWriter bwrt = null;
        ResultSet rs = null;

        try {

            bwrt = new BufferedWriter(new FileWriter(filename));

            // Create new gazetteerline table row
            if (ps == null) {
                ps = DataSource.getConnection().createStatement();
            }
                
            // "select gazid,type,name,line,format,points from gazetteerline order by name,line"; 
            rs = ps.executeQuery(SELECT_SQL);

            int gazid = 0;
            int idx = 0;
            while ( rs.next() ) {
                    gazid = rs.getInt(1);
                    type = rs.getInt(2);
                    segName = rs.getString(3);
                    segNumb = rs.getInt(4);
                    format = rs.getString(5);
                    //sblob = (BLOB) rs.getBlob(6);
                    sblob = (java.sql.Blob) rs.getBlob(6);
                    double[] latlon = getPoints(sblob); // parses binary serial stream 

                    // Now output line segment to file
                    bwrt.write(segName); // name,number
                    bwrt.write(","); // name
                    bwrt.write(String.valueOf(segNumb)); // number
                    bwrt.write("\n"); // newline

                    idx = 0;
                    while (idx<latlon.length) {
                      bwrt.write(String.format("%+10.6f",latlon[idx++])); // lat
                      if (idx != latlon.length) bwrt.write(",");
                      bwrt.write(String.format("%11.6f",latlon[idx++])); // lon 
                      if (idx != latlon.length) bwrt.write(",");
                      // 6 pairs per line
                      if (idx%12 == 0) {
                          bwrt.write("\n");
                      }
                    }
                    // newline after last pair
                    if (!(latlon.length%12 == 0)) bwrt.write("\n");
            }
        }
        catch (Exception ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (ds != null) ds.close();
            }
            catch (SQLException ex) {
            }

            try {
              if (bwrt != null) {
                  bwrt.flush();
                  bwrt.close();
              }
            }
            catch (IOException ex) { }
        }

        System.exit((status ? 0 : -1));
    }

    //private static final double[] getPoints(BLOB points) {
    private static final double[] getPoints(java.sql.Blob points) {

        long bytesToRead = 0;
        long bytesRead = 0;

        BufferedInputStream bis = null;
        
        try {
            bytesToRead = points.length();
            bis = new BufferedInputStream(points.getBinaryStream());
        }
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            return null;
        }

        int idx = 0;
        double [] dout = null;

        byte [] byteBuffer = new byte[(int) bytesToRead];

        try {
            while (bytesRead < bytesToRead) {

                int bytesInput = bis.read(byteBuffer);
                if (bytesInput == -1 ) break;

                bytesRead += bytesInput;

                DataInputStream dis = new DataInputStream(new ByteArrayInputStream(byteBuffer));

                int bytesDone = 0;
                int bytesPerPoint = 8; // getBytesPerPoint(lineData.format);
                int npts = (int) (bytesToRead/bytesPerPoint +2);
                dout = new double[npts];
                try {
                    while (bytesDone < bytesInput) {
                        dout[idx++] = dis.readDouble();
                        bytesDone += bytesPerPoint;
                    }
                }
                catch (EOFException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
                dis.close();
            }
            bis.close();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
        return dout;
    }

} // end of DumpGazetteerLine class

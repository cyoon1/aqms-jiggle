package org.trinet.apps;

import java.text.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.jiggle.*;
import org.trinet.util.TimeSpan;
import org.trinet.util.WaveClient;

/**
 * Make a FreeSnap of the first 'n' channels with phase picks. <p>
 *
 *  Usage: FreeSnap [evid] [ntraces] [max_secs] [waveServer-file]
 *
 */
public class FreeSnap extends org.trinet.web.SnapShot {

    public static void main(String args[]) {

        if (args.length < 4) {
            System.out.println("Usage: FreeSnap <evid> <waveServer-file> <dbase-host> <dbase-name>");
            System.exit(0);
        }

        // event ID
        long evid = Long.parseLong(args[0]);
        if (debug) System.out.println("Using evid "+ evid);

        // get data from waveServer?
        waveServerFile = args[1];
        WaveClient waveClient = null;
        try { // Make a WaveClient
          System.out.println("Creating WaveClient using: "+waveServerFile);
          waveClient = WaveClient.createWaveClient().configureWaveClient(waveServerFile); // property file name

          int nservers = waveClient.numberOfServers();
          if (nservers <= 0) {
            System.err.println("getDataFromWaveServer Error:"+ " no data servers specified in input file: " +
                 waveServerFile);
            System.exit(-1);
          }
        }
        catch (Exception ex) {
          System.err.println(ex.toString());
          ex.printStackTrace();
        }


        if (waveClient == null) {
          System.err.println("Fatal Error: wave client is null");
          System.exit(-1);
        }

        String dbasehost = args[2];

        String dbasename = args[3];

        System.out.println("db host:name = "+ dbasehost + ":" + dbasename );

        // Note TestDataSource logic machine name and dataname - does it work here?
        System.out.println("Making connection...");
        DataSource init = TestDataSource.create(dbasehost,dbasename);  // make connection
        init.setWriteBackEnabled(true);

      // //////////////////////////////////////////////////////////////
      AbstractWaveform.setWaveDataSource(waveClient);

      FreeSnap freeSnap =  new FreeSnap();
      Component view = freeSnap.makeView(evid);

      if (view == null) System.exit(0);

      // make a frame

      JFrame frame = new JFrame("FreeSnap of "+evid);

      frame.addWindowListener(new WindowAdapter() {
          public void windowClosing(WindowEvent e) {System.exit(0);}
      });

      // scroller
      JScrollPane scroller = new JScrollPane();
      scroller.add(view);
      frame.getContentPane().add( scroller );    // add scroller to frame

      frame.pack();
      frame.setVisible(true);

      int height = 900;
      int width  = 640;
      frame.setSize(width, height); // must be done AFTER setVisible

      //System.exit(0);

    }

    public Component makeView(long evid) {
      // Load the Solution
      Solution sol = Solution.create().getById(evid);
      if (sol == null) {
          System.out.println("No such event: "+evid);
          return null;
      }

      System.out.println(sol.toString());

      System.out.println("Reading in current channel info...");
            String compList[] = {"EH_", "HH_", "HL_", "AS_"};
          ChannelList chanList = ChannelList.getByComponent(compList);

      MasterChannelList.set(chanList);  // remember the list for later
      //ChannelList chanList = ChannelList.smartLoad();
      System.out.println("Read "+chanList.size()+" channels.");

      SimpleChannelTimeModel model = new SimpleChannelTimeModel(sol, chanList);

      //ArrayList wfList = (ArrayList) model.getWaveformList();

      // test loading an event from this model
      MasterView mv = new MasterView();


      //mv.setWaveformLoadMode(MasterView.LoadAllInForeground);

      // this loads readings, sorts and loads waveforms
      mv.setWaveformLoadMode(MasterView.LoadNone);
      mv.defineByChannelTimeWindowModel(model);

      System.out.println("mv has "+mv.wfvList.size()+" views.");

      mv.wfvList.trim(30);

      System.out.println("mv has "+mv.wfvList.size()+" views.");

      // now read waveforms
      mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
      mv.loadWaveforms();

      return makeView(mv);

    }

} // end of class

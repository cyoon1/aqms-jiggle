package org.trinet.apps;

import java.awt.event.*;

import javax.swing.*;
import java.awt.Dimension;
import org.trinet.jiggle.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.util.*;
import org.trinet.formats.*;
import org.trinet.web.*;
/**
 * <p>Create a waveform snapshot in gif format for a particular event showing
 * all the channels in a preselected list of channels. </p>
 */

public class SnapShotFromList extends org.trinet.web.SnapGif {

  private static int height = 900;
  private static int width  = 640;
  private static boolean distanceSort = true;
  private static boolean showOnScreen = false;
  private static boolean makeGif = true;
  private static boolean showPhases = true;

  public static int maxAllowedChannels = 100; // default to 100 channels

  private static DbaseConnectionDescription dbd = null;

  public SnapShotFromList() { } 

  private static boolean setPropertiesFile(String propertiesFile) {

    // assumes path/filename NOT that file is in %userhome%/.jiggle
    setProperties(new MyPropertyList(propertiesFile));

    if (getProperties() == null) return false;

    // set behavior values
    if (getProperties().isSpecified("maxAllowedChannels")) maxAllowedChannels = getProperties().getInt("maxAllowedChannels", 100);
    if (getProperties().isSpecified("frameHeight"))    height = getProperties().getInt("frameHeight");
    if (getProperties().isSpecified("frameWidth"))     width = getProperties().getInt("frameWidth");
    if (getProperties().isSpecified("distanceSort"))   distanceSort = getProperties().getBoolean("distanceSort");
    if (getProperties().isSpecified("showOnScreen"))   showOnScreen = getProperties().getBoolean("showOnScreen");
    if (getProperties().isSpecified("makeGif"))        makeGif = getProperties().getBoolean("makeGif");
    if (getProperties().isSpecified("showPhases"))     showPhases = getProperties().getBoolean("showPhases");
    // NOTE: output defaults to the value of channel time window model property "maxWindowSize" only if windowSecond, secsPerPage = 0
    // DEPRECATED name, instead use secsPerPage property of parent
    if (getProperties().isSpecified("windowSeconds"))  setMaxSeconds( (int) getProperties().getDouble("windowSeconds") );

    // Database
    dbd = getProperties().getDbaseDescription();
    if (!dbd.isValid()) {
      System.out.println("ERROR: database specification is invalid.");
      return false;
    }

    // sanity checks
    if (!getProperties().isSpecified("currentWaveServerGroup") ||
        !getProperties().isSpecified("waveServerGroupList") ) {
      System.out.println("ERROR: waveserver specification is incomplete.");
      return false;
    }

    if (!showOnScreen && !makeGif) {
      System.out.println("WARNING: no output will be created:");
      System.out.println("         both showOnScreen and makeGif properties are set false.");
    }
    if (getProperties().isSpecified("NSCLfile") && getProperties().isSpecified("adhocFile")) {
      System.out.println("WARNING: both NCSLfile and adhocFile are specified,");
      System.out.println("         ONLY NCSLfile will be used.");
    }

    return true;
  }

  public static void main(String[] args) {

    if (args.length < 1) {
      System.out.println ("Usage: SnapShotFromList <evid> <properties-file> [ output-file ]");
      System.out.println("               defaults:           "+propertyFileName+"      <evid>.gif");
      System.exit(1);
    }

    // event ID
    long evid = Long.parseLong(args[0]);
    //if (debug) 
        System.out.println ("Processing evid "+ evid);

    // props file
    if (args.length > 1) {
      propertyFileName = args[1];
    }

    // optional output file
    String gifFile = System.getProperty("user.dir") + GenericPropertyList.FILE_SEP + evid + ".gif";  //default
    if (args.length > 2) {
        gifFile = args[2];
    }

    // read properties
    if (! setPropertiesFile(propertyFileName)) {
      System.err.println("ERROR: bad/missing properties in file: "+propertyFileName) ;
      System.exit(2);
    }

    // create dbase connection
    //if (debug) {
      System.out.println ("Making db connection...");
      System.out.println( dbd.toString());
    //}

    //DataSource dataSource = new DataSource(dbd);  // make connection
    DataSource dataSource = new DataSource();  // make connection
    dataSource.set(dbd); // -aww 2008/06/26
    if (dataSource == null) {
          System.err.println ("ERROR: db connection failed");
          System.err.println( dbd.toString());
          System.exit(3);
    }

    // Get and setup the wave client
    setupWaveClient();

    // read in the event infor
    Solution sol = Solution.create().getById(evid);

    if (sol == null || sol.isDummy()) {
      System.err.println ("ERROR: input event id not found in db: "+evid);
      System.exit(4);
    }
    //if (debug)
        System.out.println("Event: " + sol.toNeatString());

    if (showPhases) {
      sol.loadPhaseList();
    }

    ChannelList clist =  ((MyPropertyList)getProperties()).getChannelList();
    //clist.insureLatLonZinfo(sol.getDateTime()); // takes long time to read from db

    boolean hasChannelList = true;
    if (clist == null) {
        System.err.println ("WARNING: channel list is null: check NSCLFile/adHocFile properties, do files exist?");
        // No fatal exit here, instead allow user to use an appchannels list declared by model property candidateListName,
        // however it must not be the default: RCG-TRINET, hopefully something like "Synopsis or MakeGif"
        hasChannelList=false;
    }
    System.out.println("candidate list size: " + clist.size());
    // read the channel list
    NamedChannelTimeWindowModel model = new NamedChannelTimeWindowModel(getProperties(), sol, clist);
    if ( ! hasChannelList && model.getCandidateListName().equals("RCG-TRINET")) {
        System.err.println ("ERROR: channel list is null: NamedChannelTimeWindowModel.candidateListName undefined (e.g. MakeGif or Synopsis)");
        System.exit(5);
    }

    ChannelableList ctwList = model.getChannelTimeWindowList();
    if ( ctwList.size() > maxAllowedChannels) { // sanity check for list, is too much memory needed for timeseries?
        System.err.println ("ERROR: channel list size exceeds allowed maximum: " + maxAllowedChannels);
        System.exit(6);
    }

    // NOTE: override if > 0, else use value of the channel time window model property "maxWindowSize"
    //if (getProperties().isSpecified("windowSeconds") || getProperties().isSpecified("secsPerPage")) {
    //  if (getMaxSeconds() > 0.) model.setMaxWindowSize(getMaxSeconds());  // set window size in sec using property -aww
    //}

    // Load master channnel list from cache (would be used by MasterView instead in doing a db lookup of channel LLZ data)
    if (getProperties().getBoolean("channelListCacheRead") ) {
        loadChannelList(sol.getDateTime());
    }
    else Channel.setLoadOnlyLatLonZ();

    MasterView mv = new MasterView();
    mv.phaseRetrieveFlag =getProperties().getBoolean("masterViewRetrievePhases", true);
    mv.ampRetrieveFlag = getProperties().getBoolean("masterViewRetrieveAmps", true);
    mv.codaRetrieveFlag = getProperties().getBoolean("masterViewRetrieveCodas", true);
    mv.loadSpectralAmps = getProperties().getBoolean("masterViewLoadSpectralAmps", false);
    mv.loadPeakGroundAmps = getProperties().getBoolean("masterViewLoadPeakGroundAmps", false);
    mv.loadPrefMags = false;
    int mode = MasterView.AlignOnTime;
    if (getProperties().isSpecified("masterViewWaveformAlignMode")) mode = getProperties().getInt("masterViewWaveformAlignMode");
    mv.setAlignmentMode(mode);
    if (getProperties().isSpecified("masterViewWaveformAlignVel")) mv.setAlignmentVelocity(getProperties().getDouble("masterViewWaveformAlignVel"));

    mv.addSolution(sol, false);
    mv.setSelectedSolution(sol);
    // This controls when frames are painted red to indicate bad clock quality, set 0.
    mv.setClockQualityThreshold(getProperties().getDouble("clockQualityThreshold", 0.0));

    mv.setWaveformLoadMode(MasterView.LoadNone);  // don't load wfs yet
    mv.defineByChannelTimeWindowList(ctwList);    // this call loads waveforms UNLESS set not to above!
    //
    if (getMaxSeconds() > 0.) {
        TimeSpan ts = mv.getViewSpan();
        ts.setEnd(ts.getStart()+getMaxSeconds());  // set window size in sec using property -aww
        mv.alignViews();
    }
    //

    if (distanceSort) mv.distanceSort();

    // now, load the waveforms
    mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
    System.out.println ("Loading mv wfs...");
    mv.loadWaveforms();

    // Make the snapshot
    org.trinet.web.SnapShot snapShot =  new org.trinet.web.SnapShot();
    JPanel view = snapShot.makeView(mv);

    int count = 0;
    int total = 0;
    StringBuilder sb = null;
    if (mv.wfvList != null) {
        WFView wfv [] =  mv.wfvList.getArray();
        sb = new StringBuilder(132*wfv.length+132);
        AbstractWaveform wf = null;
        count = 0;
        total = wfv.length; 
        for (int i=0; i < total; i++) {
            wf = (AbstractWaveform) wfv[i].getWaveform();
            if (wf != null && wf.hasTimeSeries()) {
                //sb.append("+").append(wf.getChannelObj().toDelimitedSeedNameString()).append("\n");
                count++;
            }
            else {
                sb.append("-").append(wf.getChannelObj().toDelimitedSeedNameString()).append("\n");
            }
        }
    }
    System.out.println(sb.toString());
    System.out.printf("evid=%d wf loaded/total = %d/%d = %d%% at %s\n",
                    evid, count, total, Math.round(count*100./total), new DateTime().toString());
    // sizing of view seems NOT TO WORK!?   Gif is always 640x1005
    // REMOVED here since encodeImageFile does this -aww
    //view.setPreferredSize(new Dimension(width, height));
    //view.setMinimumSize(new Dimension(width, height));
    //view.validate();

    // Make the .gif
    if (makeGif) {
      //encodeGifFile(view, gifFile);
      encodeImageFile(view, gifFile);
            Runtime runtime = Runtime.getRuntime();
            double mmem = ((double) runtime.maxMemory())/1000000.;
            double tmem = ((double) runtime.totalMemory())/1000000.;
            double fmem  = ((double) runtime.freeMemory())/1000000.;
            double umem = ((tmem - fmem)/tmem) * 100.0;
            System.out.printf("JVM Memory usage: max=%6.1f total=%6.1f free=%6.1f %%used=%6.1f\n", mmem,tmem,fmem,umem);
    }

    if (showOnScreen) { // make a frame
      JFrame frame = new JFrame("SnapShot of "+evid);

      frame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e, DataSource dataSource) {
          try {
            dataSource.getConnection().close();
          } catch (Exception ex) {
            ex.printStackTrace();
          }
          System.exit(0);
        }
      });

      view.setMinimumSize(new Dimension(width, height)); // ? is this needed ?

      frame.getContentPane().add( view ); // add scroller to frame

      frame.pack();
      frame.setVisible(true);
      frame.setSize(width, height); // must be done AFTER setVisible

    } else {

      try {
        dataSource.getConnection().close();
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      System.exit(0);
    }
  } // end of main

    /*
     * Load the Channel list. This is done one time at program startup.
     * The list is used to lookup lat/lon for new data as it is read in.
    */
    static boolean loadChannelList(java.util.Date firstSolDate) {

        boolean status = true;

        ChannelList cList = MasterChannelList.get();
        String dsStr = "Reading Channels from data source:<p> "+DataSource.getDbaseName();

        //DEBUG: here disable loading of associated channel data like corrections if db tables are missing
        Channel.setLoadOnlyLatLonZ();

        //boolean readCache = getProperties().getBoolean("cacheChannelList"); // removed aww 02/03/2005
        boolean readCache = getProperties().getBoolean("channelListCacheRead"); // added aww 02/03/2005
        String cacheFileName = getProperties().getUserFileNameFromProperty("channelCacheFilename");

        String str = null;
        if (readCache) str = "Reading Channels from file cache "+cacheFileName;
        else str = dsStr;
        if (getProperties().getBoolean("verbose")) System.out.println (str);

        // read Channel list from cache using the filename in input properties
        // uses default location if cacheFileName is null:
        String chanGroupName = getProperties().getChannelGroupName(); // is there one?
        String dbLoadStr = ">>> Load of " +
            ( (firstSolDate == null) ? "CURRENT" : firstSolDate.toString() ) +
            " Channels associated chanGroupName = " + chanGroupName +
            " from : " + DataSource.getDbaseName() +
            " might take minutes ...";


        if (readCache) {

          java.io.File file = new java.io.File(cacheFileName);
          if (file.exists()) {
              if (debug) System.out.println("Cache file exists, reading from file");
          }

          MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

          // cache read failed, read from dbase and write to cache for future use
          // uses default location if cacheFileName is null:
          if (MasterChannelList.isEmpty()) {
            String failStr = "Cache read failed.";
            System.out.println(failStr);
            System.out.println(dbLoadStr);
            if (chanGroupName == null) MasterChannelList.set(ChannelList.readList(firstSolDate));
            else MasterChannelList.set(ChannelList.readListByName(chanGroupName, firstSolDate));
           
            //db read succeeded -- write this "current" list to cache file in background
            if (MasterChannelList.get() != null && !MasterChannelList.get().isEmpty()) {
                status = MasterChannelList.get().writeToCacheInBackground(); // write cache file for future use
            }
            else {
                status = false;
                System.out.println("WARNING! Jiggle loadChannelList, empty list, check database channel data or channelGroupName.");
            }
          }
        } else { // don't read cache file, instead read "current" list and write it to cache.
          System.out.println(dbLoadStr);
          if (chanGroupName == null) MasterChannelList.set(ChannelList.readList(firstSolDate));
          else {
            MasterChannelList.set(ChannelList.readListByName(chanGroupName, firstSolDate));
          }
          if (MasterChannelList.get() != null && !MasterChannelList.get().isEmpty()) {
              status = MasterChannelList.get().writeToCacheInBackground(); // write cache file for future use
          }
          else {
              System.out.println("WARNING! loadChannelList, empty list, check database channel data or channelGroupName.");
              status = false;
          }
        }

        // create HashMap of channels in MasterChannelList, if not already one
        if (! MasterChannelList.get().hasLookupMap()) {
            MasterChannelList.get().createLookupMap();
        }
        if (getProperties().getBoolean("verbose")) System.out.println(">>> Finished loading of MasterChannelList, total channels: " + MasterChannelList.get().size());

        return status;
    }
}

// ////////////////////////////////////////////
/*
 * Use JiggleProperties for dbase, waveservers and some plot behavior.
 * Must extend it for some other parameters.
*/
class MyPropertyList extends JiggleProperties {

  private ChannelList chanList = null;

  /**
   *  Constructor: sets defaults and reads the property file. Filename must
   * be complete, no assumptions are made as to what directories to search. If the
   * properties file is invalid the application will continue with defaults.
   */
  public MyPropertyList(String fileName) {
    super();
    setUserPropertiesFileName(fileName);
    reset();    // reads all the property files
    setup();
  }

  /**
   * Set required default properties so things will still work even if they are not set
   * in the properties files or the files are not present. This must be overridden by
   * subclasses to set their private essential property values.
   */
  public void setRequiredProperties() { }

  public boolean setup() {
    super.setup();   // set all standard JiggleProperties stuff
    // custom stuff
    if (isSpecified("NSCLfile")) {
      return setNSCLfile(getProperty("NSCLfile"));
    } else if (isSpecified("adhocFile")) {
      return setAdHocFile(getProperty("adhocFile"));
    } else {
      System.err.println("WARNING: no NSCLfile or adhocFile property in properties file.");
      return false;
    }
  }

  /** Set name of and read in the optional Ad Hoc station file.
   * This remembers the modification date of the file. On subsequent calls to this method
   * the file will be REREAD if the current mod-date for the file does not match the previous
   * one. Thus, later calls to this method will refresh the list from the modified file if it changed.*/
  private boolean setAdHocFile(String filename) {

    // actually read in the file if 1st time or it has been modified since last read
    try {
      System.out.println("Reading in adhoc file: " + filename);
      setChannelList( AdHocFormat.readInFile(filename) );  // read the list
    }
    catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("ERROR: No such file or no read access: "+ filename);
      return false;
    }
    return true;
  }

  /** Set name of and read in the optional Ad Hoc station file.
   * This remembers the modification date of the file. On subsequent calls to this method
   * the file will be REREAD if the current mod-date for the file does not match the previous
   * one. Thus, later calls to this method will refresh the list from the modified file if it changed.*/
  private boolean setNSCLfile(String filename) {
    // actually read in the file if 1st time or it has been modified since last read
    try {
      System.out.println("Reading in SNCL file: " + filename);
      setChannelList( NSCLformat.readInFile(filename) );  // read the list
    }
    catch (Exception ex) {
      ex.printStackTrace(System.err);
      System.err.println("ERROR: No such file or no read access: "+ filename);
      return false;
    }
    return true;
  }

  /** Return a ChannelList built from the file */
  protected ChannelList getChannelList() {
    return chanList;
  }
  /** Set ChannelList to use*/
  protected void setChannelList(ChannelList list) {
    chanList = list;
  }

}

// TODO: Rework property/prefs code logic, subclass via factory create() model
//       allow "network" subclasses to define system defaults
// TODO: create subnodes for default panel preferences under application prefs.
// TODO: To avoid excessive memory use want to "clear" Solution lists
// upon a "ClearData" button activation which would clear the data lists
// in the Solutions not needing commit.
package org.trinet.mung;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
//import java.util.prefs.*; // removed 02/16/2005 -aww
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
import org.trinet.jiggle.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.table.*;
public class Mung {
    private static boolean  debug = false;
    public static final String  APPLICATION_NAME = "mung";
    public static final String  VERSION_ID = Version.buildTag;
    public static final String  DEFAULT_PROP_FILE_NAME = "mung.props";
    // Eliminate ":@" at end of string below and append if needed, building URL
    public static final String  DEFAULT_DS_PROTOCOL = "jdbc:" + org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL;
    public static final String  DEFAULT_DS_DRIVER   = org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER;

    public static final boolean DEFAULT_COMMIT_ENABLED =  true;

    public static final int DEFAULT_FRAME_X = 0;
    public static final int DEFAULT_FRAME_Y = 0;
    public static final int DEFAULT_FRAME_WIDTH = 1010;
    public static final int DEFAULT_FRAME_HEIGHT = 750;

    //private static GenericApplicationPrefs myPrefs = null;  // removed 02/16/2005 -aww
    private static MungPropertyList editorProps;
    private static double eventStartTime; // epoch seconds
    private static double eventEndTime;   // epoch seconds
    private JFrame aFrame = null;
    private JasiSolutionEditorPanel aJSEP = null;

    private String dbURLString = null;
    private String dbUserText = null;

    private boolean closeConnection = false;

    public Mung() {
      //myPrefs = new GenericApplicationPrefs(this); // removed 02/16/2005 -aww
    }

    /** Run Mung application gui. */
    public static final void main(String args[]) {

        Mung aMung = new Mung();
        // aMung.saveSystemPrefs(); // removed 02/16/2005 -aww

        int nargs = args.length;
        if (nargs > 0 && (args[0].equals("?") || args[0].equalsIgnoreCase("help"))) {
          System.out.println("Input args: [catalog_hrs_b4_present] [enableCommit (true)] [debug (false)]" );
          System.out.println("where hrs = 0, default to zero epoch seconds start, end times, empty catalog");
          System.out.println("      hrs < 0, forces use of event properties file values, if any");
          System.out.println("      hrs > 0, time range from the current time back to the hrsBack input value");
          System.exit(0);
        }

        if (nargs > 2) {
          // if enabled before redirect, debug output goes to stdout console
          // if enabled after redirect, debug output goes to textArea
          Debug.enable();
        }

        boolean enableCommit = (nargs > 1) ?
            Boolean.valueOf(args[1]).booleanValue() : DEFAULT_COMMIT_ENABLED;
            // aMung.myPrefs.getBoolean("defaultCommitEnabled", DEFAULT_COMMIT_ENABLED); // removed 02/16/2005 -aww

        aMung.initProperties();

        if ( aMung.createConnection(enableCommit) )
          System.out.println ("Mung getting data from connection...");
        else {
          // aMung.logPreferences();  // view default settings
          InfoDialog.informUser(null, "ERROR",
              "<html> Unable to make database connection.<p> User input incorrect or database is unavailable.<p>"+
              "Mung version: "+VERSION_ID+"</html>");
          System.exit(0);
        }
        // retrieve events preceeding current time in epoch seconds (seconds before Jan 1, 1970 are negative)
        //double endTime = aMung.myPrefs.getDouble("defaultEventEndTime", (double) (System.currentTimeMillis()/1000l));
        //double startTime = aMung.myPrefs.getDouble("defaultEventStartTime", endTime); // 86400l is 24 hrs
        double endTime = new DateTime().getTrueSeconds(); // for UTC time - aww 2008/02/08
        double startTime = endTime; // 86400l is 24 hrs
        // Below code overrides above time range settings if input arg hrsBack is non-zero
        // hrsBack = 0, default to zero epoch seconds start, end
        // hrsBack < 0, forces use eventprops file values, if any
        // hrsBack > 0, calculate time range limit from current time and the hrsBack input value
        if (nargs > 0 ) {
          try {
            double hrsBack = Double.parseDouble(args[0]);
            if (hrsBack == 0) {
              startTime = 0.;
              endTime   = 0.;
            }
            if (hrsBack > 0) {
              endTime   = new DateTime().getTrueSeconds(); // for UTC time - aww 2008/02/08
              startTime = endTime - (3600. * hrsBack);
            }
            // negative case uses event properties file settings for date range chooser
            else if (hrsBack < 0) {
              startTime = endTime; // use as flag?
            }
          }
          catch (NumberFormatException ex) {
            System.err.println("ERROR: on Mung startup parsing input arg as decimal hours: " + args[0]);
            if (DataSource.getConnection() != null) DataSource.close();
            System.exit(0);
          }
        }

        aMung.createEditor(startTime, endTime, enableCommit);

    } // end of main

    // redirect System.out, System.err output to textArea
    private void redirectSystemOutput(final JTextArea jta) {
        if (jta == null) {
           System.err.println("WARNING: Mung cannot redirect output, textArea null.");
           return;
        }
        PrintStream textAreaPrintStream = new PrintStream(
            new ByteArrayOutputStream(1024) {
                public void write(int charByte) {
                  super.write(charByte);
                  jta.append(this.toString());
                }
                public void write(byte [] b, int off, int len) {
                  super.write(b, off, len);
                  jta.append(this.toString());
                  this.reset();
                }
                public void write(byte [] b) {
                    super.write(b, 0, b.length);
                    jta.append(this.toString());
                    this.reset();
                }
            }
        );
        System.out.println("Mung now resetting system output streams");
        System.setErr(textAreaPrintStream);
        System.setOut(textAreaPrintStream);
        System.out.println("Mung text output now redirected to textArea");
    }
    private JasiSolutionEditorPanel configureDefaultPanel(boolean enableCommit) {
        JasiSolutionEditorPanel aJSEP = new JasiSolutionEditorPanel();
        JTextArea jta = new JTextArea(10,132);
        jta.setFont(new Font("Monospaced", Font.PLAIN, 12));
        jta.setEditable(true); // why not make default false?
        aJSEP.setTextLogger(new TextLogger(jta, true));
        // should we use Mung child node prefs to set attributes below:
        aJSEP.setPanelNameLabel("SOLUTION EDITOR");
        aJSEP.setUpdateEnabled(true);
        aJSEP.setCommitEnabled(enableCommit);
        aJSEP.setFinalize(true); // what function should this have
        aJSEP.setAutoSolve(true); // auto resolve summary solution, mag upon changing data
        aJSEP.setCommitButtonEnabled(enableCommit);
        aJSEP.setNumberFieldIdInputEnabled(true);
        aJSEP.setProperties(editorProps);
        //aJSEP.setPreferencesParent(myPrefs);  // remove 02/16/2005 -aww
        // allow waveform generation new mag of same type as prefmag here:
        // should enable button via editorProps input property instead 
        aJSEP.setWaveMagButtonEnabled(editorProps.getBoolean("waveMagEnabled")); // do b4 initPanel 05/2004
        aJSEP.initPanel(); // TextLogger textArea must exist prior to init
        return aJSEP;
    }
    private boolean createConnection(boolean enableCommit) {
        DbaseConnectionDescription dbDesc = editorProps.getDbaseDescription();
        dbURLString = dbDesc.getURL();
        System.out.println("Properties Db URL: " + dbURLString);

        DataSourceDialog dataSourceDialog =
          new DataSourceDialog(null, "Database Connection Info Mung "+VERSION_ID, 16);
        dataSourceDialog.setModal(true);
        dataSourceDialog.setLocation(400,350);
        dataSourceDialog.setSize(300,225);

        dataSourceDialog.setHost(dbDesc.getHostName());
        dataSourceDialog.setDomain(dbDesc.getDomain());
        dataSourceDialog.setDbaseName(dbDesc.getDbaseName());
        dataSourceDialog.setSchema(dbDesc.getUserName());
        dataSourceDialog.setPassword(dbDesc.getPassword());

        dataSourceDialog.show();
        if (! dataSourceDialog.okPressed()) return false;

        String host = dataSourceDialog.getHost();  // host name
        dbDesc.setHostName(host);

        String domain = dataSourceDialog.getDomain();  // IP domain name
        dbDesc.setDomain(domain);

        String dbName = dataSourceDialog.getDbaseName();  // database name
        dbDesc.setDbaseName(dbName);

        dbUserText = dataSourceDialog.getSchema(); // user account name (schema)
        dbDesc.setUserName(dbUserText);

        String dbPasswordText = dataSourceDialog.getPassword();
        dbDesc.setPassword(dbPasswordText);

        /*
        if (host.equals("")) { // get last saved user connection
          //dbURLString = myPrefs.getString("lastLoginDbURL", "");
          dbURLString = editorProps.getProperty("dbLastLoginURL", "");
        }
        else {
          if (host.indexOf(".") > 0) {
            host = ":@" + host;
          }
          else {
            String node = host;
            StringBuffer sb = new StringBuffer(512);
            sb.append(host);
            sb.append(".");
            //sb.append(myPrefs.getString("defaultDataSourceDomain", DEFAULT_DS_DOMAIN)); // 02/16/2005 -aww
            sb.append(editorProps.getProperty("dbaseDomain", ""));
            if (sb.toString().indexOf(":") < 1)  { // aww 02/24/2005 1.4 issue
              sb.append(":");
              //sb.append(myPrefs.getString("defaultDataSourcePort", DEFAULT_DS_PORT)); // 02/16/2005 -aww
              sb.append(editorProps.getProperty("dbasePort", "1521"));
              sb.append(":");
              //sb.append(myPrefs.getString("defaultDataSourceName", node+DEFAULT_DS_NAME)); // 02/16/2005 -aww
              sb.append(editorProps.getProperty("dbaseName", node+DEFAULT_DS_NAME));
            }
            host = ":@" + sb.toString();
          }
          //dbURLString = myPrefs.getString("defaultDataSourceProtocol", DEFAULT_DS_PROTOCOL) + host; // 02/16/2005 -aww
          dbURLString = editorProps.getProperty("dbaseProtocol", DEFAULT_DS_PROTOCOL) + host;
        }
        if (debug) System.out.println("DEBUG Mung dbURLSTRING=\""+dbURLString+"\"");

        //String dbDriverText   = myPrefs.getString("defaultDataSourceDriver", DEFAULT_DS_DRIVER); // 02/16/2005 -aww
        String dbDriverText   = editorProps.getProperty("dbaseDriver", DEFAULT_DS_DRIVER);
        */


        //DataSource ds = new DataSource(dbURLString, dbDriverText, dbUserText, dbPasswordText);
        DataSource ds = new DataSource();
        ds.set(dbDesc); // replaced above 2009/03/24 -aww
        ds.setWriteBackEnabled(enableCommit);

        dbURLString = dbDesc.getURL();
        System.out.println("Login Db URL: " + dbURLString);

        closeConnection = (ds.getConnection() != null);
        return closeConnection;
    }

    /*
    public void logPreferences() {
       myPrefs.logAllPreferences();
    }
    private boolean saveSystemPrefs() {
      Preferences prefs = myPrefs.getSystemPreferences();
      try {
        prefs.clear(); // ??
      } catch (BackingStoreException ex) {
          System.err.println("ERROR: saving Mung preferences: backing store unavailable");
          return false;
      }
      prefs.put("saveDate", new Date().toString());
      prefs.put("versionId", VERSION_ID);
      prefs.put("applicationName", APPLICATION_NAME);
      prefs.put("globalPropertyFileName", DEFAULT_PROP_FILE_NAME);
      prefs.put("defaultNetCode", DEFAULT_NET_CODE);
      prefs.put("defaultDsProtocol", DEFAULT_DS_PROTOCOL);
      prefs.put("defaultDsDriver", DEFAULT_DS_DRIVER);
      prefs.put("defaultDsDomain", DEFAULT_DS_DOMAIN);
      prefs.put("defaultDsPort", DEFAULT_DS_PORT);
      prefs.put("defaultDsName", DEFAULT_DS_NAME);
      prefs.putBoolean("defaultCommitEnabled", DEFAULT_COMMIT_ENABLED);
      prefs.putInt("defaultFrameX", DEFAULT_FRAME_X);
      prefs.putInt("defaultFrameY", DEFAULT_FRAME_Y);
      prefs.putInt("defaultFrameWidth", DEFAULT_FRAME_WIDTH);
      prefs.putInt("defaultFrameHeight", DEFAULT_FRAME_HEIGHT);
      return myPrefs.saveSystemPrefs();
    }
    public boolean saveUserPrefs() {
      Preferences prefs = myPrefs.getUserPreferences(); // .clear(); // ??
      prefs.put("saveDate", new Date().toString());
      prefs.put("versionId", VERSION_ID);
      prefs.put("applicationName", APPLICATION_NAME);
      prefs.put("globalPropertyFileName", DEFAULT_PROP_FILE_NAME);
      //prefs.put("defaultNetCode", DEFAULT_NET_CODE);
      prefs.put("defaultNetCode", EnvironmentInfo.getNetworkCode());
      prefs.put("lastLoginDbURL", dbURLString);
      //don't save class time values since other JSEP my have changed time pref/properties
      //prefs.putDouble("defaultEventStartTime", eventStartTime);
      //prefs.putDouble("defaultEventEndTime", eventEndTime);
      prefs.put("defaultDsProtocol", DEFAULT_DS_PROTOCOL);
      prefs.put("defaultDsDriver", DEFAULT_DS_DRIVER);
      prefs.put("defaultDsDomain", DEFAULT_DS_DOMAIN);
      prefs.put("defaultDsPort", DEFAULT_DS_PORT);
      prefs.put("defaultDsName", DEFAULT_DS_NAME);
      prefs.putBoolean("defaultCommitEnabled", DEFAULT_COMMIT_ENABLED);
      Rectangle bnds = aFrame.getBounds();
      prefs.putInt("defaultFrameX", (int) bnds.getX());
      prefs.putInt("defaultFrameY", (int) bnds.getY());
      prefs.putInt("defaultFrameWidth", (int) bnds.getWidth());
      prefs.putInt("defaultFrameHeight", (int) bnds.getHeight());
      return myPrefs.saveUserPrefs();
    }
    */

    private JFrame createFrameForGUI(JasiSolutionEditorPanel aPanel) {
        StringBuffer sb = new StringBuffer(132);
        sb.append("MUNG VERSION: ");
        sb.append(VERSION_ID);
        sb.append(" CONNECTION: ");
        sb.append(dbURLString);
        sb.append(" ");
        sb.append(dbUserText);
        JFrame aFrame = new JFrame(sb.toString());
        aFrame.getContentPane().add(aPanel, BorderLayout.CENTER);
        aFrame.pack();
        aFrame.setBounds(DEFAULT_FRAME_X,DEFAULT_FRAME_Y,
                         DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        return aFrame;
    }

    private void setDateTimeWindow(JasiSolutionEditorPanel aJSEP,
                    double startTime, double endTime) {
        if ((startTime == endTime) && startTime != 0. ) {
          // if no user prefs set, or input hrsBack < 0, force JSEP range chooser
          // to default to using prior settings in eventProperties
          aJSEP.setListSelectionTimeRange(null, null);
        }
        else  {
          // inputs both are  0. default or different times
          // save these input values in user's prefs and user's event props file
          // values are from either the hrsBack logic block or last user prefs settings
          aJSEP.setListSelectionTimeRange( new DateTime(startTime, true), new DateTime(endTime, true) ); // for UTC time - aww 2008/02/08
          eventStartTime = startTime;
          eventEndTime   = endTime;
        }
    }
    private void createEditor(double startTime, double endTime, boolean enableCommit) {
        aJSEP = configureDefaultPanel(enableCommit);
        redirectSystemOutput(aJSEP.getTextArea()); // System.out to textArea
        setDateTimeWindow(aJSEP, startTime, endTime);
        aJSEP.loadSolutionList(); // go fish for eligible events
        aFrame = createFrameForGUI(aJSEP); // adds window listeners

        // If adding exit confirmation window listener such as one below
        // ADD it AFTER subcomponent panel window listeners are intialized by
        // adding panel to frame (via ancestor listener) else the primary
        // JFrame closes/exits before subpanel listeners are executed
        aFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        aFrame.addWindowListener(new WindowAdapter () {
            public void windowClosing(WindowEvent evt) {
               if (JOptionPane.showConfirmDialog(null,
                   "Confirm exit",
                   "WindowClosing",
                   JOptionPane.YES_NO_OPTION,
                   JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION ){
                // check modified subpanels before arriving here, else
                // implement JasiListPanel WindowListener chain here
                // saveUserPrefs(); // removed 02/16/2005 -aww
                // Save loaded channels back to cache 10/16/03 aww
                if (editorProps.getBoolean("channelListCacheWrite") 
                    /*
                    JOptionPane.showConfirmDialog(null,
                      "Overwrite cache file on disk with current channel list",
                      "WindowClosing", JOptionPane.YES_NO_OPTION,
                      JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION
                    */
                   )
                {
                    if (! MasterChannelList.isEmpty()) {
                        dumpFinalChannelList(evt);
                        return;
                    }
                }
                destroyMainFrame(evt);
              }
            }
        });
        aFrame.setVisible(true);

        loadMasterChannelList();
    }
    private void dumpFinalChannelList(final WindowEvent evt) {
      final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
          WorkerStatusDialog workStatus = new WorkerStatusDialog(aFrame, false);
          //workStatus.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
          boolean success = false;
          // NO GRAPHICS CALLS ALLOWED IN THIS METHOD UNLESS InvokeLater!
          public Object construct() {
            String str = "<html> Writing to:<p> "+
                MasterChannelList.get().getCacheFilename()+
                "<p> may take 15 seconds ... be patient. </html>" ; 
            workStatus.pop("Writing ChannelList Before Exit", str,  true);
            success = MasterChannelList.get().writeToCache();
            return null;
          }

          //Runs on the event-dispatching thread graphics calls OK.
          public void finished() {
            workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
            if (success) {
                    destroyMainFrame(evt);
            } else {
              JOptionPane.showMessageDialog(
                        aFrame, "Failed", "ChanneList Write", JOptionPane.PLAIN_MESSAGE
              );
            }
          }
      };
      worker.start();  //required for SwingWorker 3
    }

/*
    private void dumpFinalChannelList(WindowEvent evt) {
        WorkerStatusDialog workStatus = new WorkerStatusDialog(aFrame, false);
        workStatus.setBeep(StatusDialog.BEEP_ON);
        String str = "Writing to: "+
            MasterChannelList.get().getCacheFilename()+
            " may take 15 seconds ... be patient."; 
        workStatus.pop("Writing ChannelList Before Exit", str,  true);
        MasterChannelList.get().writeToCache();
        workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
        destroyMainFrame(evt);
    }
*/

    private void destroyMainFrame(WindowEvent evt) {
                Window wnd = evt.getWindow();
                wnd.setVisible(false);
                wnd.dispose();
                System.exit(0);
    }

    private void loadMasterChannelList() {
        boolean status = false;
        try {
          status = loadMasterChannelList(aFrame);
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        if (! status) InfoDialog.informUser(aFrame, "WARNING", "ChannelList load failed!");
    }

    public void finalize() throws Throwable {
       if (closeConnection && DataSource.getConnection() != null)
           DataSource.close();
       super.finalize();
    }
    private static void initProperties() {
      if (editorProps == null) {
        editorProps = new MungPropertyList();
        //EnvironmentInfo.setApplicationName(myPrefs.getString("applicationName", APPLICATION_NAME));
        //EnvironmentInfo.setNetworkCode(myPrefs.getString("defaultNetCode", DEFAULT_NET_CODE));
        // editorProps.setFilename( (myPrefs == null) ? DEFAULT_PROP_FILE_NAME : myPrefs.getString("globalPropertyFileName", DEFAULT_PROP_FILE_NAME) );
        EnvironmentInfo.setApplicationName(APPLICATION_NAME);
        EnvironmentInfo.setAutomatic(false); // aww forced human by default - 03/24/2005
        editorProps.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
        editorProps.setFilename(DEFAULT_PROP_FILE_NAME);
        editorProps.reset();
        editorProps.setup(); // sets properties like DataSource leap/nominal secs
        EnvironmentInfo.setNetworkCode(editorProps.getProperty("localNetCode", "XX"));
      }

      debug = editorProps.getProperty("debug", "false").equalsIgnoreCase("true") || Debug.isEnabled();
      if (debug) {
        System.out.println("Mung property file: "+editorProps.getUserPropertiesFileName());
        editorProps.list(System.out);
      }
      //
      //String str = editorProps.getProperty("waveMagEnabled");
      //if (str != null) waveMagEnabled = editorProps.getBoolean("waveMagEnabled");
      //
      // TEMPORARILY list values here until db cleaned up
      if (debug) {
        System.out.println("DEBUG Mung channel matching properties set as follows:");
        System.out.println("      JasiChannelDbReader.defaultMatchMode = "+JasiChannelDbReader.defaultMatchMode);
        System.out.println("      ChannelName.useLoc                   = "+ChannelName.useLoc);
        System.out.println("      ChannelName.copyOnlySNCL             = "+ChannelName.copyOnlySNCL);
      }
    }

// see jiggle load ChannelList loading
    private boolean loadMasterChannelList(JFrame aFrame) {
        WorkerStatusDialog workStatus = new WorkerStatusDialog(aFrame, true);
        workStatus.setBeep(WorkerStatusDialog.BEEP_ON);

        String dsStr = "<html>Reading Channels from data source:<p>"+
                       DataSource.getDbaseName()+
                       "<p> may take 10 minutes ... be patient. </html>"; 
        //boolean readCache = editorProps.getBoolean("cacheChannelList"); // remove 02/03/2005
        boolean readCache = editorProps.getBoolean("channelListCacheRead"); // added 02/03/2005 aww
        String cacheFileName = editorProps.getUserFileNameFromProperty("channelCacheFilename");
        String str = null;
        if (readCache) {
          str = "<html> Reading Channels from file cache <p> "+cacheFileName+" </html>";
        } else {
          str = dsStr;
        }

        System.out.println(str);
        workStatus.pop("Loading ChannelList", str,  true);

        //
        // read Channel list from cache using the filename in input properties
        // uses default location if cacheFileName is null:
        //

        ChannelList mList = null;  
        int channelCount = 0;

        String chanGroupName = editorProps.getChannelGroupName(); // is there one?
        if (readCache) {
          // uses default location if cacheFileName is null:
          ChannelList.setCacheFilename(cacheFileName); // sets cachefilename
          mList = ChannelList.readFromCache();
          channelCount = mList.size();
          if (channelCount == 0) {
            // cache read failed, read from dbase and write to cache for future use
            System.out.println("Cache read failed.");
            System.out.println(dsStr);
            workStatus.setText(dsStr);

            if (chanGroupName == null) mList = ChannelList.readCurrentList();
            else {
              System.out.println("INFO: Loading current channels associated with chanGroupName = "+chanGroupName);
              mList = ChannelList.readCurrentListByName(chanGroupName);
            }

            channelCount = mList.size();
            //db read succeeded -- write this "current" list to cache file in background
            if (channelCount > 0) mList.writeToCacheInBackground();
          }
        } else {
          // don't read cache file, instead read "current" list and write it to cache. 
          if (chanGroupName == null) mList = ChannelList.readCurrentList();
          else {
            System.out.println("INFO: Loading current channels associated with chanGroupName = "+chanGroupName);
            mList = ChannelList.readCurrentListByName(chanGroupName);
          }

          channelCount = mList.size();
          if (channelCount > 0) mList.writeToCacheInBackground();
        }

        // create HashMap of channels in MasterChannelList, if not already one
        if (! mList.hasLookupMap()) {
            mList.createLookupMap();
        }

        // now set master list
        MasterChannelList.set(mList);
        System.out.println("MasterChannelList total channels: " + channelCount); 

        // set same list for engine delegate here - aww 05/07/04
        if (debug) System.out.println("Mung assigning MasterChannelList to editor panel engine delegate ..."); 
        if (aJSEP != null) aJSEP.setChannelList(mList);

        workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)

        return (channelCount > 0);
    }

}

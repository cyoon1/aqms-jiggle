package org.trinet.jdbc.datasources;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.spec.KeySpec;
import java.security.spec.AlgorithmParameterSpec;

import java.net.*;
import java.io.*;
import java.util.*;

//import sun.misc.BASE64Decoder;

import org.trinet.util.Base64;
//import org.trinet.util.Base64_2;

/**
 * Description of a JDBC dbase connection.
 */
public class DbaseConnectionDescription  {

   // Default port number ofor JDBC connections
    public final static String DefaultPort = AbstractSQLDataSource.DEFAULT_DS_PORT;
    public final static String DefaultDriver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
    public final static String DefaultSubprotocol  = AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL;

    /** JDBC driver name.*/
    private String subprotocol = DefaultSubprotocol;   // what comes after "jdbc:"
    private String driver = DefaultDriver;
    private String username = "";
    private String password = "";
    private String tnsName = "";
    private String dbasename = "";
    private String domain    = ""; // full IP-style address would be <domain>.<host>
    private String host = "";
    private String port = ""; // note = DefaultPort removed  - aww 2008/06/27
    private boolean encrypted = false;

    public DbaseConnectionDescription() { }

    public DbaseConnectionDescription(Properties props) {
        parseFromProperties(props);
    }

    /** No port given, use default. */
    public DbaseConnectionDescription(
                       String host,
                       String domain,
                       String dbasename,
                       String driver,
                       String username,
                       String password ) {
        this(host, domain, dbasename, DefaultPort, driver, username, password);
    }

/** Full specification. */
    public DbaseConnectionDescription( String host,
                       String domain,
                       String dbasename,
                       String port,
                       String driver,
                       String username,
                       String password ) {
        setURL(host, domain, dbasename, port);
        setDriver(driver);
        setUserName(username);
        setPassword(password);
    }

    /** For "oracle:oci" subprotocol with local client machine Oracle TNS names alias for the connection description. */
    public DbaseConnectionDescription( String tnsName, String username, String password ) {
        setURLtns(tnsName);
        //setDriver(DefaultDriver);
        setUserName(username);
        setPassword(password);
    }

    public DbaseConnectionDescription(DbaseConnectionDescription dcd) {
        subprotocol = dcd.subprotocol;
        driver = dcd.driver;
        username = dcd.username; 
        password = dcd.password;
        tnsName = dcd.tnsName;
        dbasename = dcd.dbasename;
        domain    = dcd.domain;
        host = dcd.host;
        port = dcd.port;
    }

    /** Return a new copy of this DbaseConnectionDescription*/
    public DbaseConnectionDescription copy(DbaseConnectionDescription dcd) {
        return new DbaseConnectionDescription(dcd);
    }

    public void setEncrypted(boolean tf) {
        encrypted=tf;
    }

    public String getDriver() {
        return driver;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setDriver(String driver) {
        this.driver = (driver == null) ? "" : driver;
        return !stringIsBlank(this.driver);
    }

    public String getSubprotocol() {
        return subprotocol;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setSubprotocol(String subprotocol) {
        this.subprotocol = (subprotocol == null) ? "" : subprotocol;
        return !stringIsBlank(this.subprotocol);
    }

    public String getUserName() {
        return username;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setUserName(String username) {
        this.username = (username == null) ? "" : username;
        return !stringIsBlank(this.username);
    }

    public String getPassword() {
        return password ;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setPassword(String password) {
        this.password = (password == null) ? "" : password;
        return !stringIsBlank(this.password);
    }

    public String getDomain() {
        return domain;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setDomain(String domain) {
        this.domain = (domain == null) ? "" : domain;
        return !stringIsBlank(this.domain);
    }

    public String getHostName() {
        return host;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setHostName(String host) {
        this.host = (host == null) ? "" : host;
        return !stringIsBlank(this.host);
    }

    public String getDbaseName() {
        return dbasename;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setDbaseName(String dbasename) {
        this.dbasename = (dbasename == null) ? "" : dbasename;
        return !stringIsBlank(this.dbasename);
    }

    /** Return Oracle TNS names alias, return true if string is not null or blank.*/
    public String getTNSname() {
        return tnsName;
    }

    /** Set Oracle TNS names alias, return true if string is not null or blank.*/
    public boolean setTNSname(String tnsName) {
        this.tnsName = (tnsName == null) ? "" : tnsName;
        return !stringIsBlank(this.tnsName);
    }

    public String getPort() {
        return port;
    }
    /** Set value, return true if string is not null or blank.*/
    public boolean setPort(String port) {
        this.port = (port == null) ? "" : port;
        return !stringIsBlank(this.port);
    }

    public boolean setPort(int port) {
        return setPort(String.valueOf(port));
    }

    /** Returns true if string is null or zero-length. */
    private boolean stringIsBlank(String strIn) {
      return (strIn == null || strIn.length() <= 0);
    }
 
    public void setURL(String host, String domain, String dbasename, String port) {
        setHostName(host);
        setDomain(domain);
        setDbaseName(dbasename);
        setPort(port);
        setSubprotocol(DefaultSubprotocol); //insurance in case it was "oci"
    }

    public void setURL(String host, String dbasename, String port) {
        setHostName(host);
        setDbaseName(dbasename);
        setPort(port);
    }

    public void setURL(String host, String dbasename, int portnumber) {
        setURL(host, dbasename, String.valueOf(portnumber));
    }

    /** Set the subprotocol to "oracle:oci", the Oracle TNS names connection alias to the input string,
     * and the host, domain, db name and db port attributes to empty string "".
     */
    public void setURLtns(String tnsName) {
        setTNSname(tnsName);
        setSubprotocol("oracle:oci");
        nullThinAttributes();
    }

    private void nullThinAttributes() {
        setHostName("");
        setDomain("");
        setDbaseName("");
        setPort("");
    }

    public boolean setURL(String dburl) {
      // MUST parse subprotocol, driver part is NOT parsed!!!
      boolean status1 = setSubprotocol(parseSubprotocolFromURL(dburl));

      boolean status2 = true;
      if (subprotocol.equals(DefaultSubprotocol) && !(dburl.toUpperCase().indexOf("DESCRIPTION") >= 0)) {
      // get database description: port, dbaseName, host, and domain
      // internal connections may not have description specified in the url:
          status2 = ( setPort(parsePortFromURL(dburl)) &&
                      setDbaseName(parseDbaseNameFromURL(dburl)) &&
                      setHostName(parseHostNameFromURL(dburl)) &&
                      setDomain(parseDomainNameFromURL(dburl))
                    );
      }
      else { // "oracle:oci"
          status2 = setTNSname(parseTNSnameFromURL(dburl));
          nullThinAttributes();
      }

      return (dburl.indexOf("@") == -1) ? status1 : (status1 && status2);
    }

    /** Return the fully specified name of the JDBC URL for the dbase.
     * It is composed:<br>
     * <i>protocol:driver:@dbaseHost.Domain:dbasePort:dbaseName</i><br>.
     * Like: jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq
     * Note URL set from an internal server connection may have 
     * only the protocol:driver and no database description.
     * */
    public String getURL() {
        StringBuffer sb = new StringBuffer(512);
        sb.append("jdbc:");
        sb.append(subprotocol).append(":");
        //RH sb.append("@");
        // form database description
        if (subprotocol.equals(DefaultSubprotocol)) {
          StringBuffer sb2 = new StringBuffer(256);
          if (getHostName() != null && getHostName().length() > 0) {
            sb2.append(getHostName());
            if (! getDomain().equals("")) sb2.append(".").append(getDomain());
            sb2.append(":").append(getPort()).append(":").append(getDbaseName());
            sb.append("@");
            sb.append(sb2.toString());
          }
          else if (getTNSname() != "" && getTNSname().toUpperCase().indexOf("ADDRESS") > 0 ) {
              sb.append("@");
              sb.append(getTNSname()); // aww 2008/05/14
          }
        }
        else if ( subprotocol.equals(AbstractSQLDataSource.POSTGRESQL_DS_SUBPROTOCOL) ) {
            // format: jdbc:postgresql://host:port/database
            StringBuffer sb2 = new StringBuffer(256);
            if (getHostName() != null && getHostName().length() > 0) {
                sb2.append("/");
                sb2.append(getHostName());
                if (!getDomain().equals("")) sb2.append(".").append(getDomain());
                sb2.append(":").append(getPort()).append("/").append(getDbaseName());
                sb.append("/");
                sb.append(sb2.toString());
            }
            else {
                // assume localhost connection: jdbc:postgresql:database
                sb.append(getDbaseName());
            }
        }
        else {
            sb.append("@");
            sb.append(getTNSname()); // aww 2008/05/14
        }
        return sb.toString();
    }

    /** Return the IP address formed by <domain>.<host>,
     * return "" if host and domain are not defined.*/
    public String getIPAddress() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(getHostName());
        if (! getDomain().equals("")) sb.append(".").append(getDomain());
        return sb.toString();
    }

    // parse out elements into data fields host and domain
    public void setIPAddress(String addressString) {
        int dot = addressString.indexOf(".");
        if (dot > 0) {
          setHostName(addressString.substring(0, dot));
          setDomain(addressString.substring(dot+1));
        }
        else if (dot < 0)  setHostName(addressString);
        else if (dot == 0) setDomain(addressString.substring(1));
    }

    /** Reads and parses a dbase connection description from the contents of a properties file.
     * @see: parseFromProperties() */
    public void readFromPropertyFile(String filename) {
        Properties props = new Properties();
        try {
          FileInputStream in = new FileInputStream(filename);
          props.load(in);
          parseFromProperties(props);

          in.close();
        } catch (Exception e) {
          System.err.println(e.toString());
          System.err.println("Error: check input of properties file: "+ filename);
        }
    }
    /** Create a dbase connection description from the contents of a properties file.
     Returns 'true' only if all parts of the description are set.
     Requires the following property names be defined in the properties file: <p>
     Assuming default subprotocol "oracle:thin"
     dbaseHost dbasePasswd dbaseName dbaseDomain dbaseUser dbaseDriver dbasePort<br>
     otherwise DO NOT specify any of the above but instead specify only <br>
     dbaseSubprotocol="oracle:oci" dbaseTNSname<br>
     </pre>
     unless defined by property "dbaseSubprotocol=someSubProtocolName"
     */
    public boolean parseFromProperties(Properties props) {

      setDriver(props.getProperty("dbaseDriver", DefaultDriver));

      boolean statusThin = setDomain(props.getProperty("dbaseDomain"));
      statusThin &= setDbaseName(props.getProperty("dbaseName"));
      statusThin &= setHostName(props.getProperty("dbaseHost"));
      statusThin &= setPort(props.getProperty("dbasePort", DefaultPort));

      boolean statusUser = false;
      encrypted = props.getProperty("dbaseEncrypted", "false").equalsIgnoreCase("true");
      if (encrypted) {
        statusUser = setUserName(DbaseConnectionDescription.Encryptor.decrypt(props.getProperty("dbaseUser")));
        statusUser &= setPassword(DbaseConnectionDescription.Encryptor.decrypt(props.getProperty("dbasePasswd")));
      }
      else {
        statusUser = setUserName(props.getProperty("dbaseUser"));
        statusUser &= setPassword(props.getProperty("dbasePasswd") );
      }

      subprotocol = props.getProperty("dbaseSubprotocol", DefaultSubprotocol);  // aww 2008/06/27
      //boolean statusTNS = subprotocol.equals("oracle.oci");
      boolean statusTNS = setTNSname(props.getProperty("dbaseTNSname")); // aww 2008/06/27

      return statusUser && (statusThin || statusTNS);

    }

    /** Return true if no description attributes are set null or blank.
     * Does not check that an actual connection can be made with the settings. */
    public boolean isValid() {
      // removed !stringIsBlank(domain) from host validity test, a local network does require a domain 
      return  (
                !stringIsBlank(username) && !stringIsBlank(password) && !stringIsBlank(driver)
              )
              &&
              (
               (!stringIsBlank(host) && !stringIsBlank(dbasename) && !stringIsBlank(port)) ||
               (!stringIsBlank(subprotocol) && !stringIsBlank(tnsName))
              );
    }

    public String describeUndefinedAttributes() {
        StringBuffer sb = new StringBuffer(128);
        if (stringIsBlank(username)) sb.append("username").append(" ");
        if (stringIsBlank(password)) sb.append("password").append(" ");
        if (stringIsBlank(subprotocol)) sb.append("subprotocol").append(" ");
        if (stringIsBlank(tnsName)) sb.append("tnsName").append(" ");
        if (stringIsBlank(host)) sb.append("host").append(" ");
        if (stringIsBlank(domain)) sb.append("domain").append(" ");
        if (stringIsBlank(dbasename)) sb.append("dbasename").append(" ");
        if (stringIsBlank(port)) sb.append("port").append(" ");
        if (stringIsBlank(driver)) sb.append("driver");
        if (sb.length() > 0) sb.insert(0, "UNDEFINED database connection attributes: ");
        return sb.toString();
    }

    public void writeToPropertyfile(String filename) {
        Properties props = new Properties();
        addToProperties(props);

        try {
          FileOutputStream out = new FileOutputStream(filename);

            props.store(out, "Dbase connection description");            // new as of v1.2

          out.close();
        } catch (Exception e) {System.err.println(e);}
    }

    public void addToProperties(Properties props) {
        props.put ("dbaseDriver", getDriver());
        props.put ("dbaseDomain", getDomain());
        props.put ("dbaseHost", getHostName());
        props.put ("dbaseName", getDbaseName());
        props.put ("dbasePort", getPort());
        props.put ("dbaseSubprotocol", getSubprotocol()); // aww
        props.put ("dbaseTNSname", getTNSname()); // aww
        if (encrypted) {
          props.put("dbaseEncrypted","true");
          String str = DbaseConnectionDescription.Encryptor.encrypt(getPassword());
          props.put ("dbasePasswd", str);
          str = DbaseConnectionDescription.Encryptor.encrypt(getUserName());
          props.put ("dbaseUser", str);
        }
        else {
          props.put("dbaseEncrypted","false");
          props.put ("dbasePasswd", getPassword());
          props.put ("dbaseUser", getUserName());
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DbaseConnectionDescription)) return false;
        DbaseConnectionDescription dcd = (DbaseConnectionDescription) obj;
        return getURL().equals(dcd.getURL()) &&
               getUserName().equals(dcd.getUserName()) &&
               getPassword().equals(dcd.getPassword());
    }
    /** Show driver, URL (JDBC syntax) and username but not user password.<p>
     * For example:<br>
     * <it>
     * oracle.jdbc.driver.OracleDriver jdbc:oracle:thin:@serverma.gps.caltech.edu:1521:databasema trinetdb
     * </it>*/

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(driver).append("  ");
        sb.append(getURL()).append(" ");
        sb.append(getUserName()).append(" ");
        //sb.append(getPassword()); // debug only - keep secret for security
        return sb.toString();
    }

    /** Return subprotocol string. For Oracle this is the string between "jdbc:" and ":@".
     * Assumes URL format like:
     *  <tt>
     *          "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *                ^^^^^^^^^^^
     *  </tt> */
    public static String parseSubprotocolFromURL(String URLstring) {
      if ( ! URLstring.startsWith("jdbc:") ) {
        System.err.println("ERROR DbaseConnectionDescription: Not a valid jdbc URL /"+URLstring+"/");
        return "";
      }
        String str = getURLBeforeAt(URLstring);
        int len = str.length();
        return (len < 6) ? "" : str.substring(5, len-1);  // trim "jdbc:" & trailing ":"
    }

    /** Return TNS alias name: assumes URL format like <tt>"jdbc:oracle:oci:@myaliasdb"</tt>*/
    public static String parseTNSnameFromURL(String URLstring) {
        return getURLAfterAt(URLstring);
    }

    /** Return dbase host name: assumes URL format like
     *  "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq" 
     *                     ^^^^^
     *  </tt> */
    public static String parseHostNameFromURL(String URLstring) {
        String str = getURLAfterAt(URLstring);
        if (str.equals("")) return str;

        //int i2 = str.indexOf(':');
        int i2 = str.indexOf('.');
        return str.substring(0, i2);
    }
    /** Return domain name: assumes URL format like
     *  <tt>
     *  "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *                          ^^^^^^^^^^^^^^^^
     *  </tt>
     */
    public static String parseDomainNameFromURL(String URLstring) {
        String str = getURLAfterAt(URLstring);
        if (str.equals("")) return str;
                
        int i1 = str.indexOf('.');        // 1st "."
        int i2 = str.indexOf(':', i1+1);  // 2nd ":"
        return str.substring(i1+1, i2);
    }

    /** Parse the port number out of the URL string.assumes URL format like
     *  <tt>
     *  "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *                                           ^^^^
     *  </tt>
     *  */
    public static String parsePortFromURL(String URLstring) {
        String str = getURLAfterAt(URLstring);
        if (str.equals("")) return str;

        int i1 = str.indexOf(':');        // 1st ":"
        int i2 = str.indexOf(':', i1+1);  // 2nd ":"
        return str.substring(i1+1, i2);
    }
    /** Return dbase name:assumes URL format like
     *  <tt>
     *  "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *                                                ^^^^^^^
     *  </tt>*/
    public static String parseDbaseNameFromURL(String URLstring) {
        String str = getURLAfterAt(URLstring);
        if (str.equals("")) return str;

        int i1 = str.indexOf(':');        // 1st ":"
        int i2 = str.indexOf(':', i1+1);  // 2nd ":"
        return str.substring(i2+1);
    }
    /** Helper for parse methods. Returns string AFTER the "@".
     *  Assumes URL format like:
     *  <tt>
     *  "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *  </tt>
     *  */
    private static String getURLAfterAt(String URLstring) {
        int idx = URLstring.lastIndexOf("@");
        return (idx != -1) ? URLstring.substring(idx+1) : "";
    }
    /** Helper for parse methods. Returns string BEFORE the "@".
     *  Assumes URL format like:
     *  <tt>
     *           "jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq"
     *            ^^^^^^^^^^^^^^^^^
     *  </tt>
     *  */
    private static String getURLBeforeAt(String URLstring) {
        int idx = URLstring.indexOf("@");
        return (idx != -1) ? URLstring.substring(0, idx) : URLstring; // return URL not "" - aww bug fix 12/11 
    }

    private static class Encryptor {

        private static Cipher ecipher = null;
        private static Cipher dcipher = null;

        // salt random bytes
        private static byte[] salt = {
            (byte)0xA7, (byte)0x3B, (byte)0xC5, (byte)0x1B,
            (byte)0x17, (byte)0x53, (byte)0xFE, (byte)0x13
        };

        // Iteration count
        private static int iterationCount = 23;

        private static String key = "neycZlitSlePmuR";

        static {
            try {
                // key
                KeySpec keySpec = new PBEKeySpec(key.toCharArray(), salt, iterationCount);
                SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
                ecipher = Cipher.getInstance(key.getAlgorithm());
                dcipher = Cipher.getInstance(key.getAlgorithm());

                // parameters for ciphers
                AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

                // make ciphers
                ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
                dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            } catch (java.security.InvalidAlgorithmParameterException e) {
            } catch (java.security.spec.InvalidKeySpecException e) {
            } catch (javax.crypto.NoSuchPaddingException e) {
            } catch (java.security.NoSuchAlgorithmException e) {
            } catch (java.security.InvalidKeyException e) {
            }
        }

        static String encrypt(String mystr) {
            try {
                // Encode the string into bytes using utf-8
                byte[] utf8 = mystr.getBytes("UTF8");

                // Encrypt
                byte[] enc = ecipher.doFinal(utf8);

                // Encode bytes to base64 to get a string
                //return new sun.misc.BASE64Encoder().encode(enc);
                //String tt = Base64_2.encodeToString(enc, true);
                String tt = Base64.encodeBytes(enc);
                return tt;
            } catch (javax.crypto.BadPaddingException e) {
                System.err.println(e.toString());
            } catch (IllegalBlockSizeException e) {
                System.err.println(e.toString());
            } catch (UnsupportedEncodingException e) {
                System.err.println(e.toString());
            }
            //Java7 warns as unreachable:
            //catch (java.io.IOException e) {
            //    System.err.println(e.toString());
            //}
            return null;
        }

        static String decrypt(String mystr) {
            try {
                // Decode base64 to get bytes
                //byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(mystr);
                //byte[] dec = Base64_2.decode(mystr);
                byte[] dec = Base64.decode(mystr);

                // Decrypt
                byte[] utf8 = dcipher.doFinal(dec);

                // Decode using utf-8
                String tt = new String(utf8, "UTF8");
                return tt;
            } catch (javax.crypto.BadPaddingException e) {
                System.err.println(e.toString());
            } catch (IllegalBlockSizeException e) {
                System.err.println(e.toString());
            } catch (UnsupportedEncodingException e) {
                System.err.println(e.toString());
            } catch (java.io.IOException e) {
                System.err.println(e.toString());
            }
            return null;
        }
    }

    public static final void main(String args[]) {

        DbaseConnectionDescription dbd = new DbaseConnectionDescription();

        org.trinet.jasi.JasiDatabasePropertyList props = new org.trinet.jasi.JasiDatabasePropertyList(); 
        props.setProperty("dbasePasswd",args[1]);
        props.setProperty("dbaseUser",args[0]);
        dbd.parseFromProperties(props);
        //System.out.println("ME: " + dbd.toString());

        //System.out.println("Now set encrypted and dump props:");
        dbd.encrypted=true;
        props = new org.trinet.jasi.JasiDatabasePropertyList(); 
        dbd.addToProperties(props);
        //props.dumpProperties();
        //System.out.println("Now write encrypted props to:" + args[2]);
        dbd.writeToPropertyfile(args[2]);

        /*
        System.out.println("Now read encrypted props from dbd.props:");
        DbaseConnectionDescription dbd = new DbaseConnectionDescription();
        dbd.readFromPropertyFile(args[2]);
        System.out.println("ME: " + dbd.toString());
        */
    }

} // DbaseConnectionDescription

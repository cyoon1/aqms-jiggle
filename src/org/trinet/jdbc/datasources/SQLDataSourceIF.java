package org.trinet.jdbc.datasources;
import java.sql.*;

public interface SQLDataSourceIF extends GenericDbServiceIF {

    public boolean set(DbaseConnectionDescription descript);
    public DbaseConnectionDescription getDbaseConnectionDescription();

    // configure is a wrappers around the set(),setWriteBackEnabled() combo:
    public void configure(DbaseConnectionDescription desc); // writeBack = true
    public void configure(String dbaseURL, String driver, String username, String passwd); // writeBack = true
    public void configure(String dbaseURL, String driver, String username, String passwd, boolean allowWriteBack);
    public void configure(Connection conn); // writeBack = true
    public void configure(Connection conn, boolean allowWriteBack);

    public void set(String url, String driver, String username, String passwd);
    public boolean set(Connection connection);

    public Connection getConnection();
    public Connection getNewConnect();
    public Connection createInternalDbServerConnection();

    public void commit(Connection connection);
    public void commit();
    public void rollback();
    public void close();

    public void setWriteBackEnabled(boolean tf);
    public boolean isWriteBackEnabled();
    public boolean isReadOnly();
    public boolean isClosed();
    public boolean onServer();

    public String getConnectionStatus();

    public String toDumpString();
}


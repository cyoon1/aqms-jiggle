package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Event
*/
public interface TableRowEvent extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "EVENT";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  11;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "EVSEQ";


/**  EVENT table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 0;

/**  EVENT table "prefor" column data object offset in collection stored by implementing class.
*/
    public static final int PREFOR = 1;

/**  EVENT table "prefmag" column data object offset in collection stored by implementing class.
*/
    public static final int PREFMAG = 2;

/**  EVENT table "prefmec" column data object offset in collection stored by implementing class.
*/
    public static final int PREFMEC = 3;

/**  EVENT table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 4;

/**  EVENT table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 5;

/**  EVENT table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 6;

/**  EVENT table "etype" column data object offset in collection stored by implementing class.
*/
    public static final int ETYPE = 7;

/**  EVENT table "selectflag" column data object offset in collection stored by implementing class.
*/
    public static final int SELECTFLAG = 8;

/**  EVENT table "selectflag" column data object offset in collection stored by implementing class.
*/
    public static final int VERSION = 9;

/**  EVENT table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 10;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "EVID,PREFOR,PREFMAG,PREFMEC,COMMID,AUTH,SUBSOURCE,ETYPE,SELECTFLAG,VERSION,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "EVENT.EVID,EVENT.PREFOR,EVENT.PREFMAG,EVENT.PREFMEC,EVENT.COMMID,EVENT.AUTH,EVENT.SUBSOURCE,EVENT.ETYPE,EVENT.SELECTFLAG,EVENT.VERSION,EVENT.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"EVID", "PREFOR", "PREFMAG", "PREFMEC", "COMMID", 
	"AUTH", "SUBSOURCE", "ETYPE", "SELECTFLAG", "VERSION","LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, true, true, true, 
	false, true, true, true, true,
        true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATALONG, DATALONG, 
	DATASTRING, DATASTRING, DATASTRING, DATALONG, DATALONG,
        DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 15, 15, 8, 7, 1, 15, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}

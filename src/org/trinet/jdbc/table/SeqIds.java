package org.trinet.jdbc.table;
import java.lang.*;
import java.util.*;
import java.sql.*;
import org.trinet.jdbc.*;
import org.trinet.jasi.DataSource;

/**
Class queries the database using a JDBC Connection Statement for the current or next sequence numbers in named sequences.
Known sequences for the NCEDC schema are:

AMPSEQ
ARSEQ
COMMSEQ
COSEQ
EVSEQ
FILESEQ
MAGSEQ
MECSEQ
ORSEQ
WASEQ

To get a updated list of sequence names from the database use the query:
select object_name from user_objects where object_type = 'SEQUENCE' ;
*/
public abstract class SeqIds {
    private static ResultSet rs;
    
/** Method implemented by class extensions specific to a named sequence. */
    public abstract long getNextSeq();
/** Method implemented by class extensions specific to a named sequence. */
    public abstract long getCurrSeq();
    
/** Returns the next sequence number from the database sequence with the specified input name.
* Requires an DataSource static connection, user must first instantiate a DataSource Connection.
* Returns 0, if no such sequence exits or an SQLException occurs.
*/
    public static long getNextSeq(String name) {
      return getNextSeq(DataSource.getConnection(), name);
    }
    
/** Returns the current sequence number from the database sequence with the specified input name.
* Requires an DataSource static connection, user must first instantiate a DataSource Connection.
* Returns 0, if no such sequence exits or an SQLException occurs.
*/
    public static long getCurrSeq(String name) {
      return getCurrSeq(DataSource.getConnection(), name);
    }

/** Returns the next sequence number from the database sequence with the specified input name.
* Uses the specified input Connection object for the database query, user must first instantiate a connection object.
* Returns 0, if no such sequence exits or an SQLException occurs.
*/
    public static long getNextSeq(Connection conn, String name) {
        long idum=0;
        boolean isPostgreSQL=false;
        String sql = "SELECT " + name + ".NEXTVAL FROM DUAL";
        try {
            if (conn.getMetaData().getDatabaseProductName().toString().equalsIgnoreCase("postgresql")) {
                isPostgreSQL = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (isPostgreSQL) {
            sql = "SELECT NEXTVAL('" + name + "')";
        }
        PreparedStatement sm = null;
        try {
          sm = conn.prepareStatement(sql);
          if (sm != null) {
            rs = sm.executeQuery(); 
            while ( rs.next() ) {
              idum = rs.getLong(1);
            }
          }
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
        }
        finally {
            try {
              if (rs != null) rs.close();
              if (sm != null) sm.close();
            }
            catch(SQLException ex) { }
        }
        return idum;
    }
    
/** Returns the current sequence number from the database sequence with the specified input name.
* Uses the specified input Connection object for the database query, user must first instantiate a connection object.
* Returns 0, if no such sequence exits or an SQLException occurs.
*/
    public static long getCurrSeq(Connection conn, String name) {
        long idum=0;
        boolean isPostgreSQL=false;
        String sql = "SELECT " + name + ".CURRVAL FROM DUAL";
        try {
            if (conn.getMetaData().getDatabaseProductName().toString().equalsIgnoreCase("postgresql")) {
                isPostgreSQL = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (isPostgreSQL) {
            sql = "SELECT CURRVAL('" + name + "')";
        }

        PreparedStatement ps = null;
        try {
          ps = conn.prepareStatement(sql);
          if (ps != null) {
            rs = ps.executeQuery(); 
            while ( rs.next() ) {
              idum = rs.getLong(1);
            }
          }
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
        }
        finally {
            try {
              if (rs != null) rs.close();
              if (ps != null) ps.close();
            }
            catch(SQLException ex) { }
        }
        return idum;
    }
/*
    public static final void main(String args[]) {    
      if (args.length < 4) {
        System.out.println("Args: [user] [pwd] [host.domain] [dbName]
        System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host   = args[2];
      String dbname = args[3];

      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;
      System.out.println("SeqIds: making connection... ");
      DataSource ds = new DataSource (url, org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd);
      System.out.println("UnassocSeq next: " + SeqIds.getNextSeq("UNASSOCSEQ"));
      System.out.println("UnassocSeq curr: " + SeqIds.getCurrSeq("UNASSOCSEQ"));
      ds.close();
    }
*/
}

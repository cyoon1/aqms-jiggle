package org.trinet.jdbc.table;
/** The class implements several convenience methods to provide implementing class extensions some base class methods.
* Because base class methods use a JDBC connection class to access the database containing the table described by this
* object, a connection object must be instantiated before using any of the database enabled methods of this class.
* Classes which are associated with the Netmag id (magid) extend this class:
* @see AssocCoM
* @see AssocAmM
*/

public class NetMagAssocTableRow extends DataTableRow implements Cloneable {

    NetMagAssocTableRow (String tableName, String sequenceName, int maxFields, int [] keyColumnIndex,
		String [] fieldNames, boolean [] fieldNulls, int [] fieldClassIds ) {
	super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }

/** Overrides DataTableRow.clone()
*/
    public Object clone() {
	NetMagAssocTableRow obj = null;
	obj = (NetMagAssocTableRow) super.clone();
	return obj;
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
	String  whereString = 
		"WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID IN ( SELECT ORID FROM ORIGIN WHERE EVID = " + evid + " ))";
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
	String whereString = "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID = " + orid + " )";
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the preferred origin id of the specified input event id.
*/
    public int getRowCountByPreferredOriginId(long evid) {
	String whereString = "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID = ( SELECT PREFOR FROM EVENT WHERE EVID = "
		+ evid + " ))";
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the preferred netmag id of the specified input event id.
*/
	public int getRowCountByPreferredMagId(long evid) {
	    String whereString = "WHERE MAGID = ( SELECT PREFMAG FROM EVENT WHERE EVID = " + evid + " )";
	    return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
	}

/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByEventId(long evid) {
	String  whereString = 
		"WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID IN ( SELECT ORID FROM ORIGIN WHERE EVID = "
		+ evid + " ))";
	return getRowsEquals(whereString);
    }
   
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByOriginId(long orid) {
	String whereString = "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID = " + orid + " )";
	return getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the preferred net origin id (prefor) of the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByPreferredOriginId(long evid) {
	String whereString = "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID = ( SELECT PREFOR FROM EVENT WHERE EVID = "
		+ evid + " ))";
	return getRowsEquals(whereString);
    }
/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByPreferredMagId(long evid) {
	String  whereString = "WHERE MAGID = (SELECT PREFMAG FROM FROM EVENT WHERE EVID =" + evid + " )";
	return getRowsEquals(whereString);
    }
    
/*
/** Deletes rows associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public static int deleteRowsByEventId(long evid) {
	String  whereString =
	  "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID IN (SELECT ORID FROM EVENT WHERE EVID =" + evid + " ))";
	return ExecuteSQL.deleteRowsEquals(connDB, getTableName(), whereString);
    }
*/

}

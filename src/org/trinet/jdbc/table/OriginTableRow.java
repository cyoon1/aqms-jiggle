package org.trinet.jdbc.table;

/** The class implements several convenience methods to provide implementing class extensions some base class methods.
* Because base class methods use a JDBC connection class to access the database containing the table described by this
* object, a connection object must be instantiated before using any of the database enabled methods of this class.
* Classes which associate the origin id (orid) and another table key extending this class:
* @see Origin
* @see OriginError
*/
public abstract class OriginTableRow extends DataTableRow implements Cloneable {
    public OriginTableRow (String tableName, String sequenceName, int maxFields, int[] keyColumnIndex,
        String [] fieldNames, boolean [] fieldNulls, int [] fieldClassIds ) {
    super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }
    
/** Overrides DataTableRow.clone()
*/
    public Object clone() {
    OriginTableRow obj = null;
    obj = (OriginTableRow) super.clone();
    return obj;
    }

/** Returns table row count.
*/
    public int getRowCount() {
    return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
    String whereString = "WHERE EVID = " + evid;
    return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array of the invoking class type where each element contains the data of a table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByEventId(long evid) {
    return getRowsEquals("EVID", evid);
    }

    //Why do we need this is unique to this class? - aww 11/24
    public Object getRowsBySql(String sql) {
      return getRows(connDB, sql);
    }

/** Returns an object instance containing the data parsed from an SQL query
* for the row associated with the preferred origin id (event.prefor) of the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public DataTableRow getRowByPreferredOriginId(long evid) {
    String whereString = "WHERE ORID IN (SELECT PREFOR FROM EVENT WHERE EVID = " + evid + ")";
    DataTableRow [] dtr = (DataTableRow []) getRowsEquals(whereString);
    if (dtr == null) return null;
    return dtr[0]; 
    }

/*
* Deletes rows from the database table defined by tableName associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByEvent(long evid) {
    String whereCondition = " WHERE EVID = " + evid;
    return ExecuteSQL.deleteRowsWhere(connDB, getTableName(), whereCondition);
    }

/** Deletes rows associated with the specified origin id (orid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByOrigin(long orid) {
    return ExecuteSQL.deleteRowsEquals(connDB, getTableName(), "ORID", orid);
    }
*/

}

package org.trinet.jdbc.table;
import java.util.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import org.trinet.util.LeapSeconds;

/** Class to access and manipulate NCEDC Schema tables containing station channel
 * observation data. Tables of this type have a single primary key column whose
 * value is defined by an associated database integer sequence identifier.
* Station channel data is referenced to event summary data through association tables.
* @see Amp
* @see Arrival
* @see Coda
*/
public abstract class StnChlTableRow extends DataTableRow implements Cloneable {

/** Data string member defined by extending classes is name of primary key
 * column of implementing table.
*/
    protected String keyColumn = null;

/** Data string member defined by extending classes is name of origin id (orid)
 * association table for primary key column.
*/
    protected String assocTableOrigin = null;

/** Data string member defined by extending classes is name of netmag id (magid)
 * association table for primary key column.
*/
    protected String assocTableNetmag = null;

/** Constructor invokes DataTableRow constructor.
*/
    protected StnChlTableRow(String tableName, String sequenceName, int maxFields, int [] keyColumnIndex,
                        String[] fieldNames, boolean [] fieldNulls, int[] fieldClassIds) {
    super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }

/** Overrides DataTableRow.clone()
*/
    public Object clone() {
    StnChlTableRow obj = null;
    obj = (StnChlTableRow) super.clone();
    obj.keyColumn = new String (keyColumn);
    obj.assocTableOrigin = new String (assocTableOrigin);
    obj.assocTableNetmag = new String (assocTableNetmag);
    return obj;
    }

    public static String toDateTimeConstraintSQLWhereClause(String tableName,
       java.util.Date startDate, java.util.Date endDate) {

        if (startDate == null && endDate == null) return "";

        String tableStr = "";
        if (! NullValueDb.isEmpty(tableName)) tableStr = tableName + ".";

        StringBuffer sb = new StringBuffer(128);
        sb.append(" ( ");
        if (startDate != null) {
            sb.append(tableStr).append("DATETIME >= TRUETIME.putEpoch(");
            sb.append(LeapSeconds.dateToTrue(startDate)).append(", 'UTC')"); // for UTC secs - aww 2008/01/24 
            if (endDate != null) sb.append(" AND ");
        }
        if (endDate != null) {
            sb.append(tableStr).append("DATETIME <= TRUETIME.putEpoch(");
            sb.append(LeapSeconds.dateToTrue(endDate)).append(", 'UTC')"); // for UTC secs - aww 2008/01/24
        }
        sb.append(" )");
        return sb.toString();
    }

// DataStnChl methods
/** Returns a DataStnChl object derived from the station channel data stored in this object instance, else returns null.
*/
    public DataStnChl getDataStnChl() {
    return new DataStnChl(
        (DataString) fields.get(findFieldIndex("STA")),
        (DataString) fields.get(findFieldIndex("NET")),
        (DataString) fields.get(findFieldIndex("AUTH")),
        (DataString) fields.get(findFieldIndex("SUBSOURCE")),
        (DataString) fields.get(findFieldIndex("CHANNEL")),
        (DataString) fields.get(findFieldIndex("CHANNELSRC")),
        (DataString) fields.get(findFieldIndex("SEEDCHAN")),
        (DataString) fields.get(findFieldIndex("LOCATION")));
    }
/** Sets station channel data for this object instance to the DataObjects members of the input DataStnChl object;
* a null input results in a no-op.
*/
    public void setDataStnChl(DataStnChl obj) {
    if (obj == null) return;
    fields.set(findFieldIndex("STA"), ((DataString) obj.getDataObject(DataStnChl.STA)).clone());
    fields.set(findFieldIndex("NET"), ((DataString) obj.getDataObject(DataStnChl.NET)).clone());
    fields.set(findFieldIndex("AUTH"), ((DataString) obj.getDataObject(DataStnChl.AUTH)).clone());
    fields.set(findFieldIndex("SUBSOURCE"), ((DataString) obj.getDataObject(DataStnChl.SUBSOURCE)).clone());
    fields.set(findFieldIndex("CHANNEL"), ((DataString) obj.getDataObject(DataStnChl.CHANNEL)).clone());
    fields.set(findFieldIndex("CHANNELSRC"), ((DataString) obj.getDataObject(DataStnChl.CHANNELSRC)).clone());
    fields.set(findFieldIndex("SEEDCHAN"), ((DataString) obj.getDataObject(DataStnChl.SEEDCHAN)).clone());
    fields.set(findFieldIndex("LOCATION"), ((DataString) obj.getDataObject(DataStnChl.LOCATION)).clone());
    }
/** Returns List object containing station channel DataObject fields for this object instance.
*/
    public List getDataStnChlList() {
    return fields.subList(findFieldIndex("STA"), findFieldIndex("LOCATION"));
    }
/** Sets station data fields in this object instance to the values in input List; a null input results in a no-op.
*/
    public void setDataStnChlList(List list) {
    if (list == null) return;
    fields.addAll(findFieldIndex("STA"), list);
    }

// Database query methods
/** Returns table row count.
*/
    public int getRowCount() {
        return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
        String countString = "DISTINCT " + keyColumn;
        String whereString = "WHERE " +  keyColumn + " IN (SELECT " + keyColumn + " FROM " + assocTableOrigin 
        + " WHERE ORID IN (SELECT ORID FROM ORIGIN WHERE EVID = " + evid + "))" ;
        return ExecuteSQL.getRowCount(connDB, getTableName(), countString, whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
        String countString = keyColumn;
        String whereString = "WHERE " +  keyColumn + " IN (SELECT " + keyColumn + " FROM " + assocTableOrigin 
        + " WHERE ORID = " + orid + ")" ;
        return ExecuteSQL.getRowCount(connDB, getTableName(), countString, whereString);
    }

/** Returns table row count corresponding to the preferred origin id of the specified input event id.
*/
    public int getRowCountByPreferredOriginId(long evid) {
        String countString = keyColumn;
        String whereString = "WHERE " +  keyColumn + " IN (SELECT " + keyColumn + " FROM " + assocTableOrigin 
        + " WHERE ORID = (SELECT PREFOR FROM EVENT WHERE EVID = " + evid + "))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), countString, whereString);
    }

/** Returns table row count corresponding to the preferred netmag id of the specified input event id.
*/
    public int getRowCountByPreferredMagId(long evid) {
        if (assocTableNetmag == null) return -1;
        String countString = keyColumn;
        String whereString = "WHERE " +  keyColumn + " IN (SELECT " + keyColumn + " FROM " + assocTableNetmag
        + " WHERE MAGID = (SELECT PREFMAG FROM EVENT WHERE EVID = " + evid + "))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), countString, whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the DATETIME column is BETWEEN the specified input times and the SUBSOURCE column has the specified input value.
* A return value of null indicates no data or an error condition.
*/
// Perhaps needs to qualify AUTH too?
    public Object getRowsByDateTimeRange(double tStart, double tEnd, String subsource) {
        String str = "WHERE DATETIME BETWEEN TRUETIME.putEpoch(" + StringSQL.valueOf(tStart) +
                ",'UTC') AND TRUETIME.putEpoch(" + StringSQL.valueOf(tEnd) + ", 'UTC')"; // for UTC secs - aww 2008/01/24 

        return (NullValueDb.isEmpty(subsource)) ? str : str + StringSQL.valueOf(subsource); 
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByEventId(long evid) {
    return getRowsByEvent(keyColumn, assocTableOrigin, "ORID", evid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByOriginId(long orid) {
    return getRowsByAssoc(keyColumn, assocTableOrigin, "ORID", orid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the preferred net magnitude id (prefmag) of the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByPreferredMagId(long evid) {
    if (assocTableNetmag == null) return null;
    return getRowsByEventPrefId(keyColumn, assocTableNetmag, "MAGID", "PREFMAG",  evid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the preferred net origin id (prefor) of the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByPreferredOriginId(long evid) {
    return getRowsByEventPrefId(keyColumn, assocTableOrigin, "ORID", "PREFOR",  evid);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid) and station channel data inputs. 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByEventIdStnChl(long evid, DataStnChl sc) {
        String whereString = "WHERE " + keyColumn +
                " IN ( SELECT " + keyColumn + " FROM " + assocTableOrigin + 
        " WHERE ORID IN ( SELECT ORID FROM ORIGIN WHERE EVID = " + evid + " ) ) AND " + sc.toStringSQLWhereCondition();
        return getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid) and station channel data inputs. 
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByOriginIdStnChl(long orid, DataStnChl sc) {
       String whereString = "WHERE " + keyColumn + " IN ( SELECT " + keyColumn +
                         " FROM " + assocTableOrigin + " WHERE ORID = " + orid + " ) AND " +
                         sc.toStringSQLWhereCondition();
        return getRowsEquals(whereString);
    }

/** Returns an array satisfying an SQL table query WHERE the specified (key) column is associated with the another column
* whose value match the specified input value in the named association table.
*  "SELECT DISTINCT * FROM AMP WHERE AMPID IN (SELECT AMPID FROM ASSOCAMO WHERE ORID = 12345)" 
*  "SELECT DISTINCT * FROM CODA WHERE COID IN (SELECT COID FROM ASSOCCOM WHERE MAGID = 4321)"
*  "SELECT DISTINCT * FROM ASSOCARO WHERE ARID IN (SELECT ARID FROM ARRIVAL WHERE SUBSOURCE = 'RT1'"
*
* The query table name is that initialized by the implementing class instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    protected Object getRowsByAssoc(String assocColumnName, String assocTableName
        , String assocValueColumnName, long iEqual) {
    String whereString = "WHERE " + assocColumnName + " IN ( SELECT " + assocColumnName  +
             " FROM " + assocTableName + " WHERE " + assocValueColumnName + " = " + iEqual + " )";
    return getRowsEquals(whereString);
    }


/** Returns an array satisfying an SQL table query WHERE the specified key column is associated with a column
* in an associated table whose values correspond to ORIGIN ids associated with the specified input EVENT id (evid).
* "SELECT DISTINCT * FROM AMP WHERE AMPID IN (SELECT AMPID FROM ASSOCAMO WHERE ORID IN (SELECT ORID FROM ORIGIN WHERE EVID=123))".
*
* The query table name is that initialized by the implementing class instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    protected Object getRowsByEvent(String keyColumnName, String assocTableName,
                String assocTableKeyColumnName, long evid) {
    String whereString = "WHERE " + keyColumnName +
        " IN ( SELECT " + keyColumnName + " FROM " + assocTableName + " WHERE " + assocTableKeyColumnName +
        " IN ( SELECT ORID FROM ORIGIN WHERE EVID = " + evid + " ) )";
    return getRowsEquals(whereString);
    }

/** Returns an array satisfying an SQL table query WHERE the specified key column is associated with a column
* in an associated table which has the value of the preferred id associated with the input EVENT id (evid) argument.
*
*  "SELECT DISTINCT * FROM AMP WHERE AMPID IN (SELECT AMPID FROM ASSOCAMO WHERE WHERE ORID IN
*               (SELECT PREFOR FROM EVENT WHERE EVID = 1234))".
*
*  "SELECT DISTINCT * FROM AMP WHERE AMPID IN (SELECT AMPID FROM ASSOCAMM WHERE WHERE MAGID IN
*               (SELECT PREFMAG FROM EVENT WHERE EVID = 1234))".
*
* The query table name is that initialized by the implementing class instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    protected Object getRowsByEventPrefId(String keyColumnName, String assocTableName,
                String assocTableKeyColumnName, String EventTablePrefIdColumnName,  long evid) {
    String whereString = "WHERE " + keyColumnName +
        " IN ( SELECT " + keyColumnName + " FROM " + assocTableName + " WHERE " + assocTableKeyColumnName +
        " IN ( SELECT " + EventTablePrefIdColumnName + " FROM EVENT WHERE EVID = " + evid + " ) )";
    return  getRowsEquals(whereString);
    }

// Database modification methods
/*
* Deletes rows from the database table defined by getTableName() associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByEvent(long evid) {
    return ExecuteSQL.deleteRowsByEvent(connDB, getTableName(), keyColumn, assocTableOrigin, "ORID", evid);
    }

* Deletes rows associated with the specified origin id (orid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByOrigin(long orid) {
    return ExecuteSQL.deleteRowsByAssoc(connDB, getTableName(), keyColumn, assocTableOrigin, "ORID", orid);
    }

* Deletes rows associated with the specified event id (evid) and station channel data input arguments.
* Returns number of rows deleted for specified input. A return value of -1 indicates an error condition.
* @see DataStnChl
    public int deleteRowsByEventStnChl(long evid, DataStnChl sc) {
    return ExecuteSQL.deleteRowsByEventStnChl(connDB, getTableName(), keyColumn, assocTableOrigin, "ORID", evid, sc);
    }

* Deletes rows associated with the specified origin id (orid) and station channel data input arguments.
* Returns number of rows deleted for specified input. A return value of -1 indicates an error condition.
* @see DataStnChl
    public int deleteRowsByOriginIdStnChl(long orid, DataStnChl sc) {
    return ExecuteSQL.deleteRowsByAssocStnChl(connDB, getTableName(), keyColumn, assocTableOrigin, "ORID", orid, sc);
    }
*/
}

package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see org.trinet.jasi.StaCorrections
*/
public interface TableRowStaCorrections extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "STACORRECTIONS";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  13;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  STACORRECTIONS table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  STACORRECTIONS table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  STACORRECTIONS table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 2;

/**  STACORRECTIONS table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 3;

/**  STACORRECTIONS table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 4;

/**  STACORRECTIONS table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 5;

/**  STACORRECTIONS table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 6;

/**  STACORRECTIONS table "corr" column data object offset in collection stored by implementing class.
*/
    public static final int CORR = 7;

/**  STACORRECTIONS table "corr_flag" column data object offset in collection stored by implementing class.
*/
    public static final int CORR_FLAG = 8;

/**  STACORRECTIONS table "corr_type" column data object offset in collection stored by implementing class.
*/
    public static final int CORR_TYPE = 9;

/**  STACORRECTIONS table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 10;

/**  STACORRECTIONS table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 11;

/**  STACORRECTIONS table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 12;

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,SEEDCHAN,CHANNEL,CHANNELSRC,LOCATION,AUTH,CORR,CORR_FLAG,CORR_TYPE,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "STACORRECTIONS.NET,STACORRECTIONS.STA,STACORRECTIONS.SEEDCHAN,STACORRECTIONS.CHANNEL,STACORRECTIONS.CHANNELSRC,STACORRECTIONS.LOCATION,STACORRECTIONS.AUTH,STACORRECTIONS.CORR,STACORRECTIONS.CORR_FLAG,STACORRECTIONS.CORR_TYPE,STACORRECTIONS.ONDATE,STACORRECTIONS.OFFDATE,STACORRECTIONS.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "SEEDCHAN", "CHANNEL", "CHANNELSRC", 
	"LOCATION", "AUTH", "CORR", "CORR_FLAG", "CORR_TYPE", 
	"ONDATE", "OFFDATE", "LDDATE" 
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, true, true, 
	false, true, false, true, false, 
	false, false, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATASTRING, DATADOUBLE, DATASTRING, DATASTRING, 
	DATADATE, DATADATE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1, 2, 5, 9, 10};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0,
        0, 0, 4, 0, 0,
        0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	3, 6, 3, 3, 8,
        2, 6, 7, 3, 3,
        10, 10, 10
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null,
        "'01'", null, null, null, null, 
	null, null, null, null, null,
        null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))" 
    };
}

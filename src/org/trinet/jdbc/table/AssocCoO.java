package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
/**
 * Class to access and manipulate the AssocCoO (association) table.
 */
public class AssocCoO extends OriginAssocTableRow implements TableRowAssocCoO {

    public AssocCoO () {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public AssocCoO(AssocCoO object) {
	this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public AssocCoO(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public AssocCoO(long orid, long coid, String auth) {
	this();
        fields.set(ORID, new DataLong(orid));
        fields.set(COID, new DataLong(coid));
        fields.set(AUTH, new DataString(auth));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public AssocCoO(long orid, long coid, String auth, double delta, double seaz) {
	this(orid, coid, auth);
        fields.set(DELTA, new DataDouble(delta));
        fields.set(SEAZ, new DataDouble(seaz));
    }
}

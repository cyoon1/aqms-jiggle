package org.trinet.jdbc.table;
import java.sql.*;
import java.util.Date;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;

// DDG 6/25/2003

/** Constructor uses static data members defined by the TableRowRequestCard interface
 * to initialize the base class with the parameters necessary to describe the schema
 * table named by the interface String parameter DB_TABLE_NAME.
 *
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/

public class RequestCard extends DataTableRow implements TableRowRequestCard {
    public RequestCard() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Copy constructor invokes the default constructor, then clones all the DataObject
 * classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/
    public RequestCard(RequestCard object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object
 * to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public RequestCard(Connection conn) {
        this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public RequestCard(String net, String sta, String seedchan, String loc,
                       String staauth, String channel,
                       java.util.Date onDate, java.util.Date offDate, // changed from java.sql.Date to parent superclass type - aww 2008/02/12
                       String requestType) {
        this();
        fields.set(NET, new DataString(net));
        fields.set(STA, new DataString(sta));
        fields.set(SEEDCHAN, new DataString(seedchan));
        fields.set(LOCATION, new DataString(loc));
        fields.set(STAAUTH, new DataString(staauth));
        fields.set(CHANNEL, new DataString(channel));
        fields.set(DATETIME_ON, new DataDate(onDate));
        fields.set(DATETIME_OFF, new DataDate(offDate));
        fields.set(REQUEST_TYPE, new DataString(requestType));
        valueUpdate = true;
        valueNull = false;
    }

    /** Returns an array where each element contains the data from a single table
     * row parsed from an SQL query WHERE the DATETIME_ON column is BETWEEN the
     * specified input times and the SUBSOURCE column has the specified input value.
    * A return value of null indicates no data or an error condition.
    */
// Perhaps needs to qualify AUTH too?
        public Object getRowsByType(String type) {
            if (NullValueDb.isEmpty(type)) return null;

            return getRowsEquals("WHERE Request_Type like " + StringSQL.valueOf(type) +
                            " Order by Priority, Retry");
    }
}


package org.trinet.jdbc.table;
import java.sql.*;
import java.util.*;
import org.trinet.jdbc.*;
import oracle.jdbc.driver.*;
/** Class contains static methods to enable JDBC interactions with tables of the NCEDC schema over a specified JDBC connection. 
* Key methods are:<P>
* rowQuery(...)             returns the ResultSet of an SQL query.<BR>
* rowUpdate(...)            executes SQL modification statements (UPDATE, DELETE, INSERT).<BR>
* lockTableForUpdate(...)   locks the specified table in Oracle ROW EXCLUSIVE MODE for updates.<BR>
* setSelectForUpdate(...)   flags whether an "FOR UPDATE" clause is appended to SQL query string.<BR>
* setPrintStringSQL(...)    flags whether SQL strings are printed to System.out before execution.<BR>
* getRowCountXXX(...)       gets a row count.<BR>
* getEventXXXId()           returns the preferred id.<BR>
* getOriginXXXId()<BR>
* parseResults(...)         returns an object array containing Object arrays parsed from a JDBC ResultSet.<BR>
* parseAllRows(...)         returns an object array of DataTableRow objects parsed from a JDBC ResultSet.<BR>
* parseOneRow(...)          returns an DataTableRow object parsed from the current row of a JDBC ResultSet.<BR>
* @ see #rowQuery(Statement, String)
* @ see #rowUpdate(Connection, String).
* @ see #lockTableForUpdate(Connection, String, String)
* @ see #setPrintStringSQL(boolean)
* @ see #setSelectForUpdate(boolean)
* @ see #parseResults()
* @ see #parseAllRows()
* @ see #parseOneRow()
*/
public class ExecuteSQL {

/** Flag to indicate printing to System.out any SQL string before JDBC method execution.
*/
    private static boolean printStringSQLFlag = false;
  //    private static boolean printStringSQLFlag = true;

/** Flag for rowQuery(...) method to use the "FOR UPDATE" clause appended to the SQL query.
*/
    private static boolean selectForUpdateTableFlag = false;
/** String value appended to SQL query in rowQuery(...) method when isSelectForUpdate == true.
*/
    private static final String SELECT_FOR_UPDATE = " FOR UPDATE";

/** Enables the SQL string print option for query and update methods.
* @see #isPrintStringSQL()
* @see #rowQuery(Statement, String)
* @see #rowUpdate(Connection, String)
*/
    public static void setPrintStringSQL(boolean value) {
	printStringSQLFlag = value;
	return;
    }

/** Checks setting of SQL string print option for rowQuery and rowUpdate methods.
* @see #setPrintStringSQL(boolean)
*/
    public static boolean isPrintStringSQL() {
	return printStringSQLFlag;
    }

/** Enables FOR UPDATE option for all table queries with rowQuery(...) method.
* @see #isSelectForUpdate()
*/
    public static void setSelectForUpdate(boolean value) {
	selectForUpdateTableFlag = value;
	return;
    }

/** Checks status setting of "FOR UPDATE" option for table queries with any of rowQuery(...) method.
* @see #setSelectForUpdate(boolean)
*/
    public static boolean isSelectForUpdate() {
	return selectForUpdateTableFlag;
    }

/** Returns the count of database table rows satisfying: "SELECT  COUNT( countExpression ) WHERE whereCondition". 
* If countExpression string is null, returns COUNT(*), the whereCondition input string can be null.
* The countExpression string is usually "*" or a combination of "DISTINCT or ALL" and a column name.
* Except for "*" expression, null column values are not included in the count unless the NVL function is used
* in the COUNT expression argument to specify an alternative value string.
* Thus, getRowCount("*", null) and getRowCount("*", "") both return a count of all rows in the table.
* The queried table name is that initialized by the constructor of this object instance.<P>
* Returns -1 if an error occurs while executing the JDBC query.
*/
    public static int getRowCount(Connection conn, String tableName, String countExpression, String whereCondition) {
	ResultSet rs = null;
	int nrows = 0;
	if (conn == null) {
	    System.err.println("ExecuteSQL getRowCount: JDBC connection null;" +
		" application must first instantiate a connection class" +
		"; see DataSource");
	    return -1;
	}
// Weird conditional operator, a demo:
	String tmpExprString = (countExpression != null) ? countExpression : "*";
	String tmpWhereString = (NullValueDb.isEmpty(whereCondition)) ? null :
		(whereCondition.toUpperCase().indexOf("WHERE") >= 0) ? whereCondition : "WHERE " + whereCondition;

	Statement sm = null;
	try {
	    sm = conn.createStatement();
        String query = "SELECT COUNT( " + tmpExprString + " ) FROM " + tableName + " " + tmpWhereString; 
	    if (printStringSQLFlag) System.out.println("ExecuteSQL.getRowCount(...) SQL string:\n" + query);
	    rs = sm.executeQuery(query);
	    rs.next();
	    nrows = rs.getInt(1);
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL: getCount SQLException");
	    SQLExceptionHandler.prtSQLException(ex);
	    nrows = -1;
	}
        finally {
	  try {
	    if (rs != null) rs.close();
	    if (sm != null) sm.close();
	  }
	  catch (SQLException exc) {
	    SQLExceptionHandler.prtSQLException(exc);
	  }
        }
	return nrows;
    }

/** Returns the count of database table rows satisfying: "SELECT COUNT(*) FROM tableName". 
*/
    public static int getRowCount(Connection conn, String tableName) {
        return getRowCount(conn, tableName, "*", "");
    }

/** Returns the preferred ORIGIN table orid for the specified event id.
*/
    public static long getEventPreforId(Connection conn, long evid) {
	return getPrefId(conn , "EVENT", "PREFOR", "EVID", evid);
    }
/** Returns the preferred NETMAG table magid for the specified event id.
*/
    public static long getEventPrefmagId(Connection conn, long evid) {
	return getPrefId(conn , "EVENT", "PREFMAG", "EVID", evid);
    }
/** Returns the preferred MEC table mecid for the specified event id.
*/
    public static long getEventPrefmecId(Connection conn, long evid) {
	return getPrefId(conn , "EVENT", "PREFMEC", "EVID", evid);
    }
/** Returns the preferred NETMAG table magid for the specified origin id.
*/
    public static long getOriginPrefmagId(Connection conn, long orid) {
	return getPrefId(conn , "ORIGIN", "PREFMAG", "ORID", orid);
    }
/** Returns the preferred MEC table mecid for the specified origin id.
*/
    public static long getOriginPrefmecId(Connection conn, long orid) {
	return getPrefId(conn , "ORIGIN", "PREFMEC", "ORID", orid);
    }

/** Returns the value of SELECT prefColumnName FROM tableName WHERE keyColumnName = id.
* PrefColumnName refers to the preferred foreign key column such as EVENT.PREFOR == ORIGIN.ORID.
* Creates a JDBC statement for the specified input connection and executes an SQL query.
* A return value of -1 indicates an JDBC error.
*/
    static long getPrefId(Connection conn, String tableName, String prefColumnName, String keyColumnName, long id) {
	ResultSet rs = null;
	long prefid = 0;
	if (conn == null) {
	    System.err.println("ExecuteSQL getPreferredOriginId: JDBC connection null;" +
		" application must first instantiate a connection class" +
		"; see DataSource");
	    return -1l;
	}
	String sql = "SELECT " + prefColumnName + " FROM " + tableName + " WHERE " + keyColumnName + " = " + id;
	Statement sm = null;
	try {
	    sm = conn.createStatement();
	    rs = sm.executeQuery(sql);
	    rs.next();
	    prefid = rs.getInt(1);
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL: getCount SQLException");
	    SQLExceptionHandler.prtSQLException(ex);
	    prefid = -1;
	}
        finally {
	  try {
	    if (rs != null) rs.close();
	    if (sm != null) sm.close();
	  }
	  catch (SQLException exc) {
	    SQLExceptionHandler.prtSQLException(exc);
	  }
        }
	return prefid;
    }

/** Returns ResultSet handle returned by executing JDBC Statement.executeQuery(...).
* The query text is specified by the input string argument.
* Appends "FOR UPDATE" to the query if isSelectForUpdate() == true.<P>
* Method is wrapper invoking rowQuery(Statement, String, boolean) where
* the boolean can be set with the method setPrintStringSQL(), default value == false.
* Prints the SQL string to System.out before execution, if isPrintStringSQL() == true.<P>
* Returns null if the input JDBC Statement argument is null or an error occurs while executing.
* @see #isPrintStringSQL()
* @see #setPrintStringSQL(boolean)
*/
    public static ResultSet rowQuery(Statement sm, String sql) {
	return rowQuery(sm, sql, printStringSQLFlag);
    }

/** Returns ResultSet handle returned by executing JDBC Statement.executeQuery(...).
* The query text is specified by the input string argument.
* Appends "FOR UPDATE" to the query if isSelectForUpdate() == true.
* Prints the SQL string to System.out before execution, if print == true.<P>
* Returns null if the input JDBC Statement argument is null or an error occurs while executing.
*/
    public static ResultSet rowQuery(Statement sm, String sql, boolean print) {
	ResultSet rs = null;
	if (sm== null) {
	    System.err.println("ExecuteSQL rowQuery: JDBC connection statement null;" +
		" application must first instantiate a connection class and createStatement()" +
		"; see DataSource");
	    return null;
	}
	String query = sql;
	if (isSelectForUpdate()) {
	    if (query.toUpperCase().indexOf(SELECT_FOR_UPDATE) < 0)  query = sql + SELECT_FOR_UPDATE;
/*
	    try {
	        ((OracleStatement) sm).setRowPrefetch(0);
	    }
	    catch (SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
	    }
*/
	} 
	try {
//	    System.out.println("TEST of ExecuteSQL.rowQuery(...) SQL string:\n" + query);
	    if (print) System.out.println("ExecuteSQL.rowQuery(...) SQL string:\n" + query);
	    rs = sm.executeQuery(query); 
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL: rowQuery executeQuery statement SQLException");
	    System.err.println("ExecuteSQL.rowQuery SQL string:\n" + query);
	    SQLExceptionHandler.prtSQLException(ex);
	    return null;
	}
	return rs;
    }

// Methods to modify database table rows 
/** Executes JDBC SQL statement to lock named input table in mode specified in input string.<BR>
* Returns true if successful, otherwise false.
*/
    public static boolean lockTableForUpdate(Connection conn, String tableName, String mode) {
        String lockTableString = "LOCK TABLE " + tableName + " " + mode;
	boolean retVal = true;
	if (conn == null) {
	    System.err.println("ExecuteSQL lockTableForUpdate: JDBC connection input argument null;" +
		" application must first instantiate a connection class" +
		"; see DataSource");
	    return false;
	}
	Statement sm = null;
	try {
	    sm = conn.createStatement();
	    //	    retVal = sm.execute(lockTableString); 
	    boolean stat = sm.execute(lockTableString); 

	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL: lockTableForUpdate execute lock statement SQLException");
	    SQLExceptionHandler.prtSQLException(ex);
	    retVal =  false;
	}
	finally {
	    try {
                if (sm != null) sm.close();
            }
	    catch (SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
	    }
	}

	return retVal;
    }

/** Executes JDBC update statement for SQL statements of type INSERT, UPDATE, DELETE.<BR>
* Method is wrapper invoking rowUpdate(Connection, String, boolean), where
* the boolean can be set by the method setPrintStringSQL(), its default value == false.
* Prints SQL string to System.out before execution, if isPrintStringSQL == true.<P>
* Returns number of rows modified, a return value of -1 indicates an error condition.
* @see #isPrintStringSQL()
* @see #setPrintStringSQL(boolean)
* @see #lockTableForUpdate(Connection, String, String)
*/
    public static int rowUpdate(Connection conn, String sql) {
	return rowUpdate(conn, sql, printStringSQLFlag);
    }

/** Executes JDBC update statement for SQL statements of type INSERT, UPDATE, DELETE.<BR>
* Prints SQL string to System.out before execution, if print == true.<P>
* Returns number of rows modified, a return value of -1 indicates an error condition.
* @see #lockTableForUpdate(Connection, String, String)
*/
    public static int rowUpdate(Connection conn, String sql, boolean print) {
	int nrows = 0;
	if (conn == null) {
	    System.err.println("ExecuteSQL rowUpdate: JDBC connection null;" +
		" application must first instantiate a connection class" + 
		" (see DataSource)");
	    return -1;
	}
	Statement sm = null;
	try {
	    sm = conn.createStatement();
	    if (print) System.out.println("ExecuteSQL.rowUpdate SQL string:\n" + sql);
	    nrows = sm.executeUpdate(sql);
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL: rowUpdate executeUpdate statement SQLException");
	    System.err.println("ExecuteSQL.rowUpdate SQL string:\n" + sql);
	    SQLExceptionHandler.prtSQLException(ex);
	    nrows = -1;
	} 
	finally {
	    try { if (sm != null) sm.close(); }
	    catch (SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
	    }
	}
	return nrows;
    }

/** Returns an Object array whose elements are also Object []'s resulting from the parsing each row of the input ResultSet. 
* Method iterates through the ResultSet object and closes the ResultSet upon reaching the end of the results.
* Returned Object array can be used as input to org.trinet.jdbc.table.DataTableRow methods to extract pertinent data.<P>
* Row parsing begins at the column offset specified by the offset input argument. Specify 0 to begin at the first column.
* Row parsing stops after parsing the number of objects specified by the length input argument.
* Specify a length = 0 to retrieve all of the column objects from the input offset to the end of the ResultSet row.<P>
*<PRE>
* Returns null:
*     if the input ResultSet is null.
*     if the input offset or length are < 0.
*     if offset+length > ResultSet column count.
*     if an SQLException occurs during processing of the ResultSet.
*</PRE><P>
* <em>For Oracle an database: NUMBER => java.math.BigDecimal, VARCHAR => java.lang.String, and DATE => java.sql.Timestamp.</em>
* @see org.trinet.jdbc.table.DataTableRow#objectToDataObject(Object [],int)
* @see org.trinet.jdbc.table.DataTableRow#dataObjectToFields(DataObject[])
*/
    public static Object [] parseResults(ResultSet rs, int offset, int length) {
// Oracle NUMBER is returned as BigDecimal, VARCHAR is returned as String, and DATE is returned as java.sql.Timestamp.
	if (rs == null) {
	    System.err.println("ExecuteSQL parseResults: null ResultSet input argument.");
	    return null;
	}
	if (offset < 0) {
	    System.err.println("ExecuteSQL parseResults: input offset < 0");
	    return null;
	}
	if (length < 0) {
	    System.err.println("ExecuteSQL parseResults: input length < 0");
	    return null;
	}
	int maxColumns;
	try {
	    ResultSetMetaData rsmd = rs.getMetaData();
	    maxColumns = rsmd.getColumnCount();
	    if (maxColumns < offset + length) {
		System.err.println("ExecuteSQL parseResults: ResultSet columns < offset+length");
		return null;
	    }
	}
	catch (SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
		System.err.println("ExecuteSQL parseResults: cannot get ResultSetMetaData");
		return null;
	}
	int arrayLength;
	if (length <= 0) arrayLength = maxColumns - offset;
	else arrayLength = length;
	int nrows = 0;
	Object [] row = new Object[arrayLength];
	Vector vtr = new Vector();
	try {
	    while ( rs.next() ) {
		for (int index = 0; index < arrayLength; index++) {
		    try {
			row[index] = rs.getObject(index + 1 + offset); // ResultSet indexes start at 1, but arrays start at 0.
		    }
		    catch ( SQLException ex) {
			SQLExceptionHandler.prtSQLException(ex);
			System.err.println("ExecuteSQL parseResults: error at column index value: " + index);
			return null;
		    }
		    catch ( IndexOutOfBoundsException ex) {
	    		ex.printStackTrace();
			return null;
		    }
		}
		vtr.add(row);
		nrows++;
	    }
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL parseResults: ResultSet.next() SQLException");
	    SQLExceptionHandler.prtSQLException(ex);
	    return null;
	}
	try {
	    rs.close();
	}
	catch (SQLException ex) {
	    System.err.println("ExecuteSQL parseResults: ResultSet.close() SQLException");
	    SQLExceptionHandler.prtSQLException(ex);
	    return null;
	}
	if ( vtr.size() > 0) return vtr.toArray();
	else return null;
    }

/** Returns an array object derived from parsing all rows of the input ResultSet into instances of the specified input Class. 
* The returned object can be cast to an array of the Class type specified by the input Class argument.
* Starts row parse at the column at the specified offset. After parsing all rows, the the input ResultSet is closed.
* Sets the connection object of the DataTableRow to the specified Connection object, if not null, else it is set to null.
* Sets the mutability state of the returned DataTableRow instance to false if isSelectForUpdate() == false.
* Sets the mutability state for the DataObject in any declared table key column to false.
* Returns null if input ResultSet is null or input offset < 0.
* Returns null if input Class type is not an extension of org.trinet.jdbc.table.DataTableRow.
* Returns null if input offset+table_row_columns (i.e. TableRowXXX.MAX_FIELDS) > number of columns returned in the ResultSet row.
* Returns null if an SQLException occurs during processing.
* @see #parseOneRow(ResultSet, int, Class, Connection)
* @see org.trinet.jdbc.table.DataTableRow
* @see org.trinet.jdbc.table.DataTableRow#parseOneRow(ResultSetDb, int)
*/
    public static Object parseAllRows(ResultSet rs, int offset, Class type, Connection conn) {
        Vector vtr = new Vector();
        try {
            while ( rs.next() ) {
		DataTableRow dtr = parseOneRow(rs, offset, type, conn);
		if (dtr == null) return null;
                vtr.add(dtr);
            }
        }
        catch (SQLException ex) {
            System.err.println("ExecuteSQL: parseResults ResultSet.next() SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            return null;
        }
        try {
            rs.close();
        }
        catch (SQLException ex) {
            System.err.println("ExecuteSQL: parseResults ResultSet.close() SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            return null;
        }
        return recast(vtr, type);
    }

/** Returns a DataTableRow object derived from parsing one row of the input ResultSet into instance of specified input Class. 
* Starts row parse at the column at the specified offset. After row parse, does no other JDBC operations on ResultSet.
* Sets the connection object of the DataTableRow to the specified Connection object, if not null, else it is set to null.
* Sets the mutability state of the returned DataTableRow instance to false if isSelectForUpdate() == false.
* Sets the mutability state for the DataObject in any declared table key column to false.
* Returns null if input ResultSet is null or input offset < 0.
* Returns null if input Class type is not an extension of org.trinet.jdbc.table.DataTableRow.
* Returns null if input offset+table_row_columns (i.e. TableRowXXX.MAX_FIELDS) > number of columns returned in the ResultSet row.
* Returns null if an SQLException occurs during processing.
* @see org.trinet.jdbc.table.DataTableRow
* @see org.trinet.jdbc.table.DataTableRow#parseOneRow(ResultSetDb, int)
*/
    public static DataTableRow parseOneRow(ResultSet rs, int offset, Class type, Connection conn) {
	if (rs == null ) {
	    System.err.println("ExecuteSQL parseOneRow: null ResultSet input argument.");
	    return null;
	}
	if (offset < 0) {
	    System.err.println("ExecuteSQL parseOneRow: invalid offset input argument.");
	    return null;
	}
	if (! DataTableRow.class.isAssignableFrom(type)) {
	    System.err.println("ExecuteSQL parseOneRow: invalid class input argument; must be able to cast as DataTableRow.");
	    return null;
	}
	DataTableRow row = null;
	try {
	    row = (DataTableRow) type.newInstance(); // create new instance of tableName class
	}
	catch (IllegalAccessException ex) {
	    System.err.println("ExecuteSQL parseOneRow: class or initializer not accessible: " +
	    type.getName());
	    return null;
	}
	catch (InstantiationException ex) {
	    System.err.println("ExecuteSQL parseOneRow: cannot instantiate class check type: " +
	    type.getName());
	    return null;
	}
	try {
	    ResultSetMetaData rsmd = rs.getMetaData(); 
	    if (rsmd.getColumnCount() < offset + row.getMaxFields() ) {
		System.err.println("ExecuteSQL parseOneRow: offset+totalRowColumns exceeds the total columns in ResultSet.");
		return null;
	    }
	}
	catch (SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
		System.err.println("ExecuteSQL parseOneRow: cannot get ResultSetMetaData");
		return null;
	}
	if (conn != null) row.setConnection(conn);

// Remember ResultSet column indexes start at 1, but arrays start at 0.
	for (int index = 0; index < row.getMaxFields(); index++) {
	    try {
		row.setValue(index, rs.getObject(index+1+offset) );
	    }
	    catch ( SQLException ex) {
		SQLExceptionHandler.prtSQLException(ex);
		System.err.println("ExecuteSQL parseOneRow at column index value: " + index + " columnName: " 
		    + row.getFieldNames()[index] + " fieldClass: " + row.DATA_CLASSES[row.getFieldClassIds()[index]]
		    + " offset: " + offset);
		return null;
	    }
	}
	for (int index = 0; index < row.getKeyIndex().length; index++) {
	    row.setMutableValue(row.getKeyIndex()[index], false); // key columns are not modifiable
	}
	if (! isSelectForUpdate()) row.setMutable(false); // read-only mode, do not modify row contents
	return row;
    }

/** Returns a new array of input Class type whose elements are the respective elements of the input List object.
* Returns null if the List object is null or empty.
*/
    public static Object recast(List list, Class type) {
        Object retVal = null;
        if (list == null) return null;
        if ( list.size() > 0) {
            retVal = java.lang.reflect.Array.newInstance(type, list.size());
            for (int index = 0; index < list.size(); index++) {
                java.lang.reflect.Array.set(retVal, index,  list.get(index));
            }
        }
        return retVal;
    }

}

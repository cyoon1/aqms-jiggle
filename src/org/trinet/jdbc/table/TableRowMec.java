package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Mec
*/
public interface TableRowMec extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "MEC";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  55;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "MECSEQ";


/**  MEC table "mecid" column data object offset in collection stored by implementing class.
*/
    public static final int MECID = 0;

/**  MEC table "oridin" column data object offset in collection stored by implementing class.
*/
    public static final int ORIDIN = 1;

/**  MEC table "oridout" column data object offset in collection stored by implementing class.
*/
    public static final int ORIDOUT = 2;

/**  MEC table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 3;

/**  MEC table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 4;

/**  MEC table "mechtype" column data object offset in collection stored by implementing class.
*/
    public static final int MECHTYPE = 5;

/**  MEC table "mecalgo" column data object offset in collection stored by implementing class.
*/
    public static final int MECALGO = 6;

/**  MEC table "scalar" column data object offset in collection stored by implementing class.
*/
    public static final int SCALAR = 7;

/**  MEC table "erscalar" column data object offset in collection stored by implementing class.
*/
    public static final int ERSCALAR = 8;

/**  MEC table "tft" column data object offset in collection stored by implementing class.
*/
    public static final int TFT = 9;

/**  MEC table "tfd" column data object offset in collection stored by implementing class.
*/
    public static final int TFD = 10;

/**  MEC table "mxx" column data object offset in collection stored by implementing class.
*/
    public static final int MXX = 11;

/**  MEC table "myy" column data object offset in collection stored by implementing class.
*/
    public static final int MYY = 12;

/**  MEC table "mzz" column data object offset in collection stored by implementing class.
*/
    public static final int MZZ = 13;

/**  MEC table "mxy" column data object offset in collection stored by implementing class.
*/
    public static final int MXY = 14;

/**  MEC table "mxz" column data object offset in collection stored by implementing class.
*/
    public static final int MXZ = 15;

/**  MEC table "myz" column data object offset in collection stored by implementing class.
*/
    public static final int MYZ = 16;

/**  MEC table "smxx" column data object offset in collection stored by implementing class.
*/
    public static final int SMXX = 17;

/**  MEC table "smyy" column data object offset in collection stored by implementing class.
*/
    public static final int SMYY = 18;

/**  MEC table "smzz" column data object offset in collection stored by implementing class.
*/
    public static final int SMZZ = 19;

/**  MEC table "smxy" column data object offset in collection stored by implementing class.
*/
    public static final int SMXY = 20;

/**  MEC table "smxz" column data object offset in collection stored by implementing class.
*/
    public static final int SMXZ = 21;

/**  MEC table "smyz" column data object offset in collection stored by implementing class.
*/
    public static final int SMYZ = 22;

/**  MEC table "srcduration" column data object offset in collection stored by implementing class.
*/
    public static final int SRCDURATION = 23;

/**  MEC table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 24;

/**  MEC table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 25;

/**  MEC table "strike1" column data object offset in collection stored by implementing class.
*/
    public static final int STRIKE1 = 26;

/**  MEC table "dip1" column data object offset in collection stored by implementing class.
*/
    public static final int DIP1 = 27;

/**  MEC table "rake1" column data object offset in collection stored by implementing class.
*/
    public static final int RAKE1 = 28;

/**  MEC table "strike2" column data object offset in collection stored by implementing class.
*/
    public static final int STRIKE2 = 29;

/**  MEC table "dip2" column data object offset in collection stored by implementing class.
*/
    public static final int DIP2 = 30;

/**  MEC table "rake2" column data object offset in collection stored by implementing class.
*/
    public static final int RAKE2 = 31;

/**  MEC table "unstrike1" column data object offset in collection stored by implementing class.
*/
    public static final int UNSTRIKE1 = 32;

/**  MEC table "undip1" column data object offset in collection stored by implementing class.
*/
    public static final int UNDIP1 = 33;

/**  MEC table "unrake1" column data object offset in collection stored by implementing class.
*/
    public static final int UNRAKE1 = 34;

/**  MEC table "unstrike2" column data object offset in collection stored by implementing class.
*/
    public static final int UNSTRIKE2 = 35;

/**  MEC table "undip2" column data object offset in collection stored by implementing class.
*/
    public static final int UNDIP2 = 36;

/**  MEC table "unrake2" column data object offset in collection stored by implementing class.
*/
    public static final int UNRAKE2 = 37;

/**  MEC table "eigenp" column data object offset in collection stored by implementing class.
*/
    public static final int EIGENP = 38;

/**  MEC table "plungep" column data object offset in collection stored by implementing class.
*/
    public static final int PLUNGEP = 39;

/**  MEC table "strikep" column data object offset in collection stored by implementing class.
*/
    public static final int STRIKEP = 40;

/**  MEC table "eigenn" column data object offset in collection stored by implementing class.
*/
    public static final int EIGENN = 41;

/**  MEC table "plungen" column data object offset in collection stored by implementing class.
*/
    public static final int PLUNGEN = 42;

/**  MEC table "striken" column data object offset in collection stored by implementing class.
*/
    public static final int STRIKEN = 43;

/**  MEC table "eigent" column data object offset in collection stored by implementing class.
*/
    public static final int EIGENT = 44;

/**  MEC table "plunget" column data object offset in collection stored by implementing class.
*/
    public static final int PLUNGET = 45;

/**  MEC table "striket" column data object offset in collection stored by implementing class.
*/
    public static final int STRIKET = 46;

/**  MEC table "nsta" column data object offset in collection stored by implementing class.
*/
    public static final int NSTA = 47;

/**  MEC table "pvr" column data object offset in collection stored by implementing class.
*/
    public static final int PVR = 48;

/**  MEC table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 49;

/**  MEC table "pdc" column data object offset in collection stored by implementing class.
*/
    public static final int PDC = 50;

/**  MEC table "pclvd" column data object offset in collection stored by implementing class.
*/
    public static final int PCLVD = 51;

/**  MEC table "piso" column data object offset in collection stored by implementing class.
*/
    public static final int PISO = 52;

/**  MEC table "datetime" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME = 53;

/**  MEC table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 54;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES = // for db to UTC conversion -aww 2008/02/12
   "MECID,ORIDIN,ORIDOUT,MAGID,COMMID,MECHTYPE,MECALGO,SCALAR,ERSCALAR,TFT,TFD,MXX,MYY,MZZ,MXY,MXZ,MYZ,SMXX,SMYY,SMZZ,SMXY,SMXZ,SMYZ,SRCDURATION,AUTH,SUBSOURCE,STRIKE1,DIP1,RAKE1,STRIKE2,DIP2,RAKE2,UNSTRIKE1,UNDIP1,UNRAKE1,UNSTRIKE2,UNDIP2,UNRAKE2,EIGENP,PLUNGEP,STRIKEP,EIGENN,PLUNGEN,STRIKEN,EIGENT,PLUNGET,STRIKET,NSTA,PVR,QUALITY,PDC,PCLVD,PISO,truetime.getEpoch(DATETIME,'UTC'),LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
    "MEC.MECID,MEC.ORIDIN,MEC.ORIDOUT,MEC.MAGID,MEC.COMMID,MEC.MECHTYPE,MEC.MECALGO,MEC.SCALAR,MEC.ERSCALAR,MEC.TFT,MEC.TFD,MEC.MXX,MEC.MYY,MEC.MZZ,MEC.MXY,MEC.MXZ,MEC.MYZ,MEC.SMXX,MEC.SMYY,MEC.SMZZ,MEC.SMXY,MEC.SMXZ,MEC.SMYZ,MEC.SRCDURATION,MEC.AUTH,MEC.SUBSOURCE,MEC.STRIKE1,MEC.DIP1,MEC.RAKE1,MEC.STRIKE2,MEC.DIP2,MEC.RAKE2,MEC.UNSTRIKE1,MEC.UNDIP1,MEC.UNRAKE1,MEC.UNSTRIKE2,MEC.UNDIP2,MEC.UNRAKE2,MEC.EIGENP,MEC.PLUNGEP,MEC.STRIKEP,MEC.EIGENN,MEC.PLUNGEN,MEC.STRIKEN,MEC.EIGENT,MEC.PLUNGET,MEC.STRIKET,MEC.NSTA,MEC.PVR,MEC.QUALITY,MEC.PDC,MEC.PCLVD,MEC.PISO,truetime.getEpoch(MEC.DATETIME,'UTC'),MEC.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"MECID", "ORIDIN", "ORIDOUT", "MAGID", "COMMID", 
	"MECHTYPE", "MECALGO", "SCALAR", "ERSCALAR", "TFT", 
	"TFD", "MXX", "MYY", "MZZ", "MXY", 
	"MXZ", "MYZ", "SMXX", "SMYY", "SMZZ", 
	"SMXY", "SMXZ", "SMYZ", "SRCDURATION", "AUTH", 
	"SUBSOURCE", "STRIKE1", "DIP1", "RAKE1", "STRIKE2", 
	"DIP2", "RAKE2", "UNSTRIKE1", "UNDIP1", "UNRAKE1", 
	"UNSTRIKE2", "UNDIP2", "UNRAKE2", "EIGENP", "PLUNGEP", 
	"STRIKEP", "EIGENN", "PLUNGEN", "STRIKEN", "EIGENT", 
	"PLUNGET", "STRIKET", "NSTA", "PVR", "QUALITY", 
	"PDC", "PCLVD", "PISO", "DATETIME", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, false, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, false, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATALONG, DATALONG, 
	DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATASTRING, 
	DATASTRING, DATALONG, DATALONG, DATALONG, DATALONG, 
	DATALONG, DATALONG, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATALONG, 
	DATALONG, DATADOUBLE, DATALONG, DATALONG, DATADOUBLE, 
	DATALONG, DATALONG, DATALONG, DATALONG, DATADOUBLE, 
	DATALONG, DATALONG, DATALONG, DATADOUBLE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 5, 0, 
	0, 5, 0, 0, 5, 0, 0, 0, 0, 1, 0, 0, 0, 10, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 2, 15, 126, 126, 8, 126, 126, 126, 126, 126, 126, 126, 126, 126, 126, 
	126, 126, 126, 6, 15, 8, 3, 3, 4, 3, 2, 4, 6, 5, 6, 6, 5, 6, 7, 2, 
	3, 7, 2, 3, 7, 2, 3, 5, 5, 2, 3, 3, 3, 25, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}

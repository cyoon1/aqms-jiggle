package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;

/**
 * An object corrisponding to the UnassocAmp table of the pseudo-NCEDC schema.
 * It has the same structure as the Amp table.
 * @See: Amp
*/

public class UnassocAmp extends StnChlTableRow implements TableRowUnassocAmp  {
  public UnassocAmp () {
      super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
      initialize();
  }

/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input Object.
*/
  public UnassocAmp(UnassocAmp object) {
      this();
      for (int index = 0; index < MAX_FIELDS; index++) {
          fields.set(index, ((DataObject) object.fields.get(index)).clone());
      }
      this.valueUpdate = object.valueUpdate;
      this.valueNull = object.valueNull;
      this.valueMutable = object.valueMutable;
  }

/** Constructor invokes default constructor, then sets the default connection object to the handle of input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
  public UnassocAmp(Connection conn) {
      this();
      setConnection(conn);
  }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
  public UnassocAmp(long ampid) {
      this();
      fields.set(AMPID, new DataLong(ampid));
      valueNull = false;
      valueUpdate = true;
  }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
  public UnassocAmp(long ampid, double datetime, DataStnChl sc, double amplitude, String units, double wstart, double duration) {
      this(ampid);
      setDataStnChl(sc);
      fields.set(DATETIME, new DataDouble(datetime));
      fields.set(AMPLITUDE, new DataDouble(amplitude));
      fields.set(UNITS, new DataString(units));
      fields.set(WSTART, new DataDouble(wstart));
      fields.set(DURATION, new DataDouble(duration));
  }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
  public UnassocAmp(long ampid, double datetime, DataStnChl sc, double amplitude, String units, double wstart, double duration,
              String iphase, String amptype, String ampmeas) {
      this(ampid, datetime, sc, amplitude, units, wstart, duration);
      fields.set(IPHASE, new DataString(iphase));
      fields.set(AMPTYPE, new DataString(amptype));
      fields.set(AMPMEAS, new DataString(ampmeas));
  }

/** Data string members used in base class method argument lists
*/
  protected void initialize() {
      keyColumn = FIELD_NAMES[KEY_COLUMNS[0]];
      assocTableOrigin = null;
      assocTableNetmag = null;
 }

    /*
     * Returns true if input column field name contains string "DATETIME", false otherwise,
     * subclasses should override this method to add column names whose values are
     * UTC epoch seconds.
     */
    protected boolean isColumnEpochSecsUTC(String columnName) {
        // Added method to allow subclasses handle non-standard names like WSTART, SAVESTART, TIME1 etc. -aww 2010/02/26
        return (columnName.indexOf("DATETIME") >= 0  || columnName.equals("WSTART")); // for UTC - aww 2010/02/26
    }
}

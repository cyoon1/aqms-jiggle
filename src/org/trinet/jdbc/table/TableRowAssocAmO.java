package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocAmO
*/
public interface TableRowAssocAmO extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCAMO";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  9;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCAMO table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 0;

/**  ASSOCAMO table "ampid" column data object offset in collection stored by implementing class.
*/
    public static final int AMPID = 1;

/**  ASSOCAMO table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  ASSOCAMO table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  ASSOCAMO table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  ASSOCAMO table "delta" column data object offset in collection stored by implementing class.
*/
    public static final int DELTA = 5;

/**  ASSOCAMO table "seaz" column data object offset in collection stored by implementing class.
*/
    public static final int SEAZ = 6;

/**  ASSOCAMO table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 7;

/**  ASSOCAMO table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 8;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "ORID,AMPID,COMMID,AUTH,SUBSOURCE,DELTA,SEAZ,RFLAG,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCAMO.ORID,ASSOCAMO.AMPID,ASSOCAMO.COMMID,ASSOCAMO.AUTH,ASSOCAMO.SUBSOURCE,ASSOCAMO.DELTA,ASSOCAMO.SEAZ,ASSOCAMO.RFLAG,ASSOCAMO.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"ORID", "AMPID", "COMMID", "AUTH", "SUBSOURCE", 
	"DELTA", "SEAZ", "RFLAG", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, false, true, 
	true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 1, 1, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 5, 4, 2, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}

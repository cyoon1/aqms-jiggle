package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
/**
 * Class to access and manipulate the AssocAmO (association) table.
 */
public class AssocAmO extends OriginAssocTableRow implements TableRowAssocAmO {
    
    public AssocAmO () {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public AssocAmO(AssocAmO object) {
	this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then assigns the default connection object to the handle of Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public AssocAmO(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then assigns the input values to the column data members.
* The newly instantiated object state values are isUpdate(true) and isNull() == false.
*/
    public AssocAmO(long orid, long ampid, String auth) {
	this();
        fields.set(ORID, new DataLong(orid));
        fields.set(AMPID, new DataLong(ampid));
        fields.set(AUTH, new DataString(auth));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then assigns the input values to the column data members.
* The newly instantiated object state values are isUpdate(true) and isNull() == false.
*/
    public AssocAmO(long orid, long ampid, String auth, double delta, double seaz) {
	this(orid, ampid, auth);
        fields.set(DELTA, new DataDouble(delta));
        fields.set(SEAZ, new DataDouble(seaz));
    }

}

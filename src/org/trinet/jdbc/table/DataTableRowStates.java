package org.trinet.jdbc.table;
/** Defines the possible processing modes of table row objects:
* NONE, SELECT, UPDATE, DELETE, or INSERT.
*/
public interface DataTableRowStates {
/** Number of defined processing states */
    public static final int MAX_PROCESSING_STATES = 5;
/** Flags row as not scheduled for processing; a characteristic of new instantiations. */
    public static final int NONE = 0;
/** Flags row as available for a database read; a SELECT query. */
    public static final int SELECT = 1;
/** Flags row as available for a database UPDATE. */
    public static final int UPDATE = 2;
/** Flags row as available for a database DELETE. */
    public static final int DELETE = 3;
/** Flags row as available for a database INSERT. */
    public static final int INSERT = 4;
}

package org.trinet.jdbc.table ;
import org.trinet.jdbc.StringSQL;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
/** Class to access and manipulate the NCEDC parametic schema event table.
*/
public class Event extends EventTableRow implements TableRowEvent {
                      
/** Constructor uses static data members defined by the TableRowEvent interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/
    public Event () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public Event(Event object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Event(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid) {
	this();
	fields.set(EVID, new DataLong(evid));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth) {
	this(evid);
	fields.set(AUTH, new DataString(auth));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor) {
	this(evid, auth);
	fields.set(PREFOR, new DataLong(prefor));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor, long prefmag) {
	this(evid, auth, prefor);
	fields.set(PREFMAG, new DataLong(prefmag));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor, long prefmag, long prefmec) {
	this(evid, auth, prefor, prefmag);
	fields.set(PREFMEC, new DataLong(prefmec));
    }
    
/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor, long prefmag, long prefmec, String subsource) {
	this(evid, auth, prefor, prefmag, prefmec);
	fields.set(SUBSOURCE, new DataString(subsource));
    }
    
/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor, long prefmag, long prefmec, String subsource, String etype) {
	this(evid, auth, prefor, prefmag, prefmec, subsource);
	fields.set(ETYPE, new DataString(etype));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Event(long evid, String auth, long prefor, long prefmag, long prefmec, String subsource, String etype, int selectflag) {
	this(evid, auth, prefor, prefmag, prefmec, subsource, etype);
	fields.set(SELECTFLAG, new DataLong(selectflag));
    }

/** Returns an Event array whose origins are within the specified time range inclusive.
* Returns null if no matching rows are found or if a JDBC SQLException occurs input values must be UTC seconds.
*/
    public Event [] getRowsByDateTime(double start, double end) {
	//String whereString = "WHERE PREFOR IN (SELECT ORID FROM ORIGIN WHERE DATETIME BETWEEN " + start + " AND " + end + " )"; // aww 2008/01/24
	String whereString =
            "WHERE PREFOR IN (SELECT ORID FROM ORIGIN WHERE DATETIME BETWEEN TRUETIME.putEpoch(" +
               StringSQL.valueOf(start) + ",'UTC') AND TRUETIME.putEpoch(" +
               StringSQL.valueOf(end) + ",'UTC') )"; // for leap secs input aww 2008/01/24
	return (Event []) getRowsEquals(whereString);
    }

    /** Returns current value of the row VERSION attribute.
     * @return -1 if null, undefined */
    public int getVersion() {
        DataObject dataObj = getDataObject(Event.VERSION);
        return  (dataObj.isNull()) ? -1 : dataObj.intValue();
    }
    /** Increment the current value of the row VERSION attribute by one. */
    public Event incrementVersion() {
        int vers = getVersion();
        return (Event) setValue(Event.VERSION, ++vers);
    }
}

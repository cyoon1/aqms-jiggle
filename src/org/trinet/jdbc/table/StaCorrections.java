package org.trinet.jdbc.table;
import java.util.*;
import java.sql.Connection;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;

/** Constructor uses static data members defined by the TableRowStaCorrections interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME.
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data updated, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class StaCorrections extends ResponseStnChlTableRow implements TableRowStaCorrections {
    public StaCorrections() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/
    public StaCorrections(StaCorrections object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public StaCorrections(Connection conn) {
        this();
        setConnection(conn);
    }


/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StaCorrections(String net, String sta, String seedchan, String location, java.util.Date ondate) { // qualify as java.util
	this();
        fields.set(NET, new DataString(net));
        fields.set(STA, new DataString(sta));
        fields.set(SEEDCHAN, new DataString(seedchan));
        fields.set(LOCATION, new DataString(location));
        fields.set(ONDATE, new DataDate(ondate));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StaCorrections(String net, String sta, String seedchan,
                    String location, java.util.Date ondate,
                    double corr, String corr_type) {
	this(net, sta, seedchan, location, ondate);
        fields.set(CORR, new DataDouble(corr));
        fields.set(CORR_TYPE, new DataDouble(corr_type));
    }
    public StaCorrections(String net, String sta, String seedchan,
                    String location, java.util.Date ondate,
                    double corr, String corr_type, String corr_flag) {
	this(net, sta, seedchan, location, ondate, corr, corr_type);
        fields.set(CORR_FLAG, new DataDouble(corr_flag));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StaCorrections(DataStnChl sc, java.util.Date ondate) { // qualify as java.util
	this();
        fields.set(NET, new DataString(sc.getStringValue(NET)));
        fields.set(STA, new DataString(sc.getStringValue(STA)));
        fields.set(SEEDCHAN, new DataString(sc.getStringValue(SEEDCHAN)));
        fields.set(LOCATION, new DataString(sc.getStringValue(LOCATION)));
        fields.set(CHANNEL, new DataString(sc.getStringValue(CHANNEL)));
        fields.set(CHANNELSRC, new DataString(sc.getStringValue(CHANNELSRC)));
        fields.set(ONDATE, new DataDate(ondate));
	valueUpdate = true;
	valueNull = false;
    }

}

package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Coda
*/
public interface TableRowCoda extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "CODA";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  40;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "COSEQ";


/**  CODA table "coid" column data object offset in collection stored by implementing class.
*/
    public static final int COID = 0;

/**  CODA table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 1;

/**  CODA table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 2;

/**  CODA table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 3;

/**  CODA table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 4;

/**  CODA table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 5;

/**  CODA table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 6;

/**  CODA table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 7;

/**  CODA table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 8;

/**  CODA table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 9;

/**  CODA table "codatype" column data object offset in collection stored by implementing class.
*/
    public static final int CODATYPE = 10;

/**  CODA table "afix" column data object offset in collection stored by implementing class.
*/
    public static final int AFIX = 11;

/**  CODA table "afree" column data object offset in collection stored by implementing class.
*/
    public static final int AFREE = 12;

/**  CODA table "qfix" column data object offset in collection stored by implementing class.
*/
    public static final int QFIX = 13;

/**  CODA table "qfree" column data object offset in collection stored by implementing class.
*/
    public static final int QFREE = 14;

/**  CODA table "tau" column data object offset in collection stored by implementing class.
*/
    public static final int TAU = 15;

/**  CODA table "nsample" column data object offset in collection stored by implementing class.
*/
    public static final int NSAMPLE = 16;

/**  CODA table "rms" column data object offset in collection stored by implementing class.
*/
    public static final int RMS = 17;

/**  CODA table "durtype" column data object offset in collection stored by implementing class.
*/
    public static final int DURTYPE = 18;

/**  CODA table "iphase" column data object offset in collection stored by implementing class.
*/
    public static final int IPHASE = 19;

/**  CODA table "eramp" column data object offset in collection stored by implementing class.
*/
    public static final int ERAMP = 20;

/**  CODA table "units" column data object offset in collection stored by implementing class.
*/
    public static final int UNITS = 21;

/**  CODA table "time1" column data object offset in collection stored by implementing class.
*/
    public static final int TIME1 = 22;

/**  CODA table "amp1" column data object offset in collection stored by implementing class.
*/
    public static final int AMP1 = 23;

/**  CODA table "time2" column data object offset in collection stored by implementing class.
*/
    public static final int TIME2 = 24;

/**  CODA table "amp2" column data object offset in collection stored by implementing class.
*/
    public static final int AMP2 = 25;

/**  CODA table "time3" column data object offset in collection stored by implementing class.
*/
    public static final int TIME3 = 26;

/**  CODA table "amp3" column data object offset in collection stored by implementing class.
*/
    public static final int AMP3 = 27;

/**  CODA table "time4" column data object offset in collection stored by implementing class.
*/
    public static final int TIME4 = 28;

/**  CODA table "amp4" column data object offset in collection stored by implementing class.
*/
    public static final int AMP4 = 29;

/**  CODA table "time5" column data object offset in collection stored by implementing class.
*/
    public static final int TIME5 = 30;

/**  CODA table "amp5" column data object offset in collection stored by implementing class.
*/
    public static final int AMP5 = 31;

/**  CODA table "time6" column data object offset in collection stored by implementing class.
*/
    public static final int TIME6 = 32;

/**  CODA table "amp6" column data object offset in collection stored by implementing class.
*/
    public static final int AMP6 = 33;

/**  CODA table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 34;

/**  CODA table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 35;

/**  CODA table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 36;

/**  CODA table "datetime" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME = 37;

/**  CODA table "algorithm" column data object offset in collection stored by implementing class.
*/
    public static final int ALGORITHM = 38;

/**  CODA table "winsize" column data object offset in collection stored by implementing class.
*/
    public static final int WINSIZE = 39;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES = // note TIME values are in relative tau, not absolute true seconds
   "COID,COMMID,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,CODATYPE,AFIX,AFREE,QFIX,QFREE,TAU,NSAMPLE,RMS,DURTYPE,IPHASE,ERAMP,UNITS," +
    "TIME1,AMP1,TIME2,AMP2,TIME3,AMP3,TIME4,AMP4,TIME5,AMP5,TIME6,AMP6," +
    "QUALITY,RFLAG,LDDATE,truetime.getEpoch(DATETIME,'UTC'),ALGORITHM,WINSIZE"; // for db to UTC conversion -aww 2008/02/12

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "CODA.COID,CODA.COMMID,CODA.STA,CODA.NET,CODA.AUTH,CODA.SUBSOURCE,CODA.CHANNEL,CODA.CHANNELSRC,CODA.SEEDCHAN,CODA.LOCATION,CODA.CODATYPE,CODA.AFIX,CODA.AFREE,CODA.QFIX,CODA.QFREE,CODA.TAU,CODA.NSAMPLE,CODA.RMS,CODA.DURTYPE,CODA.IPHASE,CODA.ERAMP,CODA.UNITS," +
    "CODA.TIME1,CODA.AMP1,CODA.TIME2,CODA.AMP2,CODA.TIME3,CODA.AMP3,CODA.TIME4,CODA.AMP4,CODA.TIME5,CODA.AMP5,CODA.TIME6,CODA.AMP6," +
    "CODA.QUALITY,CODA.RFLAG,CODA.LDDATE,truetime.getEpoch(CODA.DATETIME,'UTC'),CODA.ALGORITHM,CODA.WINSIZE"; // for db to UTC conversion -aww 2008/02/12

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"COID", "COMMID", "STA", "NET", "AUTH", 
	"SUBSOURCE", "CHANNEL", "CHANNELSRC", "SEEDCHAN", "LOCATION", 
	"CODATYPE", "AFIX", "AFREE", "QFIX", "QFREE", 
	"TAU", "NSAMPLE", "RMS", "DURTYPE", "IPHASE", 
	"ERAMP", "UNITS", "TIME1", "AMP1", "TIME2", 
	"AMP2", "TIME3", "AMP3", "TIME4", "AMP4", 
	"TIME5", "AMP5", "TIME6", "AMP6", "QUALITY", 
	"RFLAG", "LDDATE", "DATETIME", "ALGORITHM", "WINSIZE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, false, true, false, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, false, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, false, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATALONG, DATADOUBLE, DATASTRING, DATASTRING, 
	DATADOUBLE, DATASTRING, DATALONG, DATALONG, DATALONG, 
	DATALONG, DATALONG, DATALONG, DATALONG, DATALONG, 
	DATALONG, DATALONG, DATALONG, DATALONG, DATADOUBLE, 
	DATASTRING, DATADATE, DATADOUBLE, DATASTRING, DATADOUBLE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 3, 0, 0, 
	3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 10, 0, 2
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 6, 8, 15, 8, 3, 8, 3, 2, 3, 7, 7, 7, 7, 7, 6, 5, 3, 8, 
	5, 4, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 2, 2, 7, 25, 15, 6
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, null, null
    };
}

package org.trinet.jdbc;
import java.sql.*;
public interface ConnectionFactoryIF {
    public boolean onServer() ;
    public String getStatus() ;
    public Connection createConnection(String url, String driverName, String user, String passwd) ;
    public Connection createInternalDbServerConnection() ;
}

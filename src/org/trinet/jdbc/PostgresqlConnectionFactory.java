package org.trinet.jdbc;

import java.sql.*;
import org.postgresql.*;

/**
 * Created by renate on 9/30/15.
 */
public class PostgresqlConnectionFactory implements ConnectionFactoryIF {

    private static String status = "";

    /** null constructor */
    public PostgresqlConnectionFactory() {}

    public boolean onServer() {
        return false;
    }

    public Connection createConnection(String url, String driverName, String user, String passwd) {
        Connection conn = null;
        try {
            // register driver, only allowed value is "org.postgresql.Driver"
            Class.forName(driverName);
            conn = DriverManager.getConnection(url, user, passwd);
            if (conn != null) {
              configureConnection(conn);
              status = "OK";
            } else {
              status = "null";
            }
        } catch ( ClassNotFoundException ex ) {
            System.err.println("ERROR Cannot find the database driver class.");
            System.err.println(ex);
            status = ex.getMessage();
        } catch ( SQLException ex ) {
            SQLExceptionHandler.handleException(ex, conn);
            status = ex.getMessage();
            System.err.println("ERROR Connection data is incorrect or database is unavailable:\n");
            System.err.println("CONNECTION URL: "+url+"   for USER: "+user+"/"+passwd+"\n");
        } catch ( Exception ex ) {
            ex.printStackTrace ();
            status = ex.getMessage();
        }

        return conn;

    }

    public Connection createInternalDbServerConnection() {
        // postgress so far don't know if they have special local connection
        // but ConnectionFactory Interface requires this function
        if (! onServer()) {
            System.err.println("ERROR PostgresqlConnectionFactory internal connection not on server!");
            return null;
        }
        // TO DO: Figure out appropriate Exception to throw
        Connection conn = null;

        return conn;

    }

    public String getStatus() {
        return status;
    }

    private static void configureConnection(Connection conn) throws SQLException {
        //fix this, these are Oracle specific settings, perhaps they also exist for postgresql?
        conn.setAutoCommit(false);
        //conn.setDefaultRowPrefetch(200); // default=1; avoid row roundtrips
        //conn.setImplicitCachingEnabled(true);
        //conn.setStatementCacheSize(24);
    }
}

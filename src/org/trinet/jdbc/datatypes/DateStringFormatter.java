package org.trinet.jdbc.datatypes;
public interface DateStringFormatter {
    String toDateString(String formatPattern);
}

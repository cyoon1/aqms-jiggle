package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Class implements a DataDate range. */
public class DataDateRange extends DataObjectRange implements DateStringFormatter, ValueParser, Cloneable, Comparable {

    protected String defaultDateFormat = EpochTime.DEFAULT_FORMAT;  // "yyyy-MM-dd HH:mm:ss.SSS";

/** Default constructor, isNull() == true. */
    public DataDateRange() {
        this(new DataDate(), new DataDate());
    }

/** Constructor creates minimum and maximum bounds from the input object values.
* @exception java.lang.NullPointerException     input Date parameter min == null || max == null 
* @exception java.lang.IllegalArgumentException max < min
*/
    public DataDateRange(DataDate  min, DataDate max) {
        super(min, max);
    }

/** Constructor creates minimum and maximum bounds from the input object values.
* @exception java.lang.NullPointerException     input Date parameter min == null || max == null 
* @exception java.lang.IllegalArgumentException max < min
*/
    public DataDateRange(java.util.Date  min, java.util.Date max) {
        this(new DataDate(min), new DataDate(max));
    }

/** Constructor uses EpochTime.epochToDate(double) to construct bounds.
 * Input values can be nominal or UTC seconds, set <i>isTrue=true</i>, if input values are UTC seconds. */
    public DataDateRange(double minEpoch, double maxEpoch, boolean isTrue) { // for UTC or nominal input -aww 2008/02/12
        this( EpochTime.epochToDate(((isTrue) ? LeapSeconds.trueToNominal(minEpoch) : minEpoch)),
              EpochTime.epochToDate(((isTrue) ? LeapSeconds.trueToNominal(maxEpoch) : maxEpoch))
        );
    }

/** Constructor uses EpochTime.stringToDate(String date, String pattern) to construct date bounds.
 * Database dates are nominal GMT, no leap seconds. Default date string format is set to the input pattern, if it's not null.
 * */
    public DataDateRange(String minDate, String maxDate, String pattern ) {
        this(EpochTime.stringToDate(minDate, pattern), EpochTime.stringToDate(maxDate, pattern)); // leap seconds, become HH:mm:59 in strings -aww 2008/02/12
        if (pattern != null) defaultDateFormat = pattern;
    }

/** Returns milliseconds value of lower bound. */
    protected long getMinTime() {
        return min.longValue();
    }

/** Returns milliseconds value of upper bound. */
    protected long getMaxTime() {
        return max.longValue(); }

/** Returns a clone of the minimum bound. */
    public java.util.Date getMinValue() {
        return ((DataDate) min).dateValue();
    }

/** Returns a clone of the maximum bound. */
    public java.util.Date getMaxValue() {
        return ((DataDate) max).dateValue();
    }

/** Returns the size of the range as a double seconds. */
    public double seconds() {
        return doubleExtent()/1000.;
    }

/** Returns the size of the range as a double milliseconds. */
    public double doubleExtent() {
        return (double) (max.longValue() - min.longValue());
    }

/** Returns the size of the range as a long milliseconds. */
    public long longExtent() {
        return max.longValue() - min.longValue();
    }

/** True if minimum bound of this instance is after the input date. */
    public boolean after(java.util.Date date) {
        return ((DataDate) min).value.after(date);
    }

/** True if minimum bound of this instance is after the maximum bound of the input range. */
    public boolean after(DataDateRange range) {
        return after(range.max); 
    }

/** True if maximum bound of this instance is before the input date. */
    public boolean before(java.util.Date date) {
        return ((DataDate) max).value.before(date);
    }

/** True if maximum bound of this instance is before than mininum bound of input range. */
    public boolean before(DataDateRange range) {
        return before(range.min); 
    }

/** True if this object's range excludes input date. */
    public boolean excludes(java.util.Date date) {
        return ! contains(date);
    }

/** True if this object's range excludes input object's range. */
    public boolean excludes(DataDateRange range) {
        return ! overlaps(range.min, range.max);
    }

/** True if this object's range excludes input range. */
    public boolean excludes(java.util.Date minDate, java.util.Date maxDate) {
        return ! overlaps(minDate, maxDate);
    }

/** True if this object's range contains input object's range. */
    public boolean contains(DataDateRange range) {
        return contains(range.min, range.max); 
    }

/** True if this object's range contains input range. */
    public boolean contains(java.util.Date minDate, java.util.Date maxDate) {
        return ( contains(minDate) && contains(maxDate) );
    }

/** True if this object's range contains input date. */
    public boolean contains(java.util.Date date) {
        return ! ( after(date) || before(date) );
    }

/** True if this object's range overlaps input object's range. */
    public boolean overlaps(DataDateRange range) {
        return overlaps(range.min, range.max);
    }

/** True if this object's range overlaps input range. */
    public boolean overlaps(java.util.Date minDate, java.util.Date maxDate) {
        return ! ( minDate.after(((DataDate) max).value) || maxDate.before(((DataDate) min).value) );
    }

/** True if this object's range lies within input object's range. */
    public boolean within(DataDateRange range) {
        return range.contains(this); 
    }

/** True if this object's range lies within input range. */
    public boolean within(java.util.Date minDate, java.util.Date maxDate) {
        return ! ( ((DataDate) min).value.before(minDate) || ((DataDate) max).value.after(maxDate) );
    }

/** Returns true only if the input object is an instance of DataDateRange and
* its minimum and maximum values are equivalent to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || ! (getClass() != object.getClass()) ) return false;
        DataDateRange range = (DataDateRange) object;
        return (min.equals(range.min) && max.equals(range.max));
    }

/** Returns true only if the input range's minimum and maximum values
* are equivalent to this object's values.
*/
    public boolean equalsRange(DataDateRange range) {
        if (this == range) return true;
        if (range == null) return false;
        return (min.equalsValue(range.min) && max.equalsValue(range.max));
    }

/** Input object must in an instance of DataDateRange. 
* @return <pre>
* -1 this object's minimum value is less than the input object's value,
*    or it is equivalent and this object's maximum value is less than the input object's value.<br>
*  0 this object's minimum and maximum values are equivalent to the input object's values.<br>
*  1 this object's minimum value is greater than the input object's value,
*    or it is equivalent and its maximum value is greater than the input object's value.
* </pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of DataDateRange.
*/
    public int compareTo(Object object) {
        DataDateRange range = (DataDateRange) object;
        int retVal = min.compareTo(range.min);
        if (retVal != 0) return retVal;
        return (max.compareTo(range.max) < 0 ) ? -1 : 1;
    }

/** Returns true if max < min. */
    public boolean isReversed() {
        return ( ((DataDate) min).compareTo(max) > 0);
    }

/** Returns false if (isReversed() || min == null || max == null). */
    public boolean isValid() {
        return (min != null && max != null) ? isReversed() : false;
    }

/** Returns String concatenation of minimum and maximum bounds values 
* formatted as specified by the input pattern relative to UTC time zone.
* The min, max dates are separated by " ".
*/
    public String toDateString(String pattern) {
        StringBuffer sb = new StringBuffer(80);
        sb.append(EpochTime.dateToString( ((DataDate) min).value, pattern) );
        sb.append(" ");
        sb.append(EpochTime.dateToString( ((DataDate) max).value, pattern));
        return sb.toString();
    }

/** Returns String concatenation of minimum and maximum bounds as
* nominal seconds GMT date values separated by " ".
*/
    public String toString() { // use current set default format
        return toDateString(defaultDateFormat); // used to be "yyyy/MM/dd HH:mm:ss" -aww 2008/02/12
    }

/** Returns String concatenation of minimum and maximum dates as represented in database.
* Inserts "NULL" if value is equivalent to NULL or bound == null.
* These values are separated by ", ".
* @see org.trinet.jdbc.StringSQL#valueof(java.util.Date) 
*/
    public String toStringSQL() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(StringSQL.valueOf(min));
        sb.append(", ");
        sb.append(StringSQL.valueOf(max));
        return sb.toString();
    }

/** Convenience wrapper System.out.println(toString()). */
    public void print() {
        System.out.println(toString());
    }

/** 
* Sets the minimum bound value for the range to the input value.
*/
    public void setMin(java.util.Date minDate) {
        if (max != null) {
             if (minDate.after( ((DataDate) max).value) )
                  throw new IllegalArgumentException("DateRange constructor min-max bounds reversed.");
        }
        min.setValue(minDate);
    }

/** 
* Sets the maximum bound value of the range to the input value.
*/
    public void setMax(java.util.Date maxDate) {
        if (min != null) {
             if (maxDate.before( ((DataDate) min).value))
                  throw new IllegalArgumentException("DateRange constructor min-max bounds reversed.");
        }
        max.setValue(maxDate);
    }

/** 
* Sets the minimum, maximum bounds of the range to the input values.
*/
    public void setLimits(java.util.Date minDate, java.util.Date maxDate) {
        if (minDate == null || maxDate == null) throw new NullPointerException("DateRange setLimits() null input parameter");
        if (maxDate.before(minDate))
            throw new IllegalArgumentException("DateRange setLimits() min-max bounds reversed." );
        min.setValue(minDate);
        max.setValue(maxDate);
    }

/** 
* Sets the range bounds to clones of the input range bounds.
*/
    public void setLimits(DataDateRange range) {
        setLimits((DataDate) range.min.clone(), (DataDate) range.max.clone());
    }

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(Date) == true.
*/
    public void include(java.util.Date date) {
        if (min == null || max == null)
             throw new NullPointerException("DateRange include(date) null min or max bound");

        if (date.before( ((DataDate) min).value) ) min.setValue(date);
        else if (date.after( ((DataDate) max).value) ) max.setValue(date);
    }

/**
* Sets the appropiate range bounds to include input range bounds.
* Clones the input bounds.
* Does a no-op if contains(DataDateRange) == true.
*/
    public void include(DataDateRange range) {
        if (min == null || max == null)
             throw new NullPointerException("DateRange include(range) null min or max bound");
 
        if (((DataDate) range.min).value.before( ((DataDate) min).value) ) min = (DataDate) range.min.clone();
        if (((DataDate) range.max).value.after( ((DataDate) max).value) ) max = (DataDate) range.max.clone();
    }

/**
* Returns true if bound values can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.countTokens() < 2 
* or java.util.Date cannot be parsed from tokens.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (tokenizer.countTokens() < 2) return false;
        java.util.Date minDate = EpochTime.stringToDate(tokenizer.nextToken(), defaultDateFormat); 
        java.util.Date maxDate = EpochTime.stringToDate(tokenizer.nextToken(), defaultDateFormat);
        if (minDate == null || maxDate == null) return false;
        setLimits(minDate, maxDate);
        return true;
    }

    /*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");

         long ms = System.currentTimeMillis();
         Date now = new Date(ms);
      
         Date ontime = new Date(ms + 360000);
         Date later = new Date(ms + 3600000);
         Date laterstill = new Date(ms + 7200000);
         Date yesterday = new Date(ms - 86400000);
         Date tomorrow =  new Date(ms + 86400000);

         DataDateRange dr = new DataDateRange(now, later);
         DataDateRange dr2 = new DataDateRange(yesterday, tomorrow);

         dr.dump1(yesterday);
         dr.dump1(ontime); dr.dump1(later); dr.dump1(tomorrow);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(ontime, laterstill)");
         dr.setLimits(ontime, laterstill);
         dr.dump2(dr2);

         System.out.println("Test dr.include(now)");
         dr.include(now);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(laterstill); dr2.setMin(laterstill)");
         dr.setMax(laterstill);
         dr2.setMin(laterstill);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(later); dr2.setMin(laterstill)");
         dr.setMax(later);
         dr2.setMin(laterstill);
         dr.dump2(dr2);

    }

    public void dump(DataDateRange dr2, DataDateRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(Date val) {
         System.out.println("Dump Range bounds: " + min.toString() + ", " + max.toString() + " size: " +
                             EpochTime.elapsedTimeToText(seconds()) );
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(DataDateRange dr2) {
         System.out.println("Dump Range bounds: " + min.toString() + ", " + max.toString() + " size: " +
                             EpochTime.elapsedTimeToText(seconds()) );
         System.out.println(" Test Range2 Min,max: " + dr2.min.toString() + ", " + dr2.max.toString() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this)); 

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
    */
}


package org.trinet.jdbc.datatypes;

/** Defines a static array of Class objects, one for each of the DataObject data types.
* The class objects are stored at offsets defined by the constants in DataClassIds interface.
* The classes extending DataTableRow class map the DataObject classes to the respective column data types.
* Schema column type CHAR is mapped to DataString.class.
* Schema column type VARCHAR2(x) is mapped to the DataString.class.
* Schema column type NUMBER(x) is mapped to the DataLong.class.
* Schema column type NUMBER(x,y) where y is non-zero is mapped to the DataDouble.class.
* Schema column type DATE is mapped to the DataTimestamp.class
* @see DataClassIds
* @see DataString
* @see DataLong
* @see DataDouble
* @see DataInteger
* @see DataFloat
* @see DataDate
* @see DataTimestamp
*/
public interface DataClasses {
    public static final Class [] DATA_CLASSES = {DataString.class, DataLong.class, DataDouble.class, DataInteger.class,
						DataFloat.class, DataDate.class, DataTimestamp.class};
}

// TODO - change method parameters to generic Object then integrate this IF with NumberRangeIF
package org.trinet.jdbc.datatypes;

public abstract class DataObjectRange implements DataObjectRangeIF, ValueParser, Cloneable, Comparable {

/** Minimum value bound. */
    DataObject min;

/** Maximum value bound. */
    DataObject max;

/** Default constructor, null bounds, thus  isValid() == false. */
    public DataObjectRange() {}

/** Constructor creates minimum and maximum bounds from the input object values.
* @exception java.lang.NullPointerException null input parameter
* @exception java.lang.IllegalArgumentException <pre>
*  min == null || max  == null
*  max < min
*  </pre>
*/
    public DataObjectRange(DataObject min, DataObject max) {
        if (min == null || max == null) throw new NullPointerException("DataObjectRange  null input parameter");
        if (min.getClass() !=  max.getClass())
             throw new IllegalArgumentException("DataObjectRange min,max must be same class type.");
        if (min.compareTo(max) > 0) 
             throw new IllegalArgumentException("DataObjectRange min-max bounds reversed.");

        this.min = min;
        this.max = max;
    }

/** Returns true if both minimum and maximum bounds return isNull() == true. */
    public boolean isNull() {
        return (max.isNull() && max.isNull());
    }

/** Sets the minimum and maximum bounds such that isNull() == true . */
    public void setNull() {
        min.setNull(true);
        max.setNull(true);
    }

/** Returns the minimum bound. */
    public DataObject getMin() {
        return min;
    }

/** Returns the maximum bound. */
    public DataObject getMax() {
        return max;
    }

/** Returns the size of the range as a double. */
    public double doubleExtent() {
        return max.doubleValue() - min.doubleValue();
    }

/** Returns the size of the range as a long. */
    public long longExtent() {
        return max.longValue() - min.longValue();
    }

/** True if minimum bound of this instance is greater than the input value. */
    public boolean after(DataObject number) {
        return (min.doubleValue() > number.doubleValue());
    }

/** True if minimum bound of this instance is greater than the input range's max value. */
    public boolean after(DataObjectRange range) {
        return after(range.max); 
    }

/** True if maximum bound of this instance is less than the input value. */
    public boolean before(DataObject number) {
        return (max.doubleValue() < number.doubleValue());
    }

/** True if maximum bound of this instance less than mininum bound of input range. */
    public boolean before(DataObjectRange range) {
        return before(range.min); 
    }

/** True if this object's range excludes input number. */
    public boolean excludes(DataObject number) {
        return ! contains(number);
    }

/** True if this object's range excludes input object's range. */
    public boolean excludes(DataObjectRange range) {
        return ! overlaps(range.min, range.max);
    }

/** True if this object's range excludes input range. */
    public boolean excludes(DataObject min, DataObject max) {
        return ! overlaps(min, max);
    }

/** True if this object's range contains input object's range. */
    public boolean contains(DataObjectRange range) {
        return contains(range.min, range.max); 
    }

/** True if this object's range contains input range. */
    public boolean contains(DataObject min, DataObject max) {
        return ( contains(min) && contains(max) );
    }

/** True if this object's range contains input number. */
    public boolean contains(DataObject number) {
        return ! ( after(number) || before(number) );
    }

/** True if this object's range overlaps input object's range. */
    public boolean overlaps(DataObjectRange range) {
        return overlaps(range.min, range.max);
    }

/** True if this object's range overlaps input range. */
    public boolean overlaps(DataObject min, DataObject max) {
        return ! (after(max) || before(min) );
    }

/** True if this object's range lies within input object's range. */
    public boolean within(DataObjectRange range) {
        return range.contains(this); 
    }

/** True if this object's range lies within input range. */
    public boolean within(DataObject min, DataObject max) {
        return ( (this.min.compareTo(min) > -1) && (this.max.compareTo(max) < 1) );
    }

/** Returns a "deep copy" of this instance. */
    public Object clone() {
        DataObjectRange range = null;
        try {
            range = (DataObjectRange) super.clone();
            range.min =  (DataObject) min.clone();
            range.max = (DataObject) max.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return range;
    }

/** Returns true only if the input object is an instance of the same class type and
* its minimum and maximum DataObjects are equivalent to this object's.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || ( getClass() != object.getClass()) ) return false;
        DataObjectRange range = (DataObjectRange) object;
        return ( min.equals(range.min) && max.equals(range.max) );
    }

/** Input object must implement DataObjectRangeIF. 
* @return <pre>
/** Returns true only if the input object implements DataObjectRangeIF and
* its minimum and maximum range values are equivalent to this object's values.
*/
    public boolean equalsRange(DataObjectRangeIF range) {
        if (this == range) return true;
        if (range == null) return false;
        return ( min.equalsValue(range.getMin()) && max.equalsValue(range.getMax()) );
    }

/** Input object must in an instance of DataObjectRange. 
* @return <pre>
* -1 this object's minimum value is less than the input object's value,
*    or it is equivalent and this object's maximum value is less than the input object's value.<br>
*  0 this object's minimum and maximum values are equivalent to the input object's values.<br>
*  1 this object's minimum value is greater than the input object's value,
*    or it is equivalent and its maximum value is greater than the input object's value.
* </pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of DataObjectRange.
*/
    public int compareTo(Object object) {
        DataObjectRange range = (DataObjectRange) object;
        int retVal = min.compareTo(range.min);
        if (retVal != 0) return retVal;
        return ( max.compareTo(range.max) < 0 ) ? -1 : 1;
    }

/** Returns true if max < min. */
    public boolean isReversed() {
        return (min.compareTo(max) > 0);
    }

/** Returns false if (isReversed() || min == null || max == null). */
    public boolean isValid() {
        return (min != null && max != null) ? isReversed() : false;
    }

/** Returns String concatenation of minimum and maximum bounds values. 
* These values are separated by ",".
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(min.toString());
        sb.append(",");
        sb.append(max.toString());
        return sb.toString();
    }

/** Returns String concatenation of minimum and maximum toStringSQL(). 
* Inserts "NULL" if bound == null or isNull() == true.
* These values are separated by ", ".
*/
    public String toStringSQL() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(min.toStringSQL());
        sb.append(", ");
        sb.append(max.toStringSQL());
        return sb.toString();
    }

/** Convenience wrapper System.out.println(toString()). */
    public void print() {
        System.out.println(toString());
    }

/** 
* Sets the minimum bound to the input.
*/
    public void setMin(DataObject min) {
        if (max != null) {
             if(min.getClass() !=  max.getClass())
                 throw new IllegalArgumentException("DataObjectRange setMin min,max must be same class type.");
             if (min.compareTo(max) > 0) 
                  throw new IllegalArgumentException("DataObjectRange setMin min,max bounds reversed.");
        }
        this.min = min;
    }

/** 
* Sets the maximum bound to the input.
*/
    public void setMax(DataObject max) {
        if (min != null) {
             if(min.getClass() !=  max.getClass())
                 throw new IllegalArgumentException("DataObjectRange setMax min,max must be same class type.");
             if (max.compareTo(min) < 0)
                  throw new IllegalArgumentException("DataObjectRange setMax min,max bounds reversed.");
        }
        this.max = max;
    }

/** 
* Sets the minimum, maximum bounds of the range to the input values.
*/
    public void setLimits(DataObject min, DataObject max) {
        if (min == null || max == null) throw new NullPointerException("DataObjectRange setLimits() null input parameter");
        if (min.getClass() !=  max.getClass())
             throw new IllegalArgumentException("DataObjectRange setLimits() min,max must be same class type.");
        if (min.compareTo(max) > 0) 
             throw new IllegalArgumentException("DataObjectRange setLimits() min-max bounds reversed.");

        this.min = min;
        this.max = max;
    }

/** 
* Sets the range bounds to a clone of the input range.
*/
    public void setLimits(DataObjectRange range) {
        setLimits((DataObject) range.min.clone(), (DataObject) range.max.clone());
    }

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(DataObject) == true.
*/
    public void include(DataObject number) {
        if (min == null || max == null)
             throw new NullPointerException("DataObjectRange include(number) null min or max bound");
        if (number.getClass() !=  min.getClass())
             throw new IllegalArgumentException("DataObjectRange include() input must be same class type as bounds.");

        if (number.compareTo(min) < 0) min = number;
        else if (number.compareTo(max) > 0) max = number;
    }

/**
* Sets the appropiate range bounds to clones of the input range bounds.
* Does a no-op if contains(DataObjectRange) == true.
*/
    public void include(DataObjectRange range) {
        if (min == null || max == null)
             throw new NullPointerException("DataObjectRange include(number) null min or max bound");
        if (range.min.compareTo(min) < 0) min = (DataObject) range.min.clone();
        if (range.max.compareTo(max) > 0) max = (DataObject) range.max.clone();
    }

}

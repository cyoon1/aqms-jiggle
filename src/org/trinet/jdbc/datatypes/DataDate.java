package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;

/** Extends the base abstract DataObject class to implements a stateful Date value.
* Value is stored as a java.util.Date object.
*/
public class DataDate extends DataObject implements DataTime, DateStringFormatter, Comparable, Cloneable {
/** Data member holding value. */
    protected java.util.Date value;

/** Default SimpleDateFormat pattern string used for String conversions. */
    protected String dateFormat = EpochTime.DEFAULT_FORMAT;  // now yyyy-MM-dd HH:mm:ss.SSS used to be yyyy/MM/dd HH:mm:ss

/** Constructor sets the object value to NullValueDb.NULL_DATE and the object state flags to their default settings
* (Null: true; Update: false; Mutable: true)).
*/
    public DataDate() {
        this.value = (java.util.Date) NullValueDb.NULL_DATE.clone();
    }
 
/** Constructor sets the date to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
*/
    public DataDate(int value) {
        this((long) value);
    }

/** Constructor sets the date to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
*/
    public DataDate(long value) {
        this.value = new java.util.Date(value);
        this.valueUpdate = true;
        this.valueNull = false;
    }

/** Constructor sets the date to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
 *  If input is Float.NaN initial state is set to null value.
*/
    public DataDate(float value) {
        this();
        setValue(value);
    }

/** Constructor sets the date to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
 *  If input is Double.NaN initial state is set to null value.
*/
    public DataDate(double value) {
        this();
        setValue(value);
    }

/** Constructor sets the date to value of a string in SQL date escape format,
* or if input is a numeric string, to the numeric value as (long) millisecs.
* Assumes simple date format for UTC of: "yyyy-MM-dd HH:mm:ss.SSS".
* The state flags are set: isUpdate() == true and isNull() == false, unless input value is null.
* @throws java.lang.NumberFormatException string cannot be parsed into date  
*/
    public DataDate(String value) {
        this(); // create a default date;
        setValue(value);
    }

/** Constructor sets the date to value of a string in SQL date escape format.
* The specified input pattern (SimpleDateFormat) is used to parse the input string.
* Sets the default date format to the input pattern if it is not null.
* The state flags are set: isUpdate() == true and isNull() == false, unless input value is null.
*/
    public DataDate(String value, String pattern) {
        this();
        if (pattern != null) this.dateFormat = pattern;
        setValue(value);
    }

/** Constructor sets the date to a clone of the Date object argument.
* The state flags are set Update:true and Null:false.
*/
    public DataDate(java.util.Date object) {
        this();
        if (object == null) setNull(true);
        else {
            this.value = (java.util.Date) object.clone();
            this.valueUpdate = true;
            this.valueNull = false;
        }
    }

/** Constructor sets the date to a clone of the date object internal to the DataDate argument.
* The state flag values are set to those of the input DataDate object.
* Null state is propagated.
*/
    public DataDate(DataDate object) {
        this();
        if (object == null) setNull(true);
        else {
            this.value = (java.util.Date) object.value.clone();
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

/** Sets the default SimpleDateFormat pattern for String representations.
*/
    public void setDateFormat(String pattern) {
        this.dateFormat = pattern;
    }

/** Returns the representation of the value formatted using the default format pattern.
*/
    public String toDateString() {
        return EpochTime.dateToString(value, dateFormat); // DB dates are nominal GMT time
    }

/** Returns the representation of the value formatted using the specified format pattern.
*/
    public String toDateString(String pattern) {
        return org.trinet.util.EpochTime.dateToString(value, pattern); // DB dates are nominal GMT time
    }

/** Returns the default toString() representation of the value.
*/
    public String toString() {
        return value.toString(); 
    }

/** Returns an SQL syntax string value of the date date field; if isNull() == true, returns the string "NULL".
*/
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns the value of java.util.Date.hashcode() for the date value.
*/
    public int hashCode() {
        return value.hashCode(); 
    }

/** Returns true input object is an instance of DataDate and it value and flags are equivalent to those of this instance..
* Returns false if input object is null or not an instance of DataDate.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataDate)) return false;
        if (this.value.equals(((DataDate) object).value) && 
           this.valueUpdate == ((DataDate) object).valueUpdate && 
           this.valueMutable == ((DataDate) object).valueMutable && 
           this.valueNull == ((DataDate) object).valueNull) return true;
        else return false;
    }

/** Returns true if argument object is instanceof this DataDate or java.util.Date and its date values are equal.
*  No state flag values are checked.
*/
    public boolean equalsValue(Object object) {
        if (object == null) return false;
        if (object instanceof DataDate) {
            return ( this.value.equals( ((DataDate) object).value) );
        }
        else if (object instanceof java.util.Date) {
            return ( this.value.equals((java.util.Date) object ) );
        }
        else return false; 
    }

/** Returns the value of java.util.Date.compareTo() for the data value member of this object.
* A return of 0 == this value equals, <0 == value less than, >0 == value greater than, the value of the argument object.
* Throws an exception if argument object is not an instanceof DataDate.
*/
    public int compareTo(Object object) throws ClassCastException {
        if ( object instanceof java.util.Date) {
            return this.value.compareTo((java.util.Date)object);
        }
        else if (object instanceof DataDate) {
            return compareTo((DataDate) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a jav.util.Date or DataDate class type: "
                                + object.getClass().getName());
    }

    public int compareTo(DataDate object) {
        return this.value.compareTo(object.value);
    }

/** Overrides the DataObject.class clone(). Calls super.clone() and also clones the contained java.util.Date member.
* Returns handle to new object.
*/
    public Object clone() {
        DataDate obj = null;
        obj = (DataDate) super.clone();
        obj.value = (java.util.Date) obj.value.clone();
        return obj;
    }

/** Returns the value of the date interpreted as integer seconds (not millisecs).
*/
    public int intValue() {
        long time = value.getTime()/1000;
        return (int) time;
    }

/** Returns the value of the date as integer milliseconds.
*/
    public long longValue() {
        return (long) value.getTime();
    }

/** Returns the value of the date as integer milliseconds.
*/
    public float floatValue() {
        return (float) value.getTime();
    }
    /** Returns the value of the date as epoch seconds.
    Fractional part is NOT preserved.
    */
    public double epochTimeValue() {
      return (double) value.getTime()/1000.0;  // epoch millisecs -> seconds
    }
/** Returns the value of the date as integer milliseconds.
*/
    public double doubleValue() {
        return (double) value.getTime();
    }

/** Returns a clone of the Date data member.
*/
    public java.util.Date dateValue() {
        return (java.util.Date) value.clone();
    }

/** Returns a new java.util.Timestamp object derived the date value.
*/
    public java.sql.Timestamp timestampValue() {
        return new java.sql.Timestamp(value.getTime());
    }

/** Sets the date time value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true .
* Sets the null status flag, isNull() == false.
*/
    public void setValue(int value) {
        setValue((long) value);
    }

/** Sets the date time value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true .
* Sets the null status flag, isNull() == false.
*/
    public void setValue(long value) {
        if(! isMutable()) return;
        this.value.setTime(value);
        this.valueNull = false;
        this.valueUpdate = true;
    }

/** Sets the date time value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true .
* Sets the null status flag, isNull() == false., unless input is Float.NaN
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue((long) value);
    }

/** Sets the date time value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true .
* Sets the null status flag, isNull() == false, unless input is Double.NaN.
*/
    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        else setValue((long) value);
    }

/** Sets the date time value to the time equivalent of the input argument value.
* Input argument can be an instanceof Date, Number, DataObject, or String.
* Exceptions are thrown if the input argument can
* not be formatted/parsed as a Date.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
*/
    public void setValue(Object object) throws ClassCastException, NumberFormatException {
        if(! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (java.util.Date.class.isInstance(object)) {
            setValue(((java.util.Date) object).getTime());
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).longValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.longValue());
            //setValue(((DataObject) object).longValue());
        }
        else if (String.class.isInstance(object)) {
           setValue((String) object);
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " + object.getClass().getName());
    }

/** Sets the date time value to the time equivalent of the input argument value.
* Uses the current date format setting, which defaults to UTC of: "yyyy-MM-dd HH:mm:ss.SSS".
* if date parse fails, tries to parse input a numeric millisecs elapsed time,
* a NumberFormatException is thrown if the it can not be parsed as a number.
* Does a no-op if isMutable() == false. Sets null state true if input is null, else
* sets the null state false.
* Sets the update status flag, isUpdate() == true .
*/
    public void setValue(String value) {
        if(! isMutable()) return;
        if (value == null) {
          setNull(true);
        }
        else {
          // stringToDate returns null if not parseable
          Date newDate = org.trinet.util.EpochTime.stringToDate(value, dateFormat); // DB dates are nominal GMT time
          if (newDate != null) {
             this.value = newDate;
             this.valueNull = false;
             this.valueUpdate = true;
          }
          // try for a number input
          else setValue(Double.parseDouble((String) value));
        }
    }


/** Sets the object value to NullValueDB.NULL_DATE and sets the flags,
 * isNull() == tf and isUpdate() == true .
* Does a no-op, if isMutable == false. 
 */
   public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_DATE;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
   }


/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or java.util.Date cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        // stringToDate returns null if not parseable
        setValue( EpochTime.stringToDate(tokenizer.nextToken(), dateFormat) );
        return true;
    }

    public boolean isValidNumber() {
        return ! isNull();
    }
}

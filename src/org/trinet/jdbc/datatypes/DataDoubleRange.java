package org.trinet.jdbc.datatypes;
import java.util.*;

/**
 * General purpose range (pair) of double values
*/
public class DataDoubleRange extends DataObjectRange {

    public DataDoubleRange() {
        this(new DataDouble(), new DataDouble());
    }

    public DataDoubleRange(DataDouble min, DataDouble max) {
	super(min, max);
    }

    public DataDoubleRange(double min, double max) {
        this(new DataDouble(min), new DataDouble(max));
    }

/**
 * Returns the min value of this range.
*/
    public double getMinValue() {
	return getMin().doubleValue();
    }

/**
 * Returns the max value of this range.
*/
    public double getMaxValue() {
	return getMax().doubleValue();
    }

/**
 * Sets the min value of this range.
*/
    public void setMin(double min) {
	setMin(new DataDouble(min));
    }

/**
 * Sets the max value of this range.
*/
    public void setMax(double max) {
	setMax(new DataDouble(max));
    }

/**
 * Sets the max, min values of this range.
*/
    public void setLimits(double min, double max) {
	setLimits(new DataDouble(min), new DataDouble(max));
    }

    public void include(double val) {
	include(new DataDouble(val));
    }

    public void include(DataDoubleRange range) {
	include((DataObjectRange) range );
    }

/**
* Return true if number is within specified bounds inclusive.
*/
    public boolean contains(double val) {
        return contains(new DataDouble(val));
    }

/**
* Return true if range is within this range inclusive.
*/
    public boolean contains(DataDoubleRange range) {
	return contains((DataObjectRange) range);
    }

/**
* Returns the difference between the upper and lower range bounds.
*/
    public double size() {
	return doubleExtent();
    }

/**
* Returns min + "," + max.
*/
    public String toString() {
        return  min + "," + max;
    }

/**
* Returns true if a range can be parsed from input StringTokenizer.
* Does not set range and returns false if tokenizer.hasMoreTokens() == false
* or a range cannot be parsed from tokenizer.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (tokenizer.countTokens() < 2) return false;
        boolean retVal = false;
        try {
            setLimits( Double.parseDouble(tokenizer.nextToken()), Double.parseDouble(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
            System.err.println("DataDoubleRange parseValue()" + ex.getMessage());
        }
        return retVal;
    }

/*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");
         DataDoubleRange dr = new DataDoubleRange(-10., 10.);

         DataDoubleRange dr2 = new DataDoubleRange(-20., 20.);

         FloatRange dr3 = new FloatRange(-20.f, 20.f);

         dr.dump(dr2, dr3);
         dr3.setLimits(1.f,21.f);
         dr.dump(dr2, dr3);

         dr.dump1(-11.);
         dr.dump1(-1.);
         dr.dump1(0.);
         dr.dump1(1.);
         dr.dump1(11.);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(-14, 14)");
         dr.setLimits(-14.,14.);
         dr.dump2(dr2);

         System.out.println("Test dr.include(-16, 16)");
         dr.include(-16.);
         dr.include(16.);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(0)");
         dr.setMax(0.);
         dr2.setMin(0.);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(1)");
         dr.setMax(0.);
         dr2.setMin(1.);
         dr.dump2(dr2);

    }


    public void dump(DataObjectRange dr2, DataObjectRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(double number) {
         DataDouble val = new DataDouble(number);
         System.out.println("Dump Range bounds, size: " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(DataDoubleRange dr2) {
         System.out.println("Dump Range, number : " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test Range2 Min,max: " + dr2.getMinValue() + ", " + dr2.getMaxValue() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this)); 

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
*/
}

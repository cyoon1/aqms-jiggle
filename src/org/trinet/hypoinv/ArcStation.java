package org.trinet.hypoinv;

import org.trinet.jasi.*;
import org.trinet.util.*;
import java.lang.Math;

public class ArcStation {
  public String site;
  public String net;
  public char chnl;
  public String chan;
  public String prmk;
  public char pfm;
  public int pwta;
  public int year;
  public int month;
  public int day;
  public int hr;
  public int mn;
  public float psec;
  public float pttr;
  public float pwtu;
  public float ssec;
  public String srmk;
  public int swta;
  public float sttr;
  public float amp;
  public int ampunits;
  public float swtu;
  public float pdelay;
  public float sdelay;
  public float dist;
  public float eangle;
  public int ampmagwt;
  public int durmagwt;
  public float period;
  public char chlrmk;
  public float taud;
  public float azes;
  public float durmag;
  public float ampmag;
  public float impp;
  public float imps;
  public char datasrccode;
  public char durmagcode;
  public char ampmagcode;
  public String loc; // aww added 06/13/2005
  //public int iamptype; // noticed undocumented in CUSP HYCIN and HYLST output default 0 -aww 12/07/2005

  public boolean parseStatus;
  public static int MAX_ARC_STATION_CHARS=113;  // was 110, but 113 total out to end of location code field -aww
  public static String titleStr = "SITE NET CHL LC DATE       HR:MN   PSEC  DIST AZM  AN PD PWT   PRES  PWTU SD SWT  SSEC  S-RES  SWTU   IMPP   IMPS CODES";


  public ArcStation () {
  }

  public ArcStation(String stnStr) {
    parseArcStation(stnStr);
  }
/*
      1         2         3         4         5         6         7         8         9        10        11
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
MCV  NC VVHZ  PU0199806262007 4310  -9110    0   0   0      0 0  0   0   0  1616400  0  159297404  0 502   0WD
HMT  CI VVHZ     199811102059    0   0  0 5632IS 0 -27    151 0  0   0   0 708 9000  0    0228  0  0   0
WOF   G  P V IPD0199811120254 2436   7172    0   0   0      0 0  0   0   0 49610100  0    0165  0  0 174   0
GTM  CI  EHZ  P00199912080004 4174  -9155    0   0   0      0 0  0   0   0 264 9000  0    0201  0  0 741   0
KNW  AZ HHHE  PD0200004170736 0240   4100                                   -1              -1
WRV  CI VEHZ  P 3200209122043 3869  -1  0                                  450
*** NOTE:  when Hypoinverse cannot 'setup' a station it is still returned
           in the .arc output with an abreviated line like the following:
HMT  CI VVLN     200004071235        0  0 1285 S 0
ALV  CI EEHZ  PU2200004111537 3867   3 50
*/
  public boolean parseArcStation(String stnStr) {
    parseStatus = false;

    if (stnStr.length() < MAX_ARC_STATION_CHARS) {
      System.out.println("parseArcStation: Arc station record shorter then MAX_ARC_STATION_CHARS: " + stnStr.length());
      System.out.println("\"" + stnStr + "\"");
      return parseStatus;
    }

    //System.out.println("parseArcStation: \"" + stnStr + "\"");

    site = ParseString.getString(stnStr, 0, 5);
    net = ParseString.getString(stnStr, 5, 7);
    chnl = ParseString.getChar(stnStr, 8);
    chan = ParseString.getString(stnStr, 9, 12);
    prmk = ParseString.getString(stnStr, 13, 15);
    pfm = ParseString.getChar(stnStr, 15);
    pwta = ParseString.getInt(stnStr, 16, 17);
    year = ParseString.getInt(stnStr, 17, 21);
    month = ParseString.getInt(stnStr, 21, 23);
    day = ParseString.getInt(stnStr, 23, 25);
    hr = ParseString.getInt(stnStr, 25, 27);
    mn = ParseString.getInt(stnStr, 27, 29);
    psec = ParseString.getIntToFloat(stnStr, 29, 34, 2);
    pttr = ParseString.getIntToFloat(stnStr, 34, 38, 2);
    pwtu = ParseString.getIntToFloat(stnStr, 38, 41, 2);
    ssec = ParseString.getIntToFloat(stnStr, 41, 46, 2);
    srmk = ParseString.getString(stnStr, 46, 48);
    swta = ParseString.getInt(stnStr, 49, 50);

    sttr = ParseString.getIntToFloat(stnStr, 50, 54, 2);
    amp = ParseString.getIntToFloat(stnStr, 54, 61, 2);
    ampunits = ParseString.getInt(stnStr, 61, 63);
    swtu = ParseString.getIntToFloat(stnStr, 63, 66, 2);
    pdelay = ParseString.getIntToFloat(stnStr, 66, 70, 2);
    sdelay = ParseString.getIntToFloat(stnStr, 70, 74, 2);
    dist = ParseString.getIntToFloat(stnStr, 74, 78, 1);
    eangle = ParseString.getIntToFloat(stnStr, 78, 81, 0);
    ampmagwt = ParseString.getInt(stnStr, 81, 82);
    durmagwt = ParseString.getInt(stnStr, 82, 83);
    period = ParseString.getIntToFloat(stnStr, 83, 86, 2);
    chlrmk = ParseString.getChar(stnStr, 86);
    taud = ParseString.getIntToFloat(stnStr, 87, 91, 0);
    azes = ParseString.getIntToFloat(stnStr, 91, 94, 0);
    durmag = ParseString.getIntToFloat(stnStr, 94, 97, 2);
    ampmag = ParseString.getIntToFloat(stnStr, 97, 100, 2);
    impp = ParseString.getIntToFloat(stnStr, 100, 104, 3);
    imps = ParseString.getIntToFloat(stnStr, 104, 108, 3);
    datasrccode = ParseString.getChar(stnStr, 108);
    durmagcode = ParseString.getChar(stnStr, 109);
    // 06/13/2005 aww needs fix below for extra data:
    ampmagcode = ParseString.getChar(stnStr, 110);
    loc = ParseString.getString(stnStr, 111, 113);
    parseStatus = true;
    return parseStatus;

  }

/*
SDD  CI HHLE wPU2200506130621 4388 -23 50                            0    1027 70          272                 01
SDD  CI  HLE wPU2200506130621 4388 -23 30    0   0   0      0 0  0   0   01030 7000  0    0273  0  0   3   0      0
*/
  public String toArcStationString() {
    StringBuffer sb = new StringBuffer(112);
    //Concat cc = new Concat();
    try {
      sb = sb.append(site.substring(0,5));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcStationString: String out of bounds error for site:\"" + site + "\"");
    }
    try {
      sb = sb.append(net.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcStationString: String out of bounds error for net:\"" + net + "\"");
    }
    sb = sb.append(' ');
    sb = sb.append(chnl);
    try {
      sb = sb.append(chan.substring(0,3));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcStationString: String out of bounds error for chan:\"" + chan + "\"");
    }
    sb = sb.append(' ');
    try {
      sb = sb.append(prmk.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcStationString: String out of bounds error for prmk:\"" + prmk + "\"");
    }
    sb = sb.append(pfm);
    sb = Concatenate.format(sb, (long) pwta, 1);
    sb = Concatenate.format(sb, (long) year, 4);
    sb = Concatenate.format(sb, (long) month, 2, 2);
    sb = Concatenate.format(sb, (long) day, 2, 2);
    sb = Concatenate.format(sb, (long) hr, 2, 2);
    sb = Concatenate.format(sb, (long) mn, 2, 2);
    sb = Concatenate.format(sb, (long) Math.round(psec*100.), 5);
    sb = Concatenate.format(sb, (long) Math.round(pttr*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(pwtu*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(ssec*100.), 5);
    try {
      sb = sb.append(srmk.substring(0,2));
    }
    catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("toArcStationString: String out of bounds error for srmk:\"" + srmk + "\"");
    }
    sb = sb.append(' ');
    sb = Concatenate.format(sb, (long) swta,1);
    sb = Concatenate.format(sb, (long) Math.round(sttr*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(amp*100.), 7);
    sb = Concatenate.format(sb, (long) ampunits, 2);
    sb = Concatenate.format(sb, (long) Math.round(swtu*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(pdelay*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(sdelay*100.), 4);
    sb = Concatenate.format(sb, (long) Math.round(dist*10.), 4);
    sb = Concatenate.format(sb, (long) eangle, 3);
    sb = Concatenate.format(sb, (long) ampmagwt, 1);
    sb = Concatenate.format(sb, (long) durmagwt, 1);
    sb = Concatenate.format(sb, (long) Math.round(period*100.), 3);
    sb.append(chlrmk);
    sb = Concatenate.format(sb, (long) taud, 4);
    sb = Concatenate.format(sb, (long) azes, 3);
    sb = Concatenate.format(sb, (long) Math.round(durmag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(ampmag*100.), 3);
    sb = Concatenate.format(sb, (long) Math.round(impp*1000.), 4);
    sb = Concatenate.format(sb, (long) Math.round(imps*1000.), 4);
    sb.append(datasrccode);
    sb.append(durmagcode);
    sb.append(ampmagcode);
    sb.append(loc); // 2-char loc code for sta

    return sb.toString();

  }

  public void printArcStationString() {
    System.out.println(toArcStationString());
  }

  public static void printTitle() {
    System.out.println(titleStr);
  }

  public static String getTitle() {
    return titleStr;
  }

  public String getFormattedStationString() {
    if (! parseStatus) return "";
    StringBuffer sb = new StringBuffer(132);
    //Concat cc = new Concat();
    sb = sb.append(site);
    sb = sb.append(" ");
    sb = sb.append(net);
    sb = sb.append(" ");
    sb = sb.append(chan);
    sb = sb.append(" ");
    sb = sb.append(loc); // moved loc to here 12/7/2005 -aww
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) year, 4, 4) ;
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) month, 2, 2) ;
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) day, 2, 2) ;
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) hr, 2, 2) ;
    sb = sb.append(":");
    sb = Concatenate.format(sb, (long) mn, 2, 2) ;
    sb = sb.append(":");
    sb = Concatenate.format(sb, (double) psec, 2, 2);
    sb = Concatenate.format(sb, (double) dist, 4, 1);
    sb = Concatenate.format(sb, (long) azes, 4);
    sb = Concatenate.format(sb, (long) eangle, 4);
    sb = sb.append(" ");
    sb = sb.append(prmk);
    sb = sb.append(pfm);
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) pwta, 2) ;
    sb = Concatenate.format(sb, (double) pttr, 4, 2);
    sb = Concatenate.format(sb, (double) pwtu, 3, 2);
    sb = sb.append(" ");
    sb = sb.append(srmk);
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) swta, 2) ;
    sb = Concatenate.format(sb, (double) ssec, 4, 2);
    sb = Concatenate.format(sb, (double) sttr, 4, 2);
    sb = Concatenate.format(sb, (double) swtu, 3, 2);
    sb = Concatenate.format(sb, (double) impp, 3, 3);
    sb = Concatenate.format(sb, (double) imps, 3, 3);
    return sb.toString();
  }

  public String getTableString() {
    if (! parseStatus) return "";
    StringBuffer sb = new StringBuffer(132);
    //Concat cc = new Concat();
    sb = sb.append(site);
    sb = sb.append(" ");
    sb = sb.append(net);
    sb = sb.append(" ");
    sb = sb.append(chan);
    sb = sb.append(" ");
    sb = sb.append(loc); // moved loc to here 12/7/2005 -aww
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) year, 4, 4) ;
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) month, 2, 2) ;
    sb = sb.append("-");
    sb = Concatenate.format(sb, (long) day, 2, 2) ;
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) hr, 2, 2) ;
    sb = sb.append(":");
    sb = Concatenate.format(sb, (long) mn, 2, 2) ;
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (double) psec, 3, 2, 2);
    sb = Concatenate.format(sb, (double) dist, 4, 1, 3);
    sb = Concatenate.format(sb, (long) azes, 4, 3);
    sb = Concatenate.format(sb, (long) eangle, 4, 3);
    sb = sb.append(" ");
    sb = sb.append(prmk);
    sb = sb.append(pfm);
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) pwta, 2) ;
    sb = Concatenate.format(sb, (double) pttr, 4, 2);
    sb = Concatenate.format(sb, (double) pwtu, 3, 2);
    sb = sb.append(" ");
    if (srmk.trim().length() == 0) sb = sb.append("--");
    else sb = sb.append(srmk);
    sb = sb.append(" ");
    sb = Concatenate.format(sb, (long) swta, 2) ;
    sb = Concatenate.format(sb, (double) ssec, 4, 2, 2);
    sb = Concatenate.format(sb, (double) sttr, 4, 2);
    sb = Concatenate.format(sb, (double) swtu, 3, 2);
    sb = Concatenate.format(sb, (double) impp, 3, 3);
    sb = Concatenate.format(sb, (double) imps, 3, 3);
    // 06/13/2005 -aww added extra fields below
    sb.append(durmagcode);
    sb.append(ampmagcode);
    return sb.toString();
  }

  public String getFormattedStationString(String stnStr) {
    parseArcStation(stnStr);
    return getFormattedStationString();
  }

  public void printFormattedStation(String stnStr) {
    System.out.println(getFormattedStationString(stnStr));
  }

  public void printFormattedStation() {
    System.out.println(getFormattedStationString());
  }

/*
    public static void main(String args[]) {
     String str = "SDD  CI  HLE wPU2200506130621 4388 -23 30    0   0   0      0 0  0   0   01030 7000  0    0273  0  0   3   0      0     ";

     ArcStation as = new ArcStation();

     Phase ph = HypoFormat.parseArcToPhase(str);

     as.parseArcStation(str);
     System.out.println("   " +as.titleStr);
     System.out.println("R: " +str);
     System.out.println("A: " +as.toArcStationString());
     System.out.println("P: " +HypoFormat.toArchiveString(ph));
     System.out.println(as.getTableString());
    }
*/
}

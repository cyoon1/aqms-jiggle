package org.trinet.formats;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 */

import java.io.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
/**
Read and parse a station list in N.S.C.L format into jasi Channel objects.
Comment lines beginning with "#" are allowed. The elements of the full
SEED channel name can be delimited with any in the set "./ :,".
 <p>

<pre>
NP.ABC.HHE.--
CI/SEGS/BHZ/01
CI SEGS BHZ 01
</pre>
*
*/

public class NSCLformat extends AdHocFormat {

/** Min usable line length. Enough to get elevation but doesn't require (or read) long name.
 * Used to prevent parse errors. */
  protected static final int MIN_LINE_LENGTH = 10;

/** The file comment character "#". */
  protected static final String COMMENT_CHAR = "#";

  /*
  public static void main(String[] args) {
    //String filename = "C:\\temp\\station.list";
    String filename = "C:/temp/station.lis";
    System.out.println("Reading "+filename+"...");
    BenchMark bm = new BenchMark();
    ChannelList list =  NSCLformat.readInFile(filename);
    bm.print("Read in "+list.size());
    System.out.println(list.toString());

  }
  */

  /**
   * Parse a SNCL from single line.
   * The channel's SNCL must follow these rules:<br>
   * 1) lines in the file must be terminated by ('\n'), ('\r'), or ('\n\r') <br>
   * 2) be the first thing on each line<br>
   * 3) have first four parts in this order - net, station, channel, location<br>
   * 4) the parts must be delimited by one character in the set "./ :," <p>
   *
   * If the delimited string is malformed or incomplete the returned ChannelName
   * object may also be malformed or incomplete.
   */
  public static Channelable parse (String str) {
    String dlm = "./ :,";  // list of delimeters

    ChannelName cn = null;
    //str.replaceAll("[  ]", " ");
    cn = ChannelName.fromDelimitedString(str, dlm);
    
    // Do some checking for legal ChannelName.
//      if (cn.getNet() == null || cn.getNet().equals("")) return null;
    Channel ch = Channel.create();
    ch.setChannelName(cn);
    return ch;
    }

    // //////////////////////////////////////
    /** Read in station file. */
      public static ChannelList readInFile(File file) {
        try {
          return readInFile(new FileReader(file));
        }
        catch (FileNotFoundException ex) {
          System.err.println(ex.getMessage());
          return null;
        }
    }

  /** Read in station file with this name. */
    public static ChannelList readInFile(String filename) {
      try {
        return readInFile(new FileReader(filename));
      }
      catch (FileNotFoundException ex) {
        System.err.println(ex.getMessage());
        return null;
      }
    }

  /** Read in station file from this Reader. */
    public static ChannelList readInFile(Reader reader) {

      ChannelList list = new ChannelList();
      Channelable chan ;

      try {

        BufferedReader in = new BufferedReader(reader);
        String instr;
        // Continue to read lines while there are still some left to read
        while ((instr = in.readLine()) != null) {
          if (!instr.startsWith(COMMENT_CHAR)){
           if (instr.length() > MIN_LINE_LENGTH) {   // skip comments
            chan = parse(instr);
//          if (chan != null) list.add(chan);    // checking dups was time consuming
            // list.add() checks for instanceof Channelable plus a MasterList
            if (chan != null) list.addWithoutCheck(chan);
           }
          }
        }

        reader.close();

      } catch (Exception ex) {

        System.err.println("* NSCLFormat Error: File input error");
        System.err.println(ex.getMessage());

      }
      return list;
  }
}



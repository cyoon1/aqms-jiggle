package org.trinet.jasi;

import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
/**
 * This object defines a single channel/time-window.
 * The TimeSpan (time bounds) of the window may be set arbitrarily by the caller
 * and is independent of the time-series that may actually be available.
 * Therefore, the TimeSpan time window may
 * have no time series, be incomplete, or may clip a Waveform.<p>
 */

public class ChannelTimeWindow implements Channelable, TimeSpanIF, DateLookUpIF {
    /** Channel description and info */
    Channel chan;

    /** The time window.*/
    TimeSpan ts = new TimeSpan();

    /**
     * Construct a nul view window.
     */
    public ChannelTimeWindow() { }

    /**
     * Construct an empty view window.
     */
    public ChannelTimeWindow(Channel ch) {
        chan =  Channel.create();
        chan.setChannelObj(ch);
    }
    /**
     * Construct an empty view window with defined TimeSpan time bounds.
     */
    public ChannelTimeWindow(Channel ch, double start, double end) {
        this(ch, new TimeSpan(start, end));
    }
    /**
     * Construct an empty view window with defined TimeSpan time bounds.
     */
    public ChannelTimeWindow(Channel ch, TimeSpan ts) {
        this(ch);
        this.ts = new TimeSpan(ts);        // make new instance
    }

    // Channelable interface
    /**
     * Return the channel object associated with this reading.
     */
    public Channel getChannelObj() {
        return chan;
    }

    /** Set the Channel.  Both name and data.*/
    public void setChannelObj(Channel channel) {
       //this.chan = channel; // don't alias here , unless its necessary - aww
       //logic below mirror that of other classes with Channel members
       if (channel == null) this.chan = null; // Do you want to allow null?
       else if (this.chan == null) this.chan = (Channel) channel.clone();
       else this.chan.copy((Channelable)channel);
  /*
       // match the channel to the master list
       if ( MasterChannelList.get() != null) {
          setChannel(channel, MasterChannelList.get());
       }
  */
    }

    /** Reset only the Channel data (lat,lon,z, etc.), not the Channel name. */
    public void setChannelObjData(Channelable chan) { // Channelable interface ??
        // throws null pointer exception if input is null
        if (this.chan == null) this.chan = (Channel) chan.getChannelObj().clone();
        else this.chan.setChannelObjData(chan.getChannelObj()); // set instance data values to input's
    }

    /**
     * Given a Collection of Channels (i.e. a station list), find the one that
     * matches this WFViews's current Channel. Replace the WFView's Channel object
     * with the "found" Channel which has complete Channel location & response info.
     * This is necessary for distance calcs, sorting, magnitudes, etc.  Return
     * 'true' if match was found, else return 'false'.  */
    public boolean copyChannel(Collection chanList) {
        if (chanList == null) return false;
        Channel [] chArray = (Channel []) chanList.toArray(new Channel[chanList.size()]);
        return copyChannel(chArray);
    }
    /**
     * Given an array of Channels (i.e. a station list), find the one that matches
     * this WFViews's current Channel. Replace the WFView's Channel object with the
     * "found" Channel which has complete Channel location & response info.  This is
     * necessary for distance calcs, sorting, magnitudes, etc.  Return 'true' if
     * match was found, else return 'false'.  */
    public boolean copyChannel(Channel[] ch) {

        // No efficient way to do this unless we put Channel list in hash table
        for (int i = 0; i<ch.length; i++)
        {
            if (chan.equalsNameIgnoreCase(ch[i])) {
                /*
                // VH_/EH_ kludge
                //String origChan = chan.getChannel();
                chan = ch[i];                // substitute
                // rest of the VH_/EH_ kludge
                //                System.out.println ("1) "+chan.toString());
                chan.setChannel(origChan);
                //                System.out.println ("2) "+chan.toString());
                */
                chan.setChannelObjData(ch[i]); // only copy channel supporting data -aww 04/07/2005
                return true;
            }
        }
        return false;
    }

    /**
     * Dump some info about the view for debugging
     */
    public void dump() {
        System.out.println (toString());
    }

    /**
     * Dump some info about the view for debugging
     */
    public String toString() {
        //return chan.toString() + "  " + ts.toString();
        Format f51 = new Format("%5.1f");
        return chan.toDelimitedSeedNameString() + "  " + ts.toString() + " " + f51.form(chan.getHorizontalDistance());
    }

// /////////////////////////////////////////////////////////
//    Channelable interface

  public double calcDistance(GeoidalLatLonZ loc) { // changed arg type - aww 06/11/2004
    return chan.calcDistance(loc);
  }

  /** Set the true (slant) distance. */
  public void   setDistance(double distance) {
    chan.setDistance(distance);
  }
  /** Get the true (slant) distance. */
  public double getDistance() {
    return chan.getDistance();
  }
  /** Set the map (plan) distance. */
  public void   setHorizontalDistance(double distance) {
    chan.setHorizontalDistance(distance) ;
  }
  /** Get the map (plan) distance. */
  public double getHorizontalDistance() {
    return chan.getHorizontalDistance();
  }
  public double getVerticalDistance() {
    return chan.getVerticalDistance();
  }
  public void   setAzimuth(double azimuth) {
    chan.setAzimuth(azimuth);
  }
  public double getAzimuth() {
    return chan.getAzimuth();
  }

  /**
   * Case insensitive compare.
   * Returns true if this and the input's "net and sta" are the equal.
   */
  public boolean sameNetworkAs(Channelable id) {
    return (id == this) ? true : chan.sameNetworkAs(id);
  }
  public boolean sameStationAs(Channelable id) {
    return (id == this) ? true : chan.sameStationAs(id);
  }
  public boolean sameSeedChanAs(Channelable id) {
    return (id == this) ? true : chan.sameSeedChanAs(id);
  }
  public boolean sameTriaxialAs(Channelable id) {
    return (id == this) ? true : chan.sameTriaxialAs(id);
  }
  public boolean equalsChannelId(Channelable id) {
    return (id == this) ? true : chan.equalsChannelId(id);
  }
    public boolean setChannelObjFromMasterList(java.util.Date aDate) {
      return getChannelObj().setChannelObjFromMasterList(aDate);
    }
    public boolean setChannelObjFromList(ChannelList chanList, java.util.Date aDate) {
      return getChannelObj().setChannelObjFromList(chanList, aDate);
    }
    public boolean matchChannelWithDataSource(java.util.Date aDate) {
      return getChannelObj().matchChannelWithDataSource(aDate);
    }

    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedSeedNameString(String delimiter) {
        return getChannelObj().toDelimitedSeedNameString(delimiter);
    }
    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedNameString(String delimiter) {
        return getChannelObj().toDelimitedNameString(delimiter);
    }
// Convenience methods - wrappers around interfaces methods:
    public java.util.Date getLookUpDate() {
      return ( ts.isValid() ) ?  // use for lookUp- aww
            new DateTime(ts.getStart(), true) : new java.util.Date(); // new constructor for UTC ts -aww 2008/02/11
    }

    public boolean setChannelObjFromMasterList() {
      return setChannelObjFromMasterList(getLookUpDate());
    }
    public boolean setChannelObjFromList(ChannelList aList) {
      return setChannelObjFromList(aList, getLookUpDate());
    }
    public boolean matchChannelWithDataSource() {
      return matchChannelWithDataSource(getLookUpDate());
    }

// /////////////////////////////////////////////////////////
// TimeSpanIF
/**
 * Set the TimeSpan to the given value. This is used to align traces by
 * setting all their viewSpans equal.
 */
  public void setTimeSpan(TimeSpan ts) {
    this.ts = new TimeSpan(ts);
  }
//  public void setViewSpan(TimeSpan ts) {
//    this.ts = new TimeSpan(ts);
//  }
  /** Input values are true epoch with leap seconds.*/
  public void setLimits(double startDatetime, double endDatetime) {
    ts.setLimits(startDatetime, endDatetime);
  }
  public TimeSpan getTimeSpan() {
    return ts;
  }
  /** Input value is true epoch with leap seconds.*/
  public void setStart(double datetime) {
    ts.setStart(datetime);
  }
  /** Input value is true epoch with leap seconds.*/
  public void setEnd(double datetime)  {
    ts.setEnd(datetime);
  }
  /** Return true epoch seconds.*/
  public double getStart() {
    return ts.getStart();
  }
  /** Return true epoch seconds.*/
  public double getEnd() {
    return ts.getEnd();
  }
  public boolean isNull() {
    return ts.isNull();
  }
  /** Input value is true epoch with leap seconds.*/
  public boolean after(double datetime) {
    return ts.after(datetime);
  }
  /** Input value is true epoch with leap seconds.*/
  public boolean before(double datetime) {
    return ts.before(datetime);
  }
  /** Input value is true epoch with leap seconds.*/
  public boolean excludes(double datetime) {
    return ts.excludes(datetime);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean excludes(double startDatetime, double endDatetime) {
    return ts.excludes(startDatetime, endDatetime);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean excludes(TimeSpanIF timeSpan){
    return ts.excludes(timeSpan);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean contains(double startDatetime, double endDatetime) {
    return ts.contains(startDatetime, endDatetime);
  }
  /** Input value is true epoch with leap seconds.*/
  public boolean contains(double datetime) {
    return ts.contains(datetime);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean contains(TimeSpanIF timeSpan) {
    return ts.contains(timeSpan);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean overlaps(double startDatetime, double endDatetime) {
    return ts.overlaps(startDatetime, endDatetime);
  }
  /** Input values are true epoch with leap seconds.*/
  public boolean within(double startDatetime, double endDatetime) {
    return ts.within(startDatetime, endDatetime);
  }
  /** Input value is true epoch with leap seconds.*/
  public void include(double datetime) {
    ts.include(datetime);
  }
  public double getDuration() {
    return ts.getDuration();
  }
  public double getCenter() {
    return ts.getCenter();
  }

} // end of class

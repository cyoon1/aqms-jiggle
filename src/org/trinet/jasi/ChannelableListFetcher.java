package org.trinet.jasi;

/**
 * Fetch a channelable list from the DataSource.
 *
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 *
   *************************************
   THIS IS NOT COMPLETE -- BEGUN 4/3/03
   *************************************
 */
public abstract class ChannelableListFetcher extends JasiObject {

  public static final int INCLUDE = 0;
  public static final int EXCLUDE = 1;

  /**
   * Instantiate an object of this type. You do
   * NOT use a constructor. This "factory" method creates various
   * concrete implementations. Creates a Solution of the DEFAULT type.
   * @See: JasiObject
   */
   public static final ChannelableListFetcher create() {
     return create(DEFAULT);
   }

  /**
   * Instantiate an object of this type. You do
   * NOT use a constructor. This "factory" method creates various
   * concrete implementations. The argument is an integer implementation type.
   * @See: JasiObject
   */
   public static final ChannelableListFetcher create(int schemaType) {
      return create(JasiFactory.suffix[schemaType]);
   }

  /**
   * Instantiate an object of this type. You do
   * NOT use a constructor. This "factory" method creates various
   * concrete implementations. The argument is as 2-char implementation suffix.
   * @See: JasiObject
   */
   public static final ChannelableListFetcher create(String suffix) {
     return (ChannelableListFetcher) JasiObject.newInstance("org.trinet.jasi.ChannelableListFetcher", suffix);
     }

  abstract public void setClass (JasiObject obj);

     /** Set the list of net codes to either require or exclude.<p>
      If inexclude = INCLUDE only Objects with a net type found in the list will be returned.<br>
      If inexclude = EXCLUDE only Objects with a net type NOT found in the list will be returned. */
  abstract public void setNetList (String[] list, int inexclude) ;

     /** Set the list of station codes to either require or exclude.<p>
      If inexclude = INCLUDE only Objects with a net type found in the list will be returned.<br>
      If inexclude = EXCLUDE only Objects with a net type NOT found in the list will be returned. */
  abstract public void setStationList (String[] list, int inexclude) ;

     /** Set the list of channel codes to either require or exclude.<p>
      If inexclude = INCLUDE only Objects with a net type found in the list will be returned.<br>
      If inexclude = EXCLUDE only Objects with a net type NOT found in the list will be returned. */
  abstract public void setChannelList (String[] list, int inexclude) ;

  /** Set the list of location codes to either require or exclude.<p>
   If inexclude = INCLUDE only Objects with a net type found in the list will be returned.<br>
      If inexclude = EXCLUDE only Objects with a net type NOT found in the list will be returned. */
  abstract public void setLocationList (String[] list, int inexclude) ;

  /** Go fetch the list as defined by the setXxxx() methods. */
  abstract public ChannelableList fetch();
}
package org.trinet.jasi;

/**
 * This is a QC model that answers a yes/no question. Is this solution good
 * enough to finalize or does it need futher work.
 *
 * @author Doug Given
 * @version */

public abstract class QualityControlModel extends JasiObject {

    /** A static instance that is return by getInstance() so many modules can
     * share it without passing a referenece. */
    static QualityControlModel QCmodel = QualityControlModel.create();

/*    // block use of constructor
    QualityControlModel() {}
* DK3 removed constructors from all abstract classes
********************************/

// -- Concrete FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate an object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of object that is appropriate for your site.    */
// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final QualityControlModel create() {
	return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final QualityControlModel create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final QualityControlModel create(String suffix) {
	return (QualityControlModel) JasiObject.newInstance("org.trinet.jasi.QualityControlModel", suffix);
     }
// ////////////////////////////////////////////////////////////////////////////


    /** Return the static instance of QualityControlModel so many modules can
     * share an instance without passing a referenece.*/
    public QualityControlModel getInstance() {
	return QCmodel;
    }

    /** Return 'true' if the event is "good" according to the quality model */
    public abstract boolean isGood(Solution sol);

    /** Return 'true' if the event is "bad" according to the quality model.
     * This is just the negation of isGood() for convenience. */
    public boolean isBad (Solution sol) {
	return !isGood(sol);
    }

    /** Return a string describing what's wrong with the event. */
    public abstract String getHintString (Solution sol);


} // QualityControlModel

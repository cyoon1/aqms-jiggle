package org.trinet.jasi;

/**
 * Exception class that gets thrown when a method is passed an inapproriate
 * Phase object for its processing.
 *
*/
public class WrongPhaseTypeException extends WrongDataTypeException {
    public WrongPhaseTypeException(String msg) {
	super(msg);
    }    
} // WrongPhaseTypeException

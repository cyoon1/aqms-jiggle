/*
earthquake
explosion
tremor/tornillo
earth movement
atmospheric
earth movement
impact
unknown
*/
package org.trinet.jasi;
public class EventType {

    public static final double DEFAULT_FIX_DEPTH = Double.NaN;

    public String code;
    public String name;
    public String desc;
    public String group;
    public double fixDepth;

    public EventType() { }

    // Default fix depth, implies dont' fix !!
    public EventType(String code, String name, String desc, String group) {
       this(code, name, desc, group, DEFAULT_FIX_DEPTH);
    }

    public EventType(String code, String name, String desc, String group, double fixDepth) {
      this.code = code;
      this.name = name;
      this.desc = desc;
      this.group = group;
      this.fixDepth = fixDepth;
    }

    public EventType(EventType et) {
      this.code = et.code;
      this.name = et.name;
      this.desc = et.desc;
      this.group = et.group;
      this.fixDepth = et.fixDepth;
    }
   
    public boolean isDepthFixed() {
        return ! Double.isNaN(fixDepth);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(code).append(" ").append(name).append(" ").append(desc).append(" ").append(group).append(" ").append(fixDepth);
        return sb.toString();
    }
}

package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/** Simple response of a single channel. 
* Gain is usually the relationship between digital counts and actual ground motion
* for a seismic sensor. <p>
* For example, CI_CBK_EHZ is a short-period velocity transducer with e a gain of
* 8966788 and units of 'Units.CntCMS' ("counts/(cm/sec)"). So one cm/sec equals
* 8966788 digital counts or one count = 1/8966788 cm/sec. Thus, this is a
* very high-gain station (i.e. it magnifies the ground motion alot.<p>
* CI_CTC_HLZ is an accelerometer. It has a gain of 5356 and units of
* "counts/(cm/sec2)". Its a strong motion instrument (low-gain). */

public abstract class ChannelResponse extends AbstractChannelData implements Cloneable, java.io.Serializable {

     protected ChannelGain gain = new ChannelGain();

     //
     protected DataDouble damping      = new DataDouble();
     protected DataDouble natFreq      = new DataDouble();
     protected DataDouble loCutFreq    = new DataDouble();
     protected DataDouble hiCutFreq    = new DataDouble();
     //
     //protected DataDouble sensitivity  = new DataDouble();

    protected ChannelResponse() {}

    protected ChannelResponse(ChannelIdIF id) {
        super(id);
    }
    protected ChannelResponse(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected ChannelResponse(ChannelIdIF id, DateRange dateRange, ChannelGain gain) {
        super(id, dateRange);
        this.gain = gain;
    }
    protected ChannelResponse(ChannelIdIF id, DateRange dateRange, double gain, int gainUnits) {
        super(id, dateRange);
        this.gain = new ChannelGain(gain, gainUnits);
    }

    /*
    protected ChannelResponseTN(ChannelIdIF id, DateRange dateRange, double gain, int gainUnits,
                   double natFreq, double damping, double loCut double hiCut, double sensitivity) {
        set(id, dateRange, gainValue, gainUnits, natFreq, damping, loCut, hiCut, sensitivity);
    }
    public void set(double gainValue, int gainUnits, double natFreq, double damping, double loCut,
                    double hiCut, double sensitivity) {
        this.gain = new ChannelGain(gain, gainUnits);
        //this.gain.setValue(gain);
        //this.gainUnits = gainUnits;
        this.damping.setValue(damping);
        this.natFreq.setValue(natFreq);
        this.loCutFreq.setValue(loCut);
        this.hiCutFreq.setValue(hiCut);
        this.sensitivity.setValue(sensitivity);

    }
    */
    public static final ChannelResponse create() {
        return create(JasiObject.DEFAULT);
    }
    public static final ChannelResponse create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final ChannelResponse create(String suffix) {
        return (ChannelResponse)
            JasiObject.newInstance("org.trinet.jasi.ChannelResponse", suffix);
    }

    public static ChannelResponse getByChannel(Channel chan, java.util.Date date) {
        return (ChannelResponse)
           ChannelResponse.create().getByChannelId(chan.getChannelObj().getChannelId(), date);
    }

    public boolean copy(ChannelDataIF cd) {
      if (cd instanceof ChannelResponse) return false;
      ChannelResponse response = (ChannelResponse) cd;
      this.gain.setValue(response.getGainValue());
      this.gain.setUnits(response.getGainUnits());
      return true;
    }

    /** Return the units string, e.g. "counts/(cm/sec)", "counts/(cm/sec2)"
    * @See: Units */
    public String getGainUnitsString() {
      return gain.getUnitsString();
    }

    /** Return the value of the units type.
    *
    * @See: Units
    */
    public int getGainUnits() {
      return gain.getUnits();
    }
    /** Set the units of the gain. Returns false is the string is not a legal
    * units description.
    * @See: Units */
    public void setGainUnits(int iunits) {
      gain.setUnits(iunits);
    }
    /** Set the units of the gain. Returns false is the string is not a legal
    * units description.
    * @See: Units */
    public boolean setGainUnits(String sunits) {
       return gain.setUnits(sunits);
    }

    /** Return true if the gain value is null. */
    public boolean isGainNull () {
      return gain.isNull();
    }

    /** Return the gain value. Returns NaN if the value is null.*/
    public double getGainValue() {
      return gain.doubleValue();
    }

    /** Set the gain to this value. Units should be set also but will default to
    * counts/(cm/sec). */
    public void setGainValue(double gainValue) {
      gain.setValue(gainValue);
    }
    public void setGain(ChannelGain gain) {
      this.gain = gain;
    }
    public ChannelGain getGain() {
      return gain;
    }

    public String toString(){
      return super.toString() + " " + gain.toString();
    }

    public Object clone() {
      ChannelResponse resp = (ChannelResponse) super.clone();
      resp.gain = (ChannelGain) gain.clone();
      return resp;
    }
    public String toNeatString() { return toString(); }
    public String getNeatHeader() { return getNeatStringHeader(); }
    public static String getNeatStringHeader() {
        return "      Gain  GainUnits";
    }

    //add below clipAmp data and methods to ChannelGain?
    protected  int clipAmpUnits = Units.COUNTS;
    protected  DataDouble clipAmp = new DataDouble();
    abstract public double getMaxAmpValue();
    abstract public double getDefaultClipAmp();
    public double getClipAmpValue() {
        //return (clipAmp.isNull()) ? getDefaultClipAmp() : clipAmp.doubleValue();
        return (clipAmp.isNull()) ? 0. : clipAmp.doubleValue(); // assume no default - aww 2009/03/10
    }
}

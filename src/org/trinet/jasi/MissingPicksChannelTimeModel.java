package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;

public class MissingPicksChannelTimeModel extends DataSourceChannelTimeModel {

    public static final String defModelName = "Missing Picks"; 

    public static final String defExplanation = "channels associated with database waveforms without associated data (e.g. arrivals, amps, codas)";
    
    { 
        setModelName(defModelName);
        setExplanation(defExplanation);
    }


    public MissingPicksChannelTimeModel() {
        setMyDefaultProperties();
    }

    public MissingPicksChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public MissingPicksChannelTimeModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public MissingPicksChannelTimeModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public MissingPicksChannelTimeModel(Solution sol) {
        this();
        super.setSolution(sol);
    }


    public ChannelableList getChannelTimeWindowList() {
        if ( ! (includePhases || includeMagAmps || includeCodas || includePeakAmps) ) {
          System.err.println(getModelName() + " : ERROR Required model include properties are not set, no-op");
        }
        return super.getChannelTimeWindowList();
    }

     /** Return a Collection of Waveform objects selected by the model.
     * They will not contain time-series yet. To load time-series you must set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object)
     * and then invoke the loadTimeSeries() of each Waveform instance.<p>
     * @see: AbstractWaveform
     */
    public List getWaveformList() {
        List wfList = super.getWaveformList();
        if (wfList == null) return wfList;
        int count = wfList.size();
        Waveform wf = null;
        ChannelableList outList = new ChannelableList(count);
        for (int i = 0; i < count; i++) {
          wf = (Waveform) wfList.get(i);
          if ( requiredList.getIndexOf(wf) < 0) outList.add(wf);
        }
        //if (maxDist < Double.MAX_VALUE) outList.distanceSort(sol);
        return outList;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        includePhases = true;   // default is phases only
        includeAllComponents = false;
        includeMagAmps = false;
        includePeakAmps = false;
        includeCodas = false;
    }
}

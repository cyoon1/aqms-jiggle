package org.trinet.jasi;

import javax.swing.SwingUtilities;
import javax.swing.event.*;
import java.util.*;

/**
 * Provides base functionality for all jasi objects. Provides methods to
 * register, unregister ChangeListener events. How they are used depends on the
 * extending jasi class which must fire the change events.  <p>
 *
 * Classes that extend this class should implement create() methods.<br>
 * Examples: assumes Solution extends JasiObject
 *
 * <tt>
 *    Solution sol = Solution.create("TN");
      Solution sol = Solution.create();   // uses DEFAULT type
      Solution sol = Solution.create(JasiObject.TRINET);
  <\tt>
 *
  * @see: JasiFactory
 * */
public abstract class JasiObject extends Object
     implements JasiFactory, Cloneable, java.io.Serializable {

    /** Option flag determines the amount of text, if any, to be printed
     * via System.err or System.out PrintStreams by implemented methods of
     * concrete subclasses.
     * Default is false, not verbose.
     * */
    protected boolean verbose = false;

    // Support of state change events
    RunnableChangeNotifier threadSafeStateChange = null;
    EventListenerList listenerList = null;

    public Object clone() {
      JasiObject jo = null;
      try {
        jo = (JasiObject) super.clone();
        jo.threadSafeStateChange = null;
        jo.listenerList = null;
      }
      catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
      }
      return jo;
    }

    /** The default schema type. This is used if no schema type is specified in
     * factory methods. It can be set using JasiObject.setDefault(int schemaType).
     * DEFAULT is set to "TRINET" by default.<p>
     * @see: JasiFactory
     */
    public static int DEFAULT = TRINET;

    /**
     * Constructor is 'protected' so that jasi objects can only be instantiated
     * using the 'create()' factory method.
     */
    protected JasiObject() { }

    /** Set verbose output flag. */
    public void setVerbose(boolean tf) {
      verbose = tf;
    }
    /** Get verbose output flag. */
    public boolean getVerbose() {
      return verbose;
    }

    /** Builds a classname by concatinating bassClass and suffix.
     *  The baseClass must be fully qualified, that is, include the package.
     *  For example: "org.trinet.jasi.Solution". The suffix must be a two-character
     *  string that represents a valid concrete jasi implementation. E.g. "TN".
     *  Returns a new instance of the given class. Note that the instance is
     *  returned as an Object and must be cast to the proper class type by
     *  the caller. Returns null if the class does not exist or can't be created.<p>
     *  @See: JasiFactory. */
    public static Object newInstance(String baseClass, String suffix) {
        String sBasePath;
        String sBaseClass;
        int iIndexOfLastPeriod = baseClass.lastIndexOf('.');

        if(iIndexOfLastPeriod < 0)
          return(null);
        else
        {
          sBasePath = baseClass.substring(0,iIndexOfLastPeriod);
          sBaseClass = baseClass.substring(iIndexOfLastPeriod + 1);
          return newInstance(sBasePath + "." + suffix + "." + sBaseClass
                             + suffix);
        }
    }

    /** 
     *  The classname must be fully qualified, that is, include the package.
     *  For example: "org.trinet.jasi.SolutionTN"
     *  It must represent a valid concrete jasi implementation.
     *  Returns a new instance of the named class. Note that the instance is
     *  returned as an Object and must be cast to the proper class type by
     *  the caller.  Returns null if the class does not exist or can't be created.
     */
    public static Object newInstance(String className) {

        Object newClass = null;

        try {
            newClass =  Class.forName(className).newInstance();
        }
        catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return newClass;

    }
    // ** should throw 'noSuchJasiInterface' exception ?

    /**
     * Set the default data source type using a string like 'TRINET'. The string
     * is not case sensititive. This type will be used for any future call to
     * <jasi-object>.create(). You may still explicitely create a jasi object by
     * using <jasi-object>.create(int dataType). Returns true if successful and
     * false if not (probably a bad schema type)<p> Legal jasi-object types are
     * defined in JasiFactory.java.<p>
     *
     * @See: JasiFactory */
    public static boolean setDefault(String dataType) {

        for (int i = 0; i < typeString.length; i++)
        {
            if (dataType.equalsIgnoreCase(typeString[i])) {
                setDefault(typeValue[i]);
                return true;
            }
        }
        return false;
    }
    /**
     * Set the default data source type using a suffix string like 'TN'. The string
     * is not case sensititive. This type will be used for any future call to
     * <jasi-object>.create(). You may still explicitely create a jasi object by
     * using <jasi-object>.create(int dataType). Returns true if successful and
     * false if not (probably a bad schema type)<p> Legal jasi-object types are
     * defined in JasiFactory.java.<p>
     *
     * @See: JasiFactory */
    public static boolean setDefaultBySuffix(String suffixIn) {

        for (int i = 0; i < suffix.length; i++)
        {
            if (suffixIn.equalsIgnoreCase(suffix[i])) {
                setDefault(typeString[i]);  // DK Changed 100202
                return true;
            }
        }
        return false;
    }
    /**
     * Set the default data source type using an int constant like
     * 'JasiObject.EW'. This type will be used for any future call to
     * <jasi-object>.create(). You may still explicitely create a jasi object by
     * using <jasi-object>.create(int dataType). Returns true if successful and
     * false if not (probably a bad schema type)<p> Legal jasi-object types are
     * defined in JasiFactory.java.<p>
     *
     * @See: JasiFactory */
    public static boolean setDefault(int type) {
        if (type >=0 && type < typeValue.length) {
            DEFAULT = type;
            return true;
        }
        return false;
    }

    /**
     * Return the default schema type.<p>
     * @see: JasiFactory
     */
    public static int getDefault() {
        return DEFAULT;

    }
    /**
     * Return the default suffix.<p>
     * @see: JasiFactory
     */
    public static String getDefaultSuffix() {
        return suffix[getDefault()];
    }

    /**
     * Return the default schema type.<p>
     * @see: JasiFactory
     */
    public static String getDefaultString() {
        return typeString[DEFAULT];
    }

// -- FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate a Jasi object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of SeismicEvent that is appropriate for your site's database.
     */
    //   abstract static public JasiObject create();
    //   abstract static public JasiObject create(int schemaType);

    /* NOTE:
     * I can't define these static here because:
     * - Abstract methods can't be static
     * I can't define these non-static here because when you make it static in the child:
     * - cannot hide the instance method of the same signature declared in class
     * org.trinet.jasi.JasiObject
     */

    // ///////////////////////////////////////////////////////

    /** Register listener with list of ChangeListeners */
    public void addChangeListener(ChangeListener l) {
        if (listenerList == null) listenerList = new EventListenerList();
        // insurance to avoid, redundant adds:
        else if (listenerList.getListenerCount() > 0) removeChangeListener(l);
        listenerList.add(ChangeListener.class, l);
    }

    /** Unregister listener from list of ChangeListeners */
    public void removeChangeListener(ChangeListener l) {
        if (listenerList == null) return;
        listenerList.remove(ChangeListener.class, l);
    }

    /** Notify all listeners that have registered interest for notification on
     * this event type.  The event instance is created on each call to reflect the
     * current state. */

    protected void fireStateChanged() {
      fireStateChanged(this);
    }

    protected void fireStateChanged(Object source) {
      if (threadSafeStateChange == null) threadSafeStateChange = new RunnableChangeNotifier();
      threadSafeStateChange.setSource(source);
      SwingUtilities.invokeLater(threadSafeStateChange);
    }

    class RunnableChangeNotifier implements Runnable {
       Object source;
       protected RunnableChangeNotifier() {}
       protected void setSource(Object source) { this.source = source; }
       public void run() {
          // bail if no listeners
          if (listenerList == null || listenerList.getListenerCount() == 0) return;
          // Guaranteed to return a non-null array no NULL check needed
          Object[] listeners = listenerList.getListenerList();

          ChangeEvent changeEvent = new ChangeEvent((source == null) ? this : source);

          // Notify the listeners last to first. Note: 'listeners' is an array of
          // PAIRS of ListenerType/Listener, hence the weird indexing.
          for (int i = listeners.length-2; i>=0; i-=2) {
              if (listeners[i] == ChangeListener.class) {
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
               }
           }
       }

    }
} // JasiObject

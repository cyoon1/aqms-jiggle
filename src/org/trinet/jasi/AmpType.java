package org.trinet.jasi;

/**
 * Static class used to contain enumeration of recognized amplitude types.
 * <pre>
* FROM NCEDC SCHEMA DOC
* Range: amptype = {WA | WAS | WASF | WAC | WAU | PGA | PGV | PGD | IV2 | SP.3 | SP1.0 | SP3.0 | ML100 | ME100 | EGY}
* WA = Wood-Anderson photographic
* WAS = Wood-Anderson synthetic
* WASF = Wood-Anderson synthetic filtered
* WAC = Wood-Anderson corrected
* WAU = Wood-Anderson uncorrected
* PGA = peak ground acceleration
* PGV = peak ground velocity
* PGD = peak ground displacement
* IV2 = integral of velocity squared
* SP.3 = spectral peak
* SP1.0 = spectral peak
* SP3.0 = spectral peak
* ML100 = local magnitude
* ME100 = energy magnitude
* EGY = energy
* HEL = hand measured from helicorder or photo record
* "unknown" added for this code -- not part of schema doc
* </par>
 */
// UNITS Range: units { c | s | mm | cm | m | ms | mss | cms | cmss | mms | mmss | mc | nm | e | iovs | spa | dycm}
// where: 
// c = counts s = seconds mm = millimeters cm = centimeters m = meters
// ms = meters per second mss = meters per second per second cms = centimeters per second
// cmss = centimeters per second per second mms = millimeters per second mmss = millimeters per second per second
// mc = microns nm = nanometers e = ergs iovs = integral of velocity squared spa = spectral peak amplitude
//
public class AmpType  {
    
    static int knt = 0;
    public static final int UNKNOWN = knt++;
    public static final int WAS         = knt++;  // Wood-Anderson - synthetic
    public static final int WASF        = knt++;  // Wood-Anderson - synthetic filtered
    public static final int WA          = knt++;  // Wood-Anderson - from real W-A inst
    public static final int WAC         = knt++;  // Wood-Anderson - corrected
    public static final int WAU         = knt++;  // Wood-Anderson - uncorrected
    public static final int PGA         = knt++;  // peak ground acceleration
    public static final int PGV         = knt++;  // peak ground velocity
    public static final int PGD         = knt++;  // peak ground displacement
    public static final int VI2         = knt++;
    public static final int SP03        = knt++;  // spectral peak 0.3 secs
    public static final int SP10        = knt++;  // spectral peak 1.0 secs
    public static final int SP30        = knt++;  // spectral peak 3.0 secs
    public static final int ML100       = knt++;
    public static final int ME100       = knt++;
    public static final int EGY         = knt++;  // energy
    public static final int HEL         = knt++;  // helicorder, human read
    public static final int M0          = knt++;  // locmag moment

    // Volts? mVolts?

    /* See NCDN parametric schema doc, column 'amptype' */
    private static String typeStr[] = {
      "UNKNOWN",
      "WAS",
      "WASF",
      "WA",
      "WAC",
      "WAU",
      "PGA",
      "PGV",
      "PGD",
      "VI2",
      "SP.3", 
      "SP1.0",    
      "SP3.0",    
      "ML100",    
      "ME100",    
      "EGY",
      "HEL",
      "M0"
    };

    private static final int waUnits = Units.CM;
    //private static final int spaUnits = Units.SPA;
    private static final int spaUnits = Units.CMSS;  // DDG 6/2/08 - use physical units

    /** Define the default units of the given amp type. */
    private static int defUnits[] = {
      Units.UNKNOWN,
      waUnits,
      waUnits,
      waUnits,
      waUnits,
      waUnits,
      Units.CMSS,
      Units.CMS,
      Units.CM,
      Units.UNKNOWN,   // VI2 ??
      spaUnits,
      spaUnits,
      spaUnits,
      Units.UNKNOWN,
      Units.ERGS,
      Units.ERGS,
      Units.CM,
      Units.DYCM
    };

/** Returns default period of amp types that have one. Otherwise returns -1.0 .*/
    public static double getPeriod(int type) {
      if (type == SP03) return 0.3;  // NOTE: that "0.3" is a commonly used shortened version
      if (type == SP10) return 1.0;
      if (type == SP30) return 3.0;
      return -1.0;
    }
/**
 * Return the default units of the given amp type.
 * @see: Units()
*/
    public static int getDefaultUnits(int type) {
      return defUnits[type];
    }
    /** Return true if the passed amptype is already corrected and no static
    * correction should be applied. */
    public static boolean isCorrected (int ampType) {
      return isCorrected (getString(ampType));
    }

    /** Return true if the passed amptype is already corrected and no static
    * correction should be applied. */
    public static boolean isCorrected (String ampType) {
      return ampType.equalsIgnoreCase("WAC");   // add other corrected types here
    }
    
    /** Return a short string describing the amplitude type. Return blank string
     * if type value is not legal. */

    public static String getString(int type) { 
      if (isLegal(type)) return typeStr[type];
      return  typeStr[0];     // unknown
    }

    /** Return an 'int' matching the enumeration value given a short string
    describing the amplitude type.  Returns 0 if no match was found or if string
    = "UNKNOWN". */
    public static int getInt(String type) { 
      for (int i = 0; i < typeStr.length; i++) {
        if (type.equalsIgnoreCase(typeStr[i])) return i;
      }
      return 0;
    }

    /** Return true if value is within legal range of enumeration list. */
    public static boolean isLegal(int type) {
      if (type > typeStr.length || type < 0) return false;
      return true;
    }

    /** Return true if string is a legal enumeration type */
    public static boolean isLegal(String type) {
      return isLegal(getInt(type));
    }

    public static boolean isMoment(String ampType) {
        return ampType.equals("M0");
    }

    public static boolean isMoment(int type) {
      return (type == M0);
    }

    public static boolean isEnergy(int type) {
      return (type == EGY || type == ME100);
    }

    public static boolean isWoodAnderson(int type) {
      return (type == WAS || type == WASF || type == WA  || type == WAC || type ==WAU);
    }

    public static boolean isWoodAnderson(String ampType) {
        return ampType.startsWith("WA");
    }

    public static boolean equalWoodAnderson(String ampType1, String ampType2) {
        return (ampType1.startsWith("WA") && ampType2.startsWith("WA")) ? true : false; 
    }

    public static boolean sameFamily(String ampType1, String ampType2) {
        return (ampType1.substring(0,2).equals(ampType2.substring(0,2))) ? true : false; 
    }

    public static boolean equivalentData(String ampType1, String ampType2) {
        return equivalentData(getInt(ampType1), getInt(ampType2));
    }

    public static boolean equivalentData(int ampType1, int ampType2) {
        //return equivalentData(getString(ampType1) getString(ampType2));
        return (ampType1 == WAS || ampType1 == WASF) //  || ampType1 == WA || ampType1 == WAC || ampType1 == WAU)
               &&
               (ampType2 == WAS || ampType2 == WASF); //  || ampType2 == WA || ampType2 == WAC || ampType2 == WAU);
    }

    // ///////////////////////////////////////////////////////////////////
    // test
    /*
    public static void main (String args[]) {
        System.out.println (" -- amp types --");
        for (int i = 0; i < typeStr.length; i++) {
          System.out.println (i +" "+getString(i)+"  "+getInt(getString(i)) );
        }
        System.out.println (" WAS = "+ AmpType.WAS);
//      System.out.println (" WAU = "+ AmpType.WAU);
    }
    */
} // AmpType

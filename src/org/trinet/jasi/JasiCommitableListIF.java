package org.trinet.jasi;
import java.util.*;
public interface JasiCommitableListIF extends ActiveArrayListIF {
  public boolean hasElements();
  public boolean hasUndeletedElements();
  public boolean hasElements(boolean includeDeleted);
  public JasiCommitableIF getJasiCommitable(int idx);
  public JasiCommitableIF getEquivalent(JasiCommitableIF jc);
  public boolean commit();
  public boolean commitUndeleted();
  public boolean erase(JasiCommitableIF jc);
  public int eraseAll();
  public int eraseAll(List aList);
  public int deleteAll();
  public int deleteAll(List aList);
  public int undeleteAll();
  public int undeleteAll(List aList);
  public int removeAll();
  public int removeDeleted();
  public int removeUndeleted();
  public JasiCommitableListIF getUndeleted();
  public JasiCommitableListIF getDeleted();
  public String toNeatString(); // default no deleted?
  public String toNeatString(boolean includeDeleted);
  public JasiCommitableIF getByIdentifier(Object id); // like DataLong(id)
  public boolean delete(JasiCommitableIF jc);
  public boolean undelete(JasiCommitableIF jc);
  // Should addOrReplace implementations be changed to return null, to signal no-op,
  // instead of returning the input argument jc, else we would need to do contains(jc)?
  // to resolve the ambiguity with adding to list, and avoid wasting time testing.
  public JasiCommitableIF addOrReplace(JasiCommitableIF jc);
  public JasiCommitableIF addOrReplaceById(JasiCommitableIF jc);
  public void addOrReplaceAll(List aList);
  public void addOrReplaceAllById(List aList);
  public JasiCommitableListIF getListByType(Object type); // default no deleted?
  public JasiCommitableListIF getListByType(Object type, boolean undeletedOnly);
  public int indexOfEquivalent(JasiCommitableIF jc);
  public boolean removeEquivalent(JasiCommitableIF jc);
  public boolean removeEquivalent(JasiCommitableListIF aList);
  public boolean retainEquivalent(JasiCommitableListIF aList);
  public boolean containsEquivalent(JasiCommitableListIF aList);
  public Object clone();
  public JasiCommitableListIF newInstance();
}

package org.trinet.jasi.TN;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.NullValueDb;

public class PhaseListTN extends PhaseList {

/*
-- MERGE Using literal values through DUAL
MERGE INTO AssocArO o
  USING dual ON (o.orid = ? AND o.arid=?)
  WHEN NOT MATCHED THEN
         INSERT (ORID,ARID,AUTH,SUBSOURCE,IPHASE,IMPORTANCE,DELTA,SEAZ,IN_WGT,WGT,TIMERES,SDELAY,RFLAG) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)
  WHEN MATCHED THEN
         UPDATE SET o.AUTH=?,o.SUBSOURCE=?,o.IPHASE=?,o.IMPORTANCE=?,o.DELTA=?,o.SEAZ=?,o.IN_WGT=?,o.WGT=?,o.TIMERES=?,o.SDELAY=?,o.RFLAG=?;
*/

     //Perhaps implement call to stored EPREF function to do the assoc row to replace the util rowToDb call above
     //INSERT INTO ASSOCARO (o.ORID,o.ARID,o.AUTH,o.SUBSOURCE,o.IPHASE,o.IMPORTANCE,o.DELTA,o.SEAZ
     //   o.IN_WGT,o.WGT,o.TIMERES,o.SDELAY,o.RFLAG) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);

     //UPDATE ASSOCARO SET o.AUTH=?, o.SUBSOURCE=?, o.IPHASE=?, o.IMPORTANCE=?,
     //    o.DELTA=?, o.SEAZ=?, o.IN_WGT=?, o.WGT=?, o.TIMERES=?, o.SDELAY=?,
     //    o.RFLAG=? WHERE o.orid=? AND o.arid=?;

    private PreparedStatement psInsertArr = null;

    private static final String psInsertArrStr =
        "INSERT INTO ARRIVAL (ARID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN," + // 1-9
        " LOCATION,IPHASE,QUAL,FM, DELTIM,QUALITY,RFLAG) VALUES" +  // 10-16
        " (?,TRUETIME.putEpoch(?,'UTC'),?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; // 16 columns
      //" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; // 16 columns

    private PreparedStatement psInsertAssoc = null;
    private static final String psInsertAssocStr =
        "INSERT INTO ASSOCARO (ORID,ARID,AUTH,SUBSOURCE,IPHASE,IMPORTANCE,DELTA,SEAZ,IN_WGT,WGT,TIMERES,EMA,SDELAY,RFLAG)" +
        " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; // 14 columns

    private CallableStatement csUpsertAssoc = null;
    private static final String csUpsertAssocStr = "{ call EPREF.upsertAssocArO(?,?,?,?,?,?,?,?,?,?,?,?,?,?) }"; // 14 columns
    //P_ORID P_ARID P_AUTH P_SSRC P_IPHS P_IMPT P_DIST P_AZIM P_IWGT P_OWGT P_TRES P_EMA P_SDLY P_RFLG

    public static boolean debug = false;

    static {
        debug = JasiDatabasePropertyList.debugTN;
    }

    /** No-op default constructor, empty list. */
    public PhaseListTN() {}

    /** Create empty list with specified input capacity.*/
    public PhaseListTN(int capacity) {
        super(capacity);
    }

    /** Create list and add input collection elements to list.*/
    public PhaseListTN(Collection col) {
        super(col);
    }
    /*
    private static final String csSeqStr = "{ ? = call SEQUENCE.getSeqTable('ARSEQ', ?) }";
        CallableStatement csSeqIdStmt = DataSource.getConnection().prepareCall(csSeqStr); 
        csSeqIdStmt.registerOutParameter(1, java.sql.Types.ARRAY, "SEQ_TABLE");
        csSeqIdStmt.setInt(2, newList.size());
        int result = csSeqIdStmt.executeUpdate();
        java.sql.Array intArray = cs.SeqIdStmt.getArray(1);
        BigDecimal[] bigd = (BigDecimal[]) intArray.getArray();  // need to use bigd.longValue() for the arid 
    */
    // RH: I don't know why these are static finals so just adding another one for the PostgreSQL case
    private static final String psSeqStr = "Select column_value from table (sequence.getSeqTable('ARSEQ',?))";
    private static final String psSeqStrPG = "Select * from sequence.getSeqTable('ARSEQ',?)";
    private PreparedStatement psSeqIdStmt = null;

    public boolean commit() {

        debug = JasiDatabasePropertyList.debugTN;

        if (!DataSource.isWriteBackEnabled()) return false;
        boolean status = true;

        int mySize = this.size();
        if (mySize == 0)  return true; // nothing to commit, a no-op

        // Find new phases to be saved, but without a valid arid
        PhaseTN phaseTN = null;
        ArrayList newList = new ArrayList(mySize);
        for (int i = 0; i < mySize; i++) {
            phaseTN = (PhaseTN) get(i);
            //System.out.println(phaseTN); // dump for debug
            //if (phaseTN.isDeleted() || ! phaseTN.isAssignedToSol() || phaseTN.getArid() != 0) continue; // replaced by below - aww 2011/05/12
            if (phaseTN.isDeleted() || ! phaseTN.isAssignedToSol() ||
               (phaseTN.getArid() != 0 && !phaseTN.hasChanged()) ) continue; // skip, old id and a change=>new arid - aww 2011/05/12
            newList.add(phaseTN);
        }

        // Set the arids of new phases to be committed
        PreparedStatement psSeqIdStmt = null;
        if (newList.size() > 0) {
          ResultSet rs = null;
          try {
            if (psSeqIdStmt == null) {
                Connection conn = DataSource.getConnection();
                DatabaseMetaData db_info = conn.getMetaData();
                if ("postgresql".equalsIgnoreCase(db_info.getDatabaseProductName().toString())) {
                    psSeqIdStmt = DataSource.getConnection().prepareStatement(psSeqStrPG);
                } else {
                    psSeqIdStmt = DataSource.getConnection().prepareStatement(psSeqStr);
                }
            }
            psSeqIdStmt.setInt(1, newList.size());
            rs = psSeqIdStmt.executeQuery();
            int ii = 0;
            while (rs.next()) {
                phaseTN = (PhaseTN)newList.get(ii++);
                phaseTN.setArid( rs.getLong(1) );
            }
          }
          catch (SQLException ex) {
              status = false;
              ex.printStackTrace();
          }
          finally {
            try {
              if (rs != null) rs.close();
              if (psSeqIdStmt != null) psSeqIdStmt.close();
              psSeqIdStmt = null;
            }
            catch (SQLException ex) {
            }
          }
        }
        if (!status) return false; // error setting new arids

        // Batch queue insert or update
        try {
          for (int i = 0; i<mySize; i++) {
            phaseTN = (PhaseTN) get(i);
            if (debug) System.out.println ("DEBUG PhaseListTN commit() fromDB:"+phaseTN.fromDbase+" "+phaseTN.toNeatString());
            // Reading is deleted or not associated so skip
            if (phaseTN.isDeleted() || ! phaseTN.isAssignedToSol()) continue;
              if (phaseTN.fromDbase) {  // "existing" (came from the dbase)
                if (phaseTN.getNeedsCommit() || phaseTN.hasChanged()) { // insert new row and association
                    if (debug) System.out.println ("Phase (changed) INSERT and assoc");
                    status = dbaseInsert(phaseTN) && dbaseAssociate(phaseTN, false);
                } else { // if no changes, just make new association
                    if (debug) System.out.println ("Phase (unchanged) assoc only");
                    status = dbaseAssociate(phaseTN, true);
                    if (status) phaseTN.setUpdate(false); // mark as up-to-date
                }
              } else {        // not from dbase <INSERT>
                  if (debug) System.out.println ("Phase (new) INSERT and assoc");
                  status = dbaseInsert(phaseTN) && dbaseAssociate(phaseTN, false);
              }
              //phaseTN.setNeedsCommit(! status); // hopefully false?
              if (!status) {
                System.out.println("PhaseListTN commit() batched:" + status + " " + phaseTN.toString());
              }
          }

          // Done with queuing up, now execute DML
          status = batchToDb(true); // close and null statements
          if (debug) System.out.println("DEBUG PhaseListTN batchToDb success " + status);

          if (status) { // assume success, reset phases
            for (int i = 0; i<mySize; i++) {
              ((PhaseTN) get(i)).setNeedsCommit(! status); // hopefully ok?
            }
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return false;
        }
        return status;  // relies on commit throwing exception if unsuccessful 
    }

    private boolean dbaseAssociate(PhaseTN phaseTN, boolean update) {
        return (update) ? upsertAssoc(phaseTN) : insertAssoc(phaseTN);
    }

    private boolean upsertAssoc(PhaseTN phaseTN) {
        boolean status = true;
        try {
            SolutionTN solTN = (SolutionTN) phaseTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0) return false;

            if (csUpsertAssoc == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println("DEBUG PhaseListTN upsertAssoc for orid:" + assocOrid);
                    System.out.println(csUpsertAssocStr);
                }
                csUpsertAssoc = DataSource.getConnection().prepareCall(csUpsertAssocStr);
                //csUpsertAssoc.registerOutParameter(1, Types.INTEGER);
            }

            int icol = 1;

            // Key can't be null value
            csUpsertAssoc.setLong(icol++, assocOrid);
            csUpsertAssoc.setLong(icol++, phaseTN.getArid());

            //String str = solTN.getAuthority(); // removed -aww 2011/09/21 
            String str = phaseTN.getClosestAuthorityString(); // added  -aww 2011/09/21 
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssoc.setString(icol++, str);

            str = solTN.getSource();  // Origin
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssoc.setString(icol++, str);

            csUpsertAssoc.setString(icol++, phaseTN.description.iphase.toString());

            //importance
            if (phaseTN.importance.isValid()) csUpsertAssoc.setDouble(icol++, phaseTN.importance.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //delta
            double x = phaseTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) csUpsertAssoc.setDouble(icol++, x);
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //seaz
            x = phaseTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) csUpsertAssoc.setDouble(icol++, x);
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (phaseTN.weightIn.isValid()) csUpsertAssoc.setDouble(icol++, phaseTN.weightIn.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //wgt
            if (phaseTN.weightOut.isValid()) csUpsertAssoc.setDouble(icol++, phaseTN.weightOut.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //timeres
            if (phaseTN.residual.isValid()) csUpsertAssoc.setDouble(icol++, phaseTN.residual.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //ema
            if (phaseTN.emergenceAngle.isValidNumber()) csUpsertAssoc.setDouble(icol++, phaseTN.emergenceAngle.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            //sdelay
            if (phaseTN.delay.isValid()) csUpsertAssoc.setDouble(icol++, phaseTN.delay.doubleValue());
            else csUpsertAssoc.setNull(icol++, Types.DOUBLE);

            // rflag
            if (phaseTN.processingState.isValid()) csUpsertAssoc.setString(icol++, phaseTN.processingState.toString());
            else csUpsertAssoc.setNull(icol++, Types.VARCHAR);

            csUpsertAssoc.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(phaseTN);  // in case we need to know more about phase on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean insertAssoc(PhaseTN phaseTN) {
        boolean status = true;
        try {
            SolutionTN solTN = (SolutionTN) phaseTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0) return false;

            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG PhaseListTN insertAssoc orid:" + assocOrid + " arid:" + phaseTN.getArid());

            if (psInsertAssoc == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertAssocStr);
                }
                psInsertAssoc = DataSource.getConnection().prepareStatement(psInsertAssocStr);
            }

            int icol = 1;

            // Key can't be null value, however, we need valid ARID, for new phases that's from ARSEQ return !
            psInsertAssoc.setLong(icol++, assocOrid);
            psInsertAssoc.setLong(icol++, phaseTN.getArid());

            //String str = solTN.getAuthority(); // removed -aww 2011/09/21 
            String str = phaseTN.getClosestAuthorityString(); // added  -aww 2011/09/21 
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssoc.setString(icol++, str);

            str = solTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssoc.setString(icol++, str);

            psInsertAssoc.setString(icol++, phaseTN.description.iphase.toString());

            //importance
            if (phaseTN.importance.isValid()) psInsertAssoc.setDouble(icol++, phaseTN.importance.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //delta
            double x = phaseTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) psInsertAssoc.setDouble(icol++, x);
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //seaz
            x = phaseTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) psInsertAssoc.setDouble(icol++, x);
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (phaseTN.weightIn.isValid()) psInsertAssoc.setDouble(icol++, phaseTN.weightIn.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //wgt
            if (phaseTN.weightOut.isValid()) psInsertAssoc.setDouble(icol++, phaseTN.weightOut.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //timeres
            if (phaseTN.residual.isValid()) psInsertAssoc.setDouble(icol++, phaseTN.residual.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //ema
            if (phaseTN.emergenceAngle.isValidNumber()) psInsertAssoc.setDouble(icol++, phaseTN.emergenceAngle.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            //sdelay
            if (phaseTN.delay.isValid()) psInsertAssoc.setDouble(icol++, phaseTN.delay.doubleValue());
            else psInsertAssoc.setNull(icol++, Types.DOUBLE);

            // rflag
            if (phaseTN.processingState.isValid()) psInsertAssoc.setString(icol++, phaseTN.processingState.toString());
            else psInsertAssoc.setNull(icol++, Types.VARCHAR);

            psInsertAssoc.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(phaseTN);  // in case we need to know more about phase on error 
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    private boolean dbaseInsert(PhaseTN phaseTN) {
        boolean status = true;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG PhaseListTN dbaseInsert arid:" + phaseTN.getArid());

            // NOTE: 2nd argument below to return generatedKeys not meaningful for batch !
            //if (psInsertArr == null) psInsertArr = DataSource.getConnection().prepareStatement(psInsertArrStr, new String[] {"ARID"});
            if (psInsertArr == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertArrStr);
                }
                psInsertArr = DataSource.getConnection().prepareStatement(psInsertArrStr);
            }

            int icol = 1;

            ChannelName cn = phaseTN.getChannelObj().getChannelName();
            // ARID was assumed to be set from sequence
            psInsertArr.setDouble(icol++, phaseTN.arid.longValue());
            psInsertArr.setDouble(icol++, phaseTN.datetime.doubleValue());
            psInsertArr.setString(icol++, cn.getSta());
            psInsertArr.setString(icol++, cn.getNet());
            psInsertArr.setString(icol++, phaseTN.getClosestAuthorityString());
            psInsertArr.setString(icol++, phaseTN.source.toString());
            psInsertArr.setString(icol++, cn.getChannel());
            psInsertArr.setString(icol++, cn.getChannelsrc());
            psInsertArr.setString(icol++, cn.getSeedchan());
            psInsertArr.setString(icol++, cn.getLocation());
            psInsertArr.setString(icol++, phaseTN.description.iphase);
            psInsertArr.setString(icol++,  phaseTN.description.ei);
            psInsertArr.setString(icol++,  phaseTN.description.fm);
            if (phaseTN.deltaTime.isValid()) psInsertArr.setDouble(icol++, phaseTN.deltaTime.doubleValue());
            else psInsertArr.setNull(icol++, Types.DOUBLE);
            psInsertArr.setDouble(icol++, phaseTN.description.getQuality());
            psInsertArr.setString(icol++,  phaseTN.processingState.toString());

            psInsertArr.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(phaseTN);  // in case we need to know more about phase on error 
            status = false;
            ex.printStackTrace();
        }
        if (status) phaseTN.setUpdate(false); // mark as up-to-date -aww added 2010/09/13
        return status;
    }

    private boolean batchToDb(boolean close) {

        int cnt = batchToDb(psInsertArr, close);
        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG PhaseListTN batchToDb insertPhase status: " + cnt);
        boolean status = (cnt >= 0);

        if (status) {

            cnt = batchToDb(csUpsertAssoc, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG PhaseListTN batchToDb upsertAssoc status: " + cnt);

            cnt = batchToDb(psInsertAssoc, close);
            status &= (cnt >= 0);
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG PhaseListTN batchToDb insertAssoc status: " + cnt);
        }

        if (close) {
            psInsertArr = null;
            psInsertAssoc = null;
            csUpsertAssoc = null;
        }

        return status;
    }

    // updateCount value: Statement.SUCCESS_NO_INFO
    // updateCount value: Statement.EXECUTE_FAILED
    private int batchToDb(Statement stmt, boolean close) {
        if (stmt == null) return 0;  // no-op, assume none
        int [] updateCounts = null;
        int status = -1;
        try {
            updateCounts = stmt.executeBatch();
            status = updateCounts.length;
            for (int idx = 0; idx<updateCounts.length; idx++) {
                if (updateCounts[idx] == Statement.EXECUTE_FAILED) {
                    status = -idx; // return negative index, flagging error -aww 2010/09/15
                    System.err.println("ERROR PhaseTN executeBatchInsert failed at idx: " + idx);
                    break;
                }
            }
        }
/*
ORACLE BATCH IMPLEMENTATION
For a prepared statement batch, it is not possible to know which operation failed.
The array has one element for each operation in the batch, and each element has a value of -3.
According to the JDBC 2.0 specification, a value of -3 indicates that an operation did not complete successfully.
In this case, it was presumably just one operation that actually failed, but because the JDBC driver does not 
know which operation that was, it labels all the batched operations as failures. 
You should always perform a ROLLBACK operation in this situation.

For a generic statement batch or callable statement batch, the update counts array is only a partial array
containing the actual update counts up to the point of the error. The actual update counts can be provided
because Oracle JDBC cannot use true batching for generic and callable statements in the Oracle implementation
of standard update batching. 

For example, if there were 20 operations in the batch, the first 13 succeeded, and the 14th generated an exception,
then the update counts array will have 13 elements, containing actual update counts of the successful operations.
You can either commit or roll back the successful operations in this situation, as you prefer.
In your code, upon failed execution of a batch, you should be prepared to handle either -3's
or true update counts in the array elements when an exception occurs.
For a failed batch execution, you will have either a full array of -3's or a partial array of positive integers.
*/
        catch(BatchUpdateException b) {
            System.err.println("-----BatchUpdateException-----");
            System.err.println("SQLState:  " + b.getSQLState());
            System.err.println("Message:  " + b.getMessage());
            System.err.println("Vendor:  " + b.getErrorCode());
            System.err.print("Update counts:  ");
            updateCounts = b.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                System.err.print(updateCounts[i] + "   ");
            }
            System.err.println("");
            System.err.println("Cause: ");
            System.err.println("SQLState:  " + b.getNextException().getSQLState());
            System.err.println("Message:  " + b.getNextException().getMessage());
            System.err.println("Vendor:  " + b.getNextException().getErrorCode());
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (close) stmt.close();
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }


} // PhaseListTN

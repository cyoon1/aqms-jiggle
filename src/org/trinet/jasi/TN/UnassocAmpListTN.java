package org.trinet.jasi.TN;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.NullValueDb;
import org.trinet.jdbc.table.SeqIds;

public class UnassocAmpListTN extends AmpList {

    private PreparedStatement psInsert = null;

    private static final String psSeqStr = "Select * from sequence.getSeqTable('AMPSEQ',?)";
    private PreparedStatement psSeqIdStmt = null;

    // ? What about new UnassocAmp.Fileid column to group new amps parsed from same input GroMoPacket ?
    private static final String psInsertStr =
        "INSERT INTO UNASSOCAMP (AMPID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN," + // 1-9
        //"LOCATION,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,PER,SNR,QUALITY,RFLAG,CFLAG,WSTART,DURATION) VALUES" +  // 10-21
        //" (?,TRUETIME.putEpoch(?,'UTC'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,TRUETIME.putEpoch(?,'UTC'),?)"; // 21 columns
        "LOCATION,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,PER,SNR,QUALITY,RFLAG,CFLAG,WSTART,DURATION, FILEID, LDDATE) VALUES" +  // 10-23
        " (?,TRUETIME.putEpoch(?,'UTC'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,TRUETIME.putEpoch(?,'UTC'),?, ?, SYS_EXTRACT_UTC(current_timestamp))"; // 23 columns

    /** No-op default constructor, empty list. */
    public UnassocAmpListTN() {}

    /** Create empty list with specified input capacity.*/
    public UnassocAmpListTN(int capacity) {
        super(capacity);
    }

    /** Create list and add input collection elements to list.*/
    public UnassocAmpListTN(Collection col) {
        super(col);
    }

    public boolean commit() {

        debug = JasiDatabasePropertyList.debugTN;

        if (!DataSource.isWriteBackEnabled()) return false;
        boolean status = true;

        int mySize = this.size();
        if (mySize == 0)  return true; // nothing to commit, a no-op

        // Find new readings to be saved, but without a valid id
        UnassocAmplitudeTN ampTN = null;
        ArrayList newList = new ArrayList(mySize);
        for (int i = 0; i < mySize; i++) {
            ampTN = (UnassocAmplitudeTN) get(i);
            //System.out.println(ampTN); // dump for debug
            if (ampTN.isDeleted() || ! (ampTN.isAssignedToSol() || ampTN.isAssignedToMag()) || ampTN.getAmpid() != 0) continue; // skip
            newList.add(ampTN);
        }

        // Set the ids of new readings to be committed
        PreparedStatement psSeqIdStmt = null;
        if (newList.size() > 0) {
          ResultSet rs = null;
          try {
            if (psSeqIdStmt == null) psSeqIdStmt = DataSource.getConnection().prepareStatement(psSeqStr);
            psSeqIdStmt.setInt(1, newList.size());
            rs = psSeqIdStmt.executeQuery();
            int ii = 0;
            while (rs.next()) {
                ampTN = (UnassocAmplitudeTN)newList.get(ii++);
                ampTN.setAmpid( rs.getLong(1) );
            }
          }
          catch (SQLException ex) {
              status = false;
              ex.printStackTrace();
          }
          finally {
            try {
              if (rs != null) rs.close();
              if (psSeqIdStmt != null) psSeqIdStmt.close();
              psSeqIdStmt = null;
            }
            catch (SQLException ex) {
            }
          }
        }
        if (!status) return false; // error setting new ids

        // Batch queue insert or update
        try {
          for (int i = 0; i<mySize; i++) {
            ampTN = (UnassocAmplitudeTN) get(i);
            if (debug) System.out.println ("DEBUG UnassocAmpListTN commit() fromDB:"+ampTN.fromDbase+" "+ampTN.toNeatString());
            // Reading is deleted or not associated so skip
            if (ampTN.isDeleted() || ! ampTN.isAssignedToSol()) continue;
              if (ampTN.fromDbase) {  // "existing" (came from the dbase)
                if (ampTN.getNeedsCommit() || ampTN.hasChanged()) { // insert new row and association
                    if (debug) System.out.println ("UnassocAmplitude (changed) INSERT");
                    status = dbaseInsert(ampTN); // no association done
                } else { // if no changes, there is no association
                    if (debug) System.out.println ("UnassocAmplitude (unchanged) commit is no-op");
                    ampTN.setUpdate(false); // mark as up-to-date
                }
              } else {        // not from dbase <INSERT>
                  if (debug) System.out.println ("UnassocAmplitude (new) INSERT and assoc");
                  status = dbaseInsert(ampTN); // no association done
              }
              //ampTN.setNeedsCommit(! status); // hopefully false?
              if (!status) {
                System.out.println("UnassocAmpListTN commit() batched status:" + status + " " + ampTN.toString());
              }
          }

          // Done with queuing up, now execute DML
          status = batchToDb(true); // close and null statements
          if (debug) System.out.println("DEBUG UnassocAmpListTN batchToDb success " + status);

          if (status) { // assume success, reset commit status
            for (int i = 0; i<mySize; i++) {
              ampTN = (UnassocAmplitudeTN) get(i);
              ampTN.setNeedsCommit(! status); // hopefully ok?
            }
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return false;
        }
        return status;  // relies on commit throwing exception if unsuccessful 
    }

 
    private boolean dbaseInsert(UnassocAmplitudeTN ampTN) {
        boolean status = true;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG UnassocAmpListTN dbaseInsert ampid:" + ampTN.ampid.longValue());

            // NOTE: 2nd argument below to return generatedKeys not meaningful for batch !
            //if (psInsert == null) psInsert = DataSource.getConnection().prepareStatement(psInsertStr, new String[] {"AMPID"});
            if (psInsert == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(psInsertStr);
                }
                psInsert = DataSource.getConnection().prepareStatement(psInsertStr);
            }

            int icol = 1;

            ChannelName cn = ampTN.getChannelObj().getChannelName();
            // id was assumed to be set from sequence
            psInsert.setDouble(icol++, ampTN.ampid.longValue());

            if (ampTN.datetime.isValid()) psInsert.setDouble(icol++, ampTN.datetime.doubleValue());
            else psInsert.setNull(icol++, Types.NUMERIC);

            psInsert.setString(icol++, cn.getSta());
            psInsert.setString(icol++, cn.getNet());
            psInsert.setString(icol++, ampTN.getClosestAuthorityString());
            psInsert.setString(icol++, ampTN.source.toString());
            psInsert.setString(icol++, cn.getChannel());
            psInsert.setString(icol++, cn.getChannelsrc());
            psInsert.setString(icol++, cn.getSeedchan());
            psInsert.setString(icol++, cn.getLocation());

            // "rectify" - peak amp must be > 0.0
            psInsert.setDouble(icol++, Math.abs(ampTN.value.doubleValue()));

            psInsert.setString(icol++, AmpType.getString(ampTN.type));

            psInsert.setString(icol++, Units.getString(ampTN.units));

            // if zero-to-peak = 1 , if peak-to-peak = 0
            psInsert.setInt(icol++, ((ampTN.halfAmp) ? Amplitude.ZERO_TO_PEAK : Amplitude.PEAK_TO_PEAK));

            if (ampTN.period.isValid()) psInsert.setDouble(icol++, ampTN.period.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            if (ampTN.snr.isValid()) psInsert.setDouble(icol++, ampTN.snr.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            psInsert.setDouble(icol++, ampTN.getQuality());

            psInsert.setString(icol++,  ampTN.processingState.toString());

            int clipped = ampTN.getClipped();
            if (clipped == Amplitude.ON_SCALE)         psInsert.setString(icol++, "OS");
            else if (clipped == Amplitude.BELOW_NOISE) psInsert.setString(icol++, "BN");
            else if (clipped == Amplitude.CLIPPED)     psInsert.setString(icol++, "CL");

            if (ampTN.windowStart.isValid()) psInsert.setDouble(icol++, ampTN.windowStart.doubleValue());
            else psInsert.setNull(icol++, Types.NUMERIC);

            if (ampTN.windowDuration.isValid()) psInsert.setDouble(icol++, ampTN.windowDuration.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            if (ampTN.getFileid().isValid()) psInsert.setLong(icol++, ampTN.getFileid().longValue());
            else psInsert.setNull(icol++, Types.INTEGER);

            psInsert.addBatch();
        }

        catch (SQLException ex) {
            System.err.println(ampTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        }
        if (status) ampTN.setUpdate(false); // mark as up-to-date -aww added 2010/09/13
        return status;
    }

    private boolean batchToDb(boolean close) {

        int cnt = batchToDb(psInsert, close);
        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG UnassocAmpListTN batchToDb insertAmp status: " + cnt);
        boolean status = (cnt >= 0);

        if (close) {
            psInsert = null;
        }

        return status;
    }

    // updateCount value: Statement.SUCCESS_NO_INFO
    // updateCount value: Statement.EXECUTE_FAILED
    private int batchToDb(Statement stmt, boolean close) {
        if (stmt == null) return 0;  // no-op, assume none

        int [] updateCounts = null;
        int status = -1;

        try {
            updateCounts = stmt.executeBatch();
            status = updateCounts.length;
            for (int idx = 0; idx<updateCounts.length; idx++) {
                if (updateCounts[idx] == Statement.EXECUTE_FAILED) {
                    status = -idx; // return negative index, flagging error -aww 2010/09/15
                    System.err.println("ERROR AmplitudeTN executeBatchInsert failed at idx: " + idx);
                    break;
                }
            }
        }
/*
ORACLE BATCH IMPLEMENTATION
For a prepared statement batch, it is not possible to know which operation failed.
The array has one element for each operation in the batch, and each element has a value of -3.
According to the JDBC 2.0 specification, a value of -3 indicates that an operation did not complete successfully.
In this case, it was presumably just one operation that actually failed, but because the JDBC driver does not 
know which operation that was, it labels all the batched operations as failures. 
You should always perform a ROLLBACK operation in this situation.

For a generic statement batch or callable statement batch, the update counts array is only a partial array
containing the actual update counts up to the point of the error. The actual update counts can be provided
because Oracle JDBC cannot use true batching for generic and callable statements in the Oracle implementation
of standard update batching. 

For example, if there were 20 operations in the batch, the first 13 succeeded, and the 14th generated an exception,
then the update counts array will have 13 elements, containing actual update counts of the successful operations.
You can either commit or roll back the successful operations in this situation, as you prefer.
In your code, upon failed execution of a batch, you should be prepared to handle either -3's
or true update counts in the array elements when an exception occurs.
For a failed batch execution, you will have either a full array of -3's or a partial array of positive integers.
*/
        catch(BatchUpdateException b) {
            System.err.println("-----BatchUpdateException-----");
            System.err.println("SQLState:  " + b.getSQLState());
            System.err.println("Message:  " + b.getMessage());
            System.err.println("Vendor:  " + b.getErrorCode());
            System.err.print("Update counts:  ");
            updateCounts = b.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                System.err.print(updateCounts[i] + "   ");
            }
            System.err.println("");
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (close) stmt.close();
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

} // UnassocAmpListTN

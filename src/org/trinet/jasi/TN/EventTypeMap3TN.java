package org.trinet.jasi.TN;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.trinet.jasi.*;

/** Concrete class that defines how event types are mapped in the database schema. */
public class EventTypeMap3TN extends EventTypeMap3 {

    static {
      eventTypes = new EventType [] {
        //new EventType("eq", "eq", "fault movement", "earthquake"),
        new EventType("eq", "earthquake", "fault movement", "earthquake"),
        new EventType("st", "trigger", "subnet trigger", "other"),
        new EventType("qb", "quarry", "quarry blast", "explosion", 0.0),
        new EventType("ex", "explosion", "generic chemical blast", "explosion", 0.0),
        new EventType("sn", "sonic", "sonic shockwave", "atmospheric", 0.0),
        new EventType("th", "thunder", "thunder", "atmospheric", 0.0),
        new EventType("se", "slow", "slow earthquake", "earthquake"),
        new EventType("lp", "longperiod", "long period volcanic earthquake", "earthquake"),
        new EventType("vt", "v-tremor", "volcanic tremor", "tremor"),
        new EventType("tr", "nv-tremor", "non-volcanic tremor", "tremor"),
        new EventType("to", "tornillo", "tornillo wavelet", "tremor"),
        new EventType("ve", "eruption", "volcanic eruption", "earth movement", 0.0),
        new EventType("av", "snow-ice", "snow/ice avalanche", "earth movement", 0.0),
        new EventType("df", "debris", "debris flow/avalanche", "earth movement", 0.0),
        new EventType("ls", "landslide", "landslide", "earth movement", 0.0),
        new EventType("rs", "rockslide", "rockslide", "earth movement", 0.0),
        new EventType("rb", "rockburst", "rockburst", "earth movement"),
        new EventType("bc", "bldg", "building collapse/demolition", "impact", 0.0),
        new EventType("pc", "plane", "plane crash", "impact", 0.0),
        new EventType("nt", "nuclear", "nuclear test", "explosion", 0.0),
        new EventType("mi", "meteor", "meteor/comet impact", "impact", 0.0),
        new EventType("ce", "calibr", "calibration", "other", 0.0),
        new EventType("sh", "shot", "refraction/reflection survey shot", "explosion", 0.0),
        new EventType("co", "m-collapse", "mine/tunnel collapse", "earth movement"),
        new EventType("ot", "other", "other miscellaneous (see event comment)", "other"),
        new EventType("px", "blast?", "probable blast", "explosion", 0.0),
        new EventType("lf", "lowfreq", "low frequency signal","earthquake"),
        new EventType("su", "surface", "non-specific surface event","earth movement", 0.0),
        new EventType("uk", "unknown", "unknown type", "other")
      };
    }

    public EventTypeMap3TN() {
    }

    public void loadEventTypes() {
        Connection conn = DataSource.getConnection();
        if (conn == null) return;

        Statement sm = null;
        ResultSet rs = null;
        //String sql = "Select etype, name, description, category from EventType order by category, name";
        String sql = "SELECT e.etype,e.name,e.description,c.category FROM EventType e,EventCategory c,AssocTypeCat a " +
            "WHERE (a.etype=e.etype) AND (c.catid=a.catid) ORDER BY c.category,e.name";
        try {
            sm = conn.createStatement();
            if (JasiDatabasePropertyList.debugSQL) System.out.println ("EventTypeMap3 sql: "+sql);
            rs = sm.executeQuery(sql);
            ArrayList typeList = new ArrayList(32);
            ArrayList jasiList = new ArrayList(32);
            ArrayList etList = new ArrayList(32);
            String code = null;
            String name = null;
            String desc = null;
            String group = null;
            //double fix = 0.;
            boolean fix = false;
            EventType et = null;
            while (rs.next()) {
              code = rs.getString(1);
              typeList.add(code);

              name = rs.getString(2);
              jasiList.add(name);

              desc = rs.getString(3);
              group = rs.getString(4);

              //fix = rs.getDouble(5);
              //et = new org.trinet.jasi.EventType(code, name, desc, group, ((rs.wasNull()) ? EventType.DEFAULT_FIX_DEPTH : fix));

              fix = ( "explosion".equals(group) || "atmospheric".equals(group) || "earth movement".equals(group) || "impact".equals(group) );

              et = (fix) ? new org.trinet.jasi.EventType(code, name, desc, group, 0.) : new org.trinet.jasi.EventType(code, name, desc, group);

              etList.add(et);
            }

            if (etList.size() > 0) eventTypes = (EventType []) etList.toArray(new org.trinet.jasi.EventType [etList.size()]);
        }
        catch (SQLException ex) {
            System.err.println("EventTypeMap SQLException: " + ex.getMessage());
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        //System.out.println("Begin EventTypeMap Listing");
        //System.out.println(this.toString());
        //System.out.println("End of EventTypeMap Listing");
    }

}

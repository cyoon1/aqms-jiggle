package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

// NOTE: THIS CLASS IS NOT USED BY CODA MAG CODE AS OF 11/15/04 -aww
/**
* Specific ChannelCodaClipAmpData type originating from database table.
*/
public class ChannelCodaClipAmpDataTN extends ChannelCodaClipAmpData implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false; 

    public static double CODA_CLIP_AMP_E_DEFAULT = 819.2;
    public static double DEFAULT_CODA_CLIP_PEAK_AMP_RATIO = 0.4;
    public static double CODA_CLIP_AMP_H_DEFAULT =
                 Math.round(DEFAULT_CODA_CLIP_PEAK_AMP_RATIO * ChannelClipAmpDataTN.MAX_AMP_H_DEFAULT);
    public static double CODA_CUTOFF_AMP_E_DEFAULT = 49.14; // EW or CUSP analog
    public static double CODA_CUTOFF_AMP_H_DEFAULT = 150.; // arbitrary round number for unknown digitals?

    // note ChannelMap_AmpParms has units as CalibrUnits.COUNTS not AVG_ABS_COUNTS
    protected final static String TN_TABLE_NAME = "CHANNELMAP_CODAPARMS"; // CHANNELMAP_CODACLIP; 03/04 aww //

    protected final static String SQL_SELECT_PREFIX =
      "SELECT net,sta,seedchan,location,ondate,offdate,clip,cutoff FROM " +TN_TABLE_NAME;

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    { 
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public ChannelCodaClipAmpDataTN() { }

    public ChannelCodaClipAmpDataTN(ChannelIdIF id) {
        super(id);
    }
    public ChannelCodaClipAmpDataTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public ChannelCodaClipAmpDataTN(ChannelIdIF id, DateRange dateRange, UnitsAmp cutoffAmp, UnitsAmp clipAmp ) {
        super(id, dateRange, cutoffAmp, clipAmp);
    }
    public ChannelCodaClipAmpDataTN(ChannelIdIF id, DateRange dateRange, double cutoffAmpValue, int cutoffAmpUnits,
                    double clipAmpValue, int clipAmpUnits) {
        super(id, dateRange, cutoffAmpValue, cutoffAmpUnits, clipAmpValue, clipAmpUnits);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX ;
    }
    public String toChannelSQLSelectPrefix(java.util.Date date) {
        return SQL_SELECT_PREFIX +
          " WHERE " + DataTableRowUtil.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ;
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
    }

    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(TN_TABLE_NAME, id, dr);
    }

    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    private static ChannelCodaClipAmpDataTN parseResultSet(ResultSetDb rsdb) {
        ResultSet rs = rsdb.getResultSet();
        if (rs == null) return null;
        ChannelCodaClipAmpDataTN data = null;
        try {
            data = new ChannelCodaClipAmpDataTN();
            int offset = 0;
            data.channelId =
                jasiDataReader.parseChannelIdKeyByOffset(data.channelId, offset, rs);
            offset = 4;
            if (data.dateRange == null) data.dateRange = new DateRange();
            // 2005/04/05 -removed aww
            //data.dateRange.setMin(rs.getTimestamp(++offset));
            //data.dateRange.setMax(rs.getTimestamp(++offset));
            // String to UTC Date because jdbc times are shifted to local tz millisecs (PST)
            String dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              data.dateRange.setMin(EpochTime.stringToDate(dstr)); // -aww 2008/02/11 ok
            dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              data.dateRange.setMax(EpochTime.stringToDate(dstr)); // -aww 2008/02/11 ok

            DataDouble dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              data.clipAmp.setValue(dd);
              // note ChannelMap_AmpParms has units as CalibrUnits.COUNTS
              data.clipAmp.setUnits(CalibrUnits.AVG_ABS_COUNTS);
            }
            dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              data.cutoffAmp.setValue(dd);
              // note ChannelMap_AmpParms has units as CalibrUnits.COUNTS
              data.cutoffAmp.setUnits(CalibrUnits.AVG_ABS_COUNTS);
            }
            data.fromDbase = true; // flag as db acquired
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        return data;
    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public  Collection loadAll() {
        return loadAll((java.util.Date) null);
    }

    /*
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(TN_TABLE_NAME, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    public double getCutoffAmpValue() {
      return ( isCutoffAmpNull() ) ? getDefaultCutoffAmpValue() : cutoffAmp.doubleValue();
    }
    public double getDefaultCutoffAmpValue() {
        // TODO: Need to convert by instance amp units type
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return CODA_CUTOFF_AMP_E_DEFAULT;
        return CODA_CUTOFF_AMP_H_DEFAULT;
    }

    public double getClipAmpValue() {
      //return ( isClipAmpNull() ) ? getDefaultClipAmpValue() : clipAmp.doubleValue();
      return ( isClipAmpNull() ) ? 0. : clipAmp.doubleValue(); // assume no default -aww 2009/03/10
    }
    public double getDefaultClipAmpValue() {
        // TODO: Need to convert by instance amp units type
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return CODA_CLIP_AMP_E_DEFAULT;
        return CODA_CLIP_AMP_H_DEFAULT;
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

} // end of class ChannelCodaClipAmpDataTN


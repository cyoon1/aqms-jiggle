package org.trinet.jasi.TN;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class TestDataSourceTN extends TestDataSource {
    
  private static final String str1 = "dbbrow";
  private static final String str2 = "dbbrow";

  public TestDataSourceTN() { this("databasert"); }

  public TestDataSourceTN(String host) {
    this(host, "databasert");
  }

  public TestDataSourceTN(String host, String dbasename) {
    this(host, dbasename, str2, str1);
  }

  public TestDataSourceTN(String host, String dbasename, String str2, String str1) {
    super("jdbc:"+ AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL+":@"+host+":"+
          AbstractSQLDataSource.DEFAULT_DS_PORT+":"+ dbasename,
          AbstractSQLDataSource.DEFAULT_DS_DRIVER, str2, str1);
    setWriteBackEnabled(true);
  }
}

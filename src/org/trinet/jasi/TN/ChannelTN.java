package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * This is a schema specific implimentation of Channel that acts as the
 * interface to the Channel_Data table of the NCDC v1.5 schema.
 * Contains a ChannelName member which includes
 * descriptive information for the channel like location
 * of the site (latlonz) and response info. <p>
 *
 * @See: org.trinet.jasi.ChannelName
 */

public class ChannelTN extends Channel implements DbReadableJasiChannelObjectIF {
//SIS_CHANNEL_DATA COLUMNS
//NET STA BANDCODE INSTRUMENTCOD ORIENTATIONCO SEEDCHAN CHANNEL CHANNELSRC LOCATION SIG_UNIT
//CALIB_UNIT LAT LON ELEV EDEPTH AZIMUTH DIP STORAGEFORMAT BLOCKLENGTH SAMPRATE CLOCKDRIFT
//ONDATE OFFDATE DATECREATED CONFIGID CHANID

//CHANNEL_DATA COLUMNS
//NET STA SEEDCHAN CHANNEL CHANNELSRC LOCATION INID REMARK UNIT_SIGNAL UNIT_CALIB LAT LON ELEV
//EDEPTH AZIMUTH DIP FORMAT_ID RECORD_LENGTH SAMPRATE CLOCK_DRIFT FLAGS ONDATE OFFDATE LDDATE

// NOTE: instead of referencing specific tables in db below should reference a view
// dba's must create view with what java code expects for declared data column attributes
    private static String CONFIG_TABLE_NAME = "JASI_CONFIG_VIEW";

    private static String TN_TABLE_NAME = "JASI_CHANNEL_VIEW";
    private static String QUALIFIED_COLUMN_NAMES = 
            "JASI_CHANNEL_VIEW.NET,JASI_CHANNEL_VIEW.STA,JASI_CHANNEL_VIEW.SEEDCHAN,JASI_CHANNEL_VIEW.CHANNEL," +
            "JASI_CHANNEL_VIEW.CHANNELSRC,JASI_CHANNEL_VIEW.LOCATION,JASI_CHANNEL_VIEW.LAT,JASI_CHANNEL_VIEW.LON,"+
            "JASI_CHANNEL_VIEW.ELEV," +
            "JASI_CHANNEL_VIEW.EDEPTH,JASI_CHANNEL_VIEW.AZIMUTH,JASI_CHANNEL_VIEW.DIP,"+
            "JASI_CHANNEL_VIEW.SAMPRATE,JASI_CHANNEL_VIEW.ONDATE,JASI_CHANNEL_VIEW.OFFDATE";

    /*
    private static String TN_TABLE_NAME = null;
    static {
        TN_TABLE_NAME = (EnvironmentInfo.getNetworkCode().equals("CI")) ?  "SIS_CHANNEL_DATA" : "CHANNEL_DATA";
        if (TN_TABLE_NAME == "SIS_CHANNEL_DATA") {  
          QUALIFIED_COLUMN_NAMES = 
            "SIS_CHANNEL_DATA.NET,SIS_CHANNEL_DATA.STA,SIS_CHANNEL_DATA.SEEDCHAN,SIS_CHANNEL_DATA.CHANNEL," +
            "SIS_CHANNEL_DATA.CHANNELSRC,SIS_CHANNEL_DATA.LOCATION,SIS_CHANNEL_DATA.LAT,SIS_CHANNEL_DATA.LON,"+
            "SIS_CHANNEL_DATA.ELEV,SIS_CHANNEL_DATA.SAMPRATE,SIS_CHANNEL_DATA.ONDATE,SIS_CHANNEL_DATA.OFFDATE";
        }
        else {
          QUALIFIED_COLUMN_NAMES = 
            "CHANNEL_DATA.NET,CHANNEL_DATA.STA,CHANNEL_DATA.SEEDCHAN,CHANNEL_DATA.CHANNEL," +
            "CHANNEL_DATA.CHANNELSRC,CHANNEL_DATA.LOCATION,CHANNEL_DATA.LAT,CHANNEL_DATA.LON,"+
            "CHANNEL_DATA.ELEV,CHANNEL_DATA.SAMPRATE,CHANNEL_DATA.ONDATE,CHANNEL_DATA.OFFDATE";
        }
    }
    */

    // Note to subclass this class you must make instance reader member, non-static,
    // or declare another static reader in subclass
    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    // init of "reader" requires instance instantiation in order to invoke "getClass()"
    {
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

/*  List of RT channel groups as of 7/30/04    "select name from program"
LinkWave RCG-TRINET RCG-NEIC RCG-NT CONT-RCG SCEAmpMail DWPAmpMail SCGAmpMail CDMGAmpMail
TMTS ADA PMAG_CHANS EVT_DETECT AmpGen RAD RAD_AE RAD_FJ RAD_KO RAD_PS RAD_TZ RWP_DIGITAL
RWP_DIGITAL_AL RWP_DIGITAL_MZ RWP_ANALOG RWS CS2EW_DIGITAL CS2EW_DIGITAL_AE CS2EW_DIGITAL_FJ
CS2EW_DIGITAL_KO CS2EW_DIGITAL_PS CS2EW_DIGITAL_TZ CS2EW_ANALOG PICK_EW RWP_BASE WDA
TRIDEFS TriMag
*/
    /** Default "Name" in the JASI_CONFIG_VIEW table that defines a candidate channel list.
     * This is CASE SENSITIVE.*/
    private static String candidateListProgramName = "RCG-TRINET";


    public ChannelTN() {
       loadClipping = false; // 03/30/2006 aww - to make default consistent with below override method
    }

    public void setAutoLoadingOfAssocDataOn() {
       loadResponse = true;
       loadCorrections = true;
       loadClipping = false; // don't load this for now
    }

    /**
     * Returns a Collection of objects from the default DataSource active at the current time
    */
    public Collection loadAll() {
        return loadAll(DataSource.getConnection(), (java.util.Date) null);
    }

    /**
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(DataSource.getConnection(), date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet, String [] prefSeedchan, String [] prefChannel) {
      return loadAll(DataSource.getConnection(), date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet, String [] prefSeedchan,
                    String [] prefChannel, String [] prefLocations) {
      return loadAll(DataSource.getConnection(), date, prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return loadAll(conn, date, null, null, null);
    }

    static public Collection loadAll(Connection conn, java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(conn, date, prefNet, prefSeedchan, prefChannel, null);
    }
    static public Collection loadAll(Connection conn, java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {

        Collection col = jasiDataReader.loadAll(TN_TABLE_NAME, conn, date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
        if (col.size() > 0)  {
          Iterator iter = col.iterator();
          while (iter.hasNext()) {
            ((Channel) iter.next()).loadAssocData(date);
          }
        }
        return col;
    }

    public ChannelList readListByName(String name, java.util.Date date) {
        Collection col = loadAllByName(name, date);
        return (col == null) ? new ChannelList() : new ChannelList(col);
    }

    static public Collection loadAllByName(String name, java.util.Date date) {
        Collection col = jasiDataReader.getBySQL(toNamedProgramChannelListSelectString(name, date));
        if (col.size() > 0)  {
          Iterator iter = col.iterator();
          while (iter.hasNext()) {
            ((Channel) iter.next()).loadAssocData(date);
          }
        }
        return col;
    }

    /*
     * Finds the data in the DataSource matching the input's id
     * and valid invocation (SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)). If data is found, a new Channel with
     * that data is is returned, else the original input reference is returned.
     * Does require that the channel be currently "active", that is
     * ONDATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) <=OFFDATE.
    */
    public Channel lookUp(Channelable chan) {
      return lookUp(chan, (java.util.Date) null) ;
    }
    public Channel lookUp(Channelable chan, java.util.Date date) {
       Channel ch = (Channel) getByChannelId(chan.getChannelObj().getChannelId(), date);
       //System.out.println("DEBUG CHannel.lookUp returned: " + ch + " for " + date);
       return (ch == null) ? chan.getChannelObj(): ch;
    }

    /** Returns new Channel with the last active Channel_Data (no ancillary table data)
     * from the DataSource which matches the input ChannelIdIF.
     * If no matching data exists, returns the Channel object of input argument.
     * IS THIS METHOD EVER USED? DOES IT WORK? - AWW
     */
    public Channel lookUpLatest(Channelable chan) {
        //Channel ch = getLatestByChannelId(chan.getChannelObj().getChannelId(), DataTableRowUtil.CHANNELSRC);
        // by default matches only seedchan location, ignoring channel,channelsrc attributes
        // since they are not part of db key and may be inconsistent in db across data tables
        //Channel ch = getLatestByChannelId(chan.getChannelObj().getChannelId(), DataTableRowUtil.LOCATION);
        Channel ch = getLatestByChannelId(chan.getChannelObj().getChannelId(), JasiChannelDbReader.defaultMatchMode);
        return (ch != null) ? ch : chan.getChannelObj();
    }

    /*
     * Finds the LATEST Channel_Data (greatest ONDATE) in the DataSource matching
     * the input ChannelIdIF attributes defined by input <i>matchMode</i>.
     * If data is found, a new Channel object populated with
     * that data is returned, else null is returned.
     * Does NOT require that the channel be currently "active", i.e.
     * ONDATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) <= OFFDATE.
     * Does NO joins with ancillary tables like StaCorrections.
     */
    private Channel getLatestByChannelId(ChannelIdIF id, int matchMode) {
        if (id == null || NullValueDb.isBlank(id.getSta()))
           throw new IllegalArgumentException("ChannelId sta name must be defined.");
    //Select below gets location data but does no joins.
    //Without a SPECIFIC date then there is possible random date order
    //of multiple matching date range in join tables which overlap the Channel_Data
    //on-off range. Satisfying outer joins to Channel_Data is a complex problem.
        StringBuffer sb = new StringBuffer(2048);
        sb.append("SELECT ");
        sb.append(QUALIFIED_COLUMN_NAMES);
        sb.append(" FROM ");
        sb.append(TN_TABLE_NAME);
        sb.append(" WHERE ");

        // to discriminate only those with matching seedchan,location
        //sb.append(DataTableRowUtil.toChannelLocationSQLWhereClause(TN_TABLE_NAME, id));
        // to discriminate also differing channel,channelsrc use:
        //sb.append(DataTableRowUtil.toChannelSrcSQLWhereClause(TN_TABLE_NAME, id));
        sb.append(DataTableRowUtil.toChannelIdSQLString(TN_TABLE_NAME, id, matchMode));

        sb.append(" ORDER BY ").append(TN_TABLE_NAME).append(".ondate DESC"); // most recent first
        Collection col = jasiDataReader.getBySQL(sb.toString());
        if (col == null || col.size() == 0) return null;
        Object [] obj = col.toArray();
        return (Channel) obj[0];  // may have invalid date dependent channel attributes ?
    }

    /** Return collection of Channels found in the default DataSource
     * that where active on the input Date and that have flag attributes
     * matching those of the input flagList String array.
     */
    public Collection getByFlag(String [] flagList, java.util.Date date) {
      StringBuffer sb = new StringBuffer(256);
      sb.append(toChannelSQLSelectPrefix(date));
      boolean checkWhere = true;
      if (flagList != null && flagList.length > 0) {
        checkWhere = JasiDbReader.whereOrAndAppend(sb, checkWhere);
        sb.append("(");
        for (int idx = 0; idx < flagList.length; idx++) {
          if (idx > 0) sb.append(" OR ");
          sb.append(TN_TABLE_NAME).append(".flags LIKE ").append(StringSQL.valueOf(flagList[idx]));
        }
        sb.append(")");
      }
      return jasiDataReader.getBySQL(sb.toString());
    }

    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        Channel ch = (Channel) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
        if (ch != null) ch.loadAssocData(date);
        return ch;
    }

    /** Return a list of currently active channels that match the list of
    * component types given in the array of strings.
    * The component name is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */
    public ChannelList getByComponent(String[] compTypes) {
        return getByComponent(compTypes, null);
    }

    /** Return a list of channels that were active on the given date and
    *  that match the list of component types given in the array of strings.
    * The component type is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    * Defaults to "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) = currently active" if input Data is null.
    * */
    public ChannelList getByComponent(String[] compTypes, java.util.Date date) {
        Collection col = loadAll(date, compTypes);
        return (col == null) ? new ChannelList() : new ChannelList(col);
    }

    public ChannelList readList(java.util.Date date) {
        Collection col = loadAll(date);
        return (col == null) ? new ChannelList() : new ChannelList(col);
    }
    /**
     * Return a ChannelList of Channels that were active on the given input Date.
     * Uses the input Connection to lookup Channels in the data source.
     */
    public static ChannelList readList(Connection conn, DateTime date) {
        Collection col = loadAll(conn, date);
        return (col == null) ? new ChannelList() : new ChannelList(col);
    }

    /** Return count of all channels in data source at the given time.
     * Defaults to SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) (current time) if input is null.
     * */
    public int getCount(java.util.Date date) {
      return JasiDbReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    private static Channel parseResultSet(ResultSetDb rsdb){
        int offset = 0;
        ChannelTN chan = new ChannelTN();

        ResultSet rs = rsdb.getResultSet();
//parse order: NET STA SEEDCHAN CHANNEL CHANNELSRC LOCATION LAT LON ELEV SAMPRATE ONDATE OFFDATE
        try {
            if (chan.channelId == null) chan.channelId = new ChannelName();
            chan.channelId.setNet(rs.getString(++offset));
            chan.channelId.setSta(rs.getString(++offset));
            chan.channelId.setSeedchan(rs.getString(++offset));
            chan.channelId.setChannel(rs.getString(++offset));
            chan.channelId.setChannelsrc(rs.getString(++offset));
            chan.channelId.setLocation(rs.getString(++offset));

            DataDouble val = rsdb.getDataDouble(++offset);
            if (val != null && val.isValidNumber()) {
                chan.getLatLonZ().setLat(val.doubleValue());
            }
            val = rsdb.getDataDouble(++offset);
            if (val != null && val.isValidNumber()) {
                chan.getLatLonZ().setLon(val.doubleValue());
            }

            // ELEV convert elevation to km, which is what LatLonZ uses.
            val = rsdb.getDataDouble(++offset); // meters?
            if (val != null && val.isValidNumber()) {
                //chan.getLatLonZ().setZ(val.doubleValue()/1000.0, GeoidalUnits.KILOMETERS); // aww 06/11/2004
                chan.getLatLonZ().setZ(-val.doubleValue()/1000.0, GeoidalUnits.KILOMETERS); // change to depth-axis convention aww 06/11/2004
            }

            //
            // EDEPTH -> depth
            val = (DataDouble) rsdb.getDataDouble(++offset);   // meters
            if (val != null) chan.setSensorDepth(val.doubleValue());
            //AZIMUTH -> snsAz
            val = (DataDouble) rsdb.getDataDouble(++offset);   // degrees clockwise from N
            if (val != null) chan.setSensorAzimuth(val.doubleValue());
            //DIP -> snsDip
            val = (DataDouble) rsdb.getDataDouble(++offset);   // degrees +90 up , -90 down (Z usually -90)
            if (val != null) chan.setSensorDip(val.doubleValue());
            //

            val = rsdb.getDataDouble(++offset);
            if (val != null && val.isValidNumber()) chan.setSampleRate(val.doubleValue());

        /* Date can be done like this or below to get correct String to UTC time conversion
            String dstr = rs.getString(++offset);

            if (dstr != null) {
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              chan.getDateRange().setMin(EpochTime.stringToDate(dstr));
            }
            dstr = rs.getString(++offset);
            if (dstr != null) {
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              chan.getDateRange().setMax(EpochTime.stringToDate(dstr));
            }
        */
            DataDate on = rsdb.getDataDate(++offset);
            java.util.Date onDate =  ( on != null && on.isValidNumber()) ? on.dateValue() : null;
            DataDate off = rsdb.getDataDate(++offset);
            java.util.Date offDate = (off != null && off.isValidNumber()) ? off.dateValue() : null;
            DateRange dr = chan.getDateRange();
            if (dr == null) { // create new object
                chan.setDateRange(new DateRange(onDate, offDate));
            }
            else { // just reset values
                dr.setMin(onDate);
                dr.setMax(offDate);
            }

        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return chan;
    }

    public String toChannelSQLSelectPrefix() {
      //return toChannelSQLSelectPrefix((java.util.Date) null); // for current time "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)".
      StringBuffer sb = new StringBuffer(2048);
      sb.append("Select ").append(QUALIFIED_COLUMN_NAMES);
      sb.append(" from ").append(TN_TABLE_NAME);
      return sb.toString(); // all history, no date constraint
    }

    /** Return the SQL prefix string describing Channel_Data whose ondate/offdate
     *  window includes the input date.
    */
    public String toChannelSQLSelectPrefix(java.util.Date date) {
      String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : StringSQL.toDATE(date);
      StringBuffer sb = new StringBuffer(2048);
      sb.append("Select ").append(QUALIFIED_COLUMN_NAMES);
      sb.append(" from ").append(TN_TABLE_NAME).append(" WHERE ");
      sb.append("(");
      sb.append(TN_TABLE_NAME).append(".OFFDATE >= ").append(dateStr);
      sb.append(" and ");
      sb.append(TN_TABLE_NAME).append(".ONDATE <= ").append(dateStr);
      sb.append(")");
      return sb.toString();
    }

    /**
    * Return a list of current channels that are listed in JASI_CONFIG_VIEW with the
    * progid that matches this 'name' in view.
    * Returns empty list if no matches found in database.
    */
    public ChannelableList getNamedChannelList(String name) {
       return getNamedChannelList(name, null);
    }

    /**
    * Return a list of currently active channels that are candidates for waveform retrieval.
    * (This means channels that are listed in JASI_CONFIG_VIEW  with the
    * progid that matches "RCG-TRINET" in the program table.
    * Returns empty list if no matches found in database.
    */
    public ChannelableList getCandidateChannelList() {
      return getNamedChannelList(candidateListProgramName, new DateTime());
    }
    /**
    * Return a list of Channels that are candidates for waveform retrieval and
    * were active at <i>dTime</i> whose input value is True (leap) seconds.
    * (This currently means channels that are listed in JASI_CONFIG_VIEW with the
    * progid that matches "RCG-TRINET" in the program table.
    */
    public ChannelableList getCandidateChannelList(double dTime) {
        return getNamedChannelList(candidateListProgramName, new DateTime(dTime, true)); // assume input is now True (leap) seconds - aww 2008/02/11
    }

    public ChannelableList getNamedChannelList(String name, java.util.Date date) {
        Collection col = jasiDataReader.getBySQL(toNamedProgramChannelListSelectString(name, date));
        return (col == null) ? new ChannelableList() : new ChannelableList(col);
    }

    /**
     * Return the SQL prefix string describing Channel_Data joined with JASI_CONFIG_VIEW
     * defined by input <i>progName</i> in the view.
     * whose Channel_Data ondate/offdate window includes the input date.
     * */
    private static String toNamedProgramChannelListSelectString(String progName, java.util.Date date) {
      String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : StringSQL.toDATE(date);
      StringBuffer sb = new StringBuffer(2048);
      sb.append("Select ").append(QUALIFIED_COLUMN_NAMES);
      sb.append(" from ").append(TN_TABLE_NAME).append(",").append(CONFIG_TABLE_NAME).append(" cc");
      sb.append(" WHERE ");
      sb.append("(");
      sb.append(TN_TABLE_NAME).append(".OFFDATE >= ").append(dateStr);
      sb.append(" and ");
      sb.append(TN_TABLE_NAME).append(".ONDATE <= ").append(dateStr);
      sb.append(")");
      sb.append(" and ");
      sb.append("(");
      sb.append(TN_TABLE_NAME).append(".net = cc.net ");
      sb.append(" and ");
      sb.append(TN_TABLE_NAME).append(".sta = cc.sta ");
      sb.append(" and ");
      sb.append(TN_TABLE_NAME).append(".seedchan = cc.seedchan ");
      sb.append(" and ");
      sb.append(TN_TABLE_NAME).append(".location = cc.location "); // added location 09/21/2005 -aww
      sb.append(")");
      sb.append(" and cc.name = ").append(StringSQL.valueOf(progName));
      // Only if config table/view has ondate, offdate column attribute -aww  
      // Do we want the "current" config, if any or the config in effect from the input "date" ?
      // sb.append(" and cc.offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)");
      sb.append(" and cc.offdate > ").append(dateStr);
      sb.append(" and cc.ondate <= ").append(dateStr);
      sb.append(" order by cc.net, cc.sta, cc.seedchan, cc.location");
      return sb.toString();
    }


    //private AbstractChannelCorrection ML_CORR = MlAmpMagCalibration.create(JasiFactory.TRINET);
    //private AbstractChannelCorrection MH_CORR = MhAmpMagCalibration.create(JasiFactory.TRINET);
    //private AbstractChannelCorrection MD_CORR = MdCodaMagCalibration.create(JasiFactory.TRINET);
    //private AbstractChannelCorrection MC_CORR = McCodaMagCalibration.create(JasiFactory.TRINET);
    //private AbstractChannelCorrection ME_CORR = MeMagCalibration.create(JasiFactory.TRINET);
    //public static final String [] = CorrTypeIdIF.KNOWN_CORRECTIONS
    //
    private static List aCorrList = new ArrayList(8);
    static {
      aCorrList.add( MlAmpMagCalibration.create(JasiFactory.TRINET) );
      aCorrList.add( MdCodaMagCalibration.create(JasiFactory.TRINET) );
      //aCorrList.add( McCodaMagCalibration.create(JasiFactory.TRINET) ); // removed until we do this - aww 04/06/2006
      //aCorrList.add( MhAmpMagCalibration.create(JasiFactory.TRINET) ); // added for kate - aww 06/25/2004
      //aCorrList.add( MeMagCalibration.create(JasiFactory.TRINET) ); // class not defined yet
      //aCorrList.add( TTCorrection.create(JasiFactory.TRINET) ); // site delay? class not defined yet
    }
    public ChannelCorrectionDataIF getCorrectionFactoryForType(String corrType) {
      int count = aCorrList.size();
      ChannelCorrectionDataIF cd = null;
      for (int idx=0; idx < count; idx++) {
        cd = (ChannelCorrectionDataIF) aCorrList.get(idx);
        if (cd.getCorrType().equals(corrType) ) break;
      }
      return cd;
    }
    public int loadKnownCorrections(java.util.Date date) {
        return loadCorrections(aCorrList, date);
    }
    private int loadCorrections(List aList, java.util.Date date) {
//      ChannelCorrectionDataIF corr = null;
      int count = aList.size();
      int loaded = 0;
      for (int idx=0; idx < count; idx++) {
        if (loadCorrection((ChannelCorrectionDataIF) aList.get(idx), date)) loaded++;
      }
      return loaded;
    }

    public static Channel parseChannelFromDataTableRow(DataTableRow row, int level) {
        ChannelTN ch = new ChannelTN();
        if (ch.channelId == null) ch.channelId = new ChannelName();
        ch.channelId =
            DataTableRowUtil.parseChannelIdFromRow(row, ch.channelId, level);
        return ch;
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }


    // below 1000mv vs. 800mv => 655L cnts see TIMIT$DEV$CHAR; 360L old cusp value from MCA.for
    public static final double MAX_AMP_E_DEFAULT  = 2048.; // VCO USGS
    public static final double MAX_AMP_H_DEFAULT  = 8388608.;
    public double getMaxAmpValue(java.util.Date date) {
        double value = super.getMaxAmpValue(date);
        //return  (Double.isNaN(value)) ? getDefaultMaxAmpValue() : value; 
        return  (Double.isNaN(value)) ? 0. : value; // assume no default -aww 2009/03/10
    }
    public double getDefaultMaxAmpValue() {
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return MAX_AMP_E_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }
    public static final double CLIP_AMP_E_DEFAULT = 1250.; // VCO USGS?
    public static final double CLIP_AMP_H_DEFAULT = 8388608.;
    public double getClipAmpValue(java.util.Date date) {
        double value = super.getClipAmpValue(date);
        //return  (Double.isNaN(value)) ? getDefaultClipAmpValue() : value;
        return  (Double.isNaN(value)) ? 0. : value; // assume no default - aww 2009/03/10
    }
    public double getDefaultClipAmpValue() {
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return CLIP_AMP_E_DEFAULT;
        else if (type.equals("H")) return CLIP_AMP_H_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }

/*
  static public final class Tester {
    public static void main(String args[]){
        if (args.length < 1) {
            System.out.println("Syntax main args:  hostName");
            System.exit(0);
        }
      System.out.println("Main: Making connection on: " + args[0]);
      DataSource init = TestDataSource.create(args[0]); // DataSource(url, driver, user, passwd);
      // look up one channel by name
      //System.out.println("Current channel count = "+getCurrentCount());
      //Channel cn = Channel.create().setChannelName("CI", "PAS", "VHZ", "01","',"","EHZ","");
      Channel cnuc = Channel.create().setChannelName("CI", "PAS", "EHZ", "01");
      Channel cnlc = Channel.create().setChannelName("CI", "PAS", "ehz", "01");
      if (cnuc == null) {
        System.out.println("Main: channel create returns null.");
        System.exit(0);
      }
      //System.out.println("Main: before lookUp");
      java.util.Date date = EpochTime.stringToDate("2001-06-01 00:00:00.000 UTC");
      cnuc.setAutoLoadingOfAssocDataOff();
      Channel chnl = cnuc.lookUp(cnuc,date); // do once to allow for db first time stuff
      // Now time it
      BenchMark bm = new BenchMark();
      bm.reset();
      chnl = cnuc.lookUp(cnuc,date);
      bm.print("Main: NO assoc data load time");
      cnuc.setAutoLoadingOfAssocDataOn();
      bm.reset();
      chnl = cnuc.lookUp(cnuc,date);
      bm.print("Main: WITH assoc data load time");

      System.out.println("\nMain: lookUp found? : " + (chnl != cnuc) +"\n"+ chnl.toDumpString());
      System.out.println();

      System.out.println ("Main: HypoFormat: /"+HypoFormat.toH2StationString(chnl)+"/");
      System.out.println();

      System.out.println("Main: calcDist: " + chnl.calcDistance (new LatLonZ(34.,-118.5,-100.)));
      boolean tf = chnl.hasLatLonZ();
      System.out.println("Main: hasLatLonZ: " + tf);
      tf = chnl.hasCorrection(date, "ml") ;
      System.out.println("Main: hasCorrection ml: " + tf);
      DataCorrectionIF dc = chnl.getCorrection(date, "ml") ;
      System.out.println("Main: corr: " + ((dc == null) ? "NULL" : dc.toString()));
      tf = chnl.hasGain(date) ;
      System.out.println("Main: true hasGain: " + tf);
      ChannelGain g = chnl.getGain(date) ;
      if (g != null) System.out.println("Main: gain: " + g.toString());
      System.out.println("Main: maxAmp: " + chnl.getMaxAmpValue(date)) ;
      System.out.println("Main: clipAmp: " + chnl.getClipAmpValue(date)) ;
      tf = chnl.equals(cnuc) ;
      System.out.println("Main: false no date chnl.equals(cnuc): " + tf);
      cnuc.setDateRange(chnl.getDateRange());
      tf = chnl.equals(cnuc) ;
      System.out.println("Main: true dated chnl.equals(cnuc): " + tf);
      cnlc.setDateRange(chnl.getDateRange());
      tf = chnl.equals(cnlc) ;
      System.out.println("Main: false that chnl.equals(cnlc): " + tf);
      tf = chnl.equalsNameIgnoreCase(cnlc.getChannelId()) ;
      System.out.println("Main: true equalsIgnoreCase(cnlc): " + tf);
      tf = chnl.sameStationAs(cnlc.getChannelId()) ;
      System.out.println("Main: true sameStaAs(cnlc): " + tf);
      int cmp = chnl.compareTo(cnuc) ;
      System.out.println("Main: compare int: " + cmp);

      // From Doug's TN addition :
      if (chnl instanceof ChannelTN) {
        ChannelTN chTN = (ChannelTN) chnl;
        bm.reset();
        System.out.println("Main: Dump chTN.getNamedChannelList(\"AmpGen\")");
        ChannelableList clist = chTN.getNamedChannelList("AmpGen");
        bm.print("Main: chTN.getNamedChannelList size = "+clist.size()+ " time = ");
        //clist.dump();
        bm.reset();
        System.out.println("Main: Dump chTN.getCandidateChannelList()");
        clist = chTN.getCandidateChannelList();
        bm.print("Main: chTN.getCandidateChannelListload size = "+clist.size()+ " time = ");
        //clist.dump();
      }



    }  // end of Tester Main
  }
*/
}//end of ChannelTN class

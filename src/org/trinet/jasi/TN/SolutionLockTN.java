package org.trinet.jasi.TN;

import java.util.*;
import java.sql.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

/**
 * TriNet concrete implementation of SolutionLock. The dbase table that manages
 * event locks is called JasiEventLock. The TriNet table definition is (see
 * /home/tpp/src/eventlock):<br>

      <tt>
      create table  JasiEventLock
            (evid               number          NOT NULL,
             hostname           varchar2(40),
             application        varchar2(20),
             username           varchar2(20),
             lddate             date,
           primary key (evid)
        );
  </tt>
<p>

Note that however you implement this locking scheme ALL potential users of the
common dataset must share the same locking "table" to insure that conflicts will
be managed. Because the common data is in the dbase the most obvious way to
insure this is to use a dbase table for locking.  This is really a crude form of
persistent interprocess communication.<p>

Because this scheme does not use DBMS locking its success requires that all
applications that update data use this class and play by the rules.  If an
application does not use this class there is no way to prevent data conflicts.<p>

Table row and table locking mechanisms could not be used for this task. There
were too many dependencies in the schema. Locking an event row had unexpected results
like doing a table lock and preventing anyone from writing to the dbase.

 */

 /*
 ########################################################
 Yikes! This will NOT prevent multiple instances of the same application/user/host
 from accessing the same event.

 Possible fixes:
 1) use PID (can't see it from java)
 2) use a random number (would need to add a field to the JasiEventLock table,
 but I'm not sure how that would affect the replication.)

 ########################################################
 */

public class SolutionLockTN extends SolutionLock {

    static final String ColumnNames = "EVID, HOSTNAME, APPLICATION, USERNAME, LDDATE";

    /** Name of the table that manages event locks. */
    static final String TableName   = "JasiEventLock";

    private static final String psSqlPrefix = "Select "+ColumnNames+" from "+ TableName;

    static Connection conn;

    static boolean debug = JasiDatabasePropertyList.debugTN;

    // Don't do this at instantiation! If you do it will poke the dbase EVERY time
    // a solution is created. Instead, check it each time a DataSource connection
    // is made and set the static value of lockingWorks
    //checkLockingWorks();   // force check of lock support
    //long ran = (long) (Math.random()* 1000000.0);
    public SolutionLockTN() {
        debug = JasiDatabasePropertyList.debugTN;
    }


    /** This does not lock the solution but gets any lock info that exists for the solution. */
    public SolutionLockTN(Solution sol) {
        this();
        setSolution(sol);
    }

    //Explicitly set or change the dbase connection to be used by a solution object.
    //public static void setConnection(Connection connection) {
     // conn = connection;
    //}

    /** Return the connection object. If none was explicitly set returns the
     * DataSource default connection. If none return null.
     * */
    public static Connection getConnection() {

      // Odd, for some reason we need separate connection, else Jiggle hangs
      // when multiple threads share same Connection (e.g. releasing locks).
      // conn == DataSource.getNewConnect();

      conn = DataSource.getConnection();
      try {
        if (conn == null || conn.isClosed()) DataSource.set(DataSource.getNewConnect());
        conn = DataSource.getConnection();
      }
      catch (SQLException ex) {
        ex.printStackTrace(System.err);
      }
      return conn;
    }
    /** Returns true if locking is supported. */
    public boolean isSupported() {
        return lockingWorks;
    }

    /** Pokes the dbase to see if locking is supported. Sets the static flag
    * 'lockingWorks' to proper value. */
    public boolean checkLockingWorks() {

        if (getConnection() == null) {
            // debug
            lockingWorks = false;
            if (debug || verbose) System.out.println ( "SolutionLockTN checkLockingWorks: getConnection() == null, lockingWorks=false");
            return lockingWorks;
        }
/* this doesn't work as expected
        if (DataSource.isReadOnly()) {
          // debug
          if (debug) System.out.println( "Dbase is read-only.");
          lockingWorks = false;
          return lockingWorks;
        }
*/
     // Try it: By attempting to lock a random event

        //int id = (long) (Math.random() * 100.); // may or not be a valid db id
        lockingWorks = true;
        setId(0l); // just pick a bogus id for test - as suggested by P. Lombard
        unlock(); // try remove in case lock table has "left overs"
        if (lock()) { // now test locking
          unlock();
        } else {
          lockingWorks = false;
        }

        if (debug || verbose) System.out.println( "SolutionLockTN checkLockingWorks() lockingWorks= "+lockingWorks);

        return lockingWorks;

    }

    private static final String psLockInsert = "insert into JasiEventLock (EVID,HOSTNAME,APPLICATION,USERNAME) values (?,?,?,?)";
    /** Lock the object, returns true on success, false on failure.
    * Either way, information on the current lock holder is set in the data members. */
    public boolean lock() {

        if (!lockingWorks) {
            if (debug) System.out.println("SolutionLockTN lock() !lockingWorks == true, can't lock");
            return true; // default behavior if no locking
        }

        if (lockIsMine()) {
            if (debug) System.out.println("SolutionLockTN lock() lockIsMine == true, already locked");
            return true; // already locked
        }
        else if (isLocked()) {
            if (debug) System.out.println("SolutionLockTN lock() isLocked == true, somebody else has it");
            return false; // someone else has the lock
        }

        //String sql = "insert into "+ TableName + "(EVID,HOSTNAME,APPLICATION,USERNAME) values (" +
        //       id + ",'"+requestorHost+"','"+requestorApplication+"','"+requestorUsername+"')";
        // Note: this is non-atomic so theres a slight potential here for someone else to grag
        // the lock between when we check and when we request it.
        //boolean status = doSQL(sql);

        Connection conn = getConnection();
        if (conn == null) {
            System.err.println("ERROR: SolutionLock lock: No DataSource is open.");
            return false;
        }
        PreparedStatement ps = null;
        boolean status = false;
        try {

            if (conn.isClosed()) return false; // check that valid connection exists

            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(psLockInsert);
            ps = conn.prepareStatement(psLockInsert);
            ps.setLong(1, id);
            ps.setString(2, requestorHost);
            ps.setString(3, requestorApplication);
            ps.setString(4, requestorUsername);
            status = (ps.executeUpdate() > 0);
            if (status) {
              conn.commit();
            }
        }
        catch (SQLException ex) {
          if (debug) ex.printStackTrace(System.err);
          else System.err.println("SolutionLockTN sql error: " + ex.getMessage());
          status = false;
        }
        finally {
            try {
                if (ps != null) ps.close();
            }
            catch (SQLException ex) {
            }
        }

        if (status) {        // we got a new lock
            // the only way to get complete info about the lock old or new
            // (especially the SYSTIME) is to retreive it from the dbase.
            ArrayList list = (ArrayList) getLockInfo(id);
            copyLockInfo((SolutionLock) list.get(0));
            return true;
        } else {                // problem
            if (debug) System.out.println("SolutionLockTN lock() return status == false, db sql to lock failed");
            return false;
        }
    }

    private void copyLockInfo(SolutionLock locktoCopy) {
        host         = locktoCopy.host;
        application  = locktoCopy.application;
        username     = locktoCopy.getUsername();
        datetime     = locktoCopy.datetime;
    }

    /** Release the lock on this object. Returns true even if the lock was not held
    * in the first place. */
    private static final String psUnlockId = "DELETE FROM JASIEVENTLOCK WHERE (EVID=?)";
    public boolean unlock() {
        if (!lockingWorks) {
            if (debug) System.out.println("SolutionLockTN unlock lockingWorks == false");
            return true;      // default behavior if no locking
        }
        //String sql = "Delete from "+ TableName + " where evid = "+id;
        //return  doSQL(sql);

        Connection conn = getConnection();
        if (conn == null) {
            System.err.println("ERROR: SolutionLock lock: No DataSource is open.");
            return false;
        }
        PreparedStatement ps = null;
        boolean status = false;
        try {

            if (conn.isClosed()) {
                if (debug) System.out.println("SolutionLockTN unlock connection is closed, return false");
                return false; // check that valid connection exists
            }

            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(psUnlockId);
            ps = conn.prepareStatement(psUnlockId);
            ps.setLong(1, id);
            status = (ps.executeUpdate() > 0);
            if (status) {
              conn.commit();
            }
        }
        catch (SQLException ex) {
          if (debug) ex.printStackTrace(System.err);
          else System.err.println("SolutionLockTN unlock sql error: " + ex.getMessage());
          status = false;
        }
        finally {
            try {
                if (ps != null) ps.close();
            }
            catch (SQLException ex) {
            }
        }
        if (debug) System.out.println("SolutionLockTN unlock return status = " + status);
        return status;
    }

    /** Returns true if the object is locked by anyone, the caller included. */
    public boolean isLocked() {

        if (!lockingWorks) {
            if (debug) System.out.println("SolutionLockTN isLocked lockingWorks == false");
            return true;      // default behavior if no locking
        }

        ArrayList list = (ArrayList) getLockInfo(id);

        if (list == null || list.isEmpty()) {
            if (debug) System.out.println("SolutionLockTN isLocked == false, found no lock info");
            return false;
        }

        copyLockInfo((SolutionLock) list.get(0));
        if (debug) System.out.println("SolutionLockTN isLocked == true, found lock info");
        return true;
    }

    /** Returns true if the object is locked by the caller. */
    public boolean lockIsMine() {

        if (!lockingWorks) {
            if (debug) System.out.println("SolutionLockTN lockIsMine lockingWorks == false");
            return true;      // default behavior if no locking
        }

        ArrayList list = (ArrayList) getLockInfo(id);

        for (int i = 0; i>list.size();i++) {

          if ( ((SolutionLock)list.get(i)).matches() ) {
            if (debug) System.out.println("SolutionLockTN matches lockIsMine == true");
            return true;
          }
        }
        if (debug) System.out.println("SolutionLockTN lockIsMine == false");
        return false;

    }

    /** Return an array of SolutionLock objects that represent ALL currently
    * held locks on all events. */
    public Collection getAllLocks() {
        String sql = psSqlPrefix + " order by lddate";
        return getBySQL(sql);
    }

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this user. */
    public Collection getLocksByUser(String username) {
        String sql = psSqlPrefix + " where username = '"+username+"' order by lddate";
        return getBySQL(sql);
    }

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this host. */
    public Collection getLocksByHost(String host) {
        String sql = psSqlPrefix + " where hostname = '"+host+"' order by lddate";

        return getBySQL(sql);
    }

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this application. */
    public Collection getLocksByApplication(String application) {
        String sql = "Select "+ColumnNames+" from "+ TableName + " where "+
            " where application = '"+application+"'";

        return getBySQL(sql);
    }


    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this host/user/application. */
    public Collection getAllMyLocks() {

        String sql = psSqlPrefix +
            " where hostname = '"+requestorHost+"' and '"+
            " username = '"+requestorUsername+"' and '"+
            " application = '"+requestorApplication+"'";

        return getBySQL(sql);
    }

    /** Unlock ALL currently held locks on all events. This simply deletes all rows
  in the lock table. */
    public boolean unlockAll() {

        if (!lockingWorks) return true;      // default behavior if no locking

        String sql = "Delete from " + TableName;

        return  doSQL(sql);
    }

    /** Release ALL locks currently held by this host/user/application. */
    private static final String psUnlockAllMy = "DELETE FROM JASIEVENTLOCK WHERE (HOSTNAME=?) AND (USERNAME=?) AND (APPLICATION=?)";
    public boolean unlockAllMyLocks() {
        //String sql = "Delete from "+TableName+" where "+" hostname='"+requestorHost+
        //"' and "+" username='"+requestorUsername+"' and "+" application='"+requestorApplication+"'";
        //return  doSQL(sql);
        if (!lockingWorks) {
            if (debug) System.out.println( "SolutionLockTN unlockAllMyLocks() lockingWorks == "+lockingWorks);
            return true; // default behavior if no locking
        }

        Connection conn = getConnection();
        if (conn == null) {
            System.err.println("ERROR: SolutionLockTN lock: No DataSource is open.");
            return false;
        }
        PreparedStatement ps = null;
        boolean status = false;
        try {

            if (conn.isClosed()) return false; // check that valid connection exists

            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(psUnlockAllMy);
            ps = conn.prepareStatement(psUnlockAllMy);
            ps.setString(1, requestorHost);
            ps.setString(2, requestorUsername);
            ps.setString(3, requestorApplication);
            status = (ps.executeUpdate() > 0);
            if (status) {
              conn.commit();
            }
        }
        catch (SQLException ex) {
          if (debug) ex.printStackTrace(System.err);
          else System.err.println("SolutionLockTN unlockAllMyLocks sql error: " + ex.getMessage());
          status = false;
        }
        finally {
            try {
                if (ps != null) ps.close();
            }
            catch (SQLException ex) {
            }
        }
        if (debug) System.out.println( "SolutionLockTN unlockAllMyLocks() sql return status= "+status);
        return status;
    }

    /**
     * Returns collection of SolutionLocks for this id. Should only be one.
     */
    private static final String psLockInfo = psSqlPrefix + " WHERE (evid=?)";
    public static Collection getLockInfo(long id) {
        //String sql = psSqlPrefix + " where evid = "+id;
        //return getBySQL(sql);
        ArrayList list = new ArrayList();
        if (!lockingWorks) return list;      // default behavior if no locking

        Connection conn = getConnection();
        if (conn == null) {
            System.err.println("ERROR: SolutionLock getLockInfo: No DataSource is open.");
            return list;
        }
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if ( conn.isClosed() ) {       // check that valid connection exists
              System.err.println("SolutionLockTN getBySQL(c,s) connection c is closed");
              return list;           // list will be empty
            }
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(psLockInfo);
            ps = conn.prepareStatement(psLockInfo);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            java.sql.Date dt = null;
            if (rs != null) {
                while ( rs.next() ) {
                    SolutionLockTN solLock = new SolutionLockTN();
                    solLock.id = rs.getLong(1);
                    solLock.host = rs.getString(2);
                    if (solLock.host == null) solLock.host = "";
                    solLock.application = rs.getString(3);
                    if (solLock.application == null) solLock.application = "";
                    solLock.username = rs.getString(4);
                    if (solLock.username == null) solLock.username = "";
                    // Convert from millisecs to seconds :. /1000.0
                    // Note: time is dbase time (SYSTIME) :. probably local time not UTC.
                    dt = rs.getDate(5); // now true not nominal -aww 2008/02/11
                    if (dt == null) {
                        dt = new java.sql.Date(0l);
                    }
                    solLock.datetime = LeapSeconds.nominalToTrue(dt.getTime()/1000.); // now true not nominal -aww 2008/02/11
                    if (debug) System.out.println("SolutionLockTN getLockInfo: " + solLock);
                    list.add(solLock);
                }
            }
        }
        catch (SQLException ex) {
          if (debug) ex.printStackTrace(System.err);
          else System.err.println("SolutionLockTN sql error: " + ex.getMessage());
        }
        finally {
            try {
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            }
            catch (SQLException ex) {
            }
        }
        return (Collection) list;
    }

    /**
     * Returns collection of SolutionLocks based on the results of the SQL query to an
     * NCDCv1.5 data base. I Returns null if no data is found.  Uses default connection
     * created by DataSource.  */
    protected static Collection getBySQL(String sql) {
        if (getConnection() == null) {
          System.err.println("* No DataSource is open.");
          return null;
        }
        return getBySQL(getConnection(), sql);
    }

    /**
     * Returns collection of SolutionLocks based on the results of the SQL query
     * to the default data base.  Returns empty list if no data is found.  */
    protected static synchronized Collection getBySQL(Connection conn, String sql) {

        ArrayList list = new ArrayList();
        if (!lockingWorks) return list;      // default behavior if no locking

  // Debug
  //        System.out.println("SQL: "+sql);
        Statement sm = null;
        ResultSet rs = null;

        try {
            if ( conn.isClosed() ) {       // check that valid connection exists
              System.err.println("SolutionLockTN getBySQL(c,s) connection c is closed");
              return list;           // list will be empty
            }

            sm = conn.createStatement();
            ResultSetDb rsdb = new ResultSetDb(ExecuteSQL.rowQuery(sm, sql));
            if (rsdb != null) {
              rs = rsdb.getResultSet();
              if (rs != null) {
                  while ( rs.next() ) {
                    SolutionLockTN solLock = new SolutionLockTN();
                    solLock.id = rsdb.getDataLong("EVID").longValue();
                    solLock.host = rsdb.getStringEmpty("HOSTNAME");
                    solLock.application = rsdb.getStringEmpty("APPLICATION");
                    solLock.username = rsdb.getStringEmpty("USERNAME");
                    // Convert from millisecs to seconds :. /1000.0
                    // Note: time is dbase time (SYSTIME) :. probably local time not UTC.
                    solLock.datetime = LeapSeconds.nominalToTrue(rsdb.getDataDate("LDDATE").doubleValue()/1000.0); // now true not nominal -aww 2008/02/11

                    list.add(solLock);
                  }
              }
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
        finally {
            try {
                if (rs != null) rs.close();
                if (sm != null) sm.close();
            }
            catch(SQLException ex) {
            }
        }

        return (Collection) list;
    }

    /** */
    protected static boolean doSQL(String sql) {
        if (getConnection() == null) {
          System.err.println("* No DataSource is open.");
          return false;
        }
        return doSQL(getConnection(), sql);
    }

    /** */
    // Synchronize to avoid conflicts.
    protected static synchronized boolean doSQL(Connection conn, String sql) {

        if (!lockingWorks) return false;      // default behavior if no locking

        boolean status = true;
        Statement sm = null;

        try {
          if ( conn.isClosed() ) {        // check that valid connection exists
            System.err.println("SolutionLockTN doSQL(c,s) connection c is closed");
            return false;
          }

          sm = conn.createStatement();

          if (debug) System.out.println("DEBUG SolutionLock sql: "+sql);

          int nrows = sm.executeUpdate(sql);

          if (debug) System.out.println("DEBUG SolutionLock SQL rows: " + nrows);

          status = true;
          if (nrows >= 0) {
            conn.commit();
          }

        }
        catch (SQLException ex) {
          if (debug) ex.printStackTrace(System.err);
          else System.err.println("SolutionLockTN sql error: " + ex.getMessage());
          status = false;
        }
        finally {
            try {
                if (sm != null) sm.close();
            }
            catch (SQLException ex) {
            }
        }

        return status;
    }

/*
//
//  Tests for 3 cases:
//  1) Lock tables exist on DataSource dbase
//  2) The connection is dead
//  3) Connection is OK but no lock tables exist in the dbase

    public static void main(String args[]) {

      if ( args.length < 2 ) {
          System.out.println("Syntax input args: [dbhost(domain-qualified)]  [dbname]");
          System.exit(0);
      }
      String host   = args[0]; 
      String dbname = args[1];
      String user   = "dbbrow";
      String passwd = "dbbrow";

      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;

      int test = 1;

      DataSource ds;

      // Case 1
      if (test == 1) {
        System.err.println(" *** Test with valid DataSource connection ***");
        System.err.println("Making DataSource connection... "+url);
        ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER , user, passwd);
      // Case 2
      } else if (test == 2) {
        System.err.println(" *** Test with NULL DataSource connection ***");
        System.err.println("Making DataSource connection...");
        ds =  new DataSource();        // make connection
      // Case 3
      } else {
        System.err.println(" *** Test with valid DataSource connection to dbase not supporting locking ***");
        System.out.println("Making connection... "+url);
        ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd);
      }

      EnvironmentInfo.setApplicationName("LockTest");

      System.err.println(ds.toString());

      SolutionLock solLock = SolutionLock.create();

      System.err.println("solLock.isSupported() = "+solLock.isSupported() );

      ArrayList list = (ArrayList) solLock.getAllLocks();

      System.err.println("----- Current locks ------");
      if (list == null) {
        System.err.println("No locks.");
      } else {
        System.err.println("Lock count = "+ list.size());

        for (int i = 0; i< list.size(); i++) {

          SolutionLock sl = (SolutionLock) list.get(i);
          System.out.println(sl.toString());
        }
      }

      int base = 123000;

      Solution sol = Solution.create();
      SolutionLock lock = SolutionLock.create();

      for (int i = 0; i< 5; i++) {
        sol.id.setValue(base + i);

        System.err.print("Lock solution evid = "+sol.id);

        lock.setSolution(sol);
        boolean status = lock.lock();

        System.out.println("      Status = "+status);
      }

      list = (ArrayList) solLock.getAllLocks();


      System.err.println("----- Current locks ------");
      if (list == null) {
        System.err.println("No locks.");
      } else {
        System.err.println("Lock count = "+ list.size());

        System.err.println(SolutionLock.getHeaderString());
        for (int i = 0; i< list.size(); i++) {

          System.out.println(((SolutionLock)list.get(i)).toFormattedString());
        }
      }
      System.err.println("----- Unlock ------");

      for (int i = 0; i< 5; i++) {
        sol.id.setValue(base + i);

        System.err.print("Lock solution evid = "+sol.id);


        lock.setSolution(sol);
        boolean status = lock.unlock();

        System.out.println("      Status = "+status);
      }

      solLock.unlockAllMyLocks();

      System.err.println("----- Current locks ------");
      list = (ArrayList) solLock.getAllLocks();
      if (list == null) {
        System.err.println("No locks.");
      } else {
        System.err.println("Lock count = "+ list.size());
        System.err.println(SolutionLock.getHeaderString());
        for (int i = 0; i< list.size(); i++) {

          System.out.println(((SolutionLock)list.get(i)).toFormattedString());
        }
      }

      long id =11027256;
      solLock.setId(id);

      if (solLock.lock()) {

        String str = "EVENT "+id+" just got LOCKED.\n\n" +
               "Username:    "+solLock.getUsername()+"\n"+
               "Hostname:    "+solLock.host+"\n"+
               "Application: "+solLock.application+"\n"+
               "Time:        "+LeapSeconds.trueToString(solLock.datetime).substring(0, 19); // since all datetime are now true seconds -aww 2008/02/11
        System.out.println(str);

      // lock failed, pop a dialog
      } else {
        String str = "EVENT "+id+" IS LOCKED.\n\n" +
              "Username:    "+solLock.getUsername()+"\n"+
               "Hostname:    "+solLock.host+"\n"+
               "Application: "+solLock.application+"\n"+
               "Time:        "+LeapSeconds.trueToString(solLock.datetime).substring(0, 19); // since all datetime are now true seconds -aww 2008/02/11
        System.out.println(str);
      }

    }
*/
}

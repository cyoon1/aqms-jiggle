package org.trinet.jasi;
import java.util.*;
import org.trinet.util.gazetteer.*;

public interface ChannelableListIF extends java.util.List {

  public static final int INCLUDE = 1;
  public static final int EXCLUDE = 0;

  public void calcDistances(GeoidalLatLonZ loc); // aww 06/11/2004 changed input arg type
  public double getMaximumAzimuthalGap();
  public boolean trim(double dist);
  public Channelable[] getChannelableArray();
  public Channelable getByChannel(Channelable chan);
  public int getIndexOf(Channelable chan);
  public int getNetworkCount();
  public int getChannelCount();
  public int getStationCount();
  public int getTriaxialCount();

  public boolean trim(int count);
  public Channelable getChannelableStartingWith(String str) ;
  public ChannelableListIF cullByChannelNameAttribute(String [] attrValues, int attrId, int includeFlag) ;
  public ChannelableListIF cullByTime(double trueSecs) ;

  public void distanceSort(GeoidalLatLonZ latlonz); // aww 06/11/2004
  public void distanceSort();
  public void distanceOnlySort(GeoidalLatLonZ latlonz); // aww 06/11/2004
  public void sortByStation();

  public boolean insureLatLonZinfo(java.util.Date date);
  public boolean matchChannelsWithDataSource(java.util.Date date) ; // default date to use for lookUp, can be null
  public int matchChannelsWithList(ChannelList channelList) ;

}

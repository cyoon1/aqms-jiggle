package org.trinet.jasi;
import org.trinet.util.*;
//import java.util.regex.*;

public class ChanMatcher extends AbstractChannelNameMatcher {

    public ChanMatcher() { }

    public ChanMatcher(String [] regex) {
      this(regex, 0);
    }
    public ChanMatcher(String [] regex, int flags) {
      super(regex, flags);
    }

    public boolean matches(ChannelIdIF cid) {
      return matches(cid.getChannel()); 
    }
}

// interface implemented by JasiReading:
// NEED wrappers in classes to separate assoc and unassoc vs. delete
// Need comparator methods for equivalent contents in different objects in subclasses
package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;

public interface JasiReadingIF extends Channelable, Correctable, JasiAssociationFitIF, TimeDataIF, DateLookUpIF {

    public static final double LIKE_TIME_RESOLUTION = 0.01; // seconds

    public boolean hasValidTime();
    public boolean copySolutionDependentData(JasiReadingIF jr);
    public boolean hasSameChannel(JasiReadingIF jr);
    public boolean isSameTime(JasiReadingIF jr);
    public boolean isLikeTime(JasiReadingIF jr);
    public boolean isSameType(JasiReadingIF jr);
    public boolean isLikeChanType(JasiReadingIF jr);
    public boolean isLikeChanTypeAndTime(JasiReadingIF jr);
    public boolean isSameChanType(JasiReadingIF jr);
    public boolean isSameChanTypeAndTime(JasiReadingIF jr);
    public boolean isLikeStaType(JasiReadingIF jr);
    public boolean isSameStaType(JasiReadingIF jr);
    public boolean replace(JasiReadingIF jr);

    public double getInWgt();  // input weight stored in db ASSOCxxx tables 
    public void setInWgt(double v);
    public boolean contributes();
    public double getWeightUsed();  // output weight stored in db ASSOCxxx tables 
    public void setWeightUsed(double value);

    public double getQuality();
    public void setQuality(double v);

    public Collection getByTime(double start, double end);
    public Collection getBySolution(Solution sol, String[] readingTypeList); // assoc == true
    public Collection getBySolution(Solution sol, String[] readingTypeList, boolean assoc);

    public double getWarningLevel();

    public void setChannelList(ChannelList list);
    public boolean setChannelObjFromList();
    public boolean setChannelObjFromList(ChannelList list);
    public boolean matchChannelWithDataSource();

    public double calcDistance(); // use only assigned sol for reference coordinates
    public double getSlantDistance(); // use only assigned sol for reference coordinates
    public String getClosestAuthorityString() ;
    public boolean hasCorrection(String corrType);
    public boolean hasCorrection(java.util.Date date, String corrType) ;
    public boolean hasCorrectionAtReadingTime(String corrType) ;
    public DataCorrectionIF getCorrection(String corrType);
    public DataCorrectionIF getCorrectionAtReadingTime(String corrType) ;
    public DataCorrectionIF getCorrection(java.util.Date date, String corrType) ;
    public void setCorrection(DataCorrectionIF dc);
    public boolean existsInDataSource();
    public boolean isReject();
    public void setReject(boolean tf);

    public void require();
    public void unrequire();
}
// end of JasiReadingIF

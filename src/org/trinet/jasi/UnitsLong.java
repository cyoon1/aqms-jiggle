package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;

//TODO: extend generic DataNumber types like DataLong e.g. UnitsDataLong etc.?
public class UnitsLong implements Cloneable, java.io.Serializable {

/** The actual value. */
     protected DataLong value = new DataLong();
     /** The units of the value. Default is Units.UNKNOWN.
     * @See: Units */
     protected int units = Units.UNKNOWN;

     public UnitsLong() { }

     public UnitsLong(UnitsLong data) {
         this.value  = data.value;
         this.units = data.units;
     }

     public UnitsLong(long value, int units) { 
         this.value.setValue(value);
         this.units = units;
     }

     /** Return the units string, e.g. "counts", "counts/(cm/sec)"
     * @See: Units */
     public String getUnitsString() {
       return Units.getString(units);
     }

     /** Return the value of the units type.
     * @See: Units
     */
     public int getUnits() {
       return units;
     }
     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public void setUnits(int iunits) {
       units = iunits;
     }
     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public boolean setUnits(String sunits) {
        if (Units.isLegal(sunits)) {
          units = Units.getInt(sunits);
          return true;
        }
        return false;
     }

     /** Return true if the value is null. */
     public boolean isNull () {
       return value.isNull();
     }
     /** Return the value. Returns NaN if the value is null.*/
     public long longValue() {
       return value.longValue();
     }

     /** Set the value. Units should be set also but will default to counts
     */
     public void setValue(long value) {
       this.value.setValue(value);
     }
     public void setValue(Number value) {
       this.value.setValue(value);
     }
     public void setValue(DataNumber value) {
       if (value.isNull()) this.value.setNull(true);
       this.value.setValue(value);
     }
     public String toString (){
       return value + " " +getUnitsString();
     }
     public Object clone() {
       UnitsLong data = null;
       try {
         data = (UnitsLong) super.clone();
       }
       catch (CloneNotSupportedException ex) {
         ex.printStackTrace();
       }
       data.value = (DataLong) value.clone();
       return data;
     }
}

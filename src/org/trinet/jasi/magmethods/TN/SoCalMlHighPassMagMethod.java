package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;

/**
 * Methods for ML (local magnitude) does a high pass butterworth pre-filter before applying WA amplitude filter.<p>
 *
*/
public class SoCalMlHighPassMagMethod extends SoCalMlMagMethod {

    public SoCalMlHighPassMagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "SoCalMlHighPass";

        // set the waveform filter to Synthetic Wood-Anderson
        setWaveformFilter( new WAHighPassFilter() );

        // cutoff dist
        setMinDistance(30.0);
        setMaxDistance(600.0);
        setMinSNR(8.0);    //per Kate Hutton 4/2002

        setRequireCorrection(true);
    }

    public boolean isValid(MagnitudeAssocJasiReadingIF jr) {
        if (! super.isValid(jr)) return false;
        Amplitude amp = (Amplitude) jr;
        if ( amp.period.isValidNumber() ) { // screen by period -aww 03/13/2007 
            double d = amp.period.doubleValue();
            if (d < 0.05 || d > 5.0) return false; // used to be 0.08 min period 05/22/2007 -aww
        }
        if (! amp.value.isValidNumber() ) return false;
        // screen by min, max value -aww 05/01/2007
        // amplitude threshold for HH is 0.003 to 100 cm and for HL is .003 to 12000 cm
        double d = Math.abs(amp.getValueAsCGS());
        if (! Double.isNaN(d)) {
          if (jr.getChannelObj().isVelocity()) { // HH is 0.001 to 100 cm 
            if ( d < 0.001 || d > 100.0) return false;
          }
          else if (jr.getChannelObj().isAcceleration()) { // HL is .001 to 12000 cm
            if ( d < 0.001 || d > 12000.0) return false;
          }
        }
        //

        return true;
    }

    public boolean isValidForSummaryMag(MagnitudeAssocJasiReadingIF jr) {
        // Do this constraint first for summary mag -aww
        return (isValid(jr) && ((Amplitude)jr).snr.doubleValue() >= 10.);
    }

}

package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;
import org.trinet.util.Format;

public class Ml_BK_MagMethod extends SoCalMlHighPassMagMethod {

    private static final double [] tp =
        new double [] {+0.056, -0.031, -0.053, -0.080, -0.028, +0.015};

    private static double a = 0.;
    private static double b = 0.;

    static {
        double l_r0 = MathTN.log10(7.5);
        double l_r1 = MathTN.log10(502.5);
        b =  2.0 / (l_r1 - l_r0);
        a = -1.0 - b * l_r0;
    }

    public Ml_BK_MagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "Ml_CISN";

        // set the waveform filter to Synthetic Wood-Anderson
        setWaveformFilter( new WAHighPassFilter() ); // NOTE: changed to highpass prefilter on 05/14/2007 aww

        // cutoff dist
        setMinDistance(30.0);
        setMaxDistance(500.0); // NOTE Uhrhammer set at 500 km
        setMinSNR(8.0);    //per Kate Hutton 4/2002

        setRequireCorrection(true);
    }

    /** Override the distance correction of the Richter ML. <br>
     *  Based on an empirical relationship for southern California
     *  derived by Urlhammer at Berkeley using statewide data.
     *  Input is the hypocenter "slant" distance km (not station epicentral distance). <p>
    */
    //double CISN_mlAo(double rdist) {
    public double distanceCorrection(double rdist) {

        double mlogA0 = -9.; // rdist < 1 km or > 500 km

        if( (rdist < 1.) ) { 

          mlogA0 = -9.; // rdist < 1 km 

        }
        else if( (rdist < 8. ) ) { // linear extrapolation

          // bb = (3.708-1.533)/(MathTN.log10(500.)-MathTN.log10(8.));
          // 1.2111 = (2.175)/(2.69897 - 0.90309);  
          mlogA0 = 1.533 + 1.2111 * (MathTN.log10(rdist) - .90309);

        }
        else if ( rdist <= 500. ) { // Chebychev polynomial expansion

          mlogA0 = logA0(rdist);

          for (int j = 0; j < 6; j++) {
            mlogA0 += tp[j] * cheb(j, z(rdist));
            //System.out.println(rdist + " " + j + " " + tp[j] + " " + cheb(j, z(rdist)));
          }

        }

        return mlogA0;

    }

    private double logA0(double rdist) {
        return 1.11 * MathTN.log10( rdist ) + 0.00189 * rdist + 0.591;
    }
    
    // Chebyshev Polynomial
    private double cheb(int n, double x) {
        return Math.cos((double)(n+1) * Math.acos(x));
    }

    // translate scale from r to z
    private double z(double r) {
        return (a + (b * MathTN.log10(r)));
    }

    /* test code below
    public static final class Tester {
        public static void main(String args[]) {
            Ml_BK_MagMethod ml = new Ml_BK_MagMethod();
            Format f = new Format("%5.3f");
            if (args.length > 0) 
              System.out.println (args[0] + " km A0corr = " + ml.distanceCorrection(Double.parseDouble(args[0])));
            else {
              for (int i = 0; i < 502; i++) {
                System.out.println (i + " " + f.form(ml.distanceCorrection(i)));
              }
            }
        }
    }
    */

} // Ml_BK

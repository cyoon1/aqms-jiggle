package org.trinet.jasi.magmethods.TN;
import org.trinet.jasi.magmethods.*;
// Template
public class MdMagMethodTN extends MdMagMethod {

    {
        // Init to default of median summary stat -aww 2012/09/18 
        summaryMagValueStatType = MagnitudeMethodIF.MEDIAN_MAG_VALUE;
    }

    public MdMagMethodTN() {
        super();
    }
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }
    protected double calcChannelMagValue(double tau, double distKm, double vertKm, double chlMagCorr) {
            return -9.; // changed to -9 from -1 to make it obvious to user -aww
    }

    // Init to default of median summary stat -aww 2012/09/18 
    //public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

}

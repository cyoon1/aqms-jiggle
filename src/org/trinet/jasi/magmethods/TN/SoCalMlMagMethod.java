package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;

/**
 * Static methods for ML (local magnitude) calculation.<p>
 *
 * Formula is defined by Richter in Elementry Seismology, pg. 340: <p>
 *
 *                        ML = log A + Cd + Cs<p>
 * Where:<br>
 *                A  = half amp, in mm from a 2,800x Wood-Anderson torsion seismometer<br>
 *                Cd = Southern California distance correction<br>
 *                Cs = emperical station correction<p>
 *
 * Over-rides the distance correction of the Richter ML. <br>
 * This distance correction is based on an emperical relationship for southern
 * California derived by Hiroo Kanamori. Uses HYPOCENTRAL distance not
 * epicentral distance for the calculation and distance correction. Uses
 * epicentral distance for the station distance cutoff range values. <p>
 *
 * The method can use either corrected or uncorrected Wood-Anderson amplitudes
 * (types WAS, WAC, and WAU)
 * @see AmpType()
 *
 * @see ML()
 * @author Doug Given
 * @version */

public class SoCalMlMagMethod extends MlMagMethod {

    public SoCalMlMagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "SoCalMl";

        // set the waveform filter to Synthetic Wood-Anderson
        setWaveformFilter( new WAFilter() );

        // cutoff dist
        setMinDistance(30.0);
        setMaxDistance(600.0);
        setMinSNR(8.0);    //per Kate Hutton 4/2002

        setRequireCorrection(true);
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    /**
     * Override of method in default Richter ML method differs only in that it
     * retrieves the HYPOCENTRAL distance not epicentral distance 
     * from the input Amplitude.
     */ 
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      double value = -9.; // changed from 0. to -9. to make it obvious to user -aww 08/12/2004
      try {
        Double corr = getMagCorr(amp);
        value = calcChannelMagValue(
                        //amp.getDistance(), // Wrong to use true distance with elev - aww 06/11/2004
                        //amp.getHorizontalDistance(), // no, like Richter ML aww 06/11/2004
                        amp.getSlantDistance(), // use slant distance (dist to 0 elev) - aww 06/15/2004
                        getAmpValue(amp),
                        (corr == null)? 0. : corr.doubleValue()
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return value;
    }

    /** Override the distance correction of the Richter ML. <br>
     *  Based on an empirical relationship for southern California
     *  derived by Hiroo Kanamori.
     *  Input is the hypocenter "slant" distance km (not station epicentral distance). <p>
     *  Correction = -log10( 0.3173 * exp(-0.00505 * dist) * pow(dist, -1.14))
    */
    public double distanceCorrection (double distance) {
    // AWW input slant distance should be measured to a datum of zero solution depth 
    // that is, unlike 'true' distance station the station elevation is not included.
    // Channel Ml = log10(A/A0) + staCorr
    // where A0 is amp of the equivalent "zero" Ml magnitude and
    // log10(A0) =  a0 -n*log10(dist) -(k*log10(e)*dist)   (a0 is scalar)
    // value of log10(A0) should equal "-3" at 100 km distance
    // need published reference for magic numbers or algorithm to generate them
    // cf.1993 SSA v83 No.2 p330 
        double magic1 = -0.00505; // k
        double magic2 =  0.3173;  // A0
        double magic3 = -1.14;    // n

        double c = Math.exp(magic1 * distance);
        c = magic2 * c * Math.pow(distance, magic3);
        c = -(MathTN.log10(c));

        //if (debug) System.out.println ("SoCalMl Dist. Corr = "+c);
        return c;
    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings. In the
     * southern California magnitude scheme the distance cutoff is an empirical
     * relationship derived by Kanamori. It is given by:<p>
     *<tt>
     * cutoff = 170.0 * magnitude - 205.0 (all distances are in km)
     *</tt>
     * The formula can yields 0.0 at a magnitude of 1.21, so a minimum cutoff is
     * enforced. This method will never yield a value less then 30.0km. <p>
     * The cutoff distance is compared with station's "epicentral" distance. 
     * The difference between epicentral and slant distances is typically < 2 km,
     * for example, using epiDistance=minCutoff (30. km) and a solution 
     * depth of 10 km, the slantDistance is 31.6 km).
     */
    // mag  cut
    // 1.0  -35
    // 2.0  135
    // 3.0  305
    // 4.0  475
    public double getDistanceCutoff (double magnitude) {
        //Implement property to disable mag filter and return default MaxDistance
        if ( props.getBoolean("disableMagnitudeDistanceCutoff") ) 
           return super.getDistanceCutoff(magnitude);

        // Note in TriMag dist km is compared with station's "slant" distance 
        // here we treating it as epicentral (difference typical < 2 km) -aww 
        double dist = Math.max( getMinDistance(), (170.0 * magnitude) - 205.0 ) ;
        return Math.ceil(Math.min(dist, getMaxDistance())); // be liberal, round up to closest integer km -aww
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }
} // SoCalMlMagMethod


package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;

/**
 * Static methods for ML (local magnitude) calculation.<p>
 *
 * Formula is defined by Richter in Elementry Seismology, pg. 340: <p>
 *
 *                        ML = log A + Cd + Cs<p>
 * Where:<br>
 *                A  = half amp, in mm from a 2,080x Wood-Anderson torsion seismometer<br>
 *                Cd = Northern California REDI distance correction<br>
 *                Cs = emperical station correction<p>
 *
 * Over-rides the distance correction of the Richter ML. <br>
 * This distance correction is based on the Richter Nordquist LogA0
 * table used in redi_ml. Uses HYPOCENTRAL distance.
 * Uses epicentral distance for the station distance cutoff range values. <p>
 *
 * The method can use either corrected or uncorrected Wood-Anderson amplitudes
 * (types WAS, WAC, and WAU)
 * @see AmpType()
 *
 * @see ML()
 * @author Pete Lombard
 * @version */

public class NoCalRediMlMagMethod extends MlMagMethod {

    public NoCalRediMlMagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "NoCalRediMl";

        // set the waveform filter to Synthetic Wood-Anderson
        setWaveformFilter( new WAFilter() );

        // cutoff dist
        setMinDistance(30.0);
        setMaxDistance(600.0);
	//        setMinSNR(8.0);    //per Kate Hutton 4/2002

        setRequireCorrection(true);
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    /**
     * Override of method in default Richter ML method differs only in that it
     * retrieves the HYPOCENTRAL distance not epicentral distance 
     * from the input Amplitude.
     */ 
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      double value = -9.; // changed from 0. to -9. to make it obvious to user -aww 08/12/2004
      try {
        Double corr = getMagCorr(amp);
        value = calcChannelMagValue(
                        amp.getHorizontalDistance(),
                        getAmpValue(amp),
                        (corr == null)? 0. : corr.doubleValue()
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return value;
    }

    public double distanceCorrection (double distance) {
    // input distance is epicentral distance
    // Based on floga0.f of redi_ml, which is based on Richter-Nordquist table
    // Channel Ml = log10(A/A0) + staCorr
    // where A0 is amp of the equivalent "zero" Ml magnitude
      double dist[] = { 0.0, 5.0, 10.0, 20.0, 35.0, 55.0, 73.0, 96.0,
			220.0, 334.0, 445.0, 600.0, 750.0, 1010.0,
			1340.0, 1600.0, 1750.0, 1960.0, 2230.0};

      double corr[] = {1.40, 1.40, 1.50, 1.70, 2.30, 2.70, 2.850,
		       3.00, 3.650, 4.20, 4.60, 5.00, 5.30, 5.70,
		       6.00, 6.20, 6.30, 6.40, 6.50};
      double loga0 = 0.0;
      if (distance < dist[0]) return loga0;
      
      for (int i = 1; i < dist.length; i++) {
	  //System.out.println ("ML.distanceCorrection input distance = "+distance);
	  if (distance < dist[i]) {
	      loga0=corr[i-1] + (corr[i] - corr[i-1] ) * 
		  (distance - dist[i-1]) / (dist[i] - dist[i-1]);
	      if (debug) System.out.println ("ML dist. Corr = "+loga0);
          return loga0;
        }
      }
      return loga0; // out of allowed range
    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations farther then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings. 
     *</tt>
     */
    public double getDistanceCutoff (double magnitude) {
        //Implement property to disable mag filter and return default MaxDistance
        if ( props.getBoolean("disableMagnitudeDistanceCutoff") ) 
           return super.getDistanceCutoff(magnitude);

        // Note in TriMag dist km is compared with station's "slant" distance 
        // here we treating it as epicentral (difference typical < 2 km) -aww 
        double dist = (magnitude < 2.3) ? 22.0 * magnitude - 11.0 : 194.0 * magnitude - 407.0;
        dist = Math.max( getMinDistance(), dist) ;
        return Math.min(dist, getMaxDistance() );
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }
} // NoCalRediMlMagMethod


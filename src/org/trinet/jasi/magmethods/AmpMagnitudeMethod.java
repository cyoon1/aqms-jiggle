package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import java.io.*;

public abstract class AmpMagnitudeMethod extends AbstractMagnitudeMethod implements AmpMagnitudeMethodIF {

    /** Minimum signal-to-noise (SNR) ratio for an amplitude to be used.
    * For example, if this value is set to 1.5 a peak amplitude must be 150% of
    * background noise level to be used. */
    protected double minSNR = Double.MIN_VALUE;
    protected int peakType = AbstractWaveform.ZERO2PEAK;
    protected boolean scanPwave = true;
    protected boolean clipCheck = true;

    public static double DEFAULT_A_CLIP = 0.8;
    public static double DEFAULT_DA_CLIP = .99;
    public static double DEFAULT_DV_CLIP = .99;

    protected double clipFactorA = DEFAULT_A_CLIP;
    protected double clipFactorDA = DEFAULT_DA_CLIP;
    protected double clipFactorDV = DEFAULT_DV_CLIP;

    protected AmpMagnitudeMethod() {
      super(Amplitude.class);
    }

    /**
     * Return the channel magnitude value for the input Amplitude using this magnitude method.
     * and the data of the input.
     * Does not alter the input, only determines the value that should be set.
     */
    public abstract double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException ;

    public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException  {
      return calcChannelMagValue((Amplitude) jr);
    }
    /**
     * Return the station channel Magnitude given the epicentral distance (km) and the amplitude value
     * as required by the mag method.
     */
    public abstract double calcChannelMagValue(double horizDistance, double value) ;
    /**
     * Return the station channel Magnitude given the epicentral distance (km), the amplitude value and
     * a static correction value as required by the mag method.
     */
    public abstract double calcChannelMagValue(double horizDistance, double value, double staticCorrection) ;

    public boolean hasAcceptableSNR(MagnitudeAssocJasiReadingIF jr) {
      Amplitude amp = (Amplitude) jr;
      if (getMinSNR() == Double.MIN_VALUE || amp.snr.isNull()) return true;
      boolean accept = ( getMinSNR() <= amp.snr.doubleValue()) ; // added "equal" to min test 02/27/06 -aww
      if (! accept && debug) {
         System.out.println(methodName + " calcChannelMag channel !hasAcceptableSNR reject: "+
         amp.getChannelObj().toDelimitedSeedNameString(" ")+" snr= "+amp.snr + " minSNR = " + getMinSNR());
      }
      return accept;
    }

    /** Return the minimum SNR value for which amplitudes will contribute to the
    * magnitude.  Returns Double.MIN_VALUE if not set.*/
    public double getMinSNR() {
      return minSNR;
    }
    /** Set the minimum SNR value for which amplitudes will contribute to the
    * magnitude.*/
    public void setMinSNR(double value ) {
      this.minSNR = value;
    }

    /**
     * Set the quality attribute of the input based upon input's internal data
     * and the requirements of this instance.
     */
    public void assignWeight(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
      // should we throw WrongDataTypeException?
      if (! isIncludedReadingType(jr))
        throw new WrongAmpTypeException(
          "assignWeight reading wrong type for MagnitudeMethod " + methodName +
          " : " + jr.toNeatString()
        );
      // NOOP else need to evaluate amp data effecting wt here like ... ?
      // jr.setQuality(1.); // leave unchanged, do not reset reading quality - aww 2009/08/27

      //jr.setInWgt(jr.getQuality()); // Not here, done by calcChannelMag... ! aww 03/04 
      //jr.setWeightUsed(jr.getInWgt()); // Not here, done by calcSummaryMag... ! aww 03/04 
    }

    /**
    * Scan the time-series in the waveform, calculating the appropiate
    * channel magnitude. If this instance has a waveform filter enabled
    * it will be applied to the wavefrom before it is scanned.
    * If scanWaveformTimeWindow is set 'true',
    * the scan will commence at an estimated phase arrival time,
    * based on the input origin time.<p>
    * Signal to noise ratio is calculated for a time window
    * preceding the input origin time.
    * @return null on failure, e.g. no time-series.
    */
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf) {
        return createAssocMagDataFromWaveform(solList, wf, null);
    }
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf, TimeSpan scanSpan) {
        return createAssocMagDataFromWaveform(solList.getSolution(0), wf, scanSpan);
    }
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf) {
        return createAssocMagDataFromWaveform(aSol, wf, null);
    }
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {

       if (wf == null) return null;

       // if there is no time-series, we load it here and unload it when done.
       boolean localLoad = false;
       if (!wf.hasTimeSeries())  {
          if (wf.loadTimeSeries()) {     // try to load it
            localLoad = true;
          } else {
            if (debug)
              System.out.println("DEBUG createAssocMagDataFromWaveform b4 Scan NO timeseries: "+
                        wf.getChannelObj().toDelimitedSeedNameString(" "));
            return null ;                // nothing loaded
          }
       }
       // convert/filter if appropriate
       // Need way of passing the "clipping" level to the waveform instance
       // so that the filter knows whether to flag the resulting signal/amp
       // a clipped timeseries like: wf.setClipCounts(value) -aww
       // PROBLEM: wf may be clipped outside of the timespan of interest for the seismic phase energy, thus
       // to be robust you need to do the wf.getPeakAmplitude(ts,peakType) first before the filter, quite an overhead yuck!!! -aww
       if (wf.isClipped()) {
           if (debug)
              System.out.println("DEBUG createAssocMagDataFromWaveform b4 filter, input wf is clipped, reject: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null; // filter failed
       }

       Waveform filteredWf = wf;
       if (hasWaveformFilter()) {
          filteredWf = (Waveform) wfFilter.filter(wf);
          if (filteredWf == null) {
            if (debug)
              System.out.println("DEBUG createAssocMagDataFromWaveform b4 Filter failed: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
              if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
              return null; // filter failed
          }
       }

       Amplitude amp = null;
       currentSol = aSol; // for tracking purposes
       double originDateTime = aSol.getTime();
       double depth = aSol.getModelDepth(); // aww 2015/10/10
       if (Double.isNaN(depth)) depth = 0.; // moved here from down below in noise section -aww 06/04/2007
       // get peak in energy window
       // Note if "clipping" counts level is known to waveform
       // then it could flag it as clipped after scanning it.
       // if not already done so by filteredWf method. -aww
       //float signalAmp = 0.f;
       if (scanSpan != null) {
          amp = filteredWf.getPeakAmplitude(scanSpan, peakType);
          if (debug)
            System.out.println("DEBUG createAssocMagDataFromWaveform Scan: "+wf.getChannelObj().toDelimitedSeedNameString(" ")+
                               " windowsize= "+scanSpan.getDuration() + " " + scanSpan.toString());
          //signalAmp = filteredWf.scanForNoiseLevel(scanSpan);
       }
       else if (scanWaveformTimeWindow) {
          TimeSpan ts = (scanPwave) ?
              wf.getEnergyTimeSpan(originDateTime, wf.getHorizontalDistance(), depth) :
              wf.getSWaveEnergyTimeSpan(originDateTime, wf.getHorizontalDistance(), depth);

          amp = filteredWf.getPeakAmplitude(ts, peakType);
          //signalAmp = filteredWf.scanForNoiseLevel(ts);
          if (debug)
            System.out.println("DEBUG createAssocMagDataFromWaveform Scan: "+wf.getChannelObj().toDelimitedSeedNameString(" ")+
                               " windowsize= "+ts.getDuration() + " " + ts.toString());
       } else {         // get peak in whole waveform
          amp = filteredWf.getPeakAmplitude(filteredWf.getTimeSpan(), peakType);
          //signalAmp = filteredWf.scanForNoiseLevel();
       }
       // amp can be 'null' if the energy window is outside the available time series
       if (amp == null) {
         if (debug) 
            System.out.println("DEBUG createAssocMagDataFromWaveform peak amp NULL: "+
                   wf.getChannelObj().toDelimitedSeedNameString(" "));
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null;
       }

       // Check for clipping code added below -aww 2009/03/10
       double clip = 0.; // undefined clipping value is 0.
       // Either lookup clip amp from defined JASI_AMPPARMS_VIEW directly using waveform channel and amp date
       // clip = wf.getChannelObj().getClipAmpValue(amp.getDatetime());
       // or instead get clipping amp from correction map (which assumes correction lookup view joined to clip table)
       MagnitudeCalibrationIF calibrData = getCalibrationFor(amp);
       if (calibrData != null) {
         Double clipping = (Double) calibrData.getClipAmp(amp);
         if (clipping == null || clipping.isNaN()) clip = 0.;
         else {
             clip = Math.abs(clipping.doubleValue());
             if  (clip <= 4096.) clip = clip*clipFactorA;
             else clip = (wf.getChannelObj().isAcceleration()) ? clip*clipFactorDA : clip*clipFactorDV;
         }
       }

       // use clipping to reject amp only when clip is a defined value > 0.
       if (clip <= 0.) {
           if (debug) 
               System.out.println(methodName + " DEBUG  createAssocMagDataFromWaveform CLIPPING AMP IS ZERO for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
       }
       else if (clipCheck) {
         //Get peak amp value
         double value = 0.;
         if (hasWaveformFilter()) { // like Wood-Anderson cm, so find raw wf peak in counts
           Sample smpl  // =  wf.closestSample(amp.getTime()); // filtered peak could be "time-shifted" from raw peak
                           = wf.scanForPeak(amp.getTime()-3, amp.getTime()+3); // window 3 secs
           value = (smpl == null || Double.isNaN(smpl.value)) ? 0. : Math.abs(smpl.value);
         }
         else { // no filter raw counts?
           value = amp.getValue().doubleValue();
           if (Double.isNaN(value)) {
             System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform peak AMP IS NaN for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
             if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
             return null;
           }
           value = Math.abs(value);
           if (!amp.halfAmp) value /= 2.; // compare half
         }

         if (value >= clip) {
             if (debug)
               System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK >= CLIP: (" +value+ "," +clip+ ") rejecting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
             if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
             return null;
         }
         else {
             if (debug)
                 System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK < CLIP: (" +value+ "," +clip+ ") accepting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
         }
       }
       // end of clipping check code addition -aww

       // Associate solution to allow slant distance calc using JasiReading method
       amp.assign(aSol); // added 04/25/2005 -aww

       // calc SNR
       //TimeSpan tsp = wf.getPreEnergyTimeSpan( originDateTime, wf.getDistance(), depth); // don't use slant here aww 06/11/2004
       TimeSpan tsp = wf.getPreEnergyTimeSpan( originDateTime, wf.getHorizontalDistance(), depth); // aww 06/11/2004
       float noiseLevel = filteredWf.scanForNoiseLevel(tsp);
       // UNCOMMENT BELOW TO OUTPUT noise level per peak channel amp
       // gives best feedback for a larger ML magnitude input
       /*
         float unfilteredNoiseLevel = wf.scanForNoiseLevel(tsp);
         System.out.println(wf.getChannelObj().toDelimitedSeedNameString(" ") +
               " " + noiseLevel + " " + wf.getChannelObj().getGain(aSol.getDateTime()) + " " + unfilteredNoiseLevel);
       */
       double snr = 0.;
       if (! Float.isNaN(noiseLevel) ) {
          //System.out.println("DEBUG AmpMagnitudeMethod TEST SNR : "+signalAmp/noiseLevel+" ampSNR: "+amp.value.doubleValue()/noiseLevel);
          snr = Math.abs( amp.value.doubleValue() ) / noiseLevel;
          if (!amp.halfAmp) snr /= 2.; // compare half
          amp.snr.setValue(snr);
       }
       // TODO: should check for lop-sided waveform
       //
       // unload time-series
       if (localLoad) wf.unloadTimeSeries();
       // removed hasAcceptableSNR(amp) test below, let subclasses decide in isValid(amp) implementations  -aww 11/02/2007
       return (snr > 1.) ? amp : null;
    }

    /* removed since return is same as parent super class -aww 04/25/2005
    public double getInputWeight(MagnitudeAssocJasiReadingIF jr) {
        // return jr.getQuality();
        // return 1.; // aww 6/04
        return jr.getInWgt(); // aww 6/04
    }
    */

    public void logHeader() {
        System.out.println("    channel                 dist     mag   resid      wt ");
    }
    public void logChannelResidual(MagnitudeAssocJasiReadingIF jr) {
        Amplitude amp = (Amplitude) jr;
        StringBuffer sb = new StringBuffer(132);
        sb.append("  ");
        Concatenate.leftJustify(sb, AbstractMagnitudeMethod.getStnChlNameString(amp), 24);
        Concatenate.format(sb, amp.getHorizontalDistance(), 4,1).append(" ");
        Concatenate.format(sb, amp.getChannelMag().value.doubleValue(),4,2).append(" ");
        Concatenate.format(sb, amp.getChannelMag().residual.doubleValue(),4,2).append(" ");
        Concatenate.format(sb, amp.getWeightUsed(),4,2).append(" ");
        System.out.println(sb.toString());
    }

    public GenericPropertyList getProperties() {
      GenericPropertyList props = super.getProperties();

      props.setProperty("minSNR", minSNR);
      props.setProperty("peakType", (peakType == AbstractWaveform.PEAK2PEAK ? "p2p" : "z2p"));
      props.setProperty("clipCheck", clipCheck);
      props.setProperty("scanPwave", scanPwave);

      props.setProperty("clippingAmpScalar.analog", clipFactorA);
      props.setProperty("clippingAmpScalar.digital.acc", clipFactorDA);
      props.setProperty("clippingAmpScalar.digital.vel", clipFactorDV);

      return props;
    }

    public void initializeMethod() {
        super.initializeMethod();
        if (props == null) return; // may not have props set

        if (props.getProperty("peakType") != null) 
               peakType = (props.getProperty("peakType").equalsIgnoreCase("p2p")) ? AbstractWaveform.PEAK2PEAK : AbstractWaveform.ZERO2PEAK;

        if (props.getProperty("minSNR") != null)
               setMinSNR(props.getDouble("minSNR"));

        if (props.getProperty("scanPwave") != null)
               scanPwave = props.getBoolean("scanPwave");

        if (props.getProperty("clipCheck") != null)
               clipCheck = props.getBoolean("clipCheck");

        clipFactorA = props.getDouble("clippingAmpScalar.analog", DEFAULT_A_CLIP);
        clipFactorDA = props.getDouble("clippingAmpScalar.digital.acc", DEFAULT_DA_CLIP);
        clipFactorDV = props.getDouble("clippingAmpScalar.digital.vel", DEFAULT_DV_CLIP);

        stateIsValid = true;
    }

    /**
     * Sets or reinitializes the default setting for this method.
     */
    public void setDefaultProperties() {
        super.setDefaultProperties();
        clipCheck = true;
        scanPwave = true;
        setMinSNR(3.0);
        peakType = AbstractWaveform.ZERO2PEAK;
        clipFactorA = DEFAULT_A_CLIP;
        clipFactorDA = DEFAULT_DA_CLIP;
        clipFactorDV = DEFAULT_DV_CLIP;
    }
}

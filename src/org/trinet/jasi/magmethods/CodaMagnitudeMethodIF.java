package org.trinet.jasi.magmethods;
public interface CodaMagnitudeMethodIF extends MagnitudeMethodIF {
    // Caching methods remove or move to super class -aww
    public void setWaveformCacheLoading(boolean value) ;
    public boolean isWaveformCacheLoadingEnabled() ;
}

/*
l local
r regional
t teleseism
u unknown
*/
package org.trinet.jasi;
public class GType implements Cloneable {

    public String code;
    public String name;

    public GType() { }

    public GType(String code, String name) {
      this.code = code;
      this.name = name;
    }

    public GType(GType gt) {
      this.code = gt.code;
      this.name = gt.name;
    }
   
    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(code).append(" ").append(name);
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof GType)) return false;
        GType gt = (GType) obj;
        return (this.code.equals(gt.code) && this.name.equals(gt.name));
    }

    public Object clone() {
        GType gt = new GType();
        gt.code = this.code;
        gt.name = this.name;
        return gt;
    }
}

package org.trinet.jasi;

import java.util.*;

/**
 * Provides comparator() method to sort a list of Channelable objects by distance
 * then net, name, component. The distance from the sort point must already be set
 * for the compared objects with channelable.setDistance(double). <p>
 */

public class ChannelDistanceSorter implements Comparator {

    /** Comparator for component sort. */
    ChannelNameSorter cnSorter = new ChannelNameSorter();

    public int compare(Object o1, Object o2) {

        if ((o1 instanceof Channelable) && (o2 instanceof Channelable)) {

          Channelable ch1 = (Channelable) o1;
          Channelable ch2 = (Channelable) o2;

          double diff = (ch1.getHorizontalDistance() - ch2.getHorizontalDistance()); // 02/14/2005 -aw

          if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

          if (diff < 0.0) {
              return -1;
          } else if (diff > 0.0) {
              return  1;
          } else { // same dist sort by name 
              return cnSorter.compare(ch1.getChannelObj(), ch2.getChannelObj());
          }
        }

        return 0;  // wrong object types, void class cast exception
    }
}

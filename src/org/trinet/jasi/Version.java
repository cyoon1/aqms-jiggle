package org.trinet.jasi;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

/** Static member referenced by applications needing to track code revision dates. */
public final class Version {
    /** Holder for version tag mapped to date, update whenever code is revised. */
    public final static String buildTag = "2020.03.17";

    public final static java.util.Date getBuildDate() { 
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        return sdf.parse(buildTag, new ParsePosition(0));
    }
}

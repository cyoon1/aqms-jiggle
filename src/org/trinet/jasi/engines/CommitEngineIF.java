package org.trinet.jasi.engines;
import org.trinet.jasi.JasiCommitException;
public interface CommitEngineIF {
    boolean commit() throws JasiCommitException ;
    void setAutoCommit(boolean value) ;
    boolean isAutoCommit() ;
}


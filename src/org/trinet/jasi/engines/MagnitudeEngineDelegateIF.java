package org.trinet.jasi.engines;
import org.trinet.jasi.*;
public interface MagnitudeEngineDelegateIF extends SolutionEngineDelegateIF {

  public boolean hasMagnitudeService();
  public MagnitudeEngineIF getMagnitudeEngine();
  public boolean solve(Magnitude mag);
  public boolean calcMag(Solution sol);
  public boolean calcMagFromWaveforms(Solution sol);

  public boolean calcMag(Solution sol, String magType);
  public boolean calcMagFromWaveforms(Solution sol, String magType);
  //
  //public Magnitude calcSummaryMag(Magnitude);
  //public Magnitude calcSummaryMag(Solution sol);
  //
  public void setChannelList(ChannelList chanList);
}

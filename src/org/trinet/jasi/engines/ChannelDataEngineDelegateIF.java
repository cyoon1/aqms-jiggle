package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public interface ChannelDataEngineDelegateIF extends EngineDelegateIF {
    public void loadChannelList(java.util.Date date);
    public void setChannelList(ChannelList chanList);
    public boolean hasChannelList();
}

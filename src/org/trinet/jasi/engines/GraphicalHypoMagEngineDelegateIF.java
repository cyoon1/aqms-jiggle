package org.trinet.jasi.engines;
import javax.swing.*;
public interface GraphicalHypoMagEngineDelegateIF extends HypoMagEngineDelegateIF {
  public Action createEnginePropertyAction(); // e.g. for menubar, toolbar use
}

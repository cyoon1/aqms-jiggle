package org.trinet.jasi;

import java.text.*;
import java.util.*;
import java.io.*;
import org.trinet.util.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.util.gazetteer.*;

/**
 * Event selection properties.
 * Default properties for application should be defined in "<application_home_dir>/<defaultPropFileName>" (UNIX).
 * User (or overridden) properties should be defined in "<user_home_dir/application_dir>/<userPropFileName>" (UNIX).
 * The application default property filename is "eventSelection.props", application specific implementation may override this.
 * Known event selection property names :<br>
catalogOriginEndTime<br>
catalogOriginStartTime<br>
catalogRelativeTimeUnits<br>
catalogRelativeTimeValue<br>
catalogRelativeTimeEvid<br>
catalogMaxRows<br>
catalogTimeMode<br>
eventAuth<br>
eventValidFlag<br>
eventSubsource<br>
eventTypesExcluded<br>
eventTypesIncluded<br>
originProcTypesExcluded<br>
originProcTypesIncluded<br>
originAlgo<br>
originAmpCountRange<br>
originAuth<br>
originDepthRange<br>
originDistRange<br>
originDummyFlag<br>
originErrHorizRange<br>
originErrDepthRange<br>
originFirstMoRange<br>
originFixDepthFlag<br>
originFixEpiFlag<br>
originGapRange<br>
originPhaseCountRange<br>
originPhaseCountUsedRange<br>
originQualityRange<br>
originRmsRange<br>
originSPhasesRange<br>
originSubsource<br>
originType<br>
magAlgo<br>
magAuth<br>
magDistRange<br>
magErrRange<br>
magGapRange<br>
magPrefNull<br>
magPrefType<br>
magQualityRange<br>
magStaRange<br>
magObsRange<br>
magSubsource<br>
magValueRange<br>
regionType<br>
regionExcluded<br>
regionBorderPlaceName<br>
regionBoxLat<br>
regionBoxLon<br>
regionPolygon<br>
regionRadiusPoint<br>
regionRadiusValue<br>
regionRadiusPlaceName<br>
<p>
Declared property values add either an inclusive or exclusive filtering condition
for event selection from the data source. Either don't declare the named property
or comment it out in the property file when all possible event values are desired .</p>
Example property text value assignments (leading # => line commented out) :<br>
</pre>
localNetCode= CI
jasiObjectType= TRINET
catalogOriginEndTime=2005-01-01\ 00\:00\:00.000\ GMT+00\:00
catalogOriginStartTime=1931-12-01\ 00\:00\:00.000\ GMT+00\:00
catalogRelativeTimeUnits= Hours
catalogRelativeTimeValue= 24
catalogMaxRows= 500
#catalogTimeMode= absolute
catalogTimeMode= relative
#
#ALIAS eventValidFlag same as eventSelectFlag
eventValidFlag= true
#eventAuth= CI
#eventSubsource= Jiggle
#EVENTTYPES: "eq", "quarry", "sonic", "nuclear", "trigger", "unknown"
eventTypesIncluded= earthquake quarry
eventTypesExcluded= 
originGTypesIncluded= local regional
originGTypesExcluded= teleseism
#
#originProcTypesIncluded= automatic human
#originProcTypesExcluded= final
originDummyFlag= false
#originAuth= CI
#originSubsource= Jiggle
#originType= H
#originAlgo= HYP2000
#originQualityRange= 0.0 1.0
#originGapRange= 0 180
#originDistRange= 0. 100.
#originSPhasesRange= 0 9999
#originFirstMoRange= 0 9999
#originFixDepthFlag= false
#originFixEpiFlag= false
#originDepthRange= 0. 25.
#originRmsRange= 0. 5.0
#originErrHorizRange= 0. 5.
#originErrDepthRange= 0. 10.
#originPhaseCountRange= 0 5000
#originPhaseCountUsedRange= 0 5000
#originAmpCountRange= 0 5000
#
#magAuth= CI
#magSubsource= Jiggle
##magAlgo= SoCalMl
##magQualityRange= 0.0 1.0
#magStaRange= 0 5000
#magObsRange= 0 10000
#magPrefType= l
#magPrefNull= false
#magErrRange= 0. 0.5
##magGapRange= 0 180
#magDistRange= 0. 101.
#magValueRange= 0. 9.9
#REGION TYPES: box point place border polygon
#regionType=polygon
#TEST POINT near Baker, CA
#regionRadiusPoint= 35.3 -116.1
#regionRadiusValue= 25.
#regionBorderPlaceName= AirportLake
#regionRadiusPlaceName= BORON QUARRY
#regionBoxLat=  35.3 35.5
#regionBoxLon= -117.8 -117.6
#POLYGON below same as AirportLake border:
#regionPolygon = 35.87752 -117.70794 35.88168 -117.73871 35.90356 -117.75666 35.9171 -117.79641 \
#                35.92856 -117.80538 35.93377 -117.77461 35.92544 -117.73615 35.89939 -117.6964 \
#                35.88168 -117.68999
#END of EventSelection properties
</pre>
* @see GenericPropertyList#initialize(String subdir, String userFileName, String defaultFileName)
* @see #getKnownPropertyNames()
*/
public class EventSelectionProperties extends JasiPropertyList {

    /* Deprecated prefix to boolean properties used by Earthworm.
     * E.g. "SelectAttribute_human=TRUE" would select only human reviewed events.
     * */
    public static final String prefix = "SelectAttribute_";  // deprecate? used only by SolutionEW?

    //public static final String BOOLEAN_SUFFIX= "_TF"; // clearer than above prefix usage

    public static final String DEFAULT_FILENAME = "eventSelection.props";

    public static final String REGION_ANYWHERE = "anywhere";
    public static final String REGION_BOX      = "box";
    public static final String REGION_POINT    = "point";
    public static final String REGION_PLACE    = "place";
    public static final String REGION_BORDER   = "border";
    public static final String REGION_POLYGON  = "polygon";
    public static final String REGION_POLYLIST = "polylist";

//    static final int AbsoluteTime = 0;
//    static final int RelativeTime = 1;

    /** Name of the file for reading and saving. */
//    String filename = DEFAULT_FILENAME;

    public EventSelectionProperties() {
        //super(DEFAULT_FILENAME) // aww - don't do this, app user must load
    }

    /**
     * String file is the name of the property file.
     */
    public EventSelectionProperties(String file) {
        super(file);
    }

    /**
     * String file is the name of the property file.
     * This form of Constructor uses explicite path/file as passed and
     * does not try to build a path using <userhome>.
     */
    public EventSelectionProperties(String userPropertiesFileName, String defaultPropertiesFileName) {
        super(userPropertiesFileName, defaultPropertiesFileName);
    }

    /**
     * Copy the EventSelectionProperties object
     */
    public EventSelectionProperties(EventSelectionProperties props) {
        super(props);
       //setFilename(DEFAULT_FILENAME);  // name copied from input
    }

    /** Override removes the named property if the input value String is null. */
    public Object setProperty(String key, String value) {
        return (value == null) ? remove(key) : super.setProperty(key, value);
    }
    /** Override removes the named property if the input value String is null. */
    public void setProperty(String str, Number val) {
        if (val == null) setProperty(str, (String) null);
        else setProperty(str, val.toString());
    }
    /** Override removes the named property if the input value String is null. */
    public void setProperty(String str, String [] values) {
        if (values == null) setProperty(str, (String) null);
        else super.setProperty(str, values);
    }
    /** Override removes the named property if the input value String is null. */
    public void setProperty(String property, NumberRange nr) {
        if (nr == null) setProperty(property, (String) null);
        else setProperty(property, nr.toString());
    }
    /** Override removes the named property if the input value String is null. */
    public void setDateTime(String property, DateTime dt) {
        if (dt == null) setProperty(property, (String) null);
        else setProperty(property, dt.toString());
    }

    /**
    /*
    // Override defaults user's subdir to "jiggle" but is this wise?
    // Chance of overwriting "jiggle" file with that of "other" apps
    // apps may use different event filter file configurations
    public String getFiletype() {
      // return (filetype.equals("")) ? "jiggle" : filetype;
      if (filetype.equals("")) {
        // declare userhome subdir for property file
        filetype = (EnvironmentInfo.hasApplicationName()) ?
          EnvironmentInfo.getApplicationName().toLowerCase() : "jiggle";
      return filetype;
    }
    */

    /** Return a TimeSpan that represents the starting and ending times defined by this
    * event selection properties. */
    public TimeSpan getTimeSpan() {
        return new TimeSpan(getStartSeconds(), getEndSeconds());
    }
    public TimeSpan getTimeSpanRelative() {
        return new TimeSpan(getStartSecondsRelative(), getEndSecondsRelative());
    }
    public TimeSpan getTimeSpanAbsolute() {
        return new TimeSpan(getStartSecondsAbsolute(), getEndSecondsAbsolute());
    }

    /** Return <i>true</i> if starting and ending times are both not equal to zero
     * and starting time is less than or equal to ending time.
     */
    public boolean hasValidTimeSpan() {
        double s = getStartSeconds();
        double e = getEndSeconds();
        return (s == 0. && e == 0.) ?  false : ( s <= e);
    }

     /** Returns a string describing the time mode selected: relative or absolute. */
    public String getTimeMode() {
        return getProperty("catalogTimeMode", "") ;
    }
    public void setTimeModeAbsolute() {
        setProperty("catalogTimeMode", "absolute") ;
    }
    public void setTimeModeRelative() {
        setProperty("catalogTimeMode", "relative") ;
    }
    public void setTimeModeEvid() {
        setProperty("catalogTimeMode", "evid") ;
    }

    public void setRelativeTimeEvid(long evid) {
        setProperty("catalogRelativeTimeEvid", evid) ;
    }

     /** Return 'true' if time mode is "absolute". */
    public boolean modeIsAbsolute() {
        return getTimeMode().equalsIgnoreCase("absolute");
    }
     /** Return 'true' if time mode is "absolute". */
    public boolean modeIsRelative() {
        return getTimeMode().equalsIgnoreCase("relative");
    }
     /** Return 'true' if time mode is "absolute". */
    public boolean modeIsEvid() {
        return getTimeMode().equalsIgnoreCase("evid");
    }

    /** Return the length of the time window in seconds.*/
    public double getTimeRelativeSeconds() {
        String units = getProperty("catalogRelativeTimeUnits");
        //if (units == null)
        //    System.err.println("EventSelectionProperties ERROR catalogRelativeTimeUnits UNDEFINED!");
        double value = getDouble("catalogRelativeTimeValue");
        return value *  TimeUnits.getSecondsIn(units);
    }

    public String getRelativeTimeUnits() {
        return getProperty("catalogRelativeTimeUnits");
    }

    public double getRelativeTimeValue() {
        return getDouble("catalogRelativeTimeValue");
    }

    public long getRelativeTimeEvid() {
        return getLong("catalogRelativeTimeEvid", -1l);
    }

    /** Get the epoch seconds of the time window start. */
    public double getStartSeconds() {
        return modeIsRelative() ?
            getStartSecondsRelative() : getStartSecondsAbsolute();
    }
    public double getStartSecondsAbsolute() {
        String str = getProperty("catalogOriginStartTime");
        return (str != null) ? LeapSeconds.stringToTrue(str) : 0.; // UTC time -aww 2008/02/10
    }
    public double getStartSecondsRelative() {
        return new DateTime().getTrueSeconds() - getTimeRelativeSeconds();    // (NOW - delta) -aww 2008/02/10
    }

    /** Get the epoch seconds of the time window end. */
    public double getEndSeconds() {
        return modeIsRelative() ? 
            getEndSecondsRelative() : getEndSecondsAbsolute();
    }
    public double getEndSecondsAbsolute() {
        String str = getProperty("catalogOriginEndTime");
        return (str != null) ? LeapSeconds.stringToTrue(str) : 0.; // UTC time -aww 2008/02/10
    }
    public double getEndSecondsRelative() {
        return new DateTime().getTrueSeconds(); // NOW -aww 2008/02/10
    }

    /** Get the epoch seconds of the absolute time window start. Note that absolute
    * mode may or may not be selected.
    * @See: modeIsAbsolute() */
    public double getStartAbsolute() {
        return LeapSeconds.stringToTrue(getProperty("catalogOriginStartTime")); // UTC time -aww 2008/02/10
    }

    /** Get the epoch seconds of the time window end. Note that absolute
    * mode may or may not be selected.
    * @See: modeIsAbsolute()*/
    public double getEndAbsolute() {
        return LeapSeconds.stringToTrue(getProperty("catalogOriginEndTime")); // UTC time -aww 2008/02/10
    }

    /** Set the event selection time window */
    public void setTimeSpan(double epochStart, double epochEnd) {
        setProperty("catalogTimeMode", "absolute");
        setProperty("catalogOriginStartTime", LeapSeconds.trueToString(epochStart)); // UTC time -aww 2008/02/10
        setProperty("catalogOriginEndTime",  LeapSeconds.trueToString(epochEnd)); // UTC time -aww 2008/02/10
    }

    /** Set the event selection time window */
    public void setTimeSpan(TimeSpan ts) {
        setProperty("catalogTimeMode", "absolute");
        setTimeSpan(ts.getStart(), ts.getEnd());
    }

    /** Set the event selection time window to some number of hours back from now.
    * Returns the time span. */
    // method doesn't appear to be used anywhere -aww
    public TimeSpan setTimeWindow(double hoursBack) {
        setTimeWindow(hoursBack, TimeUnits.getUnitsName(TimeUnits.HOUR)); // aww 11/29/04
      /* removed 11/29/04 -aww
        setProperty("catalogTimeMode", "relative");
        final double secondsPerHour = 3600.;
        Calendar cal = Calendar.getInstance();
        double now = new DateTime().getTrueSeconds();      // current epoch sec (millisecs->seconds)
        double then = now - (secondsPerHour * hoursBack);   // convert to seconds
        setTimeSpan(then, now); // set absolute time reference
      */
        return getTimeSpan();
    }

    /** Set the event selection time window to some number of hours back from now.
    * Returns the time span. */
    public void setTimeWindow(double value, String units) {
     // a TimeUnits string like : "Seconds", "Minutes", "Hours", "Days", "Weeks", "Years"
        setProperty("catalogTimeMode", "relative");
        setProperty("catalogRelativeTimeValue", ""+value);
        setProperty("catalogRelativeTimeUnits", units);
        return;
   }

    /**
     * Set values for certain essential properties so the program will work in the absence of
     * a default 'properties' file.
     */
    public void setRequiredProperties() {
        // Force app user to set "catalogMaxRows" property with expected count
        // because "open" ended event filter criteria could result in a very
        // large result set being returned.
        setMaxCatalogRows(5000); // aww changed max default to 5000

        // Force relative time attributes
        setTimeWindow(24, TimeUnits.getUnitsName(TimeUnits.HOUR));

        //e.selectflag force event selected
        //setEventValidFlag(true);

        //o.bogusflag force non bogus preferred origin
        //setProperty("dummyFlag",         "FALSE");
        // allow user to get both bogus and nonbogus types by not specifying this property:
        //setOriginDummyFlag(false);   // removed 07/21/2005 aww

        // quality attributes
        // o.quality n.quality ???

        //setMagValueRange(new DoubleRange(-1.0,10.));
        //setOriginPhaseCountRange(new IntegerRange(0,9999));
        //setOriginRmsRange(new DoubleRange(0., 999.9));
        //setOriginErrHorizRange(new DoubleRange(0., 999.9));
        //setOriginDepthRange(DoubleRange distRange) {

        // removed required defaulted region attributes - 05/02/2005 aww
        // "anywhere", "box", "point", "border", "place", "polygon", "polylist"
        setProperty("regionType", REGION_ANYWHERE);

        /*
        setProperty("regionBoxLat", "-90.0  90.0");
        setProperty("regionBoxLon", "-180.0 180.0");

        setProperty("regionRadiusPoint", "34.0 -118.5");
        setProperty("regionRadiusValue",   "50.0");        //km
        */

    }

// Commented out DDG 11/5/04 - use superclass which calls setRequiredProps
// doesn't invoke the setting required props as does original super class
// May need to subclass or have method like reset(boolean setRequired) -aww
//    public void reset() {
//    readDefaultProperties();
//    readUserProperties();
//    }

// max rows returned for SQL event queries
    /** A maximum ROWNUM value for a SQL query by event selection properties.
     * If value < 0 a load by properties returns all rows satisfying properties.
     * Returns -1 when not the property is not specified.
     * */
    public int getMaxCatalogRows() {
        return (isSpecified("catalogMaxRows")) ? getInt("catalogMaxRows") : -1;
    }
    /** Number of rows (ROWNUM) to return for a SQL query by event selection properties.
     * Setting a value of 0 returns NO rows. A value < 0 will return ALL rows.
     */
    public void setMaxCatalogRows(int maxRows) {
        setProperty("catalogMaxRows", maxRows);
    }

/////  EVENT table property set, get methods
// e.auth
    public void setEventAuth(String auth) {
        setProperty("eventAuth", auth);
    }
    public String getEventAuth() {
        return getProperty("eventAuth");
    }
// e.subsource
    public void setEventSubsource(String subsource) {
        setProperty("eventSubsource", subsource);
    }
    public String getEventSubsource() {
        return getProperty("eventSubsource");
    }

// e.selectflag
/** Mapped to event table selection flag
 * */
    public Boolean getEventValidFlag() {
        return (isSpecified("eventValidFlag")) ?
            new Boolean(getBoolean("eventValidFlag")) : null;
    }

/**  Mapped to event table selection flag.
 * */
    public void setEventValidFlag(boolean tf) {
        setProperty("eventValidFlag", tf);
    }
    public void setEventValidFlag(Boolean tf) {
        if (tf != null) setProperty("eventValidFlag", tf.booleanValue());
        else remove("eventValidFlag");
    }

    public void unsetEventValidFlag() {
        remove("eventValidFlag");
    }

// e.etype
    public void setEventTypesIncluded(String [] etypes) {
        setProperty("eventTypesIncluded", etypes);
    }
    public void setEventTypesIncluded(String etypes) {
        setProperty("eventTypesIncluded", etypes);
    }

    public String [] getEventTypesIncluded() {
        return getStringArray("eventTypesIncluded");
    }

    public StringList getEventTypesIncludedList() {
        return getStringList("eventTypesIncluded");
    }

    public void setEventTypesExcluded(String [] etypes) {
        setProperty("eventTypesExcluded", etypes);
    }
    public void setEventTypesExcluded(String etypes) {
        setProperty("eventTypesExcluded", etypes);
    }

    public String [] getEventTypesExcluded() {
        return getStringArray("eventTypesExcluded");
    }
    public StringList getEventTypesExcludedList() {
        return getStringList("eventTypesExcluded");
    }

// o.gtype
    public void setOriginGTypesIncluded(String [] gtypes) {
        setProperty("originGTypesIncluded", gtypes);
    }
    public void setOriginGTypesIncluded(String gtypes) {
        setProperty("originGTypesIncluded", gtypes);
    }

    public String [] getOriginGTypesIncluded() {
        return getStringArray("originGTypesIncluded");
    }

    public StringList getOriginGTypesIncludedList() {
        return getStringList("originGTypesIncluded");
    }

    public void setOriginGTypesExcluded(String [] gtypes) {
        setProperty("originGTypesExcluded", gtypes);
    }
    public void setOriginGTypesExcluded(String gtypes) {
        setProperty("originGTypesExcluded", gtypes);
    }

    public String [] getOriginGTypesExcluded() {
        return getStringArray("originGTypesExcluded");
    }
    public StringList getOriginGTypesExcludedList() {
        return getStringList("originGTypesExcluded");
    }

/////  ORIGIN table property set, get methods
// o.rflag
    public void setOriginProcTypesIncluded(String [] ptypes) {
        setProperty("originProcTypesIncluded", ptypes);
    }
    public void setOriginProcTypesIncluded(String ptypes) {
        setProperty("originProcTypesIncluded", ptypes);
    }

    public String [] getOriginProcTypesIncluded() {
        return getStringArray("originProcTypesIncluded");
    }
    public StringList getOriginProcTypesIncludedList() {
        return getStringList("originProcTypesIncluded");
    }

    public void setOriginProcTypesExcluded(String [] ptypes) {
        setProperty("originProcTypesExcluded", ptypes);
    }
    public void setOriginProcTypesExcluded(String ptypes) {
        setProperty("originProcTypesExcluded", ptypes);
    }

    public String [] getOriginProcTypesExcluded() {
        return getStringArray("originProcTypesExcluded");
    }
    public StringList getOriginProcTypesExcludedList() {
        return getStringList("originProcTypesExcluded");
    }

// o.bogusflag
    public Boolean getOriginDummyFlag() {
        if (isSpecified("dummyFlag")) {
          return new Boolean(getBoolean("dummyFlag"));
        }
        return (isSpecified("originDummyFlag")) ?
            new Boolean(getBoolean("originDummyFlag")) : null;
    }
    /**
     * Sets "originDummyFlag" property value to input boolean value.
     * Flag value is used as an selection filter condition.
     */
    public void setOriginDummyFlag(boolean tf) {
        setProperty("originDummyFlag", tf);
    }
    /**
     * If input is not null, sets "originDummyFlag" property value to boolean value,
     * otherwise if null, removes "originDummyFlag" property, thus removing flag
     * as a selection filter condition (Origin.bogusFlag values to accept). 
     */
    public void setOriginDummyFlag(Boolean tf) {
        if (tf != null) setProperty("originDummyFlag", tf.booleanValue());
        else remove("originDummyFlag");
    }

    public void unsetOriginDummyFlag() {
        remove("originDummyFlag");
    }

// o.auth
    public void setOriginAuth(String auth) {
        setProperty("originAuth", auth);
    }
    public String getOriginAuth() {
        return getProperty("originAuth");
    }
// o.subsource
    public void setOriginSubsource(String subsource) {
        setProperty("originSubsource", subsource);
    }
    public String getOriginSubsource() {
        return getProperty("originSubsource");
    }
// o.type
    public void setOriginType(String type) {
        setProperty("originType", type);
    }
    public String getOriginType() {
        return getProperty("originType");
    }
// o.algorithm
    public void setOriginAlgo(String algo) {
        setProperty("originAlgo", algo);
    }
    public String getOriginAlgo() {
        return getProperty("originAlgo");
    }
// o.quality
    public void setOriginQualityRange(DoubleRange qual) {
        setProperty("originQualityRange", qual);
    }
    public DoubleRange getOriginQualityRange() {
        return getDoubleRange("originQualityRange");
    }
// o.gap
    public void setOriginGapRange(IntegerRange gapRange) {
        setProperty("originGapRange", gapRange);
    }
    public IntegerRange getOriginGapRange() {
        return getIntegerRange("originGapRange");
    }
// o.distance
    public void setOriginDistRange(DoubleRange distRange) {
        setProperty("originDistRange", distRange);
    }
    public DoubleRange getOriginDistRange() {
        return getDoubleRange("originDistRange");
    }
// o.nbs
    public void setOriginSPhaseRange(IntegerRange sPhaseRange) {
        setProperty("originSPhasesRange", sPhaseRange);
    }
    public IntegerRange getOriginSPhaseRange() {
        return getIntegerRange("originSPhasesRange");
    }
// o.nbfm
    public void setOriginFirstMoRange(IntegerRange fMoRange) {
        setProperty("originFirstMoRange", fMoRange);
    }
    public IntegerRange getOriginFirstMoRange() {
        return getIntegerRange("originFirstMoRange");
    }
// o.fdepth
    public void setOriginFixDepthFlag(boolean tf) {
        setProperty("originFixDepthFlag", tf);
    }
    public Boolean getOriginFixDepthFlag() {
        return ( isSpecified("originFixDepthFlag") ) ?
            new Boolean(getBoolean("originFixDepthFlag")) : null;
    }
    public void setOriginFixDepthFlag(Boolean tf) {
        if (tf != null) setProperty("originFixDepthFlag", tf.booleanValue());
        else remove("originFixDepthFlag");
    }

    public void unsetOriginFixDepthFlag() {
        remove("originFixDepthFlag");
    }
// o.fepi
    public void setOriginFixEpiFlag(boolean tf) {
        setProperty("originFixEpiFlag", tf);
    }
    public Boolean getOriginFixEpiFlag() {
        return ( isSpecified("originFixEpiFlag") ) ?
            new Boolean(getBoolean("originFixEpiFlag")) : null;
    }
    public void setOriginFixEpiFlag(Boolean tf) {
        if (tf != null) setProperty("originFixEpiFlag", tf.booleanValue());
        else remove("originFixEpiFlag");
    }
    public void unsetOriginFixEpiFlag() {
        remove("originFixEpiFlag");
    }
// o.depth
    public void setOriginDepthRange(DoubleRange distRange) {
        setProperty("originDepthRange", distRange);
    }
    public DoubleRange getOriginDepthRange() {
        return getDoubleRange("originDepthRange");
    }
// o.wrms
    public void setOriginRmsRange(DoubleRange rmsRange) {
        setProperty("originRmsRange", rmsRange);
    }
    public DoubleRange getOriginRmsRange() {
        return getDoubleRange("originRmsRange");
    }
// o.erhor
    public void setOriginErrHorizRange(DoubleRange errRange) {
        setProperty("originErrHorizRange", errRange);
    }
    public DoubleRange getOriginErrHorizRange() {
        return getDoubleRange("originErrHorizRange");
    }
// o.sdep
    public void setOriginErrDepthRange(DoubleRange errRange) {
        setProperty("originErrDepthRange", errRange);
    }
    public DoubleRange getOriginErrDepthRange() {
        return getDoubleRange("originErrDepthRange");
    }
// o.totalarr
    public void setOriginPhaseCountRange(IntegerRange phaseRange) {
        setProperty("originPhaseCountRange", phaseRange);
    }
    public IntegerRange getOriginPhaseCountRange() {
        return getIntegerRange("originPhaseCountRange");
    }
// o.ndef
    public void setOriginPhaseCountUsedRange(IntegerRange phaseRange) {
        setProperty("originPhaseCountUsedRange", phaseRange);
    }
    public IntegerRange getOriginPhaseCountUsedRange() {
        return getIntegerRange("originPhaseCountUsedRange");
    }
// o.totalamp
    public void setOriginAmpCountRange(IntegerRange phaseRange) {
        setProperty("originAmpCountRange", phaseRange);
    }
    public IntegerRange getOriginAmpCountRange() {
        return getIntegerRange("originAmpCountRange");
    }

/////  NETMAG table property set, get methods
// n.auth
    public void setMagAuth(String auth) {
        setProperty("magAuth", auth);
    }
    public String getMagAuth() {
        return getProperty("magAuth");
    }
// n.subsource
    public void setMagSubsource(String subsource) {
        setProperty("magSubsource", subsource);
    }
    public String getMagSubsource() {
        return getProperty("magSubsource");
    }
// n.algorithm
    public void setMagAlgo(String algo) {
        setProperty("magAlgo", algo);
    }
    public String getMagAlgo() {
        return getProperty("magAlgo");
    }
// o.quality
    public void setMagQualityRange(DoubleRange qual) {
        setProperty("magQualityRange", qual);
    }
    public DoubleRange getMagQualityRange() {
        return getDoubleRange("magQualityRange");
    }
// n.nsta
    public void setMagStaRange(IntegerRange magStaRange) {
        setProperty("magStaRange", magStaRange);
    }
    public IntegerRange getMagStaRange() {
        return getIntegerRange("magStaRange");
    }
// n.nobs
    public void setMagObsRange(IntegerRange magObsRange) {
        setProperty("magObsRange", magObsRange);
    }
    public IntegerRange getMagObsRange() {
        return getIntegerRange("magObsRange");
    }
// n.magtype
    public void setMagPrefType(String magType) {
        setProperty("magPrefType", magType);
    }
    public String getMagPrefType() {
        return getProperty("magPrefType");
    }
    public void setMagPrefNull(boolean isNull) {
        setProperty("magPrefNull", isNull);
    }
    public boolean getMagPrefNull() {
        return getBoolean("magPrefNull", false);
    }
// n.uncertainty
    public void setMagErrRange(DoubleRange errRange) {
        setProperty("magErrRange", errRange);
    }
    public DoubleRange getMagErrRange() {
        return getDoubleRange("magErrRange");
    }
// n.gap
    public void setMagGapRange(IntegerRange gapRange) {
        setProperty("magGapRange", gapRange);
    }
    public IntegerRange getMagGapRange() {
        return getIntegerRange("magGapRange");
    }
// n.distance
    public void setMagDistRange(DoubleRange distRange) {
        setProperty("magDistRange", distRange);
    }
    public DoubleRange getMagDistRange() {
        return getDoubleRange("magDistRange");
    }
// n.magnitude
    public void setMagValueRange(DoubleRange magRange) {
        setProperty("magValueRange", magRange);
    }
    public DoubleRange getMagValueRange() {
        return getDoubleRange("magValueRange");
    }

///// Origin lat,lon REGION property set, get methods
    public String getRegionType() {
        return getProperty("regionType");
    }
    public void setRegionType(String type) {
        setProperty("regionType", type);
    }

    public boolean isRegionExcluded() {
        return getBoolean("regionExcluded");
    }
    public void setRegionExcluded(boolean tf) {
        setProperty("regionExcluded", tf);
    }

    public LatLonZ getRegionRadiusPoint() {
        return getLatLonZ("regionRadiusPoint");
    }
    public void setRegionRadiusPoint(double lat, double lon) {
        setProperty("regionRadiusPoint", String.valueOf(lat)+" "+String.valueOf(lon));
    }

    public double getRegionRadiusValue() {
        return getDouble("regionRadiusValue");
    }
    public void setRegionRadiusValue(double dist) {
        setProperty("regionRadiusValue", dist);
    }

    public String getRegionBorderPlaceName() {
        return getProperty("regionBorderPlaceName");
    }
    public void setRegionBorderPlaceName(String name) {
        setProperty("regionBorderPlaceName", name);
    }

    public String getRegionRadiusPlaceName() {
        return getProperty("regionRadiusPlaceName");
    }
    public void setRegionRadiusPlaceName(String name) {
        setProperty("regionRadiusPlaceName", name);
    }
    public DoubleRange getRegionBoxLatRange() {
        return getDoubleRange("regionBoxLat");
    }
    public void setRegionBoxLatRange(double minLat, double maxLat) {
        setProperty("regionBoxLat", new DoubleRange(minLat,maxLat));
    }
    public DoubleRange getRegionBoxLonRange() {
        return getDoubleRange("regionBoxLon");
    }
    public void setRegionBoxLonRange(double minLon, double maxLon) {
        setProperty("regionBoxLon", new DoubleRange(minLon,maxLon));
    }

    public static class EventRegion {
        public String name = null;
        public boolean  include = true;
        public boolean  includeNullMag = false;
        public LatLonZ [] polygon = null;
        public DoubleRange magRange = null;
        public DoubleRange depthRange = null;
        EventRegion() {}
        EventRegion(String name) { this.name = name;}
        public String toString() {
            StringBuffer sb = new StringBuffer(512);
            sb.append(name);
            sb.append(" include=").append(include);
            sb.append(" depths=(").append(depthRange.toString());
            sb.append(" mags=(").append(magRange.toString());
            sb.append(" includeNullMag=").append(includeNullMag);
            sb.append(") poly=");
            for (int idx = 0; idx<polygon.length; idx++) {
              sb.append(" ");
              sb.append(polygon[idx].toLatLonString(","));
            }
            return sb.toString();
        }

        public float [] toLatLonArray() {
            if (polygon == null) return null;
            float [] ll = new float[2*polygon.length];
            for (int idx=0; idx < polygon.length; idx++) {
                ll[2*idx] = (float) polygon[idx].getLat();
                ll[2*idx+1] = (float) polygon[idx].getLon();
            }
            return ll;
        }

        public String toPropertyString() {
            StringBuffer sb = new StringBuffer(512);

            sb.append("region.").append(name).append(".include=").append(include).append("\n");

            String str = (depthRange == null) ? "-9. 999." : depthRange.toString();
            sb.append("region.").append(name).append(".orgDepthRange=").append(str).append("\n");

            str = (magRange == null) ? "0. 9." : magRange.toString();
            sb.append("region.").append(name).append(".magValueRange=").append(str).append("\n");
            sb.append("region.").append(name).append(".includeNullMag=").append(includeNullMag).append("\n");

            if (polygon.length > 0) {
              sb.append("region.").append(name).append(".polygon=");
              for (int idx=0; idx < polygon.length; idx++) {
                sb.append(polygon[idx].getLat()).append(" ").append(polygon[idx].getLon()).append(" ");
              }
              sb.append("\n");
            }

            return sb.toString();
        }
    }

    public StringList getRegionNameList() { 
        return getStringList("regionNameList");
    } 

    public void setRegionNameList(String [] names) { 
        setProperty("regionNameList", names);
    } 

    public EventRegion [] getEventRegions() { 
        String [] regionNames = getStringList("regionNameList").toArray();
        if (regionNames == null || regionNames.length == 0) return new EventRegion [0];
        EventRegion [] eRegions = new EventRegion[regionNames.length];
        for (int idx =0; idx < regionNames.length; idx++) {
            eRegions[idx] = getEventRegion(regionNames[idx]);
        }
        return eRegions;
    }

    public EventRegion getEventRegion(String name) { 
        if (name == null || name.trim().length() == 0) return null;

        EventRegion evtRegion = new EventRegion(name);

        evtRegion.polygon = getNamedRegionPolygon(name);
        if (evtRegion.polygon == null) throw new MissingPropertyException("Polygon undefined/malformed for region : " + name);

        DoubleRange dr = getDoubleRange("region."+name+".orgDepthRange");
        if (dr == null) dr = new DoubleRange(new Double(-9.),new Double(999.));
            //throw new MissingPropertyException("Min,Max depth range undefined/malformed for region : " + name);
        evtRegion.depthRange = dr;

        dr = getDoubleRange("region."+name+".magValueRange");
        if (dr == null) dr = new DoubleRange(new Double(0.),new Double(9.));
            //throw new MissingPropertyException("Min,Max magnitude range undefined/malformed for region : " + name);
        evtRegion.magRange = dr;

        evtRegion.include= getBoolean("region."+name+".include", true);
        evtRegion.includeNullMag= getBoolean("region."+name+".includeNullMag", false);

        return evtRegion;
    }

    public LatLonZ [] getRegionPolygon() {
        return getRegionPolygon("regionPolygon");
    }

    public LatLonZ [] getNamedRegionPolygon(String name) {
        if (name == null || name.trim().length() == 0) return null;
        return getRegionPolygon("region."+name+".polygon");
    }

    public void setNamedRegionPolygon(String name, float [] latlon) {
        if (name == null || name.trim().length() == 0) return;
        setProperty("region."+name+".polygon", latlon);
    }

    public void setNamedRegionPolygon(String name, String value) {
        if (name == null || name.trim().length() == 0) return;
        setProperty("region."+name+".polygon", value);
    }

    private LatLonZ [] getRegionPolygon(String name) {
        double [] dd = getDoubleArray(name);
        if (dd == null) return null;
        int pairs = dd.length/2;
        LatLonZ [] llz =  new LatLonZ[pairs];
        int count = 0;
        for (int i = 0; i < pairs; i++) {
            llz[count] = new LatLonZ(dd[2*i],dd[(2*i)+1], 0.);
            count++;
        }
        return llz;
    }

    public void setRegionPolygon(LatLonZ [] llz) {
        if (llz == null) return;
        int pairs = llz.length;
        double [] dd = new double [pairs*2];
        for (int ii = 0; ii < pairs; ii++) {
            dd[2*ii] = llz[ii].getLat();
            dd[(2*ii)+1] = llz[ii].getLon();
        }
        setProperty("regionPolygon", dd);
    }

    public void setNamedRegionOrgDepthRange(String name, DoubleRange dr) {
        setProperty("region."+name+".orgDepthRange", dr);
    }

    public void setNamedRegionMagValueRange(String name, DoubleRange dr) {
        setProperty("region."+name+".magValueRange", dr);
    }

    public void setNamedRegionIncludeNullMag(String name, boolean tf) {
        setProperty("region."+name+".includeNullMag", tf);
    }

    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(64);

        myList.add("catalogTimeMode");
        myList.add("catalogOriginStartTime");
        myList.add("catalogOriginEndTime");
        myList.add("catalogRelativeTimeValue");
        myList.add("catalogRelativeTimeUnits");
        myList.add("catalogRelativeTimeEvid");
        myList.add("catalogMaxRows");

        myList.add("eventAuth");
        myList.add("eventSubsource");
        myList.add("eventValidFlag");
        myList.add("eventTypesIncluded");
        myList.add("eventTypesExcluded");

        myList.add("originProcTypesIncluded");
        myList.add("originProcTypesExcluded");
        myList.add("originDummyFlag");
        myList.add("originAuth");
        myList.add("originSubsource");
        myList.add("originType");
        myList.add("originAlgo");
        myList.add("originQualityRange");
        myList.add("originGapRange");
        myList.add("originDistRange");
        myList.add("originSPhasesRange");
        myList.add("originFirstMoRange");
        myList.add("originFixDepthFlag");
        myList.add("originFixEpiFlag");
        myList.add("originDepthRange");
        myList.add("originRmsRange");
        myList.add("originErrHorizRange");
        myList.add("originErrDepthRange");
        myList.add("originPhaseCountRange");
        myList.add("originPhaseCountUsedRange");
        myList.add("originAmpCountRange");

        myList.add("magAuth");
        myList.add("magSubsource");
        myList.add("magAlgo");
        myList.add("magQualityRange");
        myList.add("magStaRange");
        myList.add("magObsRange");
        myList.add("magPrefNull");
        myList.add("magPrefType");
        myList.add("magErrRange");
        myList.add("magGapRange");
        myList.add("magDistRange");
        myList.add("magValueRange");

        myList.add("regionType");
        myList.add("regionExcluded");
        myList.add("regionNameList");
        myList.add("regionRadiusPoint");
        myList.add("regionRadiusValue");
        myList.add("regionBorderPlaceName");
        myList.add("regionRadiusPlaceName");
        myList.add("regionBoxLat");
        myList.add("regionBoxLon");
        myList.add("regionPolygon");
        myList.add("region.default.orgDepthRange");
        myList.add("region.default.magValueRange");
        myList.add("region.default.includeNullMag");
        myList.add("enableRegionCreation");

        Collections.sort(myList);

        return myList;
    }

    public List getUnknownPropertyNames() {
        Enumeration e = propertyNames();
        List known = getKnownPropertyNames();
        List unknown = new ArrayList(known.size());
        String name = null;
        while ( e.hasMoreElements() ) {
          name = (String) e.nextElement();
          if ( ! known.contains(name) && !name.startsWith("region.")) unknown.add(name);
        }
        return unknown;
    }

/*
  static public final class Tester {
    public static void main(String args[]) {
      if (args.length < 2) {
          System.out.println("Syntax: userAppSubdirName  userPropFileName(with event+dbconn props)");
          System.exit(0);
      }
      // Private properties
      EventSelectionProperties props = new EventSelectionProperties();
      System.out.println("Known property names:");
      System.out.println(props.getKnownPropertyNames());
      System.out.println();
      System.out.println("catalogTimeMode is known name? : " + props.isKnownPropertyName("catalogTimeMode"));
      System.out.println("ratCrap is known name? : " + props.isKnownPropertyName("ratCrap"));

      //props.setFiletype(args[0]); // test file found in the user's home "jiggle" subdir
      //props.setFilename(args[1]); // name of event selection props file
      //props.reset(); // read the props from file
      //props.setup(); // configure local environment from props
      //below does above actions
      boolean status = props.initialize(args[0], args[1], null);
      if (!status) {
        System.err.println("EventSelectionProperties initialize failed, check input: "+args[0]+" "+args[1]);
        System.exit(0);
      }

      System.out.println("-- Event Selection Properties --");
      System.out.println("File name = "+ props.getUserPropertiesFileName());

      props.dumpProperties();

      System.out.println("-----------");

      // examples of local 'get' methods
      System.out.println("radiusValue  = " + props.getFloat("regionRadiusValue") );
      System.out.println("catalogOriginStartTime   = " + props.getDateTime("catalogOriginStartTime").toString() );
      System.out.println("catalogTimeMode = " + props.getDateTime("catalogTimeMode").toString() );
      //System.out.println("endTime(sec)= " + props.getEndDateTime().toString() );
      // not TimeSpan.toString() gives LOCAL time!
      System.out.println("time span   = " + props.getTimeSpan().toString() );


      DoubleRange mr = props.getDoubleRange("magValueRange");
      System.out.println("magValueRange = " + ((mr==null)? "NULL" : (mr.getMinValue()+"  "+mr.getMaxValue())) );

      IntegerRange nph = props.getIntegerRange("originPhaseCountRange");
      System.out.println("originPhaseCountRange = " + ((nph==null) ? "NULL" : nph.getMinValue()+"  "+nph.getMaxValue()) );

      //props.saveProperties();

      // Test SQL values
      if (! props.hasValidTimeSpan()) {
          System.err.println("EventSelectionProperties has an invalid time span, check input!");
          System.exit(0);
      }

      DbaseConnectionDescription dbDesc = new DbaseConnectionDescription();
      dbDesc.parseFromProperties(props);
      DataSource ds = new DataSource(dbDesc); // TestDataSource.create("serverma");
      JasiDatabasePropertyList.debugSQL = true; // allows output dump of Solution SQL query
      Collection c = Solution.create().getByProperties(props);
      ds.close();
      if (c != null) {
          System.out.println("Solutions matching properties: " + c.size());
          GenericSolutionList solList = new GenericSolutionList(c);
          System.out.println(solList.dumpToString());
      } else {
          System.out.println("No solutions match properties!");
      }


    }  // end of main
  } // end of Tester
*/
} // end of EventSelectionProperties


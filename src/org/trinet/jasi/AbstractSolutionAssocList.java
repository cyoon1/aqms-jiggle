//NOTE: for speed, only use identifier equivalence checking? addOrReplaceAll(List)?
//NOTE: associate vs. assign, do we check list contains element or not?
package org.trinet.jasi;
import java.util.*;
public abstract class AbstractSolutionAssocList extends AbstractCommitableList implements SolutionAssociatedListIF {

  protected AbstractSolutionAssocList() { super(); }
  protected AbstractSolutionAssocList(int capacity) { super(capacity); }
  protected AbstractSolutionAssocList(Collection collection) { super(collection); }

  /** Commit all elements associated with non-null input Solution to underlying DataSource.
   *  Returns true if input is null
   * */
  public boolean commit(Solution sol) {
      return (sol == null) ? true : getAssociatedWith(sol).commit();
  }

  /** Return a list of those elements associated with the input
    * Solution. Virtually deleted elements are not returned.
    * Null input returns empty list.
  */
  public SolutionAssociatedListIF getAssociatedWith(Solution sol) {
    return getAssociatedWith(sol, true);
  }

  /**
    * Return a list containing the elements associated with input Solution.
    * undeletedOnly == true, does not return the elements flagged virtually deleted.
    * Null input returns empty list.
   */
  public SolutionAssociatedListIF getAssociatedWith(Solution sol, boolean undeletedOnly) {
      SolutionAssociatedListIF newList = (SolutionAssociatedListIF) newInstance();
      if (sol == null || newList == null) return newList;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
          JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
          if (jc.isAssignedTo(sol) ) {
            if (undeletedOnly && jc.isDeleted()) continue;
            else newList.add(jc);
          }
      }
      return newList;
  }
  public SolutionAssociatedListIF getUnassociated() {
    return getUnassociated(true);
  }
  public SolutionAssociatedListIF getUnassociated(boolean undeletedOnly) {
      SolutionAssociatedListIF newList = (SolutionAssociatedListIF) newInstance();
      if (newList == null) return newList;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
          JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
          if (! jc.isAssignedToSol()) {
            if (undeletedOnly && jc.isDeleted()) continue;
            else newList.add(jc);
          }
      }
      return newList;
  }
  /** Associates all elements in the list with input Solution.
   * Implementations usually append elements to Solution collections.
   * Null input is a no-op.
   * */
  public void associateAllWith(Solution sol) {
    if (sol == null) return;
    int mySize = size();
    for (int i = 0; i<mySize; i++) {
      ((JasiSolutionAssociationIF)get(i)).associate(sol);
    }
  }
  /**
   * Assigns input Solution to elements in list. 
   * Unlike associate...() doesn't add elements to
   * input Solution collections.
   * Throws an exception if any element is already
   * associated with a different non-null Solution.
   * @see #unassociateAll().
   */
  public void assignAllTo(Solution sol) {
    int mySize = size();
    for (int i = 0; i<mySize; i++) {
      ((JasiSolutionAssociationIF)get(i)).assign(sol);
    }
  }
  /**
   * Assigns input Solution to elements in list. 
   * Unlike associate...() doesn't add elements to
   * input Solution collections.
   * Throws an exception if any element is already
   * associated with a different non-null Solution.
   */
  public static final void assignListTo(List jcList, Solution sol) {
    int count = jcList.size();
    for (int i = 0; i<count; i++) {
      ((JasiSolutionAssociationIF)jcList.get(i)).assign(sol);
    }
  }
  /** Unassociate all elements in the list */
  public void unassociateAll() {
    for (int i = size()-1; i >= 0; i--) {
      ((JasiSolutionAssociationIF) get(i)).unassociate();
    }
  }

  /** Unassociate of all elements in list associated with input solution.*/
  public void unassociateAllFrom(Solution sol) {
    if (sol == null) return;
    for (int i = size()-1; i >= 0; i--) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      if (jc.isAssignedTo(sol)) jc.unassociate();
    }
  }

  /**
  * Return list of undeleted elements of the input type associated with the
  * input solution. If Solution input is null, returns all matching input type.
  * Returns empty list, if input type is null.
  */
  public SolutionAssociatedListIF getListByType(Solution sol, Object type) {
      SolutionAssociatedListIF newList = (SolutionAssociatedListIF) newInstance();
      if (type == null || newList == null) return newList;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
        JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
        if (sol == null || (jc.isAssignedTo(sol) && ! jc.isDeleted())) { 
          if (jc.isSameType(type)) newList.add(jc);
        }
      }
      return newList;
  }
  /**
  * Virtually delete all elements associated with input Solution.
  * Does not remove elements from list.
  * Returns the number of elements virtually deleted.
  * Null input returns 0;
  */
  public int deleteAll(Solution sol) {
    if (sol == null) return 0;
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      //if (jc.isAssignedTo(sol) && jc.setDeleteFlag(true)) knt++;
      //Replaced above to fire "stateChanged" deleted event:
      if (jc.isAssignedTo(sol) &&
          setElementDeleteState(i, true)) knt++;
    }
    return knt;
  }
  /**
  * Virtually delete all elements not associated with any Solution.
  * Does not remove elements from list.
  * Returns the number of elements virtually deleted.
  */
  public int deleteUnassociated() {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      //if (! jc.isAssignedToSol() && jc.setDeleteFlag(true)) knt++;
      if (! jc.isAssignedToSol() &&
          setElementDeleteState(i, true)) knt++;
    }
    return knt;
  }
  /** Remove all elements from list associated with the input Solution.
  *   Null input returns 0;
   */
  public int removeAll(Solution sol) {
    if (sol == null) return 0;
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      if (jc.isAssignedTo(sol)) {
       remove(i);
       knt++;
      }
    }
    return knt;
  }
  /** Remove all elements from list not associated with any Solution.  */
  public int removeUnassociated() {
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      if (! jc.isAssignedToSol()) {
       remove(i);
       knt++;
      }
    }
    return knt;
  }
  /** Convenience wrapper combining remove and delete of all elements
   * in list not associated with any Solution.  */
  public int eraseUnassociated() {
    int knt = 0;
    for (int i = size()-1; i >=0 ; i--) {
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) get(i);
      if (! jc.isAssignedToSol()) {
       remove(i);
       //jc.setDeleteFlag(true);
       setElementDeleteState(i, true);
       knt++;
      }
    }
    return knt;
  }

  /**
  * Virtually delete all elements whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int stripByResidual(double value) {
    return stripByResidual(value, null);
  }
  /**
  * Virtually delete all elements associated with input Solution
  * whose residual >= value. If input Solution is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int stripByResidual(double value, Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      JasiAssociationFitIF jc = (JasiAssociationFitIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        if ((Math.abs(jc.getResidual()) >= value)) {
          if ( (jc instanceof Phase) && ((Phase)jc).isFinal() ) continue; // don't remove required? -aww 2013/02/15
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }
  /**
  * Virtually delete all elements whose distance > value (km). 
  * Regardless of association.
  * Does not remove them from list.
  */
  public int stripByDistance(double value) {
    return stripByDistance(value, null);
  }
  /**
  * Virtually delete all elements associated with input Solution
  * whose distance > value (km). If input Solution is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int stripByDistance(double value, Solution sol) {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiAssociationFitIF jc = (JasiAssociationFitIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        //if (jc.getDistance() >= value) { // no don't use slant aww 06/11/2004
        if (jc.getHorizontalDistance() >= value) { // aww 06/11/2004
          if ( (jc instanceof Phase) && ((Phase)jc).isFinal() ) continue; // don't remove required? -aww 2013/02/15
          //jc.setDeleteFlag(true);
          setElementDeleteState(i, true);
          knt++;
        }
      }
    }
    return knt;
  }

} 

package org.trinet.jasi;
import java.util.EventObject;
public class CacheRefreshEvent extends java.util.EventObject {
    public static final int FAILURE = 0;
    public static final int SUCCESS = 1;

    private int type = -1;

    public CacheRefreshEvent(Object source, int type) {
        super(source);
        this.type = type;
    } 

    public boolean success() {
        return (type == SUCCESS);
    }

    public boolean failure() {
        return (type == FAILURE);
    }

    public int getType() {
        return type;
    }

    public String toString() {
       return super.toString() + " type : " + type; 
    }
}

package org.trinet.jasi;

import java.util.Collection;
import java.util.*;
import org.trinet.util.*;
/**
 * ChannelTimeModel that selects all channels with phase picks for the given
 * solution and constructs time windows that should include the P and S waves.
 * */
public class CTMampsOnly extends ChannelTimeWindowModel {

    protected String defModelName = "Amps Only";

    /** A string with an brief explanation of the model. For help and tooltips. */
    protected String defExplanation =
                   "channels associated with amplitudes";

    public CTMampsOnly( ) {
        super ();
        initModel();
    }
    public CTMampsOnly(Solution sol, ChannelableList channelSet ) {
        super (sol, channelSet);
        initModel();
    }
    public CTMampsOnly(ChannelList inputChannelSet ) {
        super(inputChannelSet);
        initModel();
    }
    public CTMampsOnly(Solution sol ) {
        super(sol);
        initModel();
    }

    protected void initModel () {
        setModelName(defModelName);
        setExplanation(defExplanation);
    }
    /** Return a List of ChannelTimeWindow objects, one for each channel represented
     *  by the objects in the ChannelableList. These may be amps, picks, etc.
     *  For example. to use this to create a list of waveforms to match a list of
     *  picks you might use: <tt>
     *   getWaveformList(sol.getPhaseList());
     * </tt>
     * The Waveforms will not contain time-series yet. To load time-series you must set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object)
     * and then invoke the loadTimeSeries() method of each Waveform instance.<p>
     *
     * @see: AbstractWaveform
     */
    public ChannelableList getChannelTimeWindowList(ChannelableListIF list) {

        if (list == null || list.isEmpty()) return null;

        ChannelableList newList = new ChannelableList();
        TimeSpan ts;
        Channel chan;
        double dist;

        Channelable channelable[] = new Channelable[list.size()];

        channelable = (Channelable[]) list.toArray(channelable);

        for (int i=0; i< channelable.length; i++) {

            chan = channelable[i].getChannelObj();
            //dist = channelable[i].getDistance(); // don't use slant aww 06/11/2004
            dist = channelable[i].getHorizontalDistance();
            //if (!includeChannel(chanList[i])) continue;  // skip?
            ts = getTimeWindow(dist); // horizontal dist 

            ChannelTimeWindow ctw = new ChannelTimeWindow(chan, ts);
            newList.add(ctw);
        }

        return newList;
    }

     /** Return a Collection with a ChannelTimeWindow objects for each amp reading.
     * The will not contain time-series yet. To load time-series you must set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object)
     * and then invoke the loadTimeSeries() method of each Waveform instance.<p>
     *
     * This model needs a solution with amps to determine what channels
     * to select. Returns null if Solution is not set or if Solution has no amps.<p>
     */
     // ChannelableList
    public ChannelableList getChannelTimeWindowList() {
        if (sol == null) return null;
            if (sol.getAmpList().isEmpty()) {
              //Amplitude.create().getBySolution(sol)); // does the association
              sol.loadAmpList(); // does the association too
            }
        return getChannelTimeWindowList(sol.getAmpList());
    }

/*
    public static final class Tester {
      public static void main (String args[]) {
        if (args.length <= 0) {

          System.out.println ("Usage: ChannelTimeWindowModel [evid] ");
          System.exit(0);

        }

        // event ID
        Long val = Long.valueOf(args[0]);
        long evid = (long) val.longValue();

//        DataSource db = new DataSource();
        DataSource db = TestDataSource.create();    // make connection
        System.out.println (db.toString());

        Solution sol    = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println ("No such event: "+evid);
            System.exit(0);
        }

//        sol.add((List) Phase.create().getBySolution(sol) );
        System.out.println (sol.toString());

        System.out.println ("Reading in current channel info...");

        if (false) {
          String compList[] = {"EH_", "HH_", "HL_", "AS_"};
            ChannelList chanList = ChannelList.getByComponent(compList);

          //ChannelList chanList = ChannelList.smartLoad();
          System.out.println ("Read "+chanList.size()+" channels.");
        }

        CTMampsOnly model = new CTMampsOnly(sol);

        ArrayList wfList = (ArrayList) model.getWaveformList();

        if (wfList == null) {
          System.out.println ("No channels in list");
        } else {
          System.out.println ("Channel count = "+wfList.size());
        }

        // test loading an event from this model
        org.trinet.jiggle.MasterView mv = new org.trinet.jiggle.MasterView();
        WaveClient waveClient = null;

        mv.defineByChannelTimeWindowModel(model);
        System.out.println (mv.toString());
      }
    } // end of Tester
*/
}

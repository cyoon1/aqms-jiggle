package org.trinet.jasi;

/** Static wrapper around a ChannelList instance that can be shared
 * by modules without passing a ChannelList reference.
* @see: ChannelList
*/
public class MasterChannelList {

    // Set below flag true to add only those channels found in DataSource
    // when matching a ChannelableList to masterList:
    private static boolean addChannelsOnlyFromDataSource = true; // aww 02/23/2005 used to be false

    private static ChannelList masterList = null;

    private MasterChannelList() { }

    /** Set the shared ChannelList. */
    public static void set(ChannelList list) {
        masterList = list;
    }
    /** Set the shared ChannelList. */
    public static void set(ChannelableList list) {
        masterList = new ChannelList(list);
    }
    /** Return the shared ChannelList. */
    public static ChannelList get() {
        return masterList;
    }

    /** Returns 'true' if this list is null or empty. */
    public static boolean isEmpty() {
        return (masterList == null) ? true : masterList.isEmpty();
    }

    public void addAll(ChannelableList list) {
        // Extract Channel objects from list elements - aww 03/04/2005
        if ( list != null && ! list.isEmpty() ) {
          Channelable [] ch = list.getChannelableArray();
          for (int idx = 0; idx < ch.length; idx++) {
              masterList.add(ch[idx].getChannelObj(), true);
          }
        }
    }

    /*
    protected static int percentDone;
    public static int getPercentDone() {
        return percentDone;
    }
    */

    /** Set true to only add Channels to ChannelList when found in DataSource
     * lookup for matchChannelWithDataSource methods.
     * Default is true to recover all missing data for GUI application use.
     * Bulk state processing would be more efficient when input is false
     * to avoid repeated queries for missing data, however, if value is set false,
     * application should NOT save the its master list to a cache file for reuse
     * otherwise channels with missing LatLonZ, gain, or corrections would be saved.
     */
    public static void setAddChannelsOnlyFromDataSource(boolean tf) {
        addChannelsOnlyFromDataSource = tf;
    }
    public static boolean isAddChannelsOnlyFromDataSource() {
        return addChannelsOnlyFromDataSource;
    }
    /**
    * Looks up Channel data values for each Channel in the input chList using
    * the static MasterChannelList list data if it's already loaded, otherwise it
    * creates a new list of Channels that were active on the specified input date
    * and as sets this new list as the static MasterChannelList data member list.
    * If a Channel is not found on in the list it looks it up in the DataSource
    * for the specified date, and if found it adds it to the static MasterChannelList
    * data member list.<p>
    * Returns 'false' if any channels were missing in DataSource.
    */
    public static boolean matchChannelsWithDataSource(ChannelableListIF chList, java.util.Date date) {

      return matchChannelsWithDataSource(chList, date, 100);
    }
    /**
    * Looks up Channel data values for each Channel in the input chList using
    * the static MasterChannelList list data if it's already loaded, otherwise it
    * creates a new list of Channels that were active on the specified input date
    * and as sets this new list as the static MasterChannelList data member list.
    * If a Channel is not found on in the list it looks it up in the DataSource
    * for the specified date, and if found it adds it to the static MasterChannelList
    * data member list.<p>
    *
    * CrosOverValue is the efficency Crossover; if more then this many channels need
    * looking up, load them all. <p>
    *
    * Returns 'false' if any channels were missing in DataSource.
    */
      public static boolean matchChannelsWithDataSource(ChannelableListIF chList,
          java.util.Date date,
          int crossOverValue) {

        // if more then this many channels, load them all
         int efficencyCrossover = crossOverValue;
         //
         // bail if input list is empty
         int count = chList.size();
         if (count == 0) return true; // nothing to match

         // its more efficient to load the whole list if there are more
         // than 'efficiencyCrossver' channels.
         if (isEmpty() && count > efficencyCrossover) {
           set( ChannelList.readList(date) );
         }
         if (isEmpty()) {
           set( new ChannelList() );
         }

         Channel ch = null;
         Channel lookupChan = null;

         //percentDone = 0;
         //int inc = 100/count;
         boolean status = true;
         // Match input Channels of input list with those of the masterList
         // boolean inList = false;  // debug
         for (int i = 0; i < count; i++) {
           ch = ((Channelable) chList.get(i)).getChannelObj();
           // inList  = ch.setChannelObjFromList(masterList, date); // debug
           // System.err.println("DEBUG MasterChanelList inList: " + inList+ " " + ch.toDelimitedSeedNameString("_"));
           // if ( ! inList ) {
           if ( ! ch.setChannelObjFromList(masterList, date) ) { // not in my list
             // Try individual channel lookup for channel not in masterList
             lookupChan = ch.lookUp(ch, date);
             if (lookupChan == null) {
               status = false; // error? channel lookup doesn't return null
             }
             else { // either have original or new channel returned
               if (lookupChan != ch) { // new => found in datasource
                   // set channel of input and add it to masterList
                   ch.setChannelObjData(lookupChan); // -aww 04/07/2005
                   masterList.add(lookupChan);
               }
               else { // lookup returned original channel of input chList element
                 status = false; // channel was not found in DataSource
                 // static add... flag is false, so set/add every
                 // input list Channel to the masterList
                 // to avoid repeated queries for missing data -aww
                 if ( ! addChannelsOnlyFromDataSource) {
                   //ch.setChannelObjData(lookupChan); // don't need, it's a no-op -aww 04/07/2005
                   masterList.add(lookupChan);
                 }
               }
             }
           }
           // else channel was found in masterList
           //percentDone += inc;
         }
         return status; // false if any channels were missing in DataSource
    }
    /** Trys reading the local cached channel list file to set its master list,
     * if that fails reads currently active channels from the data source,
     * then creates the master channel list cache file.
     * Returns a reference to the loaded ChannelList.
     * */
    public static ChannelList smartLoad() {
      masterList = ChannelList.smartLoad();
      return masterList;
    }
    /** Trys reading the local cached channel list file to set its master list,
     * if that fails reads channels active on the input date from the data source,
     * then creates the master channel list cache file.
     * Returns a reference to the loaded ChannelList.
     * */
    public static ChannelList smartLoad(java.util.Date date) {
      masterList = ChannelList.smartLoad(date);
      return masterList;
    }

    public static void readOnlyChannelLatLonZ() {
      ChannelList.readOnlyChannelLatLonZ();
    }

    // main for testing
    /*
    public static final class Tester {
      public static void main(String[] args) {
            TestDataSource.create();
            System.out.println("Reading channel list.");
            ChannelList aList = MasterChannelList.smartLoad();
            System.out.println("Total channels = "+  MasterChannelList.get().size());
            aList.get().dump();
      }
    } // end of Tester
   */
}

package org.trinet.jasi;
import java.util.*;
import javax.swing.event.*;
import org.trinet.util.*;
public interface ActiveArrayListIF extends java.util.List { 
    //methods inherited from List interface:
    //public boolean addAll(Collection newList);
    //public boolean addAll(int index, Collection newList);
    //public boolean add(Object obj);
    //public void add(int index, Object obj);
    //public void clear();
    //public boolean remove(Object obj);
    //public boolean removeAll(Collection col);
    //public Object set(int index, Object obj);
    
    public void clear(boolean notify);
    public Object set(int index, Object newObj, boolean notify);
    public boolean add(Object obj, boolean notify);
    public boolean addAll(Collection list, boolean notify);
    public int removeAll(List aList);
    public Object remove(int index, boolean notify);
    public boolean remove(Object obj, boolean notify);
    public void removeRange(int from, int to);
    public boolean delete(Object obj);

    public void addListDataStateListener(ListDataStateListener l);
    public void removeListDataStateListener(ListDataStateListener l);
    public void clearListDataStateListeners();
    public int countListDataStateListeners();
    public ListDataStateListener [] getListDataStateListeners();
    public void fireListElementStateChanged(String stateName, int start, int end);
    public void sort(Comparator c);
}

package org.trinet.jasi;

/**
 * Data encoding formats descriptions. See SEED v2.3, Ref. Man. pg. 106
 * @author Doug Given
 * @version
 *
 * <tt>

CODES 0-9 GENERAL
    0 ASCII text, byte order as specified in field 4
    1 16 bit integers
    2 24 bit integers
    3 32 bit integers
    4 IEEE floating point
    5 IEEE double precision floating point

CODES 10 - 29 FDSN Networks
    10 STEIM (1) Compression
    11 STEIM (2) Compression
    12 GEOSCOPE Multiplexed Format 24 bit integer
    13 GEOSCOPE Multiplexed Format 16 bit gain ranged, 3 bitexponent
    14 GEOSCOPE Multiplexed Format 16 bit gain ranged, 4 bit exponent
    15 US National Network compression
    16 CDSN 16 bit gain ranged
    17 Graefenberg 16 bit gain ranged
    18 IPG - Strasbourg 16 bit gain ranged
    19 STEIM (3) Compression

CODES 30 - 39 OLDER NETWORKS
    30 SRO Format
    31 HGLP Format
    32 DWWSSN Gain Ranged Format
    33 RSTN 16 bit gain ranged
 * <\tt>
 */

public class SeedEncodingFormat {

// General types
    public static final byte INTEGER16       = 1;
    public static final byte INTEGER24       = 2;
    public static final byte INTEGER32       = 3;
    public static final byte IEEEFLOAT       = 4;
    public static final byte IEEEDOUBLE      = 5;

// FDSN types
    public static final byte STEIM1          = 10;
    public static final byte STEIM2          = 11;
    public static final byte GEOSCOPE24      = 12;
    public static final byte GEOSCOPE16_3    = 13;
    public static final byte GEOSCOPE16_4    = 14;
    public static final byte USNSN           = 15;
    public static final byte CDSN            = 16;
    public static final byte GRAEFENBERG     = 17;
    public static final byte IGP             = 18;

// Old types
    public static final byte SRO             = 30;
    public static final byte HGLP            = 31;
    public static final byte DWWSSN          = 32;
    public static final byte RSTN            = 33;


    public static final String UNDEF_STRING  = "undefined";
    protected static final String TYPE [] = {
    UNDEF_STRING,
    "INTEGER16",           // 1;
    "INTEGER24",           // 2;
    "INTEGER32",           // 3;
    "IEEEFLOAT",           // 4;
    "IEEEDOUBLE",          // 5;
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    "STEIM1",              // 10;
    "STEIM2",              // 11;
    "GEOSCOPE24",          // 12;
    "GEOSCOPE16_3",        // 13;
    "GEOSCOPE16_4",        // 14;
    "USNSN          ",     // 15;
    "CDSN          ",      // 16;
    "GRAEFENBERG",         // 17;
    "IGP          ",       // 18;
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    UNDEF_STRING,
    "SRO",                 // 30;
    "HGLP",                // 31;
    "DWWSSN",              // 32;
    "RSTN",                // 33;
    };

/** Return a string describing the given data encoding type.
 *  Returns UNDEF_STRING if type is invalid. */
    public static String getTypeString(int code) {
      if (isValid(code)) {
        return TYPE[code];
      } else {
        return UNDEF_STRING;
      }
    }
/** Return the value of the given data encoding type.
 *  Returns -1 if type is invalid. */
    public static int getTypeValue (String string) {

      for (int i = 0; i< TYPE.length; i++) {
  if (TYPE[i].equalsIgnoreCase(string)) return i;
      }
      return -1;
    }

  public static boolean isValid(int code) {
    return (code >= 0 && code < TYPE.length);
  }

 /** Return the data size in bytes for each encoding format.
  *  See Seed manual pg. 106 */
    public static int getBytesInFormat(int code) {
           switch (code) {
       case 1:
      return 2;  // 16 bit int
       case 2:
      return 3;  // 24 bit int
       case 3:
      return 4;  // 32 bit int
       case 4:
      return 4;  // IEEE float
       case 5:
      return 8;  // IEEE double
       case 10:
      return 4;  // SCECDC (?) emperical, not sure why this value
       default:
      return 4;  // guess
           }
    }
}


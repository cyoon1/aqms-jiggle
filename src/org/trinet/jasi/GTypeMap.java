package org.trinet.jasi;

/**
 * Map database schema event types to jasi event type name labels and vice versa.
 */
public abstract class GTypeMap extends JasiObject {

    // db
    public static final String N           = "";
    public static final String L           = "l";
    public static final String R           = "r";
    public static final String T           = "t";

    // jasi
    public static final String LOCAL       = "local";
    public static final String REGIONAL    = "regional";
    public static final String TELESEISM   = "teleseism";
    public static final String NULL        = "null";

    protected static GType[] gtypes = new GType [] {
        new GType(L, LOCAL),new GType(R, REGIONAL),new GType(T, TELESEISM),new GType(N, NULL)
    };

    protected static String jasiDefault = NULL;
    protected static String dbDefault = N;

    /** Return the default event type database String. */
    public static String getDefaultDbType() {
        return dbDefault;
    }

    /** Return the default event type jasi String. */
    public static String getDefaultJasiType() {
        return jasiDefault;
    }

    /** Returns true if the input String is a valid jasi event type. This is case insensitive.
    */
    public static boolean isValidJasiType(String jasiType) {
        if (gtypes == null || jasiType == null) return false;
        for (int i = 0; i < gtypes.length; i++) {
            if (jasiType.equalsIgnoreCase(gtypes[i].name)) return true;
        }
        return false;
    }
    public static boolean isValidDbType(String dbType) {
        if (gtypes == null || dbType == null) return false;
        for (int i = 0; i < gtypes.length; i++) {
            if (dbType.equalsIgnoreCase(gtypes[i].code)) return true;
        }
        return false;
    }


    /** Returns true if the input is a valid event type. This is case insensitive.*/
    public static boolean isValidIndex(int idx) {
        return (idx > -1 && (gtypes != null && idx < gtypes.length)) ? true : false;
    }

    /** Given a jasi event type return the corresponding TriNet schema type. */
    public static String toDbType(String jasiType) {
        if (jasiType == null) return dbDefault;
        int count = (gtypes == null) ? 0 : gtypes.length;
        for (int i = 0; i < count; i++) {
            if (jasiType.equalsIgnoreCase(gtypes[i].name)) return gtypes[i].code;
        }
        return dbDefault;
    }

    /** Given a database schema type return the corresponding jasi event type. */
    public static String fromDbType(String dbType) {
        if (dbType == null) return jasiDefault;
        int count = (gtypes == null) ? 0 : gtypes.length;
        for (int i = 0; i < count; i++) {
            if (dbType.equalsIgnoreCase(gtypes[i].code)) return gtypes[i].name;
        }
        return jasiDefault;
    }

    /** Returns index of matching gtypes in gtypes array. Returns -1 if not valid.*/
    public static int getIndexOfDbType(String dbType) {
        if (dbType == null) return -1;
        int count = (gtypes == null) ? 0 : gtypes.length;
        for (int i = 0; i < count; i++) {
            if (dbType.equalsIgnoreCase(gtypes[i].code)) return i;
        }
        return -1;
    }

    /** Returns index of matching gtypes in gtypes array. Returns -1 if not valid.*/
    public static int getIndexOfJasiType(String jasiType) {
        if (jasiType == null) return -1;
        int count = (gtypes == null) ? 0 : gtypes.length;
        for (int i = 0; i < count; i++) {
            if (jasiType.equalsIgnoreCase(gtypes[i].name)) return i;
        }
        return -1;
    }

    /** Return the gtypes at index offset. If not a valid value, returns 'none'.*/
    public static GType getGTypeByIndex(int idx) {
        return (isValidIndex(idx)) ? gtypes[idx] : null;
    }

    public static GType getGTypeByDbType(String dbType) {
        if (gtypes == null || dbType == null) return null;
        for (int ii = 0; ii < gtypes.length; ii++) {
            if (gtypes[ii].code.equals(dbType)) return gtypes[ii];
        }
        return null;
    }

    public static GType getGTypeByJasiType(String jasiType) {
        if (gtypes == null || jasiType == null) return null;
        for (int ii = 0; ii < gtypes.length; ii++) {
            if (gtypes[ii].name.equals(jasiType)) return gtypes[ii];
        }
        return null;
    }

    public static GType [] getGTypes() {
        return gtypes;
    }

    public static String [] getDbTypeArray() {
        if (gtypes == null) return new String[0];
        String [] types = new String[gtypes.length];
        for (int ii = 0; ii < gtypes.length; ii++) {
            types[ii] = gtypes[ii].code;
        }
        return types;
    }

    public static String [] getJasiTypeArray() {
        if (gtypes == null) return new String[0];
        String [] types = new String[gtypes.length];
        for (int ii = 0; ii < gtypes.length; ii++) {
            types[ii] = gtypes[ii].name;
        }
        return types;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(1024);
        for (int idx = 0; idx < gtypes.length; idx++) {
            if (gtypes != null) sb.append(gtypes[idx].toString()+"\n");
        }
        return sb.toString();
    }

} // GTypesMap

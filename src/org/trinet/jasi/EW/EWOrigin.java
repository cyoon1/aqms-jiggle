
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// Individual SQL Statements
import org.trinet.jasi.EW.SQL.GetOriginStatement;
import org.trinet.jasi.EW.SQL.GetOriginListStatement;
import org.trinet.jasi.EW.SQL.CreateOriginStatement;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/11
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWOrigin
{


  // *** CLASS ATTRIBUTES *** \
  public int Retcode; // Description
  public long idOrigin; // Description
  public String xidExternal; // Description
  public String sHumanReadable; // Description
  public String sSource; // Description
  public double tOrigin; // Description
  public float dLat; // Description
  public float dLon; // Description
  public float dDepth; // Description
  public int iGap; // Description
  public float dDmin; // Description
  public float dRMS; // Description
  public int iAssocRd; // Description
  public int iAssocPh; // Description
  public int iUsedRd; // Description
  public int iUsedPh; // Description
  public int iE0Azm; // Description
  public int iE0Dip; // Description
  public float dE0; // Description
  public int iE1Azm; // Description
  public int iE1Dip; // Description
  public float dE1; // Description
  public int iE2Azm; // Description
  public int iE2Dip; // Description
  public float dE2; // Description
  public float dErLat; // Description
  public float dErLon; // Description
  public float dErZ; // Description
  public double tMCI; // Description
  public int iFixedDepth; // Description
  public String sComment; // Description

  public long idEvent; // Description

  public int bBindToEvent; // Description
  public int bSetPrefer; // Description
  public String sExternal_Table_Name; // Description

	// Preferred Magnitude
	EWMagnitude ewMagnitude;

	// Magnitude List
	Vector      vMagList;


  // *** CLASS SQL STATEMENTS *** \
  static EWSQLStatement es1;
  static EWSQLStatement es2;
  static EWSQLStatement es3;


  // *** CONSTRUCTORS *** \
  public EWOrigin()
  {
    if(es1 == null)
      es1 = new GetOriginStatement(org.trinet.jasi.DataSource.getConnection());
    if(es2 == null)
      es2 = new GetOriginListStatement(org.trinet.jasi.DataSource.getConnection());
    if(es3 == null)
      es3 = new CreateOriginStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
  }


  public EWOrigin Read()
  {

    if(es1.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWOrigin:Read()


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
     int    iRetCode;

    es2.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWOrigin:ReadList()


  public EWOrigin Write()
  {

    if(es3.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWOrigin:Write()


}  // end class EWOrigin

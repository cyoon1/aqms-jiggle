// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWCoda;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/08/28                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CreateTCodaStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CreateTCodaStatement()                          
  {                                    
    sSQLStatement = new String("Begin Create_TCoda(OUT_idTCoda => :OUT_idTCoda,IN_sExternalTableName => :IN_sExternalTableName,IN_xidExternal => :IN_xidExternal,IN_idChan => :IN_idChan,IN_tCodaTermObs => :IN_tCodaTermObs,IN_tCodaTermXtp => :IN_tCodaTermXtp); End;");
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CreateTCodaStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWCoda objEWCoda = (EWCoda)obj;              
                                     
    try                              
    {                                
      cs.setString(2, objEWCoda.sExternalTableName);    
      cs.setString(3, objEWCoda.xidExternal);    
      cs.setLong(4, objEWCoda.idChan);    
      cs.setDouble(5, objEWCoda.tCodaTermObs);    
      cs.setDouble(6, objEWCoda.tCodaTermXtp);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateTCodaStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateTCodaStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateTCodaStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CreateTCodaStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWCoda objEWCoda = (EWCoda)obj;             
                                     
    try                              
    {                                
      objEWCoda.idTCoda=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateTCodaStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateTCodaStatement::RetrieveOutputParams()
                                     
                                     
} // end class CreateTCodaStatement                      
                                       
                                       
//             <EOF>                   

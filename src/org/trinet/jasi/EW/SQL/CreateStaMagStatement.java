// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWCoda;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/08/28                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CreateStaMagStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CreateStaMagStatement()                          
  {                                    
    sSQLStatement = new String("Begin Create_Sta_Mag(OUT_idMagLink => :OUT_idMagLink,IN_idMagnitude => :IN_idMagnitude,IN_idDatum => :IN_idDatum,IN_dMag => :IN_dMag,IN_dWeight => :IN_dWeight); End;");
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CreateStaMagStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWCoda objEWCoda = (EWCoda)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWCoda.idMagnitude);    
      cs.setLong(3, objEWCoda.idDatum);    
      cs.setFloat(4, objEWCoda.dMag);    
      cs.setFloat(5, objEWCoda.dWeight);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateStaMagStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateStaMagStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateStaMagStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CreateStaMagStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWCoda objEWCoda = (EWCoda)obj;             
                                     
    try                              
    {                                
      objEWCoda.idMagLink=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateStaMagStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateStaMagStatement::RetrieveOutputParams()
                                     
                                     
} // end class CreateStaMagStatement                      
                                       
                                       
//             <EOF>                   

package org.trinet.jasi.EW.SQL;

/**
 * <p>Title: EWSQLStatement Class</p>
 * <p>Description: Base Class for creating EWDB SQL Calls</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author DavidK
 * @version 0.99
 */

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Vector;

public abstract class EWSQLStatement
{


  /*** CLASS METHODS ***
  public EWSQLStatement();
  public EWSQLStatement(Connection conn);
  public boolean CallStatement(Object InputOutputObject);
  public boolean CallQueryStatement(Object InputObject, Vector OutputObjectList);

  / * protected methods * /
  boolean init();                 // called by CallStatement() and CallQueryStatement()
  boolean PrepareStatement();     // called by init()
  boolean RegisterOutputParams(); // called by init()
  boolean SetInputParams(Object InputObject);       // called by CallStatement() and CallQueryStatement()
  boolean ExecuteStatement();     // called by CallStatement() and CallQueryStatement()
  boolean RetrieveOutputParams(Object OutputObject); // called by CallStatement()
  boolean RetrieveOutputParamsList(Vector OutputObjectList); // called by CallQueryStatement();
	void    SetConnection (Connection conn);  // used to set/change the DB Connection

  /* abstract methods */
  protected abstract boolean SetInputParams(Object InputObject);

  /*NOTE:  You will also almost always have to overload several of the
           following functions:
           RegisterOutputParams()  // for PL/SQL calls with output params
           RetrieveOutputParams()  // for PL/SQL calls returning one set of params
           RetrieveOutputParamsList() // for queries returning potentially multiple rows
    **********************************************/

  /*** END CLASS METHODS ***/

  /*** CLASS ATTRIBUTES ***/
  ResultSet          rs;
  CallableStatement  cs;
  boolean            bIsQuery;
  boolean            bInitialized;
  String             sSQLStatement;
  String             sClassName;
  Connection         conn;


  /*** CONSTRUCTORS ***/
  public EWSQLStatement()
  {
    /* by default any object is 1) Not a query, 2)Not Initialized,
        and 3)instantiated from EWSQLStatement(just for dumps) */
    bIsQuery = false;
    bInitialized = false;
    sClassName = new String("EWSQLStatement");
  }


  public EWSQLStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }

  public void SetConnection(Connection IN_conn)
  {
    conn = IN_conn;
    bInitialized = false;
    // used to set/change the DB Connection
  }


  /*** METHODS ***/
  public boolean CallQueryStatement(Object InputObject, Vector OutputObjectList)
  {
    if(OutputObjectList == null)
    {
      System.out.println("ERROR: " + sClassName + ":CallQueryStatement():  OutputObjectList is null!");
      return(false);
    }

    init();

    if(!SetInputParams(InputObject))
    {
      System.out.println("ERROR: " + sClassName + ":SetInputParams() failed!  Returning Error!");
      return(false);
    }

    if(!ExecuteStatement())
    {
      System.out.println("ERROR: " + sClassName + ":ExecuteStatement() failed!  Returning Error!");
      return(false);
    }

    if(!RetrieveOutputParamsList(OutputObjectList))
    {
      System.out.println("ERROR: " + sClassName + ":RetrieveOutputParamsList() failed!  Returning Error!");
      return(false);
    }

    // Close and release the cursor.  Make sure that we note that we
		// longer intialized since the statement will have to be reparsed
		// and the outputs will have to be re-registered.
		// DK 06/13/2002
    try
    {
      cs.close();
  	  bInitialized = false;
	    cs = null;
    }
    catch (SQLException ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }


    return(true);
  }  // end EWSQLStatement:CallQueryStatement()


  public boolean CallStatement(Object InputOutputObject)
  {
    if(InputOutputObject == null)
    {
      System.out.println("ERROR: " + sClassName + ":CallStatement():  InputOutputObject is null!");
      return(false);
    }

    init();

    if(!SetInputParams(InputOutputObject))
    {
      System.out.println("ERROR: " + sClassName + ":SetInputParams() failed!  Returning Error!");
      return(false);
    }

    if(!ExecuteStatement())
    {
      System.out.println("ERROR: " + sClassName + ":ExecuteStatement() failed!  Returning Error!");
      return(false);
    }

    if(!RetrieveOutputParams(InputOutputObject))
    {
      System.out.println("ERROR: " + sClassName + ":RetrieveOutputParams() failed!  Returning Error!");
      return(false);
    }

    // Close and release the cursor.  Make sure that we note that we
		// longer intialized since the statement will have to be reparsed
		// and the outputs will have to be re-registered.
		// DK 06/13/2002
    try
    {
      cs.close();
  	  bInitialized = false;
	    cs = null;
    }
    catch (SQLException ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }  // end EWSQLStatement:CallStatement()


  protected boolean init()
  {
    if(!bInitialized)
    {
      if(!PrepareStatement())
        return(false);
      if(!RegisterOutputParams())
        return(false);
      bInitialized = true;
    }
    return(true);
  }  // end EWSQLStatement:init()


  protected boolean PrepareStatement()
  {
    if(sSQLStatement == null || conn == null)
    {
      // handle null errors
      return(false);
    }

    try
    {
      cs = conn.prepareCall(sSQLStatement); //  catch SQLException;
			//System.out.println("Creating cs in " + sClassName);
    }
    catch (SQLException ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    // if SQLException  return false;

    return(true);
  }  // end EWSQLStatement:PrepareStatement()


  /** if you're calling a stored proc or PL/SQL
      then you need to overload this call and
      register your output variables
  **/
  protected boolean RegisterOutputParams()
  {
    return(true);
  }  // end EWSQLStatement:RegisterOutputParams()


  protected boolean ExecuteStatement()
  {
    try
    {
      if(bIsQuery)
			{
        rs=cs.executeQuery();
			}
      else
			{
        cs.execute();
        // commit
       this.conn.commit();
			}
      // return
      return(true);
    }
    catch (SQLException ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
  }

  protected boolean RetrieveOutputParams(Object OutputObject)
  {
    return(false);
  }

  protected boolean RetrieveOutputParamsList(Vector OutputObjectList)
  {
    return(false);
  }


}
//
// end class  EWSQLStatement
//

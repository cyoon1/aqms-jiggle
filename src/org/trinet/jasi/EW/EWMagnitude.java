                                                             
// *** PACKAGE NAME *** \                                   
package org.trinet.jasi.EW;                                  
                                                             
                                                             
// *** IMPORTS *** \                                        
import java.util.*;                                          
import java.sql.Connection;                                  
import org.trinet.jasi.EW.SQL.EWSQLStatement;                
                                                             
// individual SQL calls                                      
import org.trinet.jasi.EW.SQL.GetMagnitudeStatement;  
import org.trinet.jasi.EW.SQL.GetMagListStatement;      
import org.trinet.jasi.EW.SQL.CreateMagnitudeStatement;                            
                          
                                                             
/**                                    
 * <!-- Class Description>                      
 *                                              
 *                                              
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                  
                                                
                                   
                                   
public class EWMagnitude                                  
{                                                
                                                 
                                                 
  // *** CLASS ATTRIBUTES *** \                 
  public int Retcode; // Description               
  public long idMag; // Description               
  public String sSource; // Description               
  public float dMagAvg; // Description               
  public int iNumMags; // Description               
  public float dMagErr; // Description               
  public int iMagType; // Description               
  public long idOrigin; // Description               
                                                 
  // unique magnitude creation params
  public int bBindToEvent; // Description               
  public int bSetPreferred; // Description               
  public long idEvent; // Description               

                                                 
  // *** CLASS SQL STATEMENTS *** \             
  EWSQLStatement es1;                            
  EWSQLStatement es2;                            
  EWSQLStatement es3;                            
                                                 
                                                 
  // *** CONSTRUCTORS *** \                     
  public EWMagnitude()                                    
  {                                              
    es1 = new GetMagnitudeStatement(org.trinet.jasi.DataSource.getConnection()); 
    es2 = new GetMagListStatement(org.trinet.jasi.DataSource.getConnection()); 
    es3 = new CreateMagnitudeStatement(org.trinet.jasi.DataSource.getConnection()); 
  }                                              
                                                 
                                                 
  // *** CLASS METHODS *** \                    
  public void SetConnection(Connection IN_conn)  
  {                                              
    es1.SetConnection(IN_conn);                  
    es2.SetConnection(IN_conn);                  
    es3.SetConnection(IN_conn);                  
  }                                              
                                                 
                                                 
  public EWMagnitude Read()                                 
  {                                              
                                                 
    if(es1.CallStatement((Object)this))          
      return(this);                              
    else                                         
      return(null);                              
  }  // end EWMagnitude:Read()                              


  public Vector ReadList()                              
  {                                              
    Vector ResultList = new Vector();      
     int    iRetCode;                             
                                                 
    es2.CallQueryStatement((Object)this, ResultList);
    return(ResultList);                          
  }  // end EWMagnitude:ReadList()                              


  public EWMagnitude Write()                                 
  {                                              
                                                 
    if(es3.CallStatement((Object)this))          
      return(this);                              
    else                                         
      return(null);                              
  }  // end EWMagnitude:Write()                              
                                                 
                                                 
}  // end class EWMagnitude                               

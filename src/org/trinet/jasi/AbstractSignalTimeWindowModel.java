package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;

/**
 * Abstract parent of ChannelTimeWindowModels that return a list of ChannelTimeWindow that 
 * are likely to show seismic energy above the noise level. 
*/
public abstract class AbstractSignalTimeWindowModel extends ChannelTimeWindowModel {

    public static final int defMaxNoEnergySta = 10;
    protected int maxNoEnergySta = defMaxNoEnergySta;

    // Array of time-code channel names
    protected String timeChannelNames[] = null;
    

    public AbstractSignalTimeWindowModel() {
      setMyDefaultProperties();
    }

    public AbstractSignalTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public AbstractSignalTimeWindowModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public AbstractSignalTimeWindowModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public AbstractSignalTimeWindowModel(Solution sol) {
        super(sol);
    }

    // To reduce number of db query lookups of current gain (e.g. channel isAcceleration(), isVelocity())
    // overide here sets list channel values to those of the MasterList, thus we're assuming that
    // the MasterList has the desired gain values !
    public void setCandidateList(ChannelableList candidateListIn, boolean sortByDist) {
        super.setCandidateList(candidateListIn, sortByDist);
        if ((MasterChannelList.get() != null) && MasterChannelList.get() != candidateList) {
            Solution aSol = (synchListBySolution) ?  sol : null;
            DateTime time = (aSol != null) ? aSol.getDateTime() : new DateTime();
            candidateList.matchChannelsWithList(MasterChannelList.get(), time);
        } 
    }

    /** This filter is invoked by the getChannelTimeWindowList() method implementation.
     * @see #getChannelTimeWindowList()
     * */
    public abstract boolean shouldShowEnergy(Channel ch, double mag, double dist, double z);

    //Override of superclass method
    public ChannelableList getChannelTimeWindowList() {

        if (sol == null) {
          System.err.println(getModelName() + ": ERROR Required solution reference is null, no-op.");
          return null;
        }

        ChannelableList ctwCandidateList = getCandidateList();
        // Check if there any channels in list?
        if (ctwCandidateList == null) {
          System.err.println(getModelName()+": ERROR Required candidate channel list is null, no-op.");
          return null;
        }
        if (ctwCandidateList.isEmpty()) {
          System.err.println(getModelName()+": ERROR? candidate channel list is empty, no-op.");
          return new ChannelableList(0);
        }
        if (debug) 
          System.out.println(getModelName()+": DEBUG candidateList is MasterChannelList: " + (ctwCandidateList == MasterChannelList.get()));

        // Sort the master WF candidate list
        // Uncomment below if candidate list not presorted already -aww 09/25/2007
        //ctwCandidateList.distanceSort(sol);

        // Create ChannelableList subtype for adding acceptable channels, if including all, 
        // force inclusion of all orientations of seed channel type if any one is added; 
        // possible components must be in the candidateList returned above because
        // only channels in this candidateList may be added to outputList.
        ChannelableList outputList = ( getIncludeAllComponents() ) ?
            new ChannelableSetList(ctwCandidateList) : new ChannelableList();

        // if there's no mag err on the side of caution and set
        // it to the value that will keep all channels, test for magnitude existance -aww
        double mag = (sol.magnitude == null || 
                      sol.magnitude.hasNullType() || 
                      sol.magnitude.hasNullValue()) ?
                              getDefaultNullMag() : sol.magnitude.getMagValue(); // instead use "default" when unknown -aww 2009/09/29
                              //getIncludeAllMag() : sol.magnitude.getMagValue(); // as per Lombard requested change -aww 2009/09/29

        if (debug) bm.reset();

        if (mag >= getIncludeAllMag())  {  // >= threshold, include all from the master candidate list
          System.out.println(getModelName()+": Event mag > mag threshold - including ALL candidate channels.");
          outputList.addAll(ctwCandidateList);
        } else { //  for each channel of master candidate list, decide if it should be included
          outputList = addBySelectionCriteriaToList(mag, outputList, ctwCandidateList);
        }

        if (debug) {
          bm.printTimeStamp(getModelName()+": DEBUG First scan results: "+outputList.size() +
                  " channels from "+ outputList.getStationCount()+ " stations");
        }

        // Now add any defined required channels
        addRequiredChannelsToList(outputList);

        if (debug) {
          bm.printTimeStamp(getModelName()+": DEBUG Required list size = "+requiredList.size() +
              " after addition of required chans: "+outputList.size() + " channels from "+
              outputList.getStationCount()+ " stations");
          //outputList.dump();
        }

        // Below lines added to guarantee duplicate channels removed - aww 2009/01/05 
        ChannelableList outputListNoDup = new ChannelableList();
        channelableListToChannelList(outputListNoDup, outputList);
        outputList = outputListNoDup;

        // OK, now from list of selected channels make channel TimeWindows (ctw's)
        outputList = createChannelTimeWindowsFromList(outputList);

        // add optional time channels?
        // Collect the time channels
        if (timeChannelNames != null && timeChannelNames.length > 0) {
            // Query db for active time channels ...
            ChannelList timeChannels = ChannelList.getByComponent(timeChannelNames);
            if (timeChannels.isEmpty()) {
                System.out.println(getModelName() + " : no time channels returned by db query.");
            }
            else {
                if (debug) System.out.println(getModelName() + " DEBUG adding "+timeChannels.size()+ " time channels");
                // Find the earliest start and latest end times in the windows
                int count = outputList.size();
                ChannelTimeWindow ctw = null;
                TimeSpan maxSpan = new TimeSpan();
                for (int i = 0; i < count; i++) {
                  ctw = (ChannelTimeWindow) outputList.get(i);
                  maxSpan.include(ctw.getTimeSpan());
                }

                // Now add all time channels with the max span found in output list
                count = timeChannels.size();
                if ( maxSpan.isValid() ) {
                  for (int i = 0; i < count; i++) {
                    outputList.add(new ChannelTimeWindow(((Channelable)timeChannels.get(i)).getChannelObj(), maxSpan));
                  }
                }
                else {
                    System.err.println(getModelName() + " : invalid max timespan, skipping the adding of time channels.");
                }
            }
        }

        if (debug) {
          bm.printTimeStamp(getModelName()+": DEBUG Returned ChannelTimeWindow count= " + outputList.size());
        }

        outputList = filterListByChannelPropertyAttributes(outputList);
        if (filterWfListByChannelList)  filterListByChannelList(outputList);
        return (outputList.size() > getMaxChannels()) ? new ChannelableList(outputList.subList(0,getMaxChannels())) : outputList;
    }

    protected void addRequiredChannelsToList(ChannelableList aList) {

      // Include channels with picks, amps, etc. if include properties are set. 
      setRequiredChannelList(new ChannelableList());

      // Note: ChannelableList.addAll() disallows "equal" object duplicates, adds extra components when input is ChannelableSetList subtype
      boolean chanAdded = aList.addAll(requiredList, false);
      // Uncomment below if LatLonz in required list not looked up already by other methods -aww 09/25/2007
      //if (chanAdded) aList.insureLatLonZinfo(sol.getDateTime());
    }

    /** Use the instance criteria to decide which channels from the candidate list
     * should be included in the returned final list.<p>
     * The final list is passed in as an argument because the caller may want to
     * instantiate various types of ChannelableList's, e.g. ChannelableSetList() which
     * will force inclusion of all three compontents of a triaxial.*/
    private ChannelableList addBySelectionCriteriaToList(double mag,
                    ChannelableList outList,
                    ChannelableList ctwCandidateList) {

          // define loop variables
          Channel ch = null;
          double dist = 0.;
          double minDist = getMinDistance();
          double maxDist = getMaxDistance();
          double z = sol.getModelDepth(); // aww 2015/10/10
          int count = ctwCandidateList.size();
          int in = 0, out = 0, outStaCnt = 0;
          String oldSta = "";
          boolean isSelected = false;

          for (int i=0; i < count; i++) {

            if (debug) if (i > getMaxChannels()) break; // for testing only

            ch = ((Channelable) ctwCandidateList.get(i)).getChannelObj();
            dist = ch.getHorizontalDistance(); // don't use slant distance -aww

            if (dist < minDist) {        // Include all inside min dist
                isSelected = true;
                if (debug)
                    System.out.println(getModelName() + " DEBUG + "+ch.toDelimitedNameString(".") + " at " + dist + " km is inside minDist: "+ minDist);
            } else if (dist > maxDist) { // Exclude all outside max dist
                isSelected = false;
                if (debug)
                     System.out.println(getModelName() + " DEBUG - "+ch.toDelimitedNameString(".") + " at " + dist + " km is outside maxDist: "+ maxDist);
                break;                   // Since dist sorted no others will qualify, bail out of loop
            } else {                     // Otherwise test signal-to-noise
                isSelected = shouldShowEnergy(ch, mag, dist, z);
            }

            if ( isSelected ) { // add channel to list if it passed muster
              // add() method enforces rule that each channel can only appear in the list once.
              // if outList is a ChannelableSetList() will add all 3 components
              outList.add(ch);
              in++;
              outStaCnt = 0; // has energy, so reset consecutive sta w/o energy counter

            } else { // channel w/o energy
              out++;
              if ( ! ch.getSta().equals(oldSta)) { // a new sta w/o energy
                // but bump consecutive counter only when a high-gain channel ? 
                if (ch.getSeedchan().substring(1,2).equals("H")) {
                    oldSta = ch.getSta(); 
                    outStaCnt++;
                }
              }
              if (outStaCnt > maxNoEnergySta) { // consecutive count of sta w/o energy (sorted by dist) reached limit
                  if (debug) System.out.println(getModelName() +  " DEBUG  rejected energy exceeded maxNoEnergySta = " +maxNoEnergySta);
                  break; // done
              }
            }
            //if (debug) System.out.println(ch.getChannelId().toString()+" dist= "+ (int)dist);

          } // end of for loop
//          if (debug) {
//            System.out.println(" ---- addBySelectionCriteriaToList() ----");
//            System.out.println(" Included = "+in +"  excluded = "+out);
//            System.out.println(" Total inc. (including 'drag along' components) = " + outList.size());
//            System.out.println(" Beyond max dist = "+ (count-(in+out)));
//          }
          return outList;
    }

    // Sorts list, rejects channelables whose distance is undefined, then creates list of CTW
    private ChannelableList createChannelTimeWindowsFromList(ChannelableList aList) {

        int count = aList.size();
        ChannelableList ctwList = new ChannelableList(count); // allocate capacity
        if (count < 1) return ctwList; // return empty List

        // resort since channels may have to added to end of input list, does distance calc before sort.
        if (sol != null) aList.distanceSort(sol);

        // create CTW's
        // Note: since they're distance sorted and there should be lots of 3-comp
        // sta's in the list we'll optimize a bit by reusing CTW's if dist is the same.
        // This avoids repeating the traveltime calculations.
        double dist = 0.;
        double lastDist = -1.;
        TimeSpan lastTimeSpan = null;
        Channel ch = null;

        for (int i=0; i< count; i++) {

          ch = ((Channelable) aList.get(i)).getChannelObj();
          dist = ch.getHorizontalDistance(); // don't use slant distance -aww

          if (dist == Channel.NULL_DIST) {   // skip if no dist (no-op? already eliminated by addBySelectionCriteria method)
            if (debug) System.out.println(getModelName()+": DEBUG Skip channel, no distance for: "+ch.getChannelName().toString());
          } else {
            if (dist != lastDist) { // New dist, calc new window
              lastDist = dist;
              lastTimeSpan = getTimeWindow(dist);
            }
            if (debug)
              System.out.println(getModelName() + " DEBUG CTW "+ch.channelId.toDelimitedNameString(".") + " "+ lastTimeSpan);
    
            ctwList.add( new ChannelTimeWindow(ch, lastTimeSpan) );
          }
        }
        if (count > 0 && ctwList.size() == 0)
            System.out.println(getModelName() + " - Solution has unknow lat,lon ? Model requires distances, thus not valid for triggers.");

        return ctwList;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        maxNoEnergySta = defMaxNoEnergySta; // maximum number of consecutive rejected showShowEnergy() = false
        timeChannelNames = null;
    }

    public void setProperties(GenericPropertyList props) {

        super.setProperties(props);   /// get all the standard stuff

        if (props != null) {
          String pre = getPropertyPrefix();
          if (props.isSpecified(pre+"maxNoEnergySta") )
              maxNoEnergySta = props.getInt(pre+"maxNoEnergySta");
          if (props.isSpecified(pre+"timeChannelNames") )
            setTimeChannelNames(props.getProperty(pre+"timeChannelNames"));
        }
    }

    public String getParameterDescriptionString () {
        Format df = new Format("%5d");
        StringBuffer sb = new StringBuffer(1024);
        sb.append(super.getParameterDescriptionString());
        String pre = getPropertyPrefix(); 
        sb.append(pre).append("maxNoEnergySta = ").append(df.form(maxNoEnergySta)).append("\n"); 
        sb.append(pre).append("timeChannelNames = " ).append(GenericPropertyList.toPropertyString(getTimeChannelNames())).append("\n");
        return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
        ArrayList list = super.getKnownPropertyKeys();
        String pre = getPropertyPrefix(); 
        list.add(pre + "maxNoEnergySta");
        list.add(pre + "timeChannelNames");
        return list;
    }


    /**
     * Set list of 3-character channel codes that will ALWAYS be included in the list.
     * @param string
    */
    public void setTimeChannelNames(String string) {
        if (string == null) return;
        String delList = " ,:;\t\n" ;  // delimiter list - added whitespace -aww 2009/08/20
        StringTokenizer strTok = new StringTokenizer(string, delList);
        ActiveList list = new ActiveList(); // doesn't allow duplicate add
    
        try {
            while (strTok.hasMoreTokens()) {
                list.add(strTok.nextToken().trim());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Bad syntax in timeChannelNames property string:" +string);
        }
        if (!list.isEmpty())
            timeChannelNames = (String []) list.toArray(new String[list.size()]);

    }
    
    public String[] getTimeChannelNames() {
        return timeChannelNames;
    }
 
}

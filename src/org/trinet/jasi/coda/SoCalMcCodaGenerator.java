package org.trinet.jasi.coda;

/** To calculate time series amplitude coda decay data and get results in a Coda object. */
public class SoCalMcCodaGenerator extends DefaultMcCodaGenerator {

    public SoCalMcCodaGenerator() {
        super();
        this.algorithm  = "SoCalMc";
    }

    //WARNING 
    // Change to mirror the Md model a window do every 2 sec window
    // in order to do get tau at cutoff termination, otherwise method
    // need to extrapolate the final tau for termination counts aveAmp. 
    // set forceExtrap = true 
    protected double getWindowStartingSampleSecsOffset(int windowCount) {
        forceExtrap = false; 
        if (windowCount < 6)  return  2.0  * windowCount;
        forceExtrap = true;  // start skips multi window lengths along coda
        if (windowCount < 14) return  5.0  * (windowCount - 3);
        if (windowCount < 21) return 10.0  * (windowCount - 8);
        return 20.0 * (windowCount - 14);
    }
}

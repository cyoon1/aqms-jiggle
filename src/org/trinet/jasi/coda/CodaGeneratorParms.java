package org.trinet.jasi.coda;
/* Historical MCA coda parameters
   PARAMETER QFIX    = 1.8             ! default value
   PARAMETER MAXSEG  = 20              ! max good windows
   PARAMETER KLIP    = 360             ! clipping amp
   PARAMETER MINVALS = 3               ! min readings, if(nmags >= MINVALS) then do summary mag
   NBYT              = 5.12/DT         ! samples per window 
   THRHI             = 3.0*KLTA + 6.0  ! starting level
   THRLO             = 0.5*THRHI       ! background cutoff noise level
   if (XAMP < 1.2*THRLO) ...           ! 1.8 xlast pass thru else stop processing coda
*/

import java.io.*;
import java.util.*;

// Temporarily until best deployment option decided.
/** CodaGenerator algorithm parameters. */
public class CodaGeneratorParms {

    public static final double QFIX_DEFAULT = 1.8;

    public static final CodaGeneratorParms CI_MC =
        //new CodaGeneratorParms("c", false, 2, 20, 3., 1.5, 1.8, 2.0, 10., QFIX_DEFAULT); 
        new CodaGeneratorParms("c", false, 2, 20, 1., 1., 1.8, 2.0, 10., QFIX_DEFAULT);  // changed defaults 2008/03/07 -aww
    public static final CodaGeneratorParms CI_MD =
        //new CodaGeneratorParms("d", false, 2, 60, 3., 1.5, 1.8, 2.0, 10., QFIX_DEFAULT); 
        new CodaGeneratorParms("d", false, 2, 60, 1., 1., 1.8, 2.0, 10., QFIX_DEFAULT);  // changed defaults 2008/03/07 -aww
    public static final CodaGeneratorParms NC_MC =
        //new CodaGeneratorParms("C", false, 2, 20, 3., 1.5, 1.8, 2.0, 10., QFIX_DEFAULT); 
        new CodaGeneratorParms("C", false, 2, 20, 1., 1., 1.8, 2.0, 10., QFIX_DEFAULT);  // changed defaults 2008/03/07 -aww
    public static final CodaGeneratorParms NC_MD =
        //new CodaGeneratorParms("D", false, 2, 23, 3., 1.5, 1.8, 2.0, 10., QFIX_DEFAULT); 
        new CodaGeneratorParms("D", false, 2, 23, 1., 1., 1.8, 2.0, 10., QFIX_DEFAULT);  // changed defaults 2008/03/07 -aww

    public final String  id;
    public final boolean resetOnClipping;
    public final int     minGoodWindowsToTerminateCoda;
    public final int     maxGoodWindowsToTerminateCoda;
    public final double  minSNRatioForCodaStart;
    public final double  minSNRatioCodaCutoff;
    public final double  passThruNSRatio;
    public final double  windowSize;
    public final double  biasLTASecs;
    public final double  qFix;

    protected CodaGeneratorParms(
                        String id, boolean resetOnClipping, 
                        int minGoodWindows, int maxGoodWindows,
                        double snrStart, double snrCutoff,
                        double passThruNSRatio, 
                        double windowSize,
                        double biasLTASecs,
                        double qFix
              ) {
        this.id                               = id;
        this.resetOnClipping                   = resetOnClipping;
        this.minGoodWindowsToTerminateCoda     = minGoodWindows;
        this.maxGoodWindowsToTerminateCoda     = maxGoodWindows;
        this.minSNRatioForCodaStart            = snrStart;
        this.minSNRatioCodaCutoff              = snrCutoff;
        this.passThruNSRatio                   = passThruNSRatio;
        this.windowSize                        = windowSize;
        this.biasLTASecs                       = biasLTASecs;
        this.qFix                              = qFix;
    }

    // Use Hardwired parameters in static members
    // removed public accessors and propfile loading to lower risk of end-user changing
    // ascii flat file input by mistake
    /*
    protected boolean load(String fileName) {
        Properties props = new Properties();
        boolean retVal = false;
        try {
            InputStream inStream = new BufferedInputStream( new FileInputStream(fileName));
            props.load(inStream);
            inStream.close();
            load(props);
            retVal = true;
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (SecurityException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return retVal;
    }
    protected void load(Properties props) {
        String tmpStr = props.getProperty("resetOnClipping", String.valueOf(false));
        resetOnClipping = Boolean.valueOf(tmpStr).booleanValue();
        tmpStr = props.getProperty("minGoodWindowsToTerminateCoda", String.valueOf(20));
        minGoodWindowsToTerminateCoda = Integer.parseInt(tmpStr);
        tmpStr = props.getProperty("maxGoodWindowsToTerminateCoda", String.valueOf(20));
        maxGoodWindowsToTerminateCoda = Integer.parseInt(tmpStr) ;
        tmpStr = props.getProperty("minSNRatioForCodaStart", String.valueOf(2.8));
        minSNRatioForCodaStart = Float.parseFloat(tmpStr);
        tmpStr = props.getProperty("minSNRatioCodaCutoff", String.valueOf(1.5));
        minSNRatioCodaCutoff = Float.parseFloat(tmpStr);
        tmpStr = props.getProperty("passThruNSRatio", String.valueOf(1.8));
        passThruNSRatio = Float.parseFloat(tmpStr);
        tmpStr = props.getProperty("windowSize", String.valueOf( 5.12));
        windowSize = Float.parseFloat(tmpStr);
        tmpStr = props.getProperty("biasLTASecs", String.valueOf(10.));
        biasLTASecs = Double.parseDouble(tmpStr);
    }
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append("id: ").append(id).append(" ");
        sb.append(" resetOnClipping:").append(resetOnClipping);
        sb.append(" minGoodWindowsToTerminateCoda:").append(minGoodWindowsToTerminateCoda);
        sb.append(" maxGoodWindowsToTerminateCoda:").append(maxGoodWindowsToTerminateCoda);
        sb.append(" minSNRatioForCodaStart:").append(minSNRatioForCodaStart);
        sb.append(" minSNRatioCodaCutoff:").append(minSNRatioCodaCutoff);
        sb.append(" passThruNSRatio:").append(passThruNSRatio);
        sb.append(" windowSize:").append(windowSize);
        sb.append(" biasLTASecs:").append(biasLTASecs);
        sb.append(" qFix:").append(qFix);
        return sb.toString();
    }

    public String getIdString() { return id; }

} // end of CodaGeneratorParms class


package org.trinet.jasi.coda;

/** To calculate time series amplitude coda decay data and get results in a Coda object. */
public class NoCalMdCodaGenerator extends DefaultMdCodaGenerator {
    /* mirrors EarthWorm RealTime window values for 23 windows
    // why is interval shorter for last two windows?  2...4..8..16, 16, 12, 5 ?
    private static final int noCalMdWindowCountStartingSecs[] = {
       0,   2,   4,   6,   8,  10,  12,  14,  18,  22,
      30,  38,  46,  54,  62,  70,  78,  86,  94, 110,
     126, 138, 143
    }; // 23 windows
    */
    public NoCalMdCodaGenerator() {
        super();
        this.algorithm  = "NoCalMd";
    }
    /*
    protected double [] getWindowTimesToSave() {
        return noCalMdWindowCountStartingSecs;
    }
    protected double getWindowStartingSampleSecsOffset(int windowCount) {
        if (windowCount <= 3) return windowCount;
        else return super.getWindowStartingSampleSecsOffset(windowCount) - 4.0;
        //if (windowCount < 23) {
        //  return (double) noCalMdWindowCountStartingSecs[windowCount];
        //}
        //else { // outside of EarthWorm realtime bounds so ad lib
        //  return 32.0 * (windowCount - 18); // at 23 = 160 secs, then 32 sec spacing after
        //}
    }
    */
}

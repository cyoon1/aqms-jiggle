package org.trinet.jasi;

import org.trinet.util.*;

/**
 * ChannelTimeWindowModel that just accepts all the channels in the candidate
 * list and defines time windows corresponding to when there whould be seismic energy
 * as defined by the EnergyWindow class.<p>
 *
 * If magnitude is available the window is defined by:<br>
 * o start = preP seconds before the P-wave arrival time as calculated by the TravelTime model.<br>
 * o end = P-wave arrival time + S-P time + coda duration time<p>
 *
 * But will never exceed the value of MaxWindow.
 * @see: EnergyWindow()
 *
 */

public class EnergyWindowModel extends ChannelTimeWindowModel {

  /** Model name string. */
  public static final String defModelName = "Energy Window";
  /** A string with an brief explanation of the model.*/
  public static final String defExplanation = "channels predicted to have seismic energy using current traveltime model";

  EnergyWindow etw = new EnergyWindow();

  static final double defMaxWindowSize = 120.0;

  {
      modelName = defModelName;
      explanation = defExplanation;
  }

  public EnergyWindowModel() {
      setMyDefaultProperties();
  }

  public EnergyWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
    super(gpl, sol, candidateList);
  }

  /** Sets the list of channels from which the algorithm can select channels.*/
  public EnergyWindowModel(ChannelableList candidateList) {
    super(candidateList);
  }

  public EnergyWindowModel(Solution sol, ChannelableList candidateList) {
    super(sol, candidateList);  // super now does distance sort of channel list by default -aww
  }

  /** Sets the Solution.*/
  public EnergyWindowModel(Solution sol) {
    this();
    super.setSolution(sol);
  }

  public ChannelableList getChannelTimeWindowList() {
      if (sol == null) {
        System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op");
        return null;
      }
      return super.getChannelTimeWindowList();
  }

  public TimeSpan getTimeWindow(double dist) {
      if (!windowDurationType.equalsIgnoreCase("default")) return super.getTimeWindow(dist); // added -aww 2010/12/10
      return etw.getEnergyTimeSpan( sol.getTime(), dist, sol.getPreferredMagnitude().getMagValue());
  }

  public void setProperties(GenericPropertyList props) {
      super.setProperties(props);   /// get all the standard stuff
      // criteria for later decisions
      etw.setMaxWindow(getMaxWindowSize());
      etw.setMinWindow(getMinWindowSize());
      etw.setPreP(getPreEventSize());
      if (debug) System.out.println (getParameterDescriptionString());
  }

  public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
  }

  private void setMyDefaultProperties() {
      maxWindow = defMaxWindowSize;
  }

}

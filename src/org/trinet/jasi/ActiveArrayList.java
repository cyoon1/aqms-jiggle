//TODO: Instead of EventListenerList use a local synched ArrayList  & implement ELL-like methods here
//REMOVED ChangeEvent Listener code; replaced by ListDataStateListener code
//
//TAKE NOTE: For robust element type checking define a Class data member for
//element subtype set by all subclass implementations; this classtype is
//checked before adding an element to list to avoid mixing element types
//(e.g. in PhaseList {myElementClass = Phase.class;}
// protected Class myElementClass = null;  // set by subclass instance init
// public boolean add (Object obj) {
//   try {
//     if (myElementClass != null && ! myElementClass.isInstance(obj)
//         throw new CastCastException("InputArgument wrong class type.");
//   }
//   catch (ClassCastException ex) {
//     //ex.printStackTrace();
//     System.err.println(ex.getMessage());
//     return false;
//   }
//   ...
// }
// Above logic for type-safety does not work for ArrayList xxxAll(Collection) methods,
// Overriden methods would need iterate over collection contents and check element's type
// to execute above logic.  This adds some extra overhead for type safety in bulk loads.
// AWW
//
package org.trinet.jasi;
import java.util.*;
import javax.swing.event.*;
import org.trinet.util.*;

/**
   An ArrayList that fires a a ListDataStateEvent when the list is modified.
*/
public class ActiveArrayList extends ArrayList implements ActiveArrayListIF  {

//
// Replaced ChangeEvent logic with the ListDataEvent logic  AWW
// ListenerList is not created until first listener is added,
// not all applications need to listen to special list changes.
//
// Listener list could be made transient, i.e. not written when Serialized.
// In that case listeners are not restored when deserialized, such as readinglists
// of a Solution and preferred Magnitude. However, since its not  transient 
// when non-serializable objects are added to list then the object can't be
// serialized. Thus changed ChannelList subclass to null this list before writing
// cache. -aww 02/06/2006
// protected transient EventListenerList dataListenerList = null;

  protected EventListenerList dataListenerList = null; // for ListDataEvents
// Not sure of need to serialize listeners, we must force subclass ChannelList
// to implement writeObject/readObject() overrides to clear or initialize the list:
//   private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException
//   private void writeObject(java.io.ObjectOutputStream stream) throws IOException

  // debug = true enables stack trace dump in set of duplicate key list element
  //       = false print only message, not stack trace
  public static boolean debug = false;

  public ActiveArrayList() { }

/** Make an empty ActiveArrayList with specified input capacity. */
  public ActiveArrayList(int capacity) {
    super(capacity);
  }

  public ActiveArrayList(Collection col) {
    addAll(col,false);
  }

/**
 * Wrapper around ArrayList addAll(Collection).
 * For faster addings to very long lists, no pre-filtering
 * of nulls or duplicates, Use ONLY if input is KNOWN to have
 * no nulls or duplicates (such as from previous addAll()).
 * Otherwise future actions on this list will have unpredicatble results.
 * Notifies ListDataState listeners, if any.
 * Returns 'true' if any elements were added.
 * Returns 'true' if input is this list (identity).
 * Throws exception if input list size is modified during add iteration,
 * such as by an asynchronous thread.
 */
  public boolean fastAddAll(Collection newList) {
    if (newList == this) return true; // 2005/08/09 noop identity
    int oldSize = size();
    boolean retVal = super.addAll(newList);
    if (retVal) {
      fireIntervalAdded(oldSize, this.size()-1);
    }
    return retVal;
  }


/**
 * Add elements from the input Collection to the list,
 * checkng to make sure no nulls or duplicates are entered.
 * Returns 'true' and notifies listeners if elements were added.
 * Returns 'true' if input is this list (identity).
 * Throws exception if input list size is modified during add iteration,
 * such as by an asynchronous thread.
 * @throws NullPointerException if input list is null
 * */
  public boolean addAll(Collection newList) {
    return addAll(newList, true);
  }
/**
 * Add elements from the input Collection to the list,
 * checkng to make sure no nulls or duplicates are entered.
 * Returns 'true' and if elements were added.
 * Notifies listeners ONLY if elements are added and input argument
 * notify==true.
 * Throws exception if input list size is modified during add iteration,
 * such as by an asynchronous thread.
 * Returns 'true' if input Collection is null or is this list (identity).
 * */
  public boolean addAll(Collection newList, boolean notify) {
    if (newList == this) return true; // 2005/08/09 noop identity
    // To avoid exception invoke newList.toArray() making a snapshot
    // of input and loop, though toArray() isn't atomic either.
    if (newList == null) return true; // do we want this return, or null pointer error?
    Iterator e = newList.iterator(); // iterator throws comodification exception
    int oldSize = size(); // current size
    ensureCapacity(newList.size());
    while(e.hasNext()) {
      add(e.next(), notify); // so that no nulls or duplicates are entered
    }
    if (oldSize < size()) {      // some were added
      //below only needed if add(...) override doesn't fire event
      //fireIntervalAdded(oldSize, this.size()-1);
      return true;
    }
    return false;
  }

/**
 * Adds elements to the list starting at the given index.
 * Checks to make sure no nulls or duplicates are entered.
 * Returns 'true' and notifies listeners if any elements were added.
 * Returns 'false' if input is this list (identity).
 * Throws exception if input list size is modified during add
 * iteration, such as by an asynchronous thread.
 */
  public boolean addAll(int index, Collection newList) {
    if (newList == this) return false; // 2005/08/09 reject identity
    int idx = index;
    Iterator e = newList.iterator(); // iterator throws comodification exception
    int oldSize = size(); // current size
    ensureCapacity(oldSize + newList.size());
    while (e.hasNext()) {
      add(idx, e.next()); // so that no nulls or duplicates are entered
      if (size() > oldSize) {    // if it was added, increment index
        oldSize = size();
        idx++;
      }
    }
    if (idx != index) {      // some were added
      //below only needed if add(...) override doesn't fire event:
      //fireIntervalAdded(index, index+idx-1);
      return true;
    }
    return false;
  }

/**
 * Add an object. Will not add object if its null or already in the list.
 * Returns 'true' and notifies listeners if object was added.
 */
  public boolean add(Object obj) {
    //System.out.println("DEBUG: ActiveArrayList add(obj) hashcode() : " + obj.hashCode());
    return add(obj, true);
  }
/**
 * Add an object. Will not add object if its null or already in the list.
 * Returns 'true' if object was added.
 * If notify = false, list listeners are not notified.
 */
  public boolean add(Object obj, boolean notify) {
    if ( obj == null || contains(obj) ) {  // doesn't permit null or duplicates of same object - aww
      return false;
    }
    if (super.add(obj) ) {
      if (notify) {
        fireIntervalAdded(size()-1, size()-1, obj);
      }
      return true;
    }
    return false;
  }
 /**
 * Insert an object at this index. Will not add object if its null or already in the list.
 * Overrides ArrayList.add() to inforce no nulls or duplicates rule.
 * Notifies listeners if object was added.
 */
  public void add(int index, Object obj)  {
    if ( obj == null || contains(obj) ) {  // doesn't permit null or duplicates of same object - aww
      return ;
    }
    int oldSize = size();
    super.add(index, obj);
    if (oldSize < size()) { // it was added
      fireIntervalAdded(index, index, obj);
    }
  }
 /**
 * Replaces object element at specified input index with
 * the input object only if indexOf(newObj) == -1.
 * Does a no-op and returns null if either the input is null or
 * if indexOf(input) returns index > 0 that differs from input index.
 * (i.e. no null or duplicate element rule.)
 * Otherwise replaces list element found at specified input index
 * Returns old object list element found at the input index.
 * DEFAULT DOES NOT NOTIFY LISTENERS because this
 * method is used by Collection sorts that rearrange elements
 * but do not change the contents.
 */
  public Object set(int index, Object newObj)  {
    return set(index, newObj, false);
  }
 /** As set(int,Object) except notify == true notifies listeners of the change. */
  public Object set(int index, Object newObj, boolean notify)  {
    if (newObj == null) return null; // don't allow null updates -aww
    int idx = indexOf(newObj);
    if (idx >= 0) {
      try {
        if (idx == index && (get(idx) == newObj)) { // identity
          return newObj; // short-circuit, no change
        }
        else if (idx != index) {
          //remove(idx); // do avoid remove duplicate
          throw new IllegalArgumentException("Input set index: "+index
                  +" duplicate element exists at list index: "+idx);
        }
      }
      catch(IllegalArgumentException ex) {
        if (debug) ex.printStackTrace();
        else System.err.println(ex.toString());
        return null;
      }
    }
    Object oldObj =  super.set(index, newObj);  // also replace like with like
    if (notify) {
      fireContentsChanged(index, index, oldObj);
    }
    return oldObj;
  }

/**
 * Remove object at specified index from the list. Returns object removed.
 * Notifies listeners if element was removed.
 */
  public Object remove(int index) {
    return remove(index, true);
  }
/**
 * Remove object at specified index from the list. Returns object removed.
 * If notify = false, list listeners are not notified.
 */
  public Object remove(int index, boolean notify) {
    // do we save the "removed" refs and pass them along in the StateChange?
    Object obj = super.remove(index);
    if (notify) {
      fireIntervalRemoved(index, index, obj);
    }
    return obj;
  }
/**
 * Remove an object from the list.
 * Returns 'true' and notifies listeners if object was removed.
 * */
  public boolean remove(Object obj) {
    return remove(obj, true);
  }
/**
 * Remove an object from the list. Returns 'true' if object was removed.
 * If notify = false, listeners are not notified.
 */
  public boolean remove(Object obj, boolean notify) {
    int index = indexOf(obj);
    if (index < 0) return false;
    remove(index, notify);
    return true;
  }
/**
 * Remove all objects in range specified
 * from (inclusive) index, to (exclusive) index.
 * Returns 'true' if any object was removed.
 * Notifies listeners if range was removed.
 */
  public void removeRange(int from, int to) {
    //save the "removed" refs and pass them along in the StateChange
    Object [] removedArray = this.subList(from,to).toArray();
    super.removeRange(from, to);
    fireIntervalRemoved(from, to-1, removedArray);
  }

/**
 * Remove all objects in list found in input collection.
 * Returns 'true' and notifies listeners if matching list elements
 * were removed.
 * Throws exception if input list size is modified during remove iteration,
 * such as by an asynchronous thread.
 * If this list and input are coupled by list listeners, if listener fires
 * removal event input list is modified resulting in exception thrown here.
 * e.g. invoking myList.removeAll(hisList) with coupled listeners.
 * The addxxx() methods avoid this recursion by not allowing list duplicates.
 */
  public boolean removeAll(Collection col) {
    if (col == this) { // 08/09/2005 input identity same as clear -aww
        clear();
        return true;
    }
    Iterator itr = col.iterator(); // iterator throws comodification exception
    boolean retVal = false;
    while (itr.hasNext()) {
      if (remove(itr.next())) retVal = true; // fires event
    }
    return retVal;
  }
/**
 * Remove all elements in found in input List by indexOf() test.
 * Returns count elements removed.
 * Throws exception if input list size is modified during remove iteration,
 * such as by an asynchronous thread.
 * If this list and input are coupled by list listeners, if listener fires
 * removal event input list is modified resulting in exception thrown here.
 * e.g. invoking myList.removeAll(hisList) with coupled listeners.
 * The addxxx() methods avoid this recursion by not allowing list duplicates.
 */
  public int removeAll(List aList) {
    if (aList == this) { // 08/09/2005 input identity same as clear -aww
        clear();
        return size();
    }
    int inSize = aList.size();
    int count = 0;
    for (int idx = 0; idx < inSize; idx++) {
      int removeIdx = this.indexOf(aList.get(idx));
      if (removeIdx >= 0) {
        this.remove(removeIdx); // fires events
        count++;
      }
    }
    return count;
  }

 /**
 * Remove all objects. Returns 'true' if successful.
 * Notifies listeners.
 */
  public void clear() {
    clear(true);
  }
 /**
 * Remove all objects. Returns 'true' if successful.
 * If notify = false, list listeners are not notified.
 */
  public void clear(boolean notify) {
    int mySize = size();
    // save the "removed" refs and pass them along in the StateChange
    Object [] removedRefs = null;
    if (notify) removedRefs = this.toArray();
    super.clear();
    if (notify) {
      fireIntervalRemoved(0, mySize-1, removedRefs);
    }
  }

  /**
   * Deletes the object if an element of the list.
   * Implementation here is synonymous with remove().
   * Returns 'true' and notifies listeners if object was removed.
   * Subclasses overriding this method can define behavior
   * different from simple list removal, such as a virtual
   * deletion, whereby the element remains in the list
   * but its state is marked or flagged internally as
   * "deleted". Overrides should always incorporate event
   * firing to propagate list element changes to listeners.
   * */
  public boolean delete(Object obj) {
    return remove(obj);
  }

 // ListenerList methods

  /** Register ListDataState listener with list */
  public void addListDataStateListener(ListDataStateListener l) {
    if (dataListenerList == null) dataListenerList = new EventListenerList();
    dataListenerList.add(ListDataStateListener.class, l);
  }
  /** Unregister ListDataState listener from list */
  public void removeListDataStateListener(ListDataStateListener l) {
    if (dataListenerList == null) return;
    dataListenerList.remove(ListDataStateListener.class, l);
  }

  /** Unregister ALL listeners */
  public void clearListDataStateListeners() {
    /*
    if (dataListenerList != null) {
      EventListener [] lsl = dataListenerList.getListeners(ListDataStateListener.class);
      for (int i=0; i<lsl.length; i++) {
          dataListenerList.removeListener(ListDataStateListener.class, lsl[i]);
      }
    }
    */
    dataListenerList = null;
  }

  /** Notify all listeners that have registered interest for notification on
    * this event type.  The event instance is created on each call to reflect the
    * current state. <p> The return from ListDataStateEvent event getSource()
    * is this list.
    * */
  protected void fireIntervalAdded(int start, int end) {
    fireIntervalAdded(start, end, null);
  }
  protected void fireIntervalAdded(int start, int end, Object theAdded) {
    StateChange sc = new StateChange(this, "added", theAdded);
    fireListDataStateEvent(ListDataStateEvent.INTERVAL_ADDED, sc, start, end);
  }
  protected void fireIntervalRemoved(int start, int end, Object theRemoved) {
    StateChange sc = new StateChange(this, "removed", theRemoved);
    fireListDataStateEvent(ListDataStateEvent.INTERVAL_REMOVED, sc, start, end);
  }
  protected void fireContentsChanged(int start, int end, Object theReplaced) {
    StateChange sc = new StateChange(this, "replaced", theReplaced);
    fireListDataStateEvent(ListDataStateEvent.CONTENTS_CHANGED, sc, start, end);
  }
  protected void fireOrderChanged(int start, int end) {
    StateChange sc = new StateChange(this, "order", null);
    fireListDataStateEvent(ListDataStateEvent.ORDER_CHANGED, sc, start, end);
  }
  protected void fireIntervalStateChanged(StateChange stateChange, int start, int end) {
    fireListDataStateEvent(ListDataStateEvent.STATE_CHANGED, stateChange, start, end);
  }
  public void fireListElementStateChanged(String stateName, int start, int end) {
    fireListDataStateEvent(ListDataStateEvent.STATE_CHANGED, new StateChange(this, stateName, null), start, end);
  }
  protected void fireListDataStateEvent(int type, StateChange stateChange, int start, int end) {
    if (dataListenerList == null || dataListenerList.getListenerCount() == 0 ) {
      return; // bail if no listeners
    }
    ListDataStateEvent ldsEvent =
                  new ListDataStateEvent(this, type, start, end, stateChange);
    // get listener snapshot before looping notification
    // synchronize getListenerList access if collection is not a simple array
    Object[] listeners = dataListenerList.getListenerList();
    // Notify the listeners last added to first.
    for (int i = listeners.length-2; i>=0; i-=2) {
      if (listeners[i] == ListDataStateListener.class) {
        switch (type) {
          case ListDataStateEvent.INTERVAL_ADDED :
            ((ListDataStateListener)listeners[i+1]).intervalAdded(ldsEvent);
            break;
          case ListDataStateEvent.INTERVAL_REMOVED :
            ((ListDataStateListener)listeners[i+1]).intervalRemoved(ldsEvent);
            break;
          case ListDataStateEvent.CONTENTS_CHANGED :
            ((ListDataStateListener)listeners[i+1]).contentsChanged(ldsEvent);
            break;
          case ListDataStateEvent.STATE_CHANGED :
            ((ListDataStateListener)listeners[i+1]).stateChanged(ldsEvent);
            break;
          case ListDataStateEvent.ORDER_CHANGED :
            ((ListDataStateListener)listeners[i+1]).orderChanged(ldsEvent);
            break;
        }
      }
    }
  }

  /** Return count of all registered listeners. */
  public int countListDataStateListeners() {
    return (dataListenerList == null) ? 0 : dataListenerList.getListenerCount();
  }

  public ListDataStateListener [] getListDataStateListeners() {
    return (ListDataStateListener []) dataListenerList.getListeners(ListDataStateListener.class);
  }

  /**
   * Since this class overrides the set(...) methods to check for duplicates
   * in the list as well as notifying listeners of order change, you should
   * use method implemented below to perform order sorts on an ActiveArrayList
   * instance instead of Collections sort(List, Comparator) to avoid
   * exceptions generated by a duplicate element insertion done by that method.
   * If the input list is a instance of ActiveArrayList it invokes the instance
   * sort(Compartor) method, otherwise it invokes delegates the sort to
   * Collections.sort(List, Comparator).
   * @see #sort(Comparator)
   * @see Collections.sort(List, Comparator)
   */
  public static void sort(List aList , Comparator c) {
      if (aList instanceof ActiveArrayList)
              ((ActiveArrayList) aList).sort(c);
      else Collections.sort(aList, c);
  }
  /**
   * Since this class overrides the set(...) methods to check for duplicates
   * in the list as well as notifying listeners of order change, you should
   * use method implemented below to perform order sorts on an ActiveArrayList
   * notify listeners of order change, you should use this instance method
   * instead of Collections sort(List, Comparator) to avoid the exception
   * generated by a duplicate element insertion when using the
   * Collections.sort(Comparator) method.
   * This method notifies listeners of order change.
   * @see Collections.sort(Comparator)
   */
  public void sort(Comparator c) {
    if (size() == 0) return; // aww added 2007/08/13 as test
    Object a[] = this.toArray();
    if (a.length > 1) {
      Arrays.sort(a, c);
      for (int j=0; j<a.length; j++) {
          super.set(j, a[j]); // avoids the throw of duplicate element exception
      }
    }
    fireOrderChanged(0, a.length); // notify
  }
}

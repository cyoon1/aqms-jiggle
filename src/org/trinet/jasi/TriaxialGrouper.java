package org.trinet.jasi;

/**
 * Similar to a StringTokenizer for Channelable lists. Given a Channelable list,
 * it will return smaller lists that contain only the Channelable objects for
 * a triaxial group. In other words, all three orientations of a particular instrument.
 *
 * This will sort the ChannelableList by NET.STA.COMP .
 * @see: ChannelNameSorter
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 */

public class TriaxialGrouper extends StationGrouper {

  public TriaxialGrouper() { }

  public TriaxialGrouper(ChannelableListIF list) {
    super(list);
  }
  public TriaxialGrouper(ChannelableListIF list, int sortType) {
    super(list, sortType);
  }

  /** Set the total groups present .*/
  protected void setGroupCount() {
    groupcount = list.getTriaxialCount();
  }

  /** Set the list to use. */
//  public void setList(ChannelableListIF list) {
//    this.list = list;
//    listClass = list.getClass();
//    setGroupCount();
//    list.sortByStation();                  // sort by net/sta/chan
//
//    groupsReturned = 0;
//    arraypointer = 0;
//  }

// This is the important method
/** Return true if the two channelable object belong in the same group. 
 * Note that this dis based on matching Net.Sta. and the first 2 characters of SEEDchan.
 * Given current naming conventions, this will also consider multiple borehole sensors 
 * to be in one group.
 * @See: AbstractChannelData.sameTriaxialAs() */
public boolean inSameGroup (Channelable ch1, Channelable ch2) {
  return ch1.sameTriaxialAs(ch2);
}
// //////////////////////////////////////////////////////////////////
/*
 public static void main(String[] args) {

    System.out.println ("Making connection...");
    DataSource ds = TestDataSource.create();

    ChannelableList sublist;

    ChannelList chanList = ChannelList.readCurrentListByName("RCG-TRINET");

    TriaxialGrouper grouper = new TriaxialGrouper(chanList);

    System.out.println("amps = "+ chanList.size());
    System.out.println("grps = "+grouper.countGroups());

    int knt = 0;
    while (grouper.hasMoreGroups()) {
      sublist = (ChannelableList) grouper.getNext();
      System.out.println("grp #"+ knt++);
      System.out.println(sublist.toString());
    }


    Channel ch = Channel.create();
    
    ch.setChannelName("BK", "YBH", "HHZ", "  ");
    
    ChannelableList grp = grouper.getGroupContaining(ch);
    
    System.out.println (grp.toString());
    
    //
    ChannelableList list2 = chanList.findChannelGroup(ch, null);
  
    System.out.println (list2.toString());    
  }
*/

/*
// Test grouping parametric data by station

  public static void main(String[] args) {

    System.out.println ("Making connection...");
    DataSource ds = TestDataSource.create("serverhu","databasearw");

    AmpList sublist;

    // read in the current station list - use cached version if available
//    long evid = 13950596;
//    long evid = 13951588;
    long evid =     13950672;
    Solution sol = Solution.create().getById(evid);
    sol.magnitude.loadReadingList();
    AmpList amplist = (AmpList) sol.magnitude.getReadingList();
    //Amplitude amp = Amplitude.create();
    //AmpList amplist = new AmpList(amp.getBySolution(evid));

    TriaxialGrouper grouper = new TriaxialGrouper(amplist);

    System.out.println("amps = "+ amplist.size());
    System.out.println("grps = "+grouper.countGroups());

    //amplist.dump();
    int knt = 0;
    while (grouper.hasMoreGroups()) {
      sublist = (AmpList) grouper.getNext();
      System.out.println("grp #"+ knt++);
      System.out.println(sublist.toNeatString());
    }

  }
 */
}

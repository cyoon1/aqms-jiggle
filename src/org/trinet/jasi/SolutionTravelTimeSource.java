package org.trinet.jasi;
import org.trinet.util.velocitymodel.*;
import org.trinet.util.gazetteer.*;

//
// Need workout a MasterTravelTime class to store a  travel time instance
// into static member singleton, like MasterChannelList and DataSource to
// be used by applications like Jiggle, Mung etc. Note currently that the
// Waveform, WFView classes appear to have a traveltime source integrated 
// using methods with depth parameter that creates a new LatLonZ(0.,0.,depth)
// which may cause problems if it is not the same as the Solution's LatLonZ.
// Need to rework classes with traveltime to avoid potential thread conflicts
// if sharing a global source rather than using a local instance. 
//
// Do we want to implement option to turn on/off assigned input weight (quality) 
// screening for phase traveltime preference of observed vs. calculated times?
//
// Should observed SphaseTT/PSRatio take precedence over calculated PphaseTT?
//
public class SolutionTravelTimeSource {

    protected static final String P_TYPE = "P";
    protected static final String S_TYPE = "S";
    protected Phase defaultPhase = Phase.create();

    protected PrimarySecondaryWaveTravelTimeGeneratorIF ttGenerator = null;
    protected Solution sourceSol = null;

    protected static boolean debug = false;

    public SolutionTravelTimeSource() {
        initGenerator(null, null); 
    }
    public SolutionTravelTimeSource(UniformFlatLayerVelocityModelIF vm) {
        initGenerator(vm, null); 
    }
    public SolutionTravelTimeSource(UniformFlatLayerVelocityModelIF vm, Solution sol) {
        initGenerator(vm, sol); 
    }

    protected void initGenerator(UniformFlatLayerVelocityModelIF model, Solution source) {
      UniformFlatLayerVelocityModelIF myModel = model; 
      if (myModel == null) { // Kludge here for default
        myModel = createDefaultModel();
      }
      if (ttGenerator == null) ttGenerator = new TTLayer(myModel);
      else ttGenerator.setModel(myModel);

      Solution mySource = source;
      if (mySource != null) setSource(mySource);
    }

    public static UniformFlatLayerVelocityModelIF createDefaultModel() {
        UniformFlatLayerVelocityModelIF flatLayerModel = null;
        UniformFlatLayerVelocityModel.createDefaultModels();
        if (EnvironmentInfo.getNetworkCode().equals("CI")) // SCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.SOCAL_DEFAULT;
        else if (EnvironmentInfo.getNetworkCode().equals("NC")) // NCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.NOCAL_DEFAULT;
        else // UNKNOWN do we return null or?
          flatLayerModel = UniformFlatLayerVelocityModel.TWO_LAYER_DEFAULT;
        return flatLayerModel; 
    }

    public void setSource(Solution sol) {
        if (sourceSol != null && sourceSol == sol) return;
        sourceSol = sol;
        if (sourceSol == null) throw new NullPointerException("traveltime source cannot be null");
        //ttGenerator.setSource(sourceSol); // removed to use model depth -aww 2015/10/10
        if (sourceSol != null) {
          ttGenerator.setSource(sourceSol.lat.doubleValue(),sourceSol.lon.doubleValue(),sourceSol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
          if (sourceSol.phaseList == null || sourceSol.phaseList.size() == 0) {
            sourceSol.listenToPhaseListChange(false);
            sourceSol.loadPhaseList();
            //sourceSol.setStale(false); // removed, doesn't reset magnitudes 09/08/2006 aww
            // enable "stale" side-effect of phaseList add - aww 09/08/2006
            sourceSol.listenToPhaseListChange(true);
          }
          //if (debug && sourceSol.phaseList != null) sourceSol.phaseList.dump();
        }
    }
    protected Phase setDefaultPhase(Channel chan, String type, Solution aSol) {
            defaultPhase.setChannelObj(chan);
            defaultPhase.description.set(type, "", "");
            defaultPhase.sol = aSol;
            return defaultPhase;
    }
    public double calcTravelTime(Channel chan, String type) {
        double range = chan.getHorizontalDistance();
        //double tt = (type == P_TYPE) ? ttGenerator.pTravelTime(range) : ttGenerator.sTravelTime(range);
        double tt = 0.;
        if (type.equals(P_TYPE)) {
           tt = ttGenerator.pTravelTime(range);
        }
        else {
           tt = ttGenerator.sTravelTime(range);
        }
        if (debug) System.out.println("   CodaMagMeth calcTravelTime for:" + chan.toDelimitedNameString("_") +
                       " range:" + range +" "+type+" tt: " + tt +
                       " hdist: " + ((LatLonZ)ttGenerator.getSource()).horizontalDistanceFrom(chan.getLatLonZ()));
        return tt;
    }

    public double getOriginTime() {
        return (sourceSol == null) ? 0. : sourceSol.getTime();
    }
    public double getTimeOfP(Channel chan) {
        return getOriginTime() + getPTravelTime(chan);
    }
    public double getTimeOfS(Channel chan) {
        return getOriginTime() + getSTravelTime(chan);
    }
    public double getSminusP(Channel chan) {
        double pTT = getPTravelTime(chan); // may be the observed or a calculated phase time
        double sTT = getSTravelTime(chan, pTT); // may be the observed or calculated phase time
        return (sTT-pTT);
    }
    public double getTravelTime(Channel chan, String type) {
        double tt = getPhaseTravelTime(chan, type);
        return (tt > 0.) ? tt : calcTravelTime(chan, type);
    }
    // Not "symmetrical" with sTT method, if no observed P time,
    // an observed sTT/PSRatio doesn't trump the calculated value.
    // No assigned weight screening, even 4 weights trump calculation
    public double getPTravelTime(Channel chan) {
        return getTravelTime(chan, P_TYPE) ;
    }
    public double getSTravelTime(Channel chan) {
        return getSTravelTime(chan, 0.);
    }
    public double getSTravelTime(Channel chan, double pTravelTime) {
	// See if there is a "timed" S, if so return it, default is any weight
        double sTravelTime = getPhaseTravelTime(chan, S_TYPE);
        if (sTravelTime > 0.) return sTravelTime;

	// Use Vp/Vs ratio to estimate sTT if pTT is provided
        if (pTravelTime > 0.) { // 
            sTravelTime = pTravelTime * ((UniformFlatLayerVelocityModel) ttGenerator.getModel()).getPSRatio();
        }
        else { // no other data, so calculate it
            sTravelTime = calcTravelTime(chan, S_TYPE);
        }
        return sTravelTime;
    }
    public double getPhaseTravelTime(Channel chan, String type) {
        double tt = 0.;
        // hasSPhaseTimed = false;
        if (sourceSol.phaseList.size() > 0) {
            Phase phase = setDefaultPhase(chan, type, sourceSol);
            phase = (Phase) sourceSol.phaseList.getSameChanType(phase);
	    // Note no filtering by assigned weight, accepts input weights 0-4 => quality 1.-0.
            if (phase != null && phase.hasValidTime()) {
              tt = phase.getTime() - sourceSol.getTime(); // convert to traveltime by removing origin time
              //if (S_TYPE.equals(type)) hasSPhaseTimed = true;
              if (debug) System.out.println("    getPhaseTravelTime for observed phase "+type+" tt: " + tt);
            }
        }
        return tt;
    }

    public String toString() {
        return ttGenerator.toString();
    }

    public static void setDebug(boolean tf) {
        debug = tf;
    }

} // end of travelTime processing class

package org.trinet.jasi;
import java.util.ArrayList;
import org.trinet.util.*;

/**
 * <p>Just get all of the channels defined by a named list of channels.
 * The name is set with the "candidateListName" property. 
 * The concrete class will determine how this is interpreted. It could be a filename.
 * The default implementation uses a group named in the APPLICATIONS table
 * of the TriNet dbase associated with channel names in JASI_CONFIG_VIEW.
 * @see: Channel.getNamedChannelList() 
 */

public class NamedChannelTimeWindowModel extends ChannelTimeWindowModel {

    /** Model name string. */
    public static final String defModelName = "Named List";
    /** A string with an brief explanation of the model.*/
    public static final String defExplanation = "channels associated in database with a named list (e.g. program application name)";
  
    protected static final double defMaxWindowSize = 120.;

    {
        setModelName(defModelName);
        setExplanation(defExplanation);
    }
    
    public NamedChannelTimeWindowModel() {
        setMyDefaultProperties();
    }
  
    public NamedChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
      super(gpl, sol, candidateList);
      if (sol != null) setStartTime(sol.getTime()-getPreEventSize());
    }
  
    public NamedChannelTimeWindowModel(Solution sol, ChannelableList candidateList) {
      this(null, sol, candidateList);
    }
    
    public NamedChannelTimeWindowModel(ChannelableList candidateList) {
      this(null, null, candidateList);
    }
  
    /** Set the Solution to use in the model. May do special setup specific to
     * time or location of event here. */
    public void setSolution(Solution sol) {
        super.setSolution(sol);
        if (sol != null) setStartTime(sol.getTime()-getPreEventSize());
    }

    public TimeSpan getTimeWindow(double dist) {
        return new TimeSpan(starttime, starttime+getMaxWindowSize());
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
      maxWindow = defMaxWindowSize;
      starttime = defStartTime;
      includePhases = false;
      includeMagAmps = false;
      includePeakAmps = false;
      includeCodas = false;
    }
}

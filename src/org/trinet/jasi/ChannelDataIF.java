package org.trinet.jasi;
import org.trinet.jdbc.table.*;

public interface ChannelDataIF extends JasiSourceAssociationIF,
        ChannelIdentifiable, DateRangedIF, ChannelDataReaderIF {
    //public ChannelIdIF getChannelId();
    //public void setChannelId(ChannelIdIF id);
    //public boolean equalsChannelId(ChannelIdIF id);
    //public DateRange getDateRange();
    //public void setDateRange(DateRange dr);
    //public boolean hasDateRange();
    //public boolean isValidFor(java.util.Date date);
    //public boolean equalsDateRange(DateRange dr);

    public boolean equalsChannelDateRange(ChannelDataIF cd);
    public boolean copy(ChannelDataIF cd); // replace same data member type values?
    public ChannelDataIF getByChannelId(ChannelIdIF chanId, java.util.Date date);
    public Object clone();
}


package org.trinet.jasi;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for a generic amplitude calibration for a station channel.
*/
public abstract class MhAmpMagCalibration extends AbstractAmpMagCalibration {

    protected MhAmpMagCalibration() {
        this(null, null);
    }

    protected MhAmpMagCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected MhAmpMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
        corrType.setValue(CorrTypeIdIF.MH);
    }
    protected MhAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    protected MhAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag) {
        this(id, dateRange, value, corrFlag, null);
    }
    protected MhAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag, String authority) {
        super(id, dateRange, value, CorrTypeIdIF.MH, corrFlag, authority);
    }

    //override super methods if necessary e.g.:
    //public double getDefaultClipAmp() ;

    public static MhAmpMagCalibration create() {
        return create(JasiObject.DEFAULT);
    }

    public static final MhAmpMagCalibration create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final MhAmpMagCalibration create(String suffix) {
        return (MhAmpMagCalibration) JasiObject.newInstance("org.trinet.jasi.MhStaCorrAmpMagCalibrView", suffix);
    }

    public AbstractChannelCalibration createDefaultCalibration(ChannelIdIF chan) {
        MhAmpMagCalibration calibr = MhAmpMagCalibration.create();
        calibr.channelId = (ChannelIdIF) chan.clone(); // try test for cloneable?
        calibr.corr.setValue(getDefaultCorrection()); // ? aww what should it be NaN or setNull
        calibr.clipAmp.setValue(getDefaultClipAmp());
        calibr.gainCorr.setValue(getDefaultGainCorr());
        return calibr;
    }
}

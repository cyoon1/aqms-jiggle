package org.trinet.jasi;
/**
 * Interface that contains the names of all schema types supported by jasi.
 * All jasi objects (abstract JasiObject) implement this interface to control
 * their factory methods. These values are for convenience. It is possible to
 * instantiat other concrete jasi implementation classes by passing the correct
 * suffix to the JasiObject.create() method. <p>
 *
 * Currently defined concrete classes:<p>
<table BORDER COLS=1 WIDTH="100%" >
<tr>
  <td>typeString</td> <td>suffix</td> <td>Comment</td>
</tr>
<tr>
  <td>TRINET</td> <td>TN</td> <td>TriNet NCDC schema</td>
</tr>
<tr>
  <td>EARTHWORM</td> <td>EW</td> <td>Earthworm schema</td>
</tr>
  <td>BAP</td> <td>BP</td> <td>Bap USGS schema</td>
</tr>
 </tr>
  <td>PNSN</td> <td>UW</td> <td>AQMS schema, for PostgreSQL (schema itself is like Trinet's)</td>
</tr>
</table>
 *
 * @see: org.trinet.jasi.JasiObject
 */
public interface JasiFactory  {

    // Supported schema types and aliases

    /** Code for the TriNet (NCEDC v1.5) schema support jasi classes */
    public static final int TRINET     = 0;

    /** Code for the Earthworm schema support jasi classes */
    public static final int EARTHWORM  = 1;

    /** Code for the BAP schema support jasi classes */
    public static final int BAP  = 2;

    /** Code for the PNSN schema support, NCEDC schema implemented on Postgres */
    public static final int PNSN = 3;

    // Additional schemas would be added here

    /** Strings describing schema types. "TRINET", "EARTHWORM" */
    public static final String[] typeString = {"TRINET", "EARTHWORM", "BAP", "PNSN"};

    /** Strings used as suffixes for concrete class names. "TN", "EW" */
    public static final String[] suffix = {"TN", "EW", "BP", "TN"};

    /** Map string schema names to int descriptions */
    public static final int[] typeValue = {TRINET, EARTHWORM, BAP, PNSN};

}

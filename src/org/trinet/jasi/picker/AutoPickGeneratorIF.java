package org.trinet.jasi.picker;

import org.trinet.util.GenericPropertyList;

public interface AutoPickGeneratorIF {
    public PhasePickerIF getPhasePicker();
    public void setPhasePicker(PhasePickerIF picker);
    public boolean setupPicker(GenericPropertyList props);
    public boolean setupPicker(String pickerClassName, GenericPropertyList props, boolean clearParameters);
    public void autoPick(int pickFlag);
}

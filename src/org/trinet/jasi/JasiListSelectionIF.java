package org.trinet.jasi;
public interface JasiListSelectionIF {
    public JasiCommitableIF getSelected(); // use to be Solution wrapper only
    public JasiCommitableIF getNext(); // returns element following "selected id match" or wraps to beginning?
    public int getSelectedIndex();
    public boolean setSelected(JasiCommitableIF sel); // requires Object arg for an IF method. Used to be solution
    public boolean setSelected(JasiCommitableIF sel, boolean notify); // requires Object arg for an IF method
    public int getIndex(JasiCommitableIF jc);  // requires Object arg for an IF method, see above comment for equality
    public JasiCommitableIF getNext(JasiCommitableIF jc);   // requires Object arg for an IF method, see comment id equality
}

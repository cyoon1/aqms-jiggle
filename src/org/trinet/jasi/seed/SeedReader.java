package org.trinet.jasi.seed;

import java.io.*;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.*;
import java.lang.*;
import java.math.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.util.DateTime;
import org.trinet.util.TimeSpan;
import org.trinet.util.LeapSeconds;
import org.trinet.util.BenchMark;

import org.trinet.util.WaveClient;
import org.trinet.waveserver.rt.WaveServerSource;
import org.trinet.waveserver.rt.TrinetReturnCodes;

// Changed to use Fissure decoder to get Steim2
// see: http://www.seis.sc.edu/software/Fissures/seed.html
//import edu.iris.Fissures.seed.codec.Steim1;
//import edu.iris.Fissures.seed.codec.Steim2;

// 2/13/2003 DDG - Imported newer version of Fissures Steim decompression code
// The older version had an error in Steim2 decompression that was corrected in
// the newer version. They also changed the package name which seems to reflect
// the idea that the SEED code is distinct from IRIS code.
import edu.iris.dmc.seedcodec.*;
/**
 * SeedReader. Read seed files and interpret them. All methods in the class are
 * static because we want only one instance of the RemoteFileReader. The class will
 * determine if Waveform files are local or remote at instantiation time.
 * If remote, the class reads from the data source using DbBlobReader which gets
 * timeseries using a stored procedure in the dbase. <p>
 *
 * This class also knows how to read the waveforms from various data sources.
 * See the various getDataXxxxxx() methods.

 */
public class SeedReader implements SeedConstantsIF {

    // Make the FTP reader, make one at instantiation time so the connection
    // will remain open rather then getting garbage collected.
    //  static RemoteFileReader remoteReader = new RemoteFileReader();

    static DbBlobReader dbBlobReader = null;
    static BenchMark bm = new BenchMark();
    static public boolean debug = false;
    /**
    * NOTE: If the source is the database then a DataSource connection should 
    * already be established and open.
    */
    public SeedReader() { }

    /** Decompresses MiniSEED packets.  Returns float[] of time-series samples if
    * packet header indicates a data packet, else returns null.
    *
    * @exception java.lang.SeedReaderException input null, input packet <512 bytes, or i/o
    * error parsing packet data.  */
    public static float [] getDataSamples(byte [] seedPacket) throws SeedReaderException {
        float [] samples = null;
        try {
           if (seedPacket == null)
               throw new NullPointerException("SeedReader.getDataSamples(byte[]) Null input parameter");
           if (seedPacket.length < 512)
               throw new IllegalArgumentException("SeedReader.getDataSamples(byte[]) Input byte array length:"
                                                   + seedPacket.length + " is too short for known packet sizes");

            SeedHeader hdr = SeedHeader.parseSeedHeader(seedPacket);
            WFSegment wfSegment = hdr.createWFsegment();

            if (hdr.isData() ) { // decompress only data messages
                samples = decompress(hdr, seedPacket);
            }
        }
        catch (NullPointerException ex) {
            throw new SeedReaderException(ex.getMessage());
        }
        catch (IllegalArgumentException ex) {
            throw new SeedReaderException(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new SeedReaderException("getDataSamples(): " + ex.getMessage());
        }
        return samples;
    }

    /** Decodes a Collection containing miniSEED time-series data packets as elements.
    * Returns a Collection of jasi.WFSegments with the same number of elements.
    * @exception java.lang.SeedReaderException input null, or i/o error parsing packet data.
    */
    public static Collection getWFSegments(Collection packetList) {
        if (packetList == null)
                throw new NullPointerException("getWFSegments() Null input collection of seed packets.");

        final int MAX_PACKET_SIZE = 8192;        // test efficiency, instead of dynamically creating new arrays for each packet.
        Collection wfSegmentCollection = new ArrayList(packetList.size());
        Iterator iter = packetList.iterator();

        SeedHeader hdr = null;
        WFSegment wfSegment = null;
        byte [] seedPacket = null;
        try {
          while (iter.hasNext()) {
            seedPacket = (byte []) iter.next();
            hdr = SeedHeader.parseSeedHeader(seedPacket);
            wfSegment = hdr.createWFsegment();

            if ( hdr.isData() ) { // decompress only data messages
              wfSegment.setTimeSeries( decompress(hdr, seedPacket) );
            }
            wfSegmentCollection.add(wfSegment);
          }
        }
        catch (NullPointerException ex) {
            throw new SeedReaderException(ex.getMessage());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new SeedReaderException("getWFSegments(): " + ex.getMessage());
        }
        return wfSegmentCollection;
    }

    /**
     * Read a file in Seed format as described in a waveform object. Returns
     * the number of WFSegments read . The access method, local or remote,
     * is determined automatically.
     */
    public static int getData(Waveform wf) {
        return getData(wf, wf.getStart().doubleValue(), wf.getEnd().doubleValue());
    }

    public static int getData(Waveform wf, double startTime, double endTime) {
      int status = 0;

      bm.reset();

      if (wf.getWaveSourceType() == AbstractWaveform.LoadFromDataSource) {
        // Try accessing locally first, if that fails use remote access.
        if (wf.filesAreLocal()) { // timeseries source type 1
            status = getDataLocal(wf);        // try local access
        }
        else { // timeseries source type 2
            status = getDataFromDataSource(wf, startTime, endTime); // try test DB access via blob - aww
        }
      } else if (wf.getWaveSourceType() == AbstractWaveform.LoadFromWaveServer) { // timeseries source type 3
         status = getDataFromWaveServer(wf);
      }

      // collapse multiple segments into minimum number, save packetspans
      wf.collapse();

      if (SolutionWfEditorPropertyList.seedReaderVerbose) {
        bm.setPrintStream(System.out);
        bm.printTimeStamp("SeedReader getData recovery time, ");
        if (status <= 0 ) {
          String wfsrcStr = "";
          if (wf.getWaveSourceType() == AbstractWaveform.LoadFromDataSource) {
              if (wf.filesAreLocal()) { // timeseries source type 1
                wfsrcStr = "from local files";
              }
              else {
                wfsrcStr = "from DataSource path: " + wf.getPathFilename() + " for Wcopy: " + wf.getWcopy(); 
              }
          }
          else if (wf.getWaveSourceType() == AbstractWaveform.LoadFromWaveServer) {
              wfsrcStr = "from waveserver";
          }
          System.out.println("SeedReader.getData status: " + status + " no timeseries data " + wfsrcStr + " for " +
                  wf.getChannelObj().toDelimitedSeedNameString(".") + " " + new TimeSpan(startTime, endTime));
        }
      }

      return status;

    }

    //** Get timeseries from a wave server */
    public static int getDataFromWaveServer(Waveform waveform) {

      WaveServerSource waveClient = (WaveServerSource) waveform.getWaveSource();

      //       System.out.println("SeedReader: waveClient ="+waveClient);

      try {

        // Get the time-series
//        waveClient.setTruncatetAtTimeGap(false);

        // not sure this is always true
        waveform.setAmpUnits(Units.COUNTS);

        //return waveClient.getJasiWaveformDataRaw(waveform);
        int status = waveClient.getTimeSeries(waveform);
        if (status == TrinetReturnCodes.TN_SUCCESS) return waveform.getSegmentCount();

      }
      catch (Exception ex) {
        System.err.println(ex.toString());
        ex.printStackTrace();
      }
      finally {

      }

      return 0;
    }


/**
 * Return a ChannelableList of Waveform objects extracted from a Local or NFS mounted file
 * containing Seed format.  Returns null on fatal error. */

public static ChannelableList getDataFromFile(String filename, int traceoff) {

    ChannelableList wfList = new ChannelableList();
    Waveform wf = null;

    FileInputStream seedStream;
    File file = new File(filename);

    long filesize = file.length();
    filesize -= traceoff;

    //System.out.println("SeedReader opening "+file+ "  "+traceoff+"  "+nbytes);

    // open the Seed File
    try {
      seedStream = new FileInputStream(file);
    } catch (FileNotFoundException exc) {
      System.err.println("File not found: " + filename);
      return null;
    }
    // Java7 warns as unreachable:
    //catch (IOException exc) {
    //  System.err.println("IO error: " + exc.toString());
    //  return null;
    //}

    BufferedInputStream inbuff =
        new BufferedInputStream(seedStream, READBUFFERSIZE);

    int buffOffset = 0;
    int dataSize = 0;
    int totalBytes = 0;
    int lastChannelStart = 0;

    // skip the proper byte offset in the file

    if (traceoff > 0) {
      try {
        long bytesSkippped = inbuff.skip(traceoff);

        // catch non-exception problems
        if (bytesSkippped != traceoff) {
            System.err.println("IO error: could only skip "+bytesSkippped+
              " wanted to skip " + traceoff);
            return null;
        }
      } catch (IOException exc) {
          System.err.println("IO error skipping to data offset: " +
          exc.toString());
          return null;
      }
    }

    // file read loop; while data is there and we haven't yet gotten all the bytes
    WFSegment wfseg = null;
    byte[] buff = new byte[DEFAULTSEEDBLOCKSIZE];
    SeedHeader hdr = null;
    try {
       while (inbuff.available() > 0) {
        
          buffOffset = 0;
          /*
           * NOTE: Seed packets can be of various sizes, although the size must be a power of 2.
           * You don't know what the size is until you read the header of the packet.
           */
          // <1> read the header
        
          //NOTE: DANGER HERE: THIS ASSUMES THE HEADER IS TOTALLY CONTAINED WITHIN THE
          //      FIRST 64-BYTE FRAME. HEADERS MAY EXTEND INTO SUBSEQUENT FRAMES IF THE
          //      BLOCKETTES FILL THE FIRST ONE (E.G. THE "V" HEADERS ARE > 64 BYTES)
          totalBytes +=
              inbuff.read(buff, buffOffset, FrameSize);        // read one Seed header (64 bytes)
        
          lastChannelStart =  totalBytes - FrameSize;   // beginning of a channel's data
        
          hdr = SeedHeader.parseSeedHeader(buff);
        
          if (hdr == null) {
             System.err.println("Skipping malformed SEED header");
             continue;  // continue while loop to read next frame
          } else if (!hdr.isData()) {
             System.err.println("Skipping non-data header of type "+ hdr.headerType);
             continue;  // continue while loop to read next frame
          }
        
          wfseg = hdr.createWFsegment();
          if (wfseg == null) return null;   // bad data
        
        // <2> read the data part now that we know how big it is (=blockSize)
        
          dataSize = hdr.seedBlockSize - FrameSize;
        
          buffOffset = FrameSize;            // start storing bytes in buffer at # 64
        
          totalBytes +=
              inbuff.read(buff, buffOffset, dataSize);        // append Seed data frames
        
          if ( hdr.isData() )        // a data "record"
          {
        
               // decompress the data into the WFSegment
            wfseg.setTimeSeries( decompress(hdr, buff) );
        
             // is it a channel we already have in our list
             wf = (Waveform) wfList.getByChannel(wfseg.getChannelObj());
        
             if (wf == null) {            // not in list :. a new channel/waveform
                wf = AbstractWaveform.createDefaultWaveform();
        
                // not sure this is always true
                wf.setAmpUnits(Units.COUNTS);
                wf.setFileOffset(lastChannelStart);
                wf.setFilename(filename);
                wf.setFileOffset(traceoff);
                wf.setChannelObj(wfseg.getChannelObj());
                wf.setFormat(JasiTimeSeries.SEED_FORMAT);
                wf.setSampleRate(1.0/wfseg.getSampleInterval());
                wf.setEncoding(hdr.encodingFormat);
                wf.setStart(wfseg.getStart());
            //           wf.id = evid;   // no meaningfull file!
        
                wfList.add(wf);
             }
               // add segment to the Waveform
            wf.getSegmentList().add(wfseg);
            wf.setEnd(wfseg.getEnd());       // push back end time
        
          } else {        // skip non-data blocks
            // noop
          }
        
          // progress
          System.out.print("\rRead: " +(int) (((double)totalBytes/(double)filesize) * 100.0)+"%  "+
                               totalBytes+ "  / "+ filesize);
       } // end of while loop
       System.out.println("");
       inbuff.close();
       seedStream.close();

    } catch (IOException exc) {
      System.err.println("IO error: " + exc.toString());
      exc.printStackTrace();
    } catch (Exception exc) {
      System.err.println("General exception: " + exc.toString());
      exc.printStackTrace();
    }

    // collapse multiple segements into minimum possible
    Waveform wfa[] = (Waveform[]) wfList.toArray(new Waveform[0]);
    for (int i = 0; i < wfa.length; i++) {
      if (wfa[i] != null ) {
        wfa[i].collapse();
        // set max, min, bias values
        wfa[i].scanForAmps();
        wfa[i].scanForBias();
      }
    }

    return wfList;

}

/**
 * Read a Local or NFS mounted file containing Seed format as described in a
 * waveform object. Populates the Waveform's segVector vector with WFSegments
 * from the file. Returns the number of WFSegments in the vector.  */

    // NOTE: we pass the FileNotFoundException upward to the caller
public static int getDataLocal(Waveform waveform)
//    throws FileNotFoundException
{
    Waveform wf = waveform;

    // not sure this is always true
    wf.setAmpUnits(Units.COUNTS);

    // build file name. Path comes from 'algorithm'
    String file  = wf.getPathFilename();
    int traceoff = wf.getFileOffset();
    int nbytes   = wf.getFileByteCount();

    FileInputStream seedStream;

    // open the Seed file
    try {
      seedStream = new FileInputStream(file);
    } catch (FileNotFoundException exc) {
      System.err.println("File not found: " + file);
      return 0;
    }
    // Java7 warns as unreachable:
    //catch (IOException exc) {
    //  System.err.println("IO error: " + exc.toString());
    //  return 0;
    //}

    BufferedInputStream inbuff =
      new BufferedInputStream(seedStream, READBUFFERSIZE);

    int buffOffset = 0;
    int dataSize = 0;
    int totalBytes = 0;

    // skip the proper byte offset in the file
    if (traceoff > 0) {
      try
      {
        long bytesSkippped = inbuff.skip(traceoff);

        // catch non-exception problems
        if (bytesSkippped != traceoff) {
            System.err.println("IO error: could only skip "+bytesSkippped+
              " wanted to skip " + traceoff);
            return 0;
        }
      }
      catch (IOException exc)
      {
        System.err.println("IO error skipping to data offset: " +
            exc.toString());
        return 0;
      }
    }


// file read loop; while data is there and we haven't yet gotten all the bytes
    WFSegment wfseg = null;
    byte[] buff = new byte[DEFAULTSEEDBLOCKSIZE];
    SeedHeader hdr = null;
    try {
      while (inbuff.available() > 0 && totalBytes < nbytes) {

        buffOffset = 0;

/*
 * NOTE: Seed packets can be of various sizes, although the size must be a power of 2.
 * You don't know what the size is until you read the header of the packet.
 */
// <1> read the header
       totalBytes +=
          inbuff.read(buff, buffOffset, FrameSize);        // read one Seed header

       hdr = SeedHeader.parseSeedHeader(buff);
       wfseg = hdr.createWFsegment();

       //System.out.println("Parsed SEED header: " +
       //     headerType+"\n"+ wfseg.dumpToString() );

// <2> read the data part now that we know how big it is (=blockSize)

      dataSize = hdr.seedBlockSize - FrameSize;

      buffOffset = FrameSize;            // start storing bytes in buffer at # 64

      totalBytes +=
          inbuff.read(buff, buffOffset, dataSize);        // append Seed data frames

      if (hdr.isData())        // a data "record"
      {

        // decompress the data into the WFSegment

        wfseg.setTimeSeries( decompress(hdr, buff) );

        //System.out.println(" Samples count = " + wfseg.sample.length);

        wf.getSegmentList().add(wfseg);

       } else {
            // noop
       }
     } // end of while loop

      inbuff.close();
      seedStream.close();
    }
    catch (IOException exc)
    {
      System.err.println("IO error: " + exc.toString());
      exc.printStackTrace();
    }
    catch (Exception exc)
    {
      System.err.println("General exception: " + exc.toString());
      exc.printStackTrace();
    }

    return wf.getSegmentList().size();
}

  /** Get the timeseries for this waveform from the datasource. */
  public static int getDataFromDataSource(Waveform waveform) {
    if (dbBlobReader == null) dbBlobReader = new DbBlobReader();
    return dbBlobReader.getDataFromDataSource(waveform,
      waveform.getStart().doubleValue(),waveform.getEnd().doubleValue());
  }
  /** Get the timeseries for this waveform and this start/stop time from the datasource. */
  public static int getDataFromDataSource(Waveform waveform, double startTime, double endTime) {
    if (endTime < startTime) throw new IllegalArgumentException(waveform.getChannelObj().toDelimitedSeedNameString() +
            " input " + endTime + " = endTime < startTime = " + startTime );
    if (dbBlobReader == null) dbBlobReader = new DbBlobReader();
    return dbBlobReader.getDataFromDataSource(waveform, startTime, endTime);
  }

  public static void reconnectBlobReader() { // -aww 2007/01/10
      if (dbBlobReader != null) dbBlobReader.reconnect();
  }


/**
 *
 * Return a SeedHeader intepreted from this byte array.
 * Just a passthru method that calls  SeedHeader.parseSeedHeader(buff).
 */
public static SeedHeader parseSeedHeader(byte[] buff)  {
    return SeedHeader.parseSeedHeader(buff);
}

/**
 * Create a WFsegment using the information in this SEED header.
 * This method will not populate the data part of the WFsegment.
 * You must call decompress() to interpret and load timeseries.
 * Returns null if buffer did not contain a readable SEED header.
 */
public static WFSegment createWFSegment(byte[] buffer)  {
    SeedHeader hdr = SeedHeader.parseSeedHeader(buffer);
    if (debug) System.out.println(hdr.toString());
    return (hdr == null)  ? null : hdr.createWFsegment();
}

/**
 * Create a WFsegment using the information in this SEED header.
 * This method will not populate the data part of the WFsegment.
 * You must call decompress() to interpret and load timeseries.
 */
public static WFSegment createWFSegment(SeedHeader h)  {
    return h.createWFsegment();
}

/** Return last header parsed. */
public static SeedHeader getLastParsedHeader() {
    return SeedHeader.header;
}

/**
 * Decompress one SEED packet (Only handles Steim1 or 2 compression at this time)
 * Assumes you've already parsed the header. Byte-swaps if necessary. Returns
 * float[0] array on error.
 */
static float[] decompress(SeedHeader hdr, byte[] buff) {

    // determine if you must byte-swap the timeseries
    boolean swap = false;
    if (hdr.wordOrder == 0) swap = true;

    int[] samps = new int[hdr.sampleCount];

    try {
        int dataBytes = buff.length-hdr.dataOffset;
        byte[] data = new byte[dataBytes];
        System.arraycopy(buff, hdr.dataOffset, data, 0, dataBytes);

        if (hdr.encodingFormat == SeedEncodingFormat.STEIM1) {
          samps = Steim1.decode(data, hdr.sampleCount, swap);
        } else if (hdr.encodingFormat == SeedEncodingFormat.STEIM2) {
          samps = Steim2.decode(data, hdr.sampleCount, swap);
        } else {
            throw new SeedReaderException("Can't decode data format: "+hdr.encodingFormat);
        }
    } catch (Exception ex) {
        System.err.println(ex.toString());
        return new float[0];
    }

    // convert int to float
    //System.out.println("DEBUG SeedReader decompress samps.length:" + samps.length + " " + hdr.sampleCount);
    float pt[] = new float[hdr.sampleCount];
    for (int i = 0; i < hdr.sampleCount; i++) {
      pt[i] = (float) samps[i];
    }
    return pt;
}


/*
    public static void main(String args[])
    {
        Vector segVector  = new Vector();   // ref to vector of WF segments

        int evid;

        if (args.length > 0)        // translate epoch second on command-line value
        {
          Integer val = Integer.valueOf(args[0]);            // convert arg String to 'double'
          evid =  val.intValue();
        } else {
            //            evid = 9500724;            // test event
            //evid = 9749085;
            evid = 2218477;

            System.out.println("Syntax: SeedReader <evid> <mode> <dbhost> <doublestart> <doubleend>");
        }

        System.out.println("Making connection... ");
        DataSource jc = TestDataSource.create();

  // Make a persistent instance so the FTP connection can be reused

        SeedReader reader = new SeedReader();

  //        reader.setFileAccess();
        Waveform wfi = AbstractWaveform.createDefaultWaveform();

        System.out.println("evid = " +evid+ " filesAreLocal= "+ wfi.filesAreLocal());
        ArrayList wfList = (ArrayList) wfi.getBySolution(evid);

        System.out.println( "wf list length = "+ wfList.size());

        Waveform wf [] = new Waveform[wfList.size()];
        wfList.toArray(wf);
        if (wf == null)
        {
          System.out.println("No waveforms in dbase for evid "+evid);
          System.exit(0);
        }

        // local read
        int totalBytes = 0;

        int count=0;
        int status = args.length;
//        String mode = "LOCAL";
        String mode = "DB";
        if (status > 0) mode = args[1].toUpperCase();
        System.out.println("Getting waveforms source input mode: " + mode);
        double wfStartTime = 0.;
        double wfEndTime = 0.;
        double startTime = 0.;
        double endTime   = 99999999999.;
        if (args.length > 3) {
          startTime = Double.parseDouble(args[3]);
        }
        if (args.length > 4) {
          endTime = Double.parseDouble(args[4]);
        }

//        boolean verbose = false;
        boolean verbose = true;
        if (args.length > 3) {
          verbose = true;
          //verbose = Boolean.valueOf(args[5]).booleanValue();
        }

        BenchMark bm  = new BenchMark();
        BenchMark bm2 = new BenchMark();
        for (int i = 0; i<wf.length; i++)
        {
            if (verbose) System.out.print("Count =  "+ ++count + " reading... "+ wf[i].toString() );
            wfStartTime = wf[i].getStart().doubleValue();
            wfEndTime   = wf[i].getEnd().doubleValue();
            System.out.println("Requested window: " +
              LeapSeconds.trueToString(wfStartTime) + " to " + LeapSeconds.trueToString(wfEndTime) // changed aww 2008/02/04
            );
            bm.reset();
            if (mode.equals("LOCAL")) {
              status = reader.getDataLocal(wf[i]);
//            else if (mode.equals("REMOTE")) status = reader.getDataRemote(wf[i]);
//            else if (mode.equals("DB")) {
            } else if (mode.equals("REMOTE") || mode.equals("DB")) {
              //startTime = Math.min(startTime+20.,endTime);
              //endTime   = Math.max(endTime-20.,startTime);
              double sTime   = Math.max(wfStartTime,startTime);
              double eTime   = Math.min(wfEndTime, endTime);
              if (eTime > sTime) {
                if (verbose) System.out.println(" requested time window data len secs: " + (eTime - sTime)) ;
                status = reader.getDataFromDataSource(wf[i],sTime,eTime);
              }
              else if (verbose) System.out.println("** Note requested start time > end time of waveform.");
            }
            else status = getData(wf[i]);

            if (status > 0 && verbose)
              System.out.println("Read " + wf[i].getSegmentList().size() + " segments for " + wf[i].toString() );

            if (verbose) bm.printTimeStamp("BenchMark: ");

            // Dump segments
            WFSegment [] wfseg = null;
            if (args.length > 5) {
              wfseg = wf[i].getArray();
              for (int j = 0; j < wfseg.length; j++) {
                 if (verbose) System.out.println( wfseg[j].toString() );
              }
            }
            wfseg = null;
            wf[i] = null;
        }
        bm2.print("TOTAL BenchMark: ");
        bm2.reset();
    }
*/
} // end of class

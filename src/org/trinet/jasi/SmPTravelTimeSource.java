package org.trinet.jasi;
import org.trinet.util.velocitymodel.*;
import org.trinet.util.gazetteer.*;

//
// Need workout a MasterTravelTime class to store a  travel time instance
// into static member singleton, like MasterChannelList and DataSource to
// be used by applications like Jiggle, Mung etc. Note currently that the
// Waveform, WFView classes appear to have a traveltime source integrated 
// using methods with depth parameter that creates a new LatLonZ(0.,0.,depth)
// which may cause problems if it is not the same as the Solution's LatLonZ.
// Need to rework classes with traveltime to avoid potential thread conflicts
// if sharing a global source rather than using a local instance. 
//
// Do we want to implement option to turn on/off assigned input weight (quality) 
// screening for phase traveltime preference of picked vs. calculated times?
//
// Should picked SphaseTT/PSRatio take precedence over calculated PphaseTT?
//
public class SmPTravelTimeSource extends TravelTime {

    protected static final String P_TYPE = "P";
    protected static final String S_TYPE = "S";
    protected Phase defaultPhase = Phase.create();

    //protected PrimarySecondaryWaveTravelTimeGeneratorIF ttGenerator = null;

    protected Solution sourceSol = null;
    protected boolean useZeroQuality = true; // use picked 4 wt reading traveltimes
    protected boolean onlyPickedTravelTimes = true; // scan only picked waveform traces for new values


    public SmPTravelTimeSource() {
        initGenerator(null, null); 
    }
    public SmPTravelTimeSource(UniformFlatLayerVelocityModelIF vm) {
        initGenerator(vm, null); 
    }
    public SmPTravelTimeSource(UniformFlatLayerVelocityModelIF vm, Solution sol) {
        initGenerator(vm, sol.getLatLonZ()); 
    }

    /*
    protected void initGenerator(UniformFlatLayerVelocityModelIF model, Geoidal source) {
      UniformFlatLayerVelocityModelIF myModel = model; 
      if (myModel == null) { // Kludge here for default
        myModel = createDefaultModel();
      }
      if (ttGenerator == null) ttGenerator = new TTLayer(myModel);
      else ttGenerator.setModel(myModel);

      Geoidal mySource = source;
      if (mySource != null) setSource(mySource);
    }

    public UniformFlatLayerVelocityModelIF createDefaultModel() {
        UniformFlatLayerVelocityModelIF flatLayerModel = null;
        UniformFlatLayerVelocityModel.createDefaultModels();
        if (EnvironmentInfo.getNetworkCode().equals("CI")) // SCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.SOCAL_DEFAULT;
        else if (EnvironmentInfo.getNetworkCode().equals("NC")) // NCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.NOCAL_DEFAULT;
        else // UNKNOWN do we return null or?
          flatLayerModel = UniformFlatLayerVelocityModel.TWO_LAYER_DEFAULT;
        return flatLayerModel; 
    }
    */

    public void setSource(Geoidal source) {} // no-op override for now
    public void setSourceZ(double depthKm) {}  // no-op override for now

    public void setSource(Solution sol) {
        if (sourceSol != null && sourceSol == sol) return;
        sourceSol = sol;
        //ttGenerator.setSource(sourceSol); // removed to use model depth -aww 2015/10/10
        ttGenerator.setSource(sourceSol.lat.doubleValue(),sourceSol.lon.doubleValue(),sourceSol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
        if (sourceSol != null) {
          if (sourceSol.phaseList == null || sourceSol.phaseList.size() == 0) {
            // avoid event relocation and stale magnitudes by disabling list listener 
            // disable "stale" side-effect of phaseList add - aww 05/04/2005
            sourceSol.listenToPhaseListChange(false);
            sourceSol.loadPhaseList();
            //sourceSol.setStale(false); // removed, doesn't reset magnitudes 05/04/2005 aww
            // enable "stale" side-effect of phaseList add - aww 05/04/2005 
            sourceSol.listenToPhaseListChange(true);
          }
          //if (debug && sourceSol.phaseList != null) sourceSol.phaseList.dump();
        }
    }
    protected Phase setDefaultPhase(Channel chan, String type, Solution aSol) {
            defaultPhase.setChannelObj(chan);
            defaultPhase.description.set(type, "", "");
            defaultPhase.sol = aSol;
            return defaultPhase;
    }
    public double calcTravelTime(Channel chan, String type) {
        double range = chan.getHorizontalDistance();
        //double tt = (type == P_TYPE) ? ttGenerator.pTravelTime(range) : ttGenerator.sTravelTime(range);
        double tt = 0.;
        if (type.equals(P_TYPE)) {
           tt = ttGenerator.pTravelTime(range);
        }
        else {
           tt = ttGenerator.sTravelTime(range);
        }
        if (debug) System.out.println("   SmPTravelTimeSource calcTravelTime for:" + chan.toDelimitedNameString("_") +
                       " range:" + range +" "+type+" tt: " + tt +
                       " hdist: " + ((LatLonZ)ttGenerator.getSource()).horizontalDistanceFrom(chan.getLatLonZ()));
        return tt;
    }

    public double getOriginTime() {
        return (sourceSol == null) ? 0. : sourceSol.getTime();
    }
    public double getTimeOfP(Channel chan) {
        return getOriginTime() + getPTravelTime(chan);
    }
    public double getTimeOfS(Channel chan) {
        return getOriginTime() + getSTravelTime(chan);
    }
    public double getSminusP(Channel chan) {
        double pTT = getPTravelTime(chan); // may be the picked or a calculated phase time
        double sTT = getSTravelTime(chan, pTT); // may be the picked or calculated phase time
        return (sTT-pTT);
    }
    public double getTravelTime(Channel chan, String type) {
        double tt = getPhaseTravelTime(chan, type);
        return (tt > 0.) ? tt : calcTravelTime(chan, type);
    }
    public double getPTravelTime(Channel chan) {
        return getPTravelTime(chan, getSTravelTime(chan, 0.));
    }
    // Note "symmetry" with sTT method, if no picked P time,
    // min of sTT/PSRatio and the calculated model value.
    // With weight screening off, picked 4 weights trump min calculation
    public double getPTravelTime(Channel chan, double sTravelTime) {
        // See if there is a "timed" P, if so return it if weight OK
        double pPickedTravelTime = getPhaseTravelTime(chan, P_TYPE);
        // NOTE: onlyPicked condition; test for input sTT > 0. for those without pickedP but with pickedS
        if (pPickedTravelTime > 0. || (onlyPickedTravelTimes && sTravelTime <= 0.)) return pPickedTravelTime;


        // input sTravelTime is either the picked or model traveltime value
        // unless onlyPickedTravelTimes == true, then it must be the pickedSTravelTime 
        double pTravelTimeByModel = calcTravelTime(chan, P_TYPE);
        // Use Vp/Vs ratio to estimate pTT if valid sTT is input, else use model time
        double pTravelTimeByRatio = (sTravelTime > 0.) ?
          sTravelTime/((UniformFlatLayerVelocityModel)ttGenerator.getModel()).getPSRatio() : pTravelTimeByModel;
        if (onlyPickedTravelTimes) return pTravelTimeByRatio;
        // Use min of two to insure S-P time is positive, they're equal when input sTT=0.;
        return Math.min(pTravelTimeByRatio, pTravelTimeByModel);
    }
    public double getSTravelTime(Channel chan) {
        return getSTravelTime(chan, getPTravelTime(chan, 0.));
    }
    // Note "symmetry" with pTT method, if no picked S time,
    // max of pTT*PSRatio and the calculated model value.
    // With weight screening off, picked 4 weights trump max calculation
    public double getSTravelTime(Channel chan, double pTravelTime) {
        // See if there is a "timed" S, if so return it if weight OK
        double sPickedTravelTime = getPhaseTravelTime(chan, S_TYPE);
        // NOTE: onlyPicked condition; test for input pTT > 0. for those without pickedS but with pickedP
        if (sPickedTravelTime > 0. || (onlyPickedTravelTimes && pTravelTime <= 0.)) return sPickedTravelTime;

        // input pTravelTime is either the picked or model calculated traveltime value
        // unless onlyPickedTravelTimes == true, then it must be the pickedPTravelTime 
        double sTravelTimeByModel = calcTravelTime(chan, S_TYPE);
        // Use Vp/Vs ratio to estimate sTT if valid pTT is input, else use model time
        double sTravelTimeByRatio = (pTravelTime > 0.) ?
          pTravelTime*((UniformFlatLayerVelocityModel) ttGenerator.getModel()).getPSRatio() : sTravelTimeByModel;
        if (onlyPickedTravelTimes) return sTravelTimeByRatio;
        // Use max of two to insure S-P time is positive, they're equal when input pTT=0.;
        return Math.max(sTravelTimeByRatio, sTravelTimeByModel);
    }
    public double getPhaseTravelTime(Channel chan, String type) {
        double tt = 0.;
        // hasSPhaseTimed = false;
        if (sourceSol.phaseList.size() > 0) {
            Phase phase = setDefaultPhase(chan, type, sourceSol);
            phase = (Phase) sourceSol.phaseList.getSameChanType(phase);
            // Check for filtering by assigned weight, input weights 0-4 => quality 1.-0.
            if (phase != null && phase.hasValidTime()) {
              if (phase.getQuality() > 0. || useZeroQuality) { 
                tt = phase.getTime() - sourceSol.getTime(); // convert to traveltime by removing origin time
                //if (S_TYPE.equals(type)) hasSPhaseTimed = true;
                if (debug) System.out.println("    getPhaseTravelTime for picked phase "+type+" tt: " + tt);
              }
            }
        }
        return tt;
    }

    public void setUseZeroQuality(boolean tf) {
        useZeroQuality = tf; 
    }
    public void setOnlyPickedTravelTimes(boolean tf) {
        onlyPickedTravelTimes = tf; 
    }
} // end of travelTime processing class

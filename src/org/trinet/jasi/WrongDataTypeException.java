package org.trinet.jasi;
/**
 * Exception class that gets thrown when a method is passed an inapproriate
 * data object for its processing. For example:  passing a coda to a
 * to an hypocenter location class.
 *
 */
public class WrongDataTypeException extends Exception {
    public WrongDataTypeException(String msg) {
	super(msg);
    }    
} // WrongDataTypeException

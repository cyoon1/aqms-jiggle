package org.trinet.jasi;

import org.trinet.util.GenericPropertyList;
import org.trinet.util.TimeSpan;

/**
 * Create CTWs in the absence of a Solution or trigger info in the dbase.
 * Uses a Named List and creates requests of ALL channels in the list.
 */
/**
 * NOTES:
 * 
 * We do NOT read channel list from a cache because we have not idea what
 * time period is going to be requested here and the cache may not be
 * correct for that epoch.
 */
public class NewTriggerChannelTimeWindowModel extends NamedChannelTimeWindowModel {
    
    public static final String defModelName = "NewTrigger";

    /** A string with an brief explanation of the model. For help and tooltips. */
    public static final String defExplanation = 
          "All channels in named list with static start and top times";
    
    public NewTriggerChannelTimeWindowModel() {
        super();
        setModelName(defModelName);
    }

    public NewTriggerChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList channelSet) {
        super(gpl, sol, channelSet);
        if (getFixedTimeWindow() == null) setFixedTimeWindow(sol); 
        setModelName(defModelName);
    }

    public NewTriggerChannelTimeWindowModel(Solution sol, ChannelableList channelSet) {
        super(sol, channelSet);
        if (getFixedTimeWindow() == null) setFixedTimeWindow(sol); 
        setModelName(defModelName);
    }

    public NewTriggerChannelTimeWindowModel(ChannelableList channelSet) {
        super(channelSet);
        setModelName(defModelName);
    }

    // method override
    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        // set any model-specific defaults here
        setRequiresLocation(false);   // default behavior
        //setPreEventSize(0.0);  // inherit superclass default
        //setPostEventSize(0.0); // inherit superclass default
        setMinWindowSize(0.0);
        setSynchListBySolution(true); 
    }

    // method override
    public ChannelableList getChannelTimeWindowList() {

        if (sol == null) {
            System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op.");
            return null;
        }
        
        // Get ALL candidate channels, loads the candidate list, if needed
        ChannelableList oldList = getCandidateList();
            
        if (oldList.isEmpty()) return oldList; // return empty List    

        int count = oldList.size();
        
        // all CTWs will be assigned the same time window
        TimeSpan ts = getFixedTimeWindow();
        
        // "ChannelTimeWindowSetList" will force inclusion of ALL channels in a set
        // when any channel is added in the loop
//            ChannelableList outList = (getIncludeAllComponents()) ?
//                  new ChannelTimeWindowSetList(count) : new ChannelableList(count);
        // There's a bug in ChannelTimeWindowSetList() - it does NOT
        // prevent duplicates by SNCL, it only disallows adding the same
        // exact Object to the ArrayList twice. So if you add a triaxial you
        // get 3-copies of each!
        ChannelableList outList = new ChannelableList(count);
                          
        Channelable ch = null;
        count = oldList.size();
        for (int i = 0; i<count; i++) {
            ch = (Channelable) oldList.get(i);
            outList.add( new ChannelTimeWindow(ch.getChannelObj(), ts) );
        }

        // apply any include/exclude or other list criteria
        outList = filterListByChannelPropertyAttributes(outList);

        return outList;
    }
      
    // method override
    /** Set the Solution to use in the model.
    *  Just in case the timeWindow hasn't been set do so with Solution info.
    *  Start = Solution OT - preevent, duration is MaxWindowSize
    */
    public void setSolution(Solution sol) {
        super.setSolution(sol);
        if (getFixedTimeWindow() == null) setFixedTimeWindow(sol); 
    }

    private void setFixedTimeWindow(Solution sol) { 
        // set time window with solution info if it wasn't otherwise set
        double start = sol.getTime() - this.getPreEventSize();
        // could use maxWindow here but it may be too long (10 minutes)
        double stop  = start + getMaxWindowSize();   // could be extreme
        setFixedTimeWindow(new TimeSpan(start, stop));
    }
}

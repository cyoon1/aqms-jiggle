package org.trinet.jasi;
import java.util.*;
import java.sql.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
 * A generic Coda object.
 */
abstract public class Coda extends MagnitudeAssocJasiReading {

/** Describes the coda type, duration type, phase characteristics.  */
     public CodaPhaseDescriptor descriptor = CodaPhaseDescriptor.create();                // maps to TN schema Coda.codaType, Coda.durType, Coda.iphase

/** Coda duration calculated from P-wave onset  (e.g. use q,a fitting 60mv cutoff ampi). */
    public DataDouble tau = new DataDouble();

/** Coda termination time  - used to calculate tau as tau = tCodaTerm - tPPhase */
    public DataDouble tCodaTerm = new DataDouble();

/** Coda log(amp)-log(tau) line fit parameter when slope (q) is fixed and tau = 1. */
    public DataDouble aFix = new DataDouble();

/** Coda line slope parameter determined by best fit of known calibration set of
    earthquake qFree data for known magnitudes. */
    public DataDouble qFix = new DataDouble();

/** Coda log(amp)-log(tau) line fit parameter when slope (q) is free and tau =
    1. */
    public DataDouble aFree = new DataDouble();

/** Coda line slope parameter determined by least absolute residual line fit of
    single events windowed time amp data. */
    public DataDouble qFree = new DataDouble();

/** Weight assigned by the operator or the waveform analysis algorithm.
* Estimate can be used to assign weights in summary mangnitude calculations.
* Value should be between 0.0 and 1.0.
*/
    public DataDouble  quality = new DataDouble(); // maps to TN schema Coda.quality and AssocCoM.in_wgt

/** Mininum residual difference from calculated fit of the observed amplitude time window data. */
//    public DataDouble  residual = new DataDouble();       // maps to TN schema Coda.rms

/** Size of the time window used for averaging amplitude, in seconds. */
    public DataDouble  windowSize = new DataDouble();     // derived from algorithm; NEED to map to a Coda table attribute called WINDOW

// Actual or estimated P-wave arrival time at station.
// protected DataDouble datetimeP = this.dateTime;        // aliased, calc or join AssocArO and Arrival where Phase=P; NEED to map to a Coda table attribute called DATETIME

/** Actual or estimated seconds difference between the S-arrival and P-arrival. */
    public DataDouble timeS = new DataDouble();     // calc or join AssocArO and Arrival where Phase = S

/** Algorithm name used to calculate derived coda values to be cached/stored. */
    public DataString algorithm = new DataString(); // NEED to map to a Coda table attribute called ALGORITHM

/** Collection of time and amplitude value pairs for time window. */
    protected ArrayList windowTimeAmpPairs = new ArrayList();  //  time and average rectified amp of window used in calculation
// Window start times measured relative to start of coda.
//    protected ArrayList windowTimes = new ArrayList();  //  time of window maps to Trinet schema Coda.time1 etc.
// Amplitude values associated with corresponding window times.
//    protected ArrayList windowAmps = new ArrayList();  //  average rectified amp at associated windowTime maps to Coda.amp1 etc.

/** Number of sample windows used to calculate the coda fit parameters. */
    public DataLong windowCount = new DataLong();   // maps to Coda.nsample in TN schema

/** Units of amplitude */
    public DataString ampUnits = new DataString();  // maps to Coda.units in TN schema

/** Estimated error of window amplitude in the same units as Amplitude itself. */
    public DataDouble uncertainty = new DataDouble();  // maps to Coda.eramp in TN schema

 // ChannelMag maps to AssocCoM table in TN schema
 // NEED to map channelMag.value to AssocCoM attribute called MAG
 // NEED to map channelMag.residual to AssocCoM attribute called MAGRES
 // NEED to map channelMag.correction to AssocCoM attribute called MAGCORR
 // NEED to map channelMag.inWgt to AssocCoM attribute called IN_WGT
 // NEED to map channelMag.weight to AssocCoM attribute called WEIGHT

    public Object clone() {
        Coda cd          = (Coda) super.clone();
        cd.tau           = (DataDouble) this.tau.clone();
        cd.tCodaTerm     = (DataDouble) this.tCodaTerm.clone();
        cd.aFix          = (DataDouble) this.aFix.clone();
        cd.qFix          = (DataDouble) this.qFix.clone();
        cd.aFree         = (DataDouble) this.aFree.clone();
        cd.qFree         = (DataDouble) this.qFree.clone();
        cd.quality       = (DataDouble) this.quality.clone(); 
        cd.windowSize    = (DataDouble) this.windowSize.clone();
        cd.timeS         = (DataDouble) this.timeS.clone();
        cd.algorithm     = (DataString) this.algorithm.clone();
        cd.windowCount   = (DataLong)   this.windowCount.clone();
        cd.ampUnits      = (DataString) this.ampUnits.clone();
        cd.uncertainty   = (DataDouble) this.uncertainty.clone();
        cd.windowTimeAmpPairs = (ArrayList) this.windowTimeAmpPairs.clone();
        int count = cd.windowTimeAmpPairs.size();
        for (int idx =0; idx < count; idx++) {
           TimeAmp ta = (TimeAmp) ((TimeAmp) cd.windowTimeAmpPairs.get(idx)).clone();
           cd.windowTimeAmpPairs.set(idx,ta);
        }
        return cd;
    }

/**
* Default constructor is protected, so you must instantiate an instance with create().
* @see #create()
* @see #create(int)
* @see #create(String)
*/
    protected Coda() {} // protected access forces general users to use create() method.

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final Coda create() {
        return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final Coda create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final Coda create(String suffix) {
        return (Coda) JasiObject.newInstance("org.trinet.jasi.Coda", suffix);
     }
// ////////////////////////////////////////////////////////////////////////////

/** Returns true if input is Coda and descriptions match
* @See #isLikeChanType()
*/
    public boolean isSameType(JasiReadingIF jr) {
      return (jr instanceof Coda) ?
         descriptor.isSameType(((Coda)jr).descriptor) : false;
    }

/** Set the coda type, a or d (amplitude or duration).
*/
    public void setDurationType(CodaDurationType type) {
        descriptor.setDurationType(type);
    }
    public CodaDurationType getDurationType() {
        return descriptor.getDurationType();
    }
    public boolean isMcType() {
        return (descriptor.getDurationType() == CodaDurationType.A);
    }
    public boolean isMdType() {
        CodaDurationType dt = descriptor.getDurationType();
        return  (dt == CodaDurationType.D || dt == CodaDurationType.H);
    }

/** Set the coda type, P or S (USGS or UCB).
*   @See: CodaType
*/
    public void setCodaType(CodaType type) {
        descriptor.setCodaType(type);
    }

/**
* Sets the coda phase String description. Typically the letter  S or P
* concatenated with positional letter codes indicating clipped, noisy, short or truncated coda.
* The actual defined coda description characteristics are implementation dependent.
*/
    public void setPhaseDescription(String str) {
        descriptor.setDescription(str);
    }

    public String getPhaseDescription() {
        return descriptor.getDescription();
    }

/**
* Sets the coda phase descriptor, implementation dependent on Coda type.
*/
    abstract public void changeDescriptor(CodaPhaseDescriptor pd) ;

    abstract public boolean copySolutionDependentData(Coda newCoda);
    public boolean copySolutionDependentData(JasiReadingIF jr) {
        return copySolutionDependentData((Coda)jr);
    }
    abstract public boolean replace(Coda newCoda);
    public boolean replace(JasiReadingIF jr) {
       return replace((Coda) jr);
    }


    // method for JasiCommitableIF 
    public Object getTypeQualifier() { 
      return descriptor.getTypeString();
    }

    /** Return the reading quality, relates to the weight value to apply in
     * statistical calculations, should be set once by its derivation algorithm.
     *  Range is 0.0 to 1.0.
     *  */
    public double getQuality() {
      // What should the "default" value be for null, unknown value?
      if (quality == null || quality.isNull()) return 0.0; // should it be 1.0?
      return quality.doubleValue();
    }
    public void setQuality(double value) { 
      quality.setValue(value);
    }
    public double getTau() {
      return ( tau.isNull() ? Double.NaN : tau.doubleValue() );
    }
    public double getAFix() {
      return ( aFix.isNull() ? Double.NaN : aFix.doubleValue() );
    }
    public double [] getWindowTimes() {
        TimeAmp [] ta = (TimeAmp []) windowTimeAmpPairs.toArray(new TimeAmp [windowTimeAmpPairs.size()]);
        int size = ta.length;
        if (size == 0) return null;
        double [] retVal = new double [size];
        for (int idx = 0; idx < size; idx++) {
           retVal[idx] = ta[idx].getTimeValue();
        }
        return retVal;
    }

    public double [] getWindowAmps() {
        TimeAmp [] ta = (TimeAmp []) windowTimeAmpPairs.toArray(new TimeAmp [windowTimeAmpPairs.size()]);
        int size = ta.length;
        if (size == 0) return null;
        double [] retVal = new double [size];
        for (int idx = 0; idx < size; idx++) {
           retVal[idx] = ta[idx].getAmpValue();
        }
        return retVal;
    }

    public ArrayList getWindowTimeAmpPairs() {
       return windowTimeAmpPairs;
    }

    public int toWeight(double quality) { // note not static
        int wgt = PhaseDescription.toWeight(quality);
        // changed for "reject" status -aww 2009/06/12
        return (isReject()) ? wgt+5 : wgt;
    }
    public static double toQuality(int hypoinvWt) {
        return PhaseDescription.toQuality(hypoinvWt);
    }

/**
*  Return a string describing Coda data.
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(toAssocIdString());
        sb.append("\n");
        sb.append(inParmsString());
        sb.append("\n");
        sb.append(outParmsString());
        sb.append(magParmsString());
        sb.append("\n");
        sb.append(windowDataString());
        return sb.toString();
    }

    protected String windowDataString() {
        return windowTimeString() + "\n fit" + windowAmpString();
    }

    protected String windowTimeString() {
        if (windowTimeAmpPairs == null) return "";
        int count = windowTimeAmpPairs.size();
        if ( count == 0) return "";
        StringBuffer sb = new StringBuffer(16 * count);
        sb.append(" WindowTimes: ");
        for (int idx = 0; idx < count; idx++) {
            sb.append(((TimeAmp) ((ArrayList) windowTimeAmpPairs).get(idx)).getTimeValue()).append(" ");
        }
        return sb.toString();
    }

    protected String windowAmpString() {
        if (windowTimeAmpPairs == null) return "";
        int count = windowTimeAmpPairs.size();
        if ( count == 0) return "";
        StringBuffer sb = new StringBuffer(12 * count);
        sb.append(" WindowAmps: ");
        for (int idx = 0; idx < count; idx++) {
            sb.append(((TimeAmp) ((ArrayList) windowTimeAmpPairs).get(idx)).getAmpValue()).append(" ");
        }
        return sb.toString();
    }

/**
*  Return a string describing channel waveform phase parameters
*/
    protected String inParmsString() {
        StringBuffer sb = new StringBuffer(256);
        //sb.append(" dist: ").append(getDistance()); // don't use slant aww 06/11/2004
        sb.append(" dist: ").append(getHorizontalDistance()); // aww 06/11/2004
        sb.append(" az: ").append(getAzimuth());
        sb.append(" PTime: ").append(getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.fff")); // instead aww 2015/11/20
        sb.append(" S-P: ").append(timeS);
        sb.append("\n  ");
        sb.append(" ampU: ").append(ampUnits);
        sb.append(" ampErr: ").append(uncertainty);
        return sb.toString();
    }

/**
*  Return a string describing channel waveform coda decay fit parameters
*/
    protected String outParmsString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(" algo: ").append(algorithm.toStringSQL());
        sb.append(" winSec: ").append(windowSize.toStringSQL());
        sb.append(" winCnt: ").append(windowCount.toStringSQL());
        sb.append(" in_wgt: ").append(quality.toStringSQL());
        sb.append(" L1err: ").append(residual.toStringSQL());
        return sb.toString();
    }

/**
*  Return a string describing channel coda magnitude parameters
*/
    protected String magParmsString() {
        StringBuffer sb = new StringBuffer(256);
        if (magnitude != null) {
            sb.append("\n");
            sb.append(" NetMag: ").append(magnitude.toString());
        }
        if (! channelMag.value.isNull()) {
            sb.append("\nChlMag: ").append(channelMag.toString());
        }
        return sb.toString();
    }
    /*
      Return a fixed format string of the form:
<tt>
Nt Sta  Chn    Dist    Afix   Afree    Qfix   Qfree     tau   l1err   ChnlMag  Residual   Quality      Corr    Weight
CI MEC  EHZ   44.59    3.52    0.98    1.80   -1.08   17.46    0.12    1.7636    0.0000    0.0000   -1.7600    0.0000
</tt>
You must call getNeatStringHeader() to get the header line shown above.

@see getNeatStringHeader()
    */
    public String toNeatString() {

        Format df0 = new Format("%3d");
        Format df = new Format("%8.2f");
        Format df1 = new Format("%7.2f");
        StringBuffer sb = new StringBuffer(256);
        sb.append(getChannelObj().getChannelName().toNeatString()).append(" ");
        //sb.append(df.form(getDistance())).append(" "); // don't use slant aww 06/11/2004
        sb.append(df.form(getHorizontalDistance())).append(" "); // aww 06/11/2004
        sb.append(df0.form(Math.round(getAzimuth()))).append(" "); // aww 07/15/2010
        sb.append(df1.form(aFix.doubleValue())).append(" ");
        sb.append(df1.form(aFree.doubleValue())).append(" ");
        sb.append(df1.form(qFix.doubleValue())).append(" ");
        sb.append(df1.form(qFree.doubleValue())).append(" ");
        sb.append(df1.form(tau.doubleValue())).append(" ");
        sb.append(df1.form(residual.doubleValue())).append(" ");
        sb.append(df0.form(windowCount.intValue())).append(" ");
        sb.append(toWeight(getQuality())).append(" "); // aww 10/29/2004
        Concatenate.rightJustify(sb, getSource(), 8).append(" ");
        Concatenate.rightJustify(sb, channelMag.toNeatString(),29).append(" ");
        Concatenate.rightJustify(sb, descriptor.toNeatString(),8).append(" ");
        sb.append(getProcessingStateString());
        if (isDeleted()) sb.append(" DELETED");
        if (isReject()) sb.append(" REJECT");
        return sb.toString();
    }
    /*
      Return a fixed format header to match output from toNeatString(). Has the form:
<tt>
Nt Sta Chn    Dist    Afix   Afree    Qfix  Qfree      tau     res
xxxxxxxxxx xxxx.xx xxxx.xx xxxx.xx xxxx.xx xxxx.xx xxxx.xx xxxx.xx
</tt>

@see: toNeatString()
    */
    public static String getNeatStringHeader() {
        return ChannelName.getNeatStringHeader() +
             "     Dist  Az    Afix   Afree    Qfix   Qfree     tau   L1err cnt Q   Source" +
            ChannelMag.getNeatStringHeader() + // include channelmag
            " CodaDesc F";
    }
    public String getNeatHeader() { return getNeatStringHeader(); }
    /*
    // IF methods below may not be needed vacate lists but not null associations.
    public void removeFromAssocLists() { removeFromAssocSolList(); }
    public void removeFromAssocSolList() {
       if (sol != null) {
         // sol list removal should invoke removal from mag list
         if (sol.remove(this)) removeFromAssocMagList();
       }
    }
    public void removeFromAssocMagList() { if (magnitude != null) magnitude.remove(this); }
    */
} // end of Coda class

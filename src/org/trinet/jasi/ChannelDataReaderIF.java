package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.table.*;
public interface ChannelDataReaderIF {

    public int getCount(java.util.Date date);
    public int getCurrentCount();

    public ChannelDataIF load(ChannelIdIF id);
    public ChannelDataIF load(ChannelIdIF id, java.util.Date date) ;
    public ChannelDataIF load(Channelable chan) ;
    public ChannelDataIF load(Channelable chan, java.util.Date date) ;
    public ChannelDataIF load(ChannelDataIF cd) ;
    public ChannelDataIF load(ChannelDataIF cd, java.util.Date date) ;

    public boolean load(); // into this, the most current

    public Collection loadAll();   // all data for all dates, entire history
    public Collection loadAllCurrent() ; // active now, most current data
    public Collection loadAll(java.util.Date date) ; // data valid on specified date
    public Collection loadAll(java.util.Date date, String[] prefSeedchan) ; // as above but restricted seedchan
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) ; // as above but by net and channel also
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations); // as above by location also
}


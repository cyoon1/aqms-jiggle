package org.trinet.jasi;

/**
 * Recognized units types for waveform data calibrations.
 * Mirrors logic of jasi Units class so it could be integrated into it.
 * Possible unit types:<p>
<pre>

      "unknown",
      "c",    // counts
      "avgc", // averaged absolute value counts 
      "mag"   // magnitude units, like log10(amp ratios)
</pre>
 */
public class CalibrUnits  {

    public static final int UNKNOWN        = 0;
    public static final int MAGNITUDE      = 1;
    public static final int COUNTS         = 2;
    public static final int AVG_ABS_COUNTS = 3;

    private static String typeStr[] = {
      "unknown",
      "mag",       // magnitude
      "c",        // counts  // ? Note that Units.COUNTS is mapped to string "counts" 9/2004 -aww
      "avgc",     // averaged absolute valued counts
    };

// copy of apropo methods from Units.java:

    /** Return a string describing the amplitude type. Returns the string "unknown"
        if the type is not legal. */
    public static String getString(int type) {
        if (isLegal(type)) return typeStr[type];
        return typeStr[0];
    }

    /** Return an 'int' matching the enumeration value given a short string
    describing the amplitude type.  Returns 0 (=UNKNOWN) if no match was found. */
    public static int getInt(String type) {
        for (int i = 0; i < typeStr.length; i++) {
            if (type.equalsIgnoreCase(typeStr[i])) return i;
        }
        return 0;
    }

    /** Return true if value is within legal range of enumeration list. Note that a
     * value of 0 (=UNKNOWN) is legal.*/
    public static boolean isLegal(int type) {
        return ! (type > typeStr.length || type < 0);
    }

    /** Return true if the string is a legal units string. Is NOT case sensitive.
    * Note that a value of "UNKNOWN" is legal.*/
    public static boolean isLegal(String type) {
     if (type == null) return false;
     for (int i = 0; i < typeStr.length; i++) {
       if (type.equalsIgnoreCase(typeStr[i])) return true;
     }
     return false;
    }

    /** Return true if value is in array list. */
    private static boolean isInList (int value, int[] list) {
        for (int i = 0; i<list.length; i++) {
            if (value == list[i]) return true;
        }
        return false;
    }
}

// TODO: Extend JasiDbReader to JasiReadingDbReader to include any getBy...
// code common to PhaseTN,AmplitudeTN, and CodaTN etc.
//
package org.trinet.jasi;
import java.io.Serializable;
import java.util.*;
import java.sql.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
public class JasiDbReader implements JasiDbReaderIF, DbReadableJasiObjectIF, java.io.Serializable {

    public static final int LIKE = 0;
    public static final int NOT_LIKE = 1;
    public static boolean dbAttributionEnabled = false;

    protected static boolean debug = false;
    protected String factoryClassName = null;
    protected DbReadableJasiObjectIF jo = null;

    public JasiDbReader() { }

    public JasiDbReader(DbReadableJasiObjectIF jo) {
        setFactory(jo);
    }

    public JasiDbReader(String className) {
        setFactoryClassName(className);
    }

    public final void setDebug(boolean tf) { this.debug = tf; }

    public final void setFactory(DbReadableJasiObjectIF jo) {
        this.jo = jo;
        factoryClassName = (jo == null) ? "NULL" : jo.getClass().getName();
    }

    public final void setFactoryClassName(String className) {
      if (factoryClassName == null || ! factoryClassName.equals(className)) {
        // RECURSION WARNING: newInstance below invokes the default public no-arg constructor
        // this constructor as implemented in concrete subclasses invokes this method
        factoryClassName = className;
        jo = (DbReadableJasiObjectIF) JasiObject.newInstance(factoryClassName);
      }
    }

    public final String getFactoryClassName() {
        return factoryClassName;
    }

    public Collection getBySQL(String sql) {
      if (DataSource.getConnection() == null) {
        System.err.println ("JasiDbReader getBySQL Error: No DataSource is open.");
        return null;
      }
      return getBySQL(DataSource.getConnection(), sql);
    }

    public Collection getBySQL(Connection conn, String sql) {
        return getBySQL(conn, sql, jo);
    }

    public static Collection getBySQL(Connection conn, String sql, DbReadableJasiObjectIF jo) {
      if (jo == null) {
        System.err.println ("JasiDbReader Error - JasiObject factory is null");
        return null;
      }
      ArrayList aList = new ArrayList();
      Statement sm = null;
      ResultSet rs = null;
      boolean status = true;
      try {
        if ( conn == null || conn.isClosed() ) {
          System.err.println ("JasiDbReader getBySQL(c,s) connection c is closed");
          return null;
        }
        sm = conn.createStatement();
        // NOTE: if ExecuteSQL.setSelectForUpdate(true) was set in DataSource
        // this will lock the selected rows until commit() or close() are called
        if (debug) {
          System.out.println("DEBUG JasiDbReader rowQuery input SQL string: \n" + sql);
        }
        ResultSetDb rsdb = new ResultSetDb(ExecuteSQL.rowQuery(sm, sql));
        rs = rsdb.getResultSet();
        if (rs != null) {
          while ( rs.next() ) {
            aList.add(jo.parseData(rsdb));
          }
        }
        else status = false;
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
        status = false;
      }
      finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
      }
      return (status) ? aList : null;
    }

    /** Does not close the prepared statement, invoker must do this. */
    public Collection getBySQL(PreparedStatement ps) {
        return getBySQL(DataSource.getConnection(), ps, jo, false);
    }

    /** Does not close the prepared statement, invoker must do this. */
    public Collection getBySQL(Connection conn, PreparedStatement ps) {
        return getBySQL(conn, ps, jo, false);
    }

    /** Does not close the prepared statement, invoker must do this. */
    public static Collection getBySQL(Connection conn, PreparedStatement ps, DbReadableJasiObjectIF jo) {
        return getBySQL(conn, ps, jo, false);
    }

    /** Closes the prepared statement when closePS==true. */
    public static Collection getBySQL(Connection conn, PreparedStatement ps, DbReadableJasiObjectIF jo, boolean closePS) {
      if (jo == null) {
        System.err.println ("JasiDbReader Error - JasiObject factory is null");
        return null;
      }
      ArrayList aList = new ArrayList();
      ResultSet rs = null;
      boolean status = true;
      try {
        if ( conn == null || conn.isClosed() ) {
          System.err.println ("JasiDbReader getBySQL(c,ps) connection c is closed.");
          return null;
        }
        ResultSetDb rsdb = new ResultSetDb(ps.executeQuery());
        rs = rsdb.getResultSet();
        if (rs != null) {
          while ( rs.next() ) {
            aList.add(jo.parseData(rsdb));
          }
        }
        else status = false;
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
      }
      finally {
          try {
            if (rs != null) rs.close();
            if (ps != null && closePS) ps.close();
          }
          catch (SQLException ex) {}
      }
      return (status) ? aList : null;
    }

    public final JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return jo.parseResultSetDb(rsdb);
    }
    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb)rsdb);
    }

    public String setAttribution(Connection conn, long id, String tableName, String userId) { // return type now String, used to be int -aww 2010/08/25
        if (! dbAttributionEnabled) return null; // don't update the database attribution
        String attrib = null;
        CallableStatement cs = null;
        try {
            String sql = "{ ?=call epref.attribute(?,?,?,?) }";
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.VARCHAR);
            cs.setLong(2, id);
            cs.setString(3, tableName);
            cs.setString(4, userId);
            cs.setInt(5, 0); // changed to use alternate function signature, ie. don't commit - aww 2010/08/25
            cs.execute();
            attrib = cs.getString(1);
            if (debug)
                System.out.println("JasiDbReader setAttribution: " +
                    id + " table: " + tableName + " user: " + userId + " attrib: " + attrib);
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (cs != null) cs.close();
          }
          catch (SQLException ex) {}
        }
        return attrib;
    }
        
    public static int getCountBySQL(Connection conn, String tableName, String tableColumn, String tableValue) {
        int count = 0;
        PreparedStatement sm = null;
        ResultSet rs = null;
        String sql = "";
        try {
            if ( conn == null || conn.isClosed() ) {
              System.err.println ("JasiDbReader getCountBySQL(c,s) connection c is closed");
              return 0;
            }
            if (conn.getMetaData().getDatabaseProductName().toLowerCase().contains("postgres")) {
                sql = "SELECT COUNT(*) FROM (SELECT 1 FROM " + tableName + " WHERE " + tableColumn + "=?) AS exists";
            } else {
                sql = "SELECT COUNT(*) FROM (SELECT 1 FROM " + tableName + " WHERE " + tableColumn + "=?)";
            }
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            sm = conn.prepareStatement(sql);
            sm.setObject(1, tableValue, Types.BIGINT);
            rs = sm.executeQuery();
            if (rs != null) {
              rs.next();
              count = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        return count;
    }

    public static int getCountBySQL(Connection conn, String queryString) {
        int count = 0;
        Statement sm = null;
        ResultSet rs = null;
        try {
            if ( conn == null || conn.isClosed() ) {
              System.err.println ("JasiDbReader getCountBySQL(c,s) connection c is closed");
              return 0;
            }
            sm = conn.createStatement();
            StringBuffer sb = new StringBuffer(32+queryString.length());
            sb.append("SELECT COUNT(*) FROM ( ").append(queryString).append(" )");
            rs = org.trinet.jdbc.table.ExecuteSQL.rowQuery(sm, sb.toString());
            if (rs != null) {
              rs.next();
              count = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        return count;
    }

    /**
     * Return a list of program application names known by DataSource.
     * Empty list if none
     */
    public static java.util.List getApplicationNames(Connection conn) {
      Statement sm = null;
      ResultSet rs = null;
      ArrayList aList = new ArrayList();
      try {
        if ( conn == null || conn.isClosed() ) {
          System.err.println ("JasiDbReader getBySQL(c,s) connection c is closed");
          return null;
        }
        sm = conn.createStatement();
        // NOTE: if ExecuteSQL.setSelectForUpdate(true) was set in DataSource
        // this will lock the selected rows until commit() or close() are called
        String sql = "SELECT DISTINCT name FROM applications ORDER BY name";
        ResultSetDb rsdb = new ResultSetDb(ExecuteSQL.rowQuery(sm, sql));
        rs = rsdb.getResultSet();
        if (rs != null) {
          String name = null;
          while ( rs.next() ) {
            name = rs.getString(1);
            if (name != null) aList.add(rs.getString(1));
          }
        }
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
      }
      finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
      }
      return aList;
    }


    /**
     * Convenience utility method for building SQL query strings in a StringBuffer.
     * If 'checkForWhere' is 'true' searches StringBuffer for substring 'WHERE' and
     * if found appends ' AND ' else appends ' WHERE ' to input StringBuffer.
     * If 'checkForWhere' is 'false', appends ' AND ' to input StringBuffer.
     * Equivalent to invoking whereOrAndAppend(StringBuffer, boolean, (String)null).
     * @see #whereOrAndAppend(StringBuffer,boolean, String)
     * @returns  true.
     */
    public static boolean whereOrAndAppend(StringBuffer queryBuffer, boolean checkForWhere) {
        return whereOrAndAppend(queryBuffer, checkForWhere, (String) null);
    }
    /**
     * Convenience utility method for appending SQL query conditions to a StringBuffer.
     * If 'checkForWhere' is 'true' searches StringBuffer 'queryBuffer' for substring
     * 'WHERE' and if found appends ' AND ' else appends ' WHERE ' and a non-null
     * 'addString' to the query buffer.
     * If 'checkForWhere' is 'false', appends ' AND ' and a non-null 'addString'
     * to the query buffer.
     * Returns false as a convenience for resetting the 'check' flag for repeated
     * invocations as when concatenating in conditional code blocks like:
     * <pre>
       check = true; // predicate of SQL string in StringBuffer unknown
       String str = "auth='CI'";
       if (blah) {
         check = whereOrAndAppend(sb, check);
       } else if (duh) {
         check = whereOrAndAppend(sb, check, add);
       }
     * </pre>
     */
    public static boolean whereOrAndAppend(StringBuffer queryBuffer, boolean checkForWhere, String addString) {
      if (! checkForWhere || queryBuffer.toString().indexOf(" WHERE") >= 0 || queryBuffer.toString().indexOf(" where") >= 0 ) {
        queryBuffer.append(" AND ");
      }
      else queryBuffer.append(" WHERE ");
      if (addString != null) queryBuffer.append(addString);
      return false; // no need to recheck for 'WHERE'
    }
    public static boolean whereOrAndAppend(StringBuffer queryBuffer, boolean checkForWhere, StringBuffer addBuffer) {
      return whereOrAndAppend(queryBuffer, checkForWhere, (addBuffer == null) ? null : addBuffer.toString());
    }

    /** Convenience wrapper returns String representation of current DbReadableJasiObjectIF. 
     * @return "null" if factory is null.
     */
    public final String toNeatString() {
      return (jo == null) ? "null" : jo.toNeatString();
    }
    /** Convenience wrapper returns String representation of DbReadableJasiObjectIF String header.
     * @return empty String if factory is null.
     * */
    public final String getNeatHeader() {
      return (jo == null) ? "" : jo.getNeatHeader();
    }

    /** Returns a StringBuffer containing an SQL predicate condition string that can be appended to an SQL query.
     * @see #whereOrAndAppend(StringBuffer, boolean, StringBuffer)
     * */
    public static StringBuffer toMatchingColumnSQL(String tableName,
                    String tableColumnName, int likeOrNot, String[] expressions) {

      if ( ! (likeOrNot == LIKE || likeOrNot == NOT_LIKE)) {
        System.err.println("JasiChannelDbReader toMatchingColumnSQL invalid likeOrNot int flag:"+likeOrNot);
        return new StringBuffer("");
      }
      StringBuffer conditionBuffer = new StringBuffer(512); // init should common cases
      conditionBuffer.append("(");
      for (int i = 0; i < expressions.length; i++) {
        if (i > 0) conditionBuffer.append(" or ");
        conditionBuffer.append(tableName).append(".").append(tableColumnName);
        conditionBuffer.append((likeOrNot == LIKE) ? " like " : " not like ");
        conditionBuffer.append("'").append(expressions[i]).append("'");
      }
      conditionBuffer.append(")");
      return conditionBuffer;
    }

    /** Convenience for building SQL query strings returns a predicate condition.
     * @see #whereOrAndAppend(StringBuffer, boolean, StringBuffer)
     * */
    public static StringBuffer getLikeColumnSQL(String tableName,
                    String tableColumnName, String[] expressions) {
      return toMatchingColumnSQL(tableName, tableColumnName, LIKE, expressions);
    }
    /** Convenience for building SQL query strings returns a predicate condition.
     * @see #whereOrAndAppend(StringBuffer, boolean, StringBuffer)
     * */
    public static StringBuffer getNotLikeColumnSQL(String tableName,
                    String tableColumnName, String[] expressions) {
      return toMatchingColumnSQL(tableName, tableColumnName, NOT_LIKE, expressions);
    }
} // JasiDbReader

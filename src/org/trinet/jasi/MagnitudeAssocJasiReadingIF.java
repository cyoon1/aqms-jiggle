package org.trinet.jasi;
import java.util.*;
public interface MagnitudeAssocJasiReadingIF extends JasiReadingIF, JasiMagnitudeAssociationIF {
    public ChannelMag getChannelMag();
    public void setChannelMag(ChannelMag channelMag); // throws NullPointerException if null input

    public double getChannelMagValue();
    public void setChannelMagValue(double value);
    public boolean hasChannelMagValue();

    public boolean hasMagCorr(String type);
    public boolean hasMagCorr(java.util.Date date, String type) ;
    public boolean hasMagCorrAtReadingTime(String type) ;

    public void setChannelMagType(String type);
    public String getChannelMagType();

    public double getChannelMagCorrUsed() ;
    public void setChannelMagCorrUsed(double corr) ;

    public void setMagResidual(Magnitude mag);
    public double getMagResidual();

    public double getWeightUsed() ;
    public void setWeightUsed(double value) ;
    public double getInWgt() ;
    public void setInWgt(double value) ;

    public void initChannelMag() ;
    public void initChannelMagSummaryData() ; // wrapper around initMagDependentData()?

    public Collection getByMagnitude (Magnitude mag);
    public Collection getByMagnitude(Magnitude mag, String[] typeList); // assoc == true
    public Collection getByMagnitude(Magnitude mag, String[] typeList, boolean assoc);
}


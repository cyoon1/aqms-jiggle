// OVERLOADING VS. OVERIDDEN METHODS !
// CHECK EFFECT OF "IF"-TYPE BROADENING CLASS CASTS IN CLASS HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN SUBCLASSES?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE USED, NOT RUNTIME ARG TYPE.
//
// What is lacking is an automatic "method" or "listener" means of "mag.setStale(true)"
// for mapped Magnitudes when a "dependent" reading attribute is set to a "new" value
//
// TODO: implement copy/init mag,sol data IF logic in super,sub assoc classes - aww
// What about isSame etc re matching Magnitude (how by identity or by idEquals(), equivalent?
//
// What does "importance" in AssocAmM :wmean? Hypoinverse doesn't do it for mags.
package org.trinet.jasi;
import java.util.*;
import java.sql.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

abstract public class MagnitudeAssocJasiReading extends JasiReading implements MagnitudeAssocJasiReadingIF {

    /**
    * Network magnitude associated with this reading.
    */
    public Magnitude magnitude ;
    /**
    * ChannelMag describes one-station magnitude info for the reading:
    * its weight contribution to the summary magnitude, and difference
    * between the summary magnitude and the channel magnitude.
    */
    public ChannelMag channelMag = new ChannelMag();

/*
// HYPOINVERSE MULTI-MAG PROCESSING MODEL: 
// Potentially a reading instance for a Solution could be "shared" with multiple Magnitudes.
// Could implement a map coupling a copy of the ChannelMag object instance with a Magnitude key. 
// Magnitude methods would need to "clone()" reading's ChannelMag before associating it with a
// summary magnitude in this map.  Magnitude "commit()" operations would have to recover the
// appropriate channelMag clone from the map using the magnitude instance as the input key. 
// Could implement the map to do lookup/replacement by only by magType key, caching only
// one instance of a magnitude subtype.

    HashMap magChannelMagMap = new HashMap(11); //Need to clone this map in the clone() method 
    public ChannelMag getChannelMagFromCache(Magnitude mag) {
      return (ChannelMag) magChannelMagMap.get(mag);
    }
    // Clone a copy of current ChannelMag and map it to the default assigned Magnitude.
    public void cacheChannelMag() {
      cacheChannelMag(magnitude, (ChannelMag) channelMag.clone());
    }
    public void cacheChannelMag(Magnitude mag, ChannelMag chanMag) {
        magChannelMagMap.put(mag,chanMag);
    }
    public void uncacheChannelMag() {
      uncacheChannelMag(magnitude);
    }
    public void uncacheChannelMag(Magnitude mag) {
      magChannelMagMap.remove(mag);
    }
*/

    public Object clone() {
        MagnitudeAssocJasiReading mjr = (MagnitudeAssocJasiReading) super.clone();
        mjr.magnitude = this.magnitude; // leave the same?
        mjr.channelMag = (ChannelMag) this.channelMag.clone();
        //mjr.magChannelMagMap = this.magChannelMagMap.clone(); // if enabled uncomment
        return mjr;
    }

    // Default no argument constructor
    public MagnitudeAssocJasiReading() { }

    /** Resets data derived from the channel Magnitude calculation
    * to an initial value state.
    * Use before ChannelMag recalculation or after a virtual deletion.
    * Override in subclasses to change behavior. 
    * @see #delete()
    */
    public void initChannelMag() {
        channelMag.initialize();
    }
    /** Resets only data associated with the summary Magnitude calculation
    * to an its initial value state.
    * @see initChannelMag()
    */
    public void initChannelMagSummaryData() {
        channelMag.initAssocSummaryMagData();
    }
    /** Wrapper around a ChannelMag's summary magnitude data initialization method. 
     * @see #initChannelMagSummaryData()
     */
    public void initMagDependentData() {
        initChannelMagSummaryData();
    }
    /** Copy the ChannelMag data of the input reading to this instance
     * provided that its Channel data and datetime are similar. 
     * @see #initChannelMagSummaryData()
     * @see #isLikeChanTypeAndTime(JasiReadingIF)
     */
    public void copyMagDependentData(JasiMagnitudeAssociationIF jr) {
       MagnitudeAssocJasiReadingIF mjr = (MagnitudeAssocJasiReadingIF)jr;
       if (isLikeChanTypeAndTime(mjr)) channelMag.copy(mjr.getChannelMag());
    }

    /** Return the ChannelMag object associated with this instance.
    */
    public ChannelMag getChannelMag() {
        return channelMag;
    }
    /** Sets the ChannelMag object associated with this instance.
     * @throws  NullPointerException if input is null
    */
    public void setChannelMag(ChannelMag channelMag) {
        if (channelMag == null) throw new NullPointerException("Input ChannelMag is null");
        this.channelMag = channelMag;
    }

    /**
    * Extract all readings associated with this Magnitude from the DataSource
    * @see DataSource
    */
    abstract public Collection getByMagnitude (Magnitude mag);
    abstract public Collection getByMagnitude (Magnitude mag, String[] typeList);
    abstract public Collection getByMagnitude (Magnitude mag, String[] typeList, boolean assoc);
    /**
    * Extract all readings associated with this magnitude ID from the
    * DataSource
    * @see org.trinet.jasi.DataSource
    */

    /** Override flags this object as well as a its associated Magnitude
     * and Solution as needing a commit to update the DataSource archive.
     * Set true after a change that requires a DataSource update of this object and
     * possibly its associated Magnitude and Solution.
     * Set false after DataSource commit update.
     * */
    public void setNeedsCommit(boolean tf) {
      if (tf && magnitude != null) magnitude.setNeedsCommit(tf);
      super.setNeedsCommit(tf);
    }

    // override of parent method maps true to inWeight=0 and false to inWeight=1 
    public void setReject(boolean tf) {
        if (reject != tf && magnitude != null && ! magnitude.isStale() && magnitude.contains(this)) magnitude.setStale(true);
        reject = tf;
        setInWgt((reject) ? 0. : 1.); // synch with boolean, give full weight or none -aww 2009/09/12 
    }
    /** Simple assignment association, reading not added to magnitude collections.
     *  Does NOT unassociate previous assignment, if any.
     *  No state change. Programmer convenience.
     *  @see #associate(Magnitude)
     * */
    public void assign(Magnitude mag) {
        if (this.magnitude != null && this.magnitude != mag) { 
          try {
            if (this.magnitude.contains(this)) { // isAssociatedWithMag(); unassociate();?
              throw new IllegalArgumentException("Object already associated with magnitude:" +
                              this.magnitude.getIdentifier().toString() +
                            " use associate(Magnitude)");
            }
          }
          catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return;
          }
        }
        this.magnitude = mag;
        if (mag != null) assign(mag.getAssociatedSolution()); // force assign of Solution?
    }
    /** Returns 'true' if this instance contains a non-null Magnitude reference.
     * Does not check if assigned Magnitude contains a reference equivalent to this instance.
     */
    public boolean isAssignedToMag() { return (magnitude != null); }

    /** Returns 'true' if this instance affiliation is that of the non-null input Magnitude.
     * Does not check if assigned Magnitude contains a reference equivalent to this instance.
     * */
    public boolean isAssignedTo(Magnitude mag) { return (this.magnitude == mag); }

    /** Associate this instance with the input Magnitude object such that
     *  both containedBy(mag) and containedBy(getAssociatedSolution()) are
     *  true. Implies object becomes member of both Solution and Magnitude collections
     *  except that an input of null unassociates this object from any Magnitude.
     *  @see #unassociateMag()
    */
    public void associate(Magnitude newMag) {
        // delegate unassociate old /associate new actions to non-null Magnitude assignments
        if (newMag != null) {
          newMag.associate(this); // undelete() done by this or by subclass override?
        }
        this.magnitude = newMag;
    }
    /** Unassociate reading from its associated Magnitude, if any.
    *   @see #associate(Magnitude)
    */
    public void unassociateMag() {
        if (magnitude != null) magnitude.unassociate(this);
        this.magnitude = null;
    }

    /** Override here to set assigned Magnitude state stale, if reading is
     *  contained by it and it's not stale already.
    // This is needed only if Sol,Mag don't listen to lists for delete state change to toggle stale.
    public void setDeleteFlag(boolean tf) {
      if (deleteFlag == tf) return;
      deleteFlag = tf;
      // insurance with overhead?
      if (magnitude != null && ! magnitude.isStale() // test shortcuts contain 
          && magnitude.contains(this)) magnitude.setStale(true); 
    }
    */

    /**
     * Convenience wrapper combines unassociate() and delete().
     * Removes instance from its associated Magnitude and
     * Solution lists and flags it as deleted.
     */
    public boolean erase() {
      unassociateMag();
      return super.erase();
    }

    /** Return the Magnitude object associated with this instance. Returns null if unassociated.
    *   @see #associate(Magnitude)
    */
    public Magnitude getAssociatedMag() {
        return magnitude;
    }

    /**
     * Return true if this reading instance has been affiliated with a non-null Magnitude.
     * Default implementation: analog for isAssignedToMag().
     * Assigned Magnitude is NOT checked to contain a reference equivalent to this instance.
     * @see #associate(Magnitude)
    */
    public boolean isAssociatedWithMag() {
        return (magnitude != null); // && magnitude.contains(this));
    }

    /** Return true if this reading instance is associated with the input Magnitude object.
    *   Assigned Magnitude is NOT checked to contain a reference equivalent to this instance.
    *   @see #associate(Magnitude)
    *   @see #containedBy(Magnitude)
    */
    public boolean isAssociatedWith(Magnitude mag) {
        return (magnitude == mag) ; // && mag != null && mag.contains(this));
    }

    public boolean containedBy(Magnitude mag) {
        return (mag == null) ? false : mag.contains(this);
    }
    // should sol list synch be done here or by list listeners?
    public boolean addTo(Magnitude mag) {
        // reading must be already assigned to magnitude
        return (mag == null || this.magnitude != mag ) ? false : mag.add(this);
    }

    public boolean assocEquals(JasiMagnitudeAssociationIF obj) {
        if (obj == null) return false;
        return
            ( obj.isAssignedTo(this.magnitude) ||
              (this.magnitude != null && this.magnitude.equivalent(obj.getAssociatedMag())) ) ?
        super.assocEquals((JasiSolutionAssociationIF) obj) : false;
    }

    // Match magnitude (by identity or by idEquals(), or this.sol.equivalent(jr.sol) ?
    public boolean isSameChanType(JasiReading jr) {
        return (assocEquals((JasiMagnitudeAssociationIF) jr)) ?
          super.isSameChanType(jr) : false;
    }
    public boolean isSameChanTypeAndTime(JasiReading jr) {
      return (assocEquals((JasiMagnitudeAssociationIF) jr)) ?
        super.isSameChanTypeAndTime(jr) : false;
    }

    // should sol list synch be done here or by list listeners?
    // What about isSame etc re matching Magnitude (how by identity or by idEquals(), equivalent?
    public boolean addOrReplaceIn(Magnitude mag) {  // find like and substitute in list
        return (mag == null || this.magnitude != mag ) ? false : mag.addOrReplace(this);
    }

    /*
    // should mag reading list synch be done here or by list listeners?
    public boolean removeFrom(Solution sol) {
        boolean retVal = super.removeFrom(sol);
        // sol list removal should invoke removal of reading from mag list
        if (retVal && this.sol == sol) {
          removeFrom(this.magnitude);
        }
        return retVal;
    }
  */

    public boolean removeFrom(Magnitude mag) { // does not null magnitude assignment
      return (mag == null) ? false : mag.remove(this);
    }
    public boolean deleteFrom(Magnitude mag) { // does not null magnitude assignment
      return (mag == null) ? false : mag.delete(this);
    }

    /* aww need to test this implementation below after 03/03:
    public boolean delete() {
        return (this.magnitude == null) ? super.delete() : this.magnitude.delete(this);
    }
    public boolean undelete() {
        return (this.magnitude == null) ? super.undelete() : this.magnitude.undelete(this);
    }
    */


    /** Wrapper creates String describing Magnitude identifier associated with this instance. */
    public String toAssocIdString() {
      StringBuffer sb = new StringBuffer(256);
      sb.append(super.toAssocIdString());
      if (magnitude != null) sb.append(" magid: ").append(magnitude.magid.toStringSQL());
      return sb.toString();
    }

    /** Thin Wrapper to retrieve the value of the channel magnitude.
     * @return -9. if magnitude value is not a valid number.
     * */
    public double getChannelMagValue() {
      return channelMag.getMagValue();
    }
    /** Wrapper sets the channel magnitude value.*/
    public void setChannelMagValue(double value) {
      channelMag.setMagValue(value);
    }
    /** Wrapper sets return true if channel magnitude is set to a valid number.*/
    public boolean hasChannelMagValue() {
      return ! channelMag.isNull();
    }
    public String getChannelMagType() {
      return channelMag.getType();
    }
    /** Wrapper sets the channel magnitude type value.*/
    public void setChannelMagType(String type) {
      channelMag.setType(type);
    }
    /** Return the channel magnitude output weight contributing to a summary magnitude. */
    public double getWeightUsed() {
      return channelMag.getWeight();
    }
    /**
    * Set the channel magnitude output (used) weight value. Usually
    * the algorithm that calculates the summary magnitude sets this weight 
    * if the reading contributed to the magnitude. The value must be in the 
    * range 0.0 to 1.0
    * */
    public void setWeightUsed(double value) {
      channelMag.setWeight(value);
    }
    /** Returns the channel magnitude input weight that will contribute to
     * a summary magnitude calculation. */
    public double getInWgt() {
      return channelMag.getInWgt();
    }
    public void setInWgt(double value) {
      channelMag.setInWgt(value);
    }
    public void setMagResidual(Magnitude mag) {
      channelMag.setResidual(mag);
    }
    public double getMagResidual() {
      return channelMag.getResidual();
    }
    public double getChannelMagCorrUsed() {
      return channelMag.getCorrection();
    }
    public void setChannelMagCorrUsed(double corr) {
      channelMag.correction.setValue(corr);
    }
    
    public double getWarningLevel() {
      //return Math.min(Math.abs(getMagResidual()), 1.0);
      return Math.abs(getMagResidual());
    }

    /** Return true if Channel has the currently active magnitude correction for this type. */
    public boolean hasMagCorr(String type) {
      return hasCorrection(type);
    }

    // Return true if Channel magnitude correction exists for specified type active on the specified date. 
    public boolean hasMagCorr(java.util.Date date, String type) {
      return hasCorrection(date, type);
    }
    // Return true if Channel magnitude correction exists for specified type at reading time. 
    public boolean hasMagCorrAtReadingTime(String type) {
      return hasCorrectionAtReadingTime(type);
    }

    /*
    //Override implementation prefers the magnitude, then origin auth -aww added 08/17/2011, removed 2011/09/21
    public String getClosestAuthorityString() {

        if (magnitude == null) return super.getClosestAuthorityString();
        String eauth = magnitude.getAuthority();
        return (NullValueDb.isBlank(eauth) || eauth.equals(EnvironmentInfo.DEFAULT_NETCODE)) ?
                    super.getClosestAuthorityString() : eauth;
    }
    */

}

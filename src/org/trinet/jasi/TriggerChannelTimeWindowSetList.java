package org.trinet.jasi;

/**
 * Extends ChannelableList to enforce rules that if one component (orientation) of
 * a triaxial channelset is added to this list ALL components will be added.<p>
 * 
 * Works specifically on TriggerChannelTimeWindow objects which have a start/stop time which  
 * the "extra" added components inherit from the actual object being added.
 * 
 * For example: a call to ChannelableSetList.add(chan) where chan's channel name is
 * "NP.ABC.BHE.01" will result in three additions to the list: "BHZ", "BHN" & "BHE".<p>
 * 
 * This class may operate in one of two ways:<p>
 * 
 * First, if you provide a master list, that list will be scanned for matches to 
 * NT.STA.xx_.LC. In other words, it will return channels at the same site with
 * the same first two characters in the SEED channel name. This may result in more than
 * three components if, say, there's an up-hole and down-hole sensor: HLZ/N/E & HL1/2/3.<p>
 * 
 * If no master list is specified the Triaxial.getGroup method is called to get a 
 * pre-defined channel set. In this case, there is no guarentee that the channels 
 * generated actually exist.
 *  
 * @see:Triaxial.getGroup()
 */

public class TriggerChannelTimeWindowSetList extends ChannelableList {

  public TriggerChannelTimeWindowSetList() { }

  public TriggerChannelTimeWindowSetList(int initCapacity) {
      super(initCapacity);
  }

  public TriggerChannelTimeWindowSetList(ChannelableList masterList) {
      setMasterList(masterList);
  }
  /**
   * Add this CTW object to the list. <p>
   * 
   * Also add other channels in the channel group. 
   * (See description of groups above).
   * Will not allow addition of duplicate channels.
   * @return <i>false</i> if input is not a Channelable object.
   */
  public boolean add(Object obj, boolean notify) {

    if ( ! (obj instanceof TriggerChannelTimeWindow) ) return false;

    TriggerChannelTimeWindow ch = (TriggerChannelTimeWindow) obj;
    Channel triCh = null;
    ChannelableList triSet = null;
    TriggerChannelTimeWindow ctw = null;
    
    if (getMasterList() == null) {
	    // Create 3 components net,sta,seedchan of channelName
	    // copy station distance attributes, but not depth, gain, or corrections
	    triSet = Triaxial.getSet( ch.getChannelObj() );
    } else {
    	// look up channel set in list
    	triSet = getMasterList().findChannelGroup(ch, null);
    }

    if (triSet == null || triSet.isEmpty()) return false; // nothing to add
    
    boolean result = true;
    // Create CTW's for add all components of the Triaxial
    for (int i = 0; i < triSet.size(); i++){
      triCh = (Channel) triSet.get(i);
      ctw = new TriggerChannelTimeWindow(triCh, ch.getTimeSpan());
      ctw.setTriggerTime( ch.getTriggerTime() );
      ctw.setTriggerType( ch.getTriggerType() );

      result |= super.add( ctw, notify); // true if any comp not in list already
    }
    return result;
  }
  public boolean add(Object obj) {
    return add(obj, false);
  }

}

package org.trinet.jasi;
/**
 * Similar to a StringTokenizer for JasiReading lists. Given a JasiReading list,
 * it will return smaller lists that contain only JasiReading objects clumped by time.
 * The value set in setDelimiterGapWindow() is used to recognized breaks between
 * clumps (groups).<p>
 * @see: ChannelGrouper
 *
 * @author Doug Given
 */

public class JasiReadingTimeGrouper {

  JasiReadingList list, sublist;
  JasiReading jr;
  /** Remember type of Channelable list passed in so we can pass the same out. */
  Class listClass;

/** The size of the grouping gap time in seconds.
 * Consecutive sorted reading times of a subgroup are separated
 * by no more than this time.
 * */
  double gapDelimiterSecs = 300;     // 300sec = 5 min

  int groupsReturned = 0;
  int arraypointer = 0; // index into the input list
  // last subgroup element reading time (before 1970 is negative value)
  double dtpointer = 0.;

  public JasiReadingTimeGrouper(double gapWindowSecs) {
    setDelimiterGapWindow(gapWindowSecs);
  }

  /** */
  public JasiReadingTimeGrouper(JasiReadingList list, double gapWindowSecs) {
    setList(list);
    setDelimiterGapWindow(gapWindowSecs);
  }

  /** */
  public JasiReadingTimeGrouper(JasiReadingList list) {
    setList(list);
  }

  /** Set the list to use. */
  public void setList(JasiReadingList list) {
    this.list = list;
    listClass = list.getClass();

    // LIST MUST BE TIME SORTED TO DELINEATE GROUP BOUNDARIES!
    list.sortByTime();   // assumes youngest first

    groupsReturned = 0;
    arraypointer = 0;
    // earliest time
    dtpointer = 0.;
    if (list.size() > 0) {
      // reset to earlier time in sorted list
      dtpointer = ((JasiReading)list.get(0)).getTime();
    }
  }

  public void setDelimiterGapWindow(double seconds) {
    gapDelimiterSecs = seconds;
  }

  public double getDelimiterGapWindow() {
    return gapDelimiterSecs;
  }

/** Return true if this enumerator has more groups to return. */
  public boolean hasMoreGroups() {
    return arraypointer < list.size();
  }

/** Return a JasiReadingList containing the next group of JasiReading
 * objects that grouped by time.  Returns null if list
 * was not initiated or no groups remain.*/
  public JasiReadingList getNext()  {

    if (! hasMoreGroups() || list == null || list.isEmpty()) return null;

    // get reading at current pointer index, then bump pointer
    jr = (JasiReading) list.get(arraypointer++);

    // create list of specific type: AmpList, CodaList, PhaseList, etc.
    try {
      sublist = (JasiReadingList) Class.forName(listClass.getName()).newInstance();
    }
    catch (Exception ex) {
      System.err.println("JasiReadingTimeGrouper - Bad class type: "+listClass.getName());
    }

    sublist.add(jr); // the first member of sublist group
    // set pointer time for membership checking 
    dtpointer = jr.getTime();

    // collect the rest of the subgroup elements, if any
    while (arraypointer < list.size()) {
      jr = (JasiReading) list.get(arraypointer);
      if (! inThisGroup(jr)) break; // another group, bail

      sublist.add(jr);
      // set dtpointer to time of last reading added to list
      // grouping test is relative to this time 
      dtpointer = jr.getTime();
      arraypointer++; // bump list pointer to next list element

    }
    groupsReturned++; // bump subgroup count

    return sublist;
  }

  /** Return true if the input JasiReading object is in the same group.
   * It time is within the set time gap seconds (+/-) of the last element
   * in group.
   * */
  protected boolean inThisGroup(JasiReading jr) {
    // use the Math.abs() is so it will work with either sort order
    return ( Math.abs(jr.getTime() - dtpointer) < getDelimiterGapWindow() );
  }

/** Return total number of groups that
 * will be returned by repeated calls to getNext(). */
//  public int countGroups () {
//    return groupcount;
//  }
  /** Return remaining number of Channelable objects groups
   * will be returned by repeated calls to getNext().  */
//  public int countRemainingGroups () {
//    return countGroups() - groupsReturned;
//  }

/*
  public static class Tester {
    public static void main(String[] args) {

      System.out.println ("Making connection...");
      DataSource ds = TestDataSource.create();

      AmpList sublist;

      // read in the current station list - use cached version if available
//      long evid = 13950596;
//      long evid = 13951588;
//      Amplitude amp = Amplitude.create();
//      AmpList amplist = new AmpList(amp.getBySolution(evid));
      org.trinet.storedprocs.association.AmpAssocEngine ampEng =
        new  org.trinet.storedprocs.association.AmpAssocEngine();

      ampEng.setConnection(ds.getConnection());
      AmpList amplist = ampEng.getUnassocAmps();

      JasiReadingTimeGrouper grouper = new JasiReadingTimeGrouper(amplist, 300.0);

      System.out.println("amps = "+ amplist.size());
//      System.out.println("grps = "+grouper.countGroups());

//      amplist.dump();
      int knt = 0;
      while (grouper.hasMoreGroups()) {
        sublist = (AmpList) grouper.getNext();
        System.out.println("grp #"+ knt++);
        //System.out.println(sublist.toNeatString());
        System.out.println(sublist.dumpToString());
      }
    } // end of main
  } // end of Tester
*/
} // end of JasiReadingTimeGrouper

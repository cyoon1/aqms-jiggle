package org.trinet.jasi;

import java.util.*;

/**
 * Provides comparitor() method to sort a list of JasiReading objects by time.
 * If times are equal it sorts by net, name, component according to ChannelNameSorter().
 * @see: ChannelNameSorter*/

public class JasiReadingTimeSorter implements Comparator {

    ChannelNameSorter cnSorter = new ChannelNameSorter();

    public static final int RECENT_FIRST = 0;
    public static final int OLDEST_FIRST = 1;

    int order = RECENT_FIRST;

    /** 'ORDER' is either JasiReadingTimeSorter.RECENT_FIRST or JasiReadingTimeSorter.OLDEST_FIRST */
    JasiReadingTimeSorter (int order) {
      this.order = order;
    }

    public int compare(Object o1, Object o2) {

      if ( ! (o1 instanceof JasiReading) && (o2 instanceof JasiReading)) {
         return 0;  // wrong object types, void class cast exception
      }

      JasiReading jr1 = (JasiReading) o1;
      JasiReading jr2 = (JasiReading) o2;

      double diff = jr1.getTime() - jr2.getTime(); // checked ok -aww 2008/02/11

      if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

      if (diff < 0.0) {
        return (order == RECENT_FIRST) ? -1 : 1;
      } else if (diff > 0.0) {
        return (order == RECENT_FIRST) ? 1 : -1;
      } else { // same time sort by component type
        return cnSorter.compare( jr1.getChannelObj(), jr2.getChannelObj());
      }
    }
}



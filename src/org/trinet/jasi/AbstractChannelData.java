package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.table.*;

//Extend SourceAssocJasiObject or use AuthChannelId(Channel) for JasiSourceAssociationIF? aww
//public abstract class AbstractChannelData extends JasiObject implements ChannelDataIF, Cloneable {
public abstract class AbstractChannelData extends SourceAssocJasiObject implements ChannelDataIF, Cloneable {
    protected ChannelIdIF channelId;  // unique description of this channel
    protected DateRange   dateRange;  // time range over which data is valid

    protected AbstractChannelData() {
        this(null, null);
    }

    protected AbstractChannelData(ChannelIdIF channelId) {
        this(channelId, null);
    }

    protected AbstractChannelData(ChannelIdIF channelId, DateRange dateRange) {
        this.channelId = (channelId == null) ? new ChannelName() : channelId; // empty name if null
        this.dateRange = (dateRange == null) ? new DateRange() : dateRange; // "unbounded" date range if null
    }

    public void setChannelId(ChannelIdIF id) {
        channelId = id;
    }
    public ChannelIdIF getChannelId() {
        return channelId;
    }
    /**
     * Case insensitive compare of ChannelId identifiers.
     * Returns true if this and the input's net, sta, seedchan
     * (and location eventually)
     * are equal.
     */
    public boolean equalsChannelId(ChannelIdIF id) {
        //Case-sensitive:
        //if (this == id) return true;
        //return (id == null) ? false : channelId.equals(id); // case-sensitive comparison
        // change logic below when location is used by default in names
        if (id == null) return false;
        return (ChannelName.useLoc) ?
          (sameSeedChanAs(id) && channelId.getLocation().equalsIgnoreCase(id.getLocation())) : sameSeedChanAs(id);
    }

    /**
     * Case insensitive compare of ChannelId identifiers.
     * Returns true if this and the input's net, sta, seedchan
     * are equal.
     */
    public boolean sameSeedChanAs(ChannelIdIF id) {
        if (this == id) return true;
        if (id == null) return false;
        return (
                 channelId.getSta().equalsIgnoreCase(id.getSta()) &&
                 channelId.getSeedchan().equalsIgnoreCase(id.getSeedchan()) &&
                 channelId.getNet().equalsIgnoreCase(id.getNet())
               );
    }

    public DateRange getDateRange() {
        return dateRange;
    }
    public void setDateRange(DateRange dr) {
        dateRange = dr;
    }
    public boolean hasDateRange() {
        return dateRange.isLimited();
    }
    public boolean isValidFor(java.util.Date date) {
        return dateRange.contains(date);
    }
    public boolean isValidFor(Channelable chan, java.util.Date date) {
        return (equalsChannelId(chan) && isValidFor(date));
    }
    public boolean equalsDateRange(DateRange dr) {
        return ( dateRange.equals(dr) );
    }

    /**
    * Returns true if this instance and input have equivalent channel identifiers (case insensitive).
    * Their active date ranges and other internal data references may or may not be equivalent.
    */
    public boolean equalsChannelId(Channelable chan) {
        if (chan == null || chan.getChannelObj() == null) return false;
        return equalsChannelId(chan.getChannelObj().getChannelId());
    }

    /**
    * Returns true if this instance and input have equivalent channel identifiers (case insensitive)
    * and equivalent active date range.
    */
    public boolean equalsChannelDateRange(ChannelDataIF cd) {
        if (cd == null) return false;
        if (cd == this) return true;
        //return ( channelId.equals(cd.getChannelId()) && dateRange.equals(cd.getDateRange()) );
        //aww use the "case insensitive" version of the channel id comparison:
        return (equalsChannelId(cd.getChannelId()) && dateRange.equals(cd.getDateRange()) );
    }
    //public boolean equalsChannelDateRange(Channelable chan) {} // dates in Channel?

    /**
     * Sets the values of the data members of this instance
     * with the most current data values obtained from the default DataSource
     * that are associated with the channel identifier of this instance.
     * Returns false if no matching data is found.
     */
    public boolean load() {
        return (this.channelId == null) ? false : copy(load(this));
    }

    /**
     * Returns new instance whose data are the most recent associated with the channel identifier
     * specified by the input.
     * Returns null if no matching data is found.
     */
    public ChannelDataIF load(Channelable chan) {
        return load(chan.getChannelObj().getChannelId(), (java.util.Date) null);
    }
    public ChannelDataIF load(ChannelDataIF cd) {
        return load(cd.getChannelId(), (java.util.Date) null);
    }
    public ChannelDataIF load(ChannelIdIF id) {
        return load(id, (java.util.Date) null);
    }

    /**
    * Returns new instance whose data are associated with the channel identifier
    * on the specified Date.
    * Returns the null, if no matching data are found.
    */
    public ChannelDataIF load(Channelable chan, java.util.Date date) {
        return load(chan.getChannelObj().getChannelId(), date);
    }
    public ChannelDataIF load(ChannelDataIF cd, java.util.Date date) {
        return load(cd.getChannelId(), date);
    }
    public ChannelDataIF load(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) getByChannelId(id, date);
    }

    abstract public ChannelDataIF getByChannelId(ChannelIdIF chanId, java.util.Date date);
    abstract public boolean copy(ChannelDataIF chan);

    /**
    * Return Collection of objects that are currently valid.
    * Data is retrieved from the default data source.
    */
    public Collection loadAllCurrent() {
        return loadAll(new java.util.Date());
    }
    abstract public Collection loadAll() ; // the whole history, yow!
    abstract public Collection loadAll(java.util.Date date) ; // active on date
    abstract public Collection loadAll(java.util.Date date, String[] prefSeedchan) ; // as above but restricted seedchan
    abstract public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) ; // custom selection by date
    abstract public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations);

    /** Returns true if the input's ChannelId (ignoring case) and DateRange
     * data members are equivalent to those of this instance.
     */
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        return equalsChannelDateRange((ChannelDataIF) obj);
    }

    public int hashCode() {
        return channelId.hashCode() + dateRange.hashCode();
    }

    // need clone interface implementation
    public Object clone() {
        AbstractChannelData chanParms = null;
        chanParms = (AbstractChannelData) super.clone();
        chanParms.channelId = (ChannelIdIF) this.channelId.clone();
        chanParms.dateRange = (DateRange) this.dateRange.clone();
        return chanParms;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append((channelId == null) ? "NULL ChannelId" : channelId.toString());
        sb.append(" ");
        sb.append((dateRange == null) ? "NULL DateRange" : dateRange.toDateString("yyyy/MM/dd:HH:mm:ss")); // no fractional secs
        sb.append("  ");
        sb.append(super.toString()); // source,authority,processingState info 10/03 aww
        return sb.toString();
    }

    /** Return count of all channels in data source valid at specified time. */
    abstract public int getCount(java.util.Date date) ;
    /** Return count of all channels in data source valid at current system time. */
    public int getCurrentCount() {
        return getCount(new java.util.Date());
    }

    /** Returns String of the ChannelId name fields: net,sta,seedchan and/or location
     * delimited by the given String.
     */
    public String toDelimitedSeedNameString(String delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }
    /** Returns String of the ChannelId name fields: net,sta,seedchan and/or location
     * delimited by the input <i>char</i> delimiter.
     */
    public String toDelimitedSeedNameString(char delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }

    /** Returns String of ChannelId name fields delimited by the given String.
     * May possibly contain more channel naming elements than the SEED channel name.
     * @see #toDelimitedSeedNameString(String)
     */
    public String toDelimitedNameString(String delimiter) {
        return channelId.toDelimitedNameString(delimiter);
    }
    /** Returns String of ChannelId name fields delimited by the given char.
     * Could possibly contain more channel naming elements than the SEED channel name.
     * @see #toDelimitedSeedNameString(char)
     */
    public String toDelimitedNameString(char delimiter) {
        return channelId.toDelimitedNameString(delimiter);
    }

    public boolean sameOrientationAs(ChannelIdIF id) {
        if (channelId == null || id == null) return false;
        return (Character.toUpperCase(channelId.getSeedchan().charAt(2)) == Character.toUpperCase(id.getSeedchan().charAt(2)));
    }

    /**
     * Return true if the 3rd character of SEED channel is integer value (0-9).
    */
    public boolean isBorehole() {
        return Character.isDigit(Character.toUpperCase(channelId.getSeedchan().charAt(2)));
    }

    /**
     * Return true if the SEED channel code orientation is that of
     * a horizontal component.
     * Uses 3rd character of SEED channel name, case-insensitive,
     * returns true if equal to 'E', 'N', or or an integer whose modulus 3 value does not equal 1.
     */
    public boolean isHorizontal() {
        char orient = Character.toUpperCase(channelId.getSeedchan().charAt(2));
        if (Character.isDigit(orient)) {
          int i = Character.digit(orient,10)%3; 
          return (i != 1); 
        }
        return (orient == 'E' || orient == 'N' );
    }

    /**
     * Return true if the SEED channel code orientation is that of
     * a vertical component.
     * Uses 3rd character of SEED channel name, case-insensitive,
     * returns true if 'Z', 'V', 'U', or an integer whose modulus 3 value equals 1.
     */
    public boolean isVertical() {
        char orient = Character.toUpperCase(channelId.getSeedchan().charAt(2));
        if (Character.isDigit(orient)) {
          int i = Character.digit(orient,10)%3; 
          return (i == 1); 
        }
        return (orient == 'Z' || orient == 'V' || orient == 'U');
    }

    /** Return true if this channel is a low-gain component.
     * Uses 2nd character of SEED channel name, case-insensitive, 
     * rreturns true if equal to 'L', 'N', or 'G'
     */
    public boolean isLowGain() {
        char gain = Character.toUpperCase(channelId.getSeedchan().charAt(1));
        return (gain == 'L' || gain == 'N' || gain == 'G');
    }

    /** Return true if this channel is a high-gain component.
     * Uses 2nd character of SEED channel name, case-insensitive, 
     * returns true if equal to 'H'.
     */
    public boolean isHighGain() {
        char gain = Character.toUpperCase(channelId.getSeedchan().charAt(1));
        return (gain == 'H');
    }

    /** Return true if the channel is a broadband
     * Uses 1st character of SEED channel name, case-insensitive,
     * returns true if equal to 'B' or 'H'.
     */
    public boolean isBroadBand() {
        char band = Character.toUpperCase(channelId.getSeedchan().charAt(0));
        return (band == 'B' || band == 'H');
    }

    /** Return true if the channel is a short period.
     * Uses 1st character of SEED channel name, case-insensitive,
     * returns true if equal to 'E' or 'S'.
     */
    public boolean isShortPeriod() {
        char band = Character.toUpperCase(channelId.getSeedchan().charAt(0));
        return (band == 'E' || band == 'S');
    }

    /** Return true if the channel is a midband
     * Uses 1st character of SEED channel name, case-insensitive,
     * returns true if equal to 'M'.
     */
    public boolean isMidPeriod() {
        char band = Character.toUpperCase(channelId.getSeedchan().charAt(0));
        return (band == 'M');
    }

    /** Return true if the channel is a long period
     * Uses 1st character of SEED channel name, case-insensitive,
     * returns true if equal to 'L', 'V', 'U', or 'R'.
     */
    public boolean isLongPeriod() {
        char band = Character.toUpperCase(channelId.getSeedchan().charAt(0));
        return (band == 'L' || band == 'V' || band == 'U' || band == 'R');
    }

    /**
     * Returns true if the ChannelId of this instance and the input's have "net" attributes that are the equal.
     * Case insensitive compare.
     */
    public boolean sameNetworkAs(ChannelIdIF id) {
        if (this.channelId == id) return true;
        return (channelId.getNet().equalsIgnoreCase(id.getNet()));
    }
    /**
     * Returns true if the ChannelId of this instance and the input's have "net and sta" attributes that are the equal.
     * Case insensitive compare.
     */
    public boolean sameStationAs(ChannelIdIF id) {
        if (this.channelId == id) return true;
        return (
                 channelId.getSta().equalsIgnoreCase(id.getSta()) &&
                 channelId.getNet().equalsIgnoreCase(id.getNet())
               );
    }
    /**
     * Returns true if the channels are from the same triaxial instrument.
     * That is, the net,sta,location attributes must match and the 1st two characters
     * of the SEED channel name match. The compare is case insensitive.<p>
     * <b>NOTE:</b> borehole instruments will also be considered as the "same". 
     * For example:<code> 
        NP JAB HLZ -- 34.3109 -118.4967 -0.3630
        NP JAB HLN -- 34.3109 -118.4967 -0.3630
        NP JAB HLE -- 34.3109 -118.4967 -0.3630
        NP JAB HL1 -- 34.3109 -118.4967 -0.3630
        NP JAB HL2 -- 34.3109 -118.4967 -0.3630
        NP JAB HL3 -- 34.3109 -118.4967 -0.3630
       </code>
*/
    public boolean sameTriaxialAs(ChannelIdIF id) {
      if (this.channelId == id) return true;
      // same station?
      if (! sameStationAs(id)) return false;
      // locations must be the same
      if  (ChannelName.useLoc && ! channelId.getLocation().equalsIgnoreCase(id.getLocation())) return false; // added 03/01/2006 aww
      // triaxial check (1st 2 chars in seedname string must match)
      return channelId.getSeedchan().substring(0,2).equalsIgnoreCase( id.getSeedchan().substring(0,2) );
    }

} // end of class AbstractChannelData

package org.trinet.jasi;

import java.awt.Color;
import java.sql.*;
import java.text.*;
import java.util.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*; // for ColorList

/**
 * A list of solutions that make a catalog.  It is constrained by ranges or
 * limits to parameters like start-end time, magnitude range, region, etc. that
 * are set in an instance of EventSelectionProperties.  (Only box regional
 * selection is implemented).  Specific Solutions objects can be added or
 * removed.<p> Uses an existing JASI DataSource.
 * */

// Support for event Color coding, selection, wavform? GUI stuff, thus extended
// GenericSolutionList for the Jiggle GUI needs - aww
public class SolutionList extends GenericSolutionList
    implements JasiListSelectionIdIF {


    // the currently selected event. This is the nexus of the MVC model for
    // event selection. NOTE: id rather than Solution object is used because
    // Object can be replaced but ID is unique.
    //private static Solution selected = null;
    private long selectedId = -1l; // bug found was set static member, made instance -aww 10/24/2005

    private int maxColor = 0;

    /** Hashtable keeps track of the unique color of each solution in list */
    Hashtable colorTable = null;  // initial capacity = 11

    /** Make an empty SolutionList */
    public SolutionList() {
        super();
    }
    public SolutionList(int capacity) {
        super(capacity);
    }
    /** Create a SolutionList based on this property description */
    public SolutionList(EventSelectionProperties properties) {
        super(properties);
    }
    /** Create a SolutionList containing the Solution whose id matches the input value. */
    public SolutionList(long evid) {
        super(evid);
    }
    /** Create a SolutionList for this time window */
    public SolutionList(double start, double stop) {
        super(start, stop);
    }

    /** Copy a SolutionList */
    public SolutionList(Collection coll) {
        super(coll);
        // copy 'selected' solution if arg is a SolutionList
        if (coll instanceof SolutionList) {
          setSelected( ((SolutionList)coll).getSelected(), true);
        }
    }

    /**
     * Count the wfid entries in the wvevidassoc table for each Solution.
     * This is done seperately because its slow buy not always necessary.
     */
    public void countWaveforms() { // Why is this method in this class? aww
        int size = size();
        for (int i = 0; i < size; i++) {
            ((Solution) get(i)).countWaveforms();
        }
    }

    /**
     * Returns the currently selected event
     */
    public JasiCommitableIF getSelected() {
        int idx = getSelectedIndex();
        return (idx < 0) ? null : (JasiCommitableIF) get(idx);
    }

    // Added method to initialize selection and colors, as when list is empty
    private void init() {
        selectedId= -1l;
        maxColor = 0;
        colorTable = null;
    }

    public long getSelectedId() {
        if (isEmpty()) init();
        return selectedId;
    }

    /**
     * Returns the index of the currently selected event
     */
    public int getSelectedIndex() {
        long id = getSelectedId(); 
        //System.out.println("??? SolutionList getSelectedIndex.getSelectedId()=" + id + " getIndex(id)=" + getIndex(id) + " size:"+size());
        return (id < 0l) ? -1 : getIndex(id);
    }

    /**
     * Set the selected event.
     * Only works if the event is already in the list.
     * Returns true if the Solution is in the list and
     * was set as the selected Solution.
     * Notifies listeners.
     */
    public boolean setSelected(JasiCommitableIF sel) {
        return setSelected (sel , true);
    }

    /**
     * Set the selected event.
     * Only works if the event is already in the list.
     * Returns true if the Solution is in the list and
     * was set as the selected Solution.
     * Only notify listeners if 'notify' = true.
     */
    public boolean setSelected(JasiCommitableIF sel, boolean notify) {
        //  System.out.println ("SolList.setSelected: notify = "+notify);
        // prevent infinite loops, don't RE-select:
        if (sel == null || sel == getSelected()) return true;
        return
          setSelected(((DataLong)sel.getIdentifier()).longValue(), notify);
    }

    /**
     * Set the selected event by this id.
     * Only works if the event is already in the list.
     * Returns true if the Solution is in the list and
     * was set as the selected Solution.
     * Notify listeners.
     */
    public boolean setSelected(long id) {
        return setSelected(id, true);
    }

    /**
     * Set the selected event by this id.
     * Only works if the event is already in the list.
     * Returns true if the Solution is in the list and
     * was set as the selected Solution.
     * Only notify listeners if selected ID changes and 'notify' is true.
     */
    public boolean setSelected(long id, boolean notify) {
        // already selected
        //System.out.println("SolutionList setSelected input id: " + id + " current selected: " + getSelected() + " size:"+size());
        if (id == getSelectedId()) {
            //System.out.println("             setSelected no-op ids are equal, size:"+size());
            return true;
        }
        int idx = getIndex(id);
        //System.out.println("             setSelected getIndex(id): " + idx + " for: " + id);
        if (idx > -1) {
           selectedId = id;
           if (notify) {
             fireIntervalStateChanged(new StateChange(this, "selected", get(idx)), idx, idx);
           }
           return true;
        }
        return false;
    }

    /**
    * Returns the Solution following the selected Solution.
    * If it is the last in the list this will "wrap" and return
    * the first Solution in the list.
    * Returns null if the list is empty.
    * Same as getNext(getSelected()).
    */
    public JasiCommitableIF getNext() {
        return getNext(getSelected());
    }

    /**
    * Returns the Solution following this id (evid).
    * If the given id is the last in the
    * list this will "wrap" and return the first id in the list.
    * Returns null if the list is empty.
    * If the id is not found, the first Solution in the
    * list is returned.
    */
    public JasiCommitableIF getNext(long id) {
        if (isEmpty()) {
            init(); 
            return null;
        }
        int idx = getIndex(id);
        if (idx < 0) {
          return (Solution) get(0);
        } else if (idx >= size()-1) {   // last in list, wrap
          return (Solution) get(0);
        } else {
          return (Solution) get(idx+1);
        }
   }

    /**
     * Returns the Solution following this one.
     * Returns null if list is empty.
     * If the input Solution is the last in the
     * list, or the Solution is not found, the first Solution
     * in the list is returned.
     */
    public JasiCommitableIF getNext(JasiCommitableIF sol) {
        long id = -1; // this will cause 1st to select if sol is bogus
        if (sol != null) {
          DataLong dl = (DataLong) sol.getIdentifier();
          if (dl.isValidNumber()) id = dl.longValue();
        }
        return getNext(id);
    }

    /**
     * Override sets color of added object.
     * Will not add object if its already in the list.
     * Returns 'true' if object was added.
     * If notify = false, change event will not be fired.
    */
    public boolean add(Object obj, boolean notify) {
        // must set color BEFORE firing the event,
        // else listeners get the wrong color!
        if (super.add(obj, false)) {
          setColor((Solution) obj);     //sets unique event color
          if (notify) {
            fireIntervalAdded(this.size()-1, this.size()-1, obj);
          }
          return true;
        }
        return false;
    }

    /**
    * Insert an object at specified input index.
    * Will not add object if its already in the list.
    * Override sets color of added object.
    */
    public void add(int index, Object obj)  {
        super.add(index, obj);
        setColor((Solution) obj);
    }

    /**
     * Does a virtual delete but does not removes the Solution from the list 
     * If the deleted event is the selected event, the next event
     * in the list will be selected. If there are no more events
     * in the list the selected event will become null.
     */
    public boolean delete(Object sol) {
        if (sol == null) return true; // no-op
        //look-up next sol BEFORE deletion, else can't ref deleted sol's index
        Solution inSol = (Solution) sol;
        boolean selected = (inSol.getId().longValue() == getSelectedId());
        Solution nextSol = (selected) ? (Solution) getNext(inSol) : null;
        if (super.delete(inSol)) {
          if (nextSol != null && nextSol != sol) setSelected(nextSol);
          //if (selected) selectedId = -1l; // removed 04/16/2008 -aww
          if (isEmpty()) init();
          return true;
        }
        return false;
    }

    /**
     * Removes the Solution from the list and does a virtual delete.
     * If the deleted event is the selected event, the next event
     * in the list will be selected. If there are no more events
     * in the list the selected event will become null.
     */
    public boolean erase(JasiCommitableIF sol) {
        if (sol == null) return true; // no-op
        //look-up next sol BEFORE deletion, else can't ref deleted sol's index
        Solution inSol = (Solution) sol;
        boolean selected = (inSol.getId().longValue() == getSelectedId());
        Solution nextSol = (selected) ? (Solution) getNext(inSol) : null;
        if (super.erase(inSol)) {
          if (colorTable != null && inSol != null) colorTable.remove(inSol);
          if (nextSol != null && nextSol != sol) setSelected(nextSol);
          //if (selected) selectedId = -1l; // removed 04/16/2008 -aww
          if (isEmpty()) init();
          return true;
        }
        return false;
    }

    /**
     * Removes the Solution from the list without doing a virtual delete.
     * If the removed event is the selected event, the next event
     * in the list will be selected. If there are no more events
     * in the list the selected event will become null.
     */
    public boolean remove(Object sol) {
        if (sol == null) return true; // no-op
        //look-up next sol BEFORE deletion, else can't ref deleted sol's index
        Solution inSol = (Solution) sol;
        boolean selected = (inSol.getId().longValue() == getSelectedId());
        Solution nextSol = (selected) ? (Solution) getNext(inSol) : null;
        if (super.remove(inSol)) {
          if (colorTable != null && inSol != null) colorTable.remove(inSol);
          if (nextSol != null && nextSol != sol) setSelected(nextSol);
          //if (selected) selectedId = -1l; // removed 04/16/2008 -aww
          if (isEmpty()) init();
          return true;
        }
        return false;
    }

    public Object remove(int idx) {  // override of method added 2008/04/16 -aww
        //if (idx < 0 || idx > size()-1) return null;
        boolean selected = (idx == getSelectedIndex());
        Solution nextSol = null;
        if (selected) {
          if (idx >= size()-1) {   // last in list, wrap
            nextSol = (Solution) get(0);
          } else {
            nextSol = (Solution) get(idx+1);
          }
        }
        Solution oldSol = (Solution) super.remove(idx);
        if (oldSol != null) {
          if (colorTable != null) colorTable.remove(oldSol);
          if (nextSol != null && nextSol != oldSol) setSelected(nextSol.getId().longValue());
          //if (selected) selectedId = -1l; // removed 04/16/2008 -aww
          if (isEmpty()) init();
          return oldSol;
        }
        return null;
    }

    /**
     * Returns a unique color for this solution.
     * The SolutionList keeps track of a color for each solution in the list.
     * Colors are assigned as solutions are added to the list
     * according the scheme in org.trinet.util.ColorList.
     * The colors are used by GUI's to distingush between data like
     * phases and amps associated with the solutions
     */
    public Color getColorOf(Solution sol) {
        if (sol == null) return ColorList.UNASSOC;
        if (colorTable == null) colorTable = new Hashtable();
        return (Color) colorTable.get(sol.id); //sol.id is a DataObject
    }

    /** Set the unique color for this solution.
     * The SolutionList keeps track of a color for each solution in the list.
     * Colors are assigned as solutions are added to the list according the
     * scheme in org.trinet.util.ColorList.
     * The colors are used by GUI's to distingush between data like
     * phases and amps associated with the solutions.
     * */
    public void setColor(Solution sol) {
        if (colorTable == null) colorTable = new Hashtable();
        if (colorTable.get(sol.id) == null) // add only if missing
            colorTable.put(sol.id, ColorList.getColor(maxColor++));
    }
    /** Set unique colors for for all solutions in list.
     * The SolutionList keeps track of a color for each solution in the list.
     * Colors are assigned as solutions are added to the list according the
     * scheme in org.trinet.util.ColorList.
     * The colors are used by GUI's to distingush between data like
     * phases and amps associated with the solutions.
     * */
    public void setColors() {
        int size = size();
        maxColor = 0;
        colorTable = null;
        for (int i = 0; i < size; i++) {
          setColor((Solution)get(i));
        }
    }

    /** Override removes the Solution element from list
     * rather than just doing the virtually delete the
     * super class does.
     * */
    public int stripByResidual(double val) {
        //Is removal necessary? what about mistake by user?  - aww
        //Can the GUI just hide the virtually deleted from view? - aww
        int knt = 0;
        int size = size();
        for (int i = 0; i < size; i++)  {
          Solution sol = (Solution) get(i);
          if (Math.abs(sol.getResidual()) >= val) {
            //above class override of super.delete() removes it, getNext() is selected event:
            delete((Object) sol);
            knt++;
          }
        }
        return knt;
    }

/*
  public static final class Tester {
    // Main for testing: % SolutionList [hours-back]  (default = 1)
    public static final void main (String args[]) {
        if (args.length < 1) {
            System.out.println("Syntax main args:  hostName [hrsBack, default=24]");
            System.exit(0);
        }

        System.out.println("Creating new database connection for default account on:" + args[0]);

        double hoursBack;

        if (args.length > 1) // translate epoch second on command-line value
        {
          Double val = Double.valueOf(args[1]); // convert arg String to 'double'
          hoursBack = (double) val.doubleValue();
        } else {
          System.out.println ("Usage: java SolutionList host [hours-back(default=24)]");
          hoursBack = 24; // default
        }

        // make connection
        DataSource ds = TestDataSource.create(args[0]); //  DataSource (url, driver, user, passwd);

//        catView.setLatRange (25.0, 45.0);
//        catView.setLonRange (-140.0, -110.0);

        double now = new DateTime().getTrueSeconds(); // current epoch sec (millisecs -> seconds)

        double then = now - (3600. * hoursBack); // convert to seconds

        // read in saved event properties file
        EventSelectionProperties props = new EventSelectionProperties();
        props.setFiletype("jiggle"); // test file found in the user's home "jiggle" subdir
        props.setFilename("eventSelection.props"); // name of event selection props file
        props.reset(); // read the props from file
        props.setup(); // configure local environment from props

        // set time properties
        props.setTimeSpan(then, now);

        props.setEventValidFlag(true);
        props.setOriginDummyFlag(false);

        props.setEventTypesIncluded("earthquake");
        props.setOriginGTypesIncluded(GTypeMap.LOCAL);
        props.setOriginProcTypesIncluded("automatic human");

        System.out.println ("-- Event Selection Properties --");
        props.dumpProperties();

        System.out.println ("Fetching last "+hoursBack+" hours...");

         // Get a catalogview
        SolutionList catList = new SolutionList(props);
        Solution sol[] = catList.getArray();
        //System.out.println ("found ="+sol.length);
        if (sol.length == 0) System.out.println (" * No events found.");

        for (int i = 0; i < sol.length; i++)
        {
            System.out.println (sol[i].toSummaryString() +" "+
                                sol[i].source.toString() );
        }

        // test addition by time
        if (false) { 
            Solution solx = Solution.create().getById(sol[0].id.longValue());
            System.out.println (" Test insertion of: \n"+solx.toSummaryString());
            System.out.println (" ");
            catList.addByTime(solx);
            //        catList.add(solx);
            //        catList.add(1, solx);
            Solution sol2[] = catList.getArray();
            for (int i = 0; i < sol2.length; i++) {
                System.out.println (sol2[i].toSummaryString() +" "+ sol2[i].waveRecords.toString());
            }
        }     
    }
  } // end of Tester
  */
} // end of Solution class

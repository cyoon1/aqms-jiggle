package org.trinet.pcs;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.util.*;

public abstract class AbstractSolutionProcessingController extends AbstractProcessingController
    implements JasiSolutionProcessingControllerIF {

    /** Query to get all evid from event whose origin was within specified date range */
    public static final String PCS_POST_EVENT_IDS_FOR_DATERANGE_PROCESSING =
       "SELECT E.EVID FROM EVENT E, ORIGIN O WHERE E.PREFOR=O.ORID AND O.DATETIME " +
       "BETWEEN TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC')"; // for UTC -aww 2008/02/08

    /** Query to check for specified evid in event table */
    public static final String PCS_CHECK_EVENT_ID_SQL_QUERY = "SELECT EVID FROM EVENT WHERE EVID = ?";

    /** Event locks enabled */
    protected boolean enableLocking = true;

    /** Min event age in seconds since origin time before processed (i.e. sleep until aged) **/
    protected double minAgeSecs = 0.;

    /** Max event age in seconds eligible for processing **/
    protected double maxAgeDays = Double.MAX_VALUE;

    /** Commit processed data to data store */
    protected boolean autoCommit = true;

    /** If set, specifies a concatenated list of allowed origin.RFLAG chars, e.g. AHIF **/
    protected String pcsAllowedRflagStates = null; 

    /** A JasiSolutionProcessorIF "wrapper" around a class instance that does event processing, e.g HypomagSolutionProcessor. */
    protected JasiSolutionProcessorIF module = null; 

    /** A solutionlock object that matches the current DataSource. Some data sources
     * may not support event locking. This can be checked with boolean result of
     * solLock.isSupported()  */
    protected static SolutionLock solLock = null;

    // Every instance initialize:
    {
        this.pcsCheckIdSQLString = PCS_CHECK_EVENT_ID_SQL_QUERY;
        this.pcsGetPostIdsForDateRangeSQLString =  PCS_POST_EVENT_IDS_FOR_DATERANGE_PROCESSING;
    }

    protected AbstractSolutionProcessingController() {}

    protected AbstractSolutionProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, DEFAULT_THREAD_SLEEP_MILLIS,  null);
    }
    protected AbstractSolutionProcessingController(String appName, String groupName, String threadName, String stateName,
                                           int defaultSleepTimeMillis) {
        this(appName, groupName, threadName, stateName, defaultSleepTimeMillis, null);
    }
    protected AbstractSolutionProcessingController(String appName, String groupName, String threadName, String stateName,
                                                 int defaultSleepTimeMillis, JasiSolutionProcessorIF module) {
        super(appName, groupName, threadName, stateName, defaultSleepTimeMillis);
        this.module = module;
    }

    /** Set status of locking of event to be processed in JASIEVENTLOCK */
    public void setEnableLocking(boolean tf) {
        enableLocking = tf;
        if (!enableLocking) releaseAllSolutionLocks(); // is this needed here?
    }

    /** Are events to be processed locked in JASIEVENTLOCK */
    public boolean isLockingEnabled() {
        return enableLocking;
    }

    protected boolean initFromProperties() {

        if (!super.initFromProperties()) return false;

        boolean status = true;

        // processor module class 
        if (props.isSpecified("pcsModuleClassName")) {
          module =
              (JasiSolutionProcessorIF) props.getInstanceDefinedByProperty("pcsModuleClassName");
          if (module == null) {
            System.err.println(
              "ERROR: " + getClass().getName() +
               " Failed initialize from property, unable create instance of JasiSolutionProcessorIF : "
               + props.getProperty("pcsModuleClassName")
            );
          }
          status = false;
        }
        if (module != null) { // set its configuration, like mode
            // default to controller name, may be "unknown", and overriden below if property "pcsProcessorAppName" is defined in props below
            //module.setAppName(this.appName); // assume this is done by setProcessingController:
            module.setProcessingController(this);
            //if (props.isSpecified("pcsProcessorMode")) 
              //setProcessingModuleMode(props.getInt("pcsProcessorMode"));
            module.setProperties(props); // Note props must be SolutionWfEditor subclass to use scanNoiseType and channelTimeWindowModel props
        }

        // override default locking
        if (props.isSpecified("pcsSolutionLocking"))
          enableLocking = props.getBoolean("pcsSolutionLocking");

        if (props.isSpecified("pcsMinAgeSecs"))
          minAgeSecs = props.getDouble("pcsMinAgeSecs", 0.);

        if (props.isSpecified("pcsMaxAgeDays"))
          maxAgeDays = props.getDouble("pcsMaxAgeDays", Double.MAX_VALUE);

        pcsAllowedRflagStates = props.getProperty("pcsAllowedRflagStates"); // null means no restrictions

        // override default autoCommit 
        if (props.isSpecified("pcsAutoCommit"))
          autoCommit = props.getBoolean("pcsAutoCommit");

        if (verbose) {
            if (autoCommit) {
              // Notify user about db processing configuration
              System.out.println(">>>  Db autoCommit ON <<<");
            }
            else {
              System.out.println(">>>  Db autoCommit OFF <<<");
            }
        }

        return status;
    }

    /** Brief description of current run time setup configuration. */
    public String getProcessingAttributesString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(super.getProcessingAttributesString());
        sb.append(System.getProperty("line.separator"));
        sb.append(" enableLocking=").append(enableLocking);
        sb.append(" autoCommit=").append(autoCommit);
        sb.append(" minAgeSecs=").append(minAgeSecs);
        sb.append(" maxAgeDays=").append(maxAgeDays);
        sb.append(System.getProperty("line.separator"));
        sb.append("module=").append((module == null) ? "null" : module.getClass().getName()); 
        return sb.toString();
    }

    public void setProcessingModule(JasiSolutionProcessorIF module) {
        this.module = module;
    }

    public boolean setProcessingModuleMode(SolutionProcessingMode processingMode) {
        return (module == null) ? false : module.setProcessingMode(processingMode);
    }

    public boolean setProcessingModuleMode(int processingMode) {
        return (module == null) ? false : module.setProcessingMode(processingMode);
    }

    public int resultSolution(Solution solution, int resultCode) {
        return resultId(solution.id.longValue(), resultCode);
    }

    // implement method of super class IF
    public int processId(long eventId) {

        if (verbose) {
            System.out.println(Lines.POUND_TEXT_LINE); // add # line ?
            System.out.println("NEXT EVID: "+eventId+ " processing starts @ "+ new DateTime().toString()); // UTC time
        }

        boolean doit = true; // by default -aww 2014/07/25
        boolean isPrimary = true;
        // Added logic to filter events, if running by AQMS app host role
        if (props.getBoolean("pcsCheckHostAppRole")) { // do we doit?
            String myhostname = "unknown";
            try {
              myhostname = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) { }

            int idx = myhostname.indexOf(".");
            if (idx > 0) {
                myhostname = myhostname.substring(0, idx);
            }

            String myDbHostAppRole = getHostAppRole(); // where role maps to mode of P, S, A or *
            if (debug) {
                System.out.println("DEBUG " + getClass().getName() + " myhostname : " + myhostname + " myDbAppRole: " + myDbHostAppRole);
            }

            isPrimary = DataSource.isPrimaryHost(myhostname);
            if ( isPrimary ) { // it's the primary host
                // if app mode is All, Any, or Primary doit, but don't doit if app mode is run on shadow host
                doit = !myDbHostAppRole.startsWith("S");
            }
            else { // host is the shadow standby 
                // if app mode is All, Any, or Shadow doit, but don't if app mode is run on primary host 
                doit = !myDbHostAppRole.startsWith("P");
            }
        }

        if (!doit) { // a no-op, no processing, just return to result posting
            System.out.println ("SKIP EVID: "+eventId+", no  processing done, db host app role: " + getHostAppRole() +
                    " current host role: " + ((isPrimary) ? "P" : "S"));
            System.out.println(Lines.MINUS_TEXT_LINE);

            return resultId(eventId, ProcessingResult.DISQUALIFIED.getIdCode());
        }

        if ( ! isValidId(eventId) ) {
            System.err.println("ERROR: " + getClass().getName() + ".processId(id), invalid id: " + eventId);
            return resultId(eventId, ProcessingResult.INVALID_ID.getIdCode());
        }

        Solution sol = Solution.create().getById(eventId);
        if (sol == null) return ProcessingResult.NULL_INPUT.getIdCode();


        double ageSecs = sol.getAge();
        if (verbose) System.out.printf("INFO: event %d age is : %7.1g secs", eventId, ageSecs);

        if ( ageSecs > (86400. * maxAgeDays) ) { // added 2015/11/25 -aww
            System.out.printf("\nSKIP EVID: %d, event age > %f maxAgeDays configured property\n", eventId, maxAgeDays); 
            return resultId(eventId, ProcessingResult.DISQUALIFIED.getIdCode());
        }

        if ( minAgeSecs > 0.) { // added 2014/01/22 -aww
                if (ageSecs < minAgeSecs) { // wait til aged
                    ageSecs = (minAgeSecs-ageSecs);
                    if (verbose) System.out.printf(", waiting %6.1fs for event age to be: %6.1fs before being processed.", ageSecs, minAgeSecs);
                    sleep((int)(ageSecs*1000.));
                    // now recreate Solution here in case an update in DB changed the event.prefor or event.prefmag - aww 2014/01/27
                    sol = Solution.create().getById(eventId);
                }
        }

        if (verbose) {
            System.out.println();
            System.out.println("##########" + sol.getNeatStringHeader() + " " + sol.getShortErrorStringHeader());
            System.out.println("START_SUM " + sol.toNeatString() + " " + sol.toShortErrorString());
        }
        System.out.println(Lines.MINUS_TEXT_LINE);
            
        return processSolution(sol);
    }

    private int isReadyForProcessing(Solution solution) {

        releaseAllSolutionLocks(); // release any prior locks

        if (solution == null) {
            System.err.println("ERROR: isReadyForProcessing input solution is null"); 
            return ProcessingResult.NULL_INPUT.getIdCode();
        }

        int resultCode = 0;

        long id = solution.getId().longValue();

        //update processing row with id msg?
        String idStr = "Processing id="+id+" at "+ new DateTime().toString();
        if (! signalMessage(idStr) ) {
          System.err.println("ERROR: failed to signal message for " + idStr);
        }

        if ( forceStateProcessing ) {
          resultCode =
               // Returns -1 => id not posted; 0 => old result; 1 => ready;  >1 => rank blocked.
               ProcessControl.runStatusOf(myPCID.getGroupName(),
                                          myPCID.getThreadName(),
                                          myPCID.getStateName(),
                                          id);

          if (resultCode < 0) { // not posted, or could force posting, then process?
              resultCode = ProcessingResult.ID_NOT_POSTED.getIdCode(); // punt
              if (verbose) System.out.println("INFO: " + idStr +" SKIPPED: id not posted"); 
          }
          else if (resultCode > 1) {
              resultCode = ProcessingResult.ID_RANK_BLOCKED.getIdCode(); // can't do it
              if (verbose) System.out.println("INFO: " + idStr +" SKIPPED: pcs rank blocked"); 
          }
        }

        if (! handleLock(id)) {
            resultCode = ProcessingResult.DB_DATA_LOCK.getIdCode();
            if (verbose) System.out.println("INFO: " + idStr +" SKIPPED: event locked (JasiEventLock)"); 
        }

        return resultCode;
    }

    public int processSolution(Solution solution) {

        String idStr = "processSolution id=" + solution.getId().longValue();

        //Added resulting error state abort of processing here -aww 09/14/2006
        int resultCode = isReadyForProcessing(solution);

        if (resultCode < 0)
            return doResult(solution, resultCode); // 0  or > 1 codes blocked by getNext()

        if ( pcsAllowedRflagStates != null && pcsAllowedRflagStates.length() > 0 &
                pcsAllowedRflagStates.indexOf(solution.getProcessingStateString().toUpperCase()) < 0) {

            if (verbose)
                System.out.println("INFO: " + idStr +" SKIPPED: rflag="+ solution.getProcessingStateString() +
                        " not in pcsAllowedRflagStates="+ pcsAllowedRflagStates);

            return doResult(solution, ProcessingResult.UNIT_SUCCESS.getIdCode());
        }

        try {
            resultCode = module.processSolution(solution); // Where real work happens!
        }
        catch (Exception ex) {
            ex.printStackTrace(); // dump trace of exception and bail
            resultCode = ProcessingResult.JAVA_EXCEPTION.getIdCode();
        } 


        if (debug) {
          System.err.println("DEBUG " +idStr+ " module resultCode: " +resultCode);
        }

        if (verbose) System.out.println("FINAL_SUM " + solution.toNeatString() + " " + solution.toShortErrorString());
        // Skip over DISQUALIFIED
        if (resultCode == ProcessingResult.DISQUALIFIED.getIdCode() ) {
          if (verbose) System.out.println("INFO: " + idStr +" DISQUALIFIED for commit by processing module.");
        }
        else if ( autoCommit ) {
            //(resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() || resultCode == ProcessingResult.ALT_SUCCESS.getIdCode()) 
            // NOTE: allow event commit with rflag="C" 
            if ( resultCode > 0 || resultCode == ProcessingResult.DB_COMMIT_RFLAG_CANCELLED.getIdCode() ) {
                try {
                    if (! solution.commit() )
                        resultCode = ProcessingResult.DB_COMMIT_FAILURE.getIdCode();
                    else if (verbose)
                        System.out.println("INFO: " + idStr +" COMMIT SUCCESS @ " + new DateTime().toString());
                    }
                catch (JasiCommitException ex) {
                    resultCode = ProcessingResult.DB_COMMIT_FAILURE.getIdCode();
                    System.err.println("ERROR: " + idStr + " " + ex.getMessage() + " @ " + new DateTime().toString());
                }
            }
        }

        // Result id here, but in case of resultSolution() failure, 
        // return to invoker the result failure and
        // print out error message about the module failure - aww 05/19/2005
        resultCode = doResult(solution, resultCode);
        if (resultCode < 1) {
            System.err.println("ERROR ? " + idStr + " has result: " +ProcessingResult.describe(resultCode) + " @ " + new DateTime().toString());
        }

        if (verbose) System.out.println(Lines.POUND_TEXT_LINE); // add # line ?

        return resultCode; 

    }

    public int checkCommitConditions(Solution solution) {
        return ProcessingResult.UNIT_SUCCESS.getIdCode();
    }

    public int doResult(Solution sol, int result) {
        if (! forceStateProcessing) return result;

        if ( resultSolution(sol, result) != ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
            System.err.println("ERROR " + getClass().getName() +
                " processSolution(Solution) resultSolution failed id: " +
                sol.getId() + " module result: " + result);
            return ProcessingResult.ID_RESULT_FAILURE.getIdCode();
        }
        return result;
    }

    public int processSolution(Solution [] solutions) {
        if (solutions == null) return ProcessingResult.NULL_INPUT.getIdCode();
        int size = solutions.length;
        if (size < 1 ) return ProcessingResult.NO_RESULTS.getIdCode();

        long startTimeMilliSecs = System.currentTimeMillis(); // loop start time for instance

        int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();
        count = 0;
        failures = 0;
        boolean error = false;
        for (int index = 0; index < size; index++) {
            resultCode = processSolution(solutions [index]);
            if (resultCode > 0) count++;
            else if (resultCode <= 0) {
              failures++;
              error = true;
              if (fastFailLoops) break;
              // If state processing then sudden death here since upon result failure
              // must exit to avoid infinite loop getNext - aww 05/18/2005
              else if ( resultCode == ProcessingResult.ID_RESULT_FAILURE.getIdCode()) {
                if ( getNextId() == solutions[index].getId().longValue() ) {
                  System.err.println("ERROR " + getClass().getName() +
                        ".processSolution(Solution[]) unable to result id : " + solutions[index].getId().toString() +
                        " terminating loop.");
                  break;
                }
              }
            }
        }
        //if (verbose)
          System.out.println(
            "INFO: Total processing time: " +
            EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - startTimeMilliSecs)/1000)
            + " id count success : " + count + " failed: " + failures
          );

        return (error) ? resultCode : count;
    }
    public int processSolution(GenericSolutionList solList) {
        return processSolution(solList.getArray());
    }

    /** Setups datasource from properties then processes all posted event ids.
     *  Check for a processing module, the module and its mode must already have
     *  been configured for this instance from settings or properties.
     *  @see #setProcessingModule(JasiSolutionProcessorIF)
     *  @see AbstractProcessingController.processIds()
     *  @see AbstractProcessingController.processIds(List)
     */
    protected int setupAndProcessIds() {
        int resultCode = setupForProcessing();
        return (resultCode > 0) ? processIds() : resultCode;
    }
    protected int setupAndProcessIds(String idFileName) {
        int resultCode = setupForProcessing();
        return (resultCode > 0) ?
            processIds(getIdListToProcess(idFileName)) : resultCode;
    }
    protected int setupAndProcessIdsBulkPost(String idFileName) {
        int resultCode = setupForProcessing();
        return (resultCode > 0) ?
            processIdsBulkPost(getIdListToProcess(idFileName)) : resultCode;
    }

    protected int setupForProcessing() {
        // processor must first make connection to db
        if (!setupDataSourceFromProperties()) {
          System.err.println(getClass().getName()+" ERROR processor DB connect/setup failed! Check init/properties.");
          return ProcessingResult.DB_NO_CONNECTION.getIdCode();
        }

        // let the db know about running
        if (! signalStart()) {
          System.err.println(getClass().getName()+" ERROR start notification failed!");
          System.err.println("Check database PCS package or PCS_SIGNAL table for user role privileges.");
          return ProcessingResult.PCS_SIGNAL_FAILURE.getIdCode();
        }

        // Does a processing module exist?
        if (module == null) {
          System.err.println(getClass().getName()+" ERROR processor module null! Check init/properties.");
          return ProcessingResult.NO_PROCESSOR_MODULE.getIdCode();
        }

        // Does processing module have its run mode specified?
        if (module.getProcessingMode() == null ||
                module.getProcessingMode() == SolutionProcessingMode.UNKNOWN ) {
          System.err.println(getClass().getName()+" ERROR processor module mode unknown! Check init/properties.");
          return ProcessingResult.UNKNOWN_PROCESSING_MODE.getIdCode();
        }
        return ProcessingResult.UNIT_SUCCESS.getIdCode();
    }

    protected void setupLockingOnDataSource() {

        releaseAllSolutionLocks(); // release any prior locks from source

        if (enableLocking) {
          solLock = SolutionLock.create();
          solLock.setVerbose(debug); 
          // Check if locking works for new data source
          if (! solLock.checkLockingWorks()) { // connection is null, or lock(),unlock() failed
            System.out.println("WARNING: Event locking is not supported by this data source.");
          }
        }
        else {
          solLock = null;
        }
    }

    /** Close DataSource, if it was created by this instance */
    protected void closeDataSource() {
        releaseAllSolutionLocks(); // clear lock table in db
        super.closeDataSource();
    }

    /** Overide invoke parent method then enable Solution locking on DataSource. */
    protected boolean setupDataSourceFromProperties() {
        boolean status = super.setupDataSourceFromProperties();
        if (status) setupLockingOnDataSource();
        return status;
    }

    /**
     * Check to see if this solution id is locked by another user. If it is, pop a dialog
     * to inform the user and return 'false'. Otherwise, return true.  Also returns 'true'
     * if locking is not supported otherwise you would never be allowed access to events.
    */
    public boolean handleLock(long id) {

        if (solLock == null || ! enableLocking) return true;   // no locking enabled

        if (debug) System.out.println("DEBUG ProcessingController handleLock for id " + id);

        // attempt lock. Remember, this returns 'true' if locking is NOT enabled
        solLock.setId(id);
        // NOTE: SolutionTN lock does a DataSource reconnect if connection null or closed
        if (solLock.lock()) return true; // lock was successfull

        // lock failed, report 
        System.out.println(
               "EVENT "+id+" IS LOCK BLOCKED. "+
               "Username:    "+solLock.getUsername()+
               "Hostname:    "+solLock.host+
               "Application: "+solLock.application+
               "Time:        "+LeapSeconds.trueToString(solLock.datetime).substring(0, 19) // for UTC -aww 2008/02/08
        );

        return false;
    }

    /** Release all Solution Locks */
    public void releaseAllSolutionLocks() {
        if (solLock != null) {
          if (debug) System.out.println("DEBUG ProcessingController releaseAllSolutionLocks");
          solLock.unlockAllMyLocks(); // NOTE: SolutionTN unlock does a DataSource reconnect if connection null or closed
        }
    }

    /** Set <i>true</i> to enable commit of successfully processed Solutions to the database.
     * Default setting is <i>true</i>, unless overidden by input property <i>pcsAutoCommit</i>.
     */
    public final void setAutoCommit(boolean tf) {
        autoCommit = tf;
    }

    /** Returns <i>true </i> if successfully processed Solutions are committed to the database.
     */
    public final boolean isAutoCommit() {
        return autoCommit;
    }


    public int processByEventProperties() {
        return processByEventProperties(defaultPostingRank);
    }

    protected GenericSolutionList getSolutionListFromEventProperties() {
        // Use user properties filename, and default properties file name in any
        EventSelectionProperties eventProps = new EventSelectionProperties();
        boolean status = eventProps.initialize(
                            appName.toLowerCase(),
                            props.getEventSelectionProps(),
                            props.getEventSelectionDefaultProps()
                        );
        // Dump input event selection properties to log for info
        System.out.println(Lines.MINUS_TEXT_LINE);
        System.out.println("-- EventSelectionProperties --");
        eventProps.dumpProperties();
        System.out.println(Lines.MINUS_TEXT_LINE); 

        if (! status) {
          System.err.println("ERROR: EventSelectionProperties initialization failed, check input!");
          return null;
        }

        // Create a list of solutions matching event selection properties
        List aList = (List) Solution.create().getByProperties(eventProps);
        if (aList == null || aList.isEmpty()) {
            System.out.println("INFO: no Solution found matching input Event selection properties.");
            return new GenericSolutionList(0);
        }

        return new GenericSolutionList(aList);
    }

    // Note: Method below does NOT do all "postings" inclusively in table,
    // it checks each getNextId() against event id of the SolutionList 
    // that was created using any specified EventSelectionProperties
    // If this behavior is not desired, override in a subclass.
    public int processByEventProperties(int rank) {

        long startTimeMilliSecs = System.currentTimeMillis(); // start time of invocation

        // Create a list of solutions matching event selection properties
        GenericSolutionList solList = getSolutionListFromEventProperties();
        if (solList == null) return ProcessingResult.FAILURE.getIdCode();
        else if (solList.isEmpty()) return ProcessingResult.NO_RESULTS.getIdCode();

        // Post new list ids for processing state
        //  int resultCode = postIds( getIds(solList), rank ); // removed -aww 2008/03/27
        int resultCode = bulkPost( getIds(solList), rank ); // added -aww 2008/03/27
        if (resultCode < 1) {
            System.out.println("WARNING: Event posting returned code: " + resultCode +
                    " no Solutions will be processed, check tables for any existing id postings!");
            return resultCode;
        }

        // Notify user of any "ranked blocked" event ids
        Iterator iter = solList.iterator();
        long id = 0l;
        while (iter.hasNext()) {
          id = ((Solution) iter.next()).getId().longValue();
          if ( ProcessControl.isRankBlocked(myPCID.getGroupName(),
                                            myPCID.getThreadName(),
                                            myPCID.getStateName(),
                                            id)  ) {
              System.out.println("WARNING: State processing RANK blocked for id : " + id); 
          }
        }

        // Now process posted ids 
        count = 0;
        failures = 0;
        id = getNextId(); // next posted id to process, -1 if none
        boolean error = false;
        long oldId = 0l;
        Solution sol  = null;
        while (id > 0l)  {
            if ( hasStopSignal() ) break;

            sol = solList.getById(id); // is it one in the input list
            if (sol != null) { // have match it list so do it!
              resultCode = processSolution(sol);
              if (resultCode > 0) count++; // bump count of success
              else if (resultCode <= 0) {
                failures++;
                error = true;
                if (fastFailLoops) break;
                // If state processing then sudden death here since upon result failure
                // must exit to avoid infinite loop getNext - aww 05/18/2005
                else if ( resultCode == ProcessingResult.ID_RESULT_FAILURE.getIdCode()) {
                  if (getNextId() == id) {
                    System.err.println("ERROR " + getClass().getName() +
                        ".processByEventProperties(rank) unable to result id : " + id +
                        " terminating loop.");
                     break;  // have to break to avoid infinite loop getNextId() - aww 05/18/2005 
                  }
                }
              }
              if (debug)
                  System.out.println("DEBUG processSolution clearing data lists for : " + id);
              // clearing minimize memory usage may be long list -aww 05/16/2005
              sol.clearDataLists(verbose); // verbose == false => don't warn about commit state
            }
            else {  // do nothing with the id for now
              //processId(id);
            }
            // solList.remove(sol);
            id = getNextId(); // next posted id to process
            if (debug) System.out.println("DEBUG processSolution getNextId() return value at end of loop: " + id);
            // Another test break to avoid infinite while loop, just in case result failure trap also fails above
            if (oldId == id) break; // should only occur if no more postings
            else oldId = id; // valid new id
        } 

        //if (verbose)
          System.out.println(
            "INFO: Total processing time: " +
            EpochTime.elapsedTimeToText((int) (System.currentTimeMillis() - startTimeMilliSecs)/1000)
            + " id count success : " + count + " failed: " + failures
          );

        return (error && fastFailLoops) ? resultCode : count;
    }

    // Convenience wrapper utility to recover long ids to process from list elements
    public static List getIds(JasiCommitableListIF jcList) {
        int size = (jcList == null) ?  0 : jcList.size();
        ArrayList idList = new ArrayList(size);
        for (int idx = 0; idx < size; idx++) {
          idList.add( Long.valueOf( ((JasiCommitableIF) jcList.get(idx)).getIdentifier().toString()) );
        }
        return idList;
    }


    public String getHostAppRole() {

        String myhostname = "unknown";

        try {
          myhostname = InetAddress.getLocalHost().getHostName();
        } catch (IOException e) { }

        int idx = myhostname.indexOf(".");
        if (idx > 0) {
            myhostname = myhostname.substring(0, idx);
        }

        String myappname = null;
        if (module != null) {
           myappname = module.getAppName(); // module name overrides controller name
        }
        if (myappname == null) myappname = appName; // default to controller name
        if (myappname.equals("unknown")) return myappname; // punt

        return DataSource.getHostAppRole(myhostname, myappname, myPCID.getStateName());
    }

    /* Instead use static method implemented in DataSource
    public static String getHostAppRole(String hostname, String appname, String pcsState) {

        String role = "unknown";

        if (hostname == null || appname == null || pcsState == null) return role;

        if (DataSource.isNull()) return role;

        Connection c = DataSource.getConnection();
        if (c == null) return role;

        Statement s = null;
        ResultSet rs = null;
        try {
            s = c.createStatement();
            String sql = "SELECT role_mode FROM APP_HOST_ROLE WHERE" + " host = '" + hostname + "'"
               + " AND appname = '" + appname + "'" + " AND state = '" + pcsState + "'" + " AND offdate > SYSDATE";
            //if (debug)
                System.out.println(sql):
            rs = s.executeQuery(sql);

            if (rs.next()) {
              role = rs.getString(1);
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex2) {
            System.err.println(ex2.getMessage());
          }
        }
        return role;

    }
    */

 }

package org.trinet.pcs;
public interface JasiSolutionChannelProcessorIF extends JasiSolutionProcessorIF {
    boolean hasChannelList() ;
    void setChannelList(org.trinet.jasi.ChannelList list) ;
    //org.trinet.jasi.ChannelList getChannelList() ;
    //org.trinet.jasi.Channel getChannelFromList(org.trinet.jasi.Channel channel) ;
    //double getChannelToSolutionAzimuth(org.trinet.jasi.Channel chan) ;
    //double getChannelToSolutionDistance(org.trinet.jasi.Channel chan) ;
    //double getSolutionToChannelAzimuth(org.trinet.jasi.Channel chan) ;
    //double getSolutionToChannelDistance(org.trinet.jasi.Channel chan) ;
}


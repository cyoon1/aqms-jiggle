package org.trinet.pcs;
public interface JasiChannelListProcessingControllerIF extends JasiSolutionProcessingControllerIF {
    boolean hasChannelList() ;
    void setChannelList(org.trinet.jasi.ChannelList list) ;
    org.trinet.jasi.ChannelList getChannelList();
    boolean loadChannelList(boolean isMaster) ;
}

package org.trinet.pcs;

import org.trinet.jasi.*;

public abstract class AbstractSolutionChannelDataProcessor extends AbstractJasiSolutionProcessor
    implements JasiSolutionChannelProcessorIF {

    /** No-op Constructor. */
    public AbstractSolutionChannelDataProcessor() { }

    /**
     * Constructor initializes EngineDelegate with property list and
     * sets SolutionProcessingMode.UNKNOWN as the mode (default) for processing.
     */
    public AbstractSolutionChannelDataProcessor(SolutionEditorPropertyList props) {
        this(props, SolutionProcessingMode.UNKNOWN);
    }

    /**
     * Constructor initializes EngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public AbstractSolutionChannelDataProcessor(SolutionEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
        initFromProperties();
    }

    //IF subclass implementation might wrap delegate
    abstract public boolean hasChannelList();

    //IF subclass implementation might wrap delegate
    abstract public void setChannelList(ChannelList chanList);

    //IF
    /** Process input Solution as configured.*/
    abstract public int processSolution(Solution sol) ;

}

package org.trinet.pcs;
  /**
   * Put an data object in the given state/rank.
   *  % SYNTAX: recycle <group> <source> <state> <result> <newGroup> <newSource> <newState> <rank>
   */
class Recycle {
    String group;
    String table;
    String state;
    int    result;
    String newGroup;
    String newTable;
    String newState;
    int    rank;

    public static void main(String args[]) {
        Recycle p = new Recycle();
        int nargs = args.length;
        if (nargs != 4  && nargs != 8) {
            System.out.println("SYNTAX: recycle <group> <source> <state> <result> [<newGroup> <newSource> <newState> <rank>]");
            return ;
        }

        p.group = args[0];
        p.table = args[1];
        p.state = args[2];
        p.result = Integer.valueOf(args[3]).intValue(); // string -> int

        if (nargs == 8) {
            p.newGroup = args[4];
            p.newTable = args[5];
            p.newState = args[6];
            p.rank = Integer.valueOf(args[7]).intValue(); // string -> int
        }

        ProcessControl.recycle(p.group, p.table, p.state, p.result,
                p.newGroup, p.newTable, p.newState, p.rank);

        // show all state/ranks for this id
        if (nargs <= 4) return;
        StateRow rows[] = StateRow.get(p.newGroup, p.newTable, p.newState);
        //list new row results 
        for (int i=0; i < rows.length; i++) {
            System.out.println(rows[i].toOutputString());
        }

    }
}

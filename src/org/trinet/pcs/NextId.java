package org.trinet.pcs;
  /**
   * Get the next ID for the given group/table/state.
   */
class NextId {
    public static void main(String args[]) {
      if (args.length < 3)
          System.out.println("SYNTAX: next <group> <source> <state> [delay]");
      else 
          System.out.println( StateRow.getNextId(args[0], args[1], args[2], Integer.parseInt(args[3])) );
    }
}

package org.trinet.pcs;
public class CodaSolutionProcessingMode extends SolutionProcessingMode {
    public static final CodaSolutionProcessingMode CALC_CODA_ONLY = 
           new CodaSolutionProcessingMode("CALC_CODA_ONLY", 0);
    public static final CodaSolutionProcessingMode CALC_CODA_AND_SUMMARY_MAG =
           new CodaSolutionProcessingMode("CALC_CODA_AND_SUMMARY_MAG", 1);
    public static final CodaSolutionProcessingMode CALC_SUMMARY_MAG_USE_EXISTING_CODA =
           new CodaSolutionProcessingMode("CALC_SUMMARY_MAG_USE_EXISTING_CODA", 2);
    private CodaSolutionProcessingMode(String description, int idCode) {
        super(description, idCode);
    }
}

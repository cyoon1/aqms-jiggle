package org.trinet.pcs;

import java.sql.*;
import java.util.Date;
import org.trinet.jasi.*;
import org.trinet.util.DateTime;

public abstract class AbstractChannelListProcessingController extends AbstractSolutionProcessingController
    implements JasiChannelListProcessingControllerIF {

    protected ChannelList cntrlChanList = null;

    protected AbstractChannelListProcessingController () {}

    protected AbstractChannelListProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, DEFAULT_THREAD_SLEEP_MILLIS,  null);
    }
    protected AbstractChannelListProcessingController(String appName, String groupName, String threadName, String stateName,
                                           int defaultSleepTimeMillis) {
        this(appName, groupName, threadName, stateName, defaultSleepTimeMillis,  null);
    }
    protected AbstractChannelListProcessingController (String appName, String groupName, String threadName, String stateName,
                                                 int defaultSleepTimeMillis, JasiSolutionChannelProcessorIF module) {
        super(appName, groupName, threadName, stateName, defaultSleepTimeMillis);
    }

    /** Setups datasource from properties, creates ChannelList for processing if none is 
     *  defined,then processes all posted event ids.
     *  Processing module and mode must already have been configured 
     *  for instance from settings or properties.
     *  @see AbstractProcessingController.processIds()
     */
    protected int setupForProcessing() {

        int resultCode = super.setupForProcessing();
        if (resultCode < 1) return resultCode;

        // For case where a channel list is not needed
        if (props.getBoolean("pcsDisableLoadChannelList")) {
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
        }

        // If processor ChannelList D.N.E., create default masterlist
        if (cntrlChanList == null) loadChannelList(true);

        // Do channel data exist?
        if (cntrlChanList == null || cntrlChanList.isEmpty()) {
          System.err.println(appName + " WARNING processor has no channel list data; check input properties!");
          return ProcessingResult.NO_CHANNEL_DATA.getIdCode();
        }
        return ProcessingResult.UNIT_SUCCESS.getIdCode();
    }

    public void setProcessingModule(JasiSolutionProcessorIF module) {
        super.setProcessingModule(module);
        if (module != null && cntrlChanList != null)
          ((JasiSolutionChannelProcessorIF)module).setChannelList(cntrlChanList);
    }

    public boolean hasChannelList() {
        return (cntrlChanList != null);
    }

    /** Set the ChannelList of the processing module to the input list.
     * @see #setProcessingModule(JasiSolutionProcessorIF)
     */
    public void setChannelList(ChannelList chanList) {
        cntrlChanList = chanList;
        if (cntrlChanList == null) return;

        if (! cntrlChanList.hasLookupMap() ) cntrlChanList.createLookupMap();
        if (module != null) ((JasiSolutionChannelProcessorIF)module).setChannelList(cntrlChanList);
    }
    
    public ChannelList getChannelList() {
        return cntrlChanList;
    }
    /** Populates ChannelList with Channels found in file cache specified by configuration property 
     * <i>channelCacheFilename</i> otherwise adds to new list those Channels active in the database
     * on current date or date specified by the configuration property <i>channelLoadDate</i>.
     * If <i>isMaster</i>=<i>true</i>, the MasterChannelList is also set to the loaded ChannelList.
     */
    public boolean loadChannelList(boolean isMaster) {

        if (props.getBoolean("pcsDisableLoadChannelList")) return false;

        StringBuffer sb = new StringBuffer(512);
        String cacheFileName = "";
        sb.append(appName + " INFO: Reading Channels from data source: ").append(DataSource.getDbaseName());
        if (props.isSpecified("channelCacheFilename")) {
          cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");
          sb.append(" channelCacheFileName: ").append(cacheFileName);
        }
        org.trinet.util.DateTime date = null;
        if (props.isSpecified("channelLoadDate")) {  // is there a channel load date ?
          date = props.getDateTime("channelLoadDate");
          sb.append(" loadDate: ").append(date);
        }
        
        // Must be defined or no channels are found, no default group name
        String channelListName = props.getChannelGroupName();

        System.out.println(sb.toString());

        ChannelList chList = ChannelList.smartLoad(date, channelListName);
        if (chList == null) return false;
        
        // create HashMap of channels in ChannelList, if not already one
        if (! chList.hasLookupMap()) chList.createLookupMap();
        System.out.println(appName + "INFO: >>> Loaded ChannelList total channels: " + chList.size());

        setChannelList(chList);
        if (isMaster) MasterChannelList.set(chList);

        return (chList.size() > 0);
    }
}

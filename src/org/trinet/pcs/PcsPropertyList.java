package org.trinet.pcs;

import java.util.*;
import org.trinet.jasi.SolutionWfEditorPropertyList;

//pcsProcessorMode=0
/** <pre>
Example:
pcsEngineDelegateProps=delegate.props
pcsProcessorMagTypes=ml
pcsProcessorModeName=LOCATE
pcsLogFileName=hypomag.log
pcsLogFilePath=C:\\Temp\\
pcsLogging=true
pcsLogRotationInterval=24H
pcsGroupName=PostProc
pcsThreadName=HYPOMAG
pcsStateName=hypomag
pcsRunMode=0
pcsSleepMilliSecs=150
pcsDefaultNextLagSecs=0
pcsDefaultPostingRank=50
pcsForceStateProcessing=true
pcsFastFailLoops=true
pcsSolutionLocking=true
pcsAutoCommit=false
pcsOriginDeltaToCommit=0.
pcsModuleClassName=org.trinet.pcs.HypomagSolutionProcessor
pcsListExclusive=false
</pre>
*/
public class PcsPropertyList extends SolutionWfEditorPropertyList {

/**
 *  Constructor: reads no property file empty list.
 *  @see #setRequiredProperties()
 */
    public PcsPropertyList() { }

/**
 *  Constructor: Makes a COPY of a property list. Doesn't reread the properties files,
 * assumes that was already done at instantiation of passed input object.
 * @see: JasiPropertyList(String fileName)
 */
    public PcsPropertyList(PcsPropertyList props) {
        super(props);
        privateSetup();
    }

/**
 *  Constructor: input file is a name relative to the default user and system paths
 *  which are defined by System properties appending a default subdirectory of type
 *  defined by the form of the application name or "jasi".
 */
    public PcsPropertyList(String fileName)
    {
        super(fileName);  // does a reset()
        privateSetup();
    }

/**
*  Constructor: input filenames are not appended to constructed paths, but rather are
*  interpreted as specified (either relative to current working directory or a complete path). 
*/
    public PcsPropertyList(String userPropFileName, String defaultPropFileName)
    {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    private boolean privateSetup() {
      return true;
    }

    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    /* Override of GenericPropertyList method does not prepend "." to subdir name
    public String getUserFilePath() {
        String userHome = System.getProperty(getFiletype().toUpperCase()+"_USER_HOMEDIR");
        if (userHome !=  null) return userHome;
        else userHome = System.getProperty("user.home", ".");
        String subdir = getFiletype();
        if (subdir.equals("")) return userHome;
        StringBuffer sb = new StringBuffer(150);
        sb.append(userHome).append(FILE_SEP).append(subdir);
        return sb.toString();
    }
    */

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(24);

        myList.add("pcsAutomatic");
        myList.add("pcsDisableLoadChannelList");
        myList.add("pcsEngineDelegate");
        myList.add("pcsEngineDelegateProps");
        myList.add("pcsProcessorMagTypes");
        //myList.add("pcsProcessorMode");
        myList.add("pcsProcessorModeName");
        myList.add("pcsLogFileName");
        myList.add("pcsLogFilePath");
        myList.add("pcsLogRotationInterval");
        myList.add("pcsLogging");
        myList.add("pcsGroupName");
        myList.add("pcsThreadName");
        myList.add("pcsStateName");
        myList.add("pcsRunMode");
        myList.add("pcsSleepMilliSecs");
        myList.add("pcsDefaultNextLagSecs");
        myList.add("pcsDefaultPostingRank");
        myList.add("pcsForceStateProcessing");
        myList.add("pcsFastFailLoops");
        myList.add("pcsSolutionLocking");
        myList.add("pcsAutoCommit");
        myList.add("pcsOriginDeltaToCommit");
        myList.add("pcsModuleClassName");
        myList.add("pcsListExclusive");
        myList.add("pcsMinAgeSecs");
        myList.add("pcsCheckHostAppRole");
        myList.add("pcsProcessorAppName");
        
        Collections.sort(myList);

        return myList;
    }

    // set required properties here

} // end of class


package org.trinet.waveserver.rt;
class WaveformDataException extends RuntimeException {
    WaveformDataException() {
        super();
    }

    WaveformDataException(String message) {
        super(message);
    }
}


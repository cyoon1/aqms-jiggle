package org.trinet.waveserver.rt;
import java.io.*;
import java.net.*;

public class TCPConnServer {
    public static final int DEFAULT_QUEUE_LENGTH = 10;

/** The server socket for this instance. */
    ServerSocket serverSocket;

/** The client worker socket. */
    TCPConnServerClient connClient;

/** Constructor creates server socket, no client connection until acceptTCPClient() invoked.
* @exception java.io.IOException error creating ServerSocket.
* @exception java.lang.SecurityException if SecurityManager exists, checkListen not allowed.
*/
    TCPConnServer(int port) throws IOException, SecurityException {
          this(port, DEFAULT_QUEUE_LENGTH, null);
          
    }

/** Constructor creates server socket, no client connection until acceptTCPClient() invoked.
* @exception java.io.IOException error creating ServerSocket.
* @exception java.lang.SecurityException if SecurityManager exists, checkListen not allowed.
*/
    TCPConnServer(int port, int queueLength) throws IOException, SecurityException {
          this(port, queueLength, null);
          
    }

/** Constructor creates server socket, no client connection until acceptTCPClient() invoked.
* @exception java.io.IOException error creating ServerSocket.
* @exception java.lang.SecurityException if SecurityManager exists, checkListen not allowed.
*/
    TCPConnServer(int port, int queueLength, InetAddress addr) throws IOException, SecurityException {
          serverSocket = new ServerSocket(port, queueLength, addr);
          
    }

/** Closes existing client socket and server socket. */
    public void close() {
        try {
            if (connClient != null) connClient.close();
            if (serverSocket != null) serverSocket.close();
        }
        catch (IOException ex) {
            System.err.println(">>>Error TCPConnServer.close() : " + ex.toString());
            //ex.printStackTrace();
        }
    }


/** Listens for a connection request, upon request creates a TCPConnClientServer object.
*  Uses the default timeout millisecs value for acception.
* @exception java.io.IOException error creating client connection.
* @exception java.lang.SecurityException if SecurityManager exists, checkListen not allowed.
*/
    TCPConnServerClient acceptTCPConnClient() throws IOException {
        return new TCPConnServerClient(serverSocket.accept());
    }   

/** Returns the acception timeout for the server socket.
*/
    public int getAcceptTimeout() throws IOException {
        return serverSocket.getSoTimeout();
    }

/** Sets this server's default timeout millisecs for accepting connections.
* An input value of 0 == no timeout, infinite wait.  Returns true if successful.
*/
    public boolean setAcceptTimeout(int timeoutMilliSecs) {
        boolean retVal = true;
        try {
            serverSocket.setSoTimeout(timeoutMilliSecs);
        }
        catch (SocketException ex) {
            System.err.println(">>>Error TCPConnServer.close() SocketException : " + ex.toString());
            //ex.printStackTrace();
            retVal = false;
        }
        return retVal;
    }

/** Sets this server's default timeout millisecs for reading client connections.
* An input value of 0 == no timeout, infinite wait.  Returns true if successful.
*/
    public boolean setClientTimeout(int timeoutMilliSecs) {
        return connClient.setDefaultTimeout(timeoutMilliSecs);
    }

/** Returns this server's listening port number. */
    public int getLocalPort() {
        return serverSocket.getLocalPort();
    }

/** Returns this server's local address. */
    public InetAddress getInetAddress() {
        return serverSocket.getInetAddress();
    }
/** Read a TCPMessage from the client socket and print a summary of its content. */
    void getRequestMessage() {
        connClient.processRequest();
    }

    /*
    public static void main(String  [] args) {
        int port = 8080;
        TCPConnServer connServer = null;
        try {
            connServer = new TCPConnServer(port);
            connServer.setAcceptTimeout(30000);
            connServer.connClient = connServer.acceptTCPConnClient();
            connServer.setClientTimeout(30000);
            connServer.getRequestMessage();
        }
        catch (IOException ex) {
           ex.printStackTrace();
        }
        catch (Exception ex) {
           System.err.println(ex.toString());
           ex.printStackTrace();
        }
        finally {
            connServer.close();
        }
    }
    */
}

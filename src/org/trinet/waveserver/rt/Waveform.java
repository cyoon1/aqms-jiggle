package org.trinet.waveserver.rt;
import java.util.*;
import org.trinet.jasi.WFSegment;
import org.trinet.jasi.SeedEncodingFormat;
import org.trinet.jasi.seed.SeedReader;
import org.trinet.jasi.seed.SeedReaderException;
import org.trinet.util.DateTime;

/** Implemenation of the TRINET WaveClient/Server Waveform class.
* This class is a container for the time series data of a waveform belonging to a specific seismic station channel.
* The seismic channel data attributes are described by the Channel data member associated with this object.
* The time-series is contained in DataSegment objects as a collection of timestamped contiguous time-series packets.
* The sample rate for the associated time-series is assumed uniform and is set as data member in the associated Channel object.
* This class is used by a WaveClient object when processing a getData(...) or getPacketData(...) message request.
*/

public class Waveform implements Cloneable, MiniSeedDataTypes {

/** Fraction of a period jitter allowed is sampling. */
    public static final double TIME_EPSILON = 0.10;

/** MiniSEED Data type defined by NCEDC Waveform schema */
    public static final int MINISEED_TYPE = 2; // as defined by NCEDC Waveform schema

/** Estimated minimum samples in a miniSEED packet. */
    public static final int MINISEED_MIN_PACKET_SAMPLES = 128;

/** List of DataSegment objects containing summary data and SEED packets. */
    Collection dataSegmentList = null;

/** Channel descriptor object for this Waveform. */
    Channel chan = null;

/** Size of data sample buffer (frame) comprising packet. */
    int recordSize = 0; //  Supports MINISEED_REC_512, MINISEED_REC_4096

/** Time-series data format type */
    int dataFormat = 0; //  Supports MINISEED_TYPE;

/** Time-series data format type */ // dg added // aww rev 11/24
    int dataEncoding = 0; //  See: org.trinet.SeedEncodingFormat()  Steim 1, Steim 2, etc.;

/** SEED packet reader used to parse data in data segment collection.*/
    private SeedReader seedReader = null;

/** Internal pointer used by next() method as starting offset of next contiguous Waveform time series */
    private int nextSegmentIndex = 0;

/** Default constructor no data member initialization ==  null data members. */
    public Waveform() { }

/** Constructor aliases data members with references same data as that of the input Waveform.
* @exception java.lang.NullPointerException null input parameter
*/
    public Waveform(Waveform wave) {
        chan            = wave.chan;
        dataSegmentList = wave.dataSegmentList;
        recordSize      = wave.recordSize;
        dataFormat      = wave.dataFormat;
        dataEncoding    = wave.dataEncoding;
    }

/** Constructor initializes data members by parsing data from the first DataSegment in the List.
*   Verifies data integrity/consistency by scanning data list elements.
*   @exception java.lang.NullPointerException if input collection is null.
*   @exception java.lang.WaveformDataException error occurred verifying collection data.
*/
    Waveform(Collection dataSegmentList) throws WaveformDataException {
        this(dataSegmentList, true);
    }

/** Constructor initializes data members by parsing data from the first DataSegment in the List.
*   If verifyData == true, verifies data integrity/consistency by scanning all list elements.
*   else no checking is done and initialization is complete.
*   @exception java.lang.NullPointerException input data collection is null.
*   @exception java.lang.WaveformDataException error occurred verifying collection data.
*/
    Waveform(Collection dataSegmentList, boolean verifyData) throws WaveformDataException {
        if (dataSegmentList == null) {
            throw new NullPointerException("Waveform(List) null input argument.");
        }
        if (dataSegmentList.isEmpty()) {
            System.err.println("Waveform(Collection, boolean) Warning - collection is empty no data.");
        }
        setData(dataSegmentList, verifyData);
    }

/** Makes a shallow copy of this object */
    public Object clone() {
        Waveform waveform = null;
        try {
            waveform = (Waveform) super.clone();
//          waveform.chan = (Channel) chan.clone();  // optional, do you really need a new copy?
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return waveform;
    }

/** Set the trace data DataSegmentList for this Waveform with verification of data integrity/consistency.
*   @exception java.lang.NullPointerException if input collection is null.
*/
    void setData(Collection dataSegmentList) throws WaveformDataException {
         setData(dataSegmentList, true);
    }

/** Set the trace data DataSegmentList for this Waveform.
*   If verify == true, verifies data integrity/consistency by scanning all list elements.
*   else no checking is done and initialization is complete.
*   @exception java.lang.NullPointerException if input data collection is null.
*   @exception WaveformDataException error occurred initializing data members.
*/
    void setData(Collection dataSegmentList, boolean verify) throws WaveformDataException {
        if (dataSegmentList == null) {
            throw new NullPointerException("Waveform.setData() null data segment list input parameter.");
        }
        nextSegmentIndex = 0;
        this.dataSegmentList = dataSegmentList;       // assumes data is in sorted order
        if (dataSegmentList.isEmpty()) return;

        WFSegment wfSegment = null;
        DataSegment dataSegment = null;

        // Set the channel data declared in the input data
        try {
            //Obtain first data segment from list and check data samples, treat no samples as fatal error
            Iterator iter =  dataSegmentList.iterator();
            dataSegment = (DataSegment) iter.next();
            if (dataSegment.numberOfSamples <= 0) throw new WaveformDataException("No samples declared for first data segment.");

            if (seedReader == null) seedReader = new SeedReader();

            // Extract the data from the DataSegment
            wfSegment = seedReader.createWFSegment(dataSegment.dataContent);

            // Initialize Waveform data members with the data extracted from first DataSegment data Packet
            chan = makeChannel(wfSegment);
            dataFormat = initDataFormat(wfSegment.fmt);
            dataEncoding = initDataEncoding(wfSegment.encoding);
            recordSize = initRecordSize(dataSegment.numberOfDataBytes);

            // VERIFY == FALSE ASSUMES ALL REMAINING DATA SEGMENTS CONFORMING +++++++++++++++++++++++++++++++++++++++++
            if ( verify ) verifyDataConsistency(iter, dataSegment);
        }
        catch (WaveformDataException ex) {
            System.err.println("Error Waveform.setData() failure processing DataSegment collection");
            if (dataSegment != null) System.err.println(toString(nextSegmentIndex, dataSegment));
            if (wfSegment != null) System.err.println("\tDump of First WFSegment:\n" + wfSegment.dumpToString());
            System.err.println(ex.toString());
            //ex.printStackTrace();
            throw new WaveformDataException("Waveform.setData(): " + ex.getMessage());
        }
        catch (Exception ex) {
            System.err.println("Error Waveform.setData() caught exception processing DataSegment collection");
            if (dataSegment != null) System.err.println(toString(nextSegmentIndex, dataSegment));
            if (wfSegment != null) System.err.println("\tDump of First WFSegment:\n" + wfSegment.dumpToString());
            System.err.println(ex.toString());
            //ex.printStackTrace();
            throw new WaveformDataException("Waveform.setData() caught exception " + ex.getMessage());
        }
    }

/** Checks consecutive DataSegment in input collection for collective data consistency.
*/
    private void verifyDataConsistency(Iterator iter, DataSegment dataSegment) throws WaveformVerificationException {

        DataSegment dataSegmentPrevious = dataSegment;
        int currentSegmentIndex = 0;
        WFSegment wfSegment = null;

        try {
            while (iter.hasNext()) {

                currentSegmentIndex++;
                dataSegment = (DataSegment) iter.next();

                // Verify that there are data declared in this segment
                if (dataSegment.numberOfSamples <= 0)
                    throw new WaveformDataException("No samples declared for data segment, index = " + currentSegmentIndex);

                //Extract channel data from SEED header
                wfSegment = seedReader.createWFSegment(dataSegment.dataContent);
                if (wfSegment == null)
                    throw new WaveformDataException("Unable to decode SEED header, index = " + currentSegmentIndex);

                //Set the comparison data for this segment
                Channel currentChan = makeChannel(wfSegment);
                int currentDataFormat  = initDataFormat(wfSegment.fmt);
                int currentDataEncoding = initDataEncoding(wfSegment.encoding);
                int currentRecordSize  = initRecordSize(dataSegment.numberOfDataBytes);

                // Verify the channel identifier data are the same as the first segment
                if (! isEqualChannelData(currentChan))
                    throw new WaveformDataException("Channel id/samplerate mismatch in data, index = " + currentSegmentIndex);

                // Verify the data byte count is the same as the first segment
                if (currentRecordSize != recordSize)
                    throw new WaveformDataException("RecordSize mismatch in data, index = " + currentSegmentIndex);

                // Verify the data format type is the same as that of the first data segment
                if (currentDataFormat != dataFormat)
                    throw new WaveformDataException("Format mismatch in data, index = " + currentSegmentIndex);

                // Verify the data encoding type is the same as that of the first data segment
                if (currentDataEncoding != dataEncoding)
                    throw new WaveformDataException("Encoding mismatch in data, index = " + currentSegmentIndex);

                // If this packet and last overlap, display a warning, gaps between packets are OK.
// Looks like a bug to me. - DDG 6/20/01
//                if (isOverlapping(dataSegment, dataSegmentPrevious)) {
                if (isOverlapping(dataSegmentPrevious, dataSegment)) {
                    printOverlapWarning(dataSegmentPrevious, dataSegment, currentSegmentIndex);
                    // Overlapping but don't erase remaining dataSegments in list
                    //  dataSegmentList.subList(currentIndex, dataSegmentList.size()).clear();
                }
                if (hasGap(dataSegmentPrevious, dataSegment)) {
                    printGapWarning(dataSegmentPrevious, dataSegment, currentSegmentIndex);
                    // Overlapping but don't erase remaining dataSegments in list
                    //  dataSegmentList.subList(currentIndex, dataSegmentList.size()).clear();
                }
                dataSegmentPrevious = dataSegment;
            }
        }
        catch (WaveformDataException ex) {
            System.err.println("Error Waveform.verifyDataConsistency() failure processing DataSegment collection at index: "
                               + currentSegmentIndex);
            if (dataSegment != null)  System.err.println(toString(currentSegmentIndex, dataSegment));
            if (wfSegment != null) System.err.println("\tDump of equivalent WFSegment:\n" + wfSegment.dumpToString());
            System.err.println(ex.toString());
            //ex.printStackTrace();
            throw new WaveformVerificationException("Waveform.verifyDataConsistency(): " + ex.getMessage());
        }
        catch (Exception ex) {
            System.err.println("Error Waveform.verifyDataConsistency() caught exception DataSegment collection at index: "
                               + currentSegmentIndex);
            if (dataSegment != null)  System.err.println(toString(currentSegmentIndex, dataSegment));
            if (wfSegment != null) System.err.println("\tDump of equivalent WFSegment:\n" + wfSegment.dumpToString());
            System.err.println(ex.toString());
            //ex.printStackTrace();
            throw new WaveformVerificationException("Waveform.verifyDataConsistency() " + ex.getMessage());
        }
    }

/** Returns true if the input channel identifier and sample rate are equivalent to those of this waveform.*/
    private boolean isEqualChannelData(Channel newChan) {
        return (chan.equalsId(newChan) && ( Math.round(chan.sampleRate) == Math.round(newChan.sampleRate)) );
    }

/** Creates a new org.trinet.rt.seisimc.Channel object from the data in the WFSegment object. */
    private static Channel makeChannel(WFSegment wfSegment) {
        Channel channel = new Channel(wfSegment.getChannelObj().getNet().trim(),
                                      wfSegment.getChannelObj().getSta().trim(),
                                      wfSegment.getChannelObj().getSeedchan().trim(),
                                      wfSegment.getChannelObj().getLocation() // blanks ok, don't trim blanks -aww 07/11/05
                                     );
        channel.sampleRate = initSampleRate(wfSegment.getSampleInterval());
        return channel;
    }

/** Return true if time range of input data segments overlap, else false. */
    private boolean isOverlapping(DataSegment dataSegmentPrevious, DataSegment dataSegmentCurrent) {
        if ( (dataSegmentPrevious != null) && (dataSegmentCurrent != null)) {
            DateTime endtime = endTime(dataSegmentPrevious); // for UTC -aww 2008/04/01
            if (endtime.after(dataSegmentCurrent.startTimestamp) &&
                        absTimeGapSeconds(dataSegmentCurrent, dataSegmentPrevious) > getEpsilon()) {
                return true;
            }
        }
        return false;
    }

/** Prints timestamps of overlapping data segments */
    private void printOverlapWarning(DataSegment dataSegmentPrevious, DataSegment dataSegment, int index) {
        System.err.println( "Warning (Waveform): overlapping time data segments:\n" + toString(index, dataSegment));
        System.err.println( "Previous  : " + dataSegmentPrevious.startTimestamp + "->" + endTime(dataSegmentPrevious) +
                                " Samples : " + dataSegment.numberOfSamples);
        System.err.println( "Current   : " + dataSegment.startTimestamp + "->" + endTime(dataSegment) +
                                " Samples : " + dataSegment.numberOfSamples );
    }

/** Return true if there's a gap greater than getEpsilon() between packets, else false. */
    private boolean hasGap(DataSegment dataSegmentPrevious, DataSegment dataSegmentCurrent) {
        if ( (dataSegmentPrevious != null) && (dataSegmentCurrent != null)) {
            DateTime endtime = endTime(dataSegmentPrevious); // for UTC -aww 2008/04/01
            if (absTimeGapSeconds(dataSegmentPrevious, dataSegmentCurrent) > getEpsilon()) {
                return true;
            }
        }
        return false;
    }

/** Prints timestamps for gap between data segments */
    private void printGapWarning(DataSegment dataSegmentPrevious, DataSegment dataSegment, int index) {
        System.err.println( "Warning (Waveform): gap between time data segments:\n" + toString(index, dataSegment));
        System.err.println( "Previous  : " + dataSegmentPrevious.startTimestamp + "->" + endTime(dataSegmentPrevious) +
                                " Samples : " + dataSegment.numberOfSamples);
        System.err.println( "Current   : " + dataSegment.startTimestamp + "->" + endTime(dataSegment) +
                                " Samples : " + dataSegment.numberOfSamples );
    }

/** Converts period to sample rate.
* @exception WaveformDataException period <= 0.
*/
    private static double initSampleRate(double period) throws WaveformDataException {
        double sampleRate = -1.0;
        if (period > 0.) sampleRate = 1.0/period;
        else throw new WaveformDataException("Waveform.initSampleRate() Invalid sample rate in SEED header.");
        return sampleRate;
    }

/** Checks input against know record size type constants.
*   Known types: MINISEED_REC_512
* @exception WaveformDataException byteSize does match any known record size types.
*/
    private static int initRecordSize(int byteSize) throws WaveformDataException {
        int size = -1;
        switch (byteSize) {
            case MINISEED_REC_512:
                size = MINISEED_REC_512;
                break;
            case MINISEED_REC_4096:
                size = MINISEED_REC_4096;
                break;
            default:
                throw new WaveformDataException("Waveform.initRecordSize() Unsupported SEED record size: " + byteSize);
        }
        return size;
    }

/** Checks input against know format type constants.
*   Known types: MINISEED_TYPE with encoding STEIM1 or STEIM2 as declared in the SEED documentation.
*   Returns input value if data format type is not supported.
*   @exception WaveformDataException input format type is not known.
*/
    private static int initDataFormat(int formatType) throws WaveformDataException {
        int format = -1;
        switch (formatType) {
            case MINISEED_TYPE:
                format = formatType;
                break;
            default:
                throw new WaveformDataException("Waveform.initDataFormat() Unsupported data type: " + formatType);
        }
        return format;
    }
    /** Checks input against know encoding type constants.
    *   @see: org.trinet.jasi.SeedEncodingFormat()
    *   @exception WaveformDataException input format type is not known.
    */
        private static int initDataEncoding(int encodingType) throws WaveformDataException {
          if (encodingType == SeedEncodingFormat.STEIM1 ||
              encodingType == SeedEncodingFormat.STEIM2) {
          } else {
            throw new WaveformDataException("Waveform.initDataFormat() Unsupported encoding type: " + encodingType);
          }
          return encodingType;
    }
/** Returns absolute value seconds difference between the input DataSegment object's starting times. */
    double absTimeGapSeconds(DataSegment dataSegmentLast, DataSegment dataSegmentNext) {
        return Math.abs(dataSegmentNext.getStartTimeSecs() - dataSegmentLast.getStartTimeSecs()
                         - ((double) (dataSegmentLast.numberOfSamples - 1))/chan.sampleRate );
    }

/** Returns absolute value seconds difference between the input DataCounts object's starting times. */
    double absTimeGapSeconds(DataCounts dataCountsLast, DataCounts dataCountsNext) {
       int size = dataCountsLast.dataList.size();
       // Count logic here must match type instantiated in method getDataSamples -aww
       int count = 0;
       for (int ii = 0; ii < size; ii++) {
           count += ((float []) dataCountsLast.dataList.get(ii)).length;
       }
       return Math.abs(dataCountsNext.getStartTimeSecs() - dataCountsLast.getStartTimeSecs()
                         - ((double) (count - 1))/chan.sampleRate );
    }

/** Inserts the data into the input DataCounts list, appends samples to last DataCounts object in list if data is contiguous
*   else a new element is added to the list.
*/
    boolean insertCounts(List dataCountsList, DataCounts dataCountsNext) {
        int sizeofList = dataCountsList.size();
        if (sizeofList > 0) {
            DataCounts dataCountsLast = (DataCounts) dataCountsList.get(sizeofList - 1);
            double test = absTimeGapSeconds(dataCountsLast, dataCountsNext);
            if (test < getEpsilon()) {   // overlaps and gaps go to separate element
//            if (absTimeGapSeconds(dataCountsLast, dataCountsNext) < getEpsilon()) {   // overlaps and gaps go to separate element
                ArrayList dataList = (ArrayList) dataCountsLast.dataList;
                dataList.ensureCapacity(dataList.size() + Math.max(MINISEED_MIN_PACKET_SAMPLES, dataCountsNext.dataList.size()));
                return dataList.addAll(dataCountsNext.dataList); // append contiguous samples to last segment in list
            }
        }
        return dataCountsList.add(dataCountsNext); // Append a new DataCounts element to list (a new time segment)
    }


/** Unpacks the data samples from the SEED packet data contained in the input  data segment.
*   Returns a new DataCounts object containing the the samples in an array data member.
*/
    DataCounts getSamples(DataSegment dataSegment) {
        DataCounts retVal = null;
        try {
            //int [] samples = seedReader.getDataSamples(dataSegment.dataContent);
            float [] samples = seedReader.getDataSamples(dataSegment.dataContent); // re modified D.Given's SEED reader impl
            if (samples == null) {
                System.err.println( "Error Waveform.getSamples(): No sample counts returned for input packet" );
                retVal =  null;
            }
          /*  Changed to used retrieved array rather than generating new objects
            int totalSamples = samples.length;
            List arrayList = new ArrayList(totalSamples);
            for (int index = 0; index < totalSamples; index ++) {
                //arrayList.add(new Integer(samples[index]));
                //arrayList.add(new Float(samples[index]));
                arrayList.add(samples[index]);
            }
          */
            // If logic for sample insertion changes here update: absTimeGapSeconds(DataCounts,DataCounts) -aww
            List arrayList = new ArrayList(1);
            arrayList.add(samples); // aww to avoid object overhead shortcut ?
            retVal = new DataCounts(dataSegment.startTimestamp, arrayList);
        }
        catch(SeedReaderException ex) {
            ex.printStackTrace();
            System.err.println(ex.getMessage());
        }
        catch(Exception ex) {
            ex.printStackTrace();
            System.err.println("Waveform.getSamples(DataSegment) caught exception " + ex.getMessage());
        }
        return retVal;
    }

/** This method trims a single SEED packet to the specified time window.
*   The Time window and packet time ranges are assumed to overlap.
*   NO IMPLEMENTATION - this method always returns true.
*/
    private boolean trimPacket(DataSegment dataSegment, DataSegment dataSegmentPrevious, TimeWindow tw) {
         return true;
    }

/** Returns a miniSEED data packet Collection comprising the input waveform.
*   Supports only recordSize = MINISEED_REC_512, MINISEED_REC_4096, dataFormat = MINISEED_TYPE
*   with data encoding STEIM1 or STEIM2
*   Returns null if no data exist for input Waveform.
*   @exception java.lang.NullPointerException input parameter is null
*/
    public static List getMiniSEEDPackets(Waveform wave) {
        if (wave == null)
            throw new NullPointerException("Waveform.getMiniSEEDPackets null Waveform input parameter");

        if (wave.dataSegmentList == null || wave.dataSegmentList.isEmpty()) return null;

        ArrayList arrayList = new ArrayList(wave.dataSegmentList.size());

        Iterator iter = wave.dataSegmentList.iterator();
        while (iter.hasNext()) {
            arrayList.add(((DataSegment) iter.next()).dataContent);
        }
        return arrayList;
    }

/** Returns a SEED data packet Collection comprising this waveform.
*   Supports only recordSize = MINISEED_REC_512, dataFormat = MINISEED_TYPE
*   with data encoding STEIM1 or STEIM2
*   Returns null if no data exist for this Waveform.
*/
    List getMiniSEEDPackets() {
        if (dataSegmentList == null || dataSegmentList.isEmpty()) return null;
        ArrayList arrayList = new ArrayList(dataSegmentList.size());
        Iterator iter = dataSegmentList.iterator();
        while (iter.hasNext()) {
            arrayList.add(((DataSegment) iter.next()).dataContent);
        }
        return arrayList;
    }

/** Converts the time-series data found in the data segments of this Waveform into a list of DataCount objects.
*   DataCount objects are appended to the list specified as the input parameter.
*   Contiguous data samples are appended to the end of the DataCount object, if a time gap occurs a new DataCount object
*   is appended to the end of the list.
*   Returns false if an error occurs processing the data.
*   Returns true upon success; if no sample data exist for this waveform, no elements are appended to List.
*   @exception WaveformDataException input parameter is null.
*/
    public boolean getCounts(List dataCountsList) throws WaveformDataException {
        if (dataCountsList == null) throw new WaveformDataException("Waveform.getCounts() input List parameter is null");
        if (dataSegmentList == null || dataSegmentList.isEmpty()) return true;

        Iterator iter = dataSegmentList.iterator();
            // Store the counts into the DataCount list
        while (iter.hasNext()) {

            DataCounts dataCounts = getSamples((DataSegment) iter.next());
            if (dataCounts == null) {
                System.err.println( "Error Waveform.getCounts(): failure getting sample counts");
                return false;
            }

            // Store the counts into the DataCount list
            if (! insertCounts(dataCountsList, dataCounts)) {
                System.err.println( "Error Waveform.getCounts(): failure inserting retrieved counts into list");
                return false;
            }
        }
        return true;
    }

/** Gets the sample rate of the time series data. if any.
*   Returns -1. if data channel is null.
*/
    public double getSampleRate() {
        if (chan == null) return -1.;
        return chan.sampleRate;
    }

/** Returns true if time series data contains time gaps.
*   Returns false if no time gaps or no data.
*/
    public boolean hasTimeGaps() {
        int offset = offsetToFirstTimeGap(dataSegmentList);
        return ( (offset > 0) &&  (offset < dataSegmentList.size()) );
    }

/** Returns index offset to first time tear in data segment list for this Waveform.
*   Returns size of the list if the list has no gaps.
*   Returns 0 if the segment list is empty.
*   @exception java.lang.NullPointerException input Collection parameter null
*/
    int offsetToFirstTimeGap(Collection dataSegmentList) {
        if (dataSegmentList == null)
            throw new NullPointerException("Waveform.offsetToFirstTimeGap null time-series collection parameter");
        if (dataSegmentList.isEmpty()) return 0;
        Iterator iter = dataSegmentList.iterator();
        DataSegment dataSegment = (DataSegment) iter.next();
        int offset = 0;
        while (iter.hasNext()) {
            DataSegment dataSegmentNext = (DataSegment) iter.next();
            offset++;
            if (absTimeGapSeconds(dataSegment, dataSegmentNext) > getEpsilon()) break;
            dataSegment = dataSegmentNext;
        }
        return offset;
    }

/** Number of contiguous time-series segments comprised this waveform. */
    public int numberOfSegments() {
        if (dataSegmentList == null || dataSegmentList.isEmpty()) return 0;
        Iterator iter = dataSegmentList.iterator();
        DataSegment dataSegment = (DataSegment) iter.next();
        int numberOfSegments = 1;
        while (iter.hasNext()) {
            DataSegment dataSegmentNext = (DataSegment) iter.next();
            if (absTimeGapSeconds(dataSegment, dataSegmentNext) > getEpsilon()) { numberOfSegments++; }
            dataSegment = dataSegmentNext;
        }
        return numberOfSegments;
    }

/** Erases all waveform trace data beyond the first time gap */
    void trimAtFirstGap() {
        // Comment out List specific implementation
//      int offset = offsetToFirstTimeGap(this.dataSegmentList);
//      dataSegmentList.subList(offset, dataSegmentList.size()).clear();

        Iterator iter = dataSegmentList.iterator();
        DataSegment dataSegment = (DataSegment) iter.next();
        while (iter.hasNext()) {
            DataSegment dataSegmentNext = (DataSegment) iter.next();
            if (absTimeGapSeconds(dataSegment, dataSegmentNext) > getEpsilon()) {
                iter.remove();
                while (iter.hasNext()) {
                    iter.next();
                    iter.remove();
                }
            }
            break;
        }
    }

/** Create new collection for DataSegment objects.*/
    private Collection createDataSegmentCollection(int size) {
        return new ArrayList(size);
    }

/** Creates a new Waveform whose data copy the next contiguous time series segment within this Waveform.
*   If this Waveform has no time gaps, a complete copy of this Waveform. An internal pointer is advanced with
*   each invocation, so after returning the last contiguous segment, subsequent invocations return null.
*   Returns null if this Waveform time-series data collection has no more time-series segments.
*   @exception java.lang.NullPointerException null member time-series collection
*/
    public Waveform next() {
        if (dataSegmentList == null) throw new NullPointerException("Waveform.next() data segment List is null");
        if (dataSegmentList.isEmpty() || (nextSegmentIndex >= dataSegmentList.size()) ) return null;

        // Create view of list starting with next data segment to end of list
        Iterator iter = dataSegmentList.iterator();
        Collection newDataSegmentList = createDataSegmentCollection(dataSegmentList.size() - nextSegmentIndex);

        // Find the first and last packet of the next contiguous time segment
        int lastSegmentIndex = 0;
        Outer:
        while(iter.hasNext()) {
            lastSegmentIndex++;
            DataSegment dataSegment = (DataSegment) iter.next();
            if (lastSegmentIndex < nextSegmentIndex) continue;
            newDataSegmentList.add(dataSegment);
            while (iter.hasNext()) {
                lastSegmentIndex++;
                DataSegment dataSegmentNext = (DataSegment) iter.next();
                if (absTimeGapSeconds(dataSegment, dataSegmentNext) > getEpsilon()) break Outer; // time gap, quit
                newDataSegmentList.add(dataSegmentNext);
                dataSegment = dataSegmentNext;
            }
        }
        Waveform wave = (Waveform) clone(); // new Waveform(this); // alias new waveform to this object's references
        wave.dataSegmentList = newDataSegmentList;  // replace time series data for new waveform
        nextSegmentIndex = lastSegmentIndex;        // reset offset into this object's time-series data
        return wave;
    }

/** Returns a Waveform containing the specified sublist of the dataSegments (includes first, excludes last index).
*   @exception java.lang.IllegalArgumentException firstIndex > lastIndex
*   @exception java.lang.IndexOutOfBoundsException (lastIndex <= firstIndex || lastIndex > size() || firstIndex < 0)
*/
    Waveform copyData(int firstIndex, int lastIndex) {
        if (lastIndex <= firstIndex || lastIndex > dataSegmentList.size() || firstIndex < 0)
            throw new IndexOutOfBoundsException("Error Waveform.copyData input indices: " + firstIndex + " : " + lastIndex);
        // Comment out List specific implementation
/*
        // Add selected objects of the contiguous subrange as elements of new list
        Waveform wave = (Waveform) this.clone();
        wave.nextSegmentIndex = 0;
        wave.dataSegmentList.clear();
        subList = dataSegmentList.subList(firstIndex, lastIndex));
        wave.dataSegmentList.addAll(subList);
*/
        Waveform wave = (Waveform) clone(); // new Waveform(this);
        wave.dataSegmentList = createDataSegmentCollection(lastIndex - firstIndex);
        Iterator iter = dataSegmentList.iterator();
        int index = -1;
        while(iter.hasNext()) {
            index++;
            Object dataSegment = iter.next();
            if (index < firstIndex) continue;
            else if (index == lastIndex) break;
            wave.dataSegmentList.add(dataSegment);
        }
        return wave;
    }

/** Returns the exclusive allowable seconds between data segments before a data overlap/gap is declared.
*/
    double getEpsilon() {
        return (1.0 + TIME_EPSILON)/chan.sampleRate;
    }

/** Removes all time series data segments found outside of the time bounds found in the specified TimeWindow.
* @exception java.lang.NullPointerException input parameter null
* @exception WaveformDataException null member time-series segment collection
*/
    public boolean trim(TimeWindow tw) {
        if (tw == null) throw new NullPointerException("Waveform.trim() null input TimeWindow parameter");
        if (dataSegmentList == null) throw new WaveformDataException("Waveform.trim() time-series DataSegment collection null");
        Iterator iter = dataSegmentList.iterator();
        DataSegment dataSegmentPrevious = null;
        while (iter.hasNext()) {
            DataSegment dataSegment = (DataSegment) iter.next();
            DateTime endtime = endTime(dataSegment);  // for UTC -aww 2008/04/01                       // Endtime is the time of the last sample in the packet
            if (tw.excludes(dataSegment.startTimestamp, endtime)) {      // Case 1: Packet completely outside of time window
                iter.remove();
            }
            else if (tw.overlaps(dataSegment.startTimestamp, endtime)) {  // Case 2: Packet overlaps with time window
                // Need the previous packet to unpack the current packet properly, but ignore it if a time tear exists.
                if (dataSegmentPrevious == null || absTimeGapSeconds(dataSegmentPrevious, dataSegment) > getEpsilon())
                    dataSegmentPrevious = dataSegment;
                if ( ! trimPacket(dataSegment, dataSegmentPrevious, tw) ) {
                    System.err.println( "INFO Waveform.trim(): data segment overlaps time window, continuing to next segment.");
                //  return false; // disabled packet trimming so don't treat is as error just continue - aww
                }
                if (dataSegment.numberOfSamples == 0) iter.remove();     // delete current packet from collection if no samples
            }
            dataSegmentPrevious = dataSegment;                           // save a reference to last processed data segment
        }
        return true;
    }

/** Return starting time declared for input data segment. */
    private static DateTime startTime(DataSegment dataSegment) { // for UTC -aww 2008/04/01
        return new DateTime(dataSegment.startTimestamp);
    }

/** Return starting time declared for first data segment of this Waveform.
* @exception NullPointerException null member time-series segment collection
*/
    public DateTime startTime() { // for UTC -aww 2008/04/01
      if (dataSegmentList == null) throw new NullPointerException("Waveform.startTime() null DataSegment collection");
      return (dataSegmentList.size() > 0) ? startTime((DataSegment) dataSegmentList.iterator().next()) : new DateTime(0., true);
    }

/** Return ending time declared for input data segment. */
    private DateTime endTime(DataSegment dataSegment) { // for UTC -aww 2008/04/01
        return new DateTime(dataSegment.getStartTimeSecs() + (double)(dataSegment.numberOfSamples - 1)/chan.sampleRate, true);
    }

/** Return ending time declared for last data segment of this Waveform.
* @exception NullPointerException null member time-series segment collection
*/
    public DateTime endTime() { // for UTC -aww 2008/04/01
      if (dataSegmentList == null) throw new NullPointerException("Waveform.endTime() null DataSegment collection");
      if (dataSegmentList.size() <= 0) return new DateTime(0., true);
      Iterator iter = dataSegmentList.iterator();
      Object object = null;
      while(iter.hasNext()) {
          object = iter.next();
      }
      return endTime((DataSegment) object);
    }

/** Appends specified index and DataSegment.toString() to this.toString() */
    private String toString(int index, DataSegment dataSegment) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(toString());
        sb.append("index: ");
        sb.append(index);
        sb.append(" dataSegment: ");
        if (dataSegment == null) sb.append("null");
        else sb.append(dataSegment.toString());
        return sb.toString();
    }

/** Returns String summarizing data member information for the instance. */
    public String toString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("Waveform channel: ");
        if (chan != null) sb.append(chan.toIdString());
        else sb.append("null");
        sb.append(" recordSize: ");
        sb.append(recordSize);
        sb.append(" dataFormat: ");
        sb.append(dataFormat);
        sb.append(" dataEncoding: ");
        sb.append(dataEncoding);
        sb.append(" nextSegmentIndex: ");
        sb.append(nextSegmentIndex);
        sb.append(" #segments: ");
        if (dataSegmentList == null) sb.append("null");
        else sb.append(dataSegmentList.size());
        return sb.toString();
    }
}

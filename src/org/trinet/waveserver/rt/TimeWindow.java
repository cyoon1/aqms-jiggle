package org.trinet.waveserver.rt;
import java.io.*;
import java.util.*;
import org.trinet.util.DateTime;

/** Implementation of the TRINET WaveClient API TimeWindow class.
* This subclass of TrinetSerial encapsulates starting and ending timestamps values  
* (delegated to a SimpleTimeRange instance).
* TimeWindow serialized data values are incorporated into TCPMessage objects which
* can be sent/received between client/server via a byte stream serialization of
* one of more Packet objects through an open TCPConn socket connection.
* The format of the serialization protocol is defined in WaveClient API documentation.
*<p>
*<b> Note public constructor and getXXX() time methods alias the data member references,
*    thus the member data reflect any external modification to the aliases.
*    If preservation of the original data is required, first clone the external alias
*    before modifying its contents.
*</b>
*
*/
public class TimeWindow extends TrinetSerial implements TimeRange, Cloneable, Comparable {

/** Maximum bytes allowed for serialized TimeWindow timestamp data values.<br>
*   Minimum: 8 byte start time + 8 byte end time.  */
    public static final int MAX_SERIAL_BYTES = 32;

/** Starting and ending timestamp wrapper object. */
    SimpleTimeRange simpleTimeRange = null;

/** Constructor initializes data members as aliases of the specified input values.
* @exception java.lang.NullPointerException startTimestamp == null || endTimestamp == null
* @exception java.lang.IllegalArgumentException (startTimestamp <= 0 || endTimestamp<=0 || endTimestamp<startTimestamp)
*/
    public TimeWindow(DateTime startTimestamp, DateTime endTimestamp) { // for UTC -aww 2008/04/01
        super(MAX_SERIAL_BYTES);
        simpleTimeRange = new SimpleTimeRange(startTimestamp, endTimestamp);
        if (! isValid())
            throw new IllegalArgumentException("TimeWindow constructor start and/or end times violate input constraints");
    }

/** Constructor initializes data member values from an input array containing a network serialized form of the data members values.
* @exception java.io.IOException error occurred parsing timestamp values from byte array data input stream.
* @exception java.lang.IllegalArgumentException parsed (startTimestamp<=0 || endTimestamp<=0 || endTimestamp<startTimestamp)
*/
    TimeWindow(byte [] buffer) throws IOException, IllegalArgumentException {
        super(MAX_SERIAL_BYTES);
        simpleTimeRange = new SimpleTimeRange(new DateTime(0.,true), new DateTime(0., true)); // for UTC -aww 2008/04/01
        fromByteArray(buffer);
        if (! isValid())
            throw new IllegalArgumentException("TimeWindow constructor start and/or end times violate input constraints");
    }

/** Sets data members to values read from a network serialized form of these data values in the specified input stream. 
* @exception java.io.IOException error occurred parsing timestamp values from data input stream.
*/
    void readDataMembers(DataInputStream dataIn) throws IOException {
        simpleTimeRange.startTimestamp.setTrueSeconds( dataIn.readDouble() ); // for UTC -aww 2008/04/01
        simpleTimeRange.endTimestamp.setTrueSeconds( dataIn.readDouble() ); // for UTC -aww 2008/04/01
    }

/** Writes the data members values in a network serialized form to the specified output stream. 
* @exception java.io.IOException error occurred writing timestamp values to data output stream.
*/
    void writeDataMembers(DataOutputStream dataOut) throws IOException {
        dataOut.writeDouble( simpleTimeRange.getStartTimeSecs() ); // for UTC -aww 2008/04/01
        dataOut.writeDouble( simpleTimeRange.getEndTimeSecs() ); // for UTC -aww 2008/04/01
    }

/** Returns the alias to the TimeRange delegate of this instance. */
    public TimeRange getTimeRange() {
        return simpleTimeRange;
    }

/** Returns the alias to the TimeRange starting timestamp. */
    public DateTime getStartTimestamp() { // for UTC -aww 2008/04/01
        return simpleTimeRange.startTimestamp;
    }

/** Returns the alias to the TimeRange ending timestamp. */
    public DateTime getEndTimestamp() { // for UTC -aww 2008/04/01
        return simpleTimeRange.endTimestamp;
    }

/** Returns the values of the start time in seconds relative to Date(0l) == GMT Jan 1, 1970. */
    public double getStartTimeSecs() {
        return simpleTimeRange.getStartTimeSecs();
    }

/** Returns the values of the end time in seconds relative to Date(0l) == GMT Jan 1, 1970. */
    public double getEndTimeSecs() {
        return simpleTimeRange.getEndTimeSecs();
    }

/** Returns the time gap seconds between the start time of the input TimeWindow instance and the end time of this TimeWindow instance. */
    public double timeGapSeconds(TimeRange timeWindowNext) {
        return simpleTimeRange.timeGapSeconds(timeWindowNext);
    }

/** Returns the time gap seconds between the input time and the end time of the this TimeWindow instance. */
    public double timeGapSeconds(DateTime nextStartTimestamp) { // for UTC -aww 2008/04/01
        return simpleTimeRange.timeGapSeconds(nextStartTimestamp);
    }

/** True if start time is after date. */
    public boolean after(Date date) {
        return simpleTimeRange.after(date);
    }

/** True if start time is after Timewindow end time. */
    public boolean after(TimeRange tw) {
        return simpleTimeRange.after(tw.getEndTimestamp()); 
    }

/** True if ending time is before date. */
    public boolean before(Date date) {
        return simpleTimeRange.before(date);
    }

/** True if ending time is before date. */
    public boolean before(TimeRange tw) {
        return simpleTimeRange.before(tw.getStartTimestamp()); 
    }

/** True if this object's time range excludes input date. */
    public boolean excludes(Date date) {
        return simpleTimeRange.excludes(date);
    }

/** True if this object's time range excludes input object's range. */
    public boolean excludes(TimeRange tw) {
        return simpleTimeRange.excludes(tw);
    }

/** True if this object's time range excludes input range. */
    public boolean excludes(Date start, Date end) {
        return simpleTimeRange.excludes(start, end);
    }

/** True if this object's time range includes input object's range. */
    public boolean includes(TimeRange tw) {
        return simpleTimeRange.includes(tw); 
    }

/** True if this object's time range includes input range. */
    public boolean includes(Date start, Date end) {
        return simpleTimeRange.includes(start, end);
    }

/** True if this object's time range includes input date. */
    public boolean includes(Date date) {
        return simpleTimeRange.includes(date);
    }

/** True if this object's time range overlaps input object's range. */
    public boolean overlaps(TimeRange tw) {
        return simpleTimeRange.overlaps(tw);
    }

/** True if this object's time range overlaps input range. */
    public boolean overlaps(Date start, Date end) {
        return simpleTimeRange.overlaps(start, end);
    }

/** True if this object's time range lies within input object's range. */
    public boolean within(TimeRange tw) {
        return simpleTimeRange.within(tw); 
    }

/** True if this object's time range lies within input range. */
    public boolean within(Date start, Date end) {
        return simpleTimeRange.within(start, end);
    }

/** Returns a "deep copy" of this instance. */
    public Object clone() {
        TimeWindow tw = null;
        try {
            tw = (TimeWindow) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return tw;
    }

/** Returns true only if the input object is an instance of TimeRange and
* its starting and ending timestamp values are equivalent to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || ! (object instanceof TimeRange)) return false;
        return simpleTimeRange.equals(object);
    }

/** Input object must in an instance of TimeRange. 
* @return <pre>
* -1 this object's starting timestamp value is less than the input object's value,
*    or it is equivalent and this object's ending timestamp value is less than the input object's value.<br>
*  0 this object's starting and ending timestamp values are equivalent to the input object's values.<br>
*  1 this object's starting timestamp value is greater than the input object's value,
*    or it is equivalent and its ending timestamp value is greater than the input object's value. 
*</pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of TimeRange.
*/
    public int compareTo(Object object) {
        return simpleTimeRange.compareTo(object);
    }

/** Returns true if endTimetamp < startTimestamp. */
    public boolean isReversed() {
        return simpleTimeRange.isReversed();
    }

/** Returns true if ! isReversed(), and neither timestamp <= 0. */
    public boolean isValid() {
        return simpleTimeRange.isValid();
    }

/** Returns String concatenation of labeled starting and ending timestamp.*/
    public String toString() {
        return simpleTimeRange.toString();
    }

/** Convenience wrapper for System.out.println(toString()); */
    public void print() {
        System.out.println(toString());
    }
}

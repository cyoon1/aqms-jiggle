package org.trinet.waveserver.rt;
import java.io.*;
import java.net.*;
import java.util.*;

/** Implementation of a TRINET TCPConnServerClient class.
* This class extends the TCPConnClient class it is utilized by the TCPConnServer class as the target 
* of an acceptTCPConnClient() call.
* Methods are implemented to to receive TCPMessage objects from the requesting client.<p> 
*
* A TCPMessage is comprised of a collection of DataField objects whose values are
* "serialized" into Packet objects which are then "serialized" as a byte stream by
* the TCPConn socket connection. The serialization is not java.io.Serializable,
* but rather a protocol implemented by subclasses of TrinetSerial which is
* defined in the WaveClinet/Server API documentation.
*
* @see TCPConn
* @see TCPConnClient
* @see TrinetSerial
* @see TCPMessage
* @see Packet
*/

public class TCPConnServerClient extends TCPConnClient implements TrinetTCPMessageTypes {
/** Constructor sets the socket connection reference to input reference. Initializes the socket attributes to defaults.
*   @exception java.io.IOException error occurred trying to establish the connection.
*   @exception java.lang.NullPointerException null input reference
*   @exception java.lang.Security if SecurityManager exists its checkConnect(...) doesn't allow connection.
*/
    public TCPConnServerClient(Socket socket) throws IOException {
        super(socket);
    }

/** Returns true only if the input object is an instance of this class
* and the Client socket host and port values equal this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        return (super.equals(object)) ? true: false;
    }

/** Returns a String summarizing the labeled connection attributes of the client. */
    public String toString() {
        StringBuffer sb = new StringBuffer(512);
        sb.append(getSocketInfo());
        sb.append("\n");
        sb.append("client:");
        if (client == null) sb.append("null");
        else sb.append(client.toString());
        return sb.toString();
    }

/** Returns a TCPMessage from the client host described by the TCPConn socket.
* The read request is aborted if it is not completed by the default timeout for this instance.
* Returns null if unsuccessful.
*/
    TCPMessage receiveRequest() {
        return receiveRequest(timeoutDefault);
    }

/** Returns a TCPMessage from the client host described by the TCPConn socket.
* The read request is aborted if the request is not completed by the specified timeout.
* Returns null if unsuccessful.
*/
    TCPMessage receiveRequest(int timeoutMilliSecs) {
        TCPMessage requestMessage = null;
        try {
            requestMessage = receive(timeoutMilliSecs);
        }
        catch (TCPConnException ex) {
            System.err.print(">>>Error TCPConnServerClient.receiveRequest() caught TCPConnException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.println(">>>Error TCPConnServerClient.receiveRequest() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        return requestMessage;
    }

/** Parses a request message. Uses the default read timeout setting for this instance.
* Returns true if successful.
*/
    boolean processRequest() {
        return processRequest(timeoutDefault) ;
    }

/** Parses a request message. Uses the specified read timeout. Aborts if the timeout expires.
* Returns true if successful.
*/
    boolean processRequest(int timeoutMilliSecs) {
        boolean retVal = true;
        try { 
            System.err.println(" processRequest bytes available: " + socketInStream.available());
                TCPMessage requestMessage = receiveRequest(timeoutMilliSecs);
                if (requestMessage == null) {
                    System.err.println("TCPConnServerClient.processRequest(int) receiveRequest(int) returned null TCPMessage.");
                    return false;
                }

                System.out.println(requestMessage.toString());

                int reqId = requestMessage.nextFieldValueAsInt();

                System.out.println("Request sequence id: " + reqId);

                switch (requestMessage.getMessageType()) { 
                    case TN_TCP_GETDATA_REQ:
                        Channel chan = new Channel(requestMessage.nextFieldValueAsBytes());
                        System.out.println(chan.toIdString());
                        TimeWindow timeWindow = new TimeWindow(requestMessage.nextFieldValueAsBytes());
                        System.out.println(timeWindow.toString());
                        break;
                    case TN_TCP_GETTIMES_REQ:
                        chan = new Channel(requestMessage.nextFieldValueAsBytes());
                        System.out.println(chan.toIdString());
                        break;
                    case TN_TCP_GETRATE_REQ:
                        chan = new Channel(requestMessage.nextFieldValueAsBytes());
                        System.out.println(chan.toIdString());
                        break;
                    case TN_TCP_GETCHAN_REQ:
                        System.out.println(requestMessage.toString());
                        break;
                    default:
                        System.err.println(">>>Error TCPConnServerClient.processRequest(int) Unrecognized request type in message");
                }
        }
        catch (IOException ex) {
            System.err.print(">>>Error TCPConnServerClient.processRequest(int) IOException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = false;
        }
/*
        catch (Exception ex) {
            System.err.println(">>>Error TCPConnServerClient.processRequest(int) caught exception.");
            ex.printStackTrace();
        }
*/
        return retVal;
    }
}

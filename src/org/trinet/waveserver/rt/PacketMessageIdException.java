package org.trinet.waveserver.rt;
public class PacketMessageIdException extends RuntimeException {
    public PacketMessageIdException () {
        super();
    }

    public PacketMessageIdException(String message) {
        super(message);
    }
}

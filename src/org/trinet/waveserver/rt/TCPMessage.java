package org.trinet.waveserver.rt;
import java.io.*;
import java.util.*;

/** Implementation of the TRINET WaveClient/Server API TCPMessage class.
* This class is used to hold the data content of a WaveClient request and WaveServer response.
* A TCPMessage has a type identifier and is comprised of a collection of DataFields objects.
* For a data request, a list of DataField objects values can be set, then serialized into
* the one or more Packets using the transport protocol described in the API documentation.
* TCPMessages and their corresponding Packets are constructed and sent by a TCPConn object
* to the server. The TCPConn object attempts to read a response TCPMessage from the server.
* If the server responds, the TCPConn object assembles a TCPMessage from received Packets.
* The value content is the restored to a DataField list. The response could either be
* an error code value or a variable amount of data depending on the original request type.
* Message type codes are defined in the TrinetTCPMessageTypes interface.
* @see TrinetTCPMessageTypes
*/
public class TCPMessage implements TrinetTCPMessageTypes {
/** Minimum number of fields in a valid TCPMessage */
    public static final int MIN_NUMBER_OF_MESSAGE_FIELDS = 0;

/** Message type identifier (e.g. request, response) */
    int messageType;

/** Current DataField field iterator */
    Iterator fieldIterator;

/** Current field offset parsed from message.*/
    int readOffset;

/** Collection containing all data fields parsed from message. */
    Collection dataFieldList;

/** Constructor, sets the  message type.*/
    TCPMessage(int messageType) {
        this(messageType, MIN_NUMBER_OF_MESSAGE_FIELDS);
    }
    
/** Constructor, sets the  message type.*/
    TCPMessage(int messageType, int numberOfFields) {
        if (numberOfFields < MIN_NUMBER_OF_MESSAGE_FIELDS)
            throw new IllegalArgumentException("TCPMessage constructor numberOfFieldsparameter < MIN_NUMBER_OF_MESSAGE_FIELDS.");
        this.messageType = messageType;
        dataFieldList = new ArrayList(numberOfFields);
    }
    
/** Sets the message type; should be one of the types in the interface MessageTypes. */
    void setMessageType(int messageType) {
        this.messageType = messageType;
    }
    
/** Returns value of the message type. */
    public int getMessageType() {
        return messageType;
    }
    
/** Returns number of data fields parsed from message. */
    public int getNumberOfDataFields() {
        return dataFieldList.size();
    }
    
/** Set the parse position of the next DataField for i/o
* @exception java.lang.IndexOutOfBoundsException offset outside collection bounds
*/
    void setReadOffset(int offset) {
        if ((dataFieldList.size() <= offset) || (offset < 0)) {
            String message = "Error TCPMessage.setReadOffset(int) Invalid data field offset: " + offset;
            System.err.println(message);
            throw new IndexOutOfBoundsException(message);
        }
        setFieldIterator();
        for (int index = 0; index < offset; index++) {
            fieldIterator.next();
            readOffset++;
        }
    }

/** Creates a new data field collection iterator.
* Resets the field input position offset to beginning of collection.
*/
    void setFieldIterator() {
        fieldIterator = dataFieldList.iterator();
        readOffset = 0;
    }

/** Removes all elements from the data field collection and resets the collection iterator. */
    void clearFieldList() {
        dataFieldList.clear();
        setFieldIterator();
    }

/** Returns String of labeled data member values: message type, input field read offset,
*   and number of data fields in message.
*/  
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append("TCPMessage type: ");
        sb.append(String.valueOf(messageType));
        sb.append(" readOffset: ");
        sb.append(String.valueOf(readOffset));
        sb.append("  fields: ");
        sb.append(String.valueOf(dataFieldList.size()));
        return sb.toString();
    }

/** Prints to System.err exception related error messsages. */
    private void logException(String message) {
        System.err.println("Error TCPMessage: " + message + " \n" + toString());
    }

/** Appends a new DataField constructed from the input int value to the field list.
* @exception java.io.IOException error occurred converting input parameter to a DataField value 
*/
    void appendField(int value) throws IOException {
        DataField dataField = new DataField();
        try {
            dataField.setValue(value);
        }
        catch (IOException ex) {
            logException("appendField(int) value: " + value);
            throw (IOException) ex.fillInStackTrace();
        }
        dataFieldList.add(dataField);
    }

/** Appends a new DataField constructed from the input double value to the field list. 
* @exception java.io.IOException error occured converting input parameter to a DataField value 
*/
    void appendField(double value) throws IOException {
        DataField dataField = new DataField();
        try {
            dataField.setValue(value);
        }
        catch (IOException ex) {
            logException("appendField(double) value: " + value);
            throw (IOException) ex.fillInStackTrace();
        }
        dataFieldList.add(dataField);
    }
    
/** Appends a new DataField constructed from the input String value to the field list.
* @exception java.io.IOException error occured converting input parameter to a DataField value 
* @exception java.lang.NullPointerException input parameter is null
*/
    void appendField(String value) throws IOException {
        if (value == null) throw new NullPointerException("TCPMessage appendField(String) input parameter is null.");
        DataField dataField = new DataField();
        try {
            dataField.setValue(value);
        }
        catch (IllegalArgumentException ex) {
            logException("appendField(String) value: " + value);
            throw new IOException("DataField.appendField(String) " + ex.getMessage());
        }
        dataFieldList.add(dataField);
    }
    
/** Appends a new DataField constructed from the input byte [] values to the field list.
* @exception java.io.IOException error occurred converting input parameter to a DataField value 
* @exception java.lang.NullPointerException input parameter is null
*/
    void appendField(byte [] buffer) throws IOException {
        if (buffer == null) throw new NullPointerException("TCPMessage appendField(byte[]) input parameter is null.");
        DataField dataField = new DataField();
        try {
            dataField.setValue(buffer);
        }
        catch (IllegalArgumentException ex) {
            logException("appendField(byte []) ");
            throw new IOException("DataField.appendField(byte []) " + ex.getMessage());
        }
        dataFieldList.add(dataField);
    }
    
/** Returns an int representing the value of the next data field in the message field list.
* @exception java.io.IOException error occurred converting a DataField value to an int. 
* @exception java.lang.ClassCastException DataField value type is not DF_INTEGER. 
* @see DataFieldConstants
* @see #nextDataFieldValue()
*/
    int nextFieldValueAsInt() throws IOException {
        int retVal = 0;
        try {
            Integer input = (Integer) nextDataFieldValue();
            retVal = input.intValue();
        }
        catch (IOException ex) {
            logException("nextFieldValueAsInt()");
            throw (IOException) ex.fillInStackTrace();
        }
        catch (ClassCastException ex) {
            logException("nextFieldValueAsInt() int data type not in parsed data field");
            throw (ClassCastException) ex.fillInStackTrace();
        }
        return retVal;
    }
    
    
/** Returns a double representing the value of the next data field in the message field list.
* @exception java.io.IOException error occurred converting a DataField value to an double. 
* @exception java.lang.ClassCastException DataField value type is not DF_DOUBLE. 
* @see DataFieldConstants
* @see #nextDataFieldValue()
*/
    double nextFieldValueAsDouble() throws IOException {
        double retVal = 0.;
        try {
            Double input =  (Double) nextDataFieldValue();
            retVal = input.doubleValue();
        }
        catch (IOException ex) {
            logException("nextFieldValueAsDouble()");
            throw (IOException) ex.fillInStackTrace();
        }
        catch (ClassCastException ex) {
           logException("nextFieldValueAsDouble() int data type not in parsed data field");
           throw (ClassCastException) ex.fillInStackTrace();
        }
        return retVal;
    }
    
/** Returns a String representing the value of the next data field in the message field list.
* @exception java.io.IOException error occurred converting a DataField value to a String. 
* @exception java.lang.ClassCastException DataField value type is not a DF_STRING. 
* @see DataFieldConstants
* @see #nextDataFieldValue()
*/
    String nextFieldValueAsString() throws IOException {
        String retVal = null;
        try {
            retVal = (String) nextDataFieldValue();
        }
        catch (IOException ex) {
            logException("nextFieldValueAsString()");
            throw (IOException) ex.fillInStackTrace();
        }
        catch (ClassCastException ex) {
            logException("nextFieldValueAsString() int data type not in parsed data field");
            throw (ClassCastException) ex.fillInStackTrace();
        }
        return retVal;
    }
    
/** Returns an byte array representing the value of the next data field in the message field list.
* @exception java.io.IOException error occurred converting a DataField value to a byte array. 
* @exception java.lang.ClassCastException DataField value type is not DF_BINARY. 
* @see DataFieldConstants
* @see #nextDataFieldValue()
*/
    byte [] nextFieldValueAsBytes() throws IOException {
        byte [] retVal = null;
        try {
            retVal = (byte []) nextDataFieldValue();
        }
        catch (IOException ex) {
            logException("nextFieldValueAsBytes()");
            throw (IOException) ex.fillInStackTrace();
        }
        catch (ClassCastException ex) {
            logException("nextFieldValueAsBytes() binary data type not in parsed data field");
            throw (ClassCastException) ex.fillInStackTrace();
        }
        return retVal;
    }

/** Returns true if the list of data fields in the message has been traversed (e.g. nextFieldValueAsXXX() methods). */
    private boolean isEndOfDataFieldList() {
        if (fieldIterator == null) setFieldIterator();
        return ! fieldIterator.hasNext();
    }

/** Returns an object representing the value content of the next DataField in the message field list.
* @exception java.util.NoSuchElementException isEndOfDataFieldList() == true.
* @exception java.io.IOException data value type cannot be parsed from the DataField.
* @see DataField#getValue() 
*/
    private Object nextDataFieldValue() throws IOException {
        if (isEndOfDataFieldList()) {
            throw new NoSuchElementException("Trinet TCPMessage.nextDataFieldValue() at end of data field list");
        }
        Object dataValue = null;
        DataField dataField = (DataField) fieldIterator.next();
        readOffset++;
        try {
            dataValue = dataField.getValue();
        }
        catch (IOException ex) {
            logException("nextDataFieldValue()");
            throw (IOException) ex.fillInStackTrace();
        }
        return dataValue; 
    }
}

package org.trinet.waveserver.rt;

public class WaveClientOld extends org.trinet.waveserver.rt.WaveClient {

    static {
        org.trinet.waveserver.rt.Channel.new_wire_format = false;
    }

    public WaveClientOld() {
        super();
    }

    public WaveClientOld(int maxRetries, int maxTimeoutMilliSecs, boolean verifyWaveforms, boolean truncateAtTimeGap) {
        super(maxRetries, maxTimeoutMilliSecs, verifyWaveforms, truncateAtTimeGap);
    }

    public WaveClientOld(String propertyFileName) throws WaveClientException {
        super(propertyFileName);
    }

}


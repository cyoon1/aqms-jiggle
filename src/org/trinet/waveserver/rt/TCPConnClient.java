package org.trinet.waveserver.rt;
import java.io.*;
import java.net.*;
import java.util.*;

/** Implementation of a TRINET WaveClient TCPConnClient class.
* This class extends the base TCPConn class and is used by a WaveClient instance to open a
* TCP socket connection to a specified host and port.
* A Client data member instance is used as a wrapper for requested host server address and port number.
* Methods are implemented to to send/receive TCPMessage objects between a WaveClient and a WaveServer
* instances via the TCPConn socket input/outstreams.<p>
* A TCPMessage is comprised of a collection of DataField objects whose values are
* "serialized" into Packet objects which are then "serialized" as a byte stream by
* the TCPConn socket connection. The serialization is not java.io.Serializable,
* but rather a protocol implemented by subclasses of TrinetSerial which is
* defined in the WaveClinet/Server API documentation.
* @see Client
* @see TCPConn
* @see TrinetSerial
* @see TCPMessage
* @see Packet
*/

public class TCPConnClient extends TCPConn implements TrinetReturnCodes, TrinetTCPMessageTypes, java.io.Serializable {
/** Implementation version identifier.*/
    public static final int TCP_CONN_VERSION = 1;

/** Length of field storing the length of a serially formatted DataField. */
    public static final int PACKET_FIELD_HEADER_BYTES = 4;

/** Container for the server name and port id. */
    Client client;

/** Default constructer, a no-op, no connection is made, must invoke methods to create socket connection.
* @see TCPConn#createSocket(String, int)
* @see TCPConn#initSocket()
*/
    public TCPConnClient() {
        super();
    }

/** Constructor creates a socket connection to the host and port specified by the input Client object.
*   Clones the input Client reference.
*   @see Client
*   @exception java.io.IOException error occurred trying to establish the connection.
*   @exception java.lang.NullPointerException input parameter in null
*   @exception java.lang.Security if SecurityManager exists its checkConnect(...) doesn't allow connection.
*   @exception java.net.UnknownHostException host name cannot be resolved by the socket.
*/
    public TCPConnClient(Client client) throws IOException, UnknownHostException {
        if (client == null) throw new NullPointerException("TCPConnClient constructor null input Client parameter.");
        createSocket(client.host, client.port);
        this.client = (Client) client.clone();
    }

/** Constructor creates a socket connection to the host and port specified the inputs.
*   Creates a new Client data member from the input parameters.
*   @exception java.io.IOException error occurred trying to establish the connection.
*   @exception java.net.UnknownHostException host name cannot be resolved by the socket.
*   @exception java.lang.Security if SecurityManager exists its checkConnect(...) doesn't allow connection.
*/
    public TCPConnClient(String host, int port) throws IOException, UnknownHostException {
        super(host, port);
        client = new Client(host, port);
    }

/** Constructor creates a socket connection to the host and port specified the inputs.
*   Creates a new Client data member from the input parameters.
*   @exception java.io.IOException error occurred trying to establish the connection.
*   @exception java.lang.Security if SecurityManager exists its checkConnect(...) doesn't allow connection.
*/
    public TCPConnClient(InetAddress inetAddr, int port) throws IOException {
        super(inetAddr, port);
        client = new Client(inetAddr.getHostName(), port);
    }

/** Constructor set the connection reference to input socket reference. Initializes socket attributes to defaults.
*   @exception java.lang.NullPointerException null input reference
*   @exception java.io.IOException error occurred trying to establish the connection.
*   @exception java.lang.Security if SecurityManager exists its checkConnect(...) doesn't allow connection.
*/
    public TCPConnClient(Socket socket) throws IOException {
        super(socket);
        client = new Client(socket.getInetAddress().getHostName(), socket.getPort());
    }

/** Returns true only if the input object is an instance of this class
* and the Client data members (host, port) have equivalent values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        else if (! super.equals(object) ) return false;
        return (client.equals(client)) ? true: false;
    }

/** Returns a String summarizing the labeled connection attributes of the client. */
    public String toString() {
        StringBuffer sb = new StringBuffer(512);
        sb.append(getSocketInfo());
        sb.append(" client             : ");
        if (client == null) sb.append("null");
        else sb.append(client.toString());
        return sb.toString();
    }

    public long jumpToEndOfInput() throws java.io.IOException {
        if (socketInStream == null) return 0l;

        // else try clearing out the input buffer
        Thread t = Thread.currentThread();
        long count = 0l;
        try {
          t.sleep(100l); // wait for response
          while (socketInStream.available() > 0) { // if nothing is available bail
             count += socketInStream.skip(socketInStream.available()); // skip (ie. read) to end of available
             t.sleep(100l); // wait for more of response to be available
          }
        }
        catch (InterruptedException ex) {
            System.err.println(ex.getMessage());
        }
        //System.out.println("DEBUG jumpToEndOfInput buffer bytes skipped: "  + count);
        return count;
    }

/** Creates a new Packet object from the serialized input data parsed from the socket input stream.
* @exception java.io.IOException error occurred de-serializing the Packet from the socket input stream.
*/
    private Packet receivePacket() throws IOException {

        Packet pkt = new Packet(); // null or zero data members;

        // read in Packet header
        int bytesToRead = Packet.HEADER_BYTES;
        byte [] header = new byte [bytesToRead];
        int bytesRead = 0;
        int offset = 0;
        while (bytesToRead > 0) {
            bytesRead = socketInStream.read(header, offset, bytesToRead);
            if (bytesRead > 0) {
                bytesToRead -= bytesRead;
                offset += bytesRead;
            }
            else if (bytesRead <= EOF) {
               //throw new InterruptedIOException("TCPConnClient receivePacket EOF head - is stream closed?" +
               //       " last read:" +bytesRead+ ", remaining: " +bytesToRead+ ", total read: " +offset );
               if (debug) {
                   System.out.println("TCPConnClient receivePacket EOF head - is stream closed?" +
                      " last read:" +bytesRead+ ", remaining: " +bytesToRead+ ", total read: " +offset );
               }
               return getHeaderErrorPacket(pkt, TN_EOF);
               //System.err.println("TCPConnClient receivePacket EOF packet header - is stream closed by server?");
               //return null;
            }
        }

        pkt.setHeader(header);

        // read in Packet data content.
        bytesToRead = pkt.dataLength;
        byte [] data = new byte [bytesToRead];
        bytesRead = 0;
        offset = 0;
        while (bytesToRead > 0) {
            bytesRead = socketInStream.read(data, offset, bytesToRead);
            if (bytesRead > 0) {
                bytesToRead -= bytesRead;
                offset += bytesRead;
            }
            else if (bytesRead <= EOF) {
               //throw new InterruptedIOException("TCPConnClient receivePacket EOF data - is stream closed?" +
               //       " last read:" +bytesRead+ ", remaining: " +bytesToRead+ ", total read: " +offset );
               if (debug) {
                   System.out.println("TCPConnClient receivePacket EOF data - is stream closed?" +
                      " last read:" +bytesRead+ ", remaining: " +bytesToRead+ ", total read: " +offset );
               }
               return getHeaderErrorPacket(pkt, TN_EOF);
            }
        }

        pkt.setData(data);

        return pkt;
    }

    protected Packet getHeaderErrorPacket(Packet pkt, int errorValue) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream(128);
        DataOutputStream dos = new DataOutputStream(baos); 

        dos.writeInt(TCP_CONN_VERSION); // version
        dos.writeInt(TN_TCP_ERROR_RESP); // msgType
        dos.writeInt(1); // packetNumber
        dos.writeInt(1); // totalMsgPackets

        DataField df = new DataField();
        df.setValue(0); // set a bogus seq# of 0 here
        int flen = df.getSerializedFieldLength();
        // Packet's total data length is that for 2 int fields, ie 2 of (pktFldLen/pktField)
        dos.writeInt(2*(PACKET_FIELD_HEADER_BYTES + flen));

        // now write 1st data field, the seq# set above
        dos.writeInt(flen);       // write its field length
        df.writeDataMembers(dos); // write the field

        df.setValue(errorValue);  // 2nd field is the input header TN code flag
        dos.writeInt(flen);       // write its length, same as above for an int value
        df.writeDataMembers(dos); // write the field

        dos.close();

        byte [] ba = baos.toByteArray(); // get output written to buffer

        // now read all serialized data back into the packet
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(ba));
        pkt.readDataMembers(dis); // reads the packet header its data content
        dis.close();

        return pkt;
    }

/** Creates an empty Packet collection. */
    private Collection createPacketCollection() {
        return new ArrayList();
    }

/** Creates an empty DataField collection. */
    private Collection createDataFieldCollection(int size) {
        return new ArrayList(size);
    }

/** Extracts all serialized DataFields found in the input collection of Packet objects.
* @exception java.io.IOException error occurred de-serializing the DataFields from a Packet object.
*/
    private Collection extractFields(Collection packetList) throws IOException {
        Iterator iter = packetList.iterator();
        Collection dataFieldList = createDataFieldCollection(packetList.size());
        while (iter.hasNext()) {
            Packet pkt = (Packet) iter.next();
            int declaredLength = pkt.dataLength;
            int foundLength = 0;
            DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(pkt.getDataContent()));
            int fieldLength = 0;
            try {
                while (foundLength < declaredLength) {
                    fieldLength = dataIn.readInt();
                    dataIn.readFully(dataFieldBuffer, 0, fieldLength);
                    dataFieldList.add(new DataField(dataFieldBuffer));
                    foundLength += PACKET_FIELD_HEADER_BYTES + fieldLength;
                }
            }
            catch (IOException ex) {
                String msg = ">>>Error TCPConn.extractFields() parsing dataField length: " + fieldLength +
                    " from packet byte stream of declaredLength:" + declaredLength;
                System.err.println(msg);
                if (pkt != null) System.err.println(pkt.toString());
                System.err.println(ex.getMessage());
                //ex.printStackTrace();
                //throw new IOException(msg); // ? remove as test - aww 2010/08/06
            }
            finally {
                try {
                    dataIn.close();
                }
                catch (IOException ex) {
                    System.err.println(">>>Error TCPConn.extractFields() unable to close data in stream for packet data.");
                    System.err.println(ex.getMessage());
                    //ex.printStackTrace();
                }
            }

            if (foundLength != declaredLength) {
                String msg = ">>>Error TCPConn.extractFields() Corrupted packet length (declared,found): " +
                    foundLength + ", " + declaredLength;
                System.err.println(msg);
                throw new IOException(msg);
            }
        }
        return dataFieldList;
    }

/** Creates one TCPMessage from the socket input stream uses default timeout value.
* @exception TCPConnException io error or socket timed out before reading enough data to de-serialize message from socket stream.
*/
    TCPMessage receive() throws TCPConnException {
        return receive(timeoutDefault);
    }

/** Creates one TCPMessage from the socket input stream uses specified input timeout value.
* @exception TCPConnException io error or socket timed out before reading enough data to de-serialize message from socket stream.
*/
    TCPMessage receive(int timeoutMilliSecs) throws TCPConnException {

        Collection packetList = createPacketCollection();

        TCPMessage msg = null;
        try {
            socket.setSoTimeout(timeoutMilliSecs);
            Packet pkt = receivePacket();
            if (pkt == null) {
                //TCPMessage errMessage = new TCPMessage(TN_TCP_ERROR_RESP, 2);
                //errMessage.appendField(0); // sequence number
                //errMessage.appendField(TN_EOF); // TN_FAILURE, TN_NODATA
                //return errMessage;
                return null;
            }
            //DEBUG System.out.println("TCPConnClient receive(int) pkt.toString():\n" + pkt.toString());
            msg = new TCPMessage(pkt.msgType);
            int currentPacket = 1;

            while (pkt.packetNumber <= pkt.totalMsgPackets) {
                if (pkt.version != TCP_CONN_VERSION) {
                    
                    throw new TCPConnException(">>>Error TCPConn.receive() Version mismatch in packet " +
                        "TCP_CONN_VERSION: " + TCP_CONN_VERSION + " "  + pkt.toString());
                }
                else if (msg.messageType != pkt.msgType) {
                    throw new TCPConnException(">>>Error TCPConn.receive() Message type mismatch in packets " +
                        "TCPMessage type: " + msg.messageType + " packet type: " + pkt.msgType +
                        "\n pkt:" + pkt.toString());
                }

                if (currentPacket != pkt.packetNumber) {
                    throw new TCPConnException(">>>Error TCPConn.receive() Packets missing, current,found: " +
                       currentPacket + ", " + pkt.packetNumber +
                        "\n pkt:" + pkt.toString());
                }

                packetList.add(pkt);
                if (pkt.packetNumber == pkt.totalMsgPackets) break;

                pkt = receivePacket();
                currentPacket++;
            }
            socket.setSoTimeout(timeoutDefault);
            msg.dataFieldList = extractFields(packetList);
        }
        catch (IOException ex) {
          if (ex instanceof InterruptedIOException) {
            System.err.println(ex.toString() + " for TCPConn.receive() where timeout: " + timeoutMilliSecs);
            /* t seems like socket available bytes 0, so may have to just reset new Socket connection to server in caller
            try {
                int cnt = socketInStream.available(); // -aww 2010/08/06
                System.out.print("TCPConn.receive socketInStream available bytes: " + cnt );
                System.out.println(", skipped bytes: " + socketInStream.skip((long)cnt) );
            }
            catch (IOException ex2) {
                System.err.println(ex.toString() + " for TCPConn.receive() trying to skip available bytes after timeout");
            }
            finally
            */
            {
                if (msg == null) msg = new TCPMessage(TN_TCP_TIMEOUT_RESP); // - aww  2010/08/06
                else msg.setMessageType(TN_TCP_TIMEOUT_RESP); // - aww  2010/08/06
            }
          }
          else {
            //System.err.println("DEBUG TCPConn.receive() msg.toString(): " + (msg == null ? "NULL" : msg.toString()) + " timeout: " + timeoutMilliSecs);
            //ex.printStackTrace(); // for debug
            System.err.println(">>>Error TCPConn.receive() caught IOException " + ex.getMessage());
            throw new TCPConnException("Error TCPConn.receive() parsing message packets, packet list size: " + packetList.size());
          }
        }
        return msg;
    }

/** Sends the TCPMessage request to the server.
* Returns false if socket connection is null, or an io error occurs.
*/
    boolean send(TCPMessage msg) {
        if (socket == null) return false;

        // Initialize the first packet
        int packetCount = 1;

        // Convert the data field list into packets
        Collection packetList = createPacketCollection();

        DataOutputStream dataOut = null;
        boolean retVal = true;

        try {
            ByteArrayOutputStream arrayOutStream = new ByteArrayOutputStream(Packet.MAX_DATA_BYTES);
            dataOut = new DataOutputStream(arrayOutStream);

            Packet pkt = new Packet(TCP_CONN_VERSION, msg.messageType, packetCount);

            Iterator iter = msg.dataFieldList.iterator();

            int fieldCnt = 0;
            while (iter.hasNext()) {
                fieldCnt++;
                DataField df = (DataField) iter.next();               // retrieve data field from list
                int fieldLength = df.getSerializedFieldLength();      // get its length: header + value
                if((fieldLength + PACKET_FIELD_HEADER_BYTES) > Packet.MAX_DATA_BYTES) {
                     throw new IOException("TCPConn.send() data field length exceeds maximum packet size");
                }
                // if full add to list, make new packet
                if ( (fieldLength + PACKET_FIELD_HEADER_BYTES) > (Packet.MAX_DATA_BYTES - pkt.dataLength)) {
                    pkt.setData(arrayOutStream.toByteArray());        // add array buffer "field" into current packet
                    packetList.add(pkt);                              // add packet to list
                    packetCount++;                                    // bump packet counter
                    pkt = new Packet(TCP_CONN_VERSION, msg.messageType, packetCount); // create next packet
                    pkt.dataLength = 0;                               // reset length for a new packet
                    arrayOutStream.reset();                           // reset the out buffer to the begining for new packet
                }
                dataOut.writeInt(fieldLength);                        // write length to array buffer "field header"
                df.writeDataMembers(dataOut);                         // write field to array buffer "field data"
                pkt.dataLength += PACKET_FIELD_HEADER_BYTES + fieldLength;  // increment field datalength for current packet
            }
            dataOut.flush();

            if (pkt.dataLength > 0) {
                pkt.setData(arrayOutStream.toByteArray());
                packetList.add(pkt); // if data, append the last iteration packet to packet list
            }

            // Iterate over list, reset totalPackets field in packet header then write packet to the socket output stream
            iter = packetList.iterator();
            packetCount = 1;
            while (iter.hasNext()) {
                pkt = (Packet) iter.next();
                pkt.totalMsgPackets = packetList.size();
                pkt.toOutputStream(socketOutStream);
//              pkt.writeDataMembers(new DataOutputStream(socketOutStream));
                packetCount++;
            }
            socketOutStream.flush(); // send the remaining data in the socket buffer, if any
        }
        catch (java.net.SocketException ex) { // dg added // aww 11/24
          System.err.println(">>>Error TCPConn.send() SocketException: ") ;
          System.err.println(socket.toString());
          System.err.println(ex.getMessage());
          //ex.printStackTrace();
          retVal = false;
        }
        catch (IOException ex) {
              System.err.println(">>>Error TCPConn.send() Unable to send TCP message, packetList size: " +
                  packetList.size() + " packet count at error: " + packetCount);
              System.err.println(ex.getMessage());
              //ex.printStackTrace();
              retVal = false;
        }
        finally {
            try {
                dataOut.close();
            }
            catch (IOException ex) {
                System.err.println(">>>Error TCPConn.send() unable to close data output stream to packet byte buffer.");
                System.err.println(ex.getMessage());
                //ex.printStackTrace();
            }
        }
        return retVal;
    }
    /*
    public static void main(String args[]) {
        try {
          TCPConnClient tcp = new TCPConnClient();
          Packet pkt = new Packet();
          pkt = tcp.getHeaderErrorPacket(pkt, TN_EOF);
          System.out.println(pkt.toString());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    */
}

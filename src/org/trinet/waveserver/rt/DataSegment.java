package org.trinet.waveserver.rt;
import java.io.*;
import java.util.*;
import org.trinet.util.DateTime;

/** Implementation of the TRINET WaveClient/Server API DataSegment class. 
* This subclass of TrinetSerial encapsulates a time-stamped contiguous time-series segment from a seismic channel source.
* In this implementation the time series data are stored in the form of a compressed miniSEED packet.
* Data describing the stream channel source and sample rate can be obtained from a decoding of these packets.
* The starting time of the first packet sample, the total number of samples, and the total bytes length of the packet
* are stored as separate data members as a convenience to avoid the extra client overhead of decodes of the packet header
* when comparing DataSegment instances time ordering. <p>
* Typically, a WaveClient getData(...) request for timeseries for a specific seismic channel creates a TCPMessage
* response from the server which contains a collection of zero or more instances of this class.
* Instance data member values are written/read to/from the serialized byte stream sent via Packet objects through a
* TCPConn object socket. The DataSegment collection is then de-serialized and becomes a member of a Waveform object
* or is converted into TimeSeries objects by the WaveClient.
*
* @see TrinetSerial
* @see Packet
* @see TCPConn
* @see TCPMessage
* @see WaveClient
* @see Waveform
*/
public class DataSegment extends TrinetSerial implements Cloneable, Comparable {
/** Maximum size of a data segment serial format in bytes.<BR>
*   Minimum: 8 byte timestamp + 4 byte numberOfSamples + 4 bytes numberOfDataBytes + MAX_DATA_BYTES
*/
/** Maximum size of a data sample buffer that can be stored in a data segment instance. */
    //public static final int MAX_DATA_BYTES = 512; // changed to below for NC Berkeley - 01/23/2006 -aww
    public static final int MAX_DATA_BYTES = 4096;

/** Maximum size of a data sample buffer header that can be stored in a data segment instance. */
    public static final int MAX_HDR_BYTES = 16; // was 52 bytes before, not sure why -aww 01/23/2006

/** Maximum size of a serial message byte stream. */
    public static final int MAX_SERIAL_BYTES = MAX_DATA_BYTES + MAX_HDR_BYTES;

/** Starting timestamp of the first sample to the nearest millisecond. */
    DateTime startTimestamp = null; // for UTC -aww 2008/04/01

/** Total number of time series samples in the data. */
    int numberOfSamples = 0;

/** Total number of bytes in the data packet. */
    int numberOfDataBytes = 0;  // forced by protocol instead of dataContent.length

/** Buffer containing the data packet.*/
    byte [] dataContent = new byte [MAX_DATA_BYTES];

/** Default Constructor initializes members to zero/null values. */
    DataSegment() {
        super(MAX_SERIAL_BYTES);
        // create date so that fromByteArray() setTime(...), getStartTimeMilliSecs() don't blowup.
        startTimestamp = new DateTime(0., true);  // for UTC -aww 2008/04/01

    }

/** Constructor invokes default constructor then initializes data members from an input array containing a
* network serialized form of the data members.
* @see TrinetSerial#fromByteArray(byte [])
* @exception java.io.IOException error occurred parsing the data member values from the input stream.
*/
    DataSegment(byte [] buffer) throws IOException {
        this();
        fromByteArray(buffer);
    }

/** Returns the seconds equivalent of the start time of the first data sample relative to Date(0). */
    double getStartTimeSecs() {
        return startTimestamp.getTrueSeconds(); // for UTC -aww 2008/04/01
    }

/** Sets data members to the values read from a network serialized form of these data values in the specified input stream. 
* @exception java.io.IOException error occurred parsing the data member values from the input stream.
*/
    void readDataMembers(DataInputStream dataIn) throws IOException {
        startTimestamp.setTrueSeconds( dataIn.readDouble() );  // for UTC -aww 2008/04/01
        numberOfSamples = dataIn.readInt();
        numberOfDataBytes = dataIn.readInt();
        Arrays.fill(dataContent, (byte) 0);
        dataIn.readFully(dataContent, 0, numberOfDataBytes);
    }

/** Writes the data members in a network serialized form to the specified output stream.
* @exception java.io.IOException error occurred writing the data member values to the output stream.
*/
    void writeDataMembers(DataOutputStream dataOut) throws IOException {
         dataOut.writeDouble( startTimestamp.getTrueSeconds() );  // for UTC -aww 2008/04/01
         dataOut.writeInt(numberOfSamples);
         dataOut.writeInt(numberOfDataBytes);
         dataOut.write(dataContent, 0, numberOfDataBytes);
    }

/** Creates a "shallow" copy of this object. */
    public Object clone() {
      DataSegment ds = null;
      try {
          ds = (DataSegment) super.clone(); 
      }
      catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
      }
      return ds;
    }

/** No check is done for data packet equivalence, because of the overhead of decoding packet data.
* @return <pre>
* -1 this object's starting timestamp value is less than the input object's,
*    or it is equivalent and this object's number of samples is less than the input object's.<br>
*  0 this object's starting timestamp and number of samples values equal the input object's values.<br>
*  1 this object's starting timestamp value is greater than the input object's,
*    or it is equivalent and its number of samples value is greater than the input object's. 
* </pre>
* @exception java.lang.NullPointerException input object is null.
* @exception java.lang.ClassCastException input object is not an instance of this class.
*/
    public int compareTo(Object object) {
        DataSegment ds = (DataSegment) object;
        int retVal = startTimestamp.compareTo(ds.startTimestamp);
        if (retVal != 0) return retVal;
        if (numberOfSamples == ds.numberOfSamples) return 0;
        return (numberOfSamples < ds.numberOfSamples) ? -1 : 1;
    }

/** Returns true only if the input object is an instance of this class
*  and the respective starting timestamps and the number of samples are equivalent.
*  No check is done for data buffer equivalence, data samples are assumed equivalent.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        else if (! super.equals(object) ) return false;
        DataSegment ds = (DataSegment) object;
        return  (startTimestamp.equals(ds.startTimestamp) && (numberOfSamples == ds.numberOfSamples)) ? true : false;
    }

/** Returns the String concatenation of the labeled startTime, the numberOfSamples,
* and the numberOfDataBytes.
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(96);
        sb.append("DataSegment ");
        sb.append("startTimestamp: ");
        sb.append(startTimestamp);
        sb.append(" samples: ");
        sb.append(String.valueOf(numberOfSamples));
        sb.append(" dataBytes: ");
        sb.append(String.valueOf(numberOfDataBytes));
        return sb.toString();
    }
}

# Jiggle

:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:

#### Introduction

Jiggle is a Graphical User Interface (GUI) software application used for human review and analysis of earthquake waveform data and calculation of accurate earthquake (event) parameters.  Jiggle is part of the post-processing (PP) software suite in the Advanced National Seismic System Quake Monitoring System (AQMS).

This version of Jiggle in this GitLab repository should work with either a Postgres or Oracle database.  However, there is currently a known bug with saving magnitude parameters when running with an Oracle database: [#78]

#### Jiggle directory structure
- ``bin/`` -> scripts to build and run Jiggle, and folder for executable Jiggle jar file.
- ``bin/mapdata/`` -> files for mapping (not committed to repository because of large file size; available for download at https://pasadena.wr.usgs.gov/jiggle/download/mapdata.zip)
- ``lib/`` -> external libraries (.jar files) that Jiggle depends on
- ``prop/`` -> example Jiggle properties files
- ``prop/logs/`` -> Jiggle log files will be stored here when you run Jiggle
- ``src/`` -> source code, ``.class`` files, script to compile

#### How to compile, build, and run Jiggle

Requirements: Java 1.8 needs to be installed on your computer.  [Oracle, OpenJDK, Amazon Corretto]

This compiles Jiggle source code and creates the .class files in ``src/out_classes_8/`` directory.
```
$ cd src/
$ ./compile_jiggle.sh 8   # (for Java 1.8)
```

This builds the executable ``jiggle_p8g.jar`` (for Java 1.8, gtype schema).  You can rename it ``jiggle.jar``.
```
$ cd ../bin/
$ ./build_jiggle.sh 8   # (for Java 1.8)
```

This runs Jiggle (it runs ``jiggle_p8g.jar``), with the properties specified in the input file ``../prop/jiggle.props``.
```
$ cd ../bin/
$ ./run_jiggle.sh 8 ../prop/jiggle.props   # (for Java 1.8)
```
Note: ``../prop/jiggle.props`` in this repository has fake server names, usernames, and passwords.  Replace these with your own input parameters.

#### Jiggle Documentation

* Jiggle user guide (work in progress): https://pasadena.wr.usgs.gov/jiggle/UserGuide/site/, https://aqms.swg.gitlab.io/jiggle-userdocs/
* Jiggle user guide source code (work in progress): https://gitlab.com/aqms.swg/jiggle-userdocs
* Original Jiggle website: https://pasadena.wr.usgs.gov/jiggle/
  * Jiggle release notes (pre-2018): https://pasadena.wr.usgs.gov/jiggle/currentRelNotes.html
  * Jiggle properties info: https://pasadena.wr.usgs.gov/jiggle/JiggleProperties.html
* Original Jiggle source code:
  * Initial commit in this GitLab repository is from [revision 8650](https://vault.gps.caltech.edu/trac/cisn/changeset/8650/) in [AQMS Jiggle SVN repository](https://vault.gps.caltech.edu/trac/cisn/browser/PP/branches/clean-uw-dev-jungle-gtype/) where you can view the change logs 
    * NOTE: access to AQMS Jiggle SVN repository at vault.gps.caltech.edu is restricted - not open to the general public

#!/bin/bash 

# build_jiggle.sh
# This script builds the jiggle.jar executable from compiled .class files and .jar external libraries.
# This script lives in the bin/ directory
# Assumes a "manifest.txt" file lives in the bin/ directory

if [ "$#" -ne 1 ]
then
   echo "Usage: ./build_jiggle.sh <java_version>"
   echo "Possible values of <java_version>: 6, 7, 8"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   6)
      echo "6"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.6.0_65`
      ;;
   7 )
      echo "7"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.7.0_80`
      ;;
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   * )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac

# Input directory for .jar external libraries
LIB_DIR="../lib/"

# Input directory for .class files
CLASS_DIR="../src/out_classes_java"${JAVA_VER}
echo "CLASS_DIR: "${CLASS_DIR}

# Output directory for jiggle.jar file
JAR_DIR="./"

# Output jiggle.jar file name
JAR_FILE="jiggle_p"${JAVA_VER}"g.jar"
echo "JAR_FILE: "${JAR_FILE}

# Build jiggle.jar
jar cmfv manifest${JAVA_VER}.txt ${JAR_FILE} -C ${CLASS_DIR} edu -C ${CLASS_DIR} gov -C ../src images -C ${CLASS_DIR} org    
